unit pre_srm;

{

  Definition unit for the Pre_SRM DLL
  TNO EMSA Petten; Environmental Modelling Sensing and Analysis
  K.F.A. Frumau (arnoud.frumau@tno.nl)

  March 2023

  Revisons:

  12 Feb 2010: ATV Added Roughness and dry deposition function calls
  18 Mei 2010: ATV Added PS_nnmtime
  07 Mar 2011: ATV Added MadExcept exception trapping
  20 Mar 2011: ATV Added Minimum WindSpeed setting
  03 Feb 2012: ATV Added PS_MeteoVerticalProfile function and introduced MultiStation
  in PS_MeteoInit
  31 May 2014:  A.T. Vermeulen leaves ECN and finishes with PRESRM development
  23 Mar 2015:  KFA Frumau (frumau@ecn.nl) continues PRESRM development
  23 Mar 2015:  KFA Frumau added changes in the function information, to do Conc_create with Vdeff
  29 Apr 2015:  KFA Frumau adding and correcting function information
  22 Mar 2016:  KFA Frumau correction function information, eg XML tags
  04 Apr 2017:  KFA Frumau correction function getmeteolongrecord due to internal dll call
  27 Mar 2018:  KFA Frumau corrections/uniformation in description
  12 Mar 2019: KFA Frumau presrm update; inclusion of roughness based on LGN7 instead of LGN5
  09 Mar 2020: KFA Frumau presrm update; longterm meto change from 1995-2004 to 2005-2014
  17 Mar 2021: KFA Frumau presrm update	
  21 Mar 2022: KFA Frumau presrm update; inclusion of roughness based on LGN2020 instead of LGN7
  10 Jun 2022; KFA Frumau presrm update; addition of NH3 and deposition, and prognose years from 2030-2040 for O3 and NH# for Aerius
  30 Mar 2023: KFA Frumau presrm update;	
}

interface

uses
  modeldefs;

const
{$ifdef win64}
  PreSRMDLL = 'pre_srm_64.dll'
{$endif}

{$ifdef win32}
  PreSRMDLL = 'pre_srm.dll'
{$endif}

{$ifdef osx}
  PreSRMDLL = 'libpre_srm.dylib'
{$endif}


;

///	<summary>
///	  PreSRM maakt gebruik van de MadExcept exception handler om runtime fouten
///	  op te sporen en de gegevens daarvan weer te geven. Standaard vangt de DLL
///	  zelf de fouten af in MadExcept en presenteert de gebruiker in dat geval
///	  een Dialoog met informatie over de fout. De melding kan opgeslagen worden
///	  op disk, ge-emailed naar de ontwikkelaar of genegeerd. De gebruiker kan
///	  daarna het programma be�indigen of proberen voort te zetten. Deze
///	  afvangst van exceptions door PreSRM zelf kan ook uitgeschakeld worden,
///	  waarna het aanroepende programma zelf eventuele excepties in PreSRM moet
///	  afhandelen. Naast de gebruikelijke Delphi excepties genereert PreSRM de
///	  volgende eigen excepties: EPreSRMError en EGCNError. PreSRM is standaard
///	  gecompileerd met Range en Stack checking opties aan.
///	</summary>
///	<param name="ByDLL">
///	  Logische waarde die aangeeft of PreSRM de excepties zelf moet afvangen
///	  (TRUE) of dat de aanroepende applicatie dat elf doet (FALSE). Standaard
///	  is TRUE.
///	</param>
procedure PS_HandleExceptions(ByDLL: Boolean); stdcall;

///	<summary>
///	  In het geval dat de eigen EPreSRM exceptie handler niet gebruikt wordt
///	  een foutcode aangemaakt. Via deze functie kan deze foutcode worden
///	  opgevraagd. Normaal geeft deze functie de waarde nul terug.
///	</summary>
function PS_ErrorCode: Integer; stdcall;

///	<summary>
///	  Ontbinding van een standaard variabele van type TDateTime (zoals gebruikt
///	  in TMeteoRec) in waarden voor kalenderjaar, -maand en dag en uur van de
///	  dag. Bijzonder aan deze ontbinding is dat de uren lopen van 1 tot en met
///	  24, in plaats van de standaard 0 tot en met 23. Als de variabele Hour de
///	  waarde 24 heeft, dan is de datum in Year, Month, Day die van de vorige
///	  dag. Dus bijvoorbeeld een TDateTime die normaal gesproken de tijd 23
///	  december 2009 0:00 aanduidt levert volgens deze functie 22 December 24:00
///	  op. Deze functie is toegevoegd voor compatibiliteit met de wijze waarop
///	  SRM3 modellen met tijd rekenen. In andere gevallen wordt aanbevolen met
///	  de tijd ontbindingsfuncties uit de unit SysUtils of DateUtils te rekenen
///	  (zoals DecodeDate, DecodeTime, HourOf etc).
///	</summary>
///	<param name="Time">
///	  Te ontbinden variabele voor de tijd
///	</param>
///	<param name="Year">
///	  Kalenderjaar
///	</param>
///	<param name="Month">
///	  Kalendermaand
///	</param>
///	<param name="Day">
///	  Dag van de maand
///	</param>
///	<param name="Hour">
///	  Uur van de dag. Let op: krijgt waarden van 1 tot en met 24
///	</param>
procedure PS_nnmtime(Time: TDateTime; var Year, Month, Day, Hour: Word); stdcall;

///	<summary>
///	  Geeft het versienummer van de gebruikte PreSRM bibliotheek terug. Het
///	  hoofdversienummer staat in het gedeelte voor de decimale punt, het
///	  subversienummer staat in het gedeelte achter de decimale punt.
///	</summary>
///	<returns>
///	  Het versienummer als floating getal.
///	</returns>
function PS_Version: Double; stdcall;

///	<summary>
///	  Initialisatie van een PS_Proj object.
///	</summary>
///	<param name="Year">
///	  rekenjaar (2005-2030)
///	</param>
///	<param name="X">
///	  midden X-co�rdinaat van het domein in Amersfoortse co�rdinaten [meter
///	</param>
///	<param name="Y">
///	  midden Y-co�rdinaat van het domein in Amersfoortse co�rdinaten [meter]
///	</param>
///	<param name="WX">
///	  breedte van het domein [meter]
///	</param>
///	<param name="WY">
///	  hoogte van het domein [meter]
///	</param>
///	<param name="Path">
///	  basis map (directory) met de PreSRM gegevens (inclusief trailing slash)
///	</param>
///	<param name="Prognose">
///	  True bij gebruik van 10-jarige meteo  (vanaf Presrm versie 2.0 (2020)
///   de langjarige meteo 2005-2014 ipv 1995-2004). Dit is verplicht voor prognose jaren 2023-2030.
///	</param>
///	<returns>
///	  pointer naar nieuwe PS_Proj object
///	</returns>
function PS_Proj_Init(Year: Integer; X, Y, WX, WY: Integer; // RD in meters
  Path: PAnsiChar; Prognose: Boolean): Pointer; stdcall;

///	<summary>
///	  Opvragen van een suggestie voor de te hanteren zeezoutcorrectie in dagen
///	  voor PM10 voor een gridpunt. De waarden zijn gebaseerd op een naar een 1
///	  bij 1 km grid geconverteerde gemeeente- en provinciekaart voor Nederland
///	  met als attribuut de via de RBL voorgeschreven zeezoutcorrectie in �g/m�
///	  c.q. dagen overschrijding van PM10>50 �g/m�. Let wel, officieel geldt
///	  dat de enig correcte waarde die is voor de betreffende gemeente c.q.
///	  provincie waar het punt toe behoort. Door de vergridding naar 1 bij 1 km
///	  kan het voorkomen dat een punt een waarde voor de zeezoutcorrectie
///	  toegewezen krijgt van een naburig punt.
///	</summary>
///	<param name="Proj">
///	  pointer naar PS_Proj object
///	</param>
///	<param name="X">
///	  x-co�rdinaat van het gridpunt in Amersfoortse co�rdinaten [meter]
///	</param>
///	<param name="Y">
///	  y-co�rdinaat van het gridpunt in Amersfoortse co�rdinaten [meter]
///	</param>
///	<param name="Conc">
///	  Suggestie voor de waarde van de zeezout correctie [�g/m�], -999 indien
///	  geen geldige waarde gevonden (bijvoorbeeld boven water of buiten
///	  Nederland)
///	</param>
///	<param name="Days">
///	  Suggestie voor de waarde van de zeezout correctie [dagen >50], -999
///	  indien geen geldige waarde gevonden (bijvoorbeeld boven water of buiten
///	  Nederland)
///	</param>
///	<returns>
///	  Geeft met false aan dat voor tenminste een van beide waarden geen geldige
///	  waarde gevonden kon worden
///	</returns>
function PS_SeaSaltCorrection(Proj: Pointer; X, Y: Integer;
  var Conc, Days: Integer): Boolean; stdcall;

///	<summary>
///	  Opvragen van de oppervlakteruwheid voor een gridpunt.
///	</summary>
///	<param name="Proj">
///	  pointer naar PS_Proj object
///	</param>
///	<param name="X">
///	  x-co�rdinaat van het gridpunt in Amersfoortse co�rdinaten [meter]
///	<param name="Y">
///	  y-co�rdinaat van het gridpunt in Amersfoortse co�rdinaten [meter]
///	</param>
///	<param name="z0class">
///	  resultaat: SRM2 ruwheidsklasse (0=0.03m 1=0.1m 2=0.3m 3=1.0m)
///	</param>
///	<param name="oro">
///	  TRUE: pas extra ruwheid door hoogteverschillen toe; FALSE: geen extra
///	  ruwheid door hoogteverschillen [DEFAULT=false]
///	</param>
///	<returns>
///	  ruwheid [meter]
///	</returns>
function PS_Roughness(Proj: Pointer; X, Y: Integer; var z0class: Integer;
   const oro : boolean = false): Double; stdcall;

///	<summary>
///	  Opvragen van de oppervlakteruwheid representatief voor een groter
///	  oppervlak.
///	</summary>
///	<param name="Proj">
///	  pointer naar PS_Proj object
///	</param>
///	<param name="X">
///	  midden x-co�rdinaat van het gebied in Amersfoortse co�rdinaten [meter]
///	</param>
///	<param name="Y">
///	  midden y-co�rdinaat van het gebied in Amersfoortse co�rdinaten [meter]
///	</param>
///	<param name="WX">
///	  breedte van het opvraagpunt in Amersfoortse co�rdinaten [meter]
///	</param>
///	<param name="WY">
///	  hoogte van het opvraagpunt in Amersfoortse co�rdinaten [meter]
///	</param>
///	<param name="UseLogAveraging">
///	  TRUE: gebruik logaritmische middeling; FALSE: normale lineaire middeling
///	</param>
///	<param name="z0class">
///	  resultaat: SRM2 ruwheidsklasse (0=0.03m 1=0.1m 2=0.3m 3=1.0m)
///	</param>
///	<param name="oro">
///	  TRUE: pas extra ruwheid door hoogteverschillen toe; FALSE: geen extra
///	  ruwheid door hoogteverschillen
///	</param>
///	<returns>
///	  ruwheid [meter]
///	</returns>
function PS_Roughness_Area(Proj: Pointer; X, Y, WX, WY: Integer;
  UseLogAveraging: Boolean; var z0class: Integer;
  const oro : boolean = false): Double; stdcall;

///	<summary>
///	  Initialisatie van een PS_Meteo object.
///   Door aan het co�rdinatenpaar MeteoX, MeteoY de waarde -1,-1 door te geven wordt het
///   projectmidden van het doorgegeven project (proj) gebruikt.
///	</summary>
///	<param name="Proj">
///	  pointer naar PS_Proj object
///	</param>
///	<param name="UseStability">
///	  False: veronderstel neutrale atmosfeer bij schaling wind naar 60 meter
///	  True: verreken lokale en huidige stabiliteit bij opschaling
///	</param>
///	<param name="UserRoughness">
///	  te gebruiken oppervlakteruwheid bij downscalen, SRM2 default =0.08 [meter]
///	</param>
///	<param name="Ust_limit">
///	  Ondergrens voor wrijvingssnelheid U* [m/s] , indien <0 wordt de waarde
///	  niet gebruikt. Indien groter dan nul worden berekende waarden voor U*
///	  kleiner dan deze grens afgekapt naar de limietwaarde en de moninobuhkov
///	  lengte daarop aangepast
///	</param>
///	<param name="MoninObu_Limit">
///	  Bovengrens voor de Monin-Obukhov lengte L [m]. Indien <0 wordt de
///	  waarde niet gebruikt. Indien groter dan nul worden berekende waarden voor
///	  L groter dan deze grens afgekapt naar de limietwaarde en de U* daarop
///	  aangepast
///	</param>
///	<param name="MeteoX">
///	  x-co�rdinaat van het meteo gridpunt in Amersfoortse co�rdinaten [meter]
///	</param>
///	<param name="MeteoY">
///	  y-co�rdinaat van het meteo gridpunt in Amersfoortse co�rdinaten [meter]
///	</param>
///	<param name="MultiStation">
///	  Indien TRUE wordt de meteorologie berekend op basis van de beschikbare
///	  gegevens van 17 meetstations. Indien FALSE wordt de RBL standaard
///	  toegepast waarbij ge�nterpoleerd wordt tussen Eindhoven en Schiphol. [DEFAULT=false]
///	</param>
///	<returns>
///	  pointer naar nieuwe PS_Meteo object
///	</returns>
function PS_Meteo_Init(Proj: Pointer; UseStability: Boolean;
  UserRoughness, Ust_limit, MoninObu_Limit: Double; MeteoX, MeteoY: Integer;
  const MultiStation: Boolean = False): Pointer; stdcall;

///	<summary>
///	  Returns the current meteo factor of the given PS_Meteo object. �
///	</summary>
///	<param name="Meteo">
///	  pointer naar PS_PMeteo object
///	</param>
///	<returns>
///	  The meteo factor.
///	</returns>
function PS_MeteoFactor(Meteo: Pointer): Double; stdcall;

///	<summary>
///	  Berekent de verticale gradient van temperatuur en windsnelheid in de
///	  surface layer (ruwweg onderste 60 meter van de grenslaag).
///	</summary>
///	<param name="Meteo">
///	  pointer naar PS_PMeteo object
///	</param>
///	<param name="Data">
///	  ...
///	</param>
///	<param name="z0">
///	  ...
///	</param>
///	<param name="Height">
///	  ...
///	</param>
///	<param name="TempZ">
///	  Verticale gradient van temperatuur
///	</param>
///	<param name="WindZ">
///	  Verticale gradient van wind
///	</param>
///	<returns>
///	  True indien succesvol
///	</returns>
function PS_MeteoVerticalProfile(Meteo: Pointer; Data: TMeteoRec;
  z0, Height: Double; var TempZ, WindZ: Double): Boolean; stdcall;

///	<summary>
///	  Door het aanroepen van deze procedure kan het aanroepende programma de
///	  waarde van de minimale windsnelheid aanpassen. Standaard staat deze
///	  ingesteld op 1 m/s. Alle windsnelheden beneden deze waarde worden op
///	  deze minimum waarde ingesteld. Deze instelling werkt globaal voor alle
///	  projecten aangemaakt na het wijzigen van deze waarden.
///	</summary>
///	<param name="MinimumWS">
///	  Waarde voor de minimum windsnelheid (0<windspeed<10 m/s)
///	</param>
procedure PS_MeteoSetMinimumWindspeed(MinimumWS: Double); stdcall;

///	<summary>
///	  Door het aanroepen van deze procedure kan het aanroepende programma het
///	  begin jaat van de prognose meteo worden aangepast. Standaard staat deze
///	  ingesteld op het jaar 2005 met een lengte van 10 jaar. Deze instelling
///   werkt globaal voor alle projecten aangemaakt na het wijzigen van deze waarden.
///	</summary>
///	<param name="PrognoseMeteoStart">
///	  Waarde voor het start jaar van de prognose meteo (1995<jaar<2010)
///	</param>
procedure PS_MeteoPrognoseStart(PrognoseMeteoStart: Integer); stdcall;

///	<summary>
///	  Geeft het aantal records (uren) in de meteodata voor het rekenjaar weer.
///	  In prognose modus is dit aantal ruim 87600, in andere jaren is dit 8760
///	  of 8784.
///	</summary>
///	<param name="Meteo">
///	  pointer naar een PS_Meteo object
///	</param>
///	<returns>
///	  aantal records (uren) in de meteodata
///	</returns>
function PS_MeteoNrRecords(Meteo: Pointer): Integer; stdcall;

///	<summary>
///	  Ophalen van een meteorecord voor een bepaalde tijd . Bij terugkeer levert
///	  deze functie een record met alle benodigde uurlijkse waarden voor de
///	  meteo. Als de opgevraagde tijd niet beschikbaar is levert de functie de
///	  waarde false.
///	</summary>
///	<param name="Meteo">
///	  pointer naar een PS_Meteo object
///	</param>
///	<param name="Time">
///	  volgnummer van het meteorecord (0..PS_MeteoNrRecords-1)
///	</param>
///	<param name="Data">
///	  resultaat: record met de gewenste meteogegevens
///	</param>
///	<returns>
///	  True indien succesvol
///	</returns>
function PS_MeteoGetByTime(Meteo: Pointer; Time: TDateTime; var Data: TMeteoRec)
  : Boolean; stdcall;

///	<summary>
///	  Ophalen van een meteorecord met een rangnummer. Het aantal records kan
///	  opgevraagd worden met PS_MeteoNrRecords. Bij terugkeer levert deze
///	  functie een record met alle benodigde uurlijkse waarden voor de meteo.
///	</summary>
///	<param name="Meteo">
///	  pointer naar een PS_Meteo object
///	</param>
///	<param name="Nr">
///	  volgnummer van het meteorecord (0..PS_MeteoNrRecords-1)
///	</param>
///	<param name="Data">
///	  resultaat: record met de gewenste meteogegevens
///	</param>
///	<returns>
///	  True indien succesvol
///	</returns>
function PS_MeteoGetData(Meteo: Pointer; Nr: Integer; var Data: TMeteoRec)
  : Boolean; stdcall;

///	<summary>
///	  Initialisatie van een PS_Conc object. Door aan het co�rdinatenpaar
///	  ConcX,ConcY de waarde -1,-1 door te geven wordt het projectmidden van het
///	  doorgegeven project (proj) gebruikt om de uurlijkse waarden uit GCN op te
///	  vragen.
///	</summary>
///	<param name="Proj">
///	  pointer naar PS_Proj object
///	</param>
///	<param name="Comp">
///	  set van in het PS_Conc object weer te geven componenten
///	</param>
///	<param name="ConcX">
///	  RD X co�rdinaat van het opvraagpunt voor uurlijkse waarden [m]
///	</param>
///	<param name="ConcY">
///	  RD Y co�rdinaat van het opvraagpunt voor uurlijkse waarden [m]
///	</param>
///	<param name="DoDepo">
///	  optionele parameter die aangeeft of droge depositie berekeningen moeten
///	  worden uitgevoerd, indien TRUE neemt de initialisatie van PreSRM objecten
///	  aanzienlijk meer tijd en geheugen in beslag. [DEFAULT=false]
///	</param>
///	<param name="Vdeff_PreCalc">
///   Indien TRUE dan lezen van voorberekende effectieve depositiesnelheid met
///   OPS, als FALSE dan door gebruik van GDN kaarten. [DEFAULT=true]. Wordt
///   enkel gebruikt indien DoDepo=true.
///	</param>
///	<returns>
///	  pointer naar het nieuwe resulterende PS_Conc object
///	</returns>
function PS_Conc_Init(Proj: Pointer; Comp: CompSet; ConcX, ConcY: Integer;
  const Depo: Boolean = False;
  const Vdeff_Precalc : boolean = true): Pointer; stdcall;

///	<summary>
///	  Levert de jaargemiddelde concentratie voor een component op een bepaalde
///	  locatie. De resulterende waarde is exclusief de dubbeltellingscorrectie
///	  voor de invloed van het HWN. In het geval dat deze correctie toegepast
///	  moet worden dient de waarde van deze correctie gegeven door variabele dbc
///	  van het resultaat te worden afgetrokken. In de buurt van de luchthaven
///	  Schiphol wordt de bijdrage van NO2 & O3 voor de luchthaven op een resolutie
///	  van 100 meter in de achtergrondwaarde meegenomen. In het IJmond gebied
///   wordt de bijdrage van pm10 en pm2.5 op een resolutie van 250 meter in de
///   achtergrondwaarde meegenomen.
///	</summary>
///	<param name="Conc">
///	  pointer naar PS_Conc object
///	</param>
///	<param name="Comp">
///	  de component waarvoor de waarde gegeven moet worden
///	</param>
///	<param name="X">
///	  de x-co�rdinaat van het punt waarvoor de waarde gegeven moet worden
///	</param>
///	<param name="Y">
///	  de y-co�rdinaat van het punt waarvoor de waarde gegeven moet worden
///	</param>
///	<param name="Dbc">
///	  waarde voor de dubbeltellingscorrectie voor de bijdrage van HWN
///	</param>
///	<returns>
///	  resultaat voor de jaargemiddelde concentratie (inclusief HWN invloed)
///	</returns>
function PS_MeanConc(Conc: Pointer; Comp: TComponent; X, Y: Integer;
  var Dbc: Single): Single; stdcall;

///	<summary>
///	  Levert de uurgemiddelde waarden voor de concentratie van de gewenste
///	  component op de gewenste plek. De dubbeltellingscorrectie voor
///	  de invloed van het HWN is dus niet toegepast. De waarde voor de HWN
///	  dubbeltellingscorrectie wordt teruggegeven in de parameter dbc. Het
///	  resultaat zijn twee gevulde dynamische arrays met de tijd in tDateTime
///	  formaat en de concentraties in �g/m�. De initialisatie op de juiste
///	  lengte dient door de aanroepende applicatie verzorgd te worden. De
///	  benodigde lengte is op te vragen via de PS_MeteoNrRecords call uit een
///	  PS_Meteo object voor hetzelfde PS_Project object.
///	</summary>
///	<param name="Conc">
///	  pointer naar PS_Conc object
///	</param>
///	<param name="Comp">
///	  de component waarvoor de waarde gegeven moet worden
///	</param>
///	<param name="X">
///	  de x-co�rdinaat van het punt waarvoor de waardes gegeven moet worden
///	</param>
///	<param name="Y">
///	  de y-co�rdinaat van het punt waarvoor de waardes gegeven moet worden
///	</param>
///	<param name="Times">
///	  dynamische array met de uurlijkse tijden
///	</param>
///	<param name="Data">
///	  dynamische array met de uurlijkse concentratiewaarden (inclusief HWN
///	  invloed)
///	</param>
///	<param name="Dbc">
///	  waarde voor de dubbeltellingscorrectie voor de invloed van het HWN
///	</param>
procedure PS_ConcGetData(Conc: Pointer; Comp: TComponent; X, Y: Integer;
  var Times: TTimArray; var Data: TSingleArray; var Dbc: Single);
  stdcall; export;

///	<summary>
///	  Levert de uurgemiddelde waarden voor de concentratie van de gewenste
///	  component op de gewenste plek. De dubbeltellingscorrectie voor
///	  de invloed van het HWN is dus niet toegepast. De waarde voor de HWN
///	  dubbeltellingscorrectie wordt teruggegeven in de parameter dbc. Het
///	  resultaat zijn twee gevulde dynamische arrays met de tijd in tDateTime
///	  formaat en de concentraties in �g/m�. De initialisatie op de juiste
///	  lengte dient door de aanroepende applicatie verzorgd te worden. De
///	  benodigde lengte is op te vragen via de PS_MeteoNrRecords call uit een
///	  PS_Meteo object voor hetzelfde PS_Project object.
///	</summary>
///	<param name="Conc">
///	  pointer naar PS_Conc object
///	</param>
///	<param name="Comp">
///	  de component waarvoor de waarde gegeven moet worden
///	</param>
///	<param name="X">
///	  de x-co�rdinaat van het punt waarvoor de waardes gegeven moet worden
///	</param>
///	<param name="Y">
///	  de y-co�rdinaat van het punt waarvoor de waardes gegeven moet worden
///	</param>
///	<param name="Year">
///	  Het jaar waarvoor de concentratiegegevens moeten worden opgehaald, altijd
///	  gelijk aan het projectjaar in een verleden jaar, in geval van een
///	  prognoseberekening vanaf Presrm versie 2.0 (2020) de langjarige meteo
///   2005-2014 i.p.v. 1995-2004.
///	</param>
///	<param name="Times">
///	  dynamische array met de uurlijkse tijden
///	</param>
///	<param name="Data">
///	  dynamische array met de uurlijkse concentratiewaarden (inclusief HWN
///	  invloed)
///	</param>
///	<param name="NrRecords">
///	  Aantal waarden in de data en time array (8760 of 8784)
///	</param>
///	<param name="Dbc">
///	  waarde voor de dubbeltellingscorrectie voor de invloed van het HWN
///	</param>
procedure PS_ConcGetDataByYear(Conc: Pointer; Comp: TComponent;
  X, Y, Year: Integer; var Times: TTimArray; var Data: TSingleArray;
  var NrRecords: Integer; var Dbc: Single); stdcall;


function PS_ConcDryDepEstimate(conc: Pointer;
                               c: tcomponent;
                               x, y: Integer;
                               contrib, distance: Double;
                               oroinc : boolean;
                               var ddep, ddepdbc, vd: Single): Single;stdcall;

///	<summary>
///	  Initialisatie van een PS_ClassData object
///	</summary>
///	<param name="Proj">
///	  pointer naar PS_Project object
///	</param>
///	<param name="Meteo">
///	  pointer naar PS_Meteo object
///	</param>
///	<param name="Conc">
///	  pointer naar PS_Conc object
///	</param>
///	<param name="NrSecWind">
///	  aantal windroos sectoren voor de meteorologie (12)
///	</param>
///	<param name="NrSecConc">
///	  aantal windroos sectoren voor de concentraties (36)
///	</param>
///	<param name="Dbc">
///	  true: pas dubbeltellingscorrectie voor HWN toe
///	</param>
///	<returns>
///	  pointer naar het resulterende PS_Classdata object
///	</returns>
function PS_ClassData_Init(Proj, Meteo, Conc: Pointer; NrSecWind: Integer;
  NrSecConc: Integer; Dbc: Boolean): Pointer; stdcall;

///	<summary>
///	  Instellen van de huidige co�rdinaten van het PS_Classdata object
///	</summary>
///	<param name="ClassData">
///	  pointer naar het PS_Classdata object
///	</param>
///	<param name="X">
///	  de x-co�rdinaat van het punt waarvoor de waardes gegeven moet gaan worden
///	</param>
///	<param name="Y">
///	  de y-co�rdinaat van het punt waarvoor de waardes gegeven moet gaan worden
///	</param>
procedure PS_Class_SetCoord(ClassData: Pointer; X, Y: Integer); stdcall;

///	<summary>
///	  Geeft de waarden van de fractie van voorkomen van de windsnelheidsklasse
///	  UClass (0..2) in windsector USec (0..NrSecWind-1). Windroos is opgebouwd
///	  uit segmenten van hoek 360/NrSecWind. Sector 0 loopt van
///	  360-hoek/2..hoek/2 graden, sector 1 loopt van hoek/2..3*hoek/2 (bv 355-5,
///	  resp 5..15 graden bij NrSecWind=36, dus 10 graden sectoren).
///	  Windsnelheidklasse 0 is van u<2.75 m/s, klasse 1 is van u=2.75-5.75
///	  m/s en klasse 2 is u>5.75 m/s.
///	</summary>
///	<param name="ClassData">
///	  pointer naar het PS_Classdata object
///	</param>
///	<param name="USec">
///	  windsector (0..NrSecWind-1).
///	</param>
///	<param name="UClass">
///	  windsnelheidsklasse (0..2)
///	</param>
///	<param name="Weight">
///	  fractie van voorkomen van de gevraagde klasse van windsnelheid en
///	  �richting
///	</param>
///	<returns>
///	  True indien geslaagd
///	</returns>
function PS_Class_Meteo(ClassData: Pointer; USec, UClass: Integer;
  var Weight: Double): Boolean; stdcall;

///	<summary>
///	  Geeft de waarden van de fractie van voorkomen in windsector USec
///	  (0..NrSecWind-1) en de bijbehorende concentraties van de gevraagde
///	  component.
///	</summary>
///	<param name="ClassData">
///	  pointer naar het PS_Classdata object
///	</param>
///	<param name="Sec">
///	  windsector (0..NrSecWind-1).
///	</param>
///	<param name="Comp">
///	  de component waarvoor de waarde gegeven moet worden
///	</param>
///	<param name="Conc">
///	  concentratie van de gewenste component in de windsector
///	</param>
///	<param name="Weight">
///	  fractie van voorkomen van de gevraagde klasse van windsector
///	</param>
///	<returns>
///	  True indien geslaagd
///	</returns>
function PS_Class_Conc(ClassData: Pointer; Sec: Integer; Comp: TComponent;
  var Conc: Double; var Weight: Double): Boolean; stdcall;

///	<summary>
///	  Opruimfunctie voor alle objecten van PreSRM. Niet alle objecten hoeven
///	  ge�nitialiseerd te zijn. Dit kan door een dummy pointer met waarde nil
///	  door te geven.
///	</summary>
///	<param name="Proj">
///	  PS_Project object om op te ruimen
///	</param>
///	<param name="Meteo">
///	  PS_Meteo object om op te ruimen
///	</param>
///	<param name="Conc">
///	  PS_Conc object om op te ruimen
///	</param>
///	<param name="ClassData">
///	  PC_ClassData object om op te ruimen
///	</param>
procedure PS_Cleanup(var Proj, Meteo, Conc, ClassData: Pointer);
  stdcall; export;

///	<summary>
///	  TODO...
///	</summary>
procedure PS_DryDepEffective(Proj, Meteo, Conc: Pointer; c: CompSet;
  X, Y: Integer; var Result: TCompArray); stdcall;


/// <summary>
/// Get hourly NNM meteo data for a multi-year period
/// within the period 1990 up to the year preceeding the releaseyear of PreSRM
///
///	</param>
///	<param name="RDX">
///	  positie receptor RD coordinaat X [m]
///	</param>
///	<param name="RDY">
///	  positie receptor RD coordinaat Y [m]
///	</param>
///	<param name="Yearstart">
///	  Start year of long meteo record (>=1990).
///	</param>
///	<param name="yearend">
///	  End year of long meteo record (<release year).
///	</param>
///	<param name="z0">
///	  Optionally z0 used for the scaling of the windspeed. If z0<0 then
///   the z0 will be looked up in the database for the coordinate X,Y
///   without orographic correction.
///	</param>
///	<param name="Ustlimit">
///	  Ondergrens voor wrijvingssnelheid U* [m/s] , indien <0 wordt de waarde
///	  niet gebruikt. Indien groter dan nul worden berekende waarden voor U*
///	  kleiner dan deze grens afgekapt naar de limietwaarde en de moninobuhkov
///	  lengte daarop aangepast
///	</param>
///	<param name="MoninLimit">
///	  Bovengrens voor de Monin-Obukhov lengte L [m]. Indien <0 wordt de
///	  waarde niet gebruikt. Indien groter dan nul worden berekende waarden voor
///	  L groter dan deze grens afgekapt naar de limietwaarde en de U* daarop
///	  aangepast
///	</param>
///	<param name="Datadir">
///	  basis map (directory) met de PreSRM gegevens (inclusief trailing slash)
///	</param>
///	<param name="Data">
///	  resultaat: record met de gewenste meteogegevens
///   The data array should be pre-allocated to the required length
///   (number of hours in the requested period) = round(encodedate(endyear+1,1,1)-encodedate(startyear,1,1))*24
///	</param>
///	<param name="Multi">
///	  Indien TRUE wordt de meteorologie berekend op basis van de beschikbare
///	  gegevens van 18 meetstations. Indien FALSE wordt de RBL standaard
///	  toegepast; ge�nterpoleerd tussen Eindhoven en Schiphol. [DEFAULT=false]
///	</param>
/// </summary>
procedure PS_GetMeteoLongPeriod(RDX, RDY, YearStart, YearEnd: Integer;
  z0, Ustlimit, Moninlimit: Double; DataDir: PAnsiChar; var Data: TMeteoArray;
  const Multi: Boolean = False); stdcall;

implementation

procedure PS_HandleExceptions; stdcall; external PreSRMDLL;
function PS_ErrorCode; stdcall; external PreSRMDLL;
function PS_Version; stdcall; external PreSRMDLL;
procedure PS_nnmtime; stdcall; external PreSRMDLL;
function PS_Proj_Init; stdcall; external PreSRMDLL;
function PS_SeaSaltCorrection; stdcall; external PreSRMDLL;
function PS_Roughness; stdcall; external PreSRMDLL;
function PS_Roughness_Area; stdcall; external PreSRMDLL;
function PS_Meteo_Init; stdcall; external PreSRMDLL;
function PS_MeteoFactor; stdcall; external PreSRMDLL;
function PS_MeteoVerticalProfile; stdcall; external PreSRMDLL;
procedure PS_MeteoSetMinimumWindspeed; stdcall; external PreSRMDLL;
procedure PS_MeteoPrognoseStart; stdcall; external PreSRMDLL;
function PS_MeteoNrRecords; stdcall; external PreSRMDLL;
function PS_MeteoGetByTime; stdcall; external PreSRMDLL;
function PS_MeteoGetData; stdcall; external PreSRMDLL;
function PS_Conc_Init; stdcall; external PreSRMDLL;
function PS_MeanConc; stdcall; external PreSRMDLL;
procedure PS_ConcGetData; stdcall; external PreSRMDLL;
procedure PS_ConcGetDataByYear; stdcall; external PreSRMDLL;
function PS_ConcDryDepEstimate;stdcall;external PreSRMDLL;
function PS_ClassData_Init; stdcall; external PreSRMDLL;
procedure PS_Class_SetCoord; stdcall; external PreSRMDLL;
function PS_Class_Meteo; stdcall; external PreSRMDLL;
function PS_Class_Conc; stdcall; external PreSRMDLL;
procedure PS_Cleanup; stdcall; external PreSRMDLL;
procedure PS_DryDepEffective; stdcall; external PreSRMDLL;
procedure PS_GetMeteoLongPeriod; stdcall; external PreSRMDLL;

end.

