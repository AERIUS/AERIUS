/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

#ifdef cl_khr_fp64
    #pragma OPENCL EXTENSION cl_khr_fp64 : enable
#elif defined(cl_amd_fp64)
    #pragma OPENCL EXTENSION cl_amd_fp64 : enable
#else
    #error "Double precision floating point not supported by OpenCL implementation."
#endif

$$CONSTANTS_INSERT$$

double minSegmentDistance(double2 vector) {
  return max(3.5, length(vector));
}
// The MIN_SEGMENT_RADIUS is used to test if we should calculate (In a if statement).
// Set to 0 (< 10) so it's always calculated
#define MIN_SEGMENT_RADIUS 0.0
#define MIN_SEGMENT_DISTANCE minSegmentDistance

// #define WIND_SECTOR is provided by java.
// #define MAX_SOURCE_DISTANCE is provided by java.
#define MIN_DISTANCE 0.5

// Empirically determined constant used for calculating NO2 concentrations.
#define CONSTANT_K 1.0E2

#define DV_FACTOR_MIN_DIST 25.0
#define DV_FACTOR_MAX_DIST 1000.0
#ifdef VERSION_SRM2022
#define DV_FACTOR_NOX (double2) (-0.176, 2.221)
#define DV_FACTOR_NH3 (double2) (-0.090, 1.588)
#endif
#ifdef VERSION_SRM2023
#define DV_FACTOR_NOX (double2) (-0.177, 2.231)
#define DV_FACTOR_NH3 (double2) (-0.090, 1.588)
#endif

/*
 * The roughness class correction factor needs to be interpolated linearly between Schiphol and Eindhoven.
 * The lines used to interpolate are not perpendicular to the ax Schiphol - Eindhoven but use the formula
 * y = 1.21 * x + as
 *
 * as stands for 'as afsnede', the value of y where x = 0.
 *
 * The factor of Schiphol is then
 * fs = (as_point - as_eindhoven) / (as_schiphol - as_eindhoven)
 * Where fs is between 0.0 and 1.0 (both inclusive)
 *
 * The factor of Eindhoven is then
 * fe = 1.0 - fs
 */

#define AS_TAN 1.21
// Need to use scientific notation, otherwise compilation fails.
#define AS_EINDHOVEN 1.97555E5
#define EINDHOVEN_FACTOR 0.90
#define AS_SCHIPHOL 3.42455E5
// AS_LENGTH = AS_SCHIPHOL - AS_EINDHOVEN
#define AS_LENGTH 1.44900E5
/*
 * A roughness class contains the following parameters. These can be accessed with x, y, z and w.
 * The 'a' value for this classification of roughness (used for vertical diffusion).
 * The 'b' value for this classification of roughness (used for vertical diffusion).
 * moninObukhovLength The Monin-Obukhof length (used for wind correction factor).
 * correctionMeteoSchiphol The correction factor for meteo at Schiphol.
 */
constant double4 ROUGHNESS_CLASSES[4] = {
    (double4)(0.2221, 0.6574, 60.0, 0.7000),
    (double4)(0.2745, 0.6688, 60.0, 0.7050),
    (double4)(0.3613, 0.6680, 100.0, 0.6525),
    (double4)(0.7054, 0.6207, 400.0, 0.7400)
};

#define ROUGHNESS_CLASS_2_MINIMUM 0.055
#define ROUGHNESS_CLASS_3_MINIMUM 0.17
#define ROUGHNESS_CLASS_4_MINIMUM 0.55

int z0ToIndex (double z0) {
  if (z0 < ROUGHNESS_CLASS_2_MINIMUM) {
    return 0;
  } else if (z0 < ROUGHNESS_CLASS_3_MINIMUM) {
    return 1;
  } else if (z0 < ROUGHNESS_CLASS_4_MINIMUM) {
    return 2;
  } else {
    return 3;
  }
}

/**
 * Ruwheidsklasse (m) Afbakening ruwheidsklasse (m)
 * < 0,055            -> 0,03
 * >= 0,055 en < 0,17 -> 0,10
 * >= 0,17 en < 0,55  -> 0,30
 * >= 0,55            -> 1,00
 */
double4 determineRoughnessClassification (const double z0) {
  int index = z0ToIndex(z0);
  return ROUGHNESS_CLASSES[index];
}

// Constants used for calculating the psi function for vertical diffusion.
#define PSI_CONSTANT_1 -17.0
#define PSI_CONSTANT_2 -0.29

#define VERTICAL_DIFFUSION_CONSTANT 2800.0

#define Z10 10.0

#define DAY_CORRECTION_FACTOR 1.15

#define HEXAGON_RADIUS 62.04032

#define HEXAGON_HALF_HEIGHT 53.72845

/**
 * Looks up a single double value in a map.
 */
double mapLookupf(global const double* map, const int width, const int height, const double offsetX, const double offsetY, const double diameter, const double defaultValue, double2 position) {
  const int dX = (position.x - offsetX) / diameter;
  const int dY = (position.y - offsetY) / diameter;

  if (dX < 0 || dX >= width || dY < 0 || dY >= height) {
    return defaultValue;
  } else {
    return map[dY * width + dX];
  }
}

double lookupRoughnessSource(
    global const double* landUseMed,
    double2 sourcePosition) {
  return mapLookupf(landUseMed, LANDUSE_MED_WIDTH, LANDUSE_MED_HEIGHT, LANDUSE_MED_OFFSET_X, LANDUSE_MED_OFFSET_Y, LANDUSE_MED_DIAMETER, LANDUSE_MED_DEFAULT, sourcePosition);
}

double lookupRoughnessReceptor(
    global const double* landUseLow,
    global const double* landUseMed,
    global const double* landUseHigh,
    double2 receptorPosition,
    double distance) {

  double z0;
  double defaultValue;

  if (distance < 500) {
    defaultValue = LANDUSE_HIGH_DEFAULT;
    z0 = mapLookupf(landUseHigh, LANDUSE_HIGH_WIDTH, LANDUSE_HIGH_HEIGHT, LANDUSE_HIGH_OFFSET_X, LANDUSE_HIGH_OFFSET_Y, LANDUSE_HIGH_DIAMETER, LANDUSE_HIGH_DEFAULT, receptorPosition);
  } else if (distance < 1500) {
    defaultValue = LANDUSE_MED_DEFAULT;
    z0 = mapLookupf(landUseMed, LANDUSE_MED_WIDTH, LANDUSE_MED_HEIGHT, LANDUSE_MED_OFFSET_X, LANDUSE_MED_OFFSET_Y, LANDUSE_MED_DIAMETER, LANDUSE_MED_DEFAULT, receptorPosition);
  } else {
    defaultValue = LANDUSE_LOW_DEFAULT;
    z0 = mapLookupf(landUseLow, LANDUSE_LOW_WIDTH, LANDUSE_LOW_HEIGHT, LANDUSE_LOW_OFFSET_X, LANDUSE_LOW_OFFSET_Y, LANDUSE_LOW_DIAMETER, LANDUSE_LOW_DEFAULT, receptorPosition);
  }
  return z0;
}

/**
 * Calculate vertical diffusion coefficient
 *
 *           a Rb^b
 * sigmaz = -------- + sigmaz0
 *           f(Rb)
 * Where f(Rb) is:
 *   f(Rb) = 1 + 0.5*(1-e-(Rb/2800)^2)
 *
 * @param distance
 * @param roughnessClass
 * @param sigmaz0
 */
double calculateVerticalDiffusion (const double distance, const double4 roughnessClass, double sigmaz0) {
  const double f = 1.0 + 0.5 * (1.0 - exp(-pown(distance / VERTICAL_DIFFUSION_CONSTANT, 2)));
  return (roughnessClass.s0 * pow(distance, roughnessClass.s1) / f) + sigmaz0;
}

/**
 * Determine Psi.
 * The formula:
 * Psi = -17 * (1 - e^(-0.29 * z / L))
 * @param roughness Roughness of the source.
 * @param moninObukhovLength The right length corresponding to the roughness.
 * @return The result.
 */
double calculatePsi (const double roughness, const double moninObukhovLength) {
  return PSI_CONSTANT_1 * (1.0 - exp(PSI_CONSTANT_2 * roughness / moninObukhovLength));
}

/**
 * Determine Cwind
 *
 *          ln(Zp/Z0)  - psi(Zp/L)  + psi(Z0/L)
 * Cwind = ---------------------------------
 *          ln(Z10/z0) - psi(Z10/L) + psi(Z0/L)
 * Where:
 * Zp : de hoogte van de pluim = 0.75 sigmaz
 * Z0 : ruwheid in meters
 * L  : Monin-Obukhov lengte
 *
 * @param z0 roughness at the source Z0
 * @param roughnessClass
 * @param sigmaz Vertical diffusion correction
 * @return Cwind
 */
double calculateWindCorrection (const double z0, const double4 roughnessClass, double sigmaz) {
  const double Zp = 0.75 * sigmaz;
  const double L = roughnessClass.s2;

  return (log(Zp / z0) - calculatePsi(Zp, L) + calculatePsi(z0, L)) / (log(Z10 / z0) - calculatePsi(Z10, L) + calculatePsi(z0, L));
}

double calculateMeteoCorrection (const double2 source, const double correctionSchiphol) {
  double correctionEindhoven = EINDHOVEN_FACTOR * correctionSchiphol;
  double asPoint = source.y - AS_TAN * source.x;

  double factorSchiphol = clamp((asPoint - AS_EINDHOVEN) / AS_LENGTH, 0.0, 1.0);

  return factorSchiphol * correctionSchiphol + (1.0 - factorSchiphol) * correctionEindhoven;
}

/**
 * Correctiefactor (C)
 * De correctiefactor C corrigeert voor een aantal effecten en wordt berekend met de volgende formule:
 *   C = Cwind * Cmeteo * Cetmaal
 * Where
 *   Cmeteo =
 *   Cetmaal = 1.15
 *
 * @param segmentPosition The coordinate of the segment's center in meters.
 * @param vertical diffusion correction
 * @param roughness
 * @param roughnessClass
 * @return correction factor
 */
double calculateCorrectionFactor (const double2 segmentPosition, const double verticalDiffusion, const double roughness, const double4 roughnessClass) {
  return calculateWindCorrection(roughness, roughnessClass, verticalDiffusion) * calculateMeteoCorrection(segmentPosition, roughnessClass.s3)
      * DAY_CORRECTION_FACTOR;
}

/*
 * Returns the index for the windsector. This is a value between 0 (inclusive) and 'windSectorCount' (exclusive).
 */
int determineWindSectorIndex (const double2 directionVector) {
  // Angle would go counter clock wise, while the indices go clock wise.
  double angle = -atan2(directionVector.y, directionVector.x);

  // Convert angle to a range between -1 and 1.
  angle *= M_1_PI_F * 0.5;

  // Angle starts in the east, while the indices start in the north.
  angle += 0.25;

  // Wind sector 0 has 0 degrees in the middle of its range, so this offsets it by half a wind sector angle.
  angle += 0.5 / WIND_SECTORS;

  // Adjust angle range to 0 - 1
  if (angle < 0.0) {
    angle += 1.0;
  }

  // Multiply it with the number of wind sectors and convert to integer to generate the index.
  return convert_int_rtz(angle * convert_double(WIND_SECTORS));
}

/**
 *     E dw             1               | -(z-h)^2     |
 *  ---------------- * ---------- * exp |--------------| = concentration
 *  \/2pi sigmaz C u   pi Rb / n        |  2 sigmaz^2  |
 *
 * where:
 *
 * E  : de emissie per lengte-eenheid [ug * m-1 * s-1]
 * dw : de lengte van een wegsegment [m];
 * RB : de afstand van de bron (B) tot het rekenpunt (R), de rekenafstand [m];
 * sigmaz : de verticale verspreidingscoefficiënt [m];
 * z : de hoogte van het rekenpunt [m];
 * C : een ruwheidafhankelijke correctiefactor [-];
 * U : de windsnelheid [m/s];
 * n : het aantal windrichtingsectoren (36);
 * h : bronhoogte.
 *
 * This method calculates the conversion factor so
 *
 * concentration = E * dw * conversion_factor.
 *
 * @param RB distance
 * @param C concentration factor
 * @param sigmaz roughness dependent correction
 * @param zh z - h
 * @param U wind speed
 */
double calculateConversionFactor(const double RB, double C, const double sigmaz, const double zh, const double U) {
  return exp(-pown(zh, 2) / (2 * pown(sigmaz, 2))) / (sqrt(2 * M_PI_F) * sigmaz * C * U * M_PI_F * (RB / WIND_SECTORS));
}

/**
 * Calculates the factor that needs to be applied to the deposition velocity.
 *
 * @param factors The substance dependent factors.
 * @param distance The distance between the source segment and the receptor.
 */
double calculateDepositionVelocityFactor(const double2 factors, const double distance) {
  return factors.s0 * log(clamp(distance, DV_FACTOR_MIN_DIST, DV_FACTOR_MAX_DIST)) + factors.s1;
}

/**
 * Calculates the conversion factor for concentration contribution for a single source segment to a receptor.
 *
 * @param landUse The atmospheric roughness lookup map.
 * @param segmentPosition The center of the segment in meters.
 * @param sigma0 The start value for vertical dispersion for the segment.
 * @param distance the distance from the segment position to the receptor in meters.
 * @param windspeed The average windspeed for the windsector the segment lies in.
 * @param receptorHeight The height of the receptor (with respect to the segment).
 */
double calculateConversionFactorSegment(
    global const double* landUseMed,
    double2 segmentPosition,
    double sigma0,
    double distance,
    double windSpeed,
    double receptorHeight) {

  if (distance <= MIN_DISTANCE) {
    return 0.0;
  }

  const double roughness = lookupRoughnessSource(landUseMed, segmentPosition);

  const double4 roughnessClass = determineRoughnessClassification(roughness);

  const double verticalDiffusion = calculateVerticalDiffusion(distance, roughnessClass, sigma0);

  const double totalCorrectionFactor = calculateCorrectionFactor(segmentPosition, verticalDiffusion, roughness, roughnessClass);

  return calculateConversionFactor(distance, totalCorrectionFactor, verticalDiffusion, receptorHeight, windSpeed);
}

double invExp (double x, double2 fitValues) {
  return 1.0 - exp(-fitValues.s0 * pow(x, fitValues.s1));
}

/**
 * fitcoefficients for order4ln
 * fitindex 3:  f_a f_b f_c f_d f_e
 */
#ifdef VERSION_SRM2022
#define f_a  0.921826
#define f_b  0.082226
#define f_c -0.030057
#define f_d  0.004455
#define f_e -0.000243
#endif
#ifdef VERSION_SRM2023
#define f_a  0.920626
#define f_b  0.083366
#define f_c -0.030418
#define f_d  0.004498
#define f_e -0.000245
#endif

double order4ln (double x) {
  double lnx = log(x);
  return f_a + f_b * lnx + f_c * pown(lnx, 2) + f_d * pown(lnx, 3) + f_e * pown(lnx, 4);
}

/**
 * Determine if a position is within the hexagon at zoom level 1 of a receptor point.
 * First converts to a relative position with respect to the receptor point.
 * Next it determines if there is any chance it's inside the hexagon by looking at the bounding box. If not, returns false.
 * Finally it checks if the point is actually within the receptor (and not the leftover part which is still inside the bounding box).
 *
 * @param receptorPosition The position of the receptor.
 * @param position The position for which should be determined if it's inside the hexagon.
 */
bool isInsideHexagon(double2 receptorPosition, double2 position) {
  double2 relativePosition = fabs(receptorPosition - position);
  if (relativePosition.x > HEXAGON_RADIUS || relativePosition.y > HEXAGON_HALF_HEIGHT) return false;
  return HEXAGON_HALF_HEIGHT * HEXAGON_RADIUS - HEXAGON_HALF_HEIGHT * relativePosition.x - 0.5 * HEXAGON_RADIUS * relativePosition.y >= 0;
}

/**
 * Determine if a sub receptor calculation is needed for a specific segment/receptor combination.
 *
 * @param receptorPosition The position of the receptor.
 * @param segmentStartPosition The position of the start part of the segment.
 * @param segmentEndPosition The position of the end part of the segment.
 */
bool isSubReceptorCalculationNeeded(
    const double2 receptorPosition,
    const double2 segmentStartPosition,
    const double2 segmentEndPosition) {
  const double2 segmentPosition = segmentStartPosition + 0.5 * (segmentEndPosition - segmentStartPosition);
  return isInsideHexagon(receptorPosition, segmentPosition);
}

/**
 * fitcoefficients for FIT_NH3
 * fitindex  f_0 f_1
 */
constant double2 FIT_NH3[16] = {
#ifdef VERSION_SRM2022
    (double2)(29.548211, -0.511164),
    (double2)(29.379841, -0.457644),
    (double2)(29.792141, -0.439764),
    (double2)(30.092548, -0.423948),
    (double2)(22.931584, -0.493217),
    (double2)(23.274732, -0.443114),
    (double2)(23.848265, -0.426470),
    (double2)(24.368499, -0.411935),
    (double2)(17.151672, -0.470028),
    (double2)(17.809360, -0.424265),
    (double2)(18.483131, -0.409277),
    (double2)(19.146094, -0.396413),
    (double2)(6.730461,  -0.314995),
    (double2)(7.531239,  -0.286135),
    (double2)(8.258711,  -0.280785),
    (double2)(9.107263,  -0.278267),
#endif
#ifdef VERSION_SRM2023
    (double2)(29.548211, -0.511164),
    (double2)(29.379841, -0.457644),
    (double2)(29.792141, -0.439764),
    (double2)(30.092548, -0.423948),
    (double2)(22.931584, -0.493217),
    (double2)(23.274732, -0.443114),
    (double2)(23.848265, -0.426470),
    (double2)(24.368499, -0.411935),
    (double2)(17.151672, -0.470028),
    (double2)(17.809360, -0.424265),
    (double2)(18.483131, -0.409277),
    (double2)(19.146094, -0.396413),
    (double2)(6.730461,  -0.314995),
    (double2)(7.531239,  -0.286135),
    (double2)(8.258711,  -0.280785),
    (double2)(9.107263,  -0.278267),
#endif
};

int concentrationToIndex (double concentration) {
  if (concentration >= 8.0) {
    return 3;
  } else if (concentration >= 5.0) {
    return 2;
  } else if (concentration >= 3.0) {
    return 1;
  } else {
    return 0;
  }
}

/**
 * Calculate depletion factor for NH3.
 */
double calculateDepletionFactorNH3 (double distance, double z0, double backgroundConcentration) {
  int fitIndex = 4 * z0ToIndex(z0) + concentrationToIndex(backgroundConcentration);

  return clamp(invExp(distance, FIT_NH3[fitIndex]), 0.0, 1.0);
}

/**
 * Calculates the NH3 concentration for a combination of segment and receptor in micrograms/m^3.
 *
 * @param segmentPosition The coordinate of the mid position of the road segment.
 * @param windData The wind data to use.
 * @param segmentEmission The emission of the road segment in microgram/second.
 * @param distance The distance between segment and receptor.
 * @param sigma0
 * @param receptorPosition The coordinate of the receptor.
 * @param receptorHeight The height of the receptor.
 * @param backgroundConcentration The NH3 background concentration, used for depletion.
 */
double2 calculateConcentrationSegmentNH3 (
    global const double* landUseLowBuffer,
    global const double* landUseMedBuffer,
    global const double* landUseHighBuffer,
    const double2 segmentPosition,
    const double distance,
    const double2 windData,
    const double sigma0,
    const double segmentEmission,
    const double2 receptorPosition,
    const double receptorHeight,
    const double backgroundConcentration) {
  double segmentConcentration = windData.s0 * segmentEmission * calculateConversionFactorSegment(
    landUseMedBuffer,
    segmentPosition,
    sigma0,
    distance,
    windData.s1,
    receptorHeight);

  double z0 = lookupRoughnessReceptor(
    landUseLowBuffer,
    landUseMedBuffer,
    landUseHighBuffer,
    receptorPosition,
    distance);

  double depletionFactor = calculateDepletionFactorNH3(distance, z0, backgroundConcentration);
  double depositionVelocityFactor = calculateDepositionVelocityFactor(DV_FACTOR_NH3, distance);

  return (double2) (segmentConcentration, (segmentConcentration * depletionFactor * depositionVelocityFactor));
}

/**
 * Calculates the NH3 concentration for a combination of source and receptor in micrograms/m^3.
 *
 * @param sourceStartPosition The start coordinate of the road segment.
 * @param sourceEndPosition The end coordinate of the road segment.
 * @param sigma0 sigma 0 value
 * @param trafficEmission The emission of the road segment in microgram/meter/second.
 * @param receptorPosition The coordinate of the receptor.
 * @param receptorHeight The height of the receptor.
 * @param receptorDepositionVelocity The deposition velocity at the receptor.
 * @param backgroundConcentration The NH3 background concentration, used for depletion.
 */
double2 calculateContributionSourceNH3 (
    global const double* landUseLowBuffer,
    global const double* landUseMedBuffer,
    global const double* landUseHighBuffer,
    const double2* receptorWindBuffer,
    const double2 sourceStartPosition,
    const double2 sourceEndPosition,
    const double sigma0,
    const double trafficEmission,
    const double2 receptorPosition,
    const double receptorHeight,
    const double receptorDepositionVelocity,
    const double backgroundConcentration) {
  double2 sourceDirection = sourceEndPosition - sourceStartPosition;
  const double sourceLength = length(sourceDirection);

  // Quick test to determine if this source has any effect.
  if (distance(sourceStartPosition, receptorPosition) - sourceLength > MAX_SOURCE_DISTANCE) {
    return 0.0;
  }

  const int segments = convert_int_rtp(sourceLength / MAX_SEGMENT_LENGTH);
  const double segmentsf = convert_double(segments);

  sourceDirection /= segmentsf;

  // Emission in micrograms per second for a segment
  const double segmentEmission = trafficEmission * sourceLength / segmentsf;

  double2 concentration = (double2) (0.0, 0.0);

  for (int i = 0; i < segments; i++) {
    // The segment position is the center of a segment.
    const double2 segmentPosition = sourceStartPosition + (convert_double(i) + 0.5) * sourceDirection;

    const double2 segmentDirection = segmentPosition - receptorPosition;
    const double segmentDistance = MIN_SEGMENT_DISTANCE(segmentDirection);

    if (segmentDistance > 0.0 && segmentDistance <= MAX_SOURCE_DISTANCE) {
      const int windSectorIndex = determineWindSectorIndex(segmentDirection);
      const double2 windData = receptorWindBuffer[windSectorIndex];

      concentration += calculateConcentrationSegmentNH3(
          landUseLowBuffer,
          landUseMedBuffer,
          landUseHighBuffer,
          segmentPosition,
          segmentDistance,
          windData,
          sigma0,
          segmentEmission,
          receptorPosition,
          receptorHeight,
          backgroundConcentration);
    }
  }

  double deposition = concentration.s1 * receptorDepositionVelocity * DEPOSITION_CONVERSION_NH3;

  return (double2) (concentration.s0, deposition);
}

/**
 * Calculates the NH3 concentration for a combination of source and receptor in micrograms/m^3
 */
kernel void calculateContributionNH3(
    global const double* landUseLowBuffer,
    global const double* landUseMedBuffer,
    global const double* landUseHighBuffer,
    global const double8* sourceBuffer,
    const int sourceCount,
    global double2* workBuffer,
    global const double2* receptorPositionBuffer,
    global const double2* receptorWindBuffer,
    global const double* receptorHeightBuffer,
    const int receptorCount,
    global const double* receptorDepositionVelocityBuffer,
    global const double* receptorBackgroundBuffer
    ) {
  // get index into global data array
  const int iGID = get_global_id(0);

  const int receptorId = iGID / sourceCount;
  const int sourceId = iGID % sourceCount;

  // bound check, equivalent to the limit on a 'for' loop
  if (sourceId >= sourceCount || receptorId >= receptorCount) {
    return;
  }

  // Read source data
  const double8 currSource = sourceBuffer[sourceId];
  const double2 sourceStartPosition = currSource.s01;
  const double2 sourceEndPosition = currSource.s23;
  const double sigma0 = currSource.s4;
  const double emission = currSource.s6;

  // Read receptor data
  const double2 receptorPosition = receptorPositionBuffer[receptorId];
  const double receptorDepositionVelocity = receptorDepositionVelocityBuffer[receptorId];
  const double backgroundConcentration = receptorBackgroundBuffer[receptorId];
  const double receptorHeight = receptorHeightBuffer[receptorId];

  // Copy receptor meteo data to local array.
  double2 receptorWind[WIND_SECTORS];

  int offset = receptorId * WIND_SECTORS;

  for (int i = 0; i < WIND_SECTORS; i++) {
    receptorWind[i] = receptorWindBuffer[offset + i];
  }

  workBuffer[iGID] = calculateContributionSourceNH3(
      landUseLowBuffer,
      landUseMedBuffer,
      landUseHighBuffer,
      receptorWind,
      sourceStartPosition,
      sourceEndPosition,
      sigma0,
      emission,
      receptorPosition,
      receptorHeight,
      receptorDepositionVelocity,
      backgroundConcentration
  );
}

/**
 * Sums the NH3 concentration contributions of sources for the receptors.
 */
kernel void sumContributionsNH3 (
    const int sourceCount,
    global double2* resultBuffer,
    global const double2* workBuffer,
    const int receptorCount) {
  const int receptorId = get_global_id(0);

  if (receptorId >= receptorCount) {
    return;
  }

  const int start = receptorId * sourceCount;
  const int end = start + sourceCount;
  double2 receptorResult = 0.0;

  for (int i = start; i < end; i++) {
    receptorResult += workBuffer[i];
  }

  resultBuffer[receptorId] = receptorResult;
}

/**
 * fitcoefficients for FIT_NOX
 * fitindex  f_0 f_1
 */
constant double2 FIT_NOx[3] = {
#ifdef VERSION_SRM2022
    (double2)(28.477162, -0.337355),
    (double2)(24.444078, -0.322261),
    (double2)(22.242265, -0.314050),
#endif
#ifdef VERSION_SRM2023
    (double2)(28.446546, -0.337239),
    (double2)(24.410926, -0.322115),
    (double2)(22.211257, -0.313899),
#endif
};

double calculateDepletionFactorNOx (double distance, double z0) {
  int fitIndex = z0ToIndex(z0);

  if (fitIndex == 3) {
    return order4ln(distance);
  } else {
    return invExp(distance, FIT_NOx[fitIndex]);
  }
}

/**
 * Calculates the NO2 and NOx concentration for a combination of segment and receptor in micrograms/m^3.
 *
 * @param segmentPosition The coordinate of the mid position of the road segment.
 * @param distance The distance between segment and receptor.
 * @param windData The wind data to use.
 * @param segmentEmissions The NO2 and NOx emission of the road segment in microgram/second.
 * @param sigma0
 * @param receptorPosition The coordinate of the receptor.
 * @param receptorHeight The height of the receptor.
 */
double4 calculateConcentrationSegmentNOx2  (
    global const double* landUseLowBuffer,
    global const double* landUseMedBuffer,
    global const double* landUseHighBuffer,
    const double2 segmentPosition,
    const double distance,
    const double2 windData,
    const double sigma0,
    const double2 segmentEmissions,
    const double2 receptorPosition,
    const double receptorHeight
) {
  const double conversion = calculateConversionFactorSegment(
      landUseMedBuffer,
      segmentPosition,
      sigma0,
      distance,
      windData.s1,
      receptorHeight);

  // Direct concentrations NO2, NOx
  const double2 directConcentrations = segmentEmissions * conversion;

  double z0 = lookupRoughnessReceptor(
      landUseLowBuffer,
      landUseMedBuffer,
      landUseHighBuffer,
      receptorPosition,
      distance);

  double depletionFactor = calculateDepletionFactorNOx(distance, z0);
  double depositionVelocityFactor = calculateDepositionVelocityFactor(DV_FACTOR_NOX, distance);

  // Return concentrations along with depleted concentration NOx and a weighted deposition velocity factor.
  return (double4) (directConcentrations, directConcentrations.s1 * depletionFactor, directConcentrations.s1 * depositionVelocityFactor);
}

/**
 * Calculates the concentration for a combination of source and receptor.
 *
 * @param windData The wind data to use.
 * @param windSectorIndex The wind sector we're determining result for.
 * @param sourceStartPosition The start coordinate of the source.
 * @param sourceEndPosition The end coordinate of the source.
 * @param sigma0 The start value for vertical dispersion for the source.
 * @param trafficEmissions The NO2 and NOx emissions of the source in microgram/meter/second.
 * @param receptorPosition The coordinate of the receptor.
 * @param receptorHeight The height of the receptor.
 */
double4 calculateConcentrationContributionSourceNOx2 (
    global const double* landUseLowBuffer,
    global const double* landUseMedBuffer,
    global const double* landUseHighBuffer,
    const double2 windData,
    const int windSectorIndex,
    const double2 sourceStartPosition,
    const double2 sourceEndPosition,
    const double sigma0,
    const double2 trafficEmissions,
    const double2 receptorPosition,
    const double receptorHeight
) {
  double2 sourceDirection = sourceEndPosition - sourceStartPosition;
  const double sourceLength = length(sourceDirection);
  double4 concentrations = (double4) {0.0, 0.0, 0.0, 0.0};

  // Quick test to determine if this source has any effect.
  if (distance(sourceStartPosition, receptorPosition) - sourceLength > MAX_SOURCE_DISTANCE) {
    return concentrations;
  }

  const int segments = convert_int_rtp(sourceLength / MAX_SEGMENT_LENGTH);
  const double segmentsf = convert_double(segments);

  sourceDirection /= segmentsf;

  // Emission in micrograms per second for a segment
  const double2 segmentEmissions = trafficEmissions * (sourceLength / segmentsf);

  for (int i = 0; i < segments; i++) {
    // The segment position is the center of a segment.
    const double2 segmentPosition = sourceStartPosition + (convert_double(i) + 0.5) * sourceDirection;

    const double2 segmentDirection = segmentPosition - receptorPosition;
    const double segmentDistance = MIN_SEGMENT_DISTANCE(segmentDirection);

    if (segmentDistance > 0.0 && segmentDistance <= MAX_SOURCE_DISTANCE && windSectorIndex == determineWindSectorIndex(segmentDirection)) {
        concentrations += calculateConcentrationSegmentNOx2(
            landUseLowBuffer,
            landUseMedBuffer,
            landUseHighBuffer,
            segmentPosition,
            segmentDistance,
            windData,
            sigma0,
            segmentEmissions,
            receptorPosition,
            receptorHeight);
    }
  }

  return concentrations;
}

kernel void calculateContributionNOx2(
    global const double* landUseLowBuffer,
    global const double* landUseMedBuffer,
    global const double* landUseHighBuffer,
    global const double8* sourceBuffer,
    const int sourceCount,
    global double4* workBuffer,
    global const double2* receptorPositionBuffer,
    global const double2* receptorWindBuffer,
    global const double* receptorHeightBuffer,
    const int receptorCount,
    const int windSectorIndex
    ) {
  // get index into global data array
  const int iGID = get_global_id(0);

  const int receptorId = iGID / sourceCount;
  const int sourceId = iGID % sourceCount;

  // bound check, equivalent to the limit on a 'for' loop
  if (sourceId >= sourceCount || receptorId >= receptorCount) {
    return;
  }

  // Read source data
  const double8 currSource = sourceBuffer[sourceId];
  const double2 sourceStartPosition = currSource.s01;
  const double2 sourceEndPosition = currSource.s23;
  const double sigma0 = currSource.s4;
  const double2 emissions = currSource.s67;

  // Read receptor data
  const double2 receptorPosition = receptorPositionBuffer[receptorId];
  const double receptorHeight = receptorHeightBuffer[receptorId];

  int offset = receptorId * WIND_SECTORS;
  const double2 receptorWind = receptorWindBuffer[offset + windSectorIndex];

  workBuffer[iGID] = calculateConcentrationContributionSourceNOx2 (
      landUseLowBuffer,
      landUseMedBuffer,
      landUseHighBuffer,
      receptorWind,
      windSectorIndex,
      sourceStartPosition,
      sourceEndPosition,
      sigma0,
      emissions,
      receptorPosition,
      receptorHeight);
}

/**
 * Sums the NOX and NO2 concentration contributions of sources for the receptors.
 */
kernel void sumContributionsNOx2 (
    const int sourceCount,
    global double4* resultBuffer,
    global const double4* workBuffer,
    const int receptorCount,
    global const double2* receptorPositionBuffer,
    global const double2* receptorWindBuffer,
    global const double* receptorO3Buffer,
    global const double* receptorDepositionVelocityBuffer,
    const int windSectorIndex) {
  const int receptorId = get_global_id(0);

  if (receptorId >= receptorCount) {
    return;
  }

  const int start = receptorId * sourceCount;
  const int end = start + sourceCount;

  // Direct NO2, direct NOx, depleted NOx
  double4 concentration = (double4) {0.0, 0.0, 0.0, 0.0};

  for (int i = start; i < end; i++) {
    concentration += workBuffer[i];
  }

  int index = receptorId * WIND_SECTORS + windSectorIndex;
  const double2 receptorWind = receptorWindBuffer[index];
  const double receptorO3 = receptorO3Buffer[index];

  const double directConcentrationNO2 = concentration.s0;
  const double directConcentrationNOx = concentration.s1;

  if (directConcentrationNOx > 0.0) {
    const double receptorDepostionVelocity = receptorDepositionVelocityBuffer[receptorId];

    const double fractionNO2 = directConcentrationNO2 / directConcentrationNOx;
    const double fractionNO = 1.0 - fractionNO2;

    // s12 = s1:direct concentration NOx, s2:depleted concentration NOx
    const double2 concentrationNOx = concentration.s12;
    const double2 concentrationNO = concentrationNOx * fractionNO;
    // concentrationNO2 = s0: direct concentration no2, s1: depleted concentration no2
    const double2 concentrationNO2 = fractionNO2 * concentrationNOx + ((receptorO3 * concentrationNO) / (concentrationNO + CONSTANT_K));

    // Divide the deposition velocity factor by the direct concentration NOx to get a weighted factor for the source/receptor combination.
    const double depositionVelocityFactor = concentration.s3 / directConcentrationNOx;
    // depleted NO2 * depostion velocity * factor NOx depleted/direct * constant
    const double deposition = concentrationNO2.s1 * receptorDepostionVelocity * depositionVelocityFactor * DEPOSITION_CONVERSION_NO2;

    // Store concentration NOx, concentration NO2, deposition NOx
    double4 result = (double4) {directConcentrationNOx, directConcentrationNO2, concentrationNO2.s0, deposition};

    resultBuffer[receptorId] += receptorWind.s0 * result;
  } else if (isnan(directConcentrationNOx)) {
    resultBuffer[receptorId] = directConcentrationNOx;
  }
}

// Kernels to test functions
#ifdef TEST_ENABLED
kernel void testDetermineRoughnessClassification (const int testCount, global const double* roughness, global double4* results) {
  const int testId = get_global_id(0);

  if (testId >= testCount) {
    return;
  }

  results[testId] = determineRoughnessClassification(roughness[testId]);
}

kernel void testDetermineMeteoCorrection (const int testCount, global const double2* point, global const double* correctionSchiphol, global double* results) {
  const int testId = get_global_id(0);

  if (testId >= testCount) {
    return;
  }

  results[testId] = calculateMeteoCorrection(point[testId], correctionSchiphol[testId]);
}

kernel void testDetermineWindSectorIndex (const int testCount, global const double2* directions, global int* results) {
  const int testId = get_global_id(0);

  if (testId >= testCount) {
    return;
  }

  results[testId] = determineWindSectorIndex(directions[testId]);
}

kernel void testLookupRoughnessSource (
    const int testCount,
    global const double* landUseMed,
    global const double2* sourcePoints,
    global double* results) {

  const int testId = get_global_id(0);

  if (testId >= testCount) {
    return;
  }

  results[testId] = lookupRoughnessSource(landUseMed, sourcePoints[testId]);
}

kernel void testCalculateVerticalDiffusion(const int testCount, global const double* distance, global const double4* roughnessClass, global const double* sigmaz0, global double* results) {
  const int testId = get_global_id(0);

  if (testId < testCount) {
    results[testId] = calculateVerticalDiffusion(distance[testId], roughnessClass[testId], sigmaz0[testId]);
  }
}

kernel void testCalculateDepletionFactorNOx(const int testCount, global const double* distance, global const double* z0, global double* results) {
  const int testId = get_global_id(0);

  if (testId < testCount) {
    results[testId] = calculateDepletionFactorNOx(distance[testId], z0[testId]);
  }
}

kernel void testCalculateDepletionFactorNH3(const int testCount, global const double* distance, global const double* z0, global const double* backgroundConcentration, global double* results) {
  const int testId = get_global_id(0);

  if (testId < testCount) {
    results[testId] = calculateDepletionFactorNH3(distance[testId], z0[testId], backgroundConcentration[testId]);
  }
}

kernel void testSubReceptorCalculationNeeded(const int testCount,
    global const double2* receptorPoint,
    global const double2* segmentStart,
    global const double2* segmentEnd,
    global short* results) {
  const int testId = get_global_id(0);

  if (testId < testCount) {
    results[testId] = isSubReceptorCalculationNeeded(receptorPoint[testId], segmentStart[testId], segmentEnd[testId]);
  }
}
#endif
