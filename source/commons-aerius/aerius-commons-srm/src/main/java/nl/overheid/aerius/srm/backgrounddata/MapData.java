/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.nio.DoubleBuffer;

import nl.overheid.aerius.opencl.DataGrid;

public class MapData extends DataGrid {
  private static final double NO_DATA_VALUE = -1.0;
  private final double diameter;
  private final double offsetX;
  private final double offsetY;

  /**
   * Creates a new MapData instance.
   *
   * @param width The number of cells in the x direction.
   * @param height The number of cells in the y direction.
   * @param diameter The size of a single cell in map units.
   * @param offsetX The offset in map units of the data in the x direction.
   * @param offsetY The offset in map units of the data in the y direction.
   * @param data A direct buffer containing the data in row major order.
   */
  public MapData(final int width, final int height, final double offsetX, final double offsetY, final double diameter, final DoubleBuffer data) {
    super(width, height, data);

    this.offsetX = offsetX;
    this.offsetY = offsetY;

    if (diameter <= 0.0) {
      throw new IllegalArgumentException("Diameter should be larger than 0.");
    }

    this.diameter = diameter;
  }

  /**
   * @return The size of a single cell in map units.
   */
  public double getDiameter() {
    return diameter;
  }

  /**
   * @return The offset in map units of the data in the x direction.
   */
  public double getOffsetX() {
    return offsetX;
  }

  /**
   * @return The offset in map units of the data in the y direction.
   */
  public double getOffsetY() {
    return offsetY;
  }

  public double getNoDataValue() {
    return NO_DATA_VALUE;
  }

  /**
   * @param value The value to check
   * @return The value for points not contained within the data grid.
   */
  public boolean isValidValue(final double value) {
    return value > NO_DATA_VALUE;
  }

  /**
   * Returns the value at the given coordinate.
   *
   * @param x The x coordinate.
   * @param y The y coordinate.
   * @return The value at the coordinate, or the default value if outside the boundaries.
   */
  public double getValue(final double x, final double y) {
    final int indexX = (int) ((x - offsetX) / diameter);
    final int indexY = (int) ((y - offsetY) / diameter);

    if (indexX < 0 || indexX >= getWidth() || indexY < 0 || indexY >= getHeight()) {
      return NO_DATA_VALUE;
    } else {
      return getData().get(indexY * getWidth() + indexX);
    }
  }

  @Override
  public String toString() {
    return "MapData [diameter=" + diameter + ", offsetX=" + offsetX + ", offsetY=" + offsetY + "]";
  }
}
