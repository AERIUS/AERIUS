/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import java.util.Collection;
import java.util.List;

import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Class to implement to convert {@link EmissionSource} to SRM RoadSegment objects.
 */
public class SRMSourceConverter {

  private final EmissionSourceSRMConverter converter;

  public SRMSourceConverter() {
    this(null);
  }

  public SRMSourceConverter(final AbstractSRMConverter<? extends EngineSource> firstConverter) {
    this.converter = new EmissionSourceSRMConverter(firstConverter);
  }

  public boolean convert(final List<EngineSource> expanded, final List<EmissionSourceFeature> emissionSources, final List<Substance> substances)
      throws AeriusException {
    return expanded.addAll(converter.convert(emissionSources, substances));
  }

  public boolean convert(final Collection<EngineSource> expanded, final EmissionSource source, final Geometry geometry,
      final List<Substance> substances) throws AeriusException {
    return expanded.addAll(converter.convert(geometry, source, substances));
  }

}
