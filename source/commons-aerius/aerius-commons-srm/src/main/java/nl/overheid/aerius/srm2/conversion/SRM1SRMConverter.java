/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.domain.SRM1RoadSegment;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Converts {@link SRM1RoadEmissionSource} objects to {@link SRM1RoadSegment} objects.
 */
public class SRM1SRMConverter extends AbstractSRMConverter<SRM1RoadSegment> {

  @Override
  boolean canConvert(final EmissionSource source) {
    return source instanceof SRM1RoadEmissionSource;
  }

  @Override
  protected List<SRM1RoadSegment> convert(final LineString geometry, final EmissionSource source, final List<Substance> substances)
      throws AeriusException {
    final String wktGeometry = GeometryUtil.getGeometry(geometry).toText();
    final SRM1RoadSegment roadSegment = new SRM1RoadSegment(((SRM1RoadEmissionSource) source).getGmlId(), wktGeometry);

    copyEmissions(getEmissionPerMeter(geometry, source, substances), roadSegment, Optional.empty());
    return Collections.singletonList(roadSegment);
  }

}
