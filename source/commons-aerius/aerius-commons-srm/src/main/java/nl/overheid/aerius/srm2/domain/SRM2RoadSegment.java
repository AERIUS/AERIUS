/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.domain;

import nl.overheid.aerius.shared.domain.EngineEmissionSource;

/**
 * SRM2 Road data object containing all relevant data for a specific road segment to perform a srm2 calculation.
 *
 * Emissions are in g/m/s.
 */
public class SRM2RoadSegment extends EngineEmissionSource {

  private static final long serialVersionUID = 1L;

  private String segmentId;
  private double startX;
  private double startY;
  private double endX;
  private double endY;
  private int elevationHeight;
  private double sigma0;

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (segmentId == null ? 0 : segmentId.hashCode());
    result = prime * result + elevationHeight;
    long temp;
    temp = Double.doubleToLongBits(endX);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(endY);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(sigma0);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(startX);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(startY);
    return prime * result + (int) (temp ^ (temp >>> 32));
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final SRM2RoadSegment other = (SRM2RoadSegment) obj;
    return elevationHeight == other.elevationHeight
        && segmentId == other.segmentId
        && Double.doubleToLongBits(endX) == Double.doubleToLongBits(other.endX)
        && Double.doubleToLongBits(endY) == Double.doubleToLongBits(other.endY)
        && Double.doubleToLongBits(sigma0) == Double.doubleToLongBits(other.sigma0)
        && Double.doubleToLongBits(startX) == Double.doubleToLongBits(other.startX)
        && Double.doubleToLongBits(startY) == Double.doubleToLongBits(other.startY)
        && super.equals(obj);
  }

  public String getSegmentId() {
    return segmentId;
  }

  public void setSegmentId(final String segmentId) {
    this.segmentId = segmentId;
  }

  public double getLineLength() {
    return Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
  }

  public double getStartX() {
    return startX;
  }

  public double getStartY() {
    return startY;
  }

  public double getEndX() {
    return endX;
  }

  public double getEndY() {
    return endY;
  }

  public int getElevationHeight() {
    return elevationHeight;
  }

  public double getSigma0() {
    return sigma0;
  }

  public void setStartX(final double startX) {
    this.startX = startX;
  }

  public void setStartY(final double startY) {
    this.startY = startY;
  }

  public void setEndX(final double endX) {
    this.endX = endX;
  }

  public void setEndY(final double endY) {
    this.endY = endY;
  }

  public void setElevationHeight(final int elevationHeight) {
    this.elevationHeight = elevationHeight;
  }

  public void setSigma0(final double sigma0) {
    this.sigma0 = sigma0;
  }

  @Override
  public String toString() {
    return "SRM2RoadSegment [segmentId=" + segmentId + ", startX=" + startX + ", startY=" + startY + ", endX=" + endX + ", endY="
        + endY + ", elevationHeight=" + elevationHeight + ", sigma0=" + sigma0 + "]";
  }
}
