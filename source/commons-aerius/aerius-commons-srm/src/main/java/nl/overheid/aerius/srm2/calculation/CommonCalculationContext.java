/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import java.nio.DoubleBuffer;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLKernel;

import nl.overheid.aerius.opencl.Connection;
import nl.overheid.aerius.opencl.MemoryUsage;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.LandUseData.LandUseType;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

/**
 * Calculation Context for common data. I.E. Land Use, ORO, Deposition Velocity and Sources.
 */
class CommonCalculationContext {

  private static final Logger LOG = LoggerFactory.getLogger(CommonCalculationContext.class);

  private static final int CALCULATION_LANDUSE_LOW_ARG_IDX = 0;
  private static final int CALCULATION_LANDUSE_MED_ARG_IDX = 1;
  private static final int CALCULATION_LANDUSE_HIGH_ARG_IDX = 2;
  private static final int CALCULATION_SOURCES_ARG_IDX = 3;
  private static final int CALCULATION_SOURCES_SIZE_ARG_IDX = 4;

  private static final int SUM_SOURCES_SIZE_ARG_IDX = 0;

  private CLBuffer<DoubleBuffer> landUseLowBuffer;
  private CLBuffer<DoubleBuffer> landUseMedBuffer;
  private CLBuffer<DoubleBuffer> landUseHighBuffer;
  private CLBuffer<DoubleBuffer> landUseOroBuffer;
  private CLBuffer<DoubleBuffer> sourceBuffer;
  private final SourceData sourceData;
  private final Collection<SRM2RoadSegment> sources;

  public CommonCalculationContext(final SourceData sourceData, final Collection<SRM2RoadSegment> sources) {
    this.sourceData = sourceData;
    this.sources = sources;
  }

  public long getBufferSize() {
    return ((long) landUseLowBuffer.getNIOSize()) + landUseMedBuffer.getNIOSize() + landUseHighBuffer.getNIOSize() + sourceBuffer.getNIOSize()
        + (landUseOroBuffer == null ? 0L : landUseOroBuffer.getNIOSize());
  }

  public int getNumberOfSources() {
    return sources.size();
  }

  public void initBuffers(final AeriusSRMVersion aeriusSRMVersion, final Connection con, final CLKernel calculationKernel,
      final Substance substance) {
    final PreSRMData preSRMData = sourceData.getPreSRMData();
    landUseLowBuffer = con.createAndUploadBuffer(preSRMData.getLandUse(aeriusSRMVersion, LandUseType.LOW).getData(), MemoryUsage.READ_ONLY);
    landUseMedBuffer = con.createAndUploadBuffer(sourceData.getPreSRMData().getLandUse(aeriusSRMVersion, LandUseType.MED).getData(),
        MemoryUsage.READ_ONLY);
    landUseHighBuffer = con.createAndUploadBuffer(preSRMData.getLandUse(aeriusSRMVersion, LandUseType.HIGH).getData(), MemoryUsage.READ_ONLY);

    calculationKernel.setArg(CALCULATION_LANDUSE_LOW_ARG_IDX, landUseLowBuffer);
    calculationKernel.setArg(CALCULATION_LANDUSE_MED_ARG_IDX, landUseMedBuffer);
    calculationKernel.setArg(CALCULATION_LANDUSE_HIGH_ARG_IDX, landUseHighBuffer);

    landUseOroBuffer = null;
  }

  public void initSourceBuffers(final Connection con, final CLKernel calculationKernel, final CLKernel sumKernel, final Substance substance) {
    final int sourceCount = sources.size();
    sourceBuffer = con.createDoubleCLBuffer(sourceCount * SRMConstants.SOURCE_DOUBLES, MemoryUsage.READ_ONLY);

    LOG.debug("Filling buffers for {} sources.", sourceCount);

    for (final SRM2RoadSegment source : sources) {
      sourceBuffer.getBuffer().put(source.getStartX());
      sourceBuffer.getBuffer().put(source.getStartY());

      sourceBuffer.getBuffer().put(source.getEndX());
      sourceBuffer.getBuffer().put(source.getEndY());

      sourceBuffer.getBuffer().put(source.getSigma0());
      sourceBuffer.getBuffer().put(0.0);
      sourceBuffer.getBuffer().put(source.getEmission(substance) * SRMConstants.EMISSION_CONVERSION);

      // NO2 calculations also require NOx.
      if (substance == Substance.NO2) {
        sourceBuffer.getBuffer().put(source.getEmission(Substance.NOX) * SRMConstants.EMISSION_CONVERSION);
      } else {
        sourceBuffer.getBuffer().put(0D);
      }
    }
    con.rewindAndUploadBuffer(sourceBuffer);

    calculationKernel.setArg(CALCULATION_SOURCES_ARG_IDX, sourceBuffer);
    calculationKernel.setArg(CALCULATION_SOURCES_SIZE_ARG_IDX, sources.size());

    sumKernel.setArg(SUM_SOURCES_SIZE_ARG_IDX, sources.size());
  }
}
