/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.Objects;

import nl.overheid.aerius.srm.SRMConstants;

/**
 * Data class for windrose wind factor and windspeed information at a x-y coordinate.
 */
public class WindRoseSpeed extends GridPoint implements WindRose {

  private final double[] windFactors;
  private final double[] windSpeeds;

  public WindRoseSpeed(final int x, final int y) {
    this(x, y, new double[SRMConstants.WIND_SECTORS], new double[SRMConstants.WIND_SECTORS]);
  }

  WindRoseSpeed(final int x, final int y, final double[] windFactors, final double[] windSpeeds) {
    super(x, y);
    this.windFactors = windFactors;
    this.windSpeeds = windSpeeds;
  }

  @Override
  public boolean equals(final Object o) {
    return super.equals(o) && Objects.equals(windFactors, ((WindRoseSpeed) o).getWindFactors())
        && Objects.equals(windSpeeds, ((WindRoseSpeed) o).getAvgWindSpeeds());
  }

  @Override
  public int hashCode() {
    return Objects.hash(windFactors, windSpeeds) + 31 * super.hashCode();
  }

  public double[] getWindFactors() {
    return windFactors;
  }

  public double getWindFactor(final int sector) {
    if (sector < 0 || sector >= SRMConstants.WIND_SECTORS) {
      throw new IndexOutOfBoundsException("Invalid wind sector for wind factor, sector:" + sector);
    }

    return windFactors[sector];
  }

  public void setWindFactor(final int sector, final double windFactor) {
    if (sector < 0 || sector >= SRMConstants.WIND_SECTORS) {
      throw new IndexOutOfBoundsException("Invalid wind sector for wind factor, sector:" + sector);
    }
    windFactors[sector] = windFactor;
  }

  public double[] getAvgWindSpeeds() {
    return windSpeeds;
  }

  public double getAvgWindSpeed(final int sector) {
    if (sector < 0 || sector >= SRMConstants.WIND_SECTORS) {
      throw new IndexOutOfBoundsException("Invalid wind sector for wind speed, sector:" + sector);
    }
    return windSpeeds[sector];
  }

  public void setAvgWindSpeed(final int sector, final double windSpeed) {
    if (sector < 0 || sector >= SRMConstants.WIND_SECTORS) {
      throw new IndexOutOfBoundsException("Invalid wind sector for wind speed, sector:" + sector);
    }

    windSpeeds[sector] = windSpeed;
  }
}
