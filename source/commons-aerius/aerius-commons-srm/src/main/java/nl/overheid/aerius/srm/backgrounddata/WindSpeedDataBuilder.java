/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.util.FileUtil;

/**
 * Builder class to read total wind speed values for multiple years and prognose years.
 */
class WindSpeedDataBuilder {
  private static final Logger LOG = LoggerFactory.getLogger(WindSpeedDataBuilder.class);

  private static final Pattern WIND_FILE_PATTERN = Pattern.compile("wv_(\\d+)\\.grd");

  /**
   * Loads the wind speed prognose file.
   *
   * @param data data object to store wind speed data
   * @param directory location on the file system to find the wind speed data files
   * @throws FileNotFoundException
   * @throws IOException
   */
  public void loadWindSpeedPrognose(final AeriusSRMVersion aeriusSRMVersion, final PreSRMData data, final File directory)
      throws FileNotFoundException, IOException {
    LOG.info("Loading windspeed prognose data {}", SRMConstants.SRM1_WIND_FILE_PROGNOSE_NAME);
    final File file = new File(directory, SRMConstants.SRM1_WIND_FILE_PROGNOSE_NAME);

    if (!file.exists()) {
      throw new IOException("Couldn't find wind speed prognose file " + file);
    }
    data.setWindSpeedPrognoseGrid(aeriusSRMVersion, readPrognose(file));
  }

  private WindSpeedGrid readPrognose(final File file) throws IOException {
    try (final InputStream is = new FileInputStream(file); final InputStreamReader irs = new InputStreamReader(is, StandardCharsets.UTF_8);
        final BufferedReader br = new BufferedReader(irs)) {
      final WindSpeedByYearReader reader = new WindSpeedByYearReader();
      final List<WindSpeed> data = reader.readPrognose(br);
      final WindSpeedGrid table =
          new WindSpeedGrid(reader.getColumns(), reader.getRows(), reader.getDiameter(), reader.getOffsetX(), reader.getOffsetY());

      table.addAll(data);
      return table;
    }
  }

  /**
   * Loads wind speed files into the presrm data object.
   *
   * @param data data object to store wind speed data
   * @param directory location on the file system to find the wind speed data files
   * @throws FileNotFoundException
   * @throws IOException
   */
  void loadWindSpeedByYear(final AeriusSRMVersion aeriusSRMVersion, final PreSRMData data, final File directory)
      throws FileNotFoundException, IOException {
    LOG.info("Loading windspeed year data {}", WIND_FILE_PATTERN.pattern());
    final List<WindSpeedFileName> files = findFiles(directory, WIND_FILE_PATTERN);

    for (final WindSpeedFileName roseFile : files) {
      LOG.info("Loading windspeed year {} data {}", roseFile.year, roseFile.file);
      data.addWindSpeedGrid(aeriusSRMVersion, roseFile.year, readYear(roseFile));
    }
  }

  private WindSpeedGrid readYear(final WindSpeedFileName file) throws IOException {
    try (final InputStream is = new FileInputStream(file.file); final InputStreamReader irs = new InputStreamReader(is, StandardCharsets.UTF_8);
        final BufferedReader br = new BufferedReader(irs)) {
      final WindSpeedByYearReader reader = new WindSpeedByYearReader();
      final List<WindSpeed> data = reader.readYear(br, file.year);
      final WindSpeedGrid table =
          new WindSpeedGrid(reader.getColumns(), reader.getRows(), reader.getDiameter(), reader.getOffsetX(), reader.getOffsetY());

      table.addAll(data);
      return table;
    }
  }

  private List<WindSpeedFileName> findFiles(final File directory, final Pattern pattern) throws FileNotFoundException {
    return FileUtil.getFilteredFiles(directory, pattern, (dir, name, matcher) -> {
      final WindSpeedFileName wrfn = new WindSpeedFileName();

      wrfn.file = new File(dir, name);
      wrfn.year = Integer.parseInt(matcher.group(1));
      return wrfn;
    });
  }

  private static class WindSpeedFileName {
    File file;
    int year;
  }
}
