/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.backgrounddata.PreSRMDataBuilder;
import nl.overheid.aerius.srm.domain.SRMConfiguration;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerTimeLogger;

/**
 * WorkHandler for SRM calculations that directs the calculation to the profile specific implementation.
 */
class SRMWorkerHandler implements Worker<SRMInputData<?>, CalculationResult> {
  private static final Logger LOGGER = LoggerFactory.getLogger(SRMWorkerHandler.class);

  private final CIMLKWorkerHandler cimlkKWorkerHandler;
  private final OwN2000WorkHandler own2000WorkHandler;

  private final SRMDataUtilPreloader srmDataUtilPreloader;

  private final SRMConfiguration config;

  SRMWorkerHandler(final SRMConfiguration config, final SRMDataUtilPreloader srmDataUtilPreloader) throws IOException {
    this.config = config;
    final PreSRMDataBuilder builder = new PreSRMDataBuilder();

    if (config.hasThemeConfig(Theme.OWN2000)) {
      own2000WorkHandler = new OwN2000WorkHandler(config, builder.build(config));
    } else {
      own2000WorkHandler = null;
    }
    if (config.hasThemeConfig(Theme.RBL)) {
      cimlkKWorkerHandler = new CIMLKWorkerHandler(config, builder.build(config));
    } else {
      cimlkKWorkerHandler = null;
    }
    this.srmDataUtilPreloader = srmDataUtilPreloader;
  }

  @Override
  public CalculationResult run(final SRMInputData input, final JobIdentifier jobIdentifier,
      final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
    final WorkerTimeLogger timeLogger = WorkerTimeLogger.start(WorkerType.ASRM.name());

    try {
      final Theme profile = input.getTheme();

      srmDataUtilPreloader.ensureModelDataAvailable(config, input);
      if (profile == Theme.RBL && cimlkKWorkerHandler != null) {
        PreSRMDataBuilder.loadAdditionalData(cimlkKWorkerHandler.getData(), config, (AeriusSRMVersion) input.getEngineVersion());
        return cimlkKWorkerHandler.run(input, jobIdentifier, workerIntermediateResultSender);
      } else if (profile == Theme.OWN2000 && own2000WorkHandler != null) {
        PreSRMDataBuilder.loadAdditionalData(own2000WorkHandler.getData(), config, (AeriusSRMVersion) input.getEngineVersion());
        return own2000WorkHandler.run(input, jobIdentifier, workerIntermediateResultSender);
      } else {
        LOGGER.error("Unsupported or unconfigured profile passed to srm2 worker: {}", profile);
        throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
      }
    } finally {
      timeLogger.logDuration(jobIdentifier, input);
    }
  }
}
