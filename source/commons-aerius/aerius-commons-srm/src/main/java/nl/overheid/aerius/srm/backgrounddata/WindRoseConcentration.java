/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.Objects;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Data class for windrose concentration O3 and background NH3 information at a x-y coordinate.
 */
public class WindRoseConcentration extends GridPoint implements WindRose {

  private final double[] o3;
  // GCN background concentrations
  private final double[] gcn;
  // Correction Hoofdwegennet (hwn) factor
  private final double[] cHwn;

  public WindRoseConcentration(final int x, final int y) {
    super(x, y);
    o3 = new double[SRMConstants.WIND_SECTORS];
    gcn = new double[6];
    cHwn = new double[6];
  }

  private WindRoseConcentration(final int x, final int y, final double[] o3, final double[] gcn, final double[] cHwn) {
    super(x, y);
    this.o3 = o3;
    this.gcn = gcn;
    this.cHwn = cHwn;
  }

  @Override
  public boolean equals(final Object o) {
    return super.equals(o) && Objects.equals(o3, ((WindRoseConcentration) o).o3)
        && Objects.equals(gcn, ((WindRoseConcentration) o).gcn)
        && Objects.equals(cHwn, ((WindRoseConcentration) o).cHwn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(o3, gcn, cHwn) + 31 * super.hashCode();
  }

  public double getGCN(final Substance substance) {
    return gcn[index(substance)];
  }

  public void setGCN(final Substance substance, final double value) {
    gcn[index(substance)] = value;
  }

  public double getCHwn(final Substance substance) {
    return cHwn[index(substance)];
  }

  public void setCHwn(final Substance substance, final double value) {
    cHwn[index(substance)] = value;
  }

  public double getBackgroundO3() {
    return getBackground(Substance.O3);
  }

  public double getBackgroundNH3() {
    return getBackground(Substance.NH3);
  }

  public double getBackground(final Substance substance) {
    return getGCN(substance) + getCHwn(substance);
  }

  private int index(final Substance substance) {
    final int idx;

    switch (substance) {
    case O3:
      idx = 0;
      break;
    case NH3:
      idx = 1;
      break;
    case NO2:
      idx = 2;
      break;
    case PM10:
      idx = 3;
      break;
    case PM25:
      idx = 4;
      break;
    case EC:
      idx = 5;
      break;
    default:
      throw new IllegalArgumentException("No background concentrations for Substance:" + substance);
    }
    return idx;
  }

  public double getO3(final int sector) {
    if (sector < 0 || sector >= SRMConstants.WIND_SECTORS) {
      throw new IndexOutOfBoundsException("Invalid wind sector for O3, sector:" + sector);
    }
    return o3[sector];
  }

  public void setO3(final int sector, final double o3Factor) {
    if (sector < 0 || sector >= SRMConstants.WIND_SECTORS) {
      throw new IndexOutOfBoundsException("Invalid wind sector for O3, sector:" + sector);
    }

    o3[sector] = o3Factor;
  }

  public WindRoseConcentration copy() {
    return copy(getX(), getY());
  }

  /**
   * Creates a clone of this object with the new x and y-coordinates.
   *
   * @param x x-coordinate
   * @param y y-coordinate
   * @return copy of this object
   */
  public WindRoseConcentration copy(final int x, final int y) {
    return new WindRoseConcentration(x, y, o3.clone(), gcn.clone(), cHwn.clone());
  }
}
