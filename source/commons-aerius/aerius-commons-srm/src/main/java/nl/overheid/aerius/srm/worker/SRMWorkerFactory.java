/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.domain.SRMConfiguration;
import nl.overheid.aerius.srm.domain.SRMConfigurationBuilder;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.WorkerFactory;

/**
 * Worker Factory for SRM Calculations. Depending on the {@link Theme} passed as input the specific profile calculator is run.
 */
public class SRMWorkerFactory implements WorkerFactory<SRMConfiguration> {

  private static final Map<Theme, SRM2CalculationOptions> OPTIONS = List
      .of(SRMConstants.CIMLK_SRM2_CALCULATION_OPTIONS, SRMConstants.OWN2000_SRM2_CALCULATION_OPTIONS_NO_SUBRECEPTORS).stream().collect(
          Collectors.toMap(SRM2CalculationOptions::getTheme, Function.identity()));

  private final SRMDataUtilPreloader srmDataUtilPreloader;

  public SRMWorkerFactory() {
    this(new HttpClientProxy(new HttpClientManager()));
  }

  public SRMWorkerFactory(final HttpClientProxy httpClientProxy) {
    srmDataUtilPreloader = new SRMDataUtilPreloader(httpClientProxy);
  }

  @Override
  public ConfigurationBuilder<SRMConfiguration> configurationBuilder(final Properties properties) {
    return new SRMConfigurationBuilder(properties, srmDataUtilPreloader, OPTIONS);
  }

  @Override
  public <S extends Serializable, T extends Serializable> Worker<S, T> createWorkerHandler(final SRMConfiguration config,
      final WorkerConnectionHelper workerConnectionHelper) throws Exception {
    return (Worker<S, T>) new SRMWorkerHandler(config, srmDataUtilPreloader);
  }

  @Override
  public WorkerType getWorkerType() {
    return WorkerType.ASRM;
  }
}
