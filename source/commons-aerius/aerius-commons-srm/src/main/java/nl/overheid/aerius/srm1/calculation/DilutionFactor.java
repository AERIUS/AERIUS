/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm1.calculation;

import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRoadProfile;

/**
 * Calculates the 'verdunningsfactor' for different road types.
 */
final class DilutionFactor {

  // Unnamed constant part of the dilution factor algorithm
  private static final double BOEING_CONSTANT = -0.747;

  private enum RoadType {
    //@formatter:off
    TYPE_1(3.25E-4, -2.05E-2, 0.39, 0.856),
    TYPE_2(4.88E-4, -3.08E-2, 0.59, 0.0),
    TYPE_3(5.00E-4, -3.16E-2, 0.57, 0.0),
    TYPE_4(3.1E-4,  -1.82E-2, 0.33, 0.799);
    //@formatter:on

    private final double a;
    private final double b;
    private final double c;
    private final double alpha;

    RoadType(final double a, final double b, final double c, final double alpha) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.alpha = alpha;
    }

    public static RoadType from(final CIMLKRoadProfile roadProfile) {
      RoadType returnType = RoadType.TYPE_4;
      if (roadProfile != null) {
        switch (roadProfile) {
        case WIDE_STREET_CANYON:
          returnType = RoadType.TYPE_1;
          break;
        case NARROW_STREET_CANYON:
          returnType = RoadType.TYPE_2;
          break;
        case ONE_SIDE_BUILDINGS:
          returnType = RoadType.TYPE_3;
          break;
        case OTHER:
        default:
          returnType = RoadType.TYPE_4;
        }
      }
      return returnType;
    }
  }

  /**
   * Minimal distance as mentioned in "Technische beschrijving van standaardrekenmethode 1 (SRM-1)".
   *
   * @See <a href="https://www.rivm.nl/bibliotheek/rapporten/2014-0127.pdf">https://www.rivm.nl/bibliotheek/rapporten/2014-0127.pdf</a>
   */
  private static final double MIN_DISTANCE = 3.5;
  /**
   * @see <a href="https://www.infomil.nl/onderwerpen/lucht-water/luchtkwaliteit/slag/monitoren-nsl/handleiding/monitoringstool/exporteren/overdrachtslijnen/#h32433b09-0867-4747-ac41-91caa71bec34"
   *     >https://www.infomil.nl/onderwerpen/lucht-water/luchtkwaliteit/slag/monitoren-nsl/handleiding/monitoringstool/exporteren/overdrachtslijnen/#h32433b09-0867-4747-ac41-91caa71bec34</a>
   */
  private static final double MAX_DISTANCE_1_AND_4 = 60.0;
  private static final double MAX_DISTANCE_2_AND_3 = 30.0;

  public static double calculateDilutionFactor(final CIMLKRoadProfile roadType, final double distance) {
    final RoadType rtEnum = RoadType.from(roadType);
    final double computeDistance = Math.max(distance, MIN_DISTANCE);

    if (distance > MAX_DISTANCE_2_AND_3 && (roadType == CIMLKRoadProfile.WIDE_STREET_CANYON || roadType == CIMLKRoadProfile.OTHER)) {
      return dilutionMore30(rtEnum, Math.min(computeDistance, MAX_DISTANCE_1_AND_4));
    } else {
      return dilutionLess30(rtEnum, Math.min(computeDistance, MAX_DISTANCE_2_AND_3));
    }
  }

  private static double dilutionMore30(final RoadType roadType, final double distance) {
    return roadType.alpha * Math.pow(distance, BOEING_CONSTANT);
  }

  private static double dilutionLess30(final RoadType roadType, final double distance) {
    return roadType.a * distance * distance + roadType.b * distance + roadType.c;
  }
}
