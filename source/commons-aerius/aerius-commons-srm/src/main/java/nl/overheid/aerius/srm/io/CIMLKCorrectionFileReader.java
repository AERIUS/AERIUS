/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Reader for CIMLK correction files.
 *
 * Available columns:
 * <pre>
 * label;jurisdiction_id;result_type;substance;value;calculation_point_id;
 * </pre>
 */
class CIMLKCorrectionFileReader extends AbstractCIMLKFileReader<CIMLKCorrection> {

  // @formatter:off
  private enum CorrectionColumns implements Columns {
    LABEL,
    JURISDICTION_ID,
    SUBSTANCE,
    VALUE,
    CALCULATION_POINT_ID,
    DESCRIPTION;
    // @formatter:on

    static CorrectionColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  @Override
  protected CIMLKCorrection parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final CIMLKCorrection correction = new CIMLKCorrection();

    correction.setLabel(getString(CorrectionColumns.LABEL));
    correction.setJurisdictionId(getInt(CorrectionColumns.JURISDICTION_ID));
    correction.setResultType(EmissionResultType.CONCENTRATION);
    correction.setSubstance(getEnumValue(Substance.class, CorrectionColumns.SUBSTANCE, correction.getLabel()));
    correction.setValue(getDouble(CorrectionColumns.VALUE));
    final String calculationPointId = getGmlId(CorrectionColumns.CALCULATION_POINT_ID, GMLIdUtil.POINT_PREFIX, warnings);
    correction.setCalculationPointGmlId(calculationPointId);
    correction.setDescription(getString(CorrectionColumns.DESCRIPTION));

    return correction;
  }

  @Override
  Columns[] expectedColumns() {
    return CorrectionColumns.values();
  }

  @Override
  Columns safeColumnOf(final String value) {
    return CorrectionColumns.safeValueOf(value);
  }

}
