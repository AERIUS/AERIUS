/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.io.AbstractLineReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Abstract Reader for CIMLK files.
 *
 */
abstract class AbstractCIMLKFileReader<K> extends AbstractLineReader<K> {

  private static final Pattern NUMBER_PATTERN = Pattern.compile("\\d+");

  interface Columns {
    String name();
  }

  protected static final String DEFAULT_AREA_CODE = "NL";
  protected static final int ROAD_SECTOR_ID = 3100;

  private static final char COLUMN_DELIMITER = ';';
  private static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withDelimiter(COLUMN_DELIMITER);

  private final Map<Columns, Integer> columnIndexes = new HashMap<>();
  private final LineReaderResult<K> readerResults = new LineReaderResult<>();

  private CSVRecord currentRecord;
  private boolean warnedAboutId;

  abstract Columns[] expectedColumns();

  abstract Columns safeColumnOf(String value);

  /**
   * Try to parse the given header and if successful collects the header column indexes.
   *
   * @param header the string (single line) that should contain the header text
   * @return null of header file could be parsed, else the error.
   * @throws IOException
   */
  public AeriusException parseHeader(final String header) throws IOException {
    return headerToColumnIndexes(header);
  }

  @Override
  protected void preprocessLine(final String line) throws IOException {
    currentRecord = CSVParser.parse(line, CSV_FORMAT).getRecords().get(0);
  }

  @Override
  protected String processColumn(final int index) {
    return index < 0 ? "" : currentRecord.get(index);
  }

  @Override
  protected String processColumn(final int index, final int size) {
    return processColumn(index);
  }

  @Override
  protected String skipHeaderRow(final BufferedReader reader, final int headerRowCount) throws IOException, AeriusException {
    // header is handled in {@link #parseHeader}.
    // header is parsed in parseHeader, but to set the current line index passed the header the increment is needed here.
    incrementCurrentLineNumber();
    return null;
  }

  /**
   * Reads the file and returns the results.
   * @param reader containing csv file content
   * @return results
   * @throws IOException general read problem
   */
  public LineReaderResult<K> readObjects(final BufferedReader reader) throws IOException {
    return super.readObjects(reader, 0, Integer.MAX_VALUE, false);
  }

  @Override
  public LineReaderResult<K> readObjects(final InputStream inputStream) throws IOException {
    throw new UnsupportedOperationException("readObjects for inputstream is not supported.");
  }

  @Override
  protected LineReaderResult<K> createLineReaderResult() {
    return readerResults;
  }

  private AeriusException headerToColumnIndexes(final String header) throws IOException {
    final CSVRecord record = CSVParser.parse(header, CSV_FORMAT).getRecords().get(0);

    for (int index = 0; index < record.size(); index++) {
      final String column = record.get(index);
      final Columns knownValue = safeColumnOf(column);

      if (knownValue != null) {
        columnIndexes.put(knownValue, index);
      }
    }
    return validateHeader();
  }

  private AeriusException validateHeader() {
    final List<Columns> notFound = new ArrayList<>();
    for (final Columns expected : expectedColumns()) {
      if (!columnIndexes.containsKey(expected)) {
        notFound.add(expected);
      }
    }
    return notFound.isEmpty() ? null : new AeriusException(AeriusExceptionReason.SRM2_MISSING_COLUMN_HEADER, StringUtils.join(notFound, ", "));
  }

  protected int getInt(final Columns column) throws AeriusException {
    return getInt(column.name(), columnIndexes.get(column));
  }

  protected double getDouble(final Columns column) throws AeriusException {
    return getDouble(column.name(), columnIndexes.get(column));
  }

  protected String getString(final Columns column) throws AeriusException {
    return getString(column.name(), columnIndexes.get(column));
  }

  protected boolean hasColumn(final Columns column) {
    return columnIndexes.containsKey(column);
  }

  protected String getGmlId(final Columns column, final String prefix, final List<AeriusException> warnings) throws AeriusException {
    final String value = getString(column);
    final String gmlId = GMLIdUtil.toValidGmlId(value, prefix);
    if (!warnedAboutId && !gmlId.equals(value)) {
      warnings.add(new AeriusException(AeriusExceptionReason.CSV_ID_ADJUSTED));
      warnedAboutId = true;
    }
    return gmlId;
  }

  protected <T extends Enum<T>> T getNullableEnumValue(final Class<T> enumClass, final Columns column, final String recordId)
      throws AeriusException {
    return StringUtils.isEmpty(getString(column)) ? null : getEnumValue(enumClass, column, recordId);
  }

  protected <T extends Enum<T>> T getEnumValue(final Class<T> enumClass, final Columns column, final String recordId)
      throws AeriusException {
    final String value = getString(column);
    for (final T e : enumClass.getEnumConstants()) {
      if (e.name().equalsIgnoreCase(value)) {
        return e;
      }
    }
    throw new AeriusException(AeriusExceptionReason.CSV_INCORRECT_ENUM_VALUE, String.valueOf(getCurrentLineNumber()), recordId, value, column.name());
  }

  protected Geometry getGeometry(final Columns column, final Predicate<Geometry> geometryPredicate) throws AeriusException {
    final String wktString = getString(column);
    if (StringUtils.isEmpty(wktString)) {
      throw new AeriusException(AeriusExceptionReason.SRM2_INCORRECT_EXPECTED_VALUE, String.valueOf(getCurrentLineNumber()), column.name());
    }
    final Geometry geometry;
    try {
      final org.locationtech.jts.geom.Geometry jtsGeometry = GeometryUtil.getGeometry(wktString.replace('"', ' ').trim());
      geometry = GeometryUtil.getAeriusGeometry(jtsGeometry);
    } catch (final AeriusException e) {
      throw new AeriusException(AeriusExceptionReason.SRM2_INCORRECT_WKT_VALUE, String.valueOf(getCurrentLineNumber()), wktString);
    }
    if (geometryPredicate.test(geometry)) {
      throw new AeriusException(AeriusExceptionReason.SRM2_INCORRECT_WKT_VALUE, String.valueOf(getCurrentLineNumber()), wktString);
    }
    return geometry;
  }

  protected WKTGeometry getWktGeometry(final Columns column, final Predicate<Geometry> geometryPredicate) throws AeriusException {
    getGeometry(column, geometryPredicate);
    final String wktString = getString(column);
    return new WKTGeometry(wktString);
  }

}
