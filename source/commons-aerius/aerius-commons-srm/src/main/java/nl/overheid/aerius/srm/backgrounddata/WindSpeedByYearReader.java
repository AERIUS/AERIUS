/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to read wind speed grid file. Data in file is for a specific year.
 */
final class WindSpeedByYearReader {

  /**
   * If data in file is -9.99 interpret as 5.0.
   */
  private static final double DEFAULT_WIND_SPEED = 5.0;
  private static final int M2KM = 1000;

  private static final int POS_YEAR = 1;
  private static final int POS_X = 2;
  private static final int POS_Y = 3;
  private static final int POS_COLUMNS = 4;
  private static final int POS_ROWS = 5;
  private static final int POS_WIDTH = 6;

  private static final String SPLIT_PATTERN = "\\s+";
  // Y-coordinate in header row is is top of grid upper grid cell. i.e. a value 640 means top cell has y-coordinates 639000 - 640000 (diameter 1000)
  private static final Pattern HEADER_YEAR_PATTERN = Pattern.compile(
      "  0  0  0  0 Windsnelhd m/s        INTERPOL   Jaargemiddelde (\\d+)    F6\\.2    1    (\\d+)\\.000  (\\d+)\\.000(\\d{3})(\\d{3})    (\\d+)\\.000    1\\.000");
  private static final Pattern HEADER_PROGNOSE_PATTERN = Pattern.compile(
      "  0  0  0  0 Windsnelhd m/s        INTERPOL   Jaargemiddelde (05)-14   F6\\.2    1    ([\\d\\.]+)000  (\\d+)\\.000(\\d{3})(\\d{3})    (\\d+)\\.000    1\\.000");

  private int offsetX;
  private int offsetY;
  private int columns;
  private int rows;
  private int diameter;

  public List<WindSpeed> readYear(final BufferedReader reader, final int fileNameYear) throws IOException {
    final MatchResult matcher = matchHeader(reader, HEADER_YEAR_PATTERN);

    matchYear(matcher, fileNameYear);
    return read(matcher, reader);
  }

  private static void matchYear(final MatchResult matcher, final int fileNameYear) {
    final int year = Integer.parseInt(matcher.group(POS_YEAR));

    if (fileNameYear != year) {
      throw new IllegalArgumentException("Year in windsnelheid file doesn't match. Filename year: " + fileNameYear
          + ", year in file: " + year);
    }
  }

  public List<WindSpeed> readPrognose(final BufferedReader reader) throws IOException {
    return read(matchHeader(reader, HEADER_PROGNOSE_PATTERN), reader);
  }

  private static Matcher matchHeader(final BufferedReader reader, final Pattern pattern) throws IOException {
    final String header = reader.readLine();
    final Matcher matcher = pattern.matcher(header);

    if (matcher.matches()) {
      return matcher;
    } else {
      throw new IllegalArgumentException("Couldn't parse header: " + header);
    }
  }

  public int getOffsetX() {
    return offsetX;
  }

  public int getOffsetY() {
    return offsetY;
  }

  public int getDiameter() {
    return diameter;
  }

  public int getColumns() {
    return columns;
  }

  public int getRows() {
    return rows;
  }

  private List<WindSpeed> read(final MatchResult matcher, final BufferedReader reader) throws IOException {
    diameter = Integer.parseInt(matcher.group(POS_WIDTH)) * M2KM;
    final int yTop = ((int) Double.parseDouble((matcher.group(POS_Y)))) * M2KM;

    rows = Integer.parseInt(matcher.group(POS_ROWS));
    columns = Integer.parseInt(matcher.group(POS_COLUMNS));
    offsetX = ((int) Double.parseDouble((matcher.group(POS_X)))) * diameter;
    offsetY = yTop - rows * diameter;
    return read(reader, offsetX, yTop, diameter);
  }

  private List<WindSpeed> read(final BufferedReader reader, final int xOffset, final int yTop, final int diameter)
      throws IOException {
    final List<WindSpeed> list = new ArrayList<>();
    final int halfWidth = diameter / 2;
    int y = yTop - halfWidth;
    String line;

    while ((line = reader.readLine()) != null) {
      final String[] parts = line.trim().split(SPLIT_PATTERN);
      int x = xOffset + halfWidth;

      for (int column = 0; column < parts.length; column++) {
        if (!parts[column].isEmpty()) {
          final WindSpeed wrs = new WindSpeed(x, y);
          final double windSpeed = Double.parseDouble(parts[column]);

          wrs.setWindTotalSpeed(windSpeed < 0 ? DEFAULT_WIND_SPEED : windSpeed);
          list.add(wrs);
        }
        x += diameter;
      }
      y -= diameter;
    }
    return list;
  }

}
