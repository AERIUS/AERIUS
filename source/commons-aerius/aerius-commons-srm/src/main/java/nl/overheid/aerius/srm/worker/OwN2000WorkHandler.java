/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import static nl.overheid.aerius.srm.SRMConstants.OWN2000_SRM2_CALCULATION_OPTIONS_NO_SUBRECEPTORS;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.opencl.DefaultContext;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm.domain.SRMConfiguration;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.calculation.SRM2Calculator;
import nl.overheid.aerius.srm2.calculation.SRMSubReceptorAggregator;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;

/**
 * SRM WorkHandler for the {@link Theme#OWN2000}.
 */
class OwN2000WorkHandler implements Worker<SRMInputData<SRM2RoadSegment>, CalculationResult> {
  private static final Logger LOGGER = LoggerFactory.getLogger(OwN2000WorkHandler.class);

  private final SRMConfiguration config;
  private final PreSRMData preSRMData;
  private final Map<AeriusSRMVersion, SRM2Calculator> srm2Calculators = new ConcurrentHashMap<>();

  public OwN2000WorkHandler(final SRMConfiguration config, final PreSRMData preSRMData) {
    this.config = config;
    this.preSRMData = preSRMData;
  }

  @Override
  public CalculationResult run(final SRMInputData<SRM2RoadSegment> input, final JobIdentifier jobIdentifier,
      final WorkerIntermediateResultSender workerIntermediateResultSender) throws AeriusException {
    checkAndSetMeteo(input);

    LOGGER.debug("Start calculation run for profile OwN2000");
    final AeriusSRMVersion srmVersion = (AeriusSRMVersion) input.getEngineVersion();
    final boolean disableExternalSubReceptors = input.getSubReceptorCalculation() != SubReceptorCalculation.PROVIDED;
    final SRM2Calculator localSRM2Calculator = getOrCreateSrm2Calculator(srmVersion);
    final SRMSubReceptorAggregator subReceptorAggregator = disableExternalSubReceptors ? null
        : new SRMSubReceptorAggregator(input.getReceptors(), input.getEmissionResultKeys());
    final OwN2000Srm2Calculator calculator = new OwN2000Srm2Calculator(localSRM2Calculator, input.getSubstances(), input.getYear(), input.getMeteo(),
        input.getEmissionResultKeys(), input.getCalculateReceptors(), subReceptorAggregator);

    return calculator.calculate(input.getChunkStats(), input.getEmissionSources(), (CalculationEngine) input.getEngineDataKey());
  }

  PreSRMData getData() {
    return preSRMData;
  }

  private static void checkAndSetMeteo(final SRMInputData<SRM2RoadSegment> input) throws AeriusException {
    final Meteo meteo = input.getMeteo();
    final AeriusSRMVersion aeriusSRMVersion = (AeriusSRMVersion) input.getEngineVersion();

    if (meteo == null) {
      input.setMeteo(aeriusSRMVersion.getMeteoForYear(input.getYear()));
    } else if (meteo.isMultiYear() && !meteo.equals(aeriusSRMVersion.getPrognoseBaseMeteo())) {
      // If a custom Meteo is specified, check if the prognose of PreSRM is based on the same years.
      throw new AeriusException(AeriusExceptionReason.SRM_UNSUPPORTED_METEO, aeriusSRMVersion.getPrognoseBaseMeteo().toString(), meteo.toString());
    }
  }

  private SRM2Calculator getOrCreateSrm2Calculator(final AeriusSRMVersion aeriusSRMVersion) {
    return srm2Calculators.computeIfAbsent(aeriusSRMVersion, key -> createSRM2Calculator(aeriusSRMVersion));
  }

  private SRM2Calculator createSRM2Calculator(final AeriusSRMVersion aeriusSRMVersion) {
    return createSRM2Calculator(config, preSRMData, OWN2000_SRM2_CALCULATION_OPTIONS_NO_SUBRECEPTORS, aeriusSRMVersion);
  }

  private static SRM2Calculator createSRM2Calculator(final SRMConfiguration config, final PreSRMData preSRMData,
      final SRM2CalculationOptions calculationOptions, final AeriusSRMVersion aeriusSRMVersion) {
    return new SRM2Calculator(aeriusSRMVersion, new DefaultContext(true, config.isForceCpu()), preSRMData, config.getConnectionTimeout(),
        calculationOptions) {
      @Override
      protected AeriusResultPoint newResultPoint(final AeriusPoint point) {
        return new AeriusResultPoint(point);
      }
    };
  }
}
