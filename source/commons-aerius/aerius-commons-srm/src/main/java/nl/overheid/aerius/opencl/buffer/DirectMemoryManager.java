/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl.buffer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.opencl.buffer.spi.DirectBufferReleaser;
import nl.overheid.aerius.opencl.buffer.spi.NoopDirectBufferReleaser;

/**
 * Utility class for releasing directly allocated memory.
 * It will use a JVM specific DirectBufferReleaser instance to do the actual releasing of the memory. If no implementation is available, the memory
 * will not be released.
 */
public final class DirectMemoryManager {
  private static final Logger LOG = LoggerFactory.getLogger(DirectMemoryManager.class);
  private static final DirectBufferReleaser RELEASER;
  private static final Set<String> SPEC_VERSIONS = Set.of("1.6", "1.7", "1.8");
  private static final Set<String> VENDORS = Set.of("Sun Microsystems Inc.", "Oracle Corporation");
  private static final String IMPLEMENTATION_CLASS_NAME = "nl.overheid.aerius.opencl.buffer.spi.JDKDirectBufferReleaser";

  private DirectMemoryManager() {
    // util
  }

  static {
    final Properties systemProperties = System.getProperties();

    final String specVersion = systemProperties.getProperty("java.specification.version");
    final String runtimeVersion = systemProperties.getProperty("java.runtime.version");
    final String vendor = systemProperties.getProperty("java.vendor");

    DirectBufferReleaser tmpReleaser = null;

    if (SPEC_VERSIONS.contains(specVersion) && VENDORS.contains(vendor)) {
      try {
        final Class<?> tmpClass = Class.forName(IMPLEMENTATION_CLASS_NAME);

        if (DirectBufferReleaser.class.isAssignableFrom(tmpClass)) {
          // Checked above
          @SuppressWarnings("unchecked")
          final Class<? extends DirectBufferReleaser> releaserClass = (Class<? extends DirectBufferReleaser>) tmpClass;

          final Constructor<? extends DirectBufferReleaser> constructor = releaserClass.getConstructor();

          tmpReleaser = constructor.newInstance();
        }
      } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException e) {
        LOG.error("Error creating DirectMemoryReleaser instance <{}>, caused by: {}  Direct memory might not be released in a timely manner.",
            IMPLEMENTATION_CLASS_NAME, e.getMessage());
      } catch (final InvocationTargetException e) {
        LOG.error("DirectMemoryReleaser instance <{}> error in constructor: {}  Direct memory might not be released in a timely manner.",
            IMPLEMENTATION_CLASS_NAME, e.getCause().getMessage());
      }
      if (tmpReleaser == null) {
        tmpReleaser = new NoopDirectBufferReleaser();
      }
    } else {
      logWarningMessage("WARNING: No DirectBufferReleaser found for this virtual machine. Direct memory might not be released in a timely manner.",
          specVersion, runtimeVersion, vendor);
      tmpReleaser = new NoopDirectBufferReleaser();
    }
    RELEASER = tmpReleaser;
  }

  private static void logWarningMessage(final String warning, final String specVersion, final String runtimeVersion, final String vendor) {
    final StringBuilder sb = new StringBuilder(warning).append(System.lineSeparator());
    sb.append("Java specification: ").append(specVersion).append(System.lineSeparator());
    sb.append("Vendor: ").append(vendor).append(System.lineSeparator());
    sb.append("JVM version: ").append(runtimeVersion).append(System.lineSeparator());
    LOG.warn(sb.toString());
  }

  /**
   * Releases the direct memory allocated for the buffer. Does nothing if isDirect() is false.
   * @param buffer The buffer whose allocated memory needs to be released
   */
  public static void release(final ByteBuffer buffer) {
    RELEASER.release(buffer);
  }
}
