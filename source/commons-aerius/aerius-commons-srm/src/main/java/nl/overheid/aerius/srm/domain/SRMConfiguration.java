/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.domain;

import java.io.File;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.worker.EngineWorkerConfiguration;

/**
 * Configuration class for SRM worker.
 */
public class SRMConfiguration extends EngineWorkerConfiguration<AeriusSRMVersion> {

  private final Map<Theme, SRM2CalculationOptions> themeOptions;
  private boolean forceCpu;
  private long connectionTimeout;

  public SRMConfiguration(final int processes, final Map<Theme, SRM2CalculationOptions> options) {
    super(processes);
    themeOptions = options;
  }

  public SRM2CalculationOptions getThemeConfiguration(final Theme theme) {
    return themeOptions.get(theme);
  }

  public boolean hasThemeConfig(final Theme theme) {
    return themeOptions.containsKey(theme);
  }

  public boolean isForceCpu() {
    return forceCpu;
  }

  void setForceCpu(final boolean forceCpu) {
    this.forceCpu = forceCpu;
  }

  public long getConnectionTimeout() {
    return connectionTimeout;
  }

  void setConnectionTimeout(final long connectionTimeout) {
    this.connectionTimeout = connectionTimeout;
  }

  public File getAeriusPreSRMDirectory(final AeriusSRMVersion aeriusSRMVersion) {
    return new File(getVersionsRoot(), aeriusSRMVersion.getAeriusPreSrmVersion());
  }

  public File getLandUseDirectory(final AeriusSRMVersion aeriusSRMVersion) {
    return new File(getVersionsRoot(), aeriusSRMVersion.getLandUseVersion());
  }

  public File getDepositionVelocityFile(final AeriusSRMVersion aeriusSRMVersion) {
    return new File(new File(getVersionsRoot(), aeriusSRMVersion.getDepositionVelocityVersion()), aeriusSRMVersion.getDepositionVelocityFileName());
  }

  public File getWindVeldenDirectory(final AeriusSRMVersion aeriusSRMVersion) {
    return new File(getVersionsRoot(), aeriusSRMVersion.getWindveldenVersion());
  }
}
