/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.io.File;

import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.srm.domain.SRMConfiguration;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.util.ModelDataUtil;
import nl.overheid.aerius.worker.ModelDataUtilPreloader;

/**
 * Util to ensure the SRM model data is present.
 */
class SRMDataUtilPreloader implements ModelDataUtilPreloader<AeriusSRMVersion, SRMConfiguration> {

  private static final String MODEL_NAME = "srm";
  private final ModelDataUtil modelDataUtil;

  public SRMDataUtilPreloader(final HttpClientProxy httpClientProxy) {
    modelDataUtil = new ModelDataUtil(httpClientProxy);
  }

  @Override
  public void ensureModelDataPresence(final SRMConfiguration config) throws Exception {
    for (final Theme theme : Theme.values()) {
      if (config.hasThemeConfig(theme)) {
        final File srmRootDirectory = config.getVersionsRoot();

        if (srmRootDirectory != null) {
          for (final AeriusSRMVersion version : config.getModelPreloadVersions()) {
            final SRM2CalculationOptions options = config.getThemeConfiguration(version.getTheme());

            loadModelData(config, options.isCalculateSRM1(), options.isCalculateDeposition(), srmRootDirectory, version);
          }
        }
      }
    }
  }

  public void ensureModelDataAvailable(final SRMConfiguration config, final SRMInputData input) throws Exception {
    final Theme theme = input.getTheme();
    final File srmRootDirectory = config.getVersionsRoot();

    if (srmRootDirectory != null) {
      final AeriusSRMVersion version = (AeriusSRMVersion) input.getEngineVersion();
      loadModelData(config, theme == Theme.RBL, theme == Theme.OWN2000, srmRootDirectory, version);
    }
  }

  private void loadModelData(final SRMConfiguration config, final boolean isCalculateSRM1, final boolean calculateDeposition,
      final File srmRootDirectory, final AeriusSRMVersion version) throws Exception {
    if (srmRootDirectory.exists() || srmRootDirectory.mkdir()) {

      final String srmRootDirectoryPath = srmRootDirectory.getAbsolutePath();
      modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL_NAME, version.getAeriusPreSrmVersion(), srmRootDirectoryPath,
          path -> true, () -> {});

      modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL_NAME, version.getLandUseVersion(), srmRootDirectoryPath,
          path -> true, () -> {});

      if (isCalculateSRM1) {
        modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL_NAME, version.getWindveldenVersion(), srmRootDirectoryPath,
            path -> true, () -> {});
      }

      if (calculateDeposition) {
        modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL_NAME, version.getDepositionVelocityVersion(),
            srmRootDirectoryPath,
            path -> path.resolve(version.getDepositionVelocityFileName()).toFile().isFile(), () -> {});
      }
    }
  }
}
