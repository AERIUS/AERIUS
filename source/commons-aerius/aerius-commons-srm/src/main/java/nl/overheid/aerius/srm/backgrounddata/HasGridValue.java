/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

/**
 * Classes implementing return a specific data object representative for the give x-y coordinate.
 *
 * @param <T> Data object on the grid
 */
public interface HasGridValue<T> {

  /**
   * Returns an object for the give grid cell in which the coordinate is located.
   *
   * @param x x-coordinate
   * @param y y-coordinate
   * @return data object at the give coordinate or an exception if outside the grid
   */
  T get(double x, double y);
}
