/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.EnumMap;

/**
 * Data class containing landuse data.
 */
public class LandUseData {

  private final EnumMap<LandUseType, MapData> map = new EnumMap<>(LandUseType.class);

  public enum LandUseType {
    LOW("z0nl250.asc"), MED("z0nl1000.asc"), HIGH("z0nl4000.asc");

    private final String filename;

    private LandUseType(final String filename) {
      this.filename = filename;
    }

    String getFilename() {
      return filename;
    }
  }

  public MapData getMapData(final LandUseType type) {
    return map.get(type);
  }

  public void setMapData(final LandUseType type, final MapData mapData) {
    map.put(type, mapData);
  }

  /**
   * Returns the land use value for the given coordinate.
   * <p>
   * Requires the landuse files to be loaded.
   *
   * @param x The x coordinate of the source.
   * @param y The y coordinate of the source.
   * @return the landude value of the 1km map at the given location
   */
  public double getLandUseValueSource(final double x, final double y) {
    final MapData mediumMap = map.get(LandUseType.MED);
    return mediumMap.getValue(x, y);
  }
}
