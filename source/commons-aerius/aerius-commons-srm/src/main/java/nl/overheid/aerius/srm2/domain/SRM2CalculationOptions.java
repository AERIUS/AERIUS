/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.domain;

import java.io.Serializable;
import java.util.Set;

import nl.overheid.aerius.shared.domain.Theme;

/**
 * SRM2 calculation options that determine how a SRM2 calculation should be performed.
 */
public class SRM2CalculationOptions implements Serializable {

  private static final long serialVersionUID = 1L;

  public enum Option {
    /**
     * When set loads all background concentration data.
     */
    ALL_BACKGROUND,
    /**
     * When set calculates deposition.
     */
    CALCULATE_DEPOSITION,
    /**
     * When set calculates srm1 sources with the srm1 algorithm.
     */
    CALCULATE_SRM1,
    /**
     * If test kernels should be enabled. Should be false for production.
     */
    TEST,
  }

  private final Theme theme;

  private final Set<Option> options;

  protected SRM2CalculationOptions(final Theme theme, final Set<Option> options) {
    this.theme = theme;
    this.options = options;
  }

  public Theme getTheme() {
    return theme;
  }

  public boolean isAllBackground() {
    return options.contains(Option.ALL_BACKGROUND) || isCalculateSRM1();
  }

  public boolean isCalculateDeposition() {
    return options.contains(Option.CALCULATE_DEPOSITION);
  }

  public boolean isCalculateSRM1() {
    return options.contains(Option.CALCULATE_SRM1);
  }

  public boolean isTest() {
    return options.contains(Option.TEST);
  }
}
