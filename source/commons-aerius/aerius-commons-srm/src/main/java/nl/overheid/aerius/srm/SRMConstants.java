/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm;

import java.util.ArrayList;
import java.util.Set;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.geo.EPSG;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions.Option;

/**
 * Util class with constants.
 */
public final class SRMConstants {

  /**
   * Omgevingswet N2000 Profile Constants
   */
  private static final class OwN2000Srm2CalculationOptions extends SRM2CalculationOptions {
    private static final long serialVersionUID = 1L;

    OwN2000Srm2CalculationOptions(final Set<Option> options) {
      super(Theme.OWN2000, options);
    }
  }

  /**
   *  CIMLK Profile Constants
   */
  private static final class CIMLKSrm2CalculationOptions extends SRM2CalculationOptions {
    private static final long serialVersionUID = 1L;

    CIMLKSrm2CalculationOptions() {
      super(Theme.RBL, Set.of(Option.ALL_BACKGROUND, Option.CALCULATE_SRM1));
    }
  }

  public static final SRM2CalculationOptions OWN2000_SRM2_CALCULATION_OPTIONS_NO_SUBRECEPTORS =
      new OwN2000Srm2CalculationOptions(Set.of(Option.CALCULATE_DEPOSITION));
  public static final SRM2CalculationOptions CIMLK_SRM2_CALCULATION_OPTIONS = new CIMLKSrm2CalculationOptions();

  /**
   * Name of the prognose wind file for SRM1 calculations.
   */
  public static final String SRM1_WIND_FILE_PROGNOSE_NAME= "wv_05-14.grd";

  /**
   * Molar mass for NO2.
   */
  public static final double MOLAR_MASS_NO2 = 46.0056;
  /**
   * Molar mass for NH3.
   */
  public static final double MOLAR_MASS_NH3 = 17.03;

  /**
   * Maximum length of a single road segment in meters.
   */
  public static final double MAX_SEGMENT_LENGTH = 2.0;

  /**
   * Max distance between source and receptor to include in calculations.
   * The distance sources should be present to perform a calculation.
   * If any source is present in the given distance from the given point the point will be calculated.
   * If no sources present the point result will be set to 0.
   */
  public static final int MAX_SOURCE_DISTANCE = 5000;

  /**
   * The value returned when no calculation could be made due to lack of background data.
   */
  public static final double NO_DATA = Double.NaN;

  /**
   * The number of bytes in a double.
   */
  public static final long DOUBLE_BYTES = 8;

  /**
   * The number of doubles for a single source.
   */
  public static final int SOURCE_DOUBLES = 8;

  /**
   * The number of doubles for a single receptor.
   */
  public static final int RECEPTOR_POS_DOUBLES = 2;

  /**
   * The number of double for a single receptor deposition velocity.
   */

  public static final int RECEPTOR_DEPOSITION_VELOCITY_DOUBLES = 1;

  /**
   * The number of double for a single receptor height.
   */
  public static final int RECEPTOR_HEIGHT_DOUBLES = 1;

  /**
   * The number of wind sectors.
   */
  public static final int WIND_SECTORS = 36;

  /**
   * The number of doubles for the wind sector data. Per wind sector the wind factor and average wind speed.
   */
  public static final int RECEPTOR_WIND_SECTOR_DOUBLES = WIND_SECTORS * 2;

  /**
   * O3 per wind sector, only used for NO2 calculations.
   */
  public static final int RECEPTOR_WIND_SECTOR_O3_DOUBLES = WIND_SECTORS;

  /**
   * To convert from g/m/s. to μg/m/s.
   */
  public static final int EMISSION_CONVERSION = 1000 * 1000;

  /**
   * This is specific for the Dutch hexagon grid, but SRM2 is only supported for the dutch situation.
   */
  public static final EPSG EPSG = nl.overheid.aerius.shared.geo.EPSG.RDNEW;

  /**
   * The hexagon zoom level 1 for the a 100 * 100 square hexagon.
   *
   * This is specific for the Dutch hexagon grid, but SRM2 is only supported for the dutch situation.
   */
  public static final HexagonZoomLevel ZOOM_LEVEL_1 = new HexagonZoomLevel(1, 10000);

  /**
   * Total number of hexagons in a row.
   *
   * This is specific for the Dutch hexagon grid, but SRM2 is only supported for the dutch situation.
   */
  public static final int HEX_HOR = 1529;

  /**
   * Lowest X-coordinate for hexagon mapping of the Netherlands.
   */
  private static final double X_MIN = 3604;
  /**
   * Highest X-coordinate for hexagon mapping of the Netherlands.
   */
  private static final double X_MAX = 287959;

  /**
   * Lowest Y-coordinate for hexagon mapping of the Netherlands.
   */
  private static final double Y_MIN = 296800;
  /**
   * Highest Y-coordinate for hexagon mapping of the Netherlands.
   */
  private static final double Y_MAX = 629300;

  // Constant to convert from microgram/m^2/s to gram/Ha/year
  public static final double DEPOSITION_UNIT_CONVERSION = ImaerConstants.SECONDS_PER_YEAR * ImaerConstants.M2_TO_HA
      / ImaerConstants.MICROGRAM_TO_GRAM;

  //  Constants to convert
  public static final double DEPOSITION_CONVERSION_NO2 = DEPOSITION_UNIT_CONVERSION / SRMConstants.MOLAR_MASS_NO2;
  public static final double DEPOSITION_CONVERSION_NH3 = DEPOSITION_UNIT_CONVERSION / SRMConstants.MOLAR_MASS_NH3;

  /**
   * Rounding scale for PM10_EXCEEDANCE_DAYS.
   */
  public static final int PM10_EXCEEDANCE_DAYS_SCALE = 2;

  private SRMConstants() {
  }

  /**
   * @return Returns a new {@link ReceptorUtil} instance
   */
  public static ReceptorUtil getReceptorUtil() {
    final ArrayList<HexagonZoomLevel> hexagonZoomLevels = new ArrayList<>();
    hexagonZoomLevels.add(ZOOM_LEVEL_1);
    return new ReceptorUtil(new ReceptorGridSettings(new BBox(X_MIN, Y_MIN, X_MAX, Y_MAX), EPSG, HEX_HOR, hexagonZoomLevels));
  }
}
