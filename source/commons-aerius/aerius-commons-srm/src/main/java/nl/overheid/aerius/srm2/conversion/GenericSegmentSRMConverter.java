/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

public class GenericSegmentSRMConverter extends AbstractSegmentSRMConverter<SRM2RoadSegment> {

  @Override
  boolean canConvert(final EmissionSource source) {
    return true;
  }

  @Override
  void addSegments(final LineString geometry, final EmissionSource source, final List<Substance> substances, final List<SRM2RoadSegment> segments,
      final Node startNode) throws AeriusException {
    final Map<Substance, Double> emissionPerMeter = getEmissionPerMeter(geometry, source, substances);

    for (Segment segment = startNode.nextSegment; segment != null; segment = segment.next()) {
      final SRM2RoadSegment roadSegment = createObject(false, RoadElevation.NORMAL, 0, null, null);
      roadSegment.setStartX(segment.start.positionX);
      roadSegment.setStartY(segment.start.positionY);
      roadSegment.setEndX(segment.end.positionX);
      roadSegment.setEndY(segment.end.positionY);

      copyEmissions(emissionPerMeter, roadSegment, Optional.empty());
      segments.add(roadSegment);
    }
  }

}
