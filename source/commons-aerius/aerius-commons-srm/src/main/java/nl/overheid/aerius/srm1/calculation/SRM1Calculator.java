/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm1.calculation;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.theme.cimlk.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLine;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRoadProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;
import nl.overheid.aerius.srm.backgrounddata.WindSpeedGrid;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM1RoadSegment;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Calculates the SRM1 algorithm.
 * <pre>
 * Technische beschrijving van standaardrekenmethode 1 (SRM-1)
 * RIVM Briefrapport 2014-0127
 * K. van Velze, J. Wesseling
 * </pre>
 * @see <a href="https://www.rivm.nl/bibliotheek/rapporten/2014-0127.pdf">https://www.rivm.nl/bibliotheek/rapporten/2014-0127.pdf</a>
 */
public class SRM1Calculator {
  private static final Logger LOG = LoggerFactory.getLogger(SRM1Calculator.class);

  private static final double KALIBRATION_FACTOR = 0.62;
  private static final double B = 0.6;
  private static final double K = 100; // [μg/m3].

  private static final double REGIO_FACTOR_CONST = 5.0;

  private static final Set<Substance> SUBSTANCE_TO_CALCULATE = EnumSet.of(Substance.NH3, Substance.NOX, Substance.PM10, Substance.PM25, Substance.EC);

  private final PreSRMData preSRMData;

  public SRM1Calculator(final PreSRMData preSRMData) {
    this.preSRMData = preSRMData;
  }

  /**
   * Performs a SRM1 calculation.
   *
   * @param year year to calculate
   * @param meteo meteo to base the calculation on
   * @param substances substances to calculate
   * @param emissionResultKeys results that should be returned
   * @param sources emission source segments
   * @param receptors receptors to calculate results for
   * @return List of results.
   * @throws AeriusException when the calculation fails either due to user errors or internal problems.
   */
  public Collection<CIMLKResultPoint> calculate(final int year, final Meteo meteo, final List<Substance> substances,
      final Set<EmissionResultKey> emissionResultKeys, final Collection<SRM1RoadSegment> sources, final Collection<AeriusPoint> receptors,
      final AeriusSRMVersion srmVersion) throws AeriusException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Calculate SRM1 for {} sources", sources.size());
    }

    return actualCalculate(year, meteo, substances, emissionResultKeys, segmentsToMap(sources), receptors,
        receptorsCIMLKResultPointsMap(receptors, preSRMData.getWindSpeedGrid(srmVersion, meteo)), srmVersion);
  }

  /**
   * Puts a the source segments in a map with segmentId as key.
   *
   * @param sources emission sources
   * @return mapped segments
   */
  private static Map<String, SRM1RoadSegment> segmentsToMap(final Collection<SRM1RoadSegment> sources) {
    return sources.stream().collect(Collectors.toMap(SRM1RoadSegment::getSegmentId, Function.identity()));
  }

  /**
   * Creates a map with all results points prepared and the id as key.
   *
   * @param receptors receptors
   * @param table table with wind speed values
   * @return mapped results points
   */
  private static Map<Integer, CIMLKResultPoint> receptorsCIMLKResultPointsMap(final Collection<AeriusPoint> receptors, final WindSpeedGrid table) {
    return receptors.stream().map(r -> createCIMLKResultPoint(r, table))
        .collect(Collectors.toMap(CIMLKResultPoint::getId, Function.identity()));
  }

  private static CIMLKResultPoint createCIMLKResultPoint(final AeriusPoint r, final WindSpeedGrid table) {
    final CIMLKResultPoint point = new CIMLKResultPoint(r);

    final double avgWindSpeed = table.get(point.getRoundedCmX(), point.getRoundedCmY()).getWindTotalSpeed();
    LOG.trace("Windspeed: {} for {}", avgWindSpeed, point);
    point.setWindFactor(avgWindSpeed);
    return point;
  }

  private Collection<CIMLKResultPoint> actualCalculate(final int year, final Meteo meteo, final List<Substance> substances,
      final Set<EmissionResultKey> keys, final Map<String, SRM1RoadSegment> segments, final Collection<AeriusPoint> receptors,
      final Map<Integer, CIMLKResultPoint> resultCache, final AeriusSRMVersion aeriusSRMVersion)
      throws AeriusException {
    final Map<Integer, NOxFraction> noxFractionCache = resultCache.entrySet().stream().collect(
        Collectors.toMap(Map.Entry::getKey, e -> new NOxFraction()));

    for (final AeriusPoint point : receptors) {
      if (!(point instanceof CIMLKCalculationPoint)) {
        continue;
      }
      final CIMLKCalculationPoint cimlkPoint = (CIMLKCalculationPoint) point;

      for (final CIMLKDispersionLine line : cimlkPoint.getDispersionLines()) {
        final SRM1RoadSegment roadSegment = segments.get(line.getRoadGmlId());

        if (roadSegment == null) {
          LOG.debug("Segment could not be found for: {}", line);
          continue;
        }
        calculateSegment(substances, keys, roadSegment, line, resultCache.get(point.getId()), noxFractionCache.get(point.getId()));
      }
    }
    if (substances.contains(Substance.NO2) && keys.contains(EmissionResultKey.NO2_CONCENTRATION)) {
      calculateNO2(year, meteo, resultCache, noxFractionCache, aeriusSRMVersion);
    }
    return resultCache.values();
  }

  private void calculateSegment(final List<Substance> substances, final Set<EmissionResultKey> keys, final SRM1RoadSegment roadSegment,
      final CIMLKDispersionLine line, final CIMLKResultPoint resultPoint, final NOxFraction noxFraction) throws AeriusException {

    final double regioFactor = getRegioFactor(resultPoint);
    final double distance = GeometryUtil.getGeometry(roadSegment.getWKTGeometry()).distance(GeometryUtil.getGeometry(resultPoint.toUnroundedWKT()));
    final EmissionResults results = new EmissionResults();

    keys.stream()
        .filter(key -> SUBSTANCE_TO_CALCULATE.contains(key.getSubstance()))
        .forEach(key -> setData(results, key,
            calculateYearlyAvgConcentration(convertEmission(roadSegment, key.getSubstance()),
                line.getRoadProfile(), distance, line.getTreeProfile().getFactor(), regioFactor)));

    if (substances.contains(Substance.NO2) && keys.contains(EmissionResultKey.NO2_CONCENTRATION)) {
      noxFraction.add(convertEmission(roadSegment, Substance.NOX), convertEmission(roadSegment, Substance.NO2),
          results.get(EmissionResultKey.NOX_CONCENTRATION));
    }
    final EmissionResults erPoint = resultPoint.getEmissionResults();

    for (final EmissionResultKey erk : keys) {
      erPoint.add(erk, results.get(erk));
    }
  }

  private void calculateNO2(final int year, final Meteo meteo, final Map<Integer, CIMLKResultPoint> resultCache,
      final Map<Integer, NOxFraction> noxFractionCache, final AeriusSRMVersion aeriusSRMVersion)
      throws AeriusException {
    final HasGridValue<WindRoseConcentration> o3WindRoseData = preSRMData.getWindRoseConcentrationGrid(aeriusSRMVersion, meteo, year);

    resultCache.entrySet().stream().forEach(r -> {
      final NOxFraction noxFraction = noxFractionCache.get(r.getKey());
      final CIMLKResultPoint resultPoint = r.getValue();

      setData(resultPoint, EmissionResultKey.NO2_CONCENTRATION,
          calculateYearlyAvgConcentrationNO2(noxFraction, getO3(o3WindRoseData, resultPoint)));
      setData(resultPoint, EmissionResultKey.NO2_DIRECT_CONCENTRATION, noxFraction.cbjmNO2Direct());
    });
  }

  private void setData(final EmissionResults results, final EmissionResultKey key, final double value) {
    results.put(key, value < 0.0 ? SRMConstants.NO_DATA : value);
  }

  private void setData(final AeriusResultPoint aeriusResultPoint, final EmissionResultKey key, final double value) {
    aeriusResultPoint.setEmissionResult(key, value < 0.0 ? SRMConstants.NO_DATA : value);
  }

  private static double convertEmission(final SRM1RoadSegment segment, final Substance substance) {
    return segment.getEmission(substance) * SRMConstants.EMISSION_CONVERSION;
  }

  private static double getRegioFactor(final CIMLKResultPoint cimlkPoint) {
    return cimlkPoint.getWindFactor() > 0.0 ? (REGIO_FACTOR_CONST / cimlkPoint.getWindFactor()) : cimlkPoint.getWindFactor();
  }

  private static double getO3(final HasGridValue<WindRoseConcentration> concentrationData, final AeriusPoint receptor) {
    return concentrationData.get(receptor.getRoundedCmX(), receptor.getRoundedCmY()).getBackgroundO3();
  }

  /**
   * Calculate yearly average concentration contribution: Cbjm
   *
   * @param emission
   * @param roadProfile
   * @param distance
   * @param treeFactor
   * @param regioFactor
   * @return
   */
  static double calculateYearlyAvgConcentration(final double emission, final CIMLKRoadProfile roadProfile, final double distance,
      final double treeFactor, final double regioFactor) {
    final double θ = DilutionFactor.calculateDilutionFactor(roadProfile, distance);
    final double conc = KALIBRATION_FACTOR * emission * θ * treeFactor * regioFactor;
    LOG.trace("Emission:{}, roadtype:{}, distance:{}, treefactor:{}, regioFactor:{}, θ:{}, conc:{}",
        emission, roadProfile, distance, treeFactor, regioFactor, θ, conc);
    return conc;
  }

  static double calculateYearlyAvgConcentrationNO2(final NOxFraction noxFraction, final double CajmO3) {
    final double fno2 = noxFraction.fraction();
    final double CbjmNOx = noxFraction.cbjmNOx();

    return fno2 * CbjmNOx + ((B * CajmO3 * CbjmNOx * (1 - fno2)) / (CbjmNOx * (1 - fno2) + K));
  }

  /**
   * Object to calculate the aggregated NO2 fraction as described in 1.17
   */
  static class NOxFraction {
    private double cnoxDirectSum;
    private double cnoxSum;

    void add(final double emissionNOx, final double emissionNO2, final double cnox) {
      final double fno2 = emissionNOx > 0 ? (emissionNO2 / emissionNOx) : 0.0;
      cnoxDirectSum += fno2 * cnox;
      cnoxSum += cnox;
    }

    public double cbjmNO2Direct() {
      return cnoxDirectSum;
    }

    public double cbjmNOx() {
      return cnoxSum;
    }

    double fraction() {
      return cnoxSum > 0 ? (cnoxDirectSum / cnoxSum) : 0.0;
    }
  }
}
