/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.io.LegacyNSLReceptorFileReader.LegacyReceptorResult;

/**
 * Import reader for Nationaal Samenwerkingsprogramma Luchtkwaliteit (NSL) csv files.
 */
public class LegacyNSLImportReader implements ImportReader {

  private static final String SRM2_FILE_EXTENSION = ".csv";

  private final boolean readHeights;

  public LegacyNSLImportReader() {
    this(false);
  }

  public LegacyNSLImportReader(final boolean readHeights) {
    this.readHeights = readHeights;
  }

  @Override
  public boolean detect(final String filename) {
    return filename.toLowerCase(Locale.ENGLISH).endsWith(SRM2_FILE_EXTENSION);
  }

  /**
   * Reads the input stream as legacy CIMLK results file.
   *
   * @param inputStream stream to read results from
   * @return result line based result
   * @throws IOException Error reading the stream
   */
  public LineReaderResult<CIMLKResult> readResultFile(final InputStream inputStream) throws IOException {
    try (InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {
      final CIMLKResultFileReader resultsReader = new CIMLKResultFileReader();
      final AeriusException exception = resultsReader.parseHeader(bufferedReader.readLine());

      if (exception == null) {
        return resultsReader.readObjects(bufferedReader);
      } else {
        final LineReaderResult<CIMLKResult> errorResult = new LineReaderResult<>();

        errorResult.addException(exception);
        return errorResult;
      }
    }
  }

  @Override
  public void read(final String name, final InputStream inputStream, final SectorCategories categories, final Substance substance,
      final ImportParcel importResult) throws IOException, AeriusException {
    final List<Parser<?>> parsers = getPossibleParsers(categories, name);
    try (InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {
      parse(bufferedReader, importResult, parsers);
    }
  }

  private List<Parser<?>> getPossibleParsers(final SectorCategories categories, final String name) {
    final List<Parser<?>> parsers = new ArrayList<>();
    parsers.add(new Parser<>(new LegacyNSLSegmentFileReader(categories),
        (result, readResults) -> handleLegacySegmentResults(result, readResults, name)));
    parsers.add(new Parser<>(new CIMLKSRM2RoadFileReader(categories),
        (result, readResults) -> handleSegmentResults(result, readResults, name)));
    parsers.add(new Parser<>(new CIMLKSRM1RoadFileReader(categories),
        (result, readResults) -> handleSegmentResults(result, readResults, name)));
    parsers.add(new Parser<>(new LegacyNSLReceptorFileReader(readHeights),
        this::handleLegacyReceptorResults));
    parsers.add(new Parser<>(new CIMLKCalculationPointFileReader(),
        this::handleReceptorResults));
    parsers.add(new Parser<>(new CIMLKDispersionLineFileReader(),
        this::handleDispersionLineResults));
    parsers.add(new Parser<>(new CIMLKMeasureFileReader(),
        this::handleMeasureResults));
    parsers.add(new Parser<>(new CIMLKCorrectionFileReader(),
        this::handleCorrectionResults));
    return parsers;
  }

  private void parse(final BufferedReader bufferedReader, final ImportParcel importResult, final List<Parser<?>> parsers)
      throws IOException {
    LineReaderResult<?> results = null;
    final String header = bufferedReader.readLine();
    final List<ImportIntermediateResult> intermediateResults = new ArrayList<>();
    for (final Parser<?> parser : parsers) {
      final ImportIntermediateResult parseResult = parser.parseFile(importResult, bufferedReader, header);
      if (parseResult.isSuccess()) {
        results = parseResult.result;
        break;
      } else {
        intermediateResults.add(parseResult);
      }
    }

    if (results == null) {
      importResult.getExceptions().add(exceptionToAdd(intermediateResults));
    } else {
      importResult.getExceptions().addAll(results.getExceptions());
      importResult.getWarnings().addAll(results.getWarnings());
    }
  }

  /**
   * If we have multiple exceptions because of an error in the input file it's not possible to know which of the supported file types it is.
   * So we guess what it is. If the header is invalid it's probably not that type so we pass the other exception.
   * If they all have a missing header we check which error has the shortest error argument (meaning matches the header the most)
   * and assume that is the type of the file.
   *
   * @param eSegment Exception thrown by segment file reader
   * @param eReceptor Exception thrown by receptor file reader
   * @return exception that should be reported to the user
   */
  private AeriusException exceptionToAdd(final List<ImportIntermediateResult> intermediateResults) {
    return intermediateResults.stream()
        .map(intermediateResult -> intermediateResult.exception)
        .filter(intermediateException -> intermediateException.getReason() == AeriusExceptionReason.SRM2_MISSING_COLUMN_HEADER)
        .sorted((e1, e2) -> e1.getArgs()[0].length() - e2.getArgs()[0].length())
        .findFirst().orElse(intermediateResults.get(0).exception);
  }

  private void handleLegacySegmentResults(final ImportParcel importResult, final LineReaderResult<EmissionSourceFeature> results, final String name) {
    handleSegmentResults(importResult, results, name);

    importResult.getWarnings().add(srm2Warning());
  }

  private void handleSegmentResults(final ImportParcel importResult, final LineReaderResult<EmissionSourceFeature> results, final String name) {
    if (!results.getObjects().isEmpty()) {
      importResult.getSituation().getEmissionSourcesList().addAll(results.getObjects());
    }
  }

  private void handleLegacyReceptorResults(final ImportParcel importResult, final LineReaderResult<LegacyReceptorResult> results) {
    final List<LegacyReceptorResult> legacyResults = results.getObjects();
    final List<CalculationPointFeature> points = legacyResults.stream()
        .map(LegacyReceptorResult::getPoint)
        .collect(Collectors.toList());
    importResult.getCalculationPointsList().addAll(points);
    final List<CIMLKDispersionLineFeature> dispersionLines = legacyResults.stream()
        .map(LegacyReceptorResult::getDispersionLines)
        .flatMap(List::stream)
        .collect(Collectors.toList());
    importResult.getSituation().getCimlkDispersionLinesList().addAll(dispersionLines);

    importResult.getWarnings().add(srm2Warning());
  }

  private void handleReceptorResults(final ImportParcel importResult, final LineReaderResult<CalculationPointFeature> results) {
    importResult.getCalculationPointsList().addAll(results.getObjects());
  }

  private void handleDispersionLineResults(final ImportParcel importResult, final LineReaderResult<CIMLKDispersionLineFeature> results) {
    importResult.getSituation().getCimlkDispersionLinesList().addAll(results.getObjects());
  }

  private void handleMeasureResults(final ImportParcel importResult, final LineReaderResult<CIMLKMeasureFeature> results) {
    importResult.getSituation().getCimlkMeasuresList().addAll(results.getObjects());
  }

  private void handleCorrectionResults(final ImportParcel importResult, final LineReaderResult<CIMLKCorrection> results) {
    importResult.getSituation().getCimlkCorrections().addAll(results.getObjects());
  }

  private AeriusException srm2Warning() {
    return new AeriusException(AeriusExceptionReason.NSL_LEGACY_FILESUPPORT_WILL_BE_REMOVED);
  }

  private static class ImportIntermediateResult {
    private final AeriusException exception;
    private final LineReaderResult<?> result;

    ImportIntermediateResult(final AeriusException exception) {
      this.exception = exception;
      this.result = null;
    }

    ImportIntermediateResult(final LineReaderResult<?> result) {
      this.result = result;
      exception = null;
    }

    boolean isSuccess() {
      return exception == null && result != null;
    }

  }

  private static class Parser<T> {
    final AbstractCIMLKFileReader<T> reader;
    final ParseCallback<T> callback;

    Parser(final AbstractCIMLKFileReader<T> reader, final ParseCallback<T> callback) {
      this.reader = reader;
      this.callback = callback;
    }

    ImportIntermediateResult parseFile(final ImportParcel importResult, final BufferedReader bufferedReader, final String header) throws IOException {
      final AeriusException exception = reader.parseHeader(header);

      if (exception == null) {
        return new ImportIntermediateResult(readObjects(importResult, reader, bufferedReader, callback));
      } else {
        return new ImportIntermediateResult(exception);
      }
    }

    private LineReaderResult<T> readObjects(final ImportParcel importResult, final AbstractCIMLKFileReader<T> reader,
        final BufferedReader bufferedReader, final ParseCallback<T> parseCallback) throws IOException {
      final LineReaderResult<T> results = reader.readObjects(bufferedReader);
      parseCallback.handleResults(importResult, results);
      return results;
    }
  }

  @FunctionalInterface
  private interface ParseCallback<T> {

    void handleResults(ImportParcel importResult, LineReaderResult<T> results);

  }
}
