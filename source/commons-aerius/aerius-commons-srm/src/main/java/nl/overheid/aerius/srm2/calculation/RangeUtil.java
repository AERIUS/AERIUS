/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import java.util.Collection;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

/**
 * Utility class to help determine if receptors are within range of a source.
 */
final class RangeUtil {
  final int maxSourceDistanceSquaredMeter;

  /**
   * Constructor.
   *
   * @param maxSourceDistanceMeter The maximum distance to a calculation point sources should be used in a srm2 calculation.
   */
  RangeUtil(int maxSourceDistanceMeter) {
    maxSourceDistanceSquaredMeter = maxSourceDistanceMeter * maxSourceDistanceMeter;
  }

  /**
   * Determines if the given receptor lies within within range of the source.
   *
   * @param source
   * @param receptor
   * @return true if within range, false otherwise.
   */
  boolean inRange(final SRM2RoadSegment source, final AeriusResultPoint receptor) {
    final Vector2 startToReceptor = new Vector2(receptor.getX() - source.getStartX(), receptor.getY() - source.getStartY());
    final Vector2 endToReceptor = new Vector2(receptor.getX() - source.getEndX(), receptor.getY() - source.getEndY());
    final Vector2 startToEnd = new Vector2(source.getEndX() - source.getStartX(), source.getEndY() - source.getStartY());

    return distanceToPointInRange(startToReceptor) || distanceToPointInRange(endToReceptor)
        || (distanceToLineInRange(startToReceptor, startToEnd) && projectionOnSource(startToReceptor, startToEnd, endToReceptor));
  }

  /**
   * Determines if the given receptor is in range of any of the given sources.
   *
   * @param sources
   * @param receptor
   * @return true if in range, false otherwise.
   */
  boolean inAnyRange(final Collection<SRM2RoadSegment> sources, final AeriusResultPoint receptor) {
    boolean isInRange = false;

    for (final SRM2RoadSegment source : sources) {
      if (inRange(source, receptor)) {
        isInRange = true;
        break;
      }
    }

    return isInRange;
  }

  /**
   * Determines if the distance between two points, as described by the given vector is less then or equal to the
   * maximum range.
   *
   * @param vector
   *          The vector from either the start or end of the source to the receptor.
   * @return True if the length within the maximum range, false otherwise.
   */
  private boolean distanceToPointInRange(final Vector2 vector) {
    return vector.lengthSquared() <= maxSourceDistanceSquaredMeter;
  }

  /**
   * Determines if the receptor lies in range of the (infinite) line defined by the source. We calculate the area of the
   * parallelogram formed by the two vectors. By dividing this by the length of the source, we get the shortest distance
   * to the receptor.
   *
   * @param startToReceptor
   *          The vector from start of the source to the receptor.
   * @param startToEnd
   *          The vector from the start to the end of the source.
   * @return True if in range, false otherwise.
   */
  private boolean distanceToLineInRange(final Vector2 startToReceptor, final Vector2 startToEnd) {
    // Test if the receptor lies near the (infinite) line defined by the source.
    // We calculate the area of the parallelogram formed by the two vectors.
    // By dividing this by the length of the source, we get the shortest distance to the receptor.
    final double distanceToLine = startToReceptor.areaSquared(startToEnd) / startToEnd.lengthSquared();

    return distanceToLine <= maxSourceDistanceSquaredMeter;
  }

  /**
   * Determines if the projection of the receptor on the line formed by the source lies between its start and end point.
   * The dot product is equal to the cosine of the angle between the two vectors, multiplied by the magnitudes of both
   * vectors. The magnitudes are always positive, so the sign of the dot product is determined by the cosine, which is
   * then used to determine if the receptor lies either left or right of the intersection of the two vectors.
   *
   * @param startToReceptor
   *          The vector from the start of the source to the receptor.
   * @param startToEnd
   *          The vector from the start to the end of the source.
   * @param endToReceptor
   *          The vector from the end of the source to the receptor.
   * @return True if the projection lies between the start and end point, false otherwise.
   */
  private boolean projectionOnSource(final Vector2 startToReceptor, final Vector2 startToEnd, final Vector2 endToReceptor) {
    return startToEnd.dotProduct(startToReceptor) > 0 && startToEnd.dotProduct(endToReceptor) < 0;
  }
}
