/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.operation.distance.DistanceOp;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.theme.cimlk.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLine;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Util for specific specific calculation preparations.
 */
public final class PreCalculationUtil {

  private PreCalculationUtil() {
    // util class
  }

  /**
   * Copies the dispersion line objects to their respective calculation point object
   * because this is how the SRM calculation expects the dispersion lines.
   *
   * It also calculates the geometry and length (distance) of the dispersion line.
   *
   * @param sources sources to calculate the distance to the dispersion line to.
   * @param points calculation points to put the dispersion lines on.
   * @param dispersionLines the dispersion lines
   * @throws AeriusException
   */
  public static void assignDispersionLines(final List<EmissionSourceFeature> sources, final List<AeriusPoint> points,
      final List<CIMLKDispersionLineFeature> dispersionLines) throws AeriusException {
    final Map<String, CIMLKCalculationPoint> cimlkPoints = points.stream()
        .filter(CIMLKCalculationPoint.class::isInstance)
        .map(CIMLKCalculationPoint.class::cast)
        .collect(Collectors.toMap(CIMLKCalculationPoint::getGmlId, point -> point));
    final Map<String, EmissionSourceFeature> roadMap = new HashMap<>();

    roadToMap(sources, roadMap);
    if (!cimlkPoints.isEmpty()) {
      for (final CIMLKDispersionLineFeature dispersionLine : dispersionLines) {
        assignAndCalculateGeometry(cimlkPoints, roadMap, dispersionLine);
      }
    }
  }

  private static void roadToMap(final List<EmissionSourceFeature> sources, final Map<String, EmissionSourceFeature> map) {
    for (final EmissionSourceFeature es : sources) {
      map.put(es.getProperties().getGmlId(), es);
    }
  }

  /**
   * Adds the dispersion line to the calculation point and calculates the geometry/distance of the dispersion line.
   *
   * @param cimlkPoints
   * @param roadMap
   * @param dispersionLine
   * @throws AeriusException
   */
  private static void assignAndCalculateGeometry(final Map<String, CIMLKCalculationPoint> cimlkPoints, final Map<String, EmissionSourceFeature> roadMap,
      final CIMLKDispersionLineFeature dispersionLineFeature) throws AeriusException {
    final CIMLKDispersionLine dispersionLine = dispersionLineFeature.getProperties();
    final CIMLKCalculationPoint CIMLKCalculationPoint = cimlkPoints.get(dispersionLine.getCalculationPointGmlId());

    if (CIMLKCalculationPoint != null) {
      setDispersionLineGeometry(dispersionLineFeature, roadMap.get(dispersionLine.getRoadGmlId()), CIMLKCalculationPoint);
      CIMLKCalculationPoint.getDispersionLines().add(dispersionLine);
    }
  }

  /**
   * Calculates and sets the geometry and length (distance) of the dispersion line.
   * It calculates the point on the emission source geometry closest to the calculation point.
   *
   * @param dispersionLine dispersion line to set the values on.
   * @param emissionSource source with geometry starts from.
   * @param calculationPoint point the dispersion line ends
   * @throws AeriusException
   */
  static void setDispersionLineGeometry(final CIMLKDispersionLineFeature dispersionLine, final EmissionSourceFeature emissionSource,
      final CIMLKCalculationPoint calculationPoint) throws AeriusException {
    if (emissionSource != null) {

      final Geometry sourceGeometry = GeometryUtil.getGeometry(emissionSource.getGeometry());
      final Coordinate[] nearestPoints = DistanceOp.nearestPoints(sourceGeometry, GeometryUtil.getGeometry(calculationPoint));
      final LineString lineString = new LineString();
      final double[][] coordinates = new double[nearestPoints.length][];
      for (int i = 0; i < nearestPoints.length; i++) {
        coordinates[i] = new double[] {nearestPoints[i].x, nearestPoints[i].y};
      }
      lineString.setCoordinates(coordinates);
      dispersionLine.setGeometry(lineString);
    }
  }
}
