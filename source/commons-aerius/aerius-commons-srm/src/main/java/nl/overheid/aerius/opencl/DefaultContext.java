/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.common.JogampRuntimeException;
import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLCommandQueue;
import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLDevice;
import com.jogamp.opencl.CLDevice.Type;
import com.jogamp.opencl.CLErrorHandler;
import com.jogamp.opencl.CLException;
import com.jogamp.opencl.CLKernel;
import com.jogamp.opencl.CLPlatform;
import com.jogamp.opencl.CLProgram;
import com.jogamp.opencl.util.CLPlatformFilters;

import nl.overheid.aerius.opencl.buffer.DirectMemoryManager;

/**
 * The default implementation for a Context.
 */
public class DefaultContext implements Context {
  private static final int SIZEOF_BYTE = 1;
  private static final int SIZEOF_SHORT = 2;
  private static final int SIZEOF_CHAR = 2;
  private static final int SIZEOF_INT = 4;
  private static final int SIZEOF_FLOAT = 4;
  private static final int SIZEOF_LONG = 8;
  private static final int SIZEOF_DOUBLE = 8;

  private static final double KB_TO_BYTE = Math.pow(10, 3);// Math.pow(2, 10);
  private static final double BYTE_TO_KB = 1 / KB_TO_BYTE;
  private static final double BYTE_TO_MB = 1 / Math.pow(10, 6);

  private static final Logger LOG = LoggerFactory.getLogger(DefaultContext.class);

  private final CLContext context;

  // Protects access to the device lists.
  private final ReentrantLock connectionLock = new ReentrantLock();
  private final Condition connectionPoolNotEmpty = connectionLock.newCondition();
  // The list of all connections.
  private final List<ConnectionImpl> connections = new ArrayList<ConnectionImpl>();
  // The pool of unused connections.
  private final Queue<ConnectionImpl> connectionPool = new LinkedList<ConnectionImpl>();

  private final ReentrantReadWriteLock closeLock = new ReentrantReadWriteLock();
  private boolean closing;
  private boolean closed;

  /**
   * Creates a new Context, using all available devices.
   */
  public DefaultContext() {
    this(false, false);
  }

  /**
   * Creates a new Context.
   *
   * @param requireDouble
   *          If true the context will only use devices that support double precision floating point numbers.
   * @param forceCpu
   *          If true the context will use CPU devices, otherwise it will try to use GPU devices instead.
   */
  public DefaultContext(final boolean requireDouble, final boolean forceCpu) {
    final List<CLDevice> candidateDevices = getDevices(requireDouble, forceCpu);

    LOG.info("Adding {} devices to pool.", candidateDevices.size());
    context = CLContext.create(candidateDevices.toArray(new CLDevice[candidateDevices.size()]));

    context.addCLErrorHandler(new CLErrorHandler() {
      @Override
      public void onError(final String errinfo, final ByteBuffer privateInfo, final long cb) {
        LOG.error("OpenCL error: {}", errinfo);
      }
    });

    connectionLock.lock();

    try {
      // Initialize connections for all devices.
      for (final CLDevice device : candidateDevices) {
        final ConnectionImpl connection = new ConnectionImpl(device);
        connections.add(connection);
        connectionPool.add(connection);
      }

      connectionPoolNotEmpty.signalAll();

    } finally {
      connectionLock.unlock();
    }
  }

  /**
   * Test for availability of OpenCL.
   *
   * @return true if available.
   */
  public static boolean isOpenCLAvailable() {
    boolean available;
    final CLPlatform[] clPlatforms;
    try {
      clPlatforms = CLPlatform.listCLPlatforms();
      available = clPlatforms != null && clPlatforms.length > 0;
    } catch (final JogampRuntimeException | CLException e) {
      available = false;
    }
    return available;
  }

  private List<CLDevice> getDevices(final boolean requireDouble, final boolean forceCpu) {
    final List<CLDevice> candidateDevices = new ArrayList<>();

    if (!forceCpu) {
      getCandidates(candidateDevices, requireDouble, Type.GPU);
    }
    if (candidateDevices.isEmpty()) {
      getCandidates(candidateDevices, requireDouble, Type.CPU);
    }

    if (candidateDevices.isEmpty()) {
      throw new RuntimeException("No OpenCL devices available.");
    }
    return candidateDevices;
  }

  @SuppressWarnings("unchecked")
  private void getCandidates(final List<CLDevice> candidateDevices, final boolean requireDouble, final Type type) {
    final List<CLDevice> possibleDevices = new ArrayList<>();
    for (final CLPlatform platform : CLPlatform.listCLPlatforms(CLPlatformFilters.type(type))) {
      LOG.info("Querying platform {}, version {} for {} devices", platform.getVendor(), platform.getVersion(), type);
      for (final CLDevice device : platform.listCLDevices(type)) {
        if (!requireDouble || device.isDoubleFPAvailable()) {
          LOG.info("Adding device {}", device);
          LOG.info("Maximum work group size {}", device.getMaxWorkGroupSize());
          LOG.info("Maximum global memory size {}Mb", (int) (device.getGlobalMemSize() * BYTE_TO_MB));
          LOG.info("Maximum global cache size {}Kb", (int) (device.getGlobalMemCacheSize() * BYTE_TO_KB));
          LOG.info("Maximum local memory size {}Kb", (int) (device.getLocalMemSize() * BYTE_TO_KB));
          LOG.info("Local memory type {}", device.getLocalMemType());
          LOG.info("Maximum buffer size {}Kb", (int) (device.getMaxConstantBufferSize() * BYTE_TO_KB));
          LOG.info("Maximum allocatable buffer size {}Mb", (int) (device.getMaxMemAllocSize() * BYTE_TO_MB));
          LOG.info("Maximum compute units {}", device.getMaxComputeUnits());
          LOG.info("Maximum clock frequency {}MHz", device.getMaxClockFrequency());
          possibleDevices.add(device);
        } else {
          LOG.info("Ignoring device without double support {}", device);
        }
      }
    }
    if (!possibleDevices.isEmpty()) {
      addToCandidatesIfWorking(candidateDevices, possibleDevices, type);
    }
  }

  private static void addToCandidatesIfWorking(final List<CLDevice> candidateDevices, final List<CLDevice> possibleDevices, final Type type) {
    try {
      // In some case, multiple devices in one context work fine.
      // In others, an exception will be thrown if the context is created.
      // In that case, we can still try to use only the best one.
      final CLContext multipleContext = CLContext.create(possibleDevices.toArray(new CLDevice[candidateDevices.size()]));
      candidateDevices.addAll(possibleDevices);
      multipleContext.release();
    } catch (final CLException e) {
      LOG.trace("Error while creating context for possible devices", e);
      LOG.info("Could not create context for {}, trying individual one", possibleDevices);
      // Sort from high performance to low.
      possibleDevices.sort((d1, d2) -> Integer.compare(qualify(d2), qualify(d1)));
      for (final CLDevice device : possibleDevices) {
        try {
          final CLContext individualContext = CLContext.create(device);
          candidateDevices.add(device);
          LOG.info("Individual one for type {} tested: {}. Seems to work, using it as candidate.", type, device);
          individualContext.release();
          break;
        } catch (final CLException e2) {
          LOG.trace("Error while creating context for device {}", device, e2);
          LOG.warn("Could not create context for device {}, not added for this type", device);
        }
      }
    }
  }

  private static int qualify(final CLDevice device) {
    // Jogamp has a method getMaxFlopsDevice.
    // That uses the definition maxComputeUnits * maxClockFrequency to determine 'Flops'.
    return device.getMaxComputeUnits() * device.getMaxClockFrequency();
  }

  // Required??
  CLProgram compile(final String source) {
    return context.createProgram(source);
  }

  // Required??
  CLProgram compile(final InputStream source) {
    try {
      return context.createProgram(source);
    } catch (final IOException e) {
      throw new UncheckedIOException("Invalid cl source.", e);
    }
  }

  @Override
  public int getPoolSize() {
    return connections.size();
  }

  @Override
  public int getCurrentPoolSize() {
    connectionLock.lock();
    try {
      return connectionPool.size();
    } finally {
      connectionLock.unlock();
    }
  }

  @Override
  public boolean isClosed() {
    closeLock.readLock().lock();

    try {
      return closing || closed;
    } finally {
      closeLock.readLock().unlock();
    }
  }

  @Override
  public Connection getConnection() {
    if (isClosed()) {
      throw new IllegalStateException("The context has already been closed.");
    }

    connectionLock.lock();

    try {
      while (connectionPool.size() == 0) {
        try {
          connectionPoolNotEmpty.await();
        } catch (final InterruptedException e) {
          Thread.currentThread().interrupt();
        }
      }

      return connectionPool.poll();
    } finally {
      connectionLock.unlock();
    }
  }

  @Override
  public Connection getConnection(final long time, final TimeUnit unit) {
    if (isClosed()) {
      throw new IllegalStateException("The context has already been closed.");
    }

    if (time <= 0) {
      return getConnection();
    }

    // The time in nanoseconds
    long nanoTime = unit.toNanos(time);

    connectionLock.lock();

    try {
      while (connectionPool.size() == 0) {
        try {
          if (nanoTime <= 0) {
            return null;
          }

          nanoTime = connectionPoolNotEmpty.awaitNanos(nanoTime);
        } catch (final InterruptedException e) {
          Thread.currentThread().interrupt();
        }
      }

      return connectionPool.poll();
    } finally {
      connectionLock.unlock();
    }
  }

  @Override
  public void close() {
    if (isClosed()) {
      return;
    }

    LOG.debug("Closing OpenCL context.");

    closeLock.writeLock().lock();

    try {
      if (closing || closed) {
        return;
      }

      closing = true;
    } finally {
      closeLock.writeLock().unlock();
    }

    // Wait for all connections to be closed before closing the underlying OpenCL context.
    connectionLock.lock();

    try {
      while (connectionPool.size() < connections.size()) {
        try {
          connectionPoolNotEmpty.await();
        } catch (final InterruptedException e) {
          Thread.currentThread().interrupt();
        }
      }
    } finally {
      connectionLock.unlock();
    }

    closeLock.writeLock().lock();

    try {
      closed = true;
      context.release();
    } finally {
      closeLock.writeLock().unlock();
    }
  }

  private class ConnectionImpl implements Connection {
    private final CLDevice device;
    private final CLCommandQueue queue;
    private final Map<Source, CLProgram> programs = new HashMap<Source, CLProgram>();
    private final List<CLBuffer<? extends Buffer>> clBuffers = new ArrayList<>();
    private final Map<CLBuffer<? extends Buffer>, ByteBuffer> buffers = new IdentityHashMap<>();

    public ConnectionImpl(final CLDevice device) {
      this.device = device;
      this.queue = device.createCommandQueue();
    }

    @Override
    public long getMaxGlobalMemSize() {
      return device.getGlobalMemSize();
    }

    @Override
    public long getMaxLocalMemSize() {
      return device.getLocalMemSize();
    }

    @Override
    public long getMaxBufferSize() {
      return device.getMaxMemAllocSize();
    }

    @Override
    public final CLBuffer<DoubleBuffer> createDoubleCLBuffer(final int size, final MemoryUsage usage) {
      return createBuffer(size, DoubleBuffer.class, usage);
    }

    @Override
    public <T extends Buffer> CLBuffer<T> createCLBuffer(final T buffer, final MemoryUsage usage) {
      if (!buffer.isDirect()) {
        throw new RuntimeException("Direct buffer required.");
      }
      final CLBuffer<T> clBuffer = context.createBuffer(buffer, usage.getClValue());
      clBuffers.add(clBuffer);
      return clBuffer;
    }

    @Override
    public <T extends Buffer> CLBuffer<T> createAndUploadBuffer(final T buffer, final MemoryUsage usage) {
      final CLBuffer<T> cLBuffer = createCLBuffer(buffer, usage);
      uploadBuffer(cLBuffer);
      return cLBuffer;
    }

    @Override
    public void rewindAndUploadBuffer(final CLBuffer<?> buffer) {
      buffer.getBuffer().rewind();
      uploadBuffer(buffer);
    }

    @Override
    public void uploadBuffer(final CLBuffer<?> buffer) {
      queueBufferWrite(buffer, false);
    }

    @Override
    public void downloadBuffer(final CLBuffer<?> buffer) {
      queueBufferRead(buffer, true);
      buffer.getBuffer().rewind();
    }

    private ByteBuffer createDirectByteBuffer(final int bytes) {
      return ByteBuffer.allocateDirect(bytes).order(ByteOrder.nativeOrder());
    }

    @Override
    public <T extends Buffer> CLBuffer<T> createBuffer(final int size, final Class<T> type, final MemoryUsage usage) {
      ByteBuffer backingBuffer;
      T buffer;

      if (type == ByteBuffer.class) {
        backingBuffer = createDirectByteBuffer(size * SIZEOF_BYTE);
        buffer = type.cast(backingBuffer);
      } else if (type == CharBuffer.class) {
        backingBuffer = createDirectByteBuffer(size * SIZEOF_CHAR);
        buffer = type.cast(backingBuffer.asCharBuffer());
      } else if (type == ShortBuffer.class) {
        backingBuffer = createDirectByteBuffer(size * SIZEOF_SHORT);
        buffer = type.cast(backingBuffer.asShortBuffer());
      } else if (type == IntBuffer.class) {
        backingBuffer = createDirectByteBuffer(size * SIZEOF_INT);
        buffer = type.cast(backingBuffer.asIntBuffer());
      } else if (type == LongBuffer.class) {
        backingBuffer = createDirectByteBuffer(size * SIZEOF_LONG);
        buffer = type.cast(backingBuffer.asLongBuffer());
      } else if (type == FloatBuffer.class) {
        backingBuffer = createDirectByteBuffer(size * SIZEOF_FLOAT);
        buffer = type.cast(backingBuffer.asFloatBuffer());
      } else if (type == DoubleBuffer.class) {
        backingBuffer = createDirectByteBuffer(size * SIZEOF_DOUBLE);
        buffer = type.cast(backingBuffer.asDoubleBuffer());
      } else {
        throw new RuntimeException("Unsupported buffer type " + type.getName());
      }
      final CLBuffer<T> clBuffer = context.createBuffer(buffer, usage.getClValue());

      clBuffers.add(clBuffer);
      buffers.put(clBuffer, backingBuffer);

      return clBuffer;
    }

    @Override
    public void releaseBuffer(final CLBuffer<? extends Buffer> buffer) {
      LOG.trace("Release CLBufffer");
      if (clBuffers.remove(buffer)) {
        if (!buffer.isReleased()) {
          buffer.release();
        }
        final ByteBuffer removedBuffer = buffers.remove(buffer);

        if (removedBuffer != null) {
          LOG.trace("Release direct memory buffer");
          // Clear direct memory as well
          DirectMemoryManager.release(removedBuffer);
        }
      } else {
        LOG.warn("Attempting to release unknown buffer.");
      }
    }

    @Override
    public CLKernel getKernel(final Source source, final String kernelName) {
      CLProgram program = programs.get(source);

      if (program == null) {
        try {
          program = context.createProgram(source.getSource()).build(device);
        } catch (final CLException e) {
          LOG.error("Error compiling source: {} {}", e.getMessage(), e.getCLErrorString());
          try (BufferedReader br = new BufferedReader(new StringReader(source.getSource()))) {
            final StringBuilder sb = new StringBuilder("Source file:").append(System.lineSeparator());
            int lineNr = 1;

            for (String line = br.readLine(); line != null; line = br.readLine()) {
              sb.append(lineNr++).append('\t');
              sb.append(line);
              sb.append(System.lineSeparator());
            }

            LOG.error(sb.toString());
          } catch (final IOException e1) {
            // Should not happpen, but only makes debugging the cl source more difficult.
          }

          throw e;
        }

        programs.put(source, program);
      }

      return program.createCLKernel(kernelName);
    }

    @Override
    public void queueBufferWrite(final CLBuffer<? extends Buffer> buffer, final boolean block) {
      queue.putWriteBuffer(buffer, block);
    }

    @Override
    public void queueBufferRead(final CLBuffer<? extends Buffer> buffer, final boolean block) {
      queue.putReadBuffer(buffer, block);
    }

    @Override
    public void queue1DKernelExecution(final CLKernel kernel, final long worksize) {
      // Use kernel size instead of device size to calculate globalworksize.
      final long localWorkSize = (int) kernel.getWorkGroupSize(device);
      final long r = worksize % localWorkSize;
      final long globalWorkSize = r == 0 ? worksize : worksize + localWorkSize - r;
      queue.put1DRangeKernel(kernel, 0, globalWorkSize, localWorkSize);
    }

    @Override
    public void releaseBuffers() {
      try {
        // Release all buffers that have not been released yet.
        while (!clBuffers.isEmpty()) {
          try {
            releaseBuffer(clBuffers.get(0));
          } catch (final JogampRuntimeException e) {
            LOG.error("Error occured releasing CL buffer.", e);
          }
        }
      } finally {
        // Remove all references to old buffers.
        // They should all have been released before this.
        clBuffers.clear();
      }
    }

    @Override
    public void close() {
      try {
        releaseBuffers();
      } finally {
        // Return the connection to the pool again.
        connectionLock.lock();

        try {
          connectionPool.add(this);
          connectionPoolNotEmpty.signalAll();
        } finally {
          connectionLock.unlock();
        }
      }
    }
  }
}
