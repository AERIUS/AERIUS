/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.srm.backgrounddata.PreSrmXYReader.YearPattern;
import nl.overheid.aerius.srm.backgrounddata.bgconcentrations.BackgroundConcentrationReader;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.util.FileUtil;

/**
 * Builder for reading Wind Rose Concentration data for specifc years and prognose data.
 */
public class WindRoseConcentrationBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(WindRoseConcentrationBuilder.class);

  private static final String PROGNOSE = "prognose";
  // files are: con_2020_year, con_2020_prognose, con_ijmond_2020_?, con_schiphol_2020_?
  private static final Pattern CONCENTRATION_CORRECTION_FILE_PATTERN = Pattern.compile("con_?([^_]*)_(\\d+)_(\\w+)\\.txt");

  public void loadWindRoseConcentration(final AeriusSRMVersion aeriusSRMVersion, final PreSRMData data, final File directory,
      final SRM2CalculationOptions options) throws IOException {
    LOG.info("Loading concentration background data");
    LOG.info("Scanning {}", directory.getName());
    final List<WindRoseFileName> files = findFiles(directory, CONCENTRATION_CORRECTION_FILE_PATTERN);
    final Map<String, List<WindRoseFileName>> fileMap = new HashMap<>();

    files.forEach(f -> {
      final String key = f.getKey();

      fileMap.putIfAbsent(key, new ArrayList<>());
      fileMap.get(key).add(f);
    });

    fileMap.forEach((k, v) -> {
      try {
        loadWRConcentrationForYear(aeriusSRMVersion, data, options, v);
      } catch (final IOException e) {
        throw new UncheckedIOException(e);
      }
    });
  }

  private void loadWRConcentrationForYear(final AeriusSRMVersion aeriusSRMVersion, final PreSRMData data, final SRM2CalculationOptions options,
      final List<WindRoseFileName> files) throws IOException {
    final HasGridValue<WindRoseConcentration> grid = BackgroundConcentrationReader.read(aeriusSRMVersion, options, files);

    if (files.get(0).yearPattern == YearPattern.YEAR_PROGNOSE) {
      data.addWindRoseConcentrationPrognoseGrid(aeriusSRMVersion, files.get(0).year, grid);
    } else {
      data.addWindRoseConcentrationGrid(aeriusSRMVersion, files.get(0).year, grid);
    }
  }

  private List<WindRoseFileName> findFiles(final File directory, final Pattern pattern) throws FileNotFoundException {
    return FileUtil.getFilteredFiles(directory, pattern, (dir, name, matcher) -> {
      final WindRoseFileName wrfn = new WindRoseFileName();

      wrfn.file = new File(dir, name);
      if (matcher.groupCount() == 3) {
        wrfn.correction = matcher.group(1);
        wrfn.year = Integer.parseInt(matcher.group(2));
        wrfn.type = matcher.group(3);
      } else {
        wrfn.year = Integer.parseInt(matcher.group(1));
        wrfn.type = matcher.group(2);
      }
      wrfn.yearPattern = wrfn.type.equalsIgnoreCase(PROGNOSE) ? YearPattern.YEAR_PROGNOSE : YearPattern.YEAR;
      return wrfn;
    });
  }

  public static class WindRoseFileName {
    public File file;
    public int year;
    public String type;
    public String correction;
    public YearPattern yearPattern;

    public String getKey() {
      return type + '_' + year;
    }
  }
}
