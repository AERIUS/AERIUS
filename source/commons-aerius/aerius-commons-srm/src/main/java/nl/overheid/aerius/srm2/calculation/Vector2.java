/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

/**
 * Basic 2D vector class.
 */
final class Vector2 {
  private final double x;
  private final double y;

  /**
   * Creates a new instance with the given x and y components.
   *
   * @param x
   * @param y
   */
  Vector2(double x, double y) {
    this.x = x;
    this.y = y;
  }

  double getX() {
    return x;
  }

  double getY() {
    return y;
  }

  /**
   * @return The squared length of the vector.
   */
  double lengthSquared() {
    return x * x + y * y;
  }

  /**
   * @param other
   * @return The dot product of this vector with the other vector.
   */
  double dotProduct(final Vector2 other) {
    return x * other.x + y * other.y;
  }

  /**
   * @param other
   * @return The squared area of the parallelogram defined by the two vectors.
   */
  double areaSquared(final Vector2 other) {
    // The magnitude of the cross product of two 3D vectors is equal to the area of the parallelogram defined by those
    // vectors.
    // We can simplify this for 2D vectors, as the z component is 0.
    // The x and y component of the resulting cross product will therefor also be 0.
    // Because we want the square of the area, we can avoid Math.abs() as well.
    final double area = x * other.y - y * other.x;

    return area * area;
  }
}
