/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CIMLKCalculationPoint;

/**
 * Flattens a list of calculation points ({@link CIMLKCalculationPoint}) and writes it in a csv file.
 */
public final class CIMLKCalculationPointWriter extends AbstractCIMLKWriter<CIMLKCalculationPointWriter.Data> {

  protected static class Data {

    final CIMLKCalculationPoint point;
    final String wkt;

    Data(final CIMLKCalculationPoint point, final String wkt) {
      this.point = point;
      this.wkt = wkt;
    }

  }

  private static class Column extends GenericColumn<Data> {

    public Column(final String header, final Function<Data, String> function) {
      super(header, function);
    }

  }

  private static class PointColumn extends Column {

    public PointColumn(final String header, final Function<CIMLKCalculationPoint, String> function) {
      super(header, data -> function.apply(data.point));
    }

  }

  private static final Column[] COLUMNS = new Column[] {
      new PointColumn("calculation_point_id", point -> point.getGmlId()),
      new PointColumn("jurisdiction_id", point -> point.getJurisdictionId() == null ? "" : String.valueOf(point.getJurisdictionId())),
      new PointColumn("label", point -> point.getLabel()),
      new PointColumn("monitor_substance", point -> point.getMonitorSubstance() == null ? "" : point.getMonitorSubstance().name()),
      new PointColumn("rejection_ground", point -> point.getRejectionGrounds() == null ? "" : point.getRejectionGrounds().name()),
      new PointColumn("description", point -> point.getDescription()),
      new Column("geometry", data -> data.wkt),
  };

  private static final List<GenericColumn<Data>> DEFAULT_COLUMNS = Collections.unmodifiableList(Arrays.asList(COLUMNS));

  @Override
  List<GenericColumn<Data>> columns() {
    return DEFAULT_COLUMNS;
  }

  /**
   * write row of data to the writer (does not close the writer);
   */
  public void writeRow(final Writer writer, final CalculationPointFeature point) throws IOException {
    if (point.getProperties() instanceof CIMLKCalculationPoint) {
      final String wkt = toWkt(point.getGeometry());
      final Data data = new Data((CIMLKCalculationPoint) point.getProperties(), wkt);
      super.writeRow(writer, data);
    }
  }

  public void write(final File outputFile, final List<CalculationPointFeature> points) throws IOException {
    try (final Writer writer = Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8)) {
      writeHeader(writer);
      for (final CalculationPointFeature point : points) {
        writeRow(writer, point);
      }
    }
  }

}
