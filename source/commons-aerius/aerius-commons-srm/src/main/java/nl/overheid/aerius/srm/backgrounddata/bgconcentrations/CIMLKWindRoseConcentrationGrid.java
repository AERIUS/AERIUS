/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata.bgconcentrations;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.PointGrid;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;

class CIMLKWindRoseConcentrationGrid implements HasGridValue<WindRoseConcentration> {

  private final PointGrid<WindRoseConcentration> backgroundConcentrations;
  private final PointGrid<WindRoseConcentration> schipholCorrections;
  private final IjmondCorrectionConcentrationGrid ijmondCorrections;

  public CIMLKWindRoseConcentrationGrid(final PointGrid<WindRoseConcentration> backgroundConcentrations,
      final PointGrid<WindRoseConcentration> schipholCorrections, final IjmondCorrectionConcentrationGrid ijmondCorrections) {
    this.backgroundConcentrations = backgroundConcentrations;
    this.schipholCorrections = schipholCorrections;
    this.ijmondCorrections = ijmondCorrections;
  }

  @Override
  public WindRoseConcentration get(final double x, final double y) {
    return updateForSchiphol(updateForIjmond(backgroundConcentrations.get(x, y), x, y), x, y);
  }

  private WindRoseConcentration updateForIjmond(final WindRoseConcentration wc, final double x, final double y) {
    final IjmondCorrectionConcentration ijMondValue = ijmondCorrections == null ? null : ijmondCorrections.getOrNull(x, y);

    if (ijMondValue == null || wc == null) {
      return wc;
    } else {
      final WindRoseConcentration clone = wc.copy(ijMondValue.getX(), ijMondValue.getY());

      clone.setGCN(Substance.PM10, ijMondValue.getGcnPm10());
      clone.setGCN(Substance.PM25, ijMondValue.getGcnPm25());
      clone.setGCN(Substance.EC, ijMondValue.getGcnEc());
      clone.setCHwn(Substance.PM10, ijMondValue.getcHwnPm10());
      clone.setCHwn(Substance.PM25, ijMondValue.getcHwnPm25());
      clone.setCHwn(Substance.EC, ijMondValue.getcHwnEc());
      return clone;
    }
  }

  private WindRoseConcentration updateForSchiphol(final WindRoseConcentration wc, final double x, final double y) {
    final WindRoseConcentration schipholValue = schipholCorrections == null || wc == null ? null : schipholCorrections.getOrNull(x, y);

    if (schipholValue == null) {
      return wc;
    } else {
      final WindRoseConcentration clone = schipholValue.copy();

      clone.setGCN(Substance.PM10, wc.getGCN(Substance.PM10));
      clone.setGCN(Substance.PM25, wc.getGCN(Substance.PM25));
      clone.setGCN(Substance.EC, wc.getGCN(Substance.EC));
      clone.setCHwn(Substance.PM10, wc.getCHwn(Substance.PM10));
      clone.setCHwn(Substance.PM25, wc.getCHwn(Substance.PM25));
      clone.setCHwn(Substance.EC, wc.getCHwn(Substance.EC));
      return clone;
    }
  }

  PointGrid<WindRoseConcentration> getSchipholCorrections() {
    return schipholCorrections;
  }
}
