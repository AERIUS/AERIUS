/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMonitorSubstance;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRejectionGrounds;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Reader for CIMLK calculation point files.
 *
 * Available columns:
 * <pre>
 * calculation_point_id;jurisdiction_id;label;monitor_substance;rejection_ground;description;geometry
 * </pre>
 */
class CIMLKCalculationPointFileReader extends AbstractCIMLKFileReader<CalculationPointFeature> {

  // @formatter:off
  private enum CalculationPointColumns implements Columns {
    CALCULATION_POINT_ID,
    JURISDICTION_ID,
    LABEL,
    MONITOR_SUBSTANCE,
    REJECTION_GROUND,
    DESCRIPTION,
    GEOMETRY;
    // @formatter:on

    static CalculationPointColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  private int calculationPointIdx = 0;

  @Override
  protected CalculationPointFeature parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final CalculationPointFeature feature = createCurrentPoint(warnings);
    final CIMLKCalculationPoint point = (CIMLKCalculationPoint) feature.getProperties();

    point.setLabel(getString(CalculationPointColumns.LABEL));
    point.setJurisdictionId(getInt(CalculationPointColumns.JURISDICTION_ID));
    point.setMonitorSubstance(
        getNullableEnumValue(CIMLKMonitorSubstance.class, CalculationPointColumns.MONITOR_SUBSTANCE, String.valueOf(point.getGmlId())));
    point.setRejectionGrounds(
        getNullableEnumValue(CIMLKRejectionGrounds.class, CalculationPointColumns.REJECTION_GROUND, String.valueOf(point.getGmlId())));
    point.setDescription(getString(CalculationPointColumns.DESCRIPTION));

    return feature;
  }

  private CalculationPointFeature createCurrentPoint(final List<AeriusException> warnings) throws AeriusException {
    final String calculationPointId = getGmlId(CalculationPointColumns.CALCULATION_POINT_ID, GMLIdUtil.POINT_PREFIX, warnings);
    final String wktString = getString(CalculationPointColumns.GEOMETRY);
    final Geometry point = getGeometry(CalculationPointColumns.GEOMETRY, geometry -> geometry.type() != GeometryType.POINT);

    if (point instanceof Point) {
      final CalculationPointFeature feature = new CalculationPointFeature();
      feature.setGeometry((Point) point);
      feature.setId(calculationPointId);
      final CIMLKCalculationPoint calculationPoint = new CIMLKCalculationPoint();
      calculationPoint.setGmlId(calculationPointId);
      calculationPoint.setCustomPointId(++calculationPointIdx);
      feature.setProperties(calculationPoint);
      return feature;
    } else {
      throw new AeriusException(AeriusExceptionReason.SRM2_INCORRECT_WKT_VALUE, String.valueOf(getCurrentLineNumber()), wktString);
    }
  }

  @Override
  Columns[] expectedColumns() {
    return CalculationPointColumns.values();
  }

  @Override
  Columns safeColumnOf(final String value) {
    return CalculationPointColumns.safeValueOf(value);
  }

}
