/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLine;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;

/**
 * Flattens a list of dispersion lines ({@link CIMLKDispersionLine}) and writes it in a csv file.
 */
public final class CIMLKDispersionLineWriter extends AbstractCIMLKWriter<CIMLKDispersionLineWriter.Data> {

  protected static class Data {

    final CIMLKDispersionLine dispersionLine;
    final String wkt;
    final String distance;

    Data(final CIMLKDispersionLine dispersionLine, final String wkt, final String distance) {
      this.dispersionLine = dispersionLine;
      this.wkt = wkt;
      this.distance = distance;
    }

  }

  private static class Column extends GenericColumn<Data> {

    public Column(final String header, final Function<Data, String> function) {
      super(header, function);
    }

  }

  private static class DispersionLineColumn extends Column {

    public DispersionLineColumn(final String header, final Function<CIMLKDispersionLine, String> function) {
      super(header, data -> function.apply(data.dispersionLine));
    }

  }

  private static final Column[] COLUMNS = new Column[] {
      new DispersionLineColumn("srm1_road_id", dispersionLine -> dispersionLine.getRoadGmlId()),
      new DispersionLineColumn("calculation_point_id", dispersionLine -> dispersionLine.getCalculationPointGmlId()),
      new DispersionLineColumn("jurisdiction_id", dispersionLine -> dispersionLine.getJurisdictionId() == null
          ? ""
          : String.valueOf(dispersionLine.getJurisdictionId())),
      new DispersionLineColumn("label", CIMLKDispersionLine::getLabel),
      new DispersionLineColumn("road_profile", dispersionLine -> dispersionLine.getRoadProfile().name()),
      new DispersionLineColumn("tree_profile", dispersionLine -> dispersionLine.getTreeProfile().name()),
      new DispersionLineColumn("description", CIMLKDispersionLine::getDescription),
      new Column("distance", data -> data.distance),
      new Column("geometry", data -> data.wkt),
  };

  private static final List<GenericColumn<Data>> DEFAULT_COLUMNS = Collections.unmodifiableList(Arrays.asList(COLUMNS));

  @Override
  List<GenericColumn<Data>> columns() {
    return DEFAULT_COLUMNS;
  }

  /**
   * write row of data to the writer (does not close the writer);
   */
  public void writeRow(final Writer writer, final CIMLKDispersionLineFeature feature) throws IOException {
    final String wkt;
    final String distance;
    if (feature.getGeometry() == null) {
      wkt = "";
      distance = "";
    } else {
      final org.locationtech.jts.geom.Geometry jtsGeometry = toJtsGeometry(feature.getGeometry());
      wkt = jtsGeometry.toText();
      distance =  String.valueOf(BigDecimal.valueOf(jtsGeometry.getLength()).setScale(2, RoundingMode.HALF_UP).doubleValue());
    }
    final Data data = new Data(feature.getProperties(), wkt, distance);
    super.writeRow(writer, data);
  }

  public void write(final File outputFile, final List<CIMLKDispersionLineFeature> dispersionLines) throws IOException {
    try (final Writer writer = Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8)) {
      writeHeader(writer);
      for (final CIMLKDispersionLineFeature dispersionLine : dispersionLines) {
        writeRow(writer, dispersionLine);
      }
    }
  }

}
