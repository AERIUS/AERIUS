/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Abstract writer for CIMLK into a csv file.
 */
public abstract class AbstractCIMLKWriter<D> {
  protected static final String DOUBLE_NAN = "NaN";

  protected static class GenericColumn<T> {

    final String header;
    final Function<T, String> function;

    public GenericColumn(final String header, final Function<T, String> function) {
      this.header = header;
      this.function = function;
    }

    public String header() {
      return header;
    }

    public String value(final T data) {
      return function.apply(data);
    }

  }

  private static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withDelimiter(';');

  abstract List<GenericColumn<D>> columns();

  /**
   * write header to the writer (does not close the writer);
   */
  public void writeHeader(final Writer writer) throws IOException {
    // Not using try-with-resource with this printer, as it will close the writer as well.
    @SuppressWarnings("resource")
    final CSVPrinter printer = new CSVPrinter(writer, CSV_FORMAT);
    printer.printRecord(columns().stream().map(GenericColumn::header).collect(Collectors.toList()));
    printer.flush();
  }

  protected void writeRow(final Writer writer, final D data) throws IOException {
    // Not using try-with-resource with this printer, as it will close the writer as well.
    @SuppressWarnings("resource")
    final CSVPrinter printer = new CSVPrinter(writer, CSV_FORMAT);
    printer.printRecord(columns().stream().map(column -> column.value(data)).collect(Collectors.toList()));
    printer.flush();
  }

  protected String toWkt(final Geometry geometry) throws IOException {
    return toJtsGeometry(geometry).toText();
  }

  protected org.locationtech.jts.geom.Geometry toJtsGeometry(final Geometry geometry) throws IOException {
    try {
      return GeometryUtil.getGeometry(geometry);
    } catch (final AeriusException e) {
      throw new IOException("Somehow ended up with incorrect geometry when writing CIMLK object", e);
    }
  }

  protected static String roundedValue(final int scale, final double result) {
    return Double.isNaN(result) ? DOUBLE_NAN : String.valueOf(BigDecimal.valueOf(result).setScale(scale, RoundingMode.HALF_UP).doubleValue());
  }
}
