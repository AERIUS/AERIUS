/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.EC_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NO2_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.O3_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.PM10_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.PM25_CONCENTRATION;

import java.util.List;
import java.util.Set;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;

/**
 * Adds background concentration data to the result set. Both creates totals (= result + background) or
 * the background value.
 */
public final class BackgroundDataAdder {

  private BackgroundDataAdder() {
    // Util class
  }

  /**
   * Adds background concentration data to calculation results and the background data field.
   *
   * @param results Concentration results to add background data
   * @param erks keys the data should be added for
   * @param windRoseTable table containing background values
   */
  public static void add(final List<AeriusResultPoint> results, final Set<EmissionResultKey> erks,
      final HasGridValue<WindRoseConcentration> windRoseTable) {
    results.forEach(r -> {
      if (r instanceof CIMLKResultPoint) {
        final CIMLKResultPoint resultPoint = (CIMLKResultPoint) r;
        final WindRoseConcentration wrc = windRoseTable.get(r.getRoundedCmX(), r.getRoundedCmY());

        add(erks, resultPoint, wrc, NO2_CONCENTRATION);
        add(erks, resultPoint, wrc, PM10_CONCENTRATION);
        add(erks, resultPoint, wrc, PM25_CONCENTRATION);
        add(erks, resultPoint, wrc, EC_CONCENTRATION);
        addOzon(resultPoint, wrc);
      }
    });
  }

  private static void add(final Set<EmissionResultKey> erks, final CIMLKResultPoint r, final WindRoseConcentration wrc, final EmissionResultKey key) {
    if (erks.contains(key)) {
      final Substance substance = key.getSubstance();

      r.getBackgroundConcentrations().put(key, wrc.getBackground(substance));
      r.getGcnConcentrations().put(key, wrc.getGCN(substance));
      r.getCHwnConcentrations().put(key, wrc.getCHwn(substance));
      final double srm12;

      if (key == NO2_CONCENTRATION) {
        srm12 = safeNaN(r.getEmissionResult(key));
      } else {
        srm12 = safeNaN(r.getSrm2Results().get(key)) + safeNaN(r.getSrm1Results().get(key));
      }
      r.setEmissionResult(key, srm12 + wrc.getBackground(substance));
    }
  }

  private static double safeNaN(final double value) {
    return Double.isNaN(value) ? 0.0 : value;
  }

  private static void addOzon(final CIMLKResultPoint r, final WindRoseConcentration wrc) {
    r.getBackgroundConcentrations().put(O3_CONCENTRATION, wrc.getBackground(Substance.O3));
    r.getGcnConcentrations().put(O3_CONCENTRATION, wrc.getGCN(Substance.O3));
    r.getCHwnConcentrations().put(O3_CONCENTRATION, wrc.getCHwn(Substance.O3));
  }
}
