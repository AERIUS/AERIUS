/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.worker.ConfigurationValidator;
import nl.overheid.aerius.worker.EngineWorkerConfigurationBuilder;
import nl.overheid.aerius.worker.ModelDataUtilPreloader;

/**
 * Builder for {@link SRMConfiguration}.
 */
public class SRMConfigurationBuilder extends EngineWorkerConfigurationBuilder<AeriusSRMVersion, SRMConfiguration> {
  private static final String SRM_WORKER_PREFIX = "srm";

  private static final String SRM_THEMES = "themes";
  private static final String SRM2_WORKER_CONNECTION_TIMEOUT = "connection_timeout";
  private static final String SRM2_WORKER_FORCE_CPU = "force_cpu";

  private final List<String> validations = new ArrayList<>();
  private final Map<Theme, SRM2CalculationOptions> options;

  public SRMConfigurationBuilder(final Properties properties, final ModelDataUtilPreloader<AeriusSRMVersion, SRMConfiguration> preloader,
      final Map<Theme, SRM2CalculationOptions> options) {
    super(properties, SRM_WORKER_PREFIX, preloader, true);
    this.options = options;
  }

  @Override
  protected SRMConfiguration create() {
    return new SRMConfiguration(workerProperties.getProcesses(), getThemesConfigurations());
  }

  @Override
  protected void buildEngine(final SRMConfiguration configuration) {
    configuration.setForceCpu(workerProperties.getPropertyBooleanSafe(SRM2_WORKER_FORCE_CPU));
    configuration.setConnectionTimeout(workerProperties.getPropertyIntSafe(SRM2_WORKER_CONNECTION_TIMEOUT));
  }

  private Map<Theme, SRM2CalculationOptions> getThemesConfigurations() {
    final String themesPropertyValue = workerProperties.getPropertyOrDefault(SRM_THEMES, "");
    final String[] rawThemes = themesPropertyValue.split(",");
    final Map<Theme, SRM2CalculationOptions> filteredOptions = Stream.of(rawThemes).map(rw -> Theme.fromKey(rw.strip())).filter(Optional::isPresent)
        .map(t -> options.get(t.get())).filter(Objects::nonNull).collect(Collectors.toMap(SRM2CalculationOptions::getTheme, Function.identity()));

    if (rawThemes.length != filteredOptions.size()) {
      validations.add(String.format("Not all themes could be parsed in %s configuration: '%s', found: %s",
          workerProperties.getFullPropertyName(SRM_THEMES), themesPropertyValue, filteredOptions.keySet()));
    }
    return filteredOptions;
  }

  @Override
  protected List<String> validate(final SRMConfiguration configuration) {
    validations.addAll(preloadWithValidations(configuration, new ConfigurationValidator()));
    return validations;
  }

  @Override
  protected AeriusSRMVersion getVersionByLabel(final String version) {
    return AeriusSRMVersion.getByVersionLabel(version);
  }
}
