/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.backgrounddata.DepositionVelocityReader;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.domain.SRMConfiguration;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;

/**
 * Builder class to load all pre-srm related data.
 * This includes:
 * <ul>
 * <li>Pre-processed Wind Rose factors and speeds</li>
 * <li>Pre-processed Wind Rose concentration O3 and NH3 background for specific years</li>
 * <li>Land use</li>
 * <li>Deposition velocity data</li>
 * </ul>
 */
public class PreSRMDataBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(PreSRMDataBuilder.class);

  public PreSRMData build(final SRMConfiguration config) throws IOException {
    final PreSRMData data = new PreSRMData();

    for (final AeriusSRMVersion version : config.getModelPreloadVersions()) {
      final SRM2CalculationOptions options = config.getThemeConfiguration(version.getTheme());
      final File preSRMDirectory = config.getAeriusPreSRMDirectory(version).getAbsoluteFile();

      loadWindRoseSpeedData(version, config, preSRMDirectory, options, data);
      loadConcentrationData(version, config, options, preSRMDirectory, data);
      loadWindSpeedData(version, config.getWindVeldenDirectory(version), options, data);
      loadLandUseData(version, data, config.getLandUseDirectory(version).getAbsoluteFile());

      if (options.isCalculateDeposition()) {
        loadDepositionVelocities(version, data, config.getDepositionVelocityFile(version));
      }
    }

    return data;
  }

  /**
   * Loads into PreSRMData object if not yet present. If the data files are not yet available and downloading is configured it will
   * downloads the requested data files and then load them.
   *
   * @param preSRMData data object to fill with data
   * @param config configuration containing the data file versions.
   * @param srmVersion
   * @throws IOException
   */
  public static synchronized void loadAdditionalData(final PreSRMData preSRMData, final SRMConfiguration config,
      final AeriusSRMVersion srmVersion) throws IOException {
    final SRM2CalculationOptions options = config.getThemeConfiguration(srmVersion.getTheme());

    if (!preSRMData.hasWindRoseSpeedData(srmVersion)) {
      loadWindRoseSpeedData(srmVersion, config, config.getAeriusPreSRMDirectory(srmVersion), options, preSRMData);
    }

    if (!preSRMData.hasWindRoseConcentrationData(srmVersion)) {
      loadConcentrationData(srmVersion, config, options, config.getAeriusPreSRMDirectory(srmVersion), preSRMData);
    }

    if (!preSRMData.hasWindSpeedData(srmVersion)) {
      loadWindSpeedData(srmVersion, config.getWindVeldenDirectory(srmVersion), options, preSRMData);
    }

    if (!preSRMData.hasLandUseData(srmVersion)) {
      loadLandUseData(srmVersion, preSRMData, config.getLandUseDirectory(srmVersion));
    }

    if (options.isCalculateDeposition() && !preSRMData.hasDepositionVelocity(srmVersion)) {
      loadDepositionVelocities(srmVersion, preSRMData, config.getDepositionVelocityFile(srmVersion));
    }
  }

  private static void loadWindRoseSpeedData(final AeriusSRMVersion aeriusSRMVersion, final SRMConfiguration config, final File preSRMDirectory,
      final SRM2CalculationOptions options, final PreSRMData data) throws IOException {
    final WindRoseSpeedDataBuilder windRoseSpeedBuilder = new WindRoseSpeedDataBuilder(options);

    windRoseSpeedBuilder.loadWindRoseSpeedPrognose(aeriusSRMVersion, data, preSRMDirectory);
    windRoseSpeedBuilder.loadWindRoseByYear(aeriusSRMVersion, data, preSRMDirectory);
  }

  private static void loadWindSpeedData(final AeriusSRMVersion aeriusSRMVersion, final File windDirectory, final SRM2CalculationOptions options,
      final PreSRMData data) throws IOException {
    if (options.isCalculateSRM1()) {
      final WindSpeedDataBuilder builder = new WindSpeedDataBuilder();

      builder.loadWindSpeedPrognose(aeriusSRMVersion, data, windDirectory);
      builder.loadWindSpeedByYear(aeriusSRMVersion, data, windDirectory);
    }
  }

  private static void loadConcentrationData(final AeriusSRMVersion aeriusSRMVersion, final SRMConfiguration config,
      final SRM2CalculationOptions options, final File preSRMDirectory, final PreSRMData data) throws IOException {
    final WindRoseConcentrationBuilder windRoseConBuilder = new WindRoseConcentrationBuilder();

    windRoseConBuilder.loadWindRoseConcentration(aeriusSRMVersion, data, preSRMDirectory, options);
  }

  private static void loadLandUseData(final AeriusSRMVersion aeriusSRMVersion, final PreSRMData data, final File landUseDirectory)
      throws IOException {
    LOG.info("Loading landUse data from: {}.", landUseDirectory);
    data.setLandUseData(aeriusSRMVersion, LandUseDataBuilder.loadZ0(landUseDirectory));
  }

  private static void loadDepositionVelocities(final AeriusSRMVersion aeriusSRMVersion, final PreSRMData data, final File depositionVelocityFilepath)
      throws IOException {
    LOG.info("Loading deposition velocity data from {}.", depositionVelocityFilepath);
    try (final InputStream is = new FileInputStream(depositionVelocityFilepath)) {
      data.setDepositionVelocityMaps(aeriusSRMVersion,
          DepositionVelocityReader.read(is, SRMConstants.HEX_HOR, SRMConstants.getReceptorUtil(), SRMDepositionVelocityMap.class));
    }
  }
}
