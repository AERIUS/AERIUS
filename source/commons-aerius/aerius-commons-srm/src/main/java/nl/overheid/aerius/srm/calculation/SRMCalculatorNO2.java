/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import java.util.Collection;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;

/**
 * Algorithm to calculate NO2 when calculating SRM1 and SRM2 sources as described in:
 *
 * <pre>
 * Technische beschrijving van standaardrekenmethode 1 (SRM-1)
 * RIVM Briefrapport 2014-0127
 * K. van Velze, J. Wesseling
 * </pre>
 * @see <a href="https://www.rivm.nl/bibliotheek/rapporten/2014-0127.pdf">https://www.rivm.nl/bibliotheek/rapporten/2014-0127.pdf</a>
 */
public class SRMCalculatorNO2 {

  private static final double SRM1_RELEVANCY_THRESHOLD = 0.049;
  private static final double K = 100.0;
  private static final double B = 0.6;

  /**
   * Calculated the aggregated NO2 results of SRM1 and SRM2 if there are SRM1 results.
   *
   * @param results results to recalculate
   * @param hasNO2 Only run if has NO2 results
   * @param windRoseTable table of background data
   */
  public void recalculateNO2(final Collection<AeriusResultPoint> results, final boolean hasNO2,
      final HasGridValue<WindRoseConcentration> windRoseTable) {
    if (hasNO2) {
      results.forEach(r -> {
        if (r instanceof CIMLKResultPoint) {
          final CIMLKResultPoint result = (CIMLKResultPoint) r;
          final double cNO2 = calculateNO2(result.getSrm1Results(), result.getSrm2Results(),
              windRoseTable.get(result.getRoundedCmX(), result.getRoundedCmY()).getBackgroundO3());

          result.setEmissionResult(EmissionResultKey.NO2_CONCENTRATION, cNO2);
        }
      });
    }
  }

  private double calculateNO2(final EmissionResults srm1Result, final EmissionResults srm2Result, final double cajmO3) {
    final double cbjmSRM1NOx = getEmissionResult(srm1Result, EmissionResultKey.NOX_CONCENTRATION);
    final double cbjmSRM2NOx = getEmissionResult(srm2Result, EmissionResultKey.NOX_CONCENTRATION);

    if (cbjmSRM1NOx > SRM1_RELEVANCY_THRESHOLD) {
      if (cbjmSRM2NOx > 0.0) {
        return cbjmtotaalNO2(getEmissionResult(srm1Result, EmissionResultKey.NO2_DIRECT_CONCENTRATION), cbjmSRM1NOx,
            getEmissionResult(srm2Result, EmissionResultKey.NO2_DIRECT_CONCENTRATION), cbjmSRM2NOx, cajmO3);
      } else {
        return getEmissionResult(srm1Result, EmissionResultKey.NO2_CONCENTRATION);
      }
    } else {
      return getEmissionResult(srm2Result, EmissionResultKey.NO2_CONCENTRATION);
    }
  }

  private double getEmissionResult(final EmissionResults result, final EmissionResultKey key) {
    return result.get(key);
  }

  double fNO2(final double emissieNO2, final double emissieNOx) {
    return emissieNOx > 0.0 ? (emissieNO2 / emissieNOx) : 0.0;
  }

  // 1.18a en 1.18b
  double cbjmSRMxNO(final double fNO2, final double cbjmNOx) {
    return (1 - fNO2) * cbjmNOx;
  }

  // 1.18c
  double cbjmSRM2eqNO(final double fNO2, final double cbjmSRM2NOx) {
    final double cbjmSRM2NO = cbjmSRMxNO(fNO2, cbjmSRM2NOx);
    final double ε = epsilon(cbjmSRM2NO);
    return (ε * K) / (1 - ε);
  }

  // 1.18d
  double epsilon(final double cbjmSRM2NO) {
    return cbjmSRM2NO / ((cbjmSRM2NO + K) * B);
  }

  // 1.18e
  double cbjmtotaalNO(final double fSRM1NO2, final double cbjmSRM1NOx, final double fSRM2NO2, final double cbjmSRM2NOx) {
    return cbjmSRMxNO(fSRM1NO2, cbjmSRM1NOx) + cbjmSRM2eqNO(fSRM2NO2, cbjmSRM2NOx);
  }

  // 1.18f
  double ccbjmNO2(final double cajmO3, final double cbjmtotaalNO) {
    return (B * cajmO3 * cbjmtotaalNO) / (cbjmtotaalNO + K);
  }

  // 1.18g
  double cbjmtotaalNO2(final double cbjmSRM1NO2Direct, final double cbjmSRM1NOx, final double cbjmSRM2NO2Direct, final double cbjmSRM2NOx,
      final double cajmO3) {
    final double fSRM1NO2 = cbjmSRM1NO2Direct / cbjmSRM1NOx;
    final double fSRM2NO2 = cbjmSRM2NO2Direct / cbjmSRM2NOx;
    final double cbjmtotaalNO = cbjmtotaalNO(fSRM1NO2, cbjmSRM1NOx, fSRM2NO2, cbjmSRM2NOx);
    final double ccbjmNO2 = ccbjmNO2(cajmO3, cbjmtotaalNO);
    return cbjmSRM1NO2Direct + cbjmSRM2NO2Direct + ccbjmNO2;
  }
}
