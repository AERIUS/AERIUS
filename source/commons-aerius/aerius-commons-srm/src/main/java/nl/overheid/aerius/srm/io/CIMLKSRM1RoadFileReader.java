/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadManager;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Reader for CIMLK SRM1 road files.
 *
 * Available columns:
 * <pre>
 * srm1_road_id;jurisdiction_id;label;road_manager;speed_profile;tunnel_factor;
 * light_traffic_intensity;light_traffic_stagnation_factor;
 * normal_freight_intensity;normal_freight_stagnation_factor;
 * heavy_freight_intensity;heavy_freight_stagnation_factor;
 * auto_bus_intensity;auto_bus_stagnation_factor;
 * description;geometry
 * </pre>
 */
class CIMLKSRM1RoadFileReader extends AbstractCIMLKFileReader<EmissionSourceFeature> {

  // @formatter:off
  private enum SRM1RoadColumns implements Columns {
    SRM1_ROAD_ID,
    JURISDICTION_ID,
    LABEL,
    ROAD_MANAGER,
    SPEED_PROFILE,
    TUNNEL_FACTOR,
    LIGHT_TRAFFIC_INTENSITY,
    LIGHT_TRAFFIC_STAGNATION_FACTOR,
    NORMAL_FREIGHT_INTENSITY,
    NORMAL_FREIGHT_STAGNATION_FACTOR,
    HEAVY_FREIGHT_INTENSITY,
    HEAVY_FREIGHT_STAGNATION_FACTOR,
    AUTO_BUS_INTENSITY,
    AUTO_BUS_STAGNATION_FACTOR,
    DESCRIPTION,
    GEOMETRY;
    // @formatter:on

    static SRM1RoadColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  private final SectorCategories categories;

  /**
   * @param categories The context to be used for determining road emission categories.
   */
  public CIMLKSRM1RoadFileReader(final SectorCategories categories) {
    this.categories = categories;
  }

  @Override
  protected EmissionSourceFeature parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final SRM1RoadEmissionSource road = new SRM1RoadEmissionSource();

    final String roadId = getGmlId(SRM1RoadColumns.SRM1_ROAD_ID, GMLIdUtil.SOURCE_PREFIX, warnings);
    road.setGmlId(roadId);
    feature.setId(roadId);

    road.setJurisdictionId(getInt(SRM1RoadColumns.JURISDICTION_ID));
    road.setLabel(getString(SRM1RoadColumns.LABEL));
    road.setRoadManager(getNullableEnumValue(RoadManager.class, SRM1RoadColumns.ROAD_MANAGER, road.getGmlId()));
    road.setDescription(getString(SRM1RoadColumns.DESCRIPTION));

    final RoadSpeedType speedProfile = getEnumValue(RoadSpeedType.class, SRM1RoadColumns.SPEED_PROFILE, road.getGmlId());
    road.setSectorId(ROAD_SECTOR_ID);
    road.setRoadAreaCode(DEFAULT_AREA_CODE);
    road.setRoadTypeCode(speedProfile.getRoadTypeCode());
    //Store the sector id as ParticleSizeDistribution, needed for OPS (Roads should always be calculated with SRM, but you never know...)
    final Sector sector = categories.getSectorById(ROAD_SECTOR_ID);
    final OPSSourceCharacteristics characteristics = sector.getDefaultCharacteristics().copyTo(new OPSSourceCharacteristics());
    characteristics.setParticleSizeDistribution(ROAD_SECTOR_ID);
    road.setCharacteristics(characteristics);

    parseTunnelFactor(road, warnings);

    parseAllTraffic(road, speedProfile.getRoadTypeCode());

    parseGeometry(feature);

    feature.setProperties(road);
    return feature;
  }

  private void parseTunnelFactor(final SRM1RoadEmissionSource road, final List<AeriusException> warnings) throws AeriusException {
    road.setTunnelFactor(getDouble(SRM1RoadColumns.TUNNEL_FACTOR));
    if (road.getTunnelFactor() < 0) {
      throw new AeriusException(ImaerExceptionReason.SRM2_SOURCE_TUNNEL_FACTOR_ZERO, String.valueOf(road.getTunnelFactor()), road.getLabel());
    }
  }

  private void parseAllTraffic(final SRM1RoadEmissionSource road, final String roadType) throws AeriusException {
    // Max speed doesn't matter for SRM1 categories
    final int maxSpeed = 0;
    final StandardVehicles traffic = new StandardVehicles();
    traffic.setMaximumSpeed(maxSpeed);
    traffic.setStrictEnforcement(false);
    traffic.setTimeUnit(TimeUnit.DAY);

    parseTraffic(traffic, roadType,
        VehicleType.LIGHT_TRAFFIC.getStandardVehicleCode(),
        SRM1RoadColumns.LIGHT_TRAFFIC_INTENSITY,
        SRM1RoadColumns.LIGHT_TRAFFIC_STAGNATION_FACTOR);
    parseTraffic(traffic, roadType,
        VehicleType.NORMAL_FREIGHT.getStandardVehicleCode(),
        SRM1RoadColumns.NORMAL_FREIGHT_INTENSITY,
        SRM1RoadColumns.NORMAL_FREIGHT_STAGNATION_FACTOR);
    parseTraffic(traffic, roadType,
        VehicleType.HEAVY_FREIGHT.getStandardVehicleCode(),
        SRM1RoadColumns.HEAVY_FREIGHT_INTENSITY,
        SRM1RoadColumns.HEAVY_FREIGHT_STAGNATION_FACTOR);
    parseTraffic(traffic, roadType,
        VehicleType.AUTO_BUS.getStandardVehicleCode(),
        SRM1RoadColumns.AUTO_BUS_INTENSITY,
        SRM1RoadColumns.AUTO_BUS_STAGNATION_FACTOR);
    road.getSubSources().add(traffic);
  }

  /**
   * Set the source data for specific traffic types.
   * @param traffic Object to add values to
   * @param roadType type of the road
   * @param vehicleType vehicle type
   * @param intensityColumn number of vehicles
   * @param stagnationColumn stagnation factor
   */
  private void parseTraffic(final StandardVehicles traffic, final String roadType,
      final String vehicleType, final SRM1RoadColumns intensityColumn, final SRM1RoadColumns stagnationColumn) throws AeriusException {
    // Max speed doesn't matter for SRM1 categories
    final int maxSpeed = 0;

    final RoadEmissionCategory roadEmissionCategory = categories.getRoadEmissionCategories().findClosestCategory(DEFAULT_AREA_CODE, roadType,
        vehicleType, Boolean.FALSE, maxSpeed, null);

    if (roadEmissionCategory == null) {
      throw new AeriusException(AeriusExceptionReason.SRM2IO_EXCEPTION_NO_ROAD_PROPERTIES, String.valueOf(getCurrentLineNumber()),
          getCurrentColumnContent(), roadType + "," + vehicleType + "," + maxSpeed);
    }

    final double intensity = getDouble(intensityColumn);

    if (intensity > 0 || VehicleType.LIGHT_TRAFFIC.getStandardVehicleCode().equalsIgnoreCase(vehicleType)) {
      final ValuesPerVehicleType valuesPerVehicleType = new ValuesPerVehicleType();
      valuesPerVehicleType.setVehiclesPerTimeUnit(intensity);
      if (stagnationColumn != null) {
        valuesPerVehicleType.setStagnationFraction(getDouble(stagnationColumn));
      }

      traffic.getValuesPerVehicleTypes().put(vehicleType, valuesPerVehicleType);
    }
  }

  private void parseGeometry(final EmissionSourceFeature feature) throws AeriusException {
    final Geometry wktGeometry = getGeometry();
    feature.setGeometry(wktGeometry);
  }

  private Geometry getGeometry() throws AeriusException {
    return getGeometry(SRM1RoadColumns.GEOMETRY, geom -> geom.type() != GeometryType.LINESTRING);
  }

  @Override
  Columns[] expectedColumns() {
    return SRM1RoadColumns.values();
  }

  @Override
  Columns safeColumnOf(final String value) {
    return SRM1RoadColumns.safeValueOf(value);
  }

}
