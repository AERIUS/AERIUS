/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import java.util.List;
import java.util.Map.Entry;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;

/**
 * Applies the user corrections to the result points and adds the correction value to the result point.
 */
public class CorrectionsCalculator {

  /**
   * Applies the corrections to the given result points.
   *
   * @param results results to apply corrections to
   * @param corrections corrections to apply.
   */
  public void applyCorrections(final List<AeriusResultPoint> results, final List<CIMLKCorrection> corrections) {
    if (corrections != null) {
      results.stream().forEach(result -> applyCorrections(result, corrections));
    }
  }

  private void applyCorrections(final AeriusResultPoint resultPoint, final List<CIMLKCorrection> corrections) {
    corrections.stream()
        .filter(correction -> correction.getCalculationPointGmlId().equals(resultPoint.getGmlId()))
        .forEach(correction -> applyCorrection(correction, resultPoint.getEmissionResults(), resultPoint));
  }

  private void applyCorrection(final CIMLKCorrection correction, final EmissionResults results,
      final AeriusResultPoint resultPoint) {
    results.getHashMap().entrySet().stream()
        .filter(entry -> correction.shouldApplyTo(entry.getKey()))
        .forEach(entry -> applyCorrection(correction, entry, resultPoint));
  }

  private void applyCorrection(final CIMLKCorrection correction, final Entry<EmissionResultKey, Double> resultsEntry,
      final AeriusResultPoint resultPoint) {
    final double correctionValue = correction.getValue();
    resultsEntry.setValue(Math.max(0.0, resultsEntry.getValue() + correctionValue));

    if (resultPoint instanceof CIMLKResultPoint) {
      final CIMLKResultPoint CIMLKResultPoint = (CIMLKResultPoint) resultPoint;
      final EmissionResultKey emissionKey = resultsEntry.getKey();
      final double existingCorrection = CIMLKResultPoint.getUserCorrections().get(emissionKey);
      CIMLKResultPoint.getUserCorrections().put(emissionKey, existingCorrection + correctionValue);
    }
  }

}
