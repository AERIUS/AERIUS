/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import java.math.BigDecimal;
import java.math.RoundingMode;

import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Calculates the PM10 exceedance days or:
 * aantal overschrijdingen grenswaarde vierentwintig-uurgemiddelde concentratie zwevende deeltjes
 */
public final class ExceedanceDaysCalculator {

  private static final double UPPER_BOUNDARY_31_2 = 31.2;
  private static final double LOWER_BOUNDARY_16 = 16.0;

  private static final double UC_4_6128 = 4.6128;
  private static final double UC_108_92 = 108.92;

  private static final double CC_0_13401 = 0.13401;
  private static final double CC_31_2 = 31.2;
  private static final double CC_3_9427 = 3.9427;
  private static final double CC_35 = 35;

  private static final double LC_6 = 6;

  private ExceedanceDaysCalculator() {
    // util like class.
  }

  /**
   * @param cjmPM10 The concentration of PM10 (including background) to convert (should be in microgram/m^3).
   * @return the number of excess days (estimate of number of days the concentration breaks the limit).
   */
  public static double calculate(final double cjmPM10) {
    if (Double.isNaN(cjmPM10)) {
      return Double.NaN;
    }
    final double odPM10;

    if (cjmPM10 > UPPER_BOUNDARY_31_2) {
      odPM10 = UC_4_6128 * cjmPM10 - UC_108_92;
    } else if (cjmPM10 < LOWER_BOUNDARY_16) {
      odPM10 = LC_6;
    } else {
      odPM10 = CC_0_13401 * Math.pow(cjmPM10 - CC_31_2, 2) + CC_3_9427 * (cjmPM10 - CC_31_2) + CC_35;
    }

    return Math.min(round(odPM10), ImaerConstants.DAYS_PER_YEAR);
  }

  /**
   * Round 2 decimals.
   *
   * @param odPM10 pm10 concentration
   * @return rounded value
   */
  private static double round(final double odPM10) {
    return BigDecimal.valueOf(odPM10).setScale(SRMConstants.PM10_EXCEEDANCE_DAYS_SCALE, RoundingMode.HALF_UP).doubleValue();
  }
}
