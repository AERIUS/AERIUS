/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Stores the result of a list of {@link CIMLKResultPoint}s in a csv file.
 */
public final class CIMLKResultWriter extends AbstractCIMLKWriter<CIMLKResultWriter.Data> {

  protected static class Data {

    final int scale;
    final int year;
    final String aeriusVersion;
    final String databaseVersion;
    final CIMLKResultPoint resultPoint;

    Data(final int scale, final int year, final String aeriusVersion, final String databaseVersion, final CIMLKResultPoint resultPoint) {
      this.scale = scale;
      this.year = year;
      this.aeriusVersion = aeriusVersion;
      this.databaseVersion = databaseVersion;
      this.resultPoint = resultPoint;
    }

  }

  private static class Column extends GenericColumn<Data> {

    public Column(final String header, final Function<Data, String> function) {
      super(header, function);
    }

  }

  private static class PointColumn extends Column {

    public PointColumn(final String header, final Function<CIMLKResultPoint, String> function) {
      super(header, data -> function.apply(data.resultPoint));
    }

  }

  private static class ResultColumn extends Column {

    public ResultColumn(final String headerAffix, final EmissionResultKey key, final BiFunction<CIMLKResultPoint, EmissionResultKey, Double> function) {
      super(key.getSubstance().getName() + "_" + headerAffix, data -> roundedValue(data.scale, function.apply(data.resultPoint, key)));
    }

  }

  private static class TotalConcentrationResultColumn extends ResultColumn {

    public TotalConcentrationResultColumn(final EmissionResultKey key) {
      super("total_concentration", key, (result, someKey) -> result.getEmissionResult(someKey));
    }

  }

  private static class BackgroundConcentrationResultColumn extends ResultColumn {

    public BackgroundConcentrationResultColumn(final EmissionResultKey key) {
      super("background_concentration", key, (result, someKey) -> result.getBackgroundConcentrations().get(someKey));
    }

  }

  private static class GCNConcentrationResultColumn extends ResultColumn {

    public GCNConcentrationResultColumn(final EmissionResultKey key) {
      super("gcn", key, (result, someKey) -> result.getGcnConcentrations().get(someKey));
    }
  }

  private static class SRM2ConcentrationResultColumn extends ResultColumn {

    public SRM2ConcentrationResultColumn(final EmissionResultKey key) {
      super("SRM2_concentration" + (key.getEmissionResultType() == EmissionResultType.DIRECT_CONCENTRATION ? "_direct" : ""),
          key, (result, someKey) -> result.getSrm2Results().get(someKey));
    }

  }

  private static class SRM1ConcentrationResultColumn extends ResultColumn {

    public SRM1ConcentrationResultColumn(final EmissionResultKey key) {
      super("SRM1_concentration" + (key.getEmissionResultType() == EmissionResultType.DIRECT_CONCENTRATION ? "_direct" : ""),
          key, (result, someKey) -> result.getSrm1Results().get(someKey));
    }

  }

  private static class MainRoadsBackgroundCorrectionResultColumn extends ResultColumn {

    public MainRoadsBackgroundCorrectionResultColumn(final EmissionResultKey key) {
      super("GCN_main_roads_correction", key, (result, someKey) -> result.getCHwnConcentrations().get(someKey));
    }

  }

  private static class NO2ConvertedResultColumn extends Column {

    public NO2ConvertedResultColumn() {
      super("NO2_SRM_concentration_converted", data -> roundedValue(data.scale, getNo2Converted(data.resultPoint)));
    }

    private static double getNo2Converted(final CIMLKResultPoint resultPoint) {
      return resultPoint.getEmissionResult(EmissionResultKey.NO2_CONCENTRATION)
          - resultPoint.getBackgroundConcentrations().get(EmissionResultKey.NO2_CONCENTRATION)
          - resultPoint.getSrm1Results().get(EmissionResultKey.NO2_DIRECT_CONCENTRATION)
          - resultPoint.getSrm2Results().get(EmissionResultKey.NO2_DIRECT_CONCENTRATION)
          - resultPoint.getUserCorrections().get(EmissionResultKey.NO2_CONCENTRATION);
    }

  }

  private static class UserCorrectionConcentrationResultColumn extends ResultColumn {

    public UserCorrectionConcentrationResultColumn(final EmissionResultKey key) {
      super("user_correction", key, (result, someKey) -> result.getUserCorrections().get(someKey));
    }

  }

  public static final List<GenericColumn<Data>> DEFAULT_COLUMNS = Collections.unmodifiableList(Arrays.asList(
      new PointColumn("calculation_point_id", AeriusPoint::getGmlId),
      new Column("calculation_year", data -> String.valueOf(data.year)),
      new Column("aerius_version", data -> data.aeriusVersion),
      new Column("aerius_database_version", data -> data.databaseVersion),
      new PointColumn("label", AeriusPoint::getLabel),
      new PointColumn("monitor_substance", point -> point.getMonitorSubstance() == null ? "" : point.getMonitorSubstance().name()),
      new PointColumn("geometry", SridPoint::toUnroundedWKT),
      new TotalConcentrationResultColumn(EmissionResultKey.NO2_CONCENTRATION),
      new TotalConcentrationResultColumn(EmissionResultKey.PM10_CONCENTRATION),
      new TotalConcentrationResultColumn(EmissionResultKey.PM25_CONCENTRATION),
      new TotalConcentrationResultColumn(EmissionResultKey.EC_CONCENTRATION),
      new PointColumn("NO2_exceedance_hours", CIMLKResultWriter::exceedanceHours),
      new PointColumn("PM10_exceedance_days",
          result -> roundedValue(SRMConstants.PM10_EXCEEDANCE_DAYS_SCALE, result.getEmissionResult(EmissionResultKey.PM10_EXCEEDANCE_DAYS))),
      new BackgroundConcentrationResultColumn(EmissionResultKey.NO2_CONCENTRATION),
      new BackgroundConcentrationResultColumn(EmissionResultKey.PM10_CONCENTRATION),
      new BackgroundConcentrationResultColumn(EmissionResultKey.PM25_CONCENTRATION),
      new BackgroundConcentrationResultColumn(EmissionResultKey.EC_CONCENTRATION),
      new GCNConcentrationResultColumn(EmissionResultKey.NO2_CONCENTRATION),
      new GCNConcentrationResultColumn(EmissionResultKey.PM10_CONCENTRATION),
      new GCNConcentrationResultColumn(EmissionResultKey.PM25_CONCENTRATION),
      new GCNConcentrationResultColumn(EmissionResultKey.EC_CONCENTRATION),
      new GCNConcentrationResultColumn(EmissionResultKey.O3_CONCENTRATION),
      new SRM2ConcentrationResultColumn(EmissionResultKey.NO2_DIRECT_CONCENTRATION),
      new SRM2ConcentrationResultColumn(EmissionResultKey.PM10_CONCENTRATION),
      new SRM2ConcentrationResultColumn(EmissionResultKey.PM25_CONCENTRATION),
      new SRM2ConcentrationResultColumn(EmissionResultKey.EC_CONCENTRATION),
      new SRM1ConcentrationResultColumn(EmissionResultKey.NO2_DIRECT_CONCENTRATION),
      new SRM1ConcentrationResultColumn(EmissionResultKey.PM10_CONCENTRATION),
      new SRM1ConcentrationResultColumn(EmissionResultKey.PM25_CONCENTRATION),
      new SRM1ConcentrationResultColumn(EmissionResultKey.EC_CONCENTRATION),
      new BackgroundConcentrationResultColumn(EmissionResultKey.O3_CONCENTRATION),
      new SRM2ConcentrationResultColumn(EmissionResultKey.NOX_CONCENTRATION),
      new SRM1ConcentrationResultColumn(EmissionResultKey.NOX_CONCENTRATION),
      new NO2ConvertedResultColumn(),
      new MainRoadsBackgroundCorrectionResultColumn(EmissionResultKey.NO2_CONCENTRATION),
      new MainRoadsBackgroundCorrectionResultColumn(EmissionResultKey.PM10_CONCENTRATION),
      new MainRoadsBackgroundCorrectionResultColumn(EmissionResultKey.PM25_CONCENTRATION),
      new MainRoadsBackgroundCorrectionResultColumn(EmissionResultKey.EC_CONCENTRATION),
      new MainRoadsBackgroundCorrectionResultColumn(EmissionResultKey.O3_CONCENTRATION),
      new UserCorrectionConcentrationResultColumn(EmissionResultKey.NO2_CONCENTRATION),
      new UserCorrectionConcentrationResultColumn(EmissionResultKey.PM10_CONCENTRATION),
      new UserCorrectionConcentrationResultColumn(EmissionResultKey.PM25_CONCENTRATION),
      new UserCorrectionConcentrationResultColumn(EmissionResultKey.EC_CONCENTRATION),
      new UserCorrectionConcentrationResultColumn(EmissionResultKey.O3_CONCENTRATION)
      ));

  private final int scale;

  public CIMLKResultWriter(final int scale) {
    this.scale = scale;
  }

  private static String exceedanceHours(final CIMLKResultPoint result) {
    final double exceedanceHours = result.getEmissionResult(EmissionResultKey.NO2_EXCEEDANCE_HOURS);

    return Double.isNaN(exceedanceHours) ? DOUBLE_NAN : String.valueOf((int) exceedanceHours);
  }

  @Override
  List<GenericColumn<Data>> columns() {
    return DEFAULT_COLUMNS;
  }

  /**
   * write row of data to the writer (does not close the writer);
   */
  public void writeRow(final Writer writer, final AeriusResultPoint resultPoint,
      final int calculationYear, final String aeriusVersion, final String databaseVersion) throws IOException {
    if (resultPoint instanceof CIMLKResultPoint) {
      final Data data = new Data(scale, calculationYear, aeriusVersion, databaseVersion, (CIMLKResultPoint) resultPoint);
      super.writeRow(writer, data);
    }
  }

  public void write(final File outputFile, final List<AeriusResultPoint> results,
      final int calculationYear, final String aeriusVersion, final String databaseVersion) throws IOException {
    try (Writer writer = Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8)) {
      writeHeader(writer);
      for (final AeriusResultPoint result : results) {
        writeRow(writer, result, calculationYear, aeriusVersion, databaseVersion);
      }
    }
  }

}
