/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.backgrounddata.LandUseData.LandUseType;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;

/**
 * Data object containing the PRE-SRM calculated data.
 */
public class PreSRMData {
  private static final Logger LOGGER = LoggerFactory.getLogger(PreSRMData.class);

  private final Map<String, Map<Integer, HasGridValue<WindRoseConcentration>>> windRoseConcentrationYearGrid = new HashMap<>();
  private final Map<String, Map<Integer, HasGridValue<WindRoseConcentration>>> windRoseConcentrationPrognoseGrid = new HashMap<>();
  private final Map<String, Map<Integer, WindRoseSpeedGrid>> windRoseSpeedYearGrid = new HashMap<>();
  private final Map<String, WindRoseSpeedGrid> windRoseSpeedPrognoseGrid = new HashMap<>();
  private final Map<String, Map<Integer, WindSpeedGrid>> windSpeedYearGrid = new HashMap<>();
  private final Map<String, WindSpeedGrid> windSpeedPrognoseGrid = new HashMap<>();
  private final Map<String, LandUseData> landUseData = new HashMap<>();
  private final Map<String, Map<Substance, SRMDepositionVelocityMap>> depositionVelocityMaps = new HashMap<>();

  void addWindSpeedGrid(final AeriusSRMVersion aeriusSRMVersion, final int year, final WindSpeedGrid table) {
    windSpeedYearGrid.computeIfAbsent(aeriusSRMVersion.getAeriusPreSrmVersion(), key -> new HashMap<>()).put(year, table);
  }

  void addWindRoseSpeedTable(final AeriusSRMVersion aeriusSRMVersion, final int year, final WindRoseSpeedGrid table) {
    windRoseSpeedYearGrid.computeIfAbsent(aeriusSRMVersion.getAeriusPreSrmVersion(), key -> new HashMap<>()).put(year, table);
  }

  public WindSpeedGrid getWindSpeedGrid(final AeriusSRMVersion aeriusSRMVersion, final Meteo meteo) throws AeriusException {
    final String aeriusPreSrmVersion = aeriusSRMVersion.getAeriusPreSrmVersion();

    if (meteo.isSingleYear()) {
      final int meteoYear = meteo.getStartYear();
      if (windSpeedYearGrid.containsKey(aeriusPreSrmVersion) && windSpeedYearGrid.get(aeriusPreSrmVersion).containsKey(meteoYear)) {
        return windSpeedYearGrid.get(aeriusPreSrmVersion).get(meteoYear);
      }
      LOGGER.error("No windspeed table for year {}", meteoYear);
    } else if (windSpeedYearGrid.containsKey(aeriusPreSrmVersion)) {
      return windSpeedPrognoseGrid.get(aeriusPreSrmVersion);
    } else {
      LOGGER.error("No windspeed prognose grid for version {}", aeriusSRMVersion);
    }
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  public boolean hasWindRoseSpeedData(final AeriusSRMVersion aeriusSRMVersion) {
    final String aeriusPreSrmVersion = aeriusSRMVersion.getAeriusPreSrmVersion();

    return windRoseSpeedYearGrid.containsKey(aeriusPreSrmVersion) && windRoseSpeedPrognoseGrid.containsKey(aeriusPreSrmVersion);
  }

  public WindRoseSpeedGrid getWindRoseSpeedGrid(final AeriusSRMVersion aeriusSRMVersion, final Meteo meteo) throws AeriusException {
    final String aeriusPreSrmVersion = aeriusSRMVersion.getAeriusPreSrmVersion();

    if (meteo.isSingleYear()) {
      final int meteoYear = meteo.getStartYear();
      if (windRoseSpeedYearGrid.containsKey(aeriusPreSrmVersion) && windRoseSpeedYearGrid.get(aeriusPreSrmVersion).containsKey(meteoYear)) {
        return windRoseSpeedYearGrid.get(aeriusPreSrmVersion).get(meteoYear);
      }
      LOGGER.error("No windspeed rose table for version {} and year {}", aeriusSRMVersion, meteoYear);
    } else if (windRoseSpeedPrognoseGrid.containsKey(aeriusPreSrmVersion)) {
      return windRoseSpeedPrognoseGrid.get(aeriusPreSrmVersion);
    } else {
      LOGGER.error("No wind speed rose prognose table for version {}", aeriusSRMVersion);
    }
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  public boolean hasWindSpeedData(final AeriusSRMVersion aeriusSRMVersion) {
    final String aeriusPreSrmVersion = aeriusSRMVersion.getAeriusPreSrmVersion();

    return windSpeedPrognoseGrid.containsKey(aeriusPreSrmVersion) && windSpeedYearGrid.containsKey(aeriusPreSrmVersion);
  }

  void setWindSpeedPrognoseGrid(final AeriusSRMVersion aeriusSRMVersion, final WindSpeedGrid windSpeedPrognoseGrid) {
    this.windSpeedPrognoseGrid.put(aeriusSRMVersion.getAeriusPreSrmVersion(), windSpeedPrognoseGrid);
  }

  void setWindRoseSpeedPrognoseGrid(final AeriusSRMVersion aeriusSRMVersion, final WindRoseSpeedGrid windRoseSpeedPrognoseGrid) {
    this.windRoseSpeedPrognoseGrid.put(aeriusSRMVersion.getAeriusPreSrmVersion(), windRoseSpeedPrognoseGrid);
  }

  void addWindRoseConcentrationGrid(final AeriusSRMVersion aeriusSRMVersion, final int year, final HasGridValue<WindRoseConcentration> grid) {
    windRoseConcentrationYearGrid.computeIfAbsent(aeriusSRMVersion.getAeriusPreSrmVersion(), key -> new HashMap<>()).put(year, grid);
  }

  void addWindRoseConcentrationPrognoseGrid(final AeriusSRMVersion aeriusSRMVersion, final int year, final HasGridValue<WindRoseConcentration> grid) {
    windRoseConcentrationPrognoseGrid.computeIfAbsent(aeriusSRMVersion.getAeriusPreSrmVersion(), key -> new HashMap<>()).put(year, grid);
  }

  public boolean hasWindRoseConcentrationData(final AeriusSRMVersion aeriusSRMVersion) {
    final String aeriusPreSrmVersion = aeriusSRMVersion.getAeriusPreSrmVersion();

    return windRoseSpeedYearGrid.containsKey(aeriusPreSrmVersion) && windRoseConcentrationPrognoseGrid.containsKey(aeriusPreSrmVersion);
  }

  public HasGridValue<WindRoseConcentration> getWindRoseConcentrationGrid(final AeriusSRMVersion aeriusSRMVersion, final Meteo meteo, final int year)
      throws AeriusException {
    final Map<Integer, HasGridValue<WindRoseConcentration>> grid;
    final int gridYear;

    if (meteo.isSingleYear()) {
      gridYear = meteo.getStartYear();
      grid = windRoseConcentrationYearGrid.get(aeriusSRMVersion.getAeriusPreSrmVersion());
    } else {
      grid = windRoseConcentrationPrognoseGrid.get(aeriusSRMVersion.getAeriusPreSrmVersion());
      gridYear = year;
    }
    final HasGridValue<WindRoseConcentration> yearGrid = grid.get(gridYear);

    if (yearGrid == null) {
      final String simpleName = meteo.getClass().getSimpleName();
      LOGGER.error("No windspeed rose concentration for {} year {}", simpleName, gridYear);
      throw new AeriusException(AeriusExceptionReason.SRM2_NO_PRESRM_DATA_FOR_YEAR, String.valueOf(year));
    }
    return yearGrid;
  }

  public boolean hasLandUseData(final AeriusSRMVersion aeriusSRMVersion) {
    return landUseData.containsKey(aeriusSRMVersion.getLandUseVersion());
  }

  public MapData getLandUse(final AeriusSRMVersion aeriusSRMVersion, final LandUseType type) {
    return landUseData.get(aeriusSRMVersion.getLandUseVersion()).getMapData(type);
  }

  /**
   * @param x The x coordinate of the source.
   * @param y The y coordinate of the source.
   * @return the landuse value at the given location
   *
   * {@link LandUseData#getLandUseValueSource(double, double)}.
   */
  public double getLandUseValueSource(final AeriusSRMVersion aeriusSRMVersion, final double x, final double y) {
    return landUseData.get(aeriusSRMVersion.getLandUseVersion()).getLandUseValueSource(x, y);
  }

  void setLandUseData(final AeriusSRMVersion aeriusSRMVersion, final LandUseData landUseData) {
    this.landUseData.put(aeriusSRMVersion.getLandUseVersion(), landUseData);
  }

  public boolean hasDepositionVelocity(final AeriusSRMVersion aeriusSRMVersion) {
    return depositionVelocityMaps.containsKey(aeriusSRMVersion.getDepositionVelocityVersion());
  }

  public SRMDepositionVelocityMap getDepositionVelocityMap(final AeriusSRMVersion aeriusSRMVersion, final Substance substance) {
    return Optional.ofNullable(depositionVelocityMaps.get(aeriusSRMVersion.getDepositionVelocityVersion())).map(v -> v.get(substance)).orElse(null);
  }

  void setDepositionVelocityMaps(final AeriusSRMVersion aeriusSRMVersion,
      final Map<Substance, SRMDepositionVelocityMap> depositionVelocityMaps) {
    this.depositionVelocityMaps.put(aeriusSRMVersion.getDepositionVelocityVersion(), depositionVelocityMaps);
  }
}
