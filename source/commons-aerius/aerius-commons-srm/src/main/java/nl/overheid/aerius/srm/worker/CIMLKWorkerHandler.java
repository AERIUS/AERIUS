/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.opencl.DefaultContext;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;
import nl.overheid.aerius.srm.calculation.BackgroundDataAdder;
import nl.overheid.aerius.srm.calculation.CorrectionsCalculator;
import nl.overheid.aerius.srm.calculation.ExceedanceDaysCalculator;
import nl.overheid.aerius.srm.calculation.ExceedanceHoursCalculator;
import nl.overheid.aerius.srm.calculation.SRMCalculatorNO2;
import nl.overheid.aerius.srm.calculation.SRMCombineResults;
import nl.overheid.aerius.srm.domain.SRMConfiguration;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm1.calculation.SRM1Calculator;
import nl.overheid.aerius.srm2.calculation.SRM2Calculator;
import nl.overheid.aerius.srm2.domain.SRM1RoadSegment;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;

/**
 * SRM WorkHandler for the {@link Theme#RBL}.
 */
class CIMLKWorkerHandler implements Worker<SRMInputData<EngineSource>, CalculationResult> {
  private static final Logger LOGGER = LoggerFactory.getLogger(CIMLKWorkerHandler.class);

  private final CorrectionsCalculator correctionsCalculator = new CorrectionsCalculator();
  private final SRM2CalculationOptions calculationOptions = SRMConstants.CIMLK_SRM2_CALCULATION_OPTIONS;
  private final PreSRMData preSRMData;
  private final SRMCalculatorNO2 srmNO2Calculator = new SRMCalculatorNO2();
  private final SRM1Calculator srm1Calculator;
  private final SRMConfiguration config;
  private final Map<AeriusSRMVersion, SRM2Calculator> srm2Calculators = new ConcurrentHashMap<>();

  public CIMLKWorkerHandler(final SRMConfiguration config, final PreSRMData preSRMData) throws IOException {
    this.preSRMData = preSRMData;
    this.config = config;
    srm1Calculator = new SRM1Calculator(preSRMData);
  }

  @Override
  public CalculationResult run(final SRMInputData<EngineSource> input, final JobIdentifier jobIdentifier,
      final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
    final Collection<AeriusPoint> receptors = input.getReceptors();
    final List<Substance> substances = input.getSubstances();
    final int year = input.getYear();
    final EnumSet<EmissionResultKey> erks = input.getEmissionResultKeys();
    final CalculationResult calculationResult = new CalculationResult(input.getChunkStats(), input.getEngineDataKey());

    final SRM2Calculator srm2Calculator = getOrCreateSRM2Calculator(input);

    // Custom meteo is not supported for CIMLK SRM. Yearly meteo is used if year <= prognoseYear, otherwise prognose meteo is used
    input.setMeteo(((AeriusSRMVersion) input.getEngineVersion()).getMeteoForYear(year));
    final Meteo meteo = input.getMeteo();

    completeResultKeys(erks);
    LOGGER.debug("Start calculation run for Theme CIMLK");
    for (final Entry<Integer, Collection<EngineSource>> sourcesByKey : input.getEmissionSources().entrySet()) {
      final Collection<EngineSource> sources = sourcesByKey.getValue();
      // Calculate SRM 1
      final AeriusSRMVersion srmVersion = (AeriusSRMVersion) input.getEngineVersion();
      final HasGridValue<WindRoseConcentration> windRoseGrid = preSRMData.getWindRoseConcentrationGrid(srmVersion, meteo, year);
      final Collection<SRM1RoadSegment> srm1Sources = sources.stream().filter(SRM1RoadSegment.class::isInstance).map(SRM1RoadSegment.class::cast)
          .toList();
      final Collection<CIMLKResultPoint> srm1Results = srm1Calculator.calculate(year, meteo, substances, erks, srm1Sources, receptors, srmVersion);
      // Calculate SRM 2
      final Collection<SRM2RoadSegment> srm2Sources = sources.stream().filter(SRM2RoadSegment.class::isInstance).map(SRM2RoadSegment.class::cast)
          .toList();
      final ArrayList<AeriusResultPoint> srm2Results = srm2Calculator.calculate(year, meteo, substances, erks, srm2Sources, receptors, null);
      // Combine results
      SRMCombineResults.preprocessResults(srm1Results, srm2Results);
      srmNO2Calculator.recalculateNO2(srm2Results, erks.contains(EmissionResultKey.NO2_CONCENTRATION), windRoseGrid);
      BackgroundDataAdder.add(srm2Results, erks, windRoseGrid);
      // Apply corrections. This this should be done before calculating exceeding values.
      correctionsCalculator.applyCorrections(srm2Results, input.getCorrections());
      // Calculated Exceeding values.
      calculateExceedingDays(srm2Results, erks);
      calculateExceedingHours(srm2Results, erks);
      calculationResult.put(sourcesByKey.getKey(), srm2Results);
    }
    return calculationResult;
  }

  PreSRMData getData() {
    return preSRMData;
  }

  private SRM2Calculator getOrCreateSRM2Calculator(final SRMInputData<EngineSource> input) {
    final AeriusSRMVersion aeriusSRMVersion = (AeriusSRMVersion) input.getEngineVersion();
    return srm2Calculators.computeIfAbsent(aeriusSRMVersion,
        key -> new SRM2Calculator(aeriusSRMVersion, new DefaultContext(true, config.isForceCpu()), preSRMData, config.getConnectionTimeout(),
            calculationOptions) {
          @Override
          protected CIMLKResultPoint newResultPoint(final AeriusPoint point) {
            return new CIMLKResultPoint(point);
          }
        });
  }

  /**
   * Make sure certain results keys are present when calculating data because these results depend on those results being available.
   *
   * @param erks used result keys
   */
  private static void completeResultKeys(final Set<EmissionResultKey> erks) {
    if (erks.contains(EmissionResultKey.NO2_EXCEEDANCE_HOURS)) {
      erks.add(EmissionResultKey.NO2_CONCENTRATION);
    }
    if (erks.contains(EmissionResultKey.NO2_CONCENTRATION)) {
      erks.add(EmissionResultKey.NO2_DIRECT_CONCENTRATION);
    }
    if (erks.contains(EmissionResultKey.PM10_EXCEEDANCE_DAYS)) {
      erks.add(EmissionResultKey.PM10_CONCENTRATION);
    }
  }

  private static void calculateExceedingDays(final List<AeriusResultPoint> results, final Set<EmissionResultKey> erks) {
    if (erks.contains(EmissionResultKey.PM10_EXCEEDANCE_DAYS)) {
      results.forEach(r -> r.setEmissionResult(EmissionResultKey.PM10_EXCEEDANCE_DAYS,
          ExceedanceDaysCalculator.calculate(r.getEmissionResult(EmissionResultKey.PM10_CONCENTRATION))));
    }
  }

  private static void calculateExceedingHours(final List<AeriusResultPoint> results, final Set<EmissionResultKey> erks) {
    if (erks.contains(EmissionResultKey.NO2_EXCEEDANCE_HOURS)) {
      results.forEach(r -> r.setEmissionResult(EmissionResultKey.NO2_EXCEEDANCE_HOURS,
          ExceedanceHoursCalculator.calculate(r.getEmissionResult(EmissionResultKey.NO2_CONCENTRATION))));
    }
  }
}
