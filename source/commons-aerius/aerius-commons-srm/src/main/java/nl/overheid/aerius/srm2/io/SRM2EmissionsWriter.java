/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.OSUtils;

/**
 * Writes emissions and geometry of srm2 objects to file.
 */
public final class SRM2EmissionsWriter {

  private static final String EMISSIONS = "_emissions_";
  private static final String FILE_EXTENSION = ".csv";
  private static final String FORMAT = "%s;%s;%f;%f;%f" + OSUtils.NL;
  private static final String HEADER = "description;geometry;%s;%s;%s" + OSUtils.NL;
  private static final String FOOTER = "total;;%f;%f;%f" + OSUtils.NL;

  private SRM2EmissionsWriter() {
    // util class
  }

  /**
   * Writes the emission values per source to a file.
   * @param file file without extension
   * @param year year to get emissions for
   * @param sources sources
   * @throws IOException io error
   */
  public static void writeEmissions(final File file, final int year, final List<EmissionSourceFeature> sources) throws IOException {
    try (final Writer writer = Files.newBufferedWriter(createFileName(file, year), StandardCharsets.UTF_8)) {
      final List<Substance> substances = new ArrayList<>();
      substances.add(Substance.NOX);
      substances.add(Substance.NO2);
      substances.add(Substance.NH3);
      writer.write(String.format(Locale.US, HEADER, substances.get(0).getName(), substances.get(1).getName(),
          substances.get(2).getName()));
      final Map<Substance, Double> totalEmissionMap = new HashMap<>();
      for (final Substance substance : substances) {
        totalEmissionMap.put(substance, 0.0);
      }
      for (final EmissionSourceFeature es : sources) {
        writeSource(writer, substances, totalEmissionMap, es);
      }
      writer.write(String.format(Locale.US, FOOTER, totalEmissionMap.get(substances.get(0)), totalEmissionMap.get(substances.get(1)),
          totalEmissionMap.get(substances.get(2))));
    }
  }

  private static void writeSource(final Writer writer, final List<Substance> substances, final Map<Substance, Double> totalEmissionMap,
      final EmissionSourceFeature feature) throws IOException {
    final EmissionSource es = feature.getProperties();
    final String wkt = toWkt(feature.getGeometry());
    writer.write(String.format(Locale.US, FORMAT, es.getLabel(), wkt,
        es.getEmissions().get(substances.get(0)), es.getEmissions().get(substances.get(1)), es.getEmissions().get(substances.get(2))));
    for (final Substance substance : substances) {
      if (es.getEmissions().containsKey(substance)) {
        totalEmissionMap.put(substance, totalEmissionMap.get(substance) + es.getEmissions().get(substance));
      }
    }
  }

  private static String toWkt(final Geometry geometry) throws IOException {
    try {
      return GeometryUtil.getGeometry(geometry).toText();
    } catch (final AeriusException e) {
      throw new IOException("Somehow ended up with incorrect geometry when writing CIMLK object", e);
    }
  }

  private static Path createFileName(final File file, final int year) {
    return new File(file.getAbsoluteFile() + EMISSIONS + year + FILE_EXTENSION).toPath();
  }
}
