/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import java.util.Collection;
import java.util.Set;

import nl.overheid.aerius.receptor.AbstractSubReceptorAggregator;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * SRM specific implementation of {@link AbstractSubReceptorAggregator}
 */
public class SRMSubReceptorAggregator extends AbstractSubReceptorAggregator<AeriusResultPoint, AeriusPoint> {

  /**
   * Initializes the aggregator with the receptors.
   * It filters out all {@link IsSubPoint} points as those points are only relevant for subreceptors.
   *
   * @param receptors all receptors
   * @param erks emission result keys for which results are requested.
   */
  public SRMSubReceptorAggregator(final Collection<AeriusPoint> receptors, final Set<EmissionResultKey> erks) {
    super(receptors, erks, true);
  }

  @Override
  protected Collection<AeriusResultPoint> combine(final Collection<AeriusResultPoint> receptorResults,
      final Collection<AeriusResultPoint> subReceptorResults) {
    return receptorResults;
  }
}
