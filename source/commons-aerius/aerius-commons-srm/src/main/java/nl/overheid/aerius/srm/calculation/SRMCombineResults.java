/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;

/**
 * Util class to combine SRM1 and SRM2 calculation results in 1 data object per point.
 */
public final class SRMCombineResults {

  private SRMCombineResults() {
    // Util class
  }

  /**
   * Processes the results from both srm1 and srm2 and returns the results combined.
   * It fills the srm2 and srm1 data fields from into the srmResults object.
   *
   * @param srm1Results results containing only srm1 results
   * @param srm2Results results containing only srm2 results and will be filled with all results.
   */
  public static void preprocessResults(final Collection<CIMLKResultPoint> srm1Results, final List<AeriusResultPoint> srm2Results) {
    copySRM2Results(srm2Results);
    if (!srm1Results.isEmpty()) {
      combineResults(srm1Results, srm2Results);
    }
  }

  private static void copySRM2Results(final List<AeriusResultPoint> srm2Results) {
    srm2Results.forEach(r -> {
      if (r instanceof CIMLKResultPoint) {
        final CIMLKResultPoint resultPoint = (CIMLKResultPoint) r;

        resultPoint.getEmissionResults().copyTo(resultPoint.getSrm2Results());
      }
    });
  }

  private static void combineResults(final Collection<CIMLKResultPoint> srm1Results,
      final List<AeriusResultPoint> srm2Results) {
    final Map<Integer, CIMLKResultPoint> srm1Map = new HashMap<>();
    srm1Results.forEach(r -> srm1Map.put(r.getId(), r));

    srm2Results.forEach(r2 -> {
      if (r2 instanceof CIMLKResultPoint) {
        final CIMLKResultPoint srm2Result = (CIMLKResultPoint) r2;
        final CIMLKResultPoint srm1Result = srm1Map.get(srm2Result.getId());

        if (srm1Result != null) {
          srm1Result.getEmissionResults().copyTo(srm2Result.getSrm1Results());
          srm2Result.setWindFactor(srm1Result.getWindFactor());
        }
      }
    });
  }
}
