/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLine;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRoadProfile;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKTreeProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Reader for CIMLK dispersion line files.
 *
 * Available columns:
 * <pre>
 * srm1_road_id;calculation_point_id;jurisdiction_id;label;road_profile;tree_profile;description;geometry;distance
 * </pre>
 */
class CIMLKDispersionLineFileReader extends AbstractCIMLKFileReader<CIMLKDispersionLineFeature> {

  // @formatter:off
  private enum DispersionLineColumns implements Columns {
    SRM1_ROAD_ID,
    CALCULATION_POINT_ID,
    JURISDICTION_ID,
    LABEL,
    ROAD_PROFILE,
    TREE_PROFILE,
    DESCRIPTION;
    // @formatter:on

    static DispersionLineColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  @Override
  protected CIMLKDispersionLineFeature parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final CIMLKDispersionLine dispersionLine = new CIMLKDispersionLine();

    final String roadId = getGmlId(DispersionLineColumns.SRM1_ROAD_ID, GMLIdUtil.SOURCE_PREFIX, warnings);
    final String calculationPointId = getGmlId(DispersionLineColumns.CALCULATION_POINT_ID, GMLIdUtil.POINT_PREFIX, warnings);
    dispersionLine.setRoadGmlId(roadId);
    dispersionLine.setCalculationPointGmlId(calculationPointId);
    final String dispersionLineId = dispersionLine.getRoadGmlId() + "," + dispersionLine.getCalculationPointGmlId();
    dispersionLine.setJurisdictionId(getInt(DispersionLineColumns.JURISDICTION_ID));
    dispersionLine.setLabel(getString(DispersionLineColumns.LABEL));
    dispersionLine.setRoadProfile(getEnumValue(CIMLKRoadProfile.class, DispersionLineColumns.ROAD_PROFILE, dispersionLineId));
    dispersionLine.setTreeProfile(getEnumValue(CIMLKTreeProfile.class, DispersionLineColumns.TREE_PROFILE, dispersionLineId));
    dispersionLine.setDescription(getString(DispersionLineColumns.DESCRIPTION));

    // Distance is calculated in the calculation part.
    // Geometry isn't used in calculation method, it's a derived value. Hence no validation on this line.
    // (line that describes the closed distance road segment -> calculation point)
    final CIMLKDispersionLineFeature feature = new CIMLKDispersionLineFeature();
    feature.setProperties(dispersionLine);
    return feature;
  }

  @Override
  Columns[] expectedColumns() {
    return DispersionLineColumns.values();
  }

  @Override
  Columns safeColumnOf(final String value) {
    return DispersionLineColumns.safeValueOf(value);
  }

}
