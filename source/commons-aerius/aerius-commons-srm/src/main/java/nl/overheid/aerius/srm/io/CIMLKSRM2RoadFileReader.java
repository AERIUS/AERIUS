/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadManager;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Reader for CIMLK SRM2 road files.
 *
 * Available columns:
 * <pre>
 * srm2_road_id;jurisdiction_id;label;road_manager;elevation_height;category;tunnel_factor;max_speed;dynamic_max_speed;freight_max_speed;
 * barrier_left_distance;barrier_left_height;barrier_right_distance;barrier_right_height;
 * light_traffic_intensity;light_traffic_dynamic_intensity;light_traffic_stagnation_factor;
 * normal_freight_intensity;normal_freight_stagnation_factor;
 * heavy_freight_intensity;heavy_freight_stagnation_factor;
 * description;geometry
 * </pre>
 */
class CIMLKSRM2RoadFileReader extends AbstractCIMLKFileReader<EmissionSourceFeature> {

  // @formatter:off
  private enum SRM2RoadColumns implements Columns {
    SRM2_ROAD_ID,
    JURISDICTION_ID,
    LABEL,
    ROAD_MANAGER,
    ELEVATION_HEIGHT,
    CATEGORY,
    TUNNEL_FACTOR,
    BARRIER_LEFT_DISTANCE,
    BARRIER_LEFT_HEIGHT,
    BARRIER_RIGHT_DISTANCE,
    BARRIER_RIGHT_HEIGHT,
    MAX_SPEED,
    DYNAMIC_MAX_SPEED,
    LIGHT_TRAFFIC_INTENSITY,
    LIGHT_TRAFFIC_DYNAMIC_INTENSITY,
    LIGHT_TRAFFIC_STAGNATION_FACTOR,
    NORMAL_FREIGHT_INTENSITY,
    NORMAL_FREIGHT_STAGNATION_FACTOR,
    HEAVY_FREIGHT_INTENSITY,
    HEAVY_FREIGHT_STAGNATION_FACTOR,
    AUTO_BUS_INTENSITY,
    AUTO_BUS_STAGNATION_FACTOR,
    DESCRIPTION,
    GEOMETRY;
    // @formatter:on

    static SRM2RoadColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  private enum SRM2RoadType {
    NATIONAL_ROAD, FREEWAY, FREEWAY_STRICT_ENFORCEMENT;
  }

  private final SectorCategories categories;

  /**
   * @param categories The context to be used for determining road emission categories.
   */
  public CIMLKSRM2RoadFileReader(final SectorCategories categories) {
    this.categories = categories;
  }

  @Override
  protected EmissionSourceFeature parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final SRM2RoadEmissionSource road = new SRM2RoadEmissionSource();

    final String roadId = getGmlId(SRM2RoadColumns.SRM2_ROAD_ID, GMLIdUtil.SOURCE_PREFIX, warnings);
    road.setGmlId(roadId);
    feature.setId(roadId);

    road.setJurisdictionId(getInt(SRM2RoadColumns.JURISDICTION_ID));
    road.setLabel(getString(SRM2RoadColumns.LABEL));
    road.setRoadManager(getNullableEnumValue(RoadManager.class, SRM2RoadColumns.ROAD_MANAGER, road.getGmlId()));
    road.setDescription(getString(SRM2RoadColumns.DESCRIPTION));

    final SRM2RoadType inputRoadType = getEnumValue(SRM2RoadType.class, SRM2RoadColumns.CATEGORY, road.getGmlId());
    final RoadType roadType = wegBeheerToRoadType(inputRoadType);
    road.setRoadAreaCode(DEFAULT_AREA_CODE);
    road.setRoadTypeCode(roadType.getRoadTypeCode());
    road.setSectorId(ROAD_SECTOR_ID);
    //Store the sector id as ParticleSizeDistribution, needed for OPS (Roads should always be calculated with SRM, but you never know...)
    final Sector sector = categories.getSectorById(ROAD_SECTOR_ID);
    final OPSSourceCharacteristics characteristics = sector.getDefaultCharacteristics().copyTo(new OPSSourceCharacteristics());
    characteristics.setParticleSizeDistribution(ROAD_SECTOR_ID);
    road.setCharacteristics(characteristics);

    parseElevation(road);

    parseTunnelFactor(road, warnings);

    parseBarriers(road);

    final boolean strictEnforcement = inputRoadType == SRM2RoadType.FREEWAY_STRICT_ENFORCEMENT;
    parseAllTraffic(road, roadType.getRoadTypeCode(), strictEnforcement);

    parseGeometry(feature);

    feature.setProperties(road);
    return feature;
  }

  private void parseElevation(final SRM2RoadEmissionSource road) throws AeriusException {
    final int height = getInt(SRM2RoadColumns.ELEVATION_HEIGHT);
    road.setElevationHeight(height);
    road.setElevation(getRoadElevation(height));
  }

  /**
   * Returns the {@link RoadElevation} enum given a height.
   * @param height actual height
   * @return road elevation enum
   */
  private RoadElevation getRoadElevation(final int height) {
    final RoadElevation re;
    if (height > 0) {
      //In line with CIMLK Monitoringstool roads with a height are steep dykes.
      re = RoadElevation.STEEP_DYKE;
    } else if (height < 0) {
      re = RoadElevation.TUNNEL;
    } else {
      re = RoadElevation.NORMAL;
    }
    return re;
  }

  private void parseTunnelFactor(final SRM2RoadEmissionSource road, final List<AeriusException> warnings) throws AeriusException {
    road.setTunnelFactor(getDouble(SRM2RoadColumns.TUNNEL_FACTOR));
    if (road.getTunnelFactor() < 0) {
      throw new AeriusException(ImaerExceptionReason.SRM2_SOURCE_TUNNEL_FACTOR_ZERO, String.valueOf(road.getTunnelFactor()), road.getLabel());
    }
  }

  private RoadType wegBeheerToRoadType(final SRM2RoadType wegtype) throws AeriusException {
    final RoadType roadType;

    switch (wegtype) {
    case NATIONAL_ROAD:
      roadType = RoadType.NON_URBAN_ROAD;
      break;
    case FREEWAY:
    case FREEWAY_STRICT_ENFORCEMENT:
      roadType = RoadType.FREEWAY;
      break;
    default:
      // Should already be caught, hence an INTERNAL_ERROR at this point
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    return roadType;
  }

  private void parseBarriers(final SRM2RoadEmissionSource road) throws AeriusException {
    final SRM2RoadSideBarrier barrierLeft = getRoadSideBarrier(SRM2RoadColumns.BARRIER_LEFT_HEIGHT, SRM2RoadColumns.BARRIER_LEFT_DISTANCE);
    if (Double.doubleToLongBits(barrierLeft.getHeight()) != 0) {
      road.setBarrierLeft(barrierLeft);
    }

    final SRM2RoadSideBarrier barrierRight = getRoadSideBarrier(SRM2RoadColumns.BARRIER_RIGHT_HEIGHT, SRM2RoadColumns.BARRIER_RIGHT_DISTANCE);
    if (Double.doubleToLongBits(barrierRight.getHeight()) != 0) {
      road.setBarrierRight(barrierRight);
    }
  }

  private SRM2RoadSideBarrier getRoadSideBarrier(final Columns heightColumn, final Columns distanceColumn) throws AeriusException {
    final SRM2RoadSideBarrier barrier = new SRM2RoadSideBarrier();
    //no way to tell if it's a screen or a wall. Assume the worst.
    barrier.setBarrierType(SRM2RoadSideBarrierType.SCREEN);
    barrier.setHeight(getDouble(heightColumn));
    barrier.setDistance(getDouble(distanceColumn));
    return barrier;
  }

  private void parseAllTraffic(final SRM2RoadEmissionSource road, final String roadType, final boolean strictEnforcement) throws AeriusException {
    parseNormalTraffic(road, roadType, strictEnforcement);
    parseDynamicTraffic(road, roadType, strictEnforcement);
  }

  private void parseNormalTraffic(final SRM2RoadEmissionSource road, final String roadType, final boolean strictEnforcement)
      throws AeriusException {
    final StandardVehicles traffic = new StandardVehicles();
    final int maxSpeed = (int) getDouble(SRM2RoadColumns.MAX_SPEED);
    traffic.setMaximumSpeed(maxSpeed);
    traffic.setStrictEnforcement(strictEnforcement);
    traffic.setTimeUnit(TimeUnit.DAY);

    parseTraffic(traffic, roadType,
        VehicleType.LIGHT_TRAFFIC.getStandardVehicleCode(),
        maxSpeed,
        SRM2RoadColumns.LIGHT_TRAFFIC_INTENSITY,
        SRM2RoadColumns.LIGHT_TRAFFIC_STAGNATION_FACTOR,
        strictEnforcement);
    parseTraffic(traffic, roadType,
        VehicleType.NORMAL_FREIGHT.getStandardVehicleCode(),
        maxSpeed,
        SRM2RoadColumns.NORMAL_FREIGHT_INTENSITY,
        SRM2RoadColumns.NORMAL_FREIGHT_STAGNATION_FACTOR,
        strictEnforcement);
    parseTraffic(traffic, roadType,
        VehicleType.HEAVY_FREIGHT.getStandardVehicleCode(),
        maxSpeed,
        SRM2RoadColumns.HEAVY_FREIGHT_INTENSITY,
        SRM2RoadColumns.HEAVY_FREIGHT_STAGNATION_FACTOR,
        strictEnforcement);
    parseTraffic(traffic, roadType,
        VehicleType.AUTO_BUS.getStandardVehicleCode(),
        maxSpeed,
        SRM2RoadColumns.AUTO_BUS_INTENSITY,
        SRM2RoadColumns.AUTO_BUS_STAGNATION_FACTOR,
        strictEnforcement);
    road.getSubSources().add(traffic);
  }

  private void parseDynamicTraffic(final SRM2RoadEmissionSource road, final String roadType, final boolean strictEnforcement)
      throws AeriusException {
    final StandardVehicles dynamicTraffic = new StandardVehicles();
    final int maxSpeed = (int) getDouble(SRM2RoadColumns.DYNAMIC_MAX_SPEED);
    dynamicTraffic.setMaximumSpeed(maxSpeed);
    dynamicTraffic.setStrictEnforcement(strictEnforcement);
    dynamicTraffic.setTimeUnit(TimeUnit.DAY);
    //dynamic does not have a stagnation factor.
    parseTraffic(dynamicTraffic, roadType,
        VehicleType.LIGHT_TRAFFIC.getStandardVehicleCode(),
        maxSpeed,
        SRM2RoadColumns.LIGHT_TRAFFIC_DYNAMIC_INTENSITY,
        null,
        strictEnforcement);
    if (dynamicTraffic.getValuesPerVehicleTypes().values().stream().anyMatch(value -> Double.compare(value.getVehiclesPerTimeUnit(), 0) != 0)) {
      road.getSubSources().add(dynamicTraffic);
    }
  }

  /**
   * Set the source data for specific traffic types.
   * @param traffic Object to add values to
   * @param roadType type of the road
   * @param vehicleType vehicle type
   * @param maxSpeedColumn max speed
   * @param intensityColumn number of vehicles
   * @param stagnationColumn stagnation factor
   * @param strictEnforcement strict enforcement
   */
  private void parseTraffic(final StandardVehicles traffic, final String roadType, final String vehicleType,
      final int maxSpeed, final SRM2RoadColumns intensityColumn, final SRM2RoadColumns stagnationColumn,
      final boolean strictEnforcement) throws AeriusException {
    final RoadEmissionCategory roadEmissionCategory = categories.getRoadEmissionCategories().findClosestCategory(DEFAULT_AREA_CODE, roadType,
        vehicleType, strictEnforcement, maxSpeed, null);

    if (roadEmissionCategory == null) {
      throw new AeriusException(AeriusExceptionReason.SRM2IO_EXCEPTION_NO_ROAD_PROPERTIES, String.valueOf(getCurrentLineNumber()),
          getCurrentColumnContent(),
          roadType + "," + vehicleType + "," + maxSpeed + (strictEnforcement ? ",SE" : ""));
    }
    final double intensity = getDouble(intensityColumn);

    if (intensity > 0 || VehicleType.LIGHT_TRAFFIC.getStandardVehicleCode().equalsIgnoreCase(vehicleType)) {
      final ValuesPerVehicleType valuesPerVehicleType = new ValuesPerVehicleType();
      valuesPerVehicleType.setVehiclesPerTimeUnit(intensity);
      if (stagnationColumn != null) {
        valuesPerVehicleType.setStagnationFraction(getDouble(stagnationColumn));
      }

      traffic.getValuesPerVehicleTypes().put(vehicleType, valuesPerVehicleType);
    }
  }

  private void parseGeometry(final EmissionSourceFeature feature) throws AeriusException {
    final Geometry geometry = getGeometry();
    feature.setGeometry(geometry);
  }

  private Geometry getGeometry() throws AeriusException {
    return getGeometry(SRM2RoadColumns.GEOMETRY, geom -> geom.type() != GeometryType.LINESTRING);
  }

  @Override
  Columns[] expectedColumns() {
    return SRM2RoadColumns.values();
  }

  @Override
  Columns safeColumnOf(final String value) {
    return SRM2RoadColumns.safeValueOf(value);
  }

}
