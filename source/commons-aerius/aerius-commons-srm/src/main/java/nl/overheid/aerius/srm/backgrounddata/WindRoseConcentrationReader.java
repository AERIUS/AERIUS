/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.List;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Reader for pre-srm pre-processed O3 GCN per wind sector and NH3 GCN.
 */
public class WindRoseConcentrationReader extends PreSrmXYReader<WindRoseConcentration> {
  // Separate on white space characters
  private static final String CO3 = "CO3";
  // Offset where windrose O3 data starts. Skip x-y coordinate data.
  private static final int CO3_OFFSET = 2;
  private static final int GCN_NH3_COLUMN_INDEX = CO3_OFFSET + SRMConstants.WIND_SECTORS;
  private static final int C_HWN_NH3_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 1;
  private static final int GCN_NO2_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 2;
  private static final int C_HWN_NO2_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 3;
  private static final int GCN_PM10_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 4;
  private static final int C_HWN_PM10_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 5;
  private static final int GCN_PM25_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 6;
  private static final int C_HWN_PM25_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 7;
  private static final int GCN_EC_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 8;
  private static final int C_HWN_EC_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 9;
  private static final int GCN_O3_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 10;
  private static final int C_HWN_O3_COLUMN_INDEX = GCN_NH3_COLUMN_INDEX + 11;
  private static final String GCN_NH3_COLUMN = "GCN_NH3";
  private static final String GCN_NO2_COLUMN = "GCN_NO2";
  private static final String GCN_PM10_COLUMN = "GCN_PM10";
  private static final String GCN_PM25_COLUMN = "GCN_PM25";
  private static final String GCN_EC_COLUMN = "GCN_EC";
  private static final String GCN_O3_COLUMN = "GCN_O3";
  private static final String C_HWN_NH3_COLUMN = "C_HWN_NH3";
  private static final String C_HWN_NO2_COLUMN = "C_HWN_NO2";
  private static final String C_HWN_PM10_COLUMN = "C_HWN_PM10";
  private static final String C_HWN_PM25_COLUMN = "C_HWN_PM25";
  private static final String C_HWN_EC_COLUMN = "C_HWN_EC";
  private static final String C_HWN_O3_COLUMN = "C_HWN_O3";

  private final boolean allBackground;

  public WindRoseConcentrationReader(final String preSrmVersion, final YearPattern yearPattern, final int year, final boolean allBackground) {
    super(preSrmVersion, yearPattern, year);
    this.allBackground = allBackground;
  }

  @Override
  protected WindRoseConcentration parseLine(final String line, final List<AeriusException> warnings) {
    final WindRoseConcentration windrose = new WindRoseConcentration(readX(), readY());

    for (int i = 0; i < SRMConstants.WIND_SECTORS; i++) {
      windrose.setO3(i, getDouble(CO3 + i, CO3_OFFSET + i));
    }
    windrose.setGCN(Substance.NH3, getDouble(GCN_NH3_COLUMN, GCN_NH3_COLUMN_INDEX));
    windrose.setCHwn(Substance.NH3, getNegatedDouble(C_HWN_NH3_COLUMN, C_HWN_NH3_COLUMN_INDEX));
    if (allBackground) {
      windrose.setGCN(Substance.NO2, getDouble(GCN_NO2_COLUMN, GCN_NO2_COLUMN_INDEX));
      windrose.setCHwn(Substance.NO2, getNegatedDouble(C_HWN_NO2_COLUMN, C_HWN_NO2_COLUMN_INDEX));
      windrose.setGCN(Substance.PM10, getDouble(GCN_PM10_COLUMN, GCN_PM10_COLUMN_INDEX));
      windrose.setCHwn(Substance.PM10, getNegatedDouble(C_HWN_PM10_COLUMN, C_HWN_PM10_COLUMN_INDEX));
      windrose.setGCN(Substance.PM25, getDouble(GCN_PM25_COLUMN, GCN_PM25_COLUMN_INDEX));
      windrose.setCHwn(Substance.PM25, getNegatedDouble(C_HWN_PM25_COLUMN, C_HWN_PM25_COLUMN_INDEX));
      windrose.setGCN(Substance.EC, getDouble(GCN_EC_COLUMN, GCN_EC_COLUMN_INDEX));
      windrose.setCHwn(Substance.EC, getNegatedDouble(C_HWN_EC_COLUMN, C_HWN_EC_COLUMN_INDEX));
      windrose.setGCN(Substance.O3, getDouble(GCN_O3_COLUMN, GCN_O3_COLUMN_INDEX));
      windrose.setCHwn(Substance.O3, getNegatedDouble(C_HWN_O3_COLUMN, C_HWN_O3_COLUMN_INDEX));
    }

    return windrose;
  }
}
