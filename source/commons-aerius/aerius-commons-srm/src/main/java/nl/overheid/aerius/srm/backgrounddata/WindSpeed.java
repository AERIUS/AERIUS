/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.Objects;

/**
 * Data class to store total wind speed values for a given point location.
 */
/**
 *
 */
public class WindSpeed extends GridPoint {

  private double windTotalSpeed;

  public WindSpeed(final int x, final int y) {
    super(x, y);
  }

  @Override
  public boolean equals(final Object obj) {
    return super.equals(obj) && Objects.equals(windTotalSpeed, ((WindSpeed) obj).windTotalSpeed);
  }

  @Override
  public int hashCode() {
    return Double.hashCode(windTotalSpeed) + 31 * super.hashCode();
  }

  public double getWindTotalSpeed() {
    return windTotalSpeed;
  }

  public void setWindTotalSpeed(final double windTotalSpeed) {
    this.windTotalSpeed = windTotalSpeed;
  }

  @Override
  public String toString() {
    return "WindSpeed [windTotalSpeed=" + windTotalSpeed + super.toString();
  }
}
