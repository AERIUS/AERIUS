/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata.bgconcentrations;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.PointGrid;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;

class OwN2000WindRoseConcentrationGrid implements HasGridValue<WindRoseConcentration> {

  private final PointGrid<WindRoseConcentration> backgroundConcentrations;
  private final PointGrid<WindRoseConcentration> schipholCorrections;

  public OwN2000WindRoseConcentrationGrid(final PointGrid<WindRoseConcentration> backgroundConcentrations,
      final PointGrid<WindRoseConcentration> schipholCorrections) {
    this.backgroundConcentrations = backgroundConcentrations;
    this.schipholCorrections = schipholCorrections;
  }

  @Override
  public WindRoseConcentration get(final double x, final double y) {
    return updateForSchiphol(backgroundConcentrations.get(x, y), x, y);
  }

  private WindRoseConcentration updateForSchiphol(final WindRoseConcentration wc, final double x, final double y) {
    final WindRoseConcentration schipholValue = schipholCorrections == null || wc == null ? null : schipholCorrections.getOrNull(x, y);

    if (schipholValue == null) {
      return wc;
    } else {
      final WindRoseConcentration clone = schipholValue.copy();

      clone.setGCN(Substance.NH3, wc.getGCN(Substance.NH3));
      clone.setCHwn(Substance.NH3, wc.getCHwn(Substance.NH3));
      return clone;
    }
  }

  PointGrid<WindRoseConcentration> getSchipholCorrections() {
    return schipholCorrections;
  }

}
