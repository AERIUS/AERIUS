/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import java.util.concurrent.TimeUnit;

/**
 * A context is a wrapper around an OpenCL context and manages the devices and their queues.
 */
public interface Context extends AutoCloseable {
  /**
   * Returns a connection to an OpenCL device.
   * @return A connection.
   * @throws IllegalStateException Thrown when the context has already been closed.
   */
  Connection getConnection();

  /**
   * Returns a connection to an OpenCL device.
   * @param time The maximum time to wait. If less or equal to 0, it will wait indefinitely as if getConnection() had been called.
   * @param unit The ti,e unit of the time argument.
   * @return A connection, or null if the method timed out.
   * @throws IllegalStateException Thrown when the context has already been closed.
   */
  Connection getConnection(long time, TimeUnit unit);

  /**
   * @return The maximum amount of available connections.
   */
  int getPoolSize();

  /**
   * @return The amount of connections left in the pool.
   */
  int getCurrentPoolSize();

  /**
   * Closes the context. Does nothing if its already closed. The context will no longer provide connections.
   */
  @Override
  void close();

  /**
   * @return Returns true if the context has been closed.
   */
  boolean isClosed();
}
