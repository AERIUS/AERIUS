/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.domain;

import java.util.List;
import java.util.Optional;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;

/**
 * Data object containing all information for a srm2 calculation.
 */
public class SRMInputData<E extends EngineSource> extends EngineInputData<E, AeriusPoint> {

  private static final long serialVersionUID = 5L;

  private Meteo meteo;
  private List<CIMLKCorrection> corrections;

  /**
   * Constructor.
   *
   * @param theme Calculation theme this data should be calculated with
   */
  public SRMInputData(final Theme theme, final AeriusSRMVersion srmVersion) {
    super(theme, CalculationEngine.ASRM2, srmVersion, CommandType.CALCULATE);
  }

  public Meteo getMeteo() {
    return meteo;
  }

  public void setMeteo(final Meteo meteo) {
    this.meteo = meteo;
  }

  /**
   * @return the corrections
   */
  public List<CIMLKCorrection> getCorrections() {
    return corrections;
  }

  /**
   * @param corrections the corrections to set
   */
  public void setCorrections(final List<CIMLKCorrection> corrections) {
    this.corrections = corrections;
  }

  @Override
  public String toString() {
    return "SRMInputData [aeriusSRMVersion=" + getEngineVersion() + ", meteo=" + meteo
        + Optional.ofNullable(corrections).map(c -> ", #corrections=" + c.size()).orElse("") + super.toString();
  }
}
