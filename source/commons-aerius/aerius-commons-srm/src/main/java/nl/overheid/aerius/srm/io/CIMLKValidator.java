/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;

/**
 * Validator util to check for valid CIMLK values.
 */
final class CIMLKValidator {

  private static final Set<Integer> VALID_DISPERSION_LINE_ROAD_TYPES = Arrays.asList(1, 2, 3, 4).stream().collect(Collectors.toSet());
  private static final Set<Integer> VALID_TREE_FACTORS = Arrays.asList(100, 125, 150).stream().collect(Collectors.toSet());

  private CIMLKValidator() {
    // Util class
  }

  /**
   * Checks if the given road type is a valid road type for a dispersion line or else throws a {@link ImaerExceptionReason#SRM_LEGACY_INVALID_ROAD_TYPE} reason.
   */
  public static void validateDispersionLineRoadType(final int lineNumber, final int roadType) throws AeriusException {
    if (!VALID_DISPERSION_LINE_ROAD_TYPES.contains(roadType)) {
      throw new AeriusException(ImaerExceptionReason.SRM_LEGACY_INVALID_ROAD_TYPE, String.valueOf(lineNumber), String.valueOf(roadType));
    }
  }

  /**
   * Validates if the given tree factor is valid or else throws a {@link ImaerExceptionReason#SRM_LEGACY_INVALID_TREE_FACTOR}.
   */
  public static void validateTreeFactor(final int lineNumber, final double treeFactor) throws AeriusException {
    final int treeFactorInt = (int) (treeFactor * 100); // convert to int to avoid needing to compare doubles
    if (!VALID_TREE_FACTORS.contains(treeFactorInt)) {
      throw new AeriusException(ImaerExceptionReason.SRM_LEGACY_INVALID_TREE_FACTOR, String.valueOf(lineNumber), String.valueOf(treeFactor));
    }
  }
}
