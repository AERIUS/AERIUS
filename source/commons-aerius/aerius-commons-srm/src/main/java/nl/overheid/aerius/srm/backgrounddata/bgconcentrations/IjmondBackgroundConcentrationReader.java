/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata.bgconcentrations;

import java.util.List;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.backgrounddata.PreSrmXYReader;

/**
 *
 */
class IjmondBackgroundConcentrationReader extends PreSrmXYReader<IjmondCorrectionConcentration> {

  private static final int GCN_PM10_COLUMN_INDEX = 2;
  private static final int C_HWN_PM10_COLUMN_INDEX = GCN_PM10_COLUMN_INDEX + 1;
  private static final int GCN_PM25_COLUMN_INDEX = GCN_PM10_COLUMN_INDEX + 2;
  private static final int C_HWN_PM25_COLUMN_INDEX = GCN_PM10_COLUMN_INDEX + 3;
  private static final int GCN_EC_COLUMN_INDEX = GCN_PM10_COLUMN_INDEX + 4;
  private static final int C_HWN_EC_COLUMN_INDEX = GCN_PM10_COLUMN_INDEX + 5;
  private static final String GCN_PM10_COLUMN = "GCN_PM10";
  private static final String C_HWN_PM10_COLUMN = "C_HWN_PM10";
  private static final String GCN_PM25_COLUMN = "GCN_PM25";
  private static final String C_HWN_PM25_COLUMN = "C_HWN_PM25";
  private static final String GCN_EC_COLUMN = "GCN_EC";
  private static final String C_HWN_EC_COLUMN = "C_HWN_EC";

  public IjmondBackgroundConcentrationReader(final String preSrmVersion, final YearPattern yearPattern, final int year) {
    super(preSrmVersion, yearPattern, year);
  }

  @Override
  protected IjmondCorrectionConcentration parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final IjmondCorrectionConcentration ijmond = new IjmondCorrectionConcentration(readX(), readY());

    ijmond.setGcnPm10(getDouble(GCN_PM10_COLUMN, GCN_PM10_COLUMN_INDEX));
    ijmond.setcHwnPm10(getNegatedDouble(C_HWN_PM10_COLUMN, C_HWN_PM10_COLUMN_INDEX));
    ijmond.setGcnPm25(getDouble(GCN_PM25_COLUMN, GCN_PM25_COLUMN_INDEX));
    ijmond.setcHwnPm25(getNegatedDouble(C_HWN_PM25_COLUMN, C_HWN_PM25_COLUMN_INDEX));
    ijmond.setGcnEc(getDouble(GCN_EC_COLUMN, GCN_EC_COLUMN_INDEX));
    ijmond.setcHwnEc(getNegatedDouble(C_HWN_EC_COLUMN, C_HWN_EC_COLUMN_INDEX));
    return ijmond;
  }
}
