/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.srm.backgrounddata.LandUseData.LandUseType;

/**
 * Loads LandUse data from file.
 */
public final class LandUseDataBuilder {
  private static final Logger LOG = LoggerFactory.getLogger(LandUseDataBuilder.class);

  private LandUseDataBuilder() {
    // Util class.
  }

  /**
   * Load PRE-SRM z0 data files.
   *
   * @param landUseDirectory the landuse data files directory.
   * @return landuse data
   * @throws IOException when file reading failed
   */
  public static LandUseData loadZ0(final File landUseDirectory) throws IOException {
    final long startTime = System.nanoTime();
    final LandUseData data = new LandUseData();

    for (final LandUseType lut : LandUseType.values()) {
      try (final InputStream is = new FileInputStream(new File(landUseDirectory, lut.getFilename()))) {
        data.setMapData(lut, LandUseDataFileReader.read(is));
      }
    }
    LOG.info("Load time of landuse data: {} ms.", TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime));
    return data;
  }
}
