/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2LinearReference;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

public abstract class AbstractSegmentSRMConverter<E extends EngineSource> extends AbstractSRMConverter<E> {

  private static final double MIN_DISTANCE = 0.001;

  abstract void addSegments(LineString geometry, EmissionSource source, List<Substance> substances, List<E> segments, Node startNode)
      throws AeriusException;

  @Override
  List<E> convert(final LineString geometry, final EmissionSource source, final List<Substance> substances) throws AeriusException {
    final List<E> segments = new ArrayList<>();

    final Node startNode = calculateSegmentNodes(geometry);
    calculateDynamicSegments(source, startNode);
    addSegments(geometry, source, substances, segments, startNode);

    return segments;
  }

  protected SRM2RoadSegment createObject(final boolean isFreeway, final RoadElevation elevation, final int elevationHeight,
      final SRM2RoadSideBarrier barrierLeft, final SRM2RoadSideBarrier barrierRight) {
    final SRM2RoadSegment segment = new SRM2RoadSegment();
    segment.setElevationHeight(Sigma0Calculator.getElevationHeight(elevationHeight));
    segment.setSigma0(Sigma0Calculator.getSigma0(isFreeway, elevation, elevationHeight, barrierLeft, barrierRight));

    return segment;
  }

  private Node calculateSegmentNodes(final LineString geometry) {
    Node startNode = null;
    Node previousNode = null;

    for (int i = 0; i < geometry.getCoordinates().length; i++) {
      final Node node = new Node(geometry.getCoordinates()[i][0], geometry.getCoordinates()[i][1]);

      if (startNode == null) {
        startNode = node;
      } else {
        final Segment segment = new Segment(previousNode, node);
        previousNode.nextSegment = segment;
      }

      previousNode = node;
    }
    return startNode;
  }

  private void calculateDynamicSegments(final EmissionSource es, final Node startNode) {
    if (es instanceof SRM2RoadEmissionSource) {
      final List<SRM2LinearReference> dynamicSegments = ((SRM2RoadEmissionSource) es).getPartialChanges();
      if (dynamicSegments != null && !dynamicSegments.isEmpty()) {
        final double totalLength = startNode.totalLength();

        for (final SRM2LinearReference dynamicSegment : dynamicSegments) {
          final Node segmentStart = startNode.addNode(dynamicSegment.getFromPosition() * totalLength);
          final Node segmentEnd = startNode.addNode(dynamicSegment.getToPosition() * totalLength);

          for (Node node = segmentStart; node != segmentEnd; node = node.next()) {
            node.nextSegment.applyReference(dynamicSegment);
          }
        }
      }
    }
  }

  protected static class Node {
    protected final double positionX;
    protected final double positionY;

    protected Segment nextSegment;

    public Node(final double positionX, final double positionY) {
      this.positionX = positionX;
      this.positionY = positionY;
    }

    public boolean hasNext() {
      return nextSegment != null;
    }

    public Node next() {
      return nextSegment == null ? null : nextSegment.end;
    }

    public double distanceToNext() {
      return nextSegment == null ? -1 : nextSegment.length();
    }

    public double totalLength() {
      double totalLength = 0;

      for (Node node = this; node.hasNext(); node = node.next()) {
        totalLength += node.distanceToNext();
      }

      return totalLength;
    }

    public Node addNode(final double distance) {
      Node returnNode = null;
      if (Math.abs(distance) <= MIN_DISTANCE) {
        returnNode = this;
      } else {
        double distanceToTravel = distance;

        for (Node node = this; node.hasNext(); node = node.next()) {
          final double distanceToNext = node.distanceToNext();

          if (Math.abs(distanceToTravel - distanceToNext) < MIN_DISTANCE) {
            returnNode = node.next();
            break;
          }
          if (distanceToTravel < distanceToNext) {
            final double factor = distanceToTravel / distanceToNext;

            returnNode = node.nextSegment.splitSegment(factor);
            break;
          } else {
            distanceToTravel -= distanceToNext;
          }
        }
      }
      return returnNode;
    }

    @Override
    public String toString() {
      return "Node [positionX=" + positionX + ", positionY=" + positionY + " ]";
    }
  }

  protected static class Segment {
    protected final Node start;
    protected Node end;
    protected final List<SRM2LinearReference> appliedReferences = new ArrayList<>();

    public Segment(final Node start, final Node end) {
      this.start = start;
      this.end = end;
    }

    public void applyReference(final SRM2LinearReference reference) {
      appliedReferences.add(reference);
    }

    public double length() {
      final double deltaX = end.positionX - start.positionX;
      final double deltaY = end.positionY - start.positionY;

      return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    public Node splitSegment(final double factor) {
      final double positionX = (end.positionX - start.positionX) * factor + start.positionX;
      final double positionY = (end.positionY - start.positionY) * factor + start.positionY;

      final Node newNode = new Node(positionX, positionY);
      final Segment newSegment = new Segment(newNode, end);
      newSegment.appliedReferences.addAll(appliedReferences);
      newNode.nextSegment = newSegment;
      end = newNode;

      return newNode;
    }

    public Segment next() {
      return end.nextSegment;
    }

    @Override
    public String toString() {
      return "Segment [start=" + start + ", end=" + end + "]";
    }
  }

}
