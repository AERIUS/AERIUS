/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.List;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Reader for pre-srm pre-processed wind factor and speed data.
 */
class WindRoseSpeedReader extends PreSrmXYReader<WindRoseSpeed> {
  private static final String WIND_FACTOR = "wind factor";
  private static final int WIND_FACTOR_OFFSET = 2;
  private static final String AVG_WIND_SPEED = "avg wind speed";
  private static final int WIND_SPEED_OFFSET = WIND_FACTOR_OFFSET + SRMConstants.WIND_SECTORS;

  /**
   * Reads prognose wind speed expected to be generated with given pre-srm version.
   *
   * @param preSrmVersion expected pre-srm version
   */
  public WindRoseSpeedReader(final String preSrmVersion) {
    super(preSrmVersion, YearPattern.PROGNOSE, -1);
  }

  /**
   * Reads year wind speed expected to be generated with given pre-srm version and year.
   *
   * @param preSrmVersion expected pre-srm version
   * @param year expected wind year
   */
  public WindRoseSpeedReader(final String preSrmVersion, final int year) {
    super(preSrmVersion, YearPattern.YEAR, year);
  }

  @Override
  protected WindRoseSpeed parseLine(final String line, final List<AeriusException> warnings) {
    final WindRoseSpeed windrose = new WindRoseSpeed(readX(), readY());

    for (int i = 0; i < SRMConstants.WIND_SECTORS; i++) {
      windrose.setWindFactor(i, getDouble(WIND_FACTOR + i, WIND_FACTOR_OFFSET + i));
      windrose.setAvgWindSpeed(i, getDouble(AVG_WIND_SPEED + i, WIND_SPEED_OFFSET + i));
    }
    return windrose;
  }
}
