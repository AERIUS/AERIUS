/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;

/**
 * Flattens a list of SRM2 Sources ({@link SRM2RoadEmissionSource} with srm1Road = false) and writes it in a csv file.
 */
public final class CIMLKSRM2RoadWriter extends AbstractCIMLKWriter<CIMLKSRM2RoadWriter.Data> {

  private enum SRM2RoadType {
    NATIONAL_ROAD, FREEWAY, FREEWAY_STRICT_ENFORCEMENT;
  }

  protected static class Data {

    final int scale;
    final String wkt;
    final SRM2RoadEmissionSource source;
    final SRM2RoadType roadType;
    final int maximumSpeed;
    final StandardVehicles normalTraffic;
    StandardVehicles dynamicLightTraffic;

    Data(final int scale, final String wkt, final SRM2RoadEmissionSource source, final SRM2RoadType roadType, final int maximumSpeed,
        final StandardVehicles normalTraffic) {
      this.scale = scale;
      this.wkt = wkt;
      this.source = source;
      this.roadType = roadType;
      this.maximumSpeed = maximumSpeed;
      this.normalTraffic = normalTraffic;
    }

  }

  private static class Column extends GenericColumn<Data> {

    public Column(final String header, final Function<Data, String> function) {
      super(header, function);
    }

  }

  private static class SourceColumn extends Column {

    public SourceColumn(final String header, final Function<SRM2RoadEmissionSource, String> function) {
      super(header, data -> function.apply(data.source));
    }

  }

  private static class IntensityTrafficColumn extends Column {

    public IntensityTrafficColumn(final String header, final VehicleType vehicleType) {
      super(header, data -> data.normalTraffic.getValuesPerVehicleTypes().containsKey(vehicleType.getStandardVehicleCode())
          ? roundedValue(data.scale,
              intensityPerDay(data.normalTraffic.getTimeUnit(),
                  data.normalTraffic.getValuesPerVehicleTypes().get(vehicleType.getStandardVehicleCode())))
          : "");
    }

  }

  private static class StagnationFactorTrafficColumn extends Column {

    public StagnationFactorTrafficColumn(final String header, final VehicleType vehicleType) {
      super(header, data -> data.normalTraffic.getValuesPerVehicleTypes().containsKey(vehicleType.getStandardVehicleCode())
          ? roundedValue(data.scale, data.normalTraffic.getValuesPerVehicleTypes().get(vehicleType.getStandardVehicleCode()).getStagnationFraction())
          : "");
    }

  }

  private static class BarrierLeftColumn extends Column {

    public BarrierLeftColumn(final String header, final Function<SRM2RoadSideBarrier, Double> function) {
      super(header, data -> data.source.getBarrierLeft() == null
          ? ""
          : roundedValue(data.scale, function.apply(data.source.getBarrierLeft())));
    }

  }

  private static class BarrierRightColumn extends Column {

    public BarrierRightColumn(final String header, final Function<SRM2RoadSideBarrier, Double> function) {
      super(header, data -> data.source.getBarrierRight() == null
          ? ""
          : roundedValue(data.scale, function.apply(data.source.getBarrierRight())));
    }

  }

  private static final String LIGHT_TRAFFIC_CODE = VehicleType.LIGHT_TRAFFIC.getStandardVehicleCode();

  private static final Column[] COLUMNS = new Column[] {
      new SourceColumn("srm2_road_id", source -> source.getGmlId()),
      new SourceColumn("jurisdiction_id", source -> source.getJurisdictionId() == null ? "" : String.valueOf(source.getJurisdictionId())),
      new SourceColumn("label", source -> source.getLabel()),
      new SourceColumn("road_manager", source -> source.getRoadManager() == null ? "" : source.getRoadManager().name()),
      new Column("elevation_height", data -> String.valueOf(data.source.getElevationHeight())),
      new Column("category", data -> data.roadType.name()),
      new Column("tunnel_factor", data -> roundedValue(data.scale, data.source.getTunnelFactor())),
      new Column("max_speed", data -> String.valueOf(data.maximumSpeed)),
      new Column("dynamic_max_speed", data -> data.dynamicLightTraffic == null
          ? ""
          : String.valueOf(getMaximumSpeed(data.dynamicLightTraffic))),
      new BarrierLeftColumn("barrier_left_distance", barrier -> barrier.getDistance()),
      new BarrierLeftColumn("barrier_left_height", barrier -> barrier.getHeight()),
      new BarrierRightColumn("barrier_right_distance", barrier -> barrier.getDistance()),
      new BarrierRightColumn("barrier_right_height", barrier -> barrier.getHeight()),
      new IntensityTrafficColumn("light_traffic_intensity", VehicleType.LIGHT_TRAFFIC),
      new Column("light_traffic_dynamic_intensity", data -> data.dynamicLightTraffic == null
          ? ""
          : roundedValue(data.scale, intensityPerDay(data.dynamicLightTraffic.getTimeUnit(),
              data.dynamicLightTraffic.getValuesPerVehicleTypes().get(LIGHT_TRAFFIC_CODE)))),
      new StagnationFactorTrafficColumn("light_traffic_stagnation_factor", VehicleType.LIGHT_TRAFFIC),
      new IntensityTrafficColumn("normal_freight_intensity", VehicleType.NORMAL_FREIGHT),
      new StagnationFactorTrafficColumn("normal_freight_stagnation_factor", VehicleType.NORMAL_FREIGHT),
      new IntensityTrafficColumn("heavy_freight_intensity", VehicleType.HEAVY_FREIGHT),
      new StagnationFactorTrafficColumn("heavy_freight_stagnation_factor", VehicleType.HEAVY_FREIGHT),
      new IntensityTrafficColumn("auto_bus_intensity", VehicleType.AUTO_BUS),
      new StagnationFactorTrafficColumn("auto_bus_stagnation_factor", VehicleType.AUTO_BUS),
      new SourceColumn("description", source -> source.getDescription()),
      new Column("geometry", data -> data.wkt),
  };

  private static final List<GenericColumn<Data>> DEFAULT_COLUMNS = Collections.unmodifiableList(Arrays.asList(COLUMNS));

  private final int scale;

  public CIMLKSRM2RoadWriter(final int scale) {
    this.scale = scale;
  }

  @Override
  List<GenericColumn<Data>> columns() {
    return DEFAULT_COLUMNS;
  }

  /**
   * write row of data to the writer (does not close the writer);
   */
  public void writeRow(final Writer writer, final EmissionSourceFeature source) throws IOException {
    if (source.getProperties() instanceof SRM2RoadEmissionSource) {
      final String wkt = toWkt(source.getGeometry());
      writeRows(writer, wkt, (SRM2RoadEmissionSource) source.getProperties());
    }
  }

  public void write(final File outputFile, final List<EmissionSourceFeature> sources) throws IOException {
    try (final Writer writer = Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8)) {
      writeHeader(writer);
      for (final EmissionSourceFeature source : sources) {
        writeRow(writer, source);
      }
    }
  }

  private void writeRows(final Writer writer, final String geometry, final SRM2RoadEmissionSource source) throws IOException {
    final List<Data> datas = new ArrayList<>();
    final List<StandardVehicles> standardVehicles = source.getSubSources().stream()
        .filter(StandardVehicles.class::isInstance)
        .map(StandardVehicles.class::cast)
        .collect(Collectors.toList());
    for (final StandardVehicles vehicle : standardVehicles) {
      handleData(source, geometry, vehicle, datas);
    }

    for (final Data data : datas) {
      super.writeRow(writer, data);
    }
  }

  private void handleData(final SRM2RoadEmissionSource source, final String wkt, final StandardVehicles vehicle, final List<Data> datas) {
    final SRM2RoadType roadType = RoadType.FREEWAY.getRoadTypeCode().equals(source.getRoadTypeCode())
        ? (vehicle.getStrictEnforcement() ? SRM2RoadType.FREEWAY_STRICT_ENFORCEMENT : SRM2RoadType.FREEWAY)
        : SRM2RoadType.NATIONAL_ROAD;
    final Optional<Data> targetDataDynamic = datas.stream()
        .filter(data -> data.roadType == roadType
            && data.dynamicLightTraffic == null)
        .findFirst();
    if (canBeDynamic(vehicle) && targetDataDynamic.isPresent()) {
      targetDataDynamic.get().dynamicLightTraffic = vehicle;
    } else {
      final Data correctData = new Data(scale, wkt, source, roadType, getMaximumSpeed(vehicle), vehicle);
      datas.add(correctData);
    }
  }

  private boolean canBeDynamic(final StandardVehicles vehicle) {
    final boolean hasLightTraffic = vehicle.getValuesPerVehicleTypes().containsKey(LIGHT_TRAFFIC_CODE)
        && vehicle.getValuesPerVehicleTypes().get(LIGHT_TRAFFIC_CODE).getVehiclesPerTimeUnit() > 0;
    boolean hasNoOtherTraffic = true;
    for (final Entry<String, ValuesPerVehicleType> entry : vehicle.getValuesPerVehicleTypes().entrySet()) {
      if (!LIGHT_TRAFFIC_CODE.equalsIgnoreCase(entry.getKey()) && entry.getValue().getVehiclesPerTimeUnit() > 0) {
        hasNoOtherTraffic = false;
      }
    }
    return hasLightTraffic && hasNoOtherTraffic
        && Double.compare(vehicle.getValuesPerVehicleTypes().get(LIGHT_TRAFFIC_CODE).getStagnationFraction(), 0.0) == 0;
  }

  private static int getMaximumSpeed(final StandardVehicles vehicle) {
    return vehicle.getMaximumSpeed() == null ? 0 : vehicle.getMaximumSpeed();
  }

  private static double intensityPerDay(final TimeUnit timeUnit, final ValuesPerVehicleType valuesPerVehicleType) {
    return timeUnit.toUnit(valuesPerVehicleType.getVehiclesPerTimeUnit(), TimeUnit.DAY);
  }

}
