/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import java.nio.Buffer;
import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLKernel;
import com.jogamp.opencl.CLMemory;

import nl.overheid.aerius.opencl.Connection;
import nl.overheid.aerius.opencl.Context;
import nl.overheid.aerius.opencl.MemoryUsage;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.SRMDepositionVelocityMap;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;
import nl.overheid.aerius.srm.backgrounddata.WindRoseSpeed;
import nl.overheid.aerius.srm.backgrounddata.WindRoseSpeedGrid;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

/**
 * Calculation context for a substance. Manages the process and is initialized with a substance specific context.
 */
class CalculationContext {
  private static final Logger LOG = LoggerFactory.getLogger(CalculationContext.class);

  /**
   * Empty array to initialize double windrose buffer. This array should never be filled with data.
   */
  private static final double[] EMPTY_DOUBLE_WINDROSE = new double[SRMConstants.WIND_SECTORS * 2];

  private static final int CALCULATION_WORK_ARG_IDX = 5;
  private static final int CALCULATION_RECEPTOR_POSITION_ARG_IDX = 6;
  private static final int CALCULATION_RECEPTOR_WIND_ARG_IDX = 7;
  private static final int CALCULATION_RECEPTOR_HEIGHT_ARG_IDX = 8;
  private static final int CALCULATION_RECEPTOR_COUNT_ARG_IDX = 9;

  private static final int SUM_RESULT_ARG_IDX = 1;
  private static final int SUM_WORK_ARG_IDX = 2;
  private static final int SUM_WORK_SIZE_ARG_IDX = 3;

  /**
   * Amount of extra bytes to subtract from total available memory to calculate available space for number of receptors
   * per block to calculate. This number was empirically derived by testing the tipping point. It's not clear why this
   * number is needed in the first place, but it seem to make things work. Without this additional reserved space the
   * calculation can crash on large data sets after the first batch with an out of memory error by the GPU.
   */
  private static final long RESERVED_SPACE = 40_000_000;

  private static final double DEFAULT_RECEPTOR_HEIGHT = 1.5;

  private final Substance substance;

  private CLBuffer<DoubleBuffer> receptorWindBuffer;
  private CLBuffer<DoubleBuffer> receptorPositionBuffer;
  private CLBuffer<DoubleBuffer> receptorDepositionVelocityBuffer;
  private CLBuffer<DoubleBuffer> receptorHeightBuffer;
  private CLBuffer<DoubleBuffer> resultBuffer;

  private final SourceData sourceData;

  private final int year;
  private final Meteo meteo;
  private final ArrayList<AeriusResultPoint> receptors;
  private final HasGridValue<WindRoseConcentration> windRoseConcentrationGrid;
  private final WindRoseSpeedGrid windRoseSpeedGrid;
  private final SRMDepositionVelocityMap srmDepositionVelocityMap;
  private final CommonCalculationContext commonCalculationContext;
  private final SubstanceCalculationContext substanceCalculationContext;

  private final AeriusSRMVersion srmVersion;
  private final boolean calculateDeposition;

  public CalculationContext(final SourceData sourceData, final int year, final Meteo meteo,
      final Substance substance, final Collection<SRM2RoadSegment> sources, final ArrayList<AeriusResultPoint> receptors,
      final SubstanceCalculationContext customCalculationContext, final boolean calculateDeposition, final AeriusSRMVersion srmVersion)
      throws AeriusException {

    this.sourceData = sourceData;
    this.year = year;
    this.meteo = meteo;
    this.substance = substance;
    this.receptors = receptors;
    this.calculateDeposition = calculateDeposition;
    this.commonCalculationContext = new CommonCalculationContext(sourceData, sources);
    this.substanceCalculationContext = customCalculationContext;
    this.srmVersion = srmVersion;
    this.srmDepositionVelocityMap = sourceData.getPreSRMData().getDepositionVelocityMap(srmVersion, substance);
    this.windRoseSpeedGrid = sourceData.getPreSRMData().getWindRoseSpeedGrid(srmVersion, meteo);
    this.windRoseConcentrationGrid = sourceData.getPreSRMData().getWindRoseConcentrationGrid(srmVersion, meteo, year);
  }

  public final void runCalculation(final Context context, final long connectionTimeout) {
    final CLKernel calculationKernel;
    final CLKernel sumKernel;
    try (final Connection connection = context.getConnection(connectionTimeout, TimeUnit.MILLISECONDS)) {
      if (connection == null) {
        throw new IllegalStateException("Calculation timed out while waiting for OpenCL connection.");
      }
      calculationKernel = connection.getKernel(sourceData.getProgramSource(), substanceCalculationContext.getCalculationKernelName());
      sumKernel = connection.getKernel(sourceData.getProgramSource(), substanceCalculationContext.getSumKernelName());
      commonCalculationContext.initBuffers(srmVersion, connection, calculationKernel, substance);
      commonCalculationContext.initSourceBuffers(connection, calculationKernel, sumKernel, substance);

      final int batchSize = getBatchSize(connection);

      LOG.trace("Starting calculation for {}, using a batch size of {}.", substance, batchSize);
      initReceptorBuffers(connection, calculationKernel, batchSize);
      substanceCalculationContext.initReceptorBuffers(connection, calculationKernel, sumKernel, batchSize,
          receptorPositionBuffer, receptorDepositionVelocityBuffer, receptorWindBuffer);
      initWorkBuffers(connection, calculationKernel, sumKernel, batchSize);

      printMemSize(calculationKernel, "calculationKernel");
      printMemSize(sumKernel, "sumKernel");

      int batchCount = 0;
      final int size = receptors.size();

      for (int start = 0; start < size; start += batchSize) {
        final int end = Math.min(start + batchSize, size);
        final int actualBatchSize = end - start;
        LOG.trace("Running batch {}/{} for {} receptors.", batchCount++, size / batchSize, actualBatchSize);

        prepareBatch(connection, start, end);
        substanceCalculationContext.prepareBatch(connection);
        substanceCalculationContext.runBatch(connection, calculationKernel, sumKernel,
            commonCalculationContext.getNumberOfSources() * actualBatchSize, actualBatchSize);
        connection.downloadBuffer(resultBuffer);
        LOG.debug("Downloaded results {} - {}.", start, end);
        for (int i = 0; i < actualBatchSize; i++) {
          substanceCalculationContext.downloadBatchResults(
              receptors.get(start + i), resultBuffer.getBuffer(), substanceCalculationContext.getWorkSize() * i);
        }
      }
    }
    releaseKernel(calculationKernel, "calculationKernel");
    releaseKernel(sumKernel, "sumKernel");
  }

  private static void releaseKernel(final CLKernel kernel, final String kernelName) {
    if (!kernel.isReleased()) {
      LOG.trace("Release {}", kernelName);
      kernel.release();
    }
  }

  private WindRoseSpeed getWindRoseSpeed(final WindRoseSpeedGrid windRoseSpeedGrid, final double x, final double y) {
    final WindRoseSpeed windRose = windRoseSpeedGrid.get(x, y);

    if (windRose == null) {
      LOG.error("Missing windrose for {}, {} in {}.", x, y, meteo);
    }
    return windRose;
  }

  private WindRoseConcentration getWindRoseConcentration(final HasGridValue<WindRoseConcentration> windRoseGrid, final double x, final double y) {
    final WindRoseConcentration windRose = windRoseGrid.get(x, y);

    if (windRose == null) {
      LOG.error("Missing windrose for {}, {} in {}.", x, y, meteo);
    }
    return windRose;
  }

  private int getBatchSize(final Connection con) {
    // The number of bytes in a buffer is specified as an integer.
    final long maxBufferSize = Math.min(con.getMaxBufferSize(), Integer.MAX_VALUE) - RESERVED_SPACE;
    final long availableMemory = maxBufferSize - commonCalculationContext.getBufferSize();

    if (availableMemory <= 0) {
      throw new OutOfMemoryError("Insufficient memory for srm2 calculation.");
    }
    final long sourceWorkSize = getWorkSize() * SRMConstants.DOUBLE_BYTES;
    final long maxBatchSizeBySources = maxBufferSize / sourceWorkSize;
    final long batchSizeInBytes = substanceCalculationContext.getAdditionalBatchSizeInBytes()
        + sourceWorkSize
        + (substanceCalculationContext.getWorkSize() + SRMConstants.RECEPTOR_WIND_SECTOR_DOUBLES + SRMConstants.RECEPTOR_POS_DOUBLES
        + SRMConstants.RECEPTOR_DEPOSITION_VELOCITY_DOUBLES + SRMConstants.RECEPTOR_HEIGHT_DOUBLES) * SRMConstants.DOUBLE_BYTES;
    final long maxBatchSize = availableMemory / batchSizeInBytes;
    final int batchSize = (int) Math.min(maxBatchSizeBySources, Math.min(receptors.size(), maxBatchSize));

    if (LOG.isTraceEnabled()) {
      LOG.trace("MaxGlobalMemSize:{}, application buffer size:{}", con.getMaxGlobalMemSize(), commonCalculationContext.getBufferSize());
      LOG.trace("max int:{}, max alloc:{}, sourceWorkSize:{}, maxBufferSize:{}",
          Integer.MAX_VALUE, con.getMaxBufferSize(), sourceWorkSize, maxBufferSize);
      LOG.trace("Batchsize {}, available memory:{}, receptors:{}, by memory:{}, batchSizeInBytes:{}, by sources:{}",
          batchSize, availableMemory, receptors.size(), maxBatchSize, batchSizeInBytes, maxBatchSizeBySources);
    }

    if (maxBatchSize <= 0) {
      throw new RuntimeException("Not enough memory. Reduce number of road segments.");
    }
    return batchSize;
  }

  /**
   * Size of the work buffer in doubles. (size in bytes is times size of a double (8 bytes)).
   *
   * @return size of work buffer.
   */
  private long getWorkSize() {
    return (long) commonCalculationContext.getNumberOfSources() * substanceCalculationContext.getWorkSize();
  }

  private void printMemSize(final CLKernel kernel, final String name) {
    long cnt = 0;
    for (final CLMemory<? extends Buffer> clMemory : kernel.getContext().getMemoryObjects()) {
      cnt += clMemory.getNIOSize();
    }
    LOG.trace("Allocated size for {} = {}", name, cnt);
  }

  private void initReceptorBuffers(final Connection con, final CLKernel calculationKernel, final int batchSize) {
    receptorPositionBuffer = con.createDoubleCLBuffer(batchSize * SRMConstants.RECEPTOR_POS_DOUBLES, MemoryUsage.READ_ONLY);
    receptorDepositionVelocityBuffer = con.createDoubleCLBuffer(batchSize * SRMConstants.RECEPTOR_DEPOSITION_VELOCITY_DOUBLES, MemoryUsage.READ_ONLY);
    receptorWindBuffer = con.createDoubleCLBuffer(batchSize * SRMConstants.RECEPTOR_WIND_SECTOR_DOUBLES, MemoryUsage.READ_ONLY);
    receptorHeightBuffer = con.createDoubleCLBuffer(batchSize * SRMConstants.RECEPTOR_HEIGHT_DOUBLES, MemoryUsage.READ_ONLY);

    calculationKernel.setArg(CALCULATION_RECEPTOR_POSITION_ARG_IDX, receptorPositionBuffer);
    calculationKernel.setArg(CALCULATION_RECEPTOR_WIND_ARG_IDX, receptorWindBuffer);
    calculationKernel.setArg(CALCULATION_RECEPTOR_HEIGHT_ARG_IDX, receptorHeightBuffer);
    calculationKernel.setArg(CALCULATION_RECEPTOR_COUNT_ARG_IDX, batchSize);
  }

  private void initWorkBuffers(final Connection con, final CLKernel calculationKernel, final CLKernel sumKernel, final int batchSize) {
    final long sourceBatchSize = batchSize * getWorkSize();
    final CLBuffer<DoubleBuffer> workBuffer = con.createDoubleCLBuffer((int) sourceBatchSize, MemoryUsage.READ_WRITE);
    resultBuffer = con.createDoubleCLBuffer(batchSize * substanceCalculationContext.getWorkSize(), MemoryUsage.READ_WRITE);
    calculationKernel.setArg(CALCULATION_WORK_ARG_IDX, workBuffer);

    sumKernel.setArg(SUM_RESULT_ARG_IDX, resultBuffer);
    sumKernel.setArg(SUM_WORK_ARG_IDX, workBuffer);
    sumKernel.setArg(SUM_WORK_SIZE_ARG_IDX, batchSize);
  }

  private void prepareBatch(final Connection con, final int start, final int end) {
    LOG.debug("Filling buffers for receptors {} - {}.", start, end);

    receptorPositionBuffer.getBuffer().clear();
    receptorDepositionVelocityBuffer.getBuffer().clear();
    receptorHeightBuffer.getBuffer().clear();

    for (int i = start; i < end; i++) {
      final AeriusResultPoint receptorPoint = receptors.get(i);

      final double x = receptorPoint.getRoundedCmX();
      final double y = receptorPoint.getRoundedCmY();
      receptorPositionBuffer.getBuffer().put(x);
      receptorPositionBuffer.getBuffer().put(y);

      final int receptorId = receptorPoint.getId();

      setDepositionVelocity(receptorPoint, receptorId);

      setHeight(receptorPoint);

      prepareBatchWindRoseSpeedBuffers(getWindRoseSpeed(windRoseSpeedGrid, x, y));
      substanceCalculationContext.prepareBatchWindRoseBuffers(getWindRoseConcentration(windRoseConcentrationGrid, x, y));
    }
    con.rewindAndUploadBuffer(receptorPositionBuffer);
    con.rewindAndUploadBuffer(receptorDepositionVelocityBuffer);
    con.rewindAndUploadBuffer(receptorWindBuffer);
    con.rewindAndUploadBuffer(receptorHeightBuffer);
    // Reset the results to 0.
    clearBuffer(resultBuffer.getBuffer());
    con.rewindAndUploadBuffer(resultBuffer);
  }

  private void setDepositionVelocity(final AeriusResultPoint receptorPoint, final int receptorId) {
    final Double depositionVelocity;
    if (!calculateDeposition || srmDepositionVelocityMap == null) {
      depositionVelocity = 0.0;
    } else {
      depositionVelocity = srmDepositionVelocityMap.getDepositionVelocity(receptorPoint);

      if (depositionVelocity == null) {
        LOG.warn("No deposition velocity found {} <{}, {}> for {}.", receptorId, receptorPoint.getX(), receptorPoint.getY(), substance);
      }
    }
    receptorDepositionVelocityBuffer.getBuffer().put(depositionVelocity == null ? 0.0 : depositionVelocity);
  }

  private void setHeight(final AeriusResultPoint receptorPoint) {
    receptorHeightBuffer.getBuffer().put(receptorPoint.getHeight() == null ? DEFAULT_RECEPTOR_HEIGHT : receptorPoint.getHeight());
  }

  private void prepareBatchWindRoseSpeedBuffers(final WindRoseSpeed windRose) {
    if (windRose == null) {
      receptorWindBuffer.getBuffer().put(EMPTY_DOUBLE_WINDROSE, 0, EMPTY_DOUBLE_WINDROSE.length);
    } else {
      for (int j = 0; j < SRMConstants.WIND_SECTORS; j++) {
        receptorWindBuffer.getBuffer().put(windRose.getWindFactor(j));
        receptorWindBuffer.getBuffer().put(windRose.getAvgWindSpeed(j));
      }
    }
  }

  private void clearBuffer(final DoubleBuffer buffer) {
    buffer.clear(); // clear only rewinds, so fill will 0 to actually clear buffer.
    for (int i = 0; i < buffer.capacity(); i++) {
      buffer.put(0);
    }
  }
}
