/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.base.EmissionReduction;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasure;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicleMeasure;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Reader for CIMLK measure files.
 *
 * Available columns:
 * <pre>
 * measure_id;jurisdiction_id;label;substance;factor;vehicle_type;speed_type;geometry
 * </pre>
 */
class CIMLKMeasureFileReader extends AbstractCIMLKFileReader<CIMLKMeasureFeature> {

  // @formatter:off
  private enum MeasureColumns implements Columns {
    MEASURE_ID,
    JURISDICTION_ID,
    LABEL,
    SUBSTANCE,
    FACTOR,
    VEHICLE_TYPE,
    SPEED_PROFILE,
    DESCRIPTION,
    GEOMETRY;
    // @formatter:on

    static MeasureColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  private final Map<String, CIMLKMeasureFeature> measureIdCache = new HashMap<>();

  @Override
  protected CIMLKMeasureFeature parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final CIMLKMeasureFeature measure;
    final String measureId = getGmlId(MeasureColumns.MEASURE_ID, GMLIdUtil.MEASURE_PREFIX, warnings);

    boolean existingMeasure = false;
    if (measureIdCache.containsKey(measureId)) {
      measure = measureIdCache.get(measureId);
      validateWithExistingMeasure(measure);
      existingMeasure = true;
    } else {
      measure = constructMeasure(measureId);
      measureIdCache.put(measureId, measure);
    }

    final StandardVehicleMeasure vehicleMeasure = determineVehicleMeasure(measure);
    vehicleMeasure.addEmissionReduction(determineReduction(measureId));

    return existingMeasure ? null : measure;
  }

  private CIMLKMeasureFeature constructMeasure(final String measureId) throws AeriusException {
    final CIMLKMeasureFeature feature = new CIMLKMeasureFeature();
    final CIMLKMeasure measure = new CIMLKMeasure();
    measure.setGmlId(measureId);
    measure.setJurisdictionId(getInt(MeasureColumns.JURISDICTION_ID));
    measure.setLabel(getString(MeasureColumns.LABEL));
    measure.setDescription(getString(MeasureColumns.DESCRIPTION));
    feature.setProperties(measure);
    feature.setGeometry((Polygon) getGeometry(MeasureColumns.GEOMETRY, geometry -> geometry.type() != GeometryType.POLYGON));
    return feature;
  }

  private StandardVehicleMeasure determineVehicleMeasure(final CIMLKMeasureFeature feature) throws AeriusException {
    final CIMLKMeasure measure = feature.getProperties();
    final String vehicleType = getEnumValue(VehicleType.class, MeasureColumns.VEHICLE_TYPE, measure.getGmlId()).getStandardVehicleCode();
    final String roadTypeCode = getEnumValue(RoadSpeedType.class, MeasureColumns.SPEED_PROFILE, measure.getGmlId()).getRoadTypeCode();
    return measure.getVehicleMeasures().stream()
        .filter(vehicleMeasure -> vehicleMeasure.getVehicleTypeCode().equalsIgnoreCase(vehicleType)
            && vehicleMeasure.getRoadTypeCode().equalsIgnoreCase(roadTypeCode))
        .findFirst()
        .orElseGet(() -> createNewVehicleMeasure(measure, vehicleType, roadTypeCode));
  }

  private StandardVehicleMeasure createNewVehicleMeasure(final CIMLKMeasure measure, final String vehicleTypeCode, final String roadTypeCode) {
    final StandardVehicleMeasure vehicleMeasure = new StandardVehicleMeasure();
    vehicleMeasure.setVehicleTypeCode(vehicleTypeCode);
    vehicleMeasure.setRoadTypeCode(roadTypeCode);
    measure.getVehicleMeasures().add(vehicleMeasure);
    return vehicleMeasure;
  }

  private EmissionReduction determineReduction(final String id) throws AeriusException {
    final EmissionReduction reduction = new EmissionReduction();
    reduction.setSubstance(getEnumValue(Substance.class, MeasureColumns.SUBSTANCE, id));
    reduction.setFactor(getDouble(MeasureColumns.FACTOR));
    return reduction;
  }

  private void validateWithExistingMeasure(final CIMLKMeasureFeature existingMeasureFeature) throws AeriusException {
    final CIMLKMeasure existingMeasure = existingMeasureFeature.getProperties();
    if (existingMeasure.getJurisdictionId() != getInt(MeasureColumns.JURISDICTION_ID)
        || !StringUtils.equals(existingMeasure.getLabel(), getString(MeasureColumns.LABEL))
        || !StringUtils.equals(existingMeasure.getDescription(), getString(MeasureColumns.DESCRIPTION))
        || !GeometryUtil.getGeometry(existingMeasureFeature.getGeometry()).equalsTopo(GeometryUtil.getGeometry(getString(MeasureColumns.GEOMETRY)))) {
      throw new AeriusException(ImaerExceptionReason.SRM_MEASURE_RECORDS_DID_NOT_MATCH, existingMeasure.getGmlId());
    }
  }

  @Override
  Columns[] expectedColumns() {
    return MeasureColumns.values();
  }

  @Override
  Columns safeColumnOf(final String value) {
    return MeasureColumns.safeValueOf(value);
  }

}
