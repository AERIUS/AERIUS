/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import nl.overheid.aerius.shared.domain.v2.base.EmissionReduction;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasure;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicleMeasure;

/**
 * Flattens a list of measures ({@link CIMLKMeasure}) and writes it in a csv file.
 */
public final class CIMLKMeasureWriter extends AbstractCIMLKWriter<CIMLKMeasureWriter.Data> {

  protected static class Data {

    final int scale;
    final CIMLKMeasure measure;
    final StandardVehicleMeasure vehicleMeasure;
    final EmissionReduction reduction;
    final String wkt;

    Data(final int scale, final CIMLKMeasure measure, final StandardVehicleMeasure vehicleMeasure, final EmissionReduction reduction,
        final String wkt) {
      this.scale = scale;
      this.measure = measure;
      this.vehicleMeasure = vehicleMeasure;
      this.reduction = reduction;
      this.wkt = wkt;
    }

  }

  private static class Column extends GenericColumn<Data> {

    public Column(final String header, final Function<Data, String> function) {
      super(header, function);
    }

  }

  private static class MeasureColumn extends Column {

    public MeasureColumn(final String header, final Function<CIMLKMeasure, String> function) {
      super(header, data -> function.apply(data.measure));
    }

  }

  private static class VehicleMeasureColumn extends Column {

    public VehicleMeasureColumn(final String header, final Function<StandardVehicleMeasure, String> function) {
      super(header, data -> function.apply(data.vehicleMeasure));
    }

  }

  private static class ReductionColumn extends Column {

    public ReductionColumn(final String header, final Function<EmissionReduction, String> function) {
      super(header, data -> function.apply(data.reduction));
    }

  }

  private static final Column[] COLUMNS = new Column[] {
      new MeasureColumn("measure_id", measure -> measure.getGmlId()),
      new MeasureColumn("jurisdiction_id", measure -> measure.getJurisdictionId() == null
          ? ""
          : String.valueOf(measure.getJurisdictionId())),
      new MeasureColumn("label", measure -> measure.getLabel()),
      new ReductionColumn("substance", reduction -> reduction.getSubstance().getName()),
      new Column("factor", data -> roundedValue(data.scale, data.reduction.getFactor())),
      new VehicleMeasureColumn("vehicle_type", measure -> measure.getVehicleTypeCode()),
      new VehicleMeasureColumn("speed_profile", measure -> speedProfile(measure.getRoadTypeCode())),
      new MeasureColumn("description", measure -> measure.getDescription()),
      new Column("geometry", data -> data.wkt),

  };

  private static final List<GenericColumn<Data>> DEFAULT_COLUMNS = Collections.unmodifiableList(Arrays.asList(COLUMNS));

  private final int scale;

  public CIMLKMeasureWriter(final int scale) {
    this.scale = scale;
  }

  @Override
  List<GenericColumn<Data>> columns() {
    return DEFAULT_COLUMNS;
  }

  /**
   * write row of data to the writer (does not close the writer);
   */
  public void writeRow(final Writer writer, final CIMLKMeasureFeature feature) throws IOException {
    final String wkt = toWkt(feature.getGeometry());
    final CIMLKMeasure measure = feature.getProperties();
    for (final StandardVehicleMeasure vehicleMeasure : measure.getVehicleMeasures()) {
      for (final EmissionReduction reduction : vehicleMeasure.getEmissionReductions()) {
        super.writeRow(writer, new Data(scale, measure, vehicleMeasure, reduction, wkt));
      }
    }

  }

  public void write(final File outputFile, final List<CIMLKMeasureFeature> measures) throws IOException {
    try (final Writer writer = Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8)) {
      writeHeader(writer);
      for (final CIMLKMeasureFeature measure : measures) {
        writeRow(writer, measure);
      }
    }
  }

  private static String speedProfile(final String roadAreaCode) {
    for (final RoadSpeedType speedType : RoadSpeedType.values()) {
      if (speedType.getRoadTypeCode().equalsIgnoreCase(roadAreaCode)) {
        return speedType.name();
      }
    }
    return roadAreaCode;
  }
}
