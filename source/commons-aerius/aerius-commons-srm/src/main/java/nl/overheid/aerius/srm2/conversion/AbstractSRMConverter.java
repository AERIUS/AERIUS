/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

public abstract class AbstractSRMConverter<E extends EngineSource> {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractSRMConverter.class);

  private static final BigDecimal EMISSION_IN_G_PER_S_FACTOR = BigDecimal.valueOf(Substance.EMISSION_IN_G_PER_S_FACTOR);

  private static final BigDecimal ONE_CENTIMETER = BigDecimal.valueOf(0.01);

  abstract boolean canConvert(EmissionSource source);

  abstract List<E> convert(LineString geometry, EmissionSource source, List<Substance> substances) throws AeriusException;

  protected void copyEmissions(final Map<Substance, Double> emissionPerMeter, final EngineEmissionSource roadSegment,
      final Optional<Double> tunnelFactor) {
    emissionPerMeter.forEach((k,v) -> roadSegment.setEmission(k, v * tunnelFactor.orElse(1.0)));
  }

  /**
   * Gets the emissions per meter.
   *
   * @param geometry geometry of the line segment
   * @param source emission source
   * @param substances substances to calculate the emissions per meter
   * @return map with emissions per meter by substance
   * @throws AeriusException
   */
  protected Map<Substance, Double> getEmissionPerMeter(final LineString geometry, final EmissionSource source, final List<Substance> substances)
      throws AeriusException {
    final Map<Substance, Double> emissions = new EnumMap<>(Substance.class);
    // Assume emissions are always total for the source
    final double measure = BigDecimal.valueOf(GeometryUtil.getGeometry(geometry).getLength()).setScale(2, RoundingMode.HALF_UP)
        .max(ONE_CENTIMETER).doubleValue();

    for (final Substance substance : substances) {
      if (source.getEmissions().containsKey(substance)) {
        final double emission = source.getEmissions().get(substance);

        if (emission > 0) {
          emissions.put(substance, BigDecimal.valueOf(emission)
              .divide(EMISSION_IN_G_PER_S_FACTOR.multiply(BigDecimal.valueOf(measure)), MathContext.DECIMAL64).doubleValue());
          LOG.trace("substance:{}, emission: {}, measure:{}, new:{}", substance, emission, measure, emissions.get(substance));
        }
      }
    }
    return emissions;
  }

}
