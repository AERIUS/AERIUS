/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import java.nio.DoubleBuffer;
import java.util.Set;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLKernel;

import nl.overheid.aerius.opencl.Connection;
import nl.overheid.aerius.opencl.MemoryUsage;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;

/**
 * Calculation context for NOx/NO2 concentration/deposition calculation.
 */
class NOX2CalculationContext implements SubstanceCalculationContext {

  /**
   * Empty array to initialize windrose buffer. This array should never be filled with data.
   */
  private static final double[] EMPTY_WINDROSE = new double[SRMConstants.WIND_SECTORS];

  private static final String CALCULATION_NOX2_KERNEL_NAME = "calculateContributionNOx2";
  private static final String SUM_NOX2_KERNEL_NAME = "sumContributionsNOx2";

  private static final int WORK_SIZE = 4;
  private static final long RECEPTOR_O3_SIZE = 1;

  private static final int CALCULATION_WIND_SECTORS_ARG_IDX = 10;

  private static final int SUM_RECEPTOR_POSITION_ARG_IDX = 4;
  private static final int SUM_RECEPTOR_WIND_ARG_IDX = 5;
  private static final int SUM_RECEPTOR_O3_ARG_IDX = 6;
  private static final int SUM_RECEPTOR_DEPOSITION_VELOCITY_ARG_IDX = 7;
  private static final int SUM_WIND_SECTORS_ARG_IDX = 8;

  private static final int RESULT_OFFSET_NOX_CONCENTRATION = 0;
  private static final int RESULT_OFFSET_NO2_DIRECT_CONCENTRATION = 1;
  private static final int RESULT_OFFSET_NO2_CONCENTRATION = 2;
  private static final int RESULT_OFFSET_NOX_DEPOSITION = 3;

  private CLBuffer<DoubleBuffer> receptorO3Buffer;

  private final Set<EmissionResultKey> emissionResultKeys;

  public NOX2CalculationContext(final Set<EmissionResultKey> emissionResultKeys) {
    this.emissionResultKeys = emissionResultKeys;
  }

  @Override
  public String getCalculationKernelName() {
    return CALCULATION_NOX2_KERNEL_NAME;
  }

  @Override
  public String getSumKernelName() {
    return SUM_NOX2_KERNEL_NAME;
  }

  @Override
  public int getWorkSize() {
    return WORK_SIZE;
  }

  @Override
  public long getAdditionalBatchSizeInBytes() {
    return RECEPTOR_O3_SIZE * SRMConstants.DOUBLE_BYTES;
  }

  @Override
  public void initReceptorBuffers(final Connection con, final CLKernel calculationKernel, final CLKernel sumKernel, final int batchSize,
      final CLBuffer<DoubleBuffer> receptorPositionBuffer, final CLBuffer<DoubleBuffer> receptorDepositionVelocityBuffer,
      final CLBuffer<DoubleBuffer> receptorWindBuffer) {
    sumKernel.setArg(SUM_RECEPTOR_POSITION_ARG_IDX, receptorPositionBuffer);
    sumKernel.setArg(SUM_RECEPTOR_DEPOSITION_VELOCITY_ARG_IDX, receptorDepositionVelocityBuffer);
    sumKernel.setArg(SUM_RECEPTOR_WIND_ARG_IDX, receptorWindBuffer);
    receptorO3Buffer = con.createDoubleCLBuffer(batchSize * SRMConstants.RECEPTOR_WIND_SECTOR_O3_DOUBLES, MemoryUsage.READ_ONLY);
    sumKernel.setArg(SUM_RECEPTOR_O3_ARG_IDX, receptorO3Buffer);
  }

  @Override
  public void prepareBatch(final Connection con) {
    con.rewindAndUploadBuffer(receptorO3Buffer);
  }

  @Override
  public void prepareBatchWindRoseBuffers(final WindRoseConcentration windrose) {
    if (windrose == null) {
      receptorO3Buffer.getBuffer().put(EMPTY_WINDROSE, 0, EMPTY_WINDROSE.length);
    } else {
      for (int j = 0; j < SRMConstants.WIND_SECTORS; j++) {
        receptorO3Buffer.getBuffer().put(windrose.getO3(j));
      }
    }
  }

  @Override
  public void runBatch(final Connection con, final CLKernel calculationKernel, final CLKernel sumKernel, final int workSize,
      final int actualBatchSize) {
    for (int i = 0; i < SRMConstants.WIND_SECTORS; i++) {
      calculationKernel.setArg(CALCULATION_WIND_SECTORS_ARG_IDX, i);
      sumKernel.setArg(SUM_WIND_SECTORS_ARG_IDX, i);
      con.queue1DKernelExecution(calculationKernel, workSize);
      con.queue1DKernelExecution(sumKernel, actualBatchSize);
    }
  }

  @Override
  public void downloadBatchResults(final AeriusResultPoint aeriusResultPoint, final DoubleBuffer buffer, final int index) {
    if (emissionResultKeys.contains(EmissionResultKey.NOX_CONCENTRATION)) {
      setData(aeriusResultPoint, EmissionResultKey.NOX_CONCENTRATION, buffer.get(index + RESULT_OFFSET_NOX_CONCENTRATION));
    }
    if (emissionResultKeys.contains(EmissionResultKey.NO2_DIRECT_CONCENTRATION)) {
      setData(aeriusResultPoint, EmissionResultKey.NO2_DIRECT_CONCENTRATION, buffer.get(index + RESULT_OFFSET_NO2_DIRECT_CONCENTRATION));
    }
    if (emissionResultKeys.contains(EmissionResultKey.NO2_CONCENTRATION)) {
      setData(aeriusResultPoint, EmissionResultKey.NO2_CONCENTRATION, buffer.get(index + RESULT_OFFSET_NO2_CONCENTRATION));
    }
    if (emissionResultKeys.contains(EmissionResultKey.NOX_DEPOSITION)) {
      setData(aeriusResultPoint, EmissionResultKey.NOX_DEPOSITION, buffer.get(index + RESULT_OFFSET_NOX_DEPOSITION));
    }
  }
}
