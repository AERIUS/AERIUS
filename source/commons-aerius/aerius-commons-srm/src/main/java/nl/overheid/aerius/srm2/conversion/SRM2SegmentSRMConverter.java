/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2LinearReference;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

public class SRM2SegmentSRMConverter extends AbstractSegmentSRMConverter<SRM2RoadSegment> {

  private static final Logger LOG = LoggerFactory.getLogger(SRM2SegmentSRMConverter.class);

  // To not compare against 0, use small number to check against.
  private static final Double NO_TUNNEL_FACTOR = 0.001;

  @Override
  boolean canConvert(final EmissionSource source) {
    return source instanceof SRM2RoadEmissionSource;
  }

  @Override
  void addSegments(final LineString geometry, final EmissionSource source, final List<Substance> substances, final List<SRM2RoadSegment> segments,
      final Node startNode) throws AeriusException {
    final SRM2RoadEmissionSource srm2Source = (SRM2RoadEmissionSource) source;
    if (srm2Source.getSubSources().isEmpty()) {
      LOG.trace("SRM2 source has no vehicles and is ignored: source:{} ", source);
    } else if (!source.getEmissions().isEmpty() && source.getEmissions().values().stream().anyMatch(value -> value > 0)) {
      final Map<Substance, Double> emissionPerMeter = getEmissionPerMeter(geometry, source, substances);
      for (Segment segment = startNode.nextSegment; segment != null; segment = segment.next()) {
        final SRM2RoadSegment roadSegment = toRoadSegment(srm2Source, segment, emissionPerMeter);

        if (roadSegment != null) {
          if (roadSegment.getLineLength() <= 0) {
            LOG.trace("SRM2 segment line length is 0, coordinates: ({} {}), ({} {}), segment ignored ignored: {} ",
                roadSegment.getStartX(), roadSegment.getStartY(), roadSegment.getEndX(), roadSegment.getEndY(), source);
          } else {
            segments.add(roadSegment);
          }
        }
      }
    } else {
      LOG.trace("SRM2 source has no emissions and is ignored: source:{} ", source);
    }
  }

  /**
   * Converts a segment to a SRM2RoadSegment.
   *
   * @param source
   *          The original source.
   * @param segment
   *          The segment of the source.
   * @param emissions
   *          The emissions per meter.
   * @return A new SRM2RoadSegment, or null if it has no emissions and can be omitted.
   */
  private SRM2RoadSegment toRoadSegment(final SRM2RoadEmissionSource source, final Segment segment, final Map<Substance, Double> emissions) {
    RoadElevation elevation = null;
    Integer elevationHeight = null;
    Double tunnelFactor = null;
    SRM2RoadSideBarrier barrierLeft = null;
    SRM2RoadSideBarrier barrierRight = null;

    for (final SRM2LinearReference segmentOverride : segment.appliedReferences) {
      elevation = verifyOverride(segment, elevation, segmentOverride.getElevation());
      elevationHeight = verifyOverride(segment, elevationHeight, segmentOverride.getElevationHeight());
      tunnelFactor = verifyOverride(segment, tunnelFactor, segmentOverride.getTunnelFactor());
      barrierLeft = verifyOverride(segment, barrierLeft, segmentOverride.getBarrierLeft());
      barrierRight = verifyOverride(segment, barrierRight, segmentOverride.getBarrierRight());
    }

    final SRM2RoadSegment roadSegment;

    if (source.getTunnelFactor() < NO_TUNNEL_FACTOR || (tunnelFactor != null && tunnelFactor < NO_TUNNEL_FACTOR)) {
      LOG.trace("SRM2 source tunnel factor 0 and therefor is ignored: {}", source);
      roadSegment = null;
    } else {
      final boolean segmentIsFreeway = RoadType.FREEWAY.getRoadTypeCode().equalsIgnoreCase(source.getRoadTypeCode());
      final RoadElevation segmentElevation = elevation == null ? source.getElevation() : elevation;
      final int segmentElevationHeight = elevationHeight == null ? source.getElevationHeight() : elevationHeight;
      final SRM2RoadSideBarrier segmentBarrierLeft = barrierLeft == null ? source.getBarrierLeft() : barrierLeft;
      final SRM2RoadSideBarrier segmentBarrierRight = barrierRight == null ? source.getBarrierRight() : barrierRight;

      roadSegment = createObject(segmentIsFreeway, segmentElevation, segmentElevationHeight, segmentBarrierLeft, segmentBarrierRight);
      roadSegment.setSegmentId(source.getGmlId());
      roadSegment.setStartX(segment.start.positionX);
      roadSegment.setStartY(segment.start.positionY);
      roadSegment.setEndX(segment.end.positionX);
      roadSegment.setEndY(segment.end.positionY);

      copyEmissions(emissions, roadSegment, Optional.ofNullable(tunnelFactor));
    }

    return roadSegment;
  }

  private <S> S verifyOverride(final Segment segment, final S currentOverride, final S newOverride) {
    final S override;

    if (newOverride == null) {
      override = currentOverride;
    } else {
      if (currentOverride == null) {
        override = newOverride;
      } else {
        throw new IllegalArgumentException("Duplicate override specified for segment <" + segment.start.positionX + ", " + segment.start.positionY
            + "> - <" + segment.end.positionX + "," + segment.end.positionY + ">");
      }
    }

    return override;
  }

}
