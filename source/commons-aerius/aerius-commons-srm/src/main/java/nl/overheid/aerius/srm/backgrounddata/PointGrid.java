/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Table to fast search wind rose information.
 */
public abstract class PointGrid<T extends GridPoint> implements HasGridValue<T> {

  private static final Logger LOG = LoggerFactory.getLogger(PointGrid.class);

  private final int offsetX;
  private final int offsetY;
  private final int diameter;
  private final int columns;
  private final int rows;
  private final T[] grid;
  private final T noDataObject;

  public PointGrid(final int columns, final int rows, final int diameter, final int offsetX, final int offsetY, final T noDataObject) {
    this.columns = columns;
    this.rows = rows;
    this.diameter = diameter;
    this.offsetX = offsetX;
    this.offsetY = offsetY;
    this.noDataObject = noDataObject;
    grid = createGrid(columns , rows);
  }

  protected abstract T[] createGrid(int columns, int rows);

  /**
   * Add all grid cells to the grid.
   *
   * @param gridCells List of cells to add
   */
  public void addAll(final List<T> gridCells) {
    gridCells.forEach(c -> grid[getIndex(c.getX(), c.getY())] = c);
  }

  public int getDiameter() {
    return diameter;
  }

  /**
   * Get the the value at the x-y coordinates.
   *
   * @param x x-coordinate
   * @param y y-coordinate
   * @return value for x-y coordinate
   * @throws IndexOutOfBoundsException when no data for given x-y coordinate
   */
  @Override
  public T get(final double x, final double y) {
    final int index = getIndex(x, y);
    final T data;

    if (index < 0) {
      LOG.debug("No index in grid '{}' for x: {}, y: {}", getClass().getName(), x, y);
      data = noDataObject;
    } else {
      data = grid[index];

      if (data == null) {
        LOG.debug("No data in grid '{}' for x: {}, y: {}", getClass().getName(), x, y);
        return noDataObject;
      }
    }
    return data;
  }

  /**
   * Returns the data or null if no data is available.
   *
   * @param x x-coordinate
   * @param y y-coordinate
   * @return the data or null
   */
  public T getOrNull(final double x, final double y) {
    final int index = getIndex(x, y);

    return index < 0 ? null : grid[index];
  }

  private int getIndex(final double x, final double y) {
    // x-coordinate of grid cell includes left border : 0 -> 999
    final int indexX = (int) Math.floor((x - offsetX) / diameter);
    // y-coordinate of grid cell includes top border: 1000 -> 1
    final int indexY = (int) Math.ceil((y - offsetY) / diameter) - 1;

    if (indexX < 0 || indexX >= columns || indexY < 0 || indexY >= rows) {
      return -1;
    } else {
      return indexY * columns + indexX;
    }
  }
}
