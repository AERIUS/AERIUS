/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata.bgconcentrations;

import nl.overheid.aerius.srm.backgrounddata.GridPoint;

public class IjmondCorrectionConcentration extends GridPoint {

  private double gcnPm10;
  private double gcnPm25;
  private double gcnEc;
  private double cHwnPm10;
  private double cHwnPm25;
  private double cHwnEc;

  public IjmondCorrectionConcentration(final int x, final int y) {
    super(x, y);
  }

  public double getGcnPm10() {
    return gcnPm10;
  }

  public void setGcnPm10(final double gcnPm10) {
    this.gcnPm10 = gcnPm10;
  }

  public double getGcnPm25() {
    return gcnPm25;
  }

  public void setGcnPm25(final double gcnPm25) {
    this.gcnPm25 = gcnPm25;
  }

  public double getcHwnPm10() {
    return cHwnPm10;
  }

  public void setcHwnPm10(final double cHwnPm10) {
    this.cHwnPm10 = cHwnPm10;
  }

  public double getcHwnPm25() {
    return cHwnPm25;
  }

  public void setcHwnPm25(final double cHwnPm25) {
    this.cHwnPm25 = cHwnPm25;
  }

  public double getGcnEc() {
    return gcnEc;
  }

  public void setGcnEc(final double gcnEc) {
    this.gcnEc = gcnEc;
  }

  public double getcHwnEc() {
    return cHwnEc;
  }

  public void setcHwnEc(final double cHwnEc) {
    this.cHwnEc = cHwnEc;
  }
}
