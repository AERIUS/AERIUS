/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opencl.CLException.CLOutOfHostMemoryException;

import nl.overheid.aerius.opencl.Constant;
import nl.overheid.aerius.opencl.Context;
import nl.overheid.aerius.opencl.DefineConstant;
import nl.overheid.aerius.opencl.FileSource;
import nl.overheid.aerius.opencl.Source;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.LandUseData.LandUseType;
import nl.overheid.aerius.srm.backgrounddata.MapData;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;
import nl.overheid.aerius.worker.WorkerExitCode;

/**
 * An SRM2 implementation that uses OpenCL.
 */
public abstract class SRM2Calculator {

  private static final String SOURCECODE_CL = "nl/overheid/aerius/srm2/SRM2Algorithm.cl";
  private static final String TEST_ENABLED = "TEST_ENABLED";
  private static final String VERSION_PREFIX = "VERSION_";
  private static final String DEPOSITION_CONVERSION_NH3 = "DEPOSITION_CONVERSION_NH3";
  private static final String DEPOSITION_CONVERSION_NO2 = "DEPOSITION_CONVERSION_NO2";
  private static final String WIND_SECTORS = "WIND_SECTORS";
  private static final String MAX_SEGMENT_LENGTH = "MAX_SEGMENT_LENGTH";
  private static final String MAX_SOURCE_DISTANCE = "MAX_SOURCE_DISTANCE";

  private static final Logger LOG = LoggerFactory.getLogger(SRM2Calculator.class);

  // Lock actual calculation over multiple instances of this class, to make sure only one actual calculation is done.
  private static final Object LOCK = new Object();

  private final RangeUtil rangeUtil = new RangeUtil(SRMConstants.MAX_SOURCE_DISTANCE);
  private final Context context;
  private final SourceData sourceData;
  private final long connectionTimeout;
  private final SRM2CalculationOptions options;
  private final AeriusSRMVersion aeriusSRMVersion;

  /**
   * Create a new instance with the specified OpenCL context.
   *
   * @param context The OpenCL context to use.
   * @param preSRMData  Preloaded pre-srm data
   * @param connectionTimeout timout for connecting to the OpenCL runtime
   * @param options specific profile srm2 options
   */
  public SRM2Calculator(final AeriusSRMVersion aeriusSRMVersion, final Context context, final PreSRMData preSRMData, final long connectionTimeout,
      final SRM2CalculationOptions options) {
    this.context = context;
    this.connectionTimeout = connectionTimeout;
    this.options = options;
    this.aeriusSRMVersion = aeriusSRMVersion;
    final Source programSource = createSource(aeriusSRMVersion, preSRMData, options);
    sourceData = new SourceData(programSource, preSRMData);
  }

  /**
   * Loads the OpenCL source file and prefixes it with defines for constants. All pre-srm data should be loaded before
   * calling this.
   *
   * @param preSRMData
   *          The pre-srm data.
   * @param options
   * @return The newly created OpenCL source.
   */
  static Source createSource(final AeriusSRMVersion aeriusSRMVersion, final PreSRMData preSRMData, final SRM2CalculationOptions options) {
    final ArrayList<Constant> constants = new ArrayList<>();

    constants.add(new DefineConstant(VERSION_PREFIX + aeriusSRMVersion.getSrmConstantsVersion(), 1));
    constants.add(new DefineConstant(MAX_SOURCE_DISTANCE, SRMConstants.MAX_SOURCE_DISTANCE));
    constants.add(new DefineConstant(MAX_SEGMENT_LENGTH, SRMConstants.MAX_SEGMENT_LENGTH));
    constants.add(new DefineConstant(WIND_SECTORS, SRMConstants.WIND_SECTORS));
    constants.add(new DefineConstant(DEPOSITION_CONVERSION_NO2, SRMConstants.DEPOSITION_CONVERSION_NO2));
    constants.add(new DefineConstant(DEPOSITION_CONVERSION_NH3, SRMConstants.DEPOSITION_CONVERSION_NH3));

    addLandUseConstants(aeriusSRMVersion, constants, preSRMData);

    if (options.isTest()) {
      constants.add(new DefineConstant(TEST_ENABLED, true));
    }

    return new FileSource(SOURCECODE_CL, constants.toArray(new Constant[constants.size()]));
  }

  private static void addLandUseConstants(final AeriusSRMVersion aeriusSRMVersion, final List<Constant> constants, final PreSRMData preSRMData) {
    setLandUse(constants, LandUseType.LOW, preSRMData.getLandUse(aeriusSRMVersion, LandUseType.LOW));
    setLandUse(constants, LandUseType.MED, preSRMData.getLandUse(aeriusSRMVersion, LandUseType.MED));
    setLandUse(constants, LandUseType.HIGH, preSRMData.getLandUse(aeriusSRMVersion, LandUseType.HIGH));
  }

  private static void setLandUse(final List<Constant> constants, final LandUseType landUseType, final MapData mapData) {
    final String lut = landUseType.name().toUpperCase(Locale.ENGLISH);
    final String landUse = "LANDUSE_" + lut;

    constants.add(new DefineConstant(landUse + "_WIDTH", mapData.getWidth()));
    constants.add(new DefineConstant(landUse + "_HEIGHT", mapData.getHeight()));
    constants.add(new DefineConstant(landUse + "_OFFSET_X", mapData.getOffsetX()));
    constants.add(new DefineConstant(landUse + "_OFFSET_Y", mapData.getOffsetY()));
    constants.add(new DefineConstant(landUse + "_DIAMETER", mapData.getDiameter()));
    constants.add(new DefineConstant(landUse + "_DEFAULT", mapData.getNoDataValue()));
  }

  /**
   * Calculates the concentration at the receptors caused by the road segments for the given year.
   *
   * @param year
   *          The year for which to calculate the results.
   * @param meteo
   *          The meteo years to use. May be null.
   * @param substances
   *          The substances to calculate for.
   * @param emissionResultKeys
   *          The EmissionResultKeys to get results for.
   * @param sources
   *          The road segments that are the sources.
   * @param receptors
   *          The receptors for which to calculate the results.
   * @param subReceptorAggregator
   *          The aggregator to use when results need to be aggregated (assumes subreceptors have been supplied externally).
   * @return Returns a map containing a list of results for each substance in the same order as the provided calculation
   *         receptors.
   * @throws AeriusException when the calculation fails either due to user errors or internal problems.
   */
  public ArrayList<AeriusResultPoint> calculate(final int year, final Meteo meteo, final List<Substance> substances,
      final Set<EmissionResultKey> emissionResultKeys, final Collection<SRM2RoadSegment> sources, final Collection<AeriusPoint> receptors,
      final SRMSubReceptorAggregator subReceptorAggregator) throws AeriusException {
    final ArrayList<AeriusResultPoint> results = new ArrayList<>(receptors.size());
    final ArrayList<AeriusResultPoint> emptyResults = new ArrayList<>(receptors.size());

    computeEmptyResults(emissionResultKeys, sources, receptors, results, emptyResults);

    if (sources.isEmpty()) {
      LOG.debug("Nothing to do, sources list is empty");
    } else if (results.isEmpty()) {
      LOG.debug("No receptors to calculate.");
    } else {
      safeActualCalculate(year, meteo, substances, emissionResultKeys, sources, results);
    }
    results.addAll(emptyResults);
    if (subReceptorAggregator != null) {
      final List<AeriusResultPoint> aggregatedResults = subReceptorAggregator.aggregate(results);
      // Replace the results with the aggregated ones.
      results.clear();
      results.addAll(aggregatedResults);
    }
    return results;
  }

  private void computeEmptyResults(final Set<EmissionResultKey> emissionResultKeys, final Collection<SRM2RoadSegment> sources,
      final Collection<AeriusPoint> receptors, final ArrayList<AeriusResultPoint> results, final ArrayList<AeriusResultPoint> emptyResults) {

    for (final AeriusPoint inputPoint : receptors) {
      final AeriusResultPoint receptor = newResultPoint(inputPoint);

      if (rangeUtil.inAnyRange(sources, receptor)) {
        results.add(receptor);
      } else {
        emissionResultKeys.forEach(key -> receptor.setEmissionResult(key, 0.0));
        emptyResults.add(receptor);
      }
    }
    if (LOG.isDebugEnabled() && !emptyResults.isEmpty()) {
      LOG.debug("Skipping {} receptors that have no source in range.", emptyResults.size());
    }
  }

  /**
   * Creates a new result point. Can be used to create an extending result point.
   *
   * @param inputPoint AeriusPoint
   * @return new result point
   */
  protected abstract AeriusResultPoint newResultPoint(AeriusPoint inputPoint);

  private void safeActualCalculate(final int year, final Meteo meteo, final Collection<Substance> substances,
      final Set<EmissionResultKey> emissionResultKeys, final Collection<SRM2RoadSegment> sources, final ArrayList<AeriusResultPoint> results)
          throws AeriusException  {
    try {
      actualCalculate(year, meteo, substances, emissionResultKeys, sources, results);
    } catch (final CLOutOfHostMemoryException e) {
      LOG.error("Received OpenCL exception indicating a serious issue. Shutting this worker instance down.", e);
      System.exit(WorkerExitCode.FAILURE_OOM.getExitCode());
    }
  }

  private void actualCalculate(final int year, final Meteo meteo, final Collection<Substance> substances,
      final Set<EmissionResultKey> emissionResultKeys, final Collection<SRM2RoadSegment> sources, final ArrayList<AeriusResultPoint> results)
          throws AeriusException {
    final List<CalculationContext> calculations = new ArrayList<>();
    final boolean cdep = options.isCalculateDeposition();

    synchronized (LOCK) {
      if (substances.contains(Substance.NOX) || substances.contains(Substance.NO2)) {
        calculations.add(
            new CalculationContext(sourceData, year, meteo, Substance.NO2, sources, results, new NOX2CalculationContext(emissionResultKeys), cdep,
                aeriusSRMVersion));
      }
      if (substances.contains(Substance.NH3)) {
        final EnumSet<EmissionResultKey> erks = emissionResultKeys.stream()
            .filter(erk -> erk == EmissionResultKey.NH3_CONCENTRATION || erk == EmissionResultKey.NH3_DEPOSITION)
            .collect(Collectors.toCollection(() -> EnumSet.noneOf(EmissionResultKey.class)));
        calculations.add(
            new CalculationContext(sourceData, year, meteo, Substance.NH3, sources, results, new NH3CalculationContext(erks), cdep,
                aeriusSRMVersion));
      }
      if (substances.contains(Substance.PM10)) {
        final Set<EmissionResultKey> erks = EnumSet.of(EmissionResultKey.PM10_CONCENTRATION);
        calculations.add(
            new CalculationContext(sourceData, year, meteo, Substance.PM10, sources, results, new NH3CalculationContext(erks), cdep,
                aeriusSRMVersion));
      }
      if (substances.contains(Substance.PM25)) {
        final Set<EmissionResultKey> erks = EnumSet.of(EmissionResultKey.PM25_CONCENTRATION);
        calculations.add(
            new CalculationContext(sourceData, year, meteo, Substance.PM25, sources, results, new NH3CalculationContext(erks), cdep,
                aeriusSRMVersion));
      }
      if (substances.contains(Substance.EC)) {
        final Set<EmissionResultKey> erks = EnumSet.of(EmissionResultKey.EC_CONCENTRATION);
        calculations.add(
            new CalculationContext(sourceData, year, meteo, Substance.EC, sources, results, new NH3CalculationContext(erks), cdep, aeriusSRMVersion));
      }

      for (final CalculationContext calculationContext : calculations) {
        calculationContext.runCalculation(context, connectionTimeout);
      }
    }
  }
}
