/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Data class to store CIMLK legacy calculation result data.
 */
public class CIMLKResult {

  private String calculationPointId;
  private int calculationYear;
  private String aeriusVersion;
  private String aeriusDatabaseVersion;
  private String label;
  private String monitorSubstance;
  private String geometry;
  private AeriusPoint point;
  private double totalConcentrationNO2;
  private double totalConcentrationPM10;
  private double totalConcentrationPM25;
  private double totalConcentrationEC;
  private double exceedanceHoursNO2;
  private double exceedanceDaysPM10;
  private double backgroundConcentrationNO2;
  private double backgroundConcentrationPM10;
  private double backgroundConcentrationPM25;
  private double backgroundConcentrationEC;
  private double srm2DirectConcentrationNO2;
  private double srm2ConcentrationPM10;
  private double srm2ConcentrationPM25;
  private double srm2ConcentrationEC;
  private double srm1DirectConcentrationNO2;
  private double srm1ConcentrationPM10;
  private double srm1ConcentrationPM25;
  private double srm1ConcentrationEC;
  private double backgroundConcentrationO3;
  private double srm2ConcentrationNOx;
  private double srm1ConcentrationNOx;
  private double srmConcentrationConvertedNO2;
  private double mainRoadsCorrectionNO2;
  private double mainRoadsCorrectionPM10;
  private double mainRoadsCorrectionPM25;
  private double mainRoadsCorrectionEC;
  private double userCorrectionNO2;
  private double userCorrectionPM10;
  private double userCorrectionPM25;
  private double userCorrectionEC;

  public String getCalculationPointId() {
    return calculationPointId;
  }

  public void setCalculationPointId(final String calculationPointId) {
    this.calculationPointId = calculationPointId;
  }

  public int getCalculationYear() {
    return calculationYear;
  }

  public void setCalculationYear(final int calculationYear) {
    this.calculationYear = calculationYear;
  }

  public String getAeriusVersion() {
    return aeriusVersion;
  }

  public void setAeriusVersion(final String aeriusVersion) {
    this.aeriusVersion = aeriusVersion;
  }

  public String getAeriusDatabaseVersion() {
    return aeriusDatabaseVersion;
  }

  public void setAeriusDatabaseVersion(final String aeriusDatabaseVersion) {
    this.aeriusDatabaseVersion = aeriusDatabaseVersion;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  public String getMonitorSubstance() {
    return monitorSubstance;
  }

  public void setMonitorSubstance(final String monitorSubstance) {
    this.monitorSubstance = monitorSubstance;
  }

  public String getGeometry() {
    return geometry;
  }

  public void setGeometry(final String geometry) {
    this.geometry = geometry;
  }

  public AeriusPoint getPoint() {
    return point;
  }

  public void setPoint(final AeriusPoint point) {
    this.point = point;
  }

  public double getTotalConcentrationNO2() {
    return totalConcentrationNO2;
  }

  public void setTotalConcentrationNO2(final double totalConcentrationNO2) {
    this.totalConcentrationNO2 = totalConcentrationNO2;
  }

  public double getTotalConcentrationPM10() {
    return totalConcentrationPM10;
  }

  public void setTotalConcentrationPM10(final double totalConcentrationPM10) {
    this.totalConcentrationPM10 = totalConcentrationPM10;
  }

  public double getTotalConcentrationPM25() {
    return totalConcentrationPM25;
  }

  public void setTotalConcentrationPM25(final double totalConcentrationPM25) {
    this.totalConcentrationPM25 = totalConcentrationPM25;
  }

  public double getTotalConcentrationEC() {
    return totalConcentrationEC;
  }

  public void setTotalConcentrationEC(final double totalConcentrationEC) {
    this.totalConcentrationEC = totalConcentrationEC;
  }

  public double getExceedanceHoursNO2() {
    return exceedanceHoursNO2;
  }

  public void setExceedanceHoursNO2(final double exceedanceHoursNO2) {
    this.exceedanceHoursNO2 = exceedanceHoursNO2;
  }

  public double getExceedanceDaysPM10() {
    return exceedanceDaysPM10;
  }

  public void setExceedanceDaysPM10(final double exceedanceDaysPM10) {
    this.exceedanceDaysPM10 = exceedanceDaysPM10;
  }

  public double getBackgroundConcentrationNO2() {
    return backgroundConcentrationNO2;
  }

  public void setBackgroundConcentrationNO2(final double backgroundConcentrationNO2) {
    this.backgroundConcentrationNO2 = backgroundConcentrationNO2;
  }

  public double getBackgroundConcentrationPM10() {
    return backgroundConcentrationPM10;
  }

  public void setBackgroundConcentrationPM10(final double backgroundConcentrationPM10) {
    this.backgroundConcentrationPM10 = backgroundConcentrationPM10;
  }

  public double getBackgroundConcentrationPM25() {
    return backgroundConcentrationPM25;
  }

  public void setBackgroundConcentrationPM25(final double backgroundConcentrationPM25) {
    this.backgroundConcentrationPM25 = backgroundConcentrationPM25;
  }

  public double getBackgroundConcentrationEC() {
    return backgroundConcentrationEC;
  }

  public void setBackgroundConcentrationEC(final double backgroundConcentrationEC) {
    this.backgroundConcentrationEC = backgroundConcentrationEC;
  }

  public double getSrm2DirectConcentrationNO2() {
    return srm2DirectConcentrationNO2;
  }

  public void setSrm2DirectConcentrationNO2(final double srm2DirectConcentrationNO2) {
    this.srm2DirectConcentrationNO2 = srm2DirectConcentrationNO2;
  }

  public double getSrm2ConcentrationPM10() {
    return srm2ConcentrationPM10;
  }

  public void setSrm2ConcentrationPM10(final double srm2ConcentrationPM10) {
    this.srm2ConcentrationPM10 = srm2ConcentrationPM10;
  }

  public double getSrm2ConcentrationPM25() {
    return srm2ConcentrationPM25;
  }

  public void setSrm2ConcentrationPM25(final double srm2ConcentrationPM25) {
    this.srm2ConcentrationPM25 = srm2ConcentrationPM25;
  }

  public double getSrm2ConcentrationEC() {
    return srm2ConcentrationEC;
  }

  public void setSrm2ConcentrationEC(final double srm2ConcentrationEC) {
    this.srm2ConcentrationEC = srm2ConcentrationEC;
  }

  public double getSrm1DirectConcentrationNO2() {
    return srm1DirectConcentrationNO2;
  }

  public void setSrm1DirectConcentrationNO2(final double srm1DirectConcentrationNO2) {
    this.srm1DirectConcentrationNO2 = srm1DirectConcentrationNO2;
  }

  public double getSrm1ConcentrationPM10() {
    return srm1ConcentrationPM10;
  }

  public void setSrm1ConcentrationPM10(final double srm1ConcentrationPM10) {
    this.srm1ConcentrationPM10 = srm1ConcentrationPM10;
  }

  public double getSrm1ConcentrationPM25() {
    return srm1ConcentrationPM25;
  }

  public void setSrm1ConcentrationPM25(final double srm1ConcentrationPM25) {
    this.srm1ConcentrationPM25 = srm1ConcentrationPM25;
  }

  public double getSrm1ConcentrationEC() {
    return srm1ConcentrationEC;
  }

  public void setSrm1ConcentrationEC(final double srm1ConcentrationEC) {
    this.srm1ConcentrationEC = srm1ConcentrationEC;
  }

  public double getBackgroundConcentrationO3() {
    return backgroundConcentrationO3;
  }

  public void setBackgroundConcentrationO3(final double backgroundConcentrationO3) {
    this.backgroundConcentrationO3 = backgroundConcentrationO3;
  }

  public double getSrm2ConcentrationNOx() {
    return srm2ConcentrationNOx;
  }

  public void setSrm2ConcentrationNOx(final double srm2ConcentrationNOx) {
    this.srm2ConcentrationNOx = srm2ConcentrationNOx;
  }

  public double getSrm1ConcentrationNOx() {
    return srm1ConcentrationNOx;
  }

  public void setSrm1ConcentrationNOx(final double srm1ConcentrationNOx) {
    this.srm1ConcentrationNOx = srm1ConcentrationNOx;
  }

  public double getSrmConcentrationConvertedNO2() {
    return srmConcentrationConvertedNO2;
  }

  public void setSrmConcentrationConvertedNO2(final double srmConcentrationConvertedNO2) {
    this.srmConcentrationConvertedNO2 = srmConcentrationConvertedNO2;
  }

  public double getMainRoadsCorrectionNO2() {
    return mainRoadsCorrectionNO2;
  }

  public void setMainRoadsCorrectionNO2(final double mainRoadsCorrectionNO2) {
    this.mainRoadsCorrectionNO2 = mainRoadsCorrectionNO2;
  }

  public double getMainRoadsCorrectionPM10() {
    return mainRoadsCorrectionPM10;
  }

  public void setMainRoadsCorrectionPM10(final double mainRoadsCorrectionPM10) {
    this.mainRoadsCorrectionPM10 = mainRoadsCorrectionPM10;
  }

  public double getMainRoadsCorrectionPM25() {
    return mainRoadsCorrectionPM25;
  }

  public void setMainRoadsCorrectionPM25(final double mainRoadsCorrectionPM25) {
    this.mainRoadsCorrectionPM25 = mainRoadsCorrectionPM25;
  }

  public double getMainRoadsCorrectionEC() {
    return mainRoadsCorrectionEC;
  }

  public void setMainRoadsCorrectionEC(final double mainRoadsCorrectionEC) {
    this.mainRoadsCorrectionEC = mainRoadsCorrectionEC;
  }

  public double getUserCorrectionNO2() {
    return userCorrectionNO2;
  }

  public void setUserCorrectionNO2(final double userCorrectionNO2) {
    this.userCorrectionNO2 = userCorrectionNO2;
  }

  public double getUserCorrectionPM10() {
    return userCorrectionPM10;
  }

  public void setUserCorrectionPM10(final double userCorrectionPM10) {
    this.userCorrectionPM10 = userCorrectionPM10;
  }

  public double getUserCorrectionPM25() {
    return userCorrectionPM25;
  }

  public void setUserCorrectionPM25(final double userCorrectionPM25) {
    this.userCorrectionPM25 = userCorrectionPM25;
  }

  public double getUserCorrectionEC() {
    return userCorrectionEC;
  }

  public void setUserCorrectionEC(final double userCorrectionEC) {
    this.userCorrectionEC = userCorrectionEC;
  }

}
