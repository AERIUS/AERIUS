/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;

/**
 * Flattens a list of SRM1 Sources ({@link SRM1RoadEmissionSource} with srm1Road = true) and writes it in a csv file.
 */
public final class CIMLKSRM1RoadWriter extends AbstractCIMLKWriter<CIMLKSRM1RoadWriter.Data> {

  protected static class Data {

    final int scale;
    final SRM1RoadEmissionSource source;
    final String wkt;
    final StandardVehicles vehicles;

    Data(final int scale, final String wkt, final SRM1RoadEmissionSource source, final StandardVehicles vehicles) {
      this.scale = scale;
      this.wkt = wkt;
      this.source = source;
      this.vehicles = vehicles;
    }

  }

  private static class Column extends GenericColumn<Data> {

    public Column(final String header, final Function<Data, String> function) {
      super(header, function);
    }

  }

  private static class SourceColumn extends Column {

    public SourceColumn(final String header, final Function<SRM1RoadEmissionSource, String> function) {
      super(header, data -> function.apply(data.source));
    }

  }

  private static class IntensityTrafficColumn extends Column {

    public IntensityTrafficColumn(final String header, final VehicleType vehicleType) {
      super(header, data -> data.vehicles.getValuesPerVehicleTypes().containsKey(vehicleType.getStandardVehicleCode())
          ? roundedValue(data.scale,
              intensityPerDay(data.vehicles.getTimeUnit(), data.vehicles.getValuesPerVehicleTypes().get(vehicleType.getStandardVehicleCode())))
          : "");
    }

    private static double intensityPerDay(final TimeUnit timeUnit, final ValuesPerVehicleType valuesPerVehicleType) {
      return timeUnit.toUnit(valuesPerVehicleType.getVehiclesPerTimeUnit(), TimeUnit.DAY);
    }

  }

  private static class StagnationFactorTrafficColumn extends Column {

    public StagnationFactorTrafficColumn(final String header, final VehicleType vehicleType) {
      super(header, data -> data.vehicles.getValuesPerVehicleTypes().containsKey(vehicleType.getStandardVehicleCode())
          ? roundedValue(data.scale, data.vehicles.getValuesPerVehicleTypes().get(vehicleType.getStandardVehicleCode()).getStagnationFraction())
          : "");
    }

  }

  private static final Column[] COLUMNS = new Column[] {
      new SourceColumn("srm1_road_id", source -> source.getGmlId()),
      new SourceColumn("jurisdiction_id", source -> source.getJurisdictionId() == null ? "" : String.valueOf(source.getJurisdictionId())),
      new SourceColumn("label", source -> source.getLabel()),
      new SourceColumn("road_manager", source -> source.getRoadManager() == null ? "" : source.getRoadManager().name()),
      new SourceColumn("speed_profile", source -> speedProfile(source.getRoadTypeCode())),
      new Column("tunnel_factor", data -> roundedValue(data.scale, data.source.getTunnelFactor())),
      new IntensityTrafficColumn("light_traffic_intensity", VehicleType.LIGHT_TRAFFIC),
      new StagnationFactorTrafficColumn("light_traffic_stagnation_factor", VehicleType.LIGHT_TRAFFIC),
      new IntensityTrafficColumn("normal_freight_intensity", VehicleType.NORMAL_FREIGHT),
      new StagnationFactorTrafficColumn("normal_freight_stagnation_factor", VehicleType.NORMAL_FREIGHT),
      new IntensityTrafficColumn("heavy_freight_intensity", VehicleType.HEAVY_FREIGHT),
      new StagnationFactorTrafficColumn("heavy_freight_stagnation_factor", VehicleType.HEAVY_FREIGHT),
      new IntensityTrafficColumn("auto_bus_intensity", VehicleType.AUTO_BUS),
      new StagnationFactorTrafficColumn("auto_bus_stagnation_factor", VehicleType.AUTO_BUS),
      new SourceColumn("description", source -> source.getDescription()),
      new Column("geometry", data -> data.wkt),
  };

  private static final List<GenericColumn<Data>> DEFAULT_COLUMNS = Collections.unmodifiableList(Arrays.asList(COLUMNS));

  private final int scale;

  public CIMLKSRM1RoadWriter(final int scale) {
    this.scale = scale;
  }

  @Override
  List<GenericColumn<Data>> columns() {
    return DEFAULT_COLUMNS;
  }

  /**
   * write row of data to the writer (does not close the writer);
   */
  public void writeRow(final Writer writer, final EmissionSourceFeature source) throws IOException {
    if (source.getProperties() instanceof SRM1RoadEmissionSource) {
      final String wkt = toWkt(source.getGeometry());
      writeRows(writer, wkt, (SRM1RoadEmissionSource) source.getProperties());
    }
  }

  public void write(final File outputFile, final List<EmissionSourceFeature> sources) throws IOException {
    try (final Writer writer = Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8)) {
      writeHeader(writer);
      for (final EmissionSourceFeature source : sources) {
        writeRow(writer, source);
      }
    }
  }

  private void writeRows(final Writer writer, final String geometry, final SRM1RoadEmissionSource source) throws IOException {
    final List<Data> datas = new ArrayList<>();
    final List<StandardVehicles> standardVehicles = source.getSubSources().stream()
        .filter(StandardVehicles.class::isInstance)
        .map(StandardVehicles.class::cast)
        .collect(Collectors.toList());
    for (final StandardVehicles vehicle : standardVehicles) {
      datas.add(new Data(scale, geometry, source, vehicle));
    }

    for (final Data data : datas) {
      super.writeRow(writer, data);
    }
  }

  private static String speedProfile(final String roadAreaCode) {
    for (final RoadSpeedType speedType : RoadSpeedType.values()) {
      if (speedType.getRoadTypeCode().equalsIgnoreCase(roadAreaCode)) {
        return speedType.name();
      }
    }
    return roadAreaCode;
  }

}
