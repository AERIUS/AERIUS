/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata.bgconcentrations;

import java.util.List;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.PreSrmXYReader;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;

class SchipholBackgroundConcentrationReader extends PreSrmXYReader<WindRoseConcentration> {
  // Separate on white space characters
  private static final String CO3 = "CO3";
  // Offset where windrose O3 data starts. Skip x-y coordinate data.
  private static final int CO3_OFFSET = 2;
  private static final int GCN_NO2_COLUMN_INDEX = CO3_OFFSET + SRMConstants.WIND_SECTORS;
  private static final int C_HWN_NO2_COLUMN_INDEX = GCN_NO2_COLUMN_INDEX + 1;
  private static final int GCN_O3_COLUMN_INDEX = GCN_NO2_COLUMN_INDEX + 2;
  private static final int C_HWN_O3_COLUMN_INDEX = GCN_NO2_COLUMN_INDEX + 3;
  private static final String GCN_NO2_COLUMN = "GCN_NO2";
  private static final String GCN_O3_COLUMN = "GCN_O3";
  private static final String C_HWN_NO2_COLUMN = "C_HWN_NO2";
  private static final String C_HWN_O3_COLUMN = "C_HWN_O3";

  public SchipholBackgroundConcentrationReader(final String preSrmVersion, final YearPattern yearPattern, final int year) {
    super(preSrmVersion, yearPattern, year);
  }

  @Override
  protected WindRoseConcentration parseLine(final String line, final List<AeriusException> warnings) {
    final WindRoseConcentration windrose = new WindRoseConcentration(readX(), readY());

    for (int i = 0; i < SRMConstants.WIND_SECTORS; i++) {
      windrose.setO3(i, getDouble(CO3 + i, CO3_OFFSET + i));
    }
    windrose.setGCN(Substance.NO2, getDouble(GCN_NO2_COLUMN, GCN_NO2_COLUMN_INDEX));
    windrose.setCHwn(Substance.NO2, getNegatedDouble(C_HWN_NO2_COLUMN, C_HWN_NO2_COLUMN_INDEX));
    windrose.setGCN(Substance.O3, getDouble(GCN_O3_COLUMN, GCN_O3_COLUMN_INDEX));
    windrose.setCHwn(Substance.O3, getNegatedDouble(C_HWN_O3_COLUMN, C_HWN_O3_COLUMN_INDEX));

    return windrose;
  }
}
