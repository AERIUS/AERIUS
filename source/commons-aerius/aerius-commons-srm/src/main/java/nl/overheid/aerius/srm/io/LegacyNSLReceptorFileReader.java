/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLine;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRoadProfile;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKTreeProfile;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.io.LegacyNSLReceptorFileReader.LegacyReceptorResult;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Reader for NSL legacy receptor files.
 *
 * Available columns:
 * <pre>
 * segment_id;receptorid;overheidid;overheid;nummer;naam;x;y;type;aant_pers;nsl;grond;afstand;wegtype;boom_fact;opmerking;opm_ovdr
 * ;gewijzigd;geomet_wkt;actie
 * </pre>
 */
class LegacyNSLReceptorFileReader extends AbstractCIMLKFileReader<LegacyReceptorResult> {

  static class LegacyReceptorResult {
    private final CalculationPointFeature point;
    private final List<CIMLKDispersionLineFeature> dispersionLines = new ArrayList<>();

    LegacyReceptorResult(final CalculationPointFeature point) {
      this.point = point;
    }

    public CalculationPointFeature getPoint() {
      return point;
    }

    public List<CIMLKDispersionLineFeature> getDispersionLines() {
      return dispersionLines;
    }

  }

  // @formatter:off
  private enum ReceptorColumns implements Columns {
    SEGMENT_ID,
    RECEPTORID,
    OVERHEIDID,
    OVERHEID,
    NUMMER,
    NAAM,
    X,
    Y,
    TYPE,
    AANT_PERS,
    NSL,
    GROND,
    AFSTAND,
    WEGTYPE,
    BOOM_FACT,
    OPMERKING,
    OPM_OVDR,
    GEWIJZIGD,
    GEOMET_WKT,
    ACTIE;
    // @formatter:on

    static ReceptorColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  private enum ReceptorTestColumns implements Columns {
    TEST_HEIGHT;

    static ReceptorTestColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  private final Map<String, LegacyReceptorResult> receptorIdCache = new HashMap<>();
  private final boolean readHeights;
  private int calculationPointIdx = 0;

  public LegacyNSLReceptorFileReader(final boolean readHeights) {
    this.readHeights = readHeights;
  }

  @Override
  protected LegacyReceptorResult parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final String receptorId = getGmlId(ReceptorColumns.RECEPTORID, GMLIdUtil.POINT_PREFIX, warnings);
    final LegacyReceptorResult result;
    boolean cachedResult = false;

    if (receptorIdCache.containsKey(receptorId)) {
      // receptor already found, so ignore this time.
      // TODO validate if receptor the same ???
      result = receptorIdCache.get(receptorId);
      cachedResult = true;
    } else {
      result = new LegacyReceptorResult(createPoint(receptorId));
      receptorIdCache.put(receptorId, result);
    }
    if (!getString(ReceptorColumns.SEGMENT_ID).isEmpty()) {
      final String segmentId = GMLIdUtil.toValidGmlId(getString(ReceptorColumns.SEGMENT_ID), GMLIdUtil.SOURCE_PREFIX);
      final int roadType = getInt(ReceptorColumns.WEGTYPE);
      CIMLKValidator.validateDispersionLineRoadType(getCurrentLineNumber(), roadType);
      final double treeFactor = getDouble(ReceptorColumns.BOOM_FACT);
      CIMLKValidator.validateTreeFactor(getCurrentLineNumber(), treeFactor);

      final CIMLKDispersionLine dispersionLine = new CIMLKDispersionLine();
      dispersionLine.setCalculationPointGmlId(receptorId);
      dispersionLine.setRoadGmlId(segmentId);
      dispersionLine.setTreeProfile(CIMLKTreeProfile.legacySafeValueOf(getDouble(ReceptorColumns.BOOM_FACT)));
      dispersionLine.setRoadProfile(CIMLKRoadProfile.legacySafeValueOf(getInt(ReceptorColumns.WEGTYPE)));
      final CIMLKDispersionLineFeature feature = new CIMLKDispersionLineFeature();
      feature.setProperties(dispersionLine);
      result.getDispersionLines().add(feature);
    }
    if (readHeights && hasColumn(ReceptorTestColumns.TEST_HEIGHT)) {
      ((CIMLKCalculationPoint) result.getPoint().getProperties()).setHeight(getDouble(ReceptorTestColumns.TEST_HEIGHT));
    }

    return cachedResult ? null : result;
  }

  private CalculationPointFeature createPoint(final String receptorId) throws AeriusException {
    final String wktString = getString(ReceptorColumns.GEOMET_WKT);
    final Geometry point = getGeometry(ReceptorColumns.GEOMET_WKT, geometry -> geometry.type() != GeometryType.POINT);

    if (point instanceof Point) {
      final CalculationPointFeature feature = new CalculationPointFeature();
      feature.setGeometry((Point) point);
      feature.setId(receptorId);
      final CIMLKCalculationPoint calculationPoint = new CIMLKCalculationPoint();
      calculationPoint.setGmlId(receptorId);
      calculationPoint.setCustomPointId(++calculationPointIdx);
      feature.setProperties(calculationPoint);
      return feature;
    } else {
      throw new AeriusException(AeriusExceptionReason.SRM2_INCORRECT_WKT_VALUE, String.valueOf(getCurrentLineNumber()), wktString);
    }
  }

  @Override
  Columns[] expectedColumns() {
    return ReceptorColumns.values();
  }

  @Override
  Columns safeColumnOf(final String value) {
    final Columns column = ReceptorColumns.safeValueOf(value);
    return column == null ? ReceptorTestColumns.safeValueOf(value) : column;
  }

}
