/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.Vehicles;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Reader to read Nationaal Samenwerkingsprogramma Luchtkwaliteit (NSL) legacy csv files.
 */
class LegacyNSLSegmentFileReader extends AbstractCIMLKFileReader<EmissionSourceFeature> {

  private static final Logger LOGGER = LoggerFactory.getLogger(LegacyNSLSegmentFileReader.class);

  //segment_id:nwb_weg_id:nwb_versie:begin_pos:eind_pos:overheidid:overheid      :
  //9187      :261318052 :801       :0        :0.498778:1904      :Stichtse Vecht:

  //straatnaam:straatnr:wegbeheer:hoogte:x        :y        :wegtype:snelheid:
  //Floraweg  :        :G        :0     :130879.00:459350.00:92     :e       :

  //tun_factor:boom_fact:maxsnelh_p:maxs_p_dyn:maxsnelh_v:a_rand_l:a_gevel_l:bebdicht_l:
  //1         :1        :50        :          :50        :9.40    :         :          :

  //a_toepas_l:a_scherm_l:s_hoogte_l:a_rand_r:a_gevel_r:bebdicht_r:a_toepas_r:a_scherm_r:
  //          :          :          :9.50    :42.80    :0.02      :          :          :

  //s_hoogte_r:stagf_lv:int_lv:int_lv_dyn:stagf_mv:int_mv:stagf_zv:int_zv:stagf_bv:int_bv:
  //          :0.000000:8077  :          :0.000000:217   :0.000000:170   :0.000000:46    :

  //opmerking;gewijzigd           ;geomet_wkt                             ;actie
  //          ;28-05-2013 10:22:07;LINESTRING(130835 459390,130923 459309);u

  /**
   * 92 = road on the road network with a broad profile
   * weg van het onderliggende wegennet met een breed profiel
   */
  private static final int ROAD_WIDE_PROFILE = 92;
  /**
   * 93 = freeway on the road network with a broad profile
   * (snel)weg van het hoofdwegennet met een breed profiel
   */
  private static final int FREEWAY_WIDE_PROFILE = 93;
  /**
   * 94 = freeway on the road network with a broad profile and, strict enforcement of the speed
   * (snel)weg van het hoofdwegennet met een breed profiel en toepassing van strikte handhaving op de snelheid
   * */
  private static final int FREEWAY_WIDE_PROFILE_SE = 94;

  // SRM1 road types are numbers below 5 -> 0 to 4.
  private static final int SRM1_ROAD_UPPER_BOUND = 5;

  // @formatter:off
  private enum SegmentColumns implements Columns {
    SEGMENT_ID,
    NWB_WEG_ID,
    NWB_VERSIE,
    BEGIN_POS,
    EIND_POS,
    OVERHEIDID,
    OVERHEID,
    STRAATNAAM,
    STRAATNR,
    WEGBEHEER,
    HOOGTE,
    X,
    Y,
    WEGTYPE,
    SNELHEID,
    TUN_FACTOR,
    BOOM_FACT,
    /**
     * Maximum speed cars.
     */
    MAXSNELH_P,
    /**
     * Maximum speed cars dynamic. set speed different at certain times during the day.
     */
    MAXS_P_DYN,
    /**
     * Maximum speed trucks
     */
    MAXSNELH_V,
    A_RAND_L,
    A_GEVEL_L,
    BEBDICHT_L,
    A_TOEPAS_L,
    A_SCHERM_L,
    S_HOOGTE_L,
    A_RAND_R,
    A_GEVEL_R,
    BEBDICHT_R,
    A_TOEPAS_R,
    A_SCHERM_R,
    S_HOOGTE_R,
    STAGF_LV,
    INT_LV,
    INT_LV_DYN,
    STAGF_MV,
    INT_MV,
    STAGF_ZV,
    INT_ZV,
    STAGF_BV,
    INT_BV,
    OPMERKING,
    GEWIJZIGD,
    GEOMET_WKT,
    ACTIE;
    // @formatter:on

    static SegmentColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  private static final SegmentColumns[] EXPECTED_COLUMNS = new SegmentColumns[] {
      SegmentColumns.WEGTYPE, SegmentColumns.SNELHEID, SegmentColumns.NWB_WEG_ID, SegmentColumns.SEGMENT_ID, SegmentColumns.MAXSNELH_P,
      SegmentColumns.MAXS_P_DYN, SegmentColumns.INT_LV,
      SegmentColumns.INT_LV_DYN, SegmentColumns.STAGF_LV, SegmentColumns.INT_MV, SegmentColumns.STAGF_MV,
      SegmentColumns.INT_ZV, SegmentColumns.STAGF_ZV, SegmentColumns.INT_BV, SegmentColumns.STAGF_BV,
      SegmentColumns.HOOGTE, SegmentColumns.TUN_FACTOR, SegmentColumns.BOOM_FACT,
      SegmentColumns.S_HOOGTE_L, SegmentColumns.A_SCHERM_L, SegmentColumns.S_HOOGTE_R, SegmentColumns.A_SCHERM_R, SegmentColumns.GEOMET_WKT,
  };

  private final SectorCategories categories;

  /**
   * @param categories The context to be used for determining road emission categories.
   */
  public LegacyNSLSegmentFileReader(final SectorCategories categories) {
    super();
    this.categories = categories;
  }

  @Override
  protected EmissionSourceFeature parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final int wegType = (int) getDouble(SegmentColumns.WEGTYPE); //wegtype is int, but user could store as double.
    final RoadSpeedType speedType = snelheidToSpeedType(wegType, getString(SegmentColumns.SNELHEID));
    final RoadType roadType = wegBeheerToRoadType(wegType, speedType);
    final RoadEmissionSource source = wegType < SRM1_ROAD_UPPER_BOUND ? readAsSRM1(speedType) : readAsSRM2(roadType);

    source.setGmlId(getGmlId(SegmentColumns.SEGMENT_ID, GMLIdUtil.SOURCE_PREFIX, warnings));
    source.setRoadAreaCode(DEFAULT_AREA_CODE);

    final Geometry geometry = getGeometry();
    feature.setGeometry(geometry);

    source.setLabel(getString(SegmentColumns.NWB_WEG_ID) + "-" + getString(SegmentColumns.SEGMENT_ID));

    source.setSectorId(ROAD_SECTOR_ID);

    source.setTunnelFactor(getDouble(SegmentColumns.TUN_FACTOR));
    if (source.getTunnelFactor() < 0) {
      throw new AeriusException(ImaerExceptionReason.SRM2_SOURCE_TUNNEL_FACTOR_ZERO, String.valueOf(source.getTunnelFactor()), source.getLabel());
    }

    //Store the sector id as ParticleSizeDistribution, needed for OPS.
    final Sector sector = categories.getSectorById(ROAD_SECTOR_ID);
    final OPSSourceCharacteristics characteristics = sector.getDefaultCharacteristics().copyTo(new OPSSourceCharacteristics());
    characteristics.setParticleSizeDistribution(ROAD_SECTOR_ID);
    source.setCharacteristics(characteristics);

    final boolean strictEnforcement = wegType == FREEWAY_WIDE_PROFILE_SE;

    final List<Vehicles> allTraffic = source.getSubSources();
    addNormalTraffic(allTraffic, roadType, strictEnforcement);
    addDynamicTraffic(allTraffic, roadType, strictEnforcement);

    feature.setProperties(source);
    return feature;
  }

  private static SRM1RoadEmissionSource readAsSRM1(final RoadSpeedType roadSpeedType) {
    final SRM1RoadEmissionSource srm1 = new SRM1RoadEmissionSource();

    srm1.setRoadTypeCode(roadSpeedType == null ? null : roadSpeedType.getRoadTypeCode());

    return srm1;
  }

  private SRM2RoadEmissionSource readAsSRM2(final RoadType roadType) throws AeriusException {
    final SRM2RoadEmissionSource srm2 = new SRM2RoadEmissionSource();
    final int height = (int) Math.round(getDouble(SegmentColumns.HOOGTE));
    srm2.setElevationHeight(height);
    srm2.setElevation(getRoadElevation(height));
    srm2.setRoadTypeCode(roadType.getRoadTypeCode());

    final SRM2RoadSideBarrier barrierLeft = getRoadSideBarrier(SegmentColumns.S_HOOGTE_L, SegmentColumns.A_SCHERM_L);
    if (Double.doubleToLongBits(barrierLeft.getHeight()) != 0) {
      srm2.setBarrierLeft(barrierLeft);
    }

    final SRM2RoadSideBarrier barrierRight = getRoadSideBarrier(SegmentColumns.S_HOOGTE_R, SegmentColumns.A_SCHERM_R);
    if (Double.doubleToLongBits(barrierRight.getHeight()) != 0) {
      srm2.setBarrierRight(barrierRight);
    }
    return srm2;
  }

  private Geometry getGeometry() throws AeriusException {
    return getGeometry(SegmentColumns.GEOMET_WKT, geom -> geom.type() != GeometryType.LINESTRING);
  }

  /**
   * Returns the {@link RoadElevation} enum given a height.
   * @param height actual height
   * @return road elevation enum
   */
  private RoadElevation getRoadElevation(final int height) {
    final RoadElevation re;
    if (height > 0) {
      //In line with NSL Monitoringstool roads with a height are steep dykes.
      re = RoadElevation.STEEP_DYKE;
    } else if (height < 0) {
      re = RoadElevation.TUNNEL;
    } else {
      re = RoadElevation.NORMAL;
    }
    return re;
  }

  private SRM2RoadSideBarrier getRoadSideBarrier(final Columns heightColumn, final Columns distanceColumn) throws AeriusException {
    final SRM2RoadSideBarrier barrier = new SRM2RoadSideBarrier();
    //no way to tell if it's a screen or a wall. Assume the worst.
    barrier.setBarrierType(SRM2RoadSideBarrierType.SCREEN);
    barrier.setHeight(getDouble(heightColumn));
    barrier.setDistance(getDouble(distanceColumn));
    return barrier;
  }

  private void addNormalTraffic(final List<Vehicles> allTraffic, final RoadType roadType, final boolean strictEnforcement) throws AeriusException {
    final int maxSpeed = (int) getDouble(SegmentColumns.MAXSNELH_P);
    final StandardVehicles traffic = new StandardVehicles();
    traffic.setTimeUnit(TimeUnit.DAY);
    traffic.setMaximumSpeed(maxSpeed);
    traffic.setStrictEnforcement(strictEnforcement);

    addTraffic(traffic, maxSpeed, roadType, VehicleType.LIGHT_TRAFFIC, SegmentColumns.INT_LV, SegmentColumns.STAGF_LV, strictEnforcement);
    addTraffic(traffic, maxSpeed, roadType, VehicleType.NORMAL_FREIGHT, SegmentColumns.INT_MV, SegmentColumns.STAGF_MV, strictEnforcement);
    addTraffic(traffic, maxSpeed, roadType, VehicleType.HEAVY_FREIGHT, SegmentColumns.INT_ZV, SegmentColumns.STAGF_ZV, strictEnforcement);
    addTraffic(traffic, maxSpeed, roadType, VehicleType.AUTO_BUS, SegmentColumns.INT_BV, SegmentColumns.STAGF_BV, strictEnforcement);

    if (hasAnyTraffic(traffic)) {
      allTraffic.add(traffic);
    }
  }

  private void addDynamicTraffic(final List<Vehicles> allTraffic, final RoadType roadType, final boolean strictEnforcement) throws AeriusException {
    final int maxSpeed = (int) getDouble(SegmentColumns.MAXS_P_DYN);
    final StandardVehicles traffic = new StandardVehicles();
    traffic.setTimeUnit(TimeUnit.DAY);
    traffic.setMaximumSpeed(maxSpeed);
    traffic.setStrictEnforcement(strictEnforcement);

    //dynamic does not have a stagnation factor.
    addTraffic(traffic, maxSpeed, roadType, VehicleType.LIGHT_TRAFFIC, SegmentColumns.INT_LV_DYN, null, strictEnforcement);

    if (hasAnyTraffic(traffic)) {
      allTraffic.add(traffic);
    }
  }

  private boolean hasAnyTraffic(final StandardVehicles traffic) {
    return traffic.getValuesPerVehicleTypes().values().stream().anyMatch(value -> Double.compare(value.getVehiclesPerTimeUnit(), 0) != 0);
  }

  /**
   * Set the source data for specific traffic types.
   * @param traffic Object to add values to
   * @param maxSpeed Maximum speed on the segment
   * @param roadType type of the road
   * @param vehicleType vehicle type
   * @param intensityColumn number of vehicles
   * @param stagnationColumn stagnation factor
   * @param strictEnforcement strict enforcement
   * @throws AeriusException exception when data error
   */
  private void addTraffic(final StandardVehicles traffic, final int maxSpeed, final RoadType roadType, final VehicleType vehicleType,
      final SegmentColumns intensityColumn, final SegmentColumns stagnationColumn, final boolean strictEnforcement)
      throws AeriusException {

    final RoadEmissionCategory roadEmissionCategory = categories.getRoadEmissionCategories().findClosestCategory(DEFAULT_AREA_CODE,
        roadType.getRoadTypeCode(), vehicleType.getStandardVehicleCode(),
        strictEnforcement, maxSpeed, null);

    if (roadEmissionCategory == null) {
      throw new AeriusException(AeriusExceptionReason.SRM2IO_EXCEPTION_NO_ROAD_PROPERTIES, String.valueOf(getCurrentLineNumber()),
          getCurrentColumnContent(),
          roadType + "," + vehicleType + "," + maxSpeed + (strictEnforcement ? ",SE" : ""));
    }

    final ValuesPerVehicleType valuesPerVehicleType = new ValuesPerVehicleType();
    valuesPerVehicleType.setVehiclesPerTimeUnit(getDouble(intensityColumn));
    if (stagnationColumn != null) {
      valuesPerVehicleType.setStagnationFraction(getDouble(stagnationColumn));
    }
    traffic.getValuesPerVehicleTypes().put(vehicleType.getStandardVehicleCode(), valuesPerVehicleType);
  }

  private RoadType wegBeheerToRoadType(final int wegtype, final RoadSpeedType speedType) throws AeriusException {
    final RoadType roadType;

    switch (wegtype) {
    case ROAD_WIDE_PROFILE:
      roadType = RoadType.NON_URBAN_ROAD;
      break;
    case FREEWAY_WIDE_PROFILE:
    case FREEWAY_WIDE_PROFILE_SE:
      roadType = RoadType.FREEWAY;
      break;
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
      // For SRM1-road valid values are: 0, 1, 2, 3, 4
      // If speed type == B this should be handled as a non urban road.
      roadType = speedType == RoadSpeedType.NON_URBAN_TRAFFIC ? RoadType.NON_URBAN_ROAD : RoadType.URBAN_ROAD;
      break;
    default:
      throw new AeriusException(ImaerExceptionReason.SRM_LEGACY_INVALID_ROAD_TYPE, String.valueOf(getCurrentLineNumber()), String.valueOf(wegtype));
    }
    return roadType;
  }

  private RoadSpeedType snelheidToSpeedType(final int wegType, final String snelheid) throws AeriusException {
    if (wegType >= SRM1_ROAD_UPPER_BOUND) {
      return null; // snelheid is not a property of srm2 roads and therefore should not be read or checked.
    }
    final RoadSpeedType speedType = RoadSpeedType.safeLegacyValueOf(snelheid);

    if (speedType == null) {
      throw new AeriusException(ImaerExceptionReason.SRM_LEGACY_INVALID_SPEED_TYPE, String.valueOf(getCurrentLineNumber()), snelheid);
    } else {
      return speedType;
    }
  }

  @Override
  Columns[] expectedColumns() {
    return EXPECTED_COLUMNS;
  }

  @Override
  Columns safeColumnOf(final String value) {
    return SegmentColumns.safeValueOf(value);
  }

}
