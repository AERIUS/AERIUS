/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata.bgconcentrations;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.PointGrid;
import nl.overheid.aerius.srm.backgrounddata.PreSrmXYReader;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentrationBuilder.WindRoseFileName;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentrationGrid;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentrationReader;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;

/**
 *
 */
public final class BackgroundConcentrationReader {

  private static final Logger LOGGER = LoggerFactory.getLogger(BackgroundConcentrationReader.class);
  private static final String IJMOND = "ijmond";
  private static final String SCHIPHOL = "schiphol";

  private BackgroundConcentrationReader() {
    // Util class
  }

  public static HasGridValue<WindRoseConcentration> read(final AeriusSRMVersion aeriusSRMVersion, final SRM2CalculationOptions options,
      final List<WindRoseFileName> files) throws IOException {
    final Theme theme = options.getTheme();

    if (theme == Theme.OWN2000) {
      return readOwN2000(aeriusSRMVersion.getPreSrmVersionNumber(), files, options.isAllBackground());
    } else if (theme == Theme.RBL) {
      return readCIMLK(aeriusSRMVersion.getPreSrmVersionNumber(), files);
    }
    throw new IllegalArgumentException("Unknown profile '" + theme + "', can't read background concentration data");
  }

  private static OwN2000WindRoseConcentrationGrid readOwN2000(final String preSRMVersion, final List<WindRoseFileName> files, final boolean allBackground)
      throws IOException {
    PointGrid<WindRoseConcentration> backgroundConcentrations = null;
    PointGrid<WindRoseConcentration> schipholCorrections = null;

    for (final WindRoseFileName f : files) {
      LOGGER.info("Loading {}", f.file.getName());
      if (SCHIPHOL.equalsIgnoreCase(f.correction)) {
        schipholCorrections = loadConcentratonForYear(preSRMVersion, f,
            new SchipholBackgroundConcentrationReader(preSRMVersion, f.yearPattern, f.year));
      } else if (IJMOND.equalsIgnoreCase(f.correction)) {
        // Ignore Ijmond files.
      } else {
        backgroundConcentrations = loadConcentratonForYear(preSRMVersion, f,
            new WindRoseConcentrationReader(preSRMVersion, f.yearPattern, f.year, allBackground));
      }
    }
    return new OwN2000WindRoseConcentrationGrid(backgroundConcentrations, schipholCorrections);
  }

  private static CIMLKWindRoseConcentrationGrid readCIMLK(final String preSRMVersion, final List<WindRoseFileName> files) throws IOException {
    PointGrid<WindRoseConcentration> backgroundConcentrations = null;
    PointGrid<WindRoseConcentration> schipholCorrections = null;
    IjmondCorrectionConcentrationGrid ijmondCorrections = null;

    for (final WindRoseFileName f : files) {
      LOGGER.info("Loading {}", f.file.getName());
      if (SCHIPHOL.equalsIgnoreCase(f.correction)) {
        schipholCorrections = loadConcentratonForYear(preSRMVersion, f,
            new SchipholBackgroundConcentrationReader(preSRMVersion, f.yearPattern, f.year));
      } else if (IJMOND.equalsIgnoreCase(f.correction)) {
        ijmondCorrections = readIjmond(preSRMVersion, f);
      } else {
        backgroundConcentrations = loadConcentratonForYear(preSRMVersion, f,
            new WindRoseConcentrationReader(preSRMVersion, f.yearPattern, f.year, true));
      }
    }
    return new CIMLKWindRoseConcentrationGrid(backgroundConcentrations, schipholCorrections, ijmondCorrections);
  }

  private static WindRoseConcentrationGrid loadConcentratonForYear(final String preSRMVersion, final WindRoseFileName f,
      final PreSrmXYReader<WindRoseConcentration> reader) throws IOException {
    try (final InputStream is = new FileInputStream(f.file)) {
      final LineReaderResult<WindRoseConcentration> results = reader.readObjects(is);
      final List<AeriusException> exceptions = results.getExceptions();

      if (!exceptions.isEmpty()) {
        throw new IOException("Error while reading meteo file (presrm:" + preSRMVersion + ", year:" + f.year + "): "
            + exceptions.get(0).getMessage(), exceptions.get(0));
      }
      final WindRoseConcentrationGrid grid =
          new WindRoseConcentrationGrid(reader.getColumns(), reader.getRows(), reader.getDiameter(), reader.getOffsetX(), reader.getOffsetY());
      grid.addAll(results.getObjects());
      return grid;
    }
  }

  private static IjmondCorrectionConcentrationGrid readIjmond(final String preSRMVersion, final WindRoseFileName f) throws IOException {
    try (final InputStream is = new FileInputStream(f.file)) {
      final IjmondBackgroundConcentrationReader reader =
          new IjmondBackgroundConcentrationReader(preSRMVersion, f.yearPattern, f.year);
      final LineReaderResult<IjmondCorrectionConcentration> results = reader.readObjects(is);
      final List<AeriusException> exceptions = results.getExceptions();

      if (!exceptions.isEmpty()) {
        throw new IOException("Error while reading meteo file (presrm:" + preSRMVersion + ", year:" + f.year + "): "
            + exceptions.get(0).getMessage(), exceptions.get(0));
      }
      final IjmondCorrectionConcentrationGrid grid = new IjmondCorrectionConcentrationGrid(reader.getColumns(), reader.getRows(),
          reader.getDiameter(), reader.getOffsetX(), reader.getOffsetY());
      grid.addAll(results.getObjects());
      return grid;
    }
  }

}
