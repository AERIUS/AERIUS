/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.util.FileUtil;

class WindRoseSpeedDataBuilder {
  private static class WindRoseFileName {
    File file;
    int year;
  }

  private static final Logger LOG = LoggerFactory.getLogger(WindRoseSpeedDataBuilder.class);

  private static final String WIND_ROSE_PROGNOSE_FILENAME = "windrose_prognose.txt";
  private static final Pattern WIND_ROSE_FILE_PATTERN = Pattern.compile("windrose_(\\d+)_year.txt");

  private final SRM2CalculationOptions options;

  public WindRoseSpeedDataBuilder(final SRM2CalculationOptions options) {
    this.options = options;
  }

  void loadWindRoseSpeedPrognose(final AeriusSRMVersion aeriusSRMVersion, final PreSRMData data, final File directory)
      throws IOException {
    LOG.info("Loading wind rose speed/factor prognose from: {}.", directory);
    final WindRoseSpeedReader reader = new WindRoseSpeedReader(aeriusSRMVersion.getPreSrmVersionNumber());
    final List<WindRoseSpeed> objects = readObjects(reader, new File(directory, WIND_ROSE_PROGNOSE_FILENAME));
    final WindRoseSpeedGrid table =
        new WindRoseSpeedGrid(reader.getColumns(), reader.getRows(), reader.getDiameter(), reader.getOffsetX(), reader.getOffsetY());

    table.addAll(objects);
    data.setWindRoseSpeedPrognoseGrid(aeriusSRMVersion, table);
  }

  void loadWindRoseByYear(final AeriusSRMVersion aeriusSRMVersion, final PreSRMData data, final File directory)
      throws IOException {
    final List<WindRoseFileName> roseFiles = findFiles(directory, WIND_ROSE_FILE_PATTERN);

    for (final WindRoseFileName roseFile : roseFiles) {
      LOG.info("Loading {}", roseFile.file.getName());
      final WindRoseSpeedReader reader = new WindRoseSpeedReader(aeriusSRMVersion.getPreSrmVersionNumber(), roseFile.year);
      final List<WindRoseSpeed> objects = readObjects(reader, roseFile.file);
      final WindRoseSpeedGrid table =
          new WindRoseSpeedGrid(reader.getColumns(), reader.getRows(), reader.getDiameter(), reader.getOffsetX(), reader.getOffsetY());

      table.addAll(objects);
      data.addWindRoseSpeedTable(aeriusSRMVersion, roseFile.year, table);
    }
  }

  private List<WindRoseSpeed> readObjects(final PreSrmXYReader<WindRoseSpeed> reader, final File file) throws IOException {
    try (final InputStream is = new FileInputStream(file)) {
      final LineReaderResult<WindRoseSpeed> results = reader.readObjects(is);
      final List<AeriusException> exceptions = results.getExceptions();

      if (!exceptions.isEmpty()) {
        throw new IOException("Error while reading meteo file.", exceptions.get(0));
      }
      return results.getObjects();
    }
  }

  private List<WindRoseFileName> findFiles(final File directory, final Pattern pattern) throws FileNotFoundException {
    return FileUtil.getFilteredFiles(directory, pattern, (dir, name, matcher) -> {
      final WindRoseFileName wrfn = new WindRoseFileName();

      wrfn.file = new File(dir, name);
      wrfn.year = Integer.parseInt(matcher.group(1));
      return wrfn;
    });
  }

}
