/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import nl.overheid.aerius.srm.SRMConstants;

/**
 * Grid object storing the {@link WindSpeed} data.
 */
public class WindSpeedGrid extends PointGrid<WindSpeed> {

  private static final WindSpeed NO_DATA_POINT = new WindSpeed(0, 0) {
    @Override
    public double getWindTotalSpeed() {
      return SRMConstants.NO_DATA;
    }
  };

  public WindSpeedGrid(final int columns, final int rows, final int diameter, final int offsetX, final int offsetY) {
    super(columns, rows, diameter, offsetX, offsetY, NO_DATA_POINT);
  }

  @Override
  protected WindSpeed[] createGrid(final int columns, final int rows) {
    return new WindSpeed[columns * rows];
  }
}
