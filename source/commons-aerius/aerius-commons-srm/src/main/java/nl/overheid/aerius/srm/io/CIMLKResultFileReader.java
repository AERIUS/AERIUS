/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Reader for CIMLK legacy result files.
 *
 * Available columns:
 * <pre>
 * calculation_point_id;calculation_year;aerius_version;aerius_database_version;label;monitor_substance;geometry;
 * NO2_total_concentration;PM10_total_concentration;PM25_total_concentration;EC_total_concentration;
 * NO2_exceedance_hours;PM10_exceedance_days;
 * NO2_background_concentration;PM10_background_concentration;PM25_background_concentration;EC_background_concentration;
 * NO2_SRM2_concentration_direct;PM10_SRM2_concentration;PM25_SRM2_concentration;EC_SRM2_concentration;NO2_SRM1_concentration_direct;
 * PM10_SRM1_concentration;PM25_SRM1_concentration;EC_SRM1_concentration;O3_background_concentration;
 * NOx_SRM2_concentration;NOx_SRM1_concentration;NO2_SRM_concentration_converted;
 * NO2_GCN_main_roads_correction;PM10_GCN_main_roads_correction;PM25_GCN_main_roads_correction;EC_GCN_main_roads_correction;
 * NO2_user_correction;PM10_user_correction;PM25_user_correction;EC_user_correction
 * </pre>
 */
public class CIMLKResultFileReader extends AbstractCIMLKFileReader<CIMLKResult> {

  // @formatter:off
  private enum ResultColumns implements Columns {
    CALCULATION_POINT_ID, CALCULATION_YEAR, AERIUS_VERSION, AERIUS_DATABASE_VERSION, LABEL, MONITOR_SUBSTANCE, GEOMETRY,
    NO2_TOTAL_CONCENTRATION, PM10_TOTAL_CONCENTRATION, PM25_TOTAL_CONCENTRATION, EC_TOTAL_CONCENTRATION,
    NO2_EXCEEDANCE_HOURS, PM10_EXCEEDANCE_DAYS,
    NO2_BACKGROUND_CONCENTRATION, PM10_BACKGROUND_CONCENTRATION, PM25_BACKGROUND_CONCENTRATION, EC_BACKGROUND_CONCENTRATION,
    NO2_SRM2_CONCENTRATION_DIRECT, PM10_SRM2_CONCENTRATION, PM25_SRM2_CONCENTRATION, EC_SRM2_CONCENTRATION, NO2_SRM1_CONCENTRATION_DIRECT,
    PM10_SRM1_CONCENTRATION, PM25_SRM1_CONCENTRATION, EC_SRM1_CONCENTRATION, O3_BACKGROUND_CONCENTRATION,
    NOX_SRM2_CONCENTRATION, NOX_SRM1_CONCENTRATION, NO2_SRM_CONCENTRATION_CONVERTED,
    NO2_GCN_MAIN_ROADS_CORRECTION, PM10_GCN_MAIN_ROADS_CORRECTION, PM25_GCN_MAIN_ROADS_CORRECTION, EC_GCN_MAIN_ROADS_CORRECTION,
    NO2_USER_CORRECTION, PM10_USER_CORRECTION, PM25_USER_CORRECTION, EC_USER_CORRECTION;
    // @formatter:on

    static ResultColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  @Override
  protected CIMLKResult parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final CIMLKResult result = new CIMLKResult();
    result.setCalculationPointId(getGmlId(ResultColumns.CALCULATION_POINT_ID, GMLIdUtil.POINT_PREFIX, warnings));
    result.setCalculationYear(getInt(ResultColumns.CALCULATION_YEAR));
    result.setAeriusVersion(getString(ResultColumns.AERIUS_VERSION));
    result.setAeriusDatabaseVersion(getString(ResultColumns.AERIUS_DATABASE_VERSION));
    result.setLabel(getString(ResultColumns.LABEL));
    result.setMonitorSubstance(getString(ResultColumns.MONITOR_SUBSTANCE));
    result.setGeometry(getString(ResultColumns.GEOMETRY));
    result.setPoint(wktToPoint());
    result.setTotalConcentrationNO2(getDouble(ResultColumns.NO2_TOTAL_CONCENTRATION));
    result.setTotalConcentrationPM10(getDouble(ResultColumns.PM10_TOTAL_CONCENTRATION));
    result.setTotalConcentrationPM25(getDouble(ResultColumns.PM25_TOTAL_CONCENTRATION));
    result.setTotalConcentrationEC(getDouble(ResultColumns.EC_TOTAL_CONCENTRATION));
    result.setExceedanceHoursNO2(getDouble(ResultColumns.NO2_EXCEEDANCE_HOURS));
    result.setExceedanceDaysPM10(getDouble(ResultColumns.PM10_EXCEEDANCE_DAYS));
    result.setBackgroundConcentrationNO2(getDouble(ResultColumns.NO2_BACKGROUND_CONCENTRATION));
    result.setBackgroundConcentrationPM10(getDouble(ResultColumns.PM10_BACKGROUND_CONCENTRATION));
    result.setBackgroundConcentrationPM25(getDouble(ResultColumns.PM25_BACKGROUND_CONCENTRATION));
    result.setBackgroundConcentrationEC(getDouble(ResultColumns.EC_BACKGROUND_CONCENTRATION));
    result.setSrm2DirectConcentrationNO2(getDouble(ResultColumns.NO2_SRM2_CONCENTRATION_DIRECT));
    result.setSrm2ConcentrationPM10(getDouble(ResultColumns.PM10_SRM2_CONCENTRATION));
    result.setSrm2ConcentrationPM25(getDouble(ResultColumns.PM25_SRM2_CONCENTRATION));
    result.setSrm2ConcentrationEC(getDouble(ResultColumns.EC_SRM2_CONCENTRATION));
    result.setSrm1DirectConcentrationNO2(getDouble(ResultColumns.NO2_SRM1_CONCENTRATION_DIRECT));
    result.setSrm1ConcentrationPM10(getDouble(ResultColumns.PM10_SRM1_CONCENTRATION));
    result.setSrm1ConcentrationPM25(getDouble(ResultColumns.PM25_SRM1_CONCENTRATION));
    result.setSrm1ConcentrationEC(getDouble(ResultColumns.EC_SRM1_CONCENTRATION));
    result.setBackgroundConcentrationO3(getDouble(ResultColumns.O3_BACKGROUND_CONCENTRATION));
    result.setSrm2ConcentrationNOx(getDouble(ResultColumns.NOX_SRM2_CONCENTRATION));
    result.setSrm1ConcentrationNOx(getDouble(ResultColumns.NOX_SRM1_CONCENTRATION));
    result.setSrmConcentrationConvertedNO2(getDouble(ResultColumns.NO2_SRM_CONCENTRATION_CONVERTED));
    result.setMainRoadsCorrectionNO2(getDouble(ResultColumns.NO2_GCN_MAIN_ROADS_CORRECTION));
    result.setMainRoadsCorrectionPM10(getDouble(ResultColumns.PM10_GCN_MAIN_ROADS_CORRECTION));
    result.setMainRoadsCorrectionPM25(getDouble(ResultColumns.PM25_GCN_MAIN_ROADS_CORRECTION));
    result.setMainRoadsCorrectionEC(getDouble(ResultColumns.EC_GCN_MAIN_ROADS_CORRECTION));
    result.setUserCorrectionNO2(getDouble(ResultColumns.NO2_USER_CORRECTION));
    result.setUserCorrectionPM10(getDouble(ResultColumns.PM10_USER_CORRECTION));
    result.setUserCorrectionPM25(getDouble(ResultColumns.PM25_USER_CORRECTION));
    result.setUserCorrectionEC(getDouble(ResultColumns.EC_USER_CORRECTION));
    return result;
  }

  private AeriusPoint wktToPoint() throws AeriusException {
    final String wktString = getString(ResultColumns.GEOMETRY);
    final String[] points = getWktGeometry(ResultColumns.GEOMETRY, geometry -> geometry.type() != GeometryType.POINT).getPoints().split("\\s+");

    if (points.length == 2) {
      return new AeriusPoint(Double.parseDouble(points[0]), Double.parseDouble(points[1]));
    } else {
      throw new AeriusException(AeriusExceptionReason.SRM2_INCORRECT_WKT_VALUE, String.valueOf(getCurrentLineNumber()), wktString);
    }
  }

  @Override
  Columns[] expectedColumns() {
    return ResultColumns.values();
  }

  @Override
  Columns safeColumnOf(final String value) {
    return ResultColumns.safeValueOf(value);
  }

}
