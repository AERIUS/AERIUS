/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.version;

import nl.overheid.aerius.calculation.EngineVersion;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.meteo.Meteo;

/**
 * Versions of data files used by SRM calculation.
 */
public enum AeriusSRMVersion implements EngineVersion {
  /*
   * Omgevingswet Natura2000 related constants.
   * For the Omgevingswet Natura2000 only prognose meteo data is used, also for historical years. Therefor -1 is passed for prognoseYear.
   */
  STABLE("SRM-2024", Theme.OWN2000, "SRM2023","depositionvelocity-vd_zoomlevel-2_all_20230421", "presrm-lu-2.401", "2.402-1", -1, ""),
  LATEST("SRM-2024", Theme.OWN2000, "SRM2023","depositionvelocity-vd_zoomlevel-2_all_20230421", "presrm-lu-2.401", "2.402-1", -1, ""),
  CHECK_STABLE("SRM-2022", Theme.OWN2000, "SRM2022","depositionvelocity-vd_zoomlevel-2_all_20220718", "presrm-lu-2.203", "2.203-1", -1, ""),
  /*
   * CIMLK related constants
   */
  RBL_STABLE("SRM-2024-RBL", Theme.RBL, "SRM2023","", "presrm-lu-2.401", "2.401-1", 2023, "windvelden-2024"),
  RBL_LATEST("SRM-2025-RBL", Theme.RBL, "SRM2023","", "presrm-lu-2.401", "2.401-1", 2023, "windvelden-2024");

  private static final String PRE_SRM_VERSION_PREFIX = "aerius-presrm-";
  private static final String DEPOSITION_VELOCITY_FILE_FORMAT = ".csv";
  /**
   * Years on which the generated PreSRM prognose is based.
   */
  private static final Meteo PRESRM_PROGNOSE_BASE_METEO = new Meteo(2005, 2014);
  private final String version;
  private final String srmConstantsVersion;
  private final String depositionVelocityVersion;
  private final String landUseVersion;
  private final String preSrmVersion;
  private final String windveldenVersion;
  private final Theme theme;
  private final int prognoseYear;

  /**
   * Constructs a SRM version enum with theme and release version specific data.
   *
   * @param version Unique key to identify this version. Not used, but can be set in the database as alternative instead of enum value
   * @param theme Theme this version is related to
   * @param srmConstantsVersion Version of the SRM data; used in OpenCL code to differentiate implementations
   * @param depositionVelocityVersion Version of the deposition velocity file. Only relevant for deposition calculations
   * @param landUseVersion PreSrm version of the landuse data
   * @param preSrmVersion The PreSrm version used. Appended with the AERIUS specific version of the data
   * @param prognoseYear Year on which the generated PreSRM prognose is based. Use -1
   * @param windveldenVersion version number of the windvelden data, needed for SRM1 calculations
   */
  AeriusSRMVersion(final String version, final Theme theme, final String srmConstantsVersion, final String depositionVelocityVersion,
      final String landUseVersion, final String preSrmVersion, final int prognoseYear, final String windveldenVersion) {
    this.version = version;
    this.theme = theme;
    this.srmConstantsVersion = srmConstantsVersion;
    this.landUseVersion = landUseVersion;
    this.depositionVelocityVersion = depositionVelocityVersion;
    this.preSrmVersion = preSrmVersion;
    this.prognoseYear = prognoseYear;
    this.windveldenVersion = windveldenVersion;
  }

  @Override
  public String getVersion() {
    return version;
  }

  public Theme getTheme() {
    return theme;
  }

  public String getSrmConstantsVersion() {
    return srmConstantsVersion;
  }

  public String getDepositionVelocityVersion() {
    return depositionVelocityVersion;
  }

  public String getDepositionVelocityFileName() {
    return depositionVelocityVersion + DEPOSITION_VELOCITY_FILE_FORMAT;
  }

  public String getLandUseVersion() {
    return landUseVersion;
  }

  public String getAeriusPreSrmVersion() {
    return PRE_SRM_VERSION_PREFIX + preSrmVersion;
  }

  public String getPreSrmVersionNumber() {
    return preSrmVersion;
  }

  public Meteo getPrognoseBaseMeteo() {
    return PRESRM_PROGNOSE_BASE_METEO;
  }

  /**
   * Returns the yearly meteo if available, prognose meteo otherwise. This is determined as follows:
   * If the requested year is beyond the current prognoseYear, return the prognose meteo.
   * The prognoseYear indicates up until which year yearly meteo is available.
   *
   * @param year year to request meteo for
   * @return Meteo
   */
  public Meteo getMeteoForYear(final int year) {
    if (year > prognoseYear) {
      return PRESRM_PROGNOSE_BASE_METEO;
    } else {
      return new Meteo(year);
    }
  }

  public String getWindveldenVersion() {
    return windveldenVersion;
  }

  public static AeriusSRMVersion getByVersionLabel(final String aeriusSRMVersion) {
    return EngineVersion.getByVersionLabel(AeriusSRMVersion.values(), "SRM", aeriusSRMVersion);
  }
}
