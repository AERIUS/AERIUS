/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import java.nio.DoubleBuffer;

/**
 * Two dimensional data grid.
 */
public class DataGrid {
  private final int width;
  private final int height;
  private final DoubleBuffer data;

  /**
   * Creates a new data grid.
   *
   * @param width The number of cells in the x direction.
   * @param height The number of cells in the y direction.
   * @param data A direct buffer containing the data in row major order.
   */
  public DataGrid(final int width, final int height, final DoubleBuffer data) {
    if (width <= 0) {
      throw new IllegalArgumentException("Width should be larger than 0.");
    }

    if (height <= 0) {
      throw new IllegalArgumentException("Height should be larger than 0.");
    }

    if (!data.isDirect()) {
      throw new IllegalArgumentException("Data should be a direct double buffer.");
    }

    if (data.capacity() != width * height) {
      throw new IllegalArgumentException("Invalid data size. Should be equal to width * height.");
    }

    this.width = width;
    this.height = height;
    this.data = data;
  }

  /**
   * @return The number of cells in the x direction.
   */
  public int getWidth() {
    return width;
  }

  /**
   * @return The number of cells in the y direction.
   */
  public int getHeight() {
    return height;
  }

  /**
   * @return The underlying direct DoubleBuffer.
   */
  public DoubleBuffer getData() {
    return data;
  }

  @Override
  public String toString() {
    return "DataGrid [width=" + width + ", height=" + height + "]";
  }
}
