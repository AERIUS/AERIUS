/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.function.Supplier;

import nl.overheid.aerius.backgrounddata.DepositionVelocityMap;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * Utility class that looks up deposition velocities for level 1 hexagons, in a map containing level 2 hexagons,
 * interpolating where necessary.
 */
public final class SRMDepositionVelocityMap extends DepositionVelocityMap {
  private static final int HEXAGON_IN_LEVEL = 4;
  private static final BigDecimal TWO = BigDecimal.valueOf(2);

  /**
   * @param hexHor Number of hexagons on a receptor grid row for zoomlevel 1.
   * @param receptorUtil The receptor util to calculate receptor id's
   * @param defaultValueSupplier supplies the default value if no value available
   */
  public SRMDepositionVelocityMap(final int hexHor, final ReceptorUtil receptorUtil, final Supplier<Double> defaultValueSupplier) {
    super(hexHor, receptorUtil, defaultValueSupplier);
  }

  /**
   * Finds the deposition velocity for the level 1 hexagon with the given id.
   *
   * @param receptorId The level 1 receptor id.
   * @return The deposition velocity, or null if they could not be found.
   */
  @Override
  public Double getDepositionVelocityById(final int receptorId) {
    final int id = receptorId - 1;
    final int dx = id % hexHor;
    final int dy = id / hexHor;

    final Double result;

    final boolean modX = dx % 2 == 0;
    switch (dy % HEXAGON_IN_LEVEL) {
    case 0:
      result = resultCase02(dx, dy, modX);
      break;
    case 1:
      result = resultCase13(dx, dy, modX);
      break;
    case 2:
      result = resultCase02(dx, dy, !modX);
      break;
    default: // 3
      result = resultCase13(dx, dy, !modX);
    }
    return result;
  }

  private Double resultCase02(final int dx, final int dy, final boolean modX) {
    final Double result;
    if (modX) {
      result = getLvl2Velocity(dx, dy);
    } else {
      result = getResult(dx, dy - 2, dx, dy + 2);
    }
    return result;
  }

  private Double resultCase13(final int dx, final int dy, final boolean modX) {
    final Double result;
    if (modX) {
      result = getResult(dx, dy - 1, dx + 1, dy + 1);
    } else {
      result = getResult(dx, dy + 1, dx + 1, dy - 1);
    }
    return result;
  }

  private Double getResult(final int dv1x, final int dv1y, final int dv2x, final int dv2y) {
    final Double result1 = getLvl2Velocity(dv1x, dv1y);
    final Double result2 = getLvl2Velocity(dv2x, dv2y);

    return result1 == null || result2 == null ? null
        : BigDecimal.valueOf(result1).add(BigDecimal.valueOf(result2)).divide(TWO, MathContext.DECIMAL64).doubleValue();
  }

  private Double getLvl2Velocity(final int x, final int y) {
    final int id = x + y * hexHor + 1;

    return depositionVelocities.get(id);
  }
}
