/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import java.nio.DoubleBuffer;
import java.util.Set;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLKernel;

import nl.overheid.aerius.opencl.Connection;
import nl.overheid.aerius.opencl.MemoryUsage;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;

/**
 * Calculation context for NH3 concentration/deposition and PM10 and PM25 concentration calculation.
 *
 * This file is named NH3, but also calculates PM10, PM25 as that is the same algorithm, except the latter doesn't have deposition.
 */
class NH3CalculationContext implements SubstanceCalculationContext {
  private static final String CALCULATE_NH3_KERNEL_NAME = "calculateContributionNH3";
  private static final String SUM_NH3_KERNEL_NAME = "sumContributionsNH3";

  private static final int WORK_SIZE = 2;
  private static final int RECEPTOR_BACKGROUND_SIZE = 1;

  private static final int CALCULATION_RECEPTOR_DEPOSITION_VELOCITY_ARG_IDX = 10;
  private static final int CALCULATION_RECEPTOR_BACKGROUND_ARG_IDX = 11;

  private CLBuffer<DoubleBuffer> receptorBackgroundBuffer;

  private final Set<EmissionResultKey> emissionResultKeys;

  public NH3CalculationContext(final Set<EmissionResultKey> emissionResultKeys) {
    this.emissionResultKeys = emissionResultKeys;
  }

  @Override
  public String getCalculationKernelName() {
    return CALCULATE_NH3_KERNEL_NAME;
  }

  @Override
  public String getSumKernelName() {
    return SUM_NH3_KERNEL_NAME;
  }

  @Override
  public int getWorkSize() {
    return WORK_SIZE;
  }

  @Override
  public long getAdditionalBatchSizeInBytes() {
    return RECEPTOR_BACKGROUND_SIZE * SRMConstants.DOUBLE_BYTES;
  }

  @Override
  public void initReceptorBuffers(final Connection con, final CLKernel calculationKernel, final CLKernel sumKernel, final int batchSize,
      final CLBuffer<DoubleBuffer> receptorPositionBuffer, final CLBuffer<DoubleBuffer> receptorDepositionVelocityBuffer,
      final CLBuffer<DoubleBuffer> receptorWindBuffer) {
    receptorBackgroundBuffer = con.createDoubleCLBuffer(batchSize, MemoryUsage.READ_ONLY);
    calculationKernel.setArg(CALCULATION_RECEPTOR_DEPOSITION_VELOCITY_ARG_IDX, receptorDepositionVelocityBuffer);
    calculationKernel.setArg(CALCULATION_RECEPTOR_BACKGROUND_ARG_IDX, receptorBackgroundBuffer);
  }

  @Override
  public void prepareBatch(final Connection con) {
    con.rewindAndUploadBuffer(receptorBackgroundBuffer);
  }

  @Override
  public void prepareBatchWindRoseBuffers(final WindRoseConcentration windrose) {
    receptorBackgroundBuffer.getBuffer().put(windrose == null ? 0.0D : windrose.getBackgroundNH3());
  }

  @Override
  public void runBatch(final Connection con, final CLKernel calculationKernel, final CLKernel sumKernel, final int workSize,
      final int actualBatchSize) {
    con.queue1DKernelExecution(calculationKernel, workSize);
    con.queue1DKernelExecution(sumKernel, actualBatchSize);
  }

  @Override
  public void downloadBatchResults(final AeriusResultPoint aeriusResultPoint, final DoubleBuffer buffer, final int index) {
    if (emissionResultKeys.contains(EmissionResultKey.NH3_CONCENTRATION)) {
      setData(aeriusResultPoint, EmissionResultKey.NH3_CONCENTRATION, buffer.get(index));
    }
    if (emissionResultKeys.contains(EmissionResultKey.NH3_DEPOSITION)) {
      setData(aeriusResultPoint, EmissionResultKey.NH3_DEPOSITION, buffer.get(index + 1));
    }
    if (emissionResultKeys.contains(EmissionResultKey.PM10_CONCENTRATION)) {
      setData(aeriusResultPoint, EmissionResultKey.PM10_CONCENTRATION, buffer.get(index));
    }
    if (emissionResultKeys.contains(EmissionResultKey.PM25_CONCENTRATION)) {
      setData(aeriusResultPoint, EmissionResultKey.PM25_CONCENTRATION, buffer.get(index));
    }
    if (emissionResultKeys.contains(EmissionResultKey.EC_CONCENTRATION)) {
      setData(aeriusResultPoint, EmissionResultKey.EC_CONCENTRATION, buffer.get(index));
    }
  }
}
