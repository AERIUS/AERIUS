/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl.buffer.spi;

import java.nio.ByteBuffer;

/**
 * Interface to abstract the JVM specific immediate release of direct buffer allocations.<p>
 * Implementations are required to have a public default constructor.
 */
public interface DirectBufferReleaser {
  /**
   * Releases the direct memory allocated for the buffer. Does nothing if isDirect() is false.
   * @param directBuffer The buffer whose allocated memory needs to be released.
   */
  void release(ByteBuffer directBuffer);
}
