/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link PointGrid}.
 */
class WindRoseTableTest {

  @Test
  void testGet() {
    final WindRoseSpeedGrid table = new WindRoseSpeedGrid(10, 10, 1000, 10000, 10000);
    final List<WindRoseSpeed> list = new ArrayList<>();

    list.add(new WindRoseSpeed(10500, 10500));
    list.add(new WindRoseSpeed(11500, 10500));
    list.add(new WindRoseSpeed(12500, 10500));
    list.add(new WindRoseSpeed(10500, 11500));
    list.add(new WindRoseSpeed(11500, 11500));
    list.add(new WindRoseSpeed(12500, 11500));
    list.add(new WindRoseSpeed(10500, 12500));

    final WindRoseSpeed windrose = new WindRoseSpeed(11500, 12500);
    list.add(windrose);
    windrose.setAvgWindSpeed(3, 3.33f);
    list.add(new WindRoseSpeed(12500, 12500));
    table.addAll(list);

    assertSame(windrose, table.get(11000, 12001), "Find windrose left-below");
    assertSame(windrose, table.get(11000, 13000), "Find windrose left-below");
    assertSame(windrose, table.get(11999, 12001), "Find windrose left-below");
    assertSame(windrose, table.get(11999, 13000), "Find windrose left-below");
    assertEquals(3.33f, table.get(11700, 12700).getAvgWindSpeed(3), 0.0001, "Is it really the same, check value");
  }
}
