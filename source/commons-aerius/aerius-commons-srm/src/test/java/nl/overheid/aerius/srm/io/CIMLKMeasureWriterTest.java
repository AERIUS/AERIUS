/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.base.EmissionReduction;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasure;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicleMeasure;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;

/**
 * Test class for {@link CIMLKMeasureWriter}.
 */
class CIMLKMeasureWriterTest {

  @Test
  void testNormal() throws IOException {
    final CIMLKMeasureWriter resultWriter = new CIMLKMeasureWriter(4);

    final File outputFile = new File(Files.createTempDirectory("CIMLKMeasureWriterTest").toFile(), "normalTestFile.csv");

    final List<CIMLKMeasureFeature> measures = new ArrayList<>();
    measures.add(testCase(87));

    resultWriter.write(outputFile, measures);

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_measures.csv"));

    assertEquals(expected, result, "Result for normal CIMLK points export");
  }

  @Test
  void testWriteSeparateActions() throws IOException {
    final CIMLKMeasureWriter resultWriter = new CIMLKMeasureWriter(4);

    final File outputFile = new File(Files.createTempDirectory("CIMLKMeasureWriterTest").toFile(), "extendedTestFile.csv");

    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeHeader(writer);
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(25));
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(600));
    }

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_measures_separate_writing.csv"));

    assertEquals(expected, result, "Result for separate writing CIMLK points export");
  }

  private static CIMLKMeasureFeature testCase(final int id) {
    final CIMLKMeasureFeature feature = new CIMLKMeasureFeature();
    final CIMLKMeasure measure = new CIMLKMeasure();
    feature.setProperties(measure);
    measure.setGmlId("PREFIX." + id);
    measure.setLabel("maatregel " + id);
    measure.setDescription("maatregelen voor gebieden");

    final StandardVehicleMeasure vehicleMeasure = new StandardVehicleMeasure();
    vehicleMeasure.setVehicleTypeCode(VehicleType.LIGHT_TRAFFIC.getStandardVehicleCode());
    vehicleMeasure.setRoadTypeCode(RoadSpeedType.URBAN_TRAFFIC_NORMAL.getRoadTypeCode());
    final EmissionReduction emissionReduction1 = new EmissionReduction();
    emissionReduction1.setSubstance(Substance.PM10);
    emissionReduction1.setFactor(id * 1.123456789);
    vehicleMeasure.addEmissionReduction(emissionReduction1);
    final EmissionReduction emissionReduction2 = new EmissionReduction();
    emissionReduction2.setSubstance(Substance.NOX);
    emissionReduction2.setFactor(id * 5.0);
    vehicleMeasure.addEmissionReduction(emissionReduction2);
    measure.getVehicleMeasures().add(vehicleMeasure);

    final StandardVehicleMeasure vehicleMeasure2 = new StandardVehicleMeasure();
    vehicleMeasure2.setVehicleTypeCode(VehicleType.NORMAL_FREIGHT.getStandardVehicleCode());
    vehicleMeasure2.setRoadTypeCode(RoadSpeedType.URBAN_TRAFFIC_NORMAL.getRoadTypeCode());
    final EmissionReduction emissionReduction3 = new EmissionReduction();
    emissionReduction3.setSubstance(Substance.PM10);
    emissionReduction3.setFactor(id * 0.8);
    vehicleMeasure2.addEmissionReduction(emissionReduction3);
    measure.getVehicleMeasures().add(vehicleMeasure2);

    final Polygon polygon = new Polygon();
    polygon.setCoordinates(new double[][][] {{
      {(id + 20.0), (id + 10.0)},
      {(id + 20.0), (id + 30.0)},
      {(id + 40.0), (id + 30.0)},
      {(id + 20.0), (id + 10.0)}
    }});
    feature.setGeometry(polygon);

    if (id % 2 == 1) {
      measure.setJurisdictionId(4);
    }

    return feature;
  }

}
