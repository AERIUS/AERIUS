/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.stream.Collectors;

/**
 *
 */
final class AssertCSV {

  private AssertCSV() {
    //util convenience class.
  }

  static String getFileContent(final File file) throws IOException {
    // Remove windows style line ending because csv output writes files with windows line endings
    // while our test files are unix file format.
    return Files.readAllLines(file.toPath()).stream().collect(Collectors.joining());
  }

  static File getFile(final String fileName) throws FileNotFoundException {
    final URL url = AssertCSV.class.getResource(fileName);
    File file = null;
    if (url != null) {
      file = new File(url.getFile());
      if (!file.exists()) {
        file = null;
      }
    }
    return file;
  }

  static InputStream getFileInputStream(final String fileName) throws FileNotFoundException {
    final InputStream is = AssertCSV.class.getResourceAsStream(fileName);
    if (is == null) {
      throw new FileNotFoundException("Input file not found:" + fileName);
    }
    return new BufferedInputStream(is);
  }

}
