/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link CIMLKCorrectionFileReader}.
 */
class CIMLKCorrectionFileReaderTest {

  private static final String SPECIFIC_TEST_FILE = "correction_example.csv";
  private static final String INCORRECT_ENUM_TEST_FILE = "correction_incorrect_enum_example.csv";
  private static final String INCORRECT_NUMBER_TEST_FILE = "correction_incorrect_number_example.csv";

  @Test
  void testSpecificCase() throws Exception {
    final CIMLKCorrectionFileReader reader = new CIMLKCorrectionFileReader();
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {

      final String header = bufferedReader.readLine();
      final AeriusException parsingHeaderException = reader.parseHeader(header);

      assertNull(parsingHeaderException, "Header should be correctly parsed");

      final LineReaderResult<CIMLKCorrection> result = reader.readObjects(bufferedReader);

      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
      final List<CIMLKCorrection> corrections = result.getObjects();
      validateResults(corrections);
    }
  }

  @Test
  void testSpecificCaseWithMainImporter() throws Exception {
    final LegacyNSLImportReader reader = new LegacyNSLImportReader();
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {

      final SectorCategories categories = Mockito.mock(SectorCategories.class);
      final Substance substance = Substance.NOX;
      final ImportParcel importResult = new ImportParcel();

      reader.read("some name", inputStream, categories, substance, importResult);

      if (!importResult.getExceptions().isEmpty()) {
        throw importResult.getExceptions().get(0);
      }
      final List<CIMLKCorrection> corrections = importResult.getSituation().getCimlkCorrections();
      validateResults(corrections);
    }
  }

  @Test
  void testIncorrectEnumCase() throws Exception {
    final CIMLKCorrectionFileReader reader = new CIMLKCorrectionFileReader();
    final File file = new File(getClass().getResource(INCORRECT_ENUM_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {

      final String header = bufferedReader.readLine();
      final AeriusException parsingHeaderException = reader.parseHeader(header);

      assertNull(parsingHeaderException, "Header should be correctly parsed");

      final LineReaderResult<CIMLKCorrection> result = reader.readObjects(bufferedReader);

      if (result.getExceptions().isEmpty()) {
        fail("Expected exceptions at this point");
      } else {
        assertEquals(1, result.getExceptions().size(), "Expected number of errors");
        assertEquals(AeriusExceptionReason.CSV_INCORRECT_ENUM_VALUE, result.getExceptions().get(0).getReason(), "Expected reason");
        assertArrayEquals(new String[] {"2", "een correctie", "NONOx", "SUBSTANCE"}, result.getExceptions().get(0).getArgs(), "Expected reason");
      }
    }
  }

  @Test
  void testIncorrectNumberCase() throws Exception {
    final CIMLKCorrectionFileReader reader = new CIMLKCorrectionFileReader();
    final File file = new File(getClass().getResource(INCORRECT_NUMBER_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {

      final String header = bufferedReader.readLine();
      final AeriusException parsingHeaderException = reader.parseHeader(header);

      assertNull(parsingHeaderException, "Header should be correctly parsed");

      final LineReaderResult<CIMLKCorrection> result = reader.readObjects(bufferedReader);

      if (result.getExceptions().isEmpty()) {
        fail("Expected exceptions at this point");
      } else {
        assertEquals(1, result.getExceptions().size(), "Expected number of errors");
        assertEquals(AeriusExceptionReason.IO_EXCEPTION_NUMBER_FORMAT, result.getExceptions().get(0).getReason(), "Expected reason");
        assertArrayEquals(new String[] {"2", "VALUE", "-1punt9"}, result.getExceptions().get(0).getArgs(), "Expected reason");
      }
    }
  }

  private void validateResults(final List<CIMLKCorrection> corrections) throws AeriusException {
    assertEquals(1, corrections.size(), "Count nr. of corrections");
    final CIMLKCorrection correction = corrections.get(0);
    assertEquals("een correctie", correction.getLabel(), "Label");
    assertNotNull(correction.getJurisdictionId(), "Jurisdiction id");
    assertEquals(12, correction.getJurisdictionId().intValue(), "Jurisdiction id");
    assertEquals(EmissionResultType.CONCENTRATION, correction.getResultType(), "Result type");
    assertEquals(Substance.NOX, correction.getSubstance(), "emission type");
    assertEquals(-1.9, correction.getValue(), 1E-10, "value");
    assertEquals("PREFIX.9101", correction.getCalculationPointGmlId(), "calculation point id");
    assertEquals("beschrijving van correctie", correction.getDescription(), "Description");
  }

}
