/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.nio.DoubleBuffer;

import org.junit.jupiter.api.Test;

import com.jogamp.common.nio.Buffers;

/**
 * Test class for {@link MapData}.
 */
class MapDataTest {
  private static int WIDTH = 5;
  private static int HEIGHT = 6;
  private static double OFFSET_X = 250;
  private static double OFFSET_Y = 300;
  private static double DIAMETER = 10;
  private static final DoubleBuffer DATA = Buffers.newDirectDoubleBuffer(
      new double[] {
          1, 2, 3, 4, 5,
          6, 7, 8, 9, 10,
          11, 12, 13, 14, 15,
          16, 17, 18, 19, 20,
          21, 22, 23, 24, 25,
          26, 27, 28, 29, 30});

  @Test
  void testConstructorOk() {
    final MapData test = new MapData(WIDTH, HEIGHT, OFFSET_X, OFFSET_Y, DIAMETER, DATA);

    assertNotNull(test, "DataGrid not created.");
    assertEquals(WIDTH, test.getWidth(), "Invalid width.");
    assertEquals(HEIGHT, test.getHeight(), "Invalid height.");
    assertEquals(OFFSET_X, test.getOffsetX(), 0.1, "Invalid width.");
    assertEquals(OFFSET_Y, test.getOffsetY(), 0.1, "Invalid height.");
    assertEquals(DIAMETER, test.getDiameter(), 0.1, "Invalid height.");
  }

  @Test
  void testInvalidDiameter() {
    assertThrows(IllegalArgumentException.class, () -> new MapData(WIDTH, HEIGHT, OFFSET_X, OFFSET_Y, -10, DATA),
        "Expected exception on invalid diameter");
  }

  @Test
  void testGetValue() {
    final MapData test = new MapData(WIDTH, HEIGHT, OFFSET_X, OFFSET_Y, DIAMETER, DATA);

    assertValues(test, 0, 0, -1);
    assertValues(test, 0, 500, -1);
    assertValues(test, 500, 0, -1);
    assertValues(test, 500, 500, -1);
    assertValues(test, -10, -10, -1);
    assertValues(test, 250, 250, -1);
    assertValues(test, 250, 340, 21);
    assertValues(test, 290, 320, 15);
  }

  private static void assertValues(final MapData test, final double x, final double y, final double expected) {
    assertEquals(expected, test.getValue(x, y), 0.1, "Invalid value for [" + x + ", " + y + "]");
  }
}
