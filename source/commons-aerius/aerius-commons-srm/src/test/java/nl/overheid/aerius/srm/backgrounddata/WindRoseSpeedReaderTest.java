/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;

/**
 * Test class for {@link WindRoseSpeedReader}.
 */
class WindRoseSpeedReaderTest {

  private static final AeriusSRMVersion STABLE = MockAeriusSRMVersion.mockAeriusSRMVersionLatest();
  private static final String PRE_SRM_VERSION = STABLE.getPreSrmVersionNumber();
  private static final String WINDROSE_TXT = STABLE.getAeriusPreSrmVersion() + "/windrose_2018_year.txt";

  @Test
  void testReadFile() throws IOException {
    final WindRoseSpeedReader reader = new WindRoseSpeedReader(PRE_SRM_VERSION, 2018);
    final LineReaderResult<WindRoseSpeed> results = readResults(reader, WINDROSE_TXT);
    assertEquals(43960, results.getObjects().size(), "# rows");
    final int row = 0;
    assertEquals(0.02112, results.getObjects().get(row).getWindFactor(35), 0.0001, "Windfactor sector 35");
    assertEquals(2.961, results.getObjects().get(row).getAvgWindSpeed(35), 0.0001, "Avg windspeed sector 35");
    assertEquals(13500.0, results.getObjects().get(row).getX(), 0.0, "X");
    assertEquals(370500.0, results.getObjects().get(row).getY(), 0.0, "Y");
  }

  private LineReaderResult<WindRoseSpeed> readResults(final WindRoseSpeedReader reader, final String filename) throws IOException {
    final LineReaderResult<WindRoseSpeed> results;

    try (final InputStream is = WindRoseSpeedReaderTest.class.getResourceAsStream(filename)) {
      results = reader.readObjects(is);
    }
    return results;
  }
}
