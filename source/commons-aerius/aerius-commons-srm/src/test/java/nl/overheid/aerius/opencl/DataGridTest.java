/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.nio.DoubleBuffer;

import org.junit.jupiter.api.Test;

import com.jogamp.common.nio.Buffers;

/**
 * Unit test for {@link DataGrid}.
 */
public class DataGridTest {
  @Test
  public void testConstructorOk() {
    final DataGrid test = new DataGrid(5, 6, Buffers.newDirectDoubleBuffer(30));

    assertNotNull(test, "DataGrid not created.");
    assertEquals(5, test.getWidth(), "Invalid width.");
    assertEquals(6, test.getHeight(), "Invalid height.");
  }

  @Test
  public void testInvalidWidth() {
    assertThrows(IllegalArgumentException.class, () -> new DataGrid(-1, 6, Buffers.newDirectDoubleBuffer(30)),
        "Expected exception for invalid width");
  }

  @Test
  public void testInvalidHeight() {
    assertThrows(IllegalArgumentException.class, () -> new DataGrid(5, -1, Buffers.newDirectDoubleBuffer(30)),
        "Expected exception for invalid height");
  }

  @Test
  public void testInvalidBufferSize() {
    assertThrows(IllegalArgumentException.class, () -> new DataGrid(5, 6, Buffers.newDirectDoubleBuffer(25)),
        "Expected exception for invalid buffer size");
  }

  @Test
  public void testBufferNotDirect() {
    assertThrows(IllegalArgumentException.class, () -> new DataGrid(5, 6, DoubleBuffer.allocate(25)),
        "Expected exception for being no direct buffer");
  }
}
