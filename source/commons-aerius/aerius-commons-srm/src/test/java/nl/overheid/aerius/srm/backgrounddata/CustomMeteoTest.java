/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;

/**
 * Unit test class for Custom Meteo.
 */
class CustomMeteoTest {

  private static final AeriusSRMVersion STABLE = MockAeriusSRMVersion.mockAeriusSRMVersionStable();

  @Test
  void testSingleYear() throws AeriusException {
    final WindRoseSpeedGrid singleYearGrid2013 = new WindRoseSpeedGrid(0, 0, 0, 0, 0);
    final WindRoseSpeedGrid singleYearGrid2014 = new WindRoseSpeedGrid(0, 0, 0, 0, 0);

    final WindRoseConcentrationGrid singleConcentrationGrid2013 = new WindRoseConcentrationGrid(0, 0, 0, 0, 0);
    final WindRoseConcentrationGrid singleConcentrationGrid2014 = new WindRoseConcentrationGrid(0, 0, 0, 0, 0);

    final PreSRMData preSRMData = new PreSRMData();
    preSRMData.addWindRoseSpeedTable(STABLE, 2013, singleYearGrid2013);
    preSRMData.addWindRoseSpeedTable(STABLE, 2014, singleYearGrid2014);
    preSRMData.addWindRoseConcentrationGrid(STABLE, 2013, singleConcentrationGrid2013);
    preSRMData.addWindRoseConcentrationGrid(STABLE, 2014, singleConcentrationGrid2014);

    assertEquals(singleYearGrid2014, preSRMData.getWindRoseSpeedGrid(STABLE, new Meteo(2014)),
        "Single year meteo should return yearly WindRoseSpeedGrid");
    assertEquals(singleConcentrationGrid2014, preSRMData.getWindRoseConcentrationGrid(STABLE, new Meteo(2014), 2013),
        "Single year meteo should return yearly WindRoseConcentrationGrid, NOT the calculation year");
  }

  @Test
  void testMultipleYears() throws AeriusException {
    final WindRoseSpeedGrid multiYearGrid = new WindRoseSpeedGrid(0, 0, 0, 0, 0);

    final WindRoseConcentrationGrid singleConcentrationGrid2013Y = new WindRoseConcentrationGrid(0, 0, 0, 0, 0);
    final WindRoseConcentrationGrid singleConcentrationGrid2014Y = new WindRoseConcentrationGrid(0, 0, 0, 0, 0);
    final WindRoseConcentrationGrid singleConcentrationGrid2013P = new WindRoseConcentrationGrid(0, 0, 0, 0, 0);
    final WindRoseConcentrationGrid singleConcentrationGrid2014P = new WindRoseConcentrationGrid(0, 0, 0, 0, 0);

    final PreSRMData preSRMData = new PreSRMData();
    preSRMData.setWindRoseSpeedPrognoseGrid(STABLE, multiYearGrid);
    preSRMData.addWindRoseConcentrationGrid(STABLE, 2013, singleConcentrationGrid2013Y);
    preSRMData.addWindRoseConcentrationGrid(STABLE, 2014, singleConcentrationGrid2014Y);
    preSRMData.addWindRoseConcentrationPrognoseGrid(STABLE, 2013, singleConcentrationGrid2013P);
    preSRMData.addWindRoseConcentrationPrognoseGrid(STABLE, 2014, singleConcentrationGrid2014P);

    assertEquals(multiYearGrid, preSRMData.getWindRoseSpeedGrid(STABLE, new Meteo(1995, 2004)),
        "Multi year meteo should return prognose WindRoseSpeedGrid");
    assertEquals(singleConcentrationGrid2013P, preSRMData.getWindRoseConcentrationGrid(STABLE, new Meteo(1995, 2004), 2013),
        "Multi year meteo should return prognose WindRoseConcentrationGrid, with correct calculation year");
  }
}
