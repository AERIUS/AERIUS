/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;

/**
 * Test class for {@link WindSpeedDataBuilder}.
 */
class WindSpeedDataBuilderTest {

  private static final int TEST_YEAR = 2018;
  public static final AeriusSRMVersion AERIUS_SRM_VERSION = MockAeriusSRMVersion.mockAeriusSRMVersionStable();

  private static WindSpeedGrid grid;

  @BeforeAll
  static void beforeClass() throws IOException, AeriusException {
    final WindSpeedDataBuilder builder = new WindSpeedDataBuilder();
    final PreSRMData data = new PreSRMData();
    builder.loadWindSpeedByYear(AERIUS_SRM_VERSION, data,
        new File(WindSpeedDataBuilderTest.class.getResource(AERIUS_SRM_VERSION.getWindveldenVersion()).getFile()));
    grid = data.getWindSpeedGrid(AERIUS_SRM_VERSION, new Meteo(TEST_YEAR));
  }

  @Test
  void testRead() throws IOException, AeriusException {
    assertEquals(3.74, grid.get(168284, 449537).getWindTotalSpeed(), 0.0001, "Check windspeed value 1");
    assertEquals(5.01, grid.get(236300, 621200).getWindTotalSpeed(), 0.0001, "Check windspeed value 2");
  }

  @Test
  void testBelowGrid() {
    final WindSpeed wr = grid.get(100000, 298999);
    assertNotNull(wr, "Value is outside grid, and should return the no data object, but was: ");
    assertEquals(Double.NaN, wr.getWindTotalSpeed(), 0.0001, "Should have no data value");
  }

  @Test
  void testAboveGrid() {
    final WindSpeed wr = grid.get(100000, 641001);
    assertNotNull(wr, "Value is outside grid, and should return the no data object, but was: ");
    assertEquals(Double.NaN, wr.getWindTotalSpeed(), 0.0001, "Should have no data value");
  }
}
