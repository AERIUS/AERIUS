/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.shared.exception.InvalidInputException;
import nl.overheid.aerius.srm.SRMWorkerTestUtil;
import nl.overheid.aerius.srm.domain.SRMConfiguration;

/**
 * Test class for {@link PreSRMDataBuilder}.
 */
class PreSRMDataBuilderTest {

  @Test
  void testBuildOwN2000() throws InvalidInputException, IOException {
    final PreSRMDataBuilder builder = new PreSRMDataBuilder();
    final HttpClientProxy httpClientProxy = mock(HttpClientProxy.class);
    final SRMConfiguration config = SRMWorkerTestUtil.getTestSRMConfiguration(httpClientProxy);
    final PreSRMData data = builder.build(config);
    assertNotNull(data, "We have data");
  }

  @Test
  void testBuildCIMLK() throws InvalidInputException, IOException {
    final PreSRMDataBuilder builder = new PreSRMDataBuilder();
    final HttpClientProxy httpClientProxy = mock(HttpClientProxy.class);
    final SRMConfiguration config = SRMWorkerTestUtil.getTestSRMConfiguration(httpClientProxy);
    final PreSRMData data = builder.build(config);
    assertNotNull(data, "We have data");
  }

}
