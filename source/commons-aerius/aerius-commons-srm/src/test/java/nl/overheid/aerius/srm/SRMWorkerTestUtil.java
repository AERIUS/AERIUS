/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.srm.domain.SRMConfiguration;
import nl.overheid.aerius.srm.worker.SRMWorkerFactory;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.PropertiesUtil;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * Util class to read background data.
 */
public final class SRMWorkerTestUtil {

  private SRMWorkerTestUtil() {
    // util class
  }

  public static Worker<SRMInputData<EngineSource>, ?> createWorkerHandler() throws Exception {
    final SRMWorkerFactory factory = new SRMWorkerFactory();
    final SRMConfiguration config = getTestSRMConfiguration(factory);

    return factory.createWorkerHandler(config, mock(WorkerConnectionHelper.class));
  }

  public static SRMConfiguration getTestSRMConfiguration() throws IOException {
    final HttpClientProxy httpClientProxy = mock(HttpClientProxy.class);
    return SRMWorkerTestUtil.getTestSRMConfiguration(httpClientProxy);
  }

  public static SRMConfiguration getTestSRMConfiguration(final HttpClientProxy httpClientProxy) throws IOException {
    return getTestSRMConfiguration(new SRMWorkerFactory(httpClientProxy));
  }

  public static SRMConfiguration getTestSRMConfiguration(final SRMWorkerFactory factory) throws IOException {
    final Properties properties = PropertiesUtil.getFromTestPropertiesFile("srm");
    try {
      properties.setProperty("srm.model.data.url",
          new File(Path.of(factory.getClass().getResource(".").toURI()).getParent().toFile(), "backgrounddata").getAbsolutePath());
    } catch (final URISyntaxException e) {
      throw new IllegalStateException(e);
    }
    final ConfigurationBuilder<SRMConfiguration> builder = factory.configurationBuilder(properties);
    final SRMConfiguration config = builder.buildAndValidate().get();
    final List<String> validations = builder.getValidationErrors();

    assertEquals(List.of(), validations, "Validations should be empty: " + String.join(", ", validations));
    return config;
  }
}
