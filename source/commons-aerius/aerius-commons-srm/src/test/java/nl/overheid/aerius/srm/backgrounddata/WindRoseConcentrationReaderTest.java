/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.backgrounddata.PreSrmXYReader.YearPattern;

/**
 * Test class for {@link WindRoseConcentrationReader}.
 */
class WindRoseConcentrationReaderTest {

  private static final int DATA_VERSION = 2018;
  private static final String PRE_SRM_VERSION = MockAeriusSRMVersion.PRESRM_VERSION;
  private static final String CON_2018_TXT = MockAeriusSRMVersion.AERIUS_PRESRM_VERSION + "/con_2018_year.txt";
  private static final YearPattern YEAR_PATTERN = YearPattern.YEAR;
  private static final boolean READD_ALL_BACKGROUND = true;

  @Test
  void testReadFile() throws IOException {
    final WindRoseConcentrationReader reader = new WindRoseConcentrationReader(PRE_SRM_VERSION, YEAR_PATTERN, DATA_VERSION, READD_ALL_BACKGROUND);
    final LineReaderResult<WindRoseConcentration> results = readResults(reader, CON_2018_TXT);
    assertEquals(43960, results.getObjects().size(), "# rows");
    final int row = 0;
    final WindRoseConcentration wrc = results.getObjects().get(row);
    assertEquals(53.667, wrc.getO3(35), 0.0001, "O3 sector 35");
    assertEquals(6.260, wrc.getBackgroundNH3(), 0.0001, "NH3 background concentration");
    assertEquals(15.040, wrc.getBackground(Substance.NO2), 0.0001, "NO2 background concentration");
    assertEquals(16.940, wrc.getBackground(Substance.PM10), 0.0001, "PM10 background concentration");
    assertEquals(9.973, wrc.getBackground(Substance.PM25), 0.0001, "PM25 background concentration");
    assertEquals(47.670, wrc.getBackgroundO3(), 0.0001, "03 background concentration");
    assertEquals(13500.0, wrc.getX(), 0.0, "X");
    assertEquals(370500.0, wrc.getY(), 0.0, "Y");
  }

  @Test
  void testReadFileIncorrectPreSrmVersion() {
    final WindRoseConcentrationReader reader = new WindRoseConcentrationReader("0,999", YEAR_PATTERN, DATA_VERSION, READD_ALL_BACKGROUND);
    final IOException e = assertThrows(IOException.class, () -> readResults(reader, CON_2018_TXT));

    assertTrue(e.getMessage().contains("version"), "Expected exception for invalid presrm verion");
  }

  @Test
  void testReadFileIncorrectYear() throws IOException {
    final WindRoseConcentrationReader reader = new WindRoseConcentrationReader(PRE_SRM_VERSION, YEAR_PATTERN, 2000, READD_ALL_BACKGROUND);
    final IOException e = assertThrows(IOException.class, () -> readResults(reader, CON_2018_TXT));
    assertTrue(e.getMessage().contains("year"), "Expected exception for invalid presrm verion");
  }

  @Test
  void testReadFileInvalidYear() throws IOException {
    final WindRoseConcentrationReader reader = new WindRoseConcentrationReader(PRE_SRM_VERSION, YEAR_PATTERN, DATA_VERSION, READD_ALL_BACKGROUND);
    assertThrows(NumberFormatException.class, () -> readResults(reader, "con_2020_invalid_year.txt"),
        "Expected NumberFormatException for invalid year in data");
  }

  @Test
  void testReadFileInvalidHeader() throws IOException {
    final WindRoseConcentrationReader reader = new WindRoseConcentrationReader(PRE_SRM_VERSION, YEAR_PATTERN, DATA_VERSION, READD_ALL_BACKGROUND);
    assertThrows(IOException.class, () -> readResults(reader, "con_2020_invalid_header.txt"),
        "Expected exception for invalid header");
  }

  private LineReaderResult<WindRoseConcentration> readResults(final WindRoseConcentrationReader reader, final String filename) throws IOException {
    final LineReaderResult<WindRoseConcentration> results;
    try (final InputStream is = WindRoseSpeedReaderTest.class.getResourceAsStream(filename)) {
      results = reader.readObjects(is);
    }
    return results;
  }

}
