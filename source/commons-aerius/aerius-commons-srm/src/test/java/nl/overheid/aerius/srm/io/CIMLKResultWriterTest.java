/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMonitorSubstance;

/**
 * Test class for {@link CIMLKResultWriter}.
 */
class CIMLKResultWriterTest {

  @Test
  void testNormal() throws IOException {
    final CIMLKResultWriter resultWriter = new CIMLKResultWriter(4);

    final File outputFile = new File(Files.createTempDirectory("CIMLKResultWriterTest").toFile(), "extendedTestFile.csv");

    final List<AeriusResultPoint> results = new ArrayList<>();
    results.add(testCase(19));

    resultWriter.write(outputFile, results, 1600, "testing-version", "testing-database-version");

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_result.csv"));

    assertEquals(expected, result, "Result for extended CIMLK result export");
  }

  @Test
  void testWriteSeparateActions() throws IOException {
    final CIMLKResultWriter resultWriter = new CIMLKResultWriter(4);

    final File outputFile = new File(Files.createTempDirectory("CIMLKResultWriterTest").toFile(), "extendedTestFile.csv");

    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeHeader(writer);
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(23), 1600, "testing-version", "testing-database-version");
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(400), 1600, "testing-version", "testing-database-version");
    }

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_result_separate_writing.csv"));

    assertEquals(expected, result, "Result for separate writing CIMLK result export");
  }

  private static CIMLKResultPoint testCase(final int id) {
    final CIMLKResultPoint resultPoint = new CIMLKResultPoint(new AeriusPoint());
    resultPoint.setGmlId("PREFIX." + id);
    resultPoint.setX(id + 10.0);
    resultPoint.setY(id + 20.0);
    resultPoint.setWindFactor(32.235232);
    if (id % 2 == 1) {
      resultPoint.setLabel("Some label thing");
      resultPoint.setMonitorSubstance(CIMLKMonitorSubstance.ALL);
    }

    for (final EmissionResultKey key : EmissionResultKey.values()) {
      resultPoint.setEmissionResult(key, id + key.getSubstance().getId() * 0.1234567890123456);
      resultPoint.getBackgroundConcentrations().add(key, id + (key.getSubstance().getId() - 1) * 0.1234567890123456);
      resultPoint.getGcnConcentrations().add(key, id + (key.getSubstance().getId() - 2) * 0.1234567890123456);
      resultPoint.getCHwnConcentrations().add(key, id + (key.getSubstance().getId() - 3) * 0.1234567890123456);
      resultPoint.getSrm1Results().add(key, id + (key.getSubstance().getId() - 4) * 0.1234567890123456);
      resultPoint.getSrm2Results().add(key, id + (key.getSubstance().getId() - 5) * 0.1234567890123456);
      resultPoint.getUserCorrections().add(key, id + (key.getSubstance().getId() - 6) * 0.1234567890123456);
    }

    return resultPoint;
  }

}
