/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;

/**
 * Test class for {@link CIMLKCorrectionWriter}.
 */
class CIMLKCorrectionWriterTest {

  @Test
  void testNormal() throws IOException {
    final CIMLKCorrectionWriter resultWriter = new CIMLKCorrectionWriter(4);

    final File outputFile = new File(Files.createTempDirectory("CIMLKCorrectionWriterTest").toFile(), "normalTestFile.csv");

    final List<CIMLKCorrection> corrections = new ArrayList<>();
    corrections.add(testCase(27));

    resultWriter.write(outputFile, corrections);

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_corrections.csv"));

    assertEquals(expected, result, "Result for normal CIMLK points export");
  }

  @Test
  void testWriteSeparateActions() throws IOException {
    final CIMLKCorrectionWriter resultWriter = new CIMLKCorrectionWriter(4);

    final File outputFile = new File(Files.createTempDirectory("CIMLKCorrectionWriterTest").toFile(), "extendedTestFile.csv");

    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeHeader(writer);
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(89));
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(700));
    }

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_corrections_separate_writing.csv"));

    assertEquals(expected, result, "Result for separate writing CIMLK points export");
  }

  private static CIMLKCorrection testCase(final int id) {
    final CIMLKCorrection correction = new CIMLKCorrection();
    correction.setCalculationPointGmlId("PREFIX." + id);
    correction.setLabel("Correctie nummer " + id);
    correction.setDescription("Omschrijving voor deze correctie");
    correction.setResultType(EmissionResultType.CONCENTRATION);
    correction.setSubstance(Substance.NO2);
    correction.setValue(id * 2.123456789);

    if (id % 2 == 1) {
      correction.setJurisdictionId(4);
    }

    return correction;
  }

}
