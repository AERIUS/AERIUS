/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

/**
 * Test class for {@link RangeUtil}.
 */
class RangeUtilTest {
  private final RangeUtil rangeUtil = new RangeUtil(5000);

  @ParameterizedTest
  @MethodSource("data")
  void testInRange(final String description, final double sourceStartX, final double sourceStartY, final double sourceEndX, final double sourceEndY,
      final double receptorX, final double receptorY, final boolean expected) {
    final SRM2RoadSegment source = new SRM2RoadSegment();
    source.setStartX(sourceStartX);
    source.setStartY(sourceStartY);
    source.setEndX(sourceEndX);
    source.setEndY(sourceEndY);
    final AeriusResultPoint receptor = new AeriusResultPoint(0, 0, AeriusPointType.RECEPTOR, receptorX, receptorY);
    assertEquals(expected, rangeUtil.inRange(source, receptor), description);
  }

  static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
      {"Receptor near source start.", 0, 0, 10000, 0, -1000, -1000, true},
      {"Receptor near source end.", 0, 0, 10000, 0, 11000, 1000, true},
      {"Receptor just above line between source start and end.", 0, 0, 10000, 0, 3000, 1000, true},
      {"Receptor just below line between source start and end.", 0, 0, 10000, 0, 4000, -1500, true},
      {"Receptor left of source start 3km.", 0, 0, 10000, 0, -1000, -3000, true},
      {"Receptor left of source start 5km.", 0, 0, 10000, 0, -1000, -5000, false},
      {"Receptor right of source end 3km.", 0, 0, 10000, 0, 13001, 0, true},
      {"Receptor right of source end 5km.", 0, 0, 10000, 0, 15001, 0, false},
      {"Receptor above line between source start and end 3km.", 0, 0, 10000, 0, 3000, 3001, true},
      {"Receptor above line between source start and end 3km.", 0, 0, 10000, 0, 3000, 5001, false},
      {"Receptor below line between source start and end 5km.", 0, 0, 10000, 0, 7000, -3001, true},
      {"Receptor below line between source start and end 5km.", 0, 0, 10000, 0, 7000, -5001, false}
    });
  }
}
