/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata.bgconcentrations;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentrationBuilder;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentrationBuilderTest;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;

/**
 * Unit test for OwN2000 Background refinement test of {@link WindRoseConcentration}.
 */
class OwN2000BackgroundRefinementTest {

  private static final int TEST_YEAR = 2018;
  private static final WindRoseConcentrationBuilder BUILDER = new WindRoseConcentrationBuilder();

  private static HasGridValue<WindRoseConcentration> grid;

  @BeforeAll
  static void beforeClass() throws IOException, AeriusException {
    final PreSRMData data = new PreSRMData();
    final AeriusSRMVersion aeriusVersion = MockAeriusSRMVersion.mockAeriusSRMVersionStable();
    final File directory = new File(WindRoseConcentrationBuilderTest.class.getResource(aeriusVersion.getAeriusPreSrmVersion()).getFile());

    BUILDER.loadWindRoseConcentration(aeriusVersion, data, directory, SRMConstants.OWN2000_SRM2_CALCULATION_OPTIONS_NO_SUBRECEPTORS);
    grid = data.getWindRoseConcentrationGrid(aeriusVersion, new Meteo(TEST_YEAR), TEST_YEAR);
  }

  @Test
  void testBackground() {
    final WindRoseConcentration wrc1 = grid.get(111200, 481000);
    assertEquals(32.18, wrc1.getBackground(Substance.NO2), 0.0001, "NO2 schiphol background should match corrected value");
    final WindRoseConcentration wrc2 = grid.get(111450, 481000);
    assertEquals(31.28, wrc2.getBackground(Substance.NO2), 0.0001, "NO2 schiphol background should match corrected value");
  }

}
