/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadManager;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;

/**
 * Test class for {@link CIMLKSRM1RoadWriter}.
 */
class CIMLKSRM1RoadWriterTest {

  @Test
  void testWrite() throws IOException {
    final CIMLKSRM1RoadWriter writerToTest = new CIMLKSRM1RoadWriter(4);

    final File outputFile = new File(Files.createTempDirectory("CIMLKCorrectionWriterTest").toFile(), "normalTestFile.csv");

    final List<EmissionSourceFeature> results = new ArrayList<>();
    results.add(testCase(19));

    writerToTest.write(outputFile, results);

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_srm1_roads.csv"));

    assertEquals(expected, result, "Result for normal CIMLK result export");
  }

  @Test
  void testWriteSeparateActions() throws IOException {
    final CIMLKSRM1RoadWriter writerToTest = new CIMLKSRM1RoadWriter(4);

    final File outputFile = new File(Files.createTempDirectory("CIMLKCorrectionWriterTest").toFile(), "separateActionsTestFile.csv");

    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      writerToTest.writeHeader(writer);
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      writerToTest.writeRow(writer, testCase(23));
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      writerToTest.writeRow(writer, testCase(400));
    }

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_srm1_roads_separate_writing.csv"));

    assertEquals(expected, result, "Result for separate writing CIMLK result export");
  }

  private static EmissionSourceFeature testCase(final int id) {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setId(String.valueOf(id));
    final LineString lineString = new LineString();
    lineString.setCoordinates(new double[][] {{id + 20.0, id + 10.0}, {id + 20.0, id + 30.0}});
    feature.setGeometry(lineString);

    final SRM1RoadEmissionSource source = new SRM1RoadEmissionSource();
    feature.setProperties(source);

    source.setGmlId("PREFIX." + id);
    source.setLabel("Label for source " + id);
    source.setDescription("Description for source " + id);
    source.setRoadTypeCode(RoadSpeedType.URBAN_TRAFFIC_NORMAL.getRoadTypeCode());
    if (id % 2 == 1) {
      source.setDescription(source.getDescription() + "; annoying semicolon in description.");
      source.setJurisdictionId(235);
      source.setRoadManager(RoadManager.PRIVATE);
    }

    final StandardVehicles vehicles = new StandardVehicles();
    vehicles.setStrictEnforcement(false);
    vehicles.setMaximumSpeed(99);
    vehicles.setTimeUnit(TimeUnit.DAY);
    for (final VehicleType vehicleType : VehicleType.values()) {
      final ValuesPerVehicleType valuesPerVehicleType = new ValuesPerVehicleType();
      valuesPerVehicleType.setVehiclesPerTimeUnit(2342.23 * vehicleType.ordinal());
      valuesPerVehicleType.setStagnationFraction(0.1 * vehicleType.ordinal());
      vehicles.getValuesPerVehicleTypes().put(vehicleType.getStandardVehicleCode(), valuesPerVehicleType);
    }
    source.getSubSources().add(vehicles);

    return feature;
  }

}
