/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.theme.cimlk.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;

/**
 * Test class for {@link BackgroundDataAdder} class.
 */
class BackgroundDataAdderTest {

  @Test
  void testAdd() {
    final List<AeriusResultPoint> results = new ArrayList<>();
    final CIMLKResultPoint point = new CIMLKResultPoint(new CIMLKCalculationPoint(1,  "some unique ID", 10, 10));
    point.getSrm1Results().add(EmissionResultKey.PM10_CONCENTRATION, 188);
    point.getSrm2Results().add(EmissionResultKey.PM10_CONCENTRATION, 112);
    point.getEmissionResults().add(EmissionResultKey.PM10_CONCENTRATION, 88);
    point.getEmissionResults().add(EmissionResultKey.NO2_CONCENTRATION, 77);
    point.getSrm2Results().add(EmissionResultKey.EC_CONCENTRATION, Double.NaN);
    results.add(point);
    final Set<EmissionResultKey> erks =
        Stream.of(EmissionResultKey.NO2_CONCENTRATION, EmissionResultKey.PM10_CONCENTRATION, EmissionResultKey.EC_CONCENTRATION)
        .collect(Collectors.toSet());
    final WindRoseConcentration wrc = new WindRoseConcentration(10, 10);
    wrc.setGCN(Substance.PM10, 22);
    wrc.setCHwn(Substance.PM10, -4);
    wrc.setGCN(Substance.NO2, 300);
    wrc.setCHwn(Substance.NO2, -2);
    wrc.setGCN(Substance.O3, 33);
    wrc.setCHwn(Substance.O3, -6);
    wrc.setGCN(Substance.EC, 44);
    final HasGridValue<WindRoseConcentration> windRoseTable = (x, y) -> wrc;

    BackgroundDataAdder.add(results, erks, windRoseTable);
    final CIMLKResultPoint result = results.stream().map(CIMLKResultPoint.class::cast).collect(Collectors.toList()).get(0);
    assertEquals(22, (int) result.getGcnConcentrations().get(EmissionResultKey.PM10_CONCENTRATION), "Incorrect gcn pm10");
    assertEquals(33, (int) result.getGcnConcentrations().get(EmissionResultKey.O3_CONCENTRATION), "Incorrect gcn o3");
    assertEquals(-4, (int) result.getCHwnConcentrations().get(EmissionResultKey.PM10_CONCENTRATION), "Incorrect c_hwn pm10");
    assertEquals(-6, (int) result.getCHwnConcentrations().get(EmissionResultKey.O3_CONCENTRATION), "Incorrect c_hwn o3");
    assertEquals(318, (int) result.getEmissionResult(EmissionResultKey.PM10_CONCENTRATION), "Incorrect pm10");
    assertEquals(375, (int) result.getEmissionResult(EmissionResultKey.NO2_CONCENTRATION), "Incorrect no2");
    assertEquals(375, (int) result.getEmissionResult(EmissionResultKey.NO2_CONCENTRATION), "Incorrect no2");
    assertEquals(44, (int) result.getEmissionResult(EmissionResultKey.EC_CONCENTRATION), "Incorrect ec");
  }

}
