/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLine;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRoadProfile;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKTreeProfile;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link CIMLKDispersionLineFileReader}.
 */
class CIMLKDispersionLineFileReaderTest {

  private static final String SPECIFIC_TEST_FILE = "dispersion_line_example.csv";

  @Test
  void testSpecificCase() throws Exception {
    final CIMLKDispersionLineFileReader reader = new CIMLKDispersionLineFileReader();
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {

      final String header = bufferedReader.readLine();
      final AeriusException parsingHeaderException = reader.parseHeader(header);

      assertNull(parsingHeaderException, "Header should be correctly parsed");

      final LineReaderResult<CIMLKDispersionLineFeature> result = reader.readObjects(bufferedReader);

      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
      final List<CIMLKDispersionLineFeature> lines = result.getObjects();
      validateResults(lines);
    }
  }

  @Test
  void testSpecificCaseWithMainImporter() throws Exception {
    final LegacyNSLImportReader reader = new LegacyNSLImportReader();
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {

      final SectorCategories categories = Mockito.mock(SectorCategories.class);
      final Substance substance = Substance.NOX;
      final ImportParcel importResult = new ImportParcel();

      reader.read("some name", inputStream, categories, substance, importResult);

      if (!importResult.getExceptions().isEmpty()) {
        throw importResult.getExceptions().get(0);
      }
      final List<CIMLKDispersionLineFeature> lines = importResult.getSituation().getCimlkDispersionLinesList();
      validateResults(lines);
    }
  }

  private void validateResults(final List<CIMLKDispersionLineFeature> lines) throws AeriusException {
    assertEquals(1, lines.size(), "Count nr. of points");
    final CIMLKDispersionLineFeature feature = lines.get(0);
    final CIMLKDispersionLine line = feature.getProperties();
    assertEquals("PREFIX.9323", line.getRoadGmlId(), "Road GML id");
    assertEquals("PREFIX.32", line.getCalculationPointGmlId(), "CalculationPoint id");
    assertEquals(Integer.valueOf(42), line.getJurisdictionId(), "Jurisdiction id");
    assertEquals("Mijn mooie label", line.getLabel(), "Label");
    assertEquals(CIMLKRoadProfile.WIDE_STREET_CANYON, line.getRoadProfile(), "Road profile");
    assertEquals(CIMLKTreeProfile.NONE_OR_FEW, line.getTreeProfile(), "Tree profile");
    assertEquals("Mijn mooie opmerking", line.getDescription(), "Description");
  }

}
