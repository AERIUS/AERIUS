/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMonitorSubstance;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRejectionGrounds;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CIMLKCalculationPoint;

/**
 * Test class for {@link CIMLKCalculationPointWriter}.
 */
class CIMLKCalculationPointWriterTest {

  @Test
  void testNormal() throws IOException {
    final CIMLKCalculationPointWriter resultWriter = new CIMLKCalculationPointWriter();

    final File outputFile = new File(Files.createTempDirectory("CIMLKCalculationPointWriterTest").toFile(), "normalTestFile.csv");

    final List<CalculationPointFeature> results = new ArrayList<>();
    results.add(testCase(95));

    resultWriter.write(outputFile, results);

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_points.csv"));

    assertEquals(expected, result, "Result for normal CIMLK points export");
  }

  @Test
  void testWriteSeparateActions() throws IOException {
    final CIMLKCalculationPointWriter resultWriter = new CIMLKCalculationPointWriter();

    final File outputFile = new File(Files.createTempDirectory("CIMLKCalculationPointWriterTest").toFile(), "extendedTestFile.csv");

    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeHeader(writer);
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(53));
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(200));
    }

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_points_separate_writing.csv"));

    assertEquals(expected, result, "Result for separate writing CIMLK points export");
  }

  private static CalculationPointFeature testCase(final int id) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setGeometry(new Point(id + 10.0, id + 20.0));
    final CIMLKCalculationPoint point = new CIMLKCalculationPoint();
    feature.setProperties(point);
    point.setGmlId("PREFIX." + id);
    point.setLabel("Some label");
    point.setDescription("Some point");

    if (id % 2 == 1) {
      point.setJurisdictionId(4);
      point.setMonitorSubstance(CIMLKMonitorSubstance.ALL);
      point.setRejectionGrounds(CIMLKRejectionGrounds.EXPOSURE_CRITERION);
    }

    return feature;
  }

}
