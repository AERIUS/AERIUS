/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2LinearReference;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.Vehicles;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.srm.io.LegacyNSLImportReader;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link EmissionSourceSRMConverter}.
 */
class EmissionSourceSRM2ConverterTest {

  private static final String SRM2_ROADS_EXAMPLE_CSV = "../../srm/io/legacy_srm2_roads_example.csv";
  private static final String SRM2_ROADS_EXAMPLE_WARNINGS_CSV = "srm2_roads_example_warnings.csv";

  private static final Set<String> EXPECTED_SEGMENTS_WITHOUT_INTENSITY = Stream
      .of("ES.1312707", "ES.1312716")
      .collect(Collectors.toSet());

  private final List<Substance> NOX = List.of(Substance.NOX);
  private final EmissionSourceSRMConverter converter = new EmissionSourceSRMConverter();
  private final LegacyNSLImportReader reader = new LegacyNSLImportReader();

  @Test
  void testConvert1() throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final LineString lineString = new LineString();
    lineString.setCoordinates(new double[][] {
        {135363, 455540},
        {135414, 455621},
        {135612, 455738},
        {135904, 455231},
        {135501, 454643},
        {135209, 455083},
        {135118, 455144}});
    feature.setGeometry(lineString);
    final SRM2RoadEmissionSource ev = new SRM2RoadEmissionSource();
    feature.setProperties(ev);
    final double roadLength = 2261.25623097497;
    addVehicle(ev, 200 * 3.0);

    final List<EngineSource> segments = converter.convert(feature, NOX);
    assertEquals(6, segments.size(), "#segments check");
    final double referenceEmission = BigDecimal.valueOf(200 * 3).divide(
        BigDecimal.valueOf(Substance.EMISSION_IN_G_PER_S_FACTOR).multiply(BigDecimal.valueOf(roadLength)), MathContext.DECIMAL64).doubleValue();
    assertEquals(referenceEmission, ((EngineEmissionSource) segments.get(0)).getEmission(Substance.NOX), 0.1E-7, "Emission");
  }

  @Test
  void testConvert2() throws AeriusException {
    assertConversionOfSimpleRoad(new double[][] {
        {135363, 455540},
        {135414, 455621}});
  }

  @Test
  void testConvertVerySmallRoadSegment() throws AeriusException {
    assertConversionOfSimpleRoad(new double[][] {
        {135414, 455621},
        {135414, 455621.001}});
  }

  private void assertConversionOfSimpleRoad(final double[][] coordinates) throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final LineString lineString = new LineString();
    lineString.setCoordinates(coordinates);
    feature.setGeometry(lineString);
    final SRM2RoadEmissionSource ev = new SRM2RoadEmissionSource();
    feature.setProperties(ev);
    addVehicle(ev, 234.0);

    final List<EngineSource> segments = converter.convert(feature, NOX);
    assertEquals(1, segments.size(), "#segments check");
  }

  @Test
  void testConvert3() throws IOException, AeriusException {
    final File file = new File(getClass().getResource(SRM2_ROADS_EXAMPLE_CSV).getFile());

    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportParcel results = new ImportParcel();
      reader.read(SRM2_ROADS_EXAMPLE_CSV, inputStream, new TestDomain().getCategories(), null, results);

      assertEquals(2, results.getWarnings().size(), "Warning list should only contain deprecation and ID warning");
      final List<EngineSource> roadSegments = converter.convert(results.getSituation().getEmissionSourcesList(), NOX);

      for (final EngineSource segment : roadSegments) {
        assertTrue(segment instanceof SRM2RoadSegment, "Segment instance of SRM2RoadSegment " + segment);
        if (!EXPECTED_SEGMENTS_WITHOUT_INTENSITY.contains(((SRM2RoadSegment) segment).getSegmentId())) {
          assertNotEquals(0.0, ((EngineEmissionSource) segment).getEmission(Substance.NOX), "Segment " + segment);
        }
      }
    }
  }

  @Test
  void testConvertGenericEmissionSource() throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final LineString lineString = new LineString();
    lineString.setCoordinates(new double[][] {
        {135363, 455540},
        {135414, 455621},
        {135612, 455738},
        {135904, 455231},
        {135501, 454643},
        {135209, 455083},
        {135118, 455144}});
    feature.setGeometry(lineString);
    final GenericEmissionSource ev = new GenericEmissionSource();
    feature.setProperties(ev);
    ev.getEmissions().put(Substance.NOX, 600.0 * 365 / (1000 * 1000));

    final List<EngineSource> segments = converter.convert(feature, NOX);
    assertEquals(6, segments.size(), "#segments check");
    assertEquals((200 * 3.0 / (24 * 60 * 60 * 1000)) / 2261, ((EngineEmissionSource) segments.get(0)).getEmission(Substance.NOX), 0.1E-7, "Emission");
  }

  @Test
  void testConvertWarings() throws IOException, AeriusException {
    final File file = new File(getClass().getResource(SRM2_ROADS_EXAMPLE_WARNINGS_CSV).getFile());

    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportParcel results = new ImportParcel();
      reader.read(SRM2_ROADS_EXAMPLE_CSV, inputStream, new TestDomain().getCategories(), null, results);

      final List<EngineSource> roadSegments = converter.convert(results.getSituation().getEmissionSourcesList(), NOX);
      assertFalse(results.getWarnings().isEmpty(), "Warning list should not be empty");
      assertEquals(2, results.getWarnings().size(), "Warning list length");

      for (final AeriusException warning : results.getWarnings()) {
        if (warning.getReason() != AeriusExceptionReason.NSL_LEGACY_FILESUPPORT_WILL_BE_REMOVED
            && warning.getReason() != AeriusExceptionReason.CSV_ID_ADJUSTED) {
          fail("Did not expect warning, but got warning with reason " + warning.getReason());
        }
      }

      assertFalse(results.getExceptions().isEmpty(), "Exception list should not be empty");
      assertEquals(1, results.getExceptions().size(), "Exception list length");
      for (final AeriusException exception : results.getExceptions()) {
        if (exception.getReason() != ImaerExceptionReason.SRM2_SOURCE_TUNNEL_FACTOR_ZERO) {
          fail("Did not expect exception, but got exception with reason " + exception.getReason());
        }
      }

      for (final EngineSource segment : roadSegments) {
        assertEquals(Double.doubleToLongBits(0.0),
            Double.doubleToLongBits(((EngineEmissionSource) segment).getEmission(Substance.NOX)), "Segment " + segment);
      }
    }
  }

  @Test
  void testInvalidGeometry() throws AeriusException {
    final SRM2RoadEmissionSource ev = new SRM2RoadEmissionSource();
    final Point geometry = new Point(135363, 455540);
    addVehicle(ev, 293.0);

    final AeriusException e = assertThrows(AeriusException.class, () -> converter.convert(geometry, ev, NOX),
        "Expected AeriusException on invalid geometry");

    assertEquals(AeriusExceptionReason.ROAD_GEOMETRY_NOT_ALLOWED, e.getReason(), "Unexpected reason.");
  }

  @Test
  void testToSRM2Input() throws AeriusException {
    final List<Substance> substances = new ArrayList<>();
    substances.add(Substance.NOX);
    substances.add(Substance.PM10);
    final EnumSet<EmissionResultKey> emissionResultKeys = EnumSet.noneOf(EmissionResultKey.class);
    emissionResultKeys.add(EmissionResultKey.NOX_DEPOSITION);
    emissionResultKeys.add(EmissionResultKey.PM10_CONCENTRATION);

    final ArrayList<EmissionSourceFeature> emissionSources = createSources();
    final List<AeriusPoint> receptors = new ArrayList<>();
    receptors.add(new AeriusPoint(100, 0, AeriusPointType.POINT, 200, 1));
    receptors.add(new AeriusPoint(300, 0, AeriusPointType.POINT, 400, 2));

    final SRMInputData<EngineSource> data = new SRMInputData<EngineSource>(Theme.OWN2000, AeriusSRMVersion.STABLE);
    data.setSubstances(substances);
    data.setEmissionResultKeys(emissionResultKeys);
    data.setEmissionSources(1, converter.convert(emissionSources, substances));
    data.setReceptors(receptors);

    assertEquals(2, ((EngineEmissionSource) data.getEmissionSources().get(1).iterator().next()).getEmissions().size(), "#keys");
    assertEquals(2, data.getEmissionSources().get(1).size(), "#segments");
    assertEquals(2, data.getReceptors().size(), "#receptors");
  }

  @Test
  void testToSRM2RoadSegments() throws AeriusException {
    final List<Substance> substances = new ArrayList<>();
    substances.add(Substance.NOX);
    substances.add(Substance.PM10);
    final List<EmissionSourceFeature> emissionSources = createSources();
    final List<EngineSource> segments = converter.convert(emissionSources, substances);

    assertEquals(2, ((EngineEmissionSource) segments.get(0)).getEmissions().size(), "#keys");
    assertEquals(2, segments.size(), "#segments");
  }

  @Test
  void testSRM2DynamicSegments() throws AeriusException {
    final ArrayList<EmissionSourceFeature> emissionSources = new ArrayList<>();
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final LineString geometry = new LineString();
    geometry.setCoordinates(new double[][] {
        {1000, 1200},
        {1100, 1100},
        {2000, 2000}});
    feature.setGeometry(geometry);
    final SRM2RoadEmissionSource es = new SRM2RoadEmissionSource();
    es.setRoadTypeCode("FREEWAY");
    addVehicle(es, 234.0);

    final ArrayList<SRM2LinearReference> dynamicSegments = new ArrayList<>();

    final SRM2LinearReference heightSegment = new SRM2LinearReference();
    heightSegment.setFromPosition(0);
    heightSegment.setToPosition(0.25);
    heightSegment.setElevationHeight(50);
    dynamicSegments.add(heightSegment);

    final SRM2LinearReference leftBarrierSegment = new SRM2LinearReference();
    leftBarrierSegment.setFromPosition(0.25);
    leftBarrierSegment.setToPosition(0.75);
    final SRM2RoadSideBarrier leftBarrier = new SRM2RoadSideBarrier();
    leftBarrier.setBarrierType(SRM2RoadSideBarrierType.SCREEN);
    leftBarrier.setHeight(3);
    leftBarrier.setDistance(2);
    leftBarrierSegment.setBarrierLeft(leftBarrier);
    dynamicSegments.add(leftBarrierSegment);

    final SRM2LinearReference rightBarrierSegment = new SRM2LinearReference();
    rightBarrierSegment.setFromPosition(0.5);
    rightBarrierSegment.setToPosition(1);
    final SRM2RoadSideBarrier rightBarrier = new SRM2RoadSideBarrier();
    rightBarrier.setBarrierType(SRM2RoadSideBarrierType.WALL);
    rightBarrier.setHeight(4);
    rightBarrier.setDistance(3);
    rightBarrierSegment.setBarrierRight(rightBarrier);
    dynamicSegments.add(rightBarrierSegment);

    es.setPartialChanges(dynamicSegments);
    feature.setProperties(es);
    emissionSources.add(feature);

    final List<EngineSource> segments = converter.convert(emissionSources, NOX);

    assertEquals(5, segments.size(), "Invalid number of segments.");
    for (final EngineSource engineSource : segments) {
      assertTrue(engineSource instanceof SRM2RoadSegment, "Engine source should be a SRM2 road segment");
    }

    final SRM2RoadSegment segment0 = (SRM2RoadSegment) segments.get(0);
    assertEquals(1000, segment0.getStartX(), 0.001, "Invalid X coordinate segment 0 start.");
    assertEquals(1200, segment0.getStartY(), 0.001, "Invalid Y coordinate segment 0 start.");
    assertEquals(1100, segment0.getEndX(), 0.001, "Invalid X coordinate segment 0 end.");
    assertEquals(1100, segment0.getEndY(), 0.001, "Invalid Y coordinate segment 0 end.");
    assertEquals(12, segment0.getElevationHeight(), "Invalid elevation height segment 0.");
    assertEquals(3.0, segment0.getSigma0(), 0.001, "Invalid sigma0 segment 0.");

    final SRM2RoadSegment segment1 = (SRM2RoadSegment) segments.get(1);
    assertEquals(1100, segment1.getStartX(), 0.001, "Invalid X coordinate segment 1 start.");
    assertEquals(1100, segment1.getStartY(), 0.001, "Invalid Y coordinate segment 1 start.");
    assertEquals(1250, segment1.getEndX(), 0.001, "Invalid X coordinate segment 1 end.");
    assertEquals(1250, segment1.getEndY(), 0.001, "Invalid Y coordinate segment 1 end.");
    assertEquals(12, segment1.getElevationHeight(), "Invalid elevation height segment 1.");
    assertEquals(3.0, segment1.getSigma0(), 0.001, "Invalid sigma0 segment 1.");

    final SRM2RoadSegment segment2 = (SRM2RoadSegment) segments.get(2);
    assertEquals(1250, segment2.getStartX(), 0.001, "Invalid X coordinate segment 2 start.");
    assertEquals(1250, segment2.getStartY(), 0.001, "Invalid Y coordinate segment 2 start.");
    assertEquals(1500, segment2.getEndX(), 0.001, "Invalid X coordinate segment 2 end.");
    assertEquals(1500, segment2.getEndY(), 0.001, "Invalid Y coordinate segment 2 end.");
    assertEquals(0, segment2.getElevationHeight(), "Invalid elevation height segment 2.");
    assertEquals(4.5, segment2.getSigma0(), 0.001, "Invalid sigma0 segment 2.");

    final SRM2RoadSegment segment3 = (SRM2RoadSegment) segments.get(3);
    assertEquals(1500, segment3.getStartX(), 0.001, "Invalid X coordinate segment 3 start.");
    assertEquals(1500, segment3.getStartY(), 0.001, "Invalid Y coordinate segment 3 start.");
    assertEquals(1750, segment3.getEndX(), 0.001, "Invalid X coordinate segment 3 end.");
    assertEquals(1750, segment3.getEndY(), 0.001, "Invalid Y coordinate segment 3 end.");
    assertEquals(0, segment3.getElevationHeight(), "Invalid elevation height segment 3.");
    assertEquals(5.5, segment3.getSigma0(), 0.001, "Invalid sigma0 segment 3.");

    final SRM2RoadSegment segment4 = (SRM2RoadSegment) segments.get(4);
    assertEquals(1750, segment4.getStartX(), 0.001, "Invalid X coordinate segment 4 start.");
    assertEquals(1750, segment4.getStartY(), 0.001, "Invalid Y coordinate segment 4 start.");
    assertEquals(2000, segment4.getEndX(), 0.001, "Invalid X coordinate segment 4 end.");
    assertEquals(2000, segment4.getEndY(), 0.001, "Invalid Y coordinate segment 4 end.");
    assertEquals(0, segment4.getElevationHeight(), "Invalid elevation height segment 4.");
    assertEquals(4.0, segment4.getSigma0(), 0.001, "Invalid sigma0 segment 4.");
  }

  private ArrayList<EmissionSourceFeature> createSources() {
    final ArrayList<EmissionSourceFeature> emissionSources = new ArrayList<>();
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final LineString lineString = new LineString();
    lineString.setCoordinates(new double[][] {{100, 200}, {300, 400}, {500, 600}});
    feature.setGeometry(lineString);
    final SRM2RoadEmissionSource es = new SRM2RoadEmissionSource();
    final List<Vehicles> traffic = es.getSubSources();
    final StandardVehicles tr = new StandardVehicles();
    tr.setStrictEnforcement(false);
    tr.setMaximumSpeed(120);
    final ValuesPerVehicleType valuesPerVehicleType = new ValuesPerVehicleType();
    valuesPerVehicleType.setVehiclesPerTimeUnit(200);
    valuesPerVehicleType.setStagnationFraction(1.0);
    tr.getValuesPerVehicleTypes().put("LIGHT_TRAFFIC", valuesPerVehicleType);
    tr.setTimeUnit(TimeUnit.DAY);
    traffic.add(tr);
    es.setEmissions(Map.of(Substance.NOX, 200.0, Substance.PM10, 230.0));
    feature.setProperties(es);
    emissionSources.add(feature);
    return emissionSources;
  }

  private void addVehicle(final SRM2RoadEmissionSource ev, final double emission) {
    final List<Vehicles> traffic = ev.getSubSources();
    final StandardVehicles lt = new StandardVehicles();

    final ValuesPerVehicleType valuesPerVehicleType = new ValuesPerVehicleType();
    valuesPerVehicleType.setVehiclesPerTimeUnit(200);
    lt.getValuesPerVehicleTypes().put("LIGHT_TRAFFIC", valuesPerVehicleType);

    lt.setTimeUnit(TimeUnit.DAY);
    lt.setMaximumSpeed(100);
    lt.setStrictEnforcement(false);
    traffic.add(lt);
    ev.getEmissions().merge(Substance.NOX, emission, (a, b) -> a + b);
  }
}
