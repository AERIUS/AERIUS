/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link LegacyNSLReceptorFileReader}.
 */
class LegacyNSLReceptorFileReaderTest {

  private static final String TEST_FILENAME = "legacy_nsl_receptor.csv";

  private final LegacyNSLImportReader reader = new LegacyNSLImportReader();

  @Test
  void testReadFile() throws FileNotFoundException, IOException, AeriusException {
    final File file = new File(getClass().getResource(TEST_FILENAME).getFile());
    final ImportParcel results = new ImportParcel();

    try (InputStream inputStream = new FileInputStream(file)) {
      reader.read(TEST_FILENAME, inputStream, null, null, results);
    }
    assertEquals(3, results.getCalculationPointsList().size(), "Incorrect nr of receptor points");
    assertEquals(3, results.getSituation().getCimlkDispersionLinesList().size(), "Incorrect nr of overdrachtslijnen");
    assertEquals(3, results.getCalculationPointsList().stream()
        .map(CalculationPointFeature::getProperties)
        .filter(CIMLKCalculationPoint.class::isInstance)
        .count(),
        "Incorrect nr of overdrachtslijnen of type CIMLKCalculationPoint");
  }
}
