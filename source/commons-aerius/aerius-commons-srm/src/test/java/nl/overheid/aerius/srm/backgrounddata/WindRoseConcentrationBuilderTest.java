/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;

/**
 * Test class for {@link WindRoseConcentrationBuilder}
 */
public class WindRoseConcentrationBuilderTest {

  private static final int TEST_YEAR = 2018;
  private static final WindRoseConcentrationBuilder BUILDER = new WindRoseConcentrationBuilder();

  private static HasGridValue<WindRoseConcentration> grid;

  @BeforeAll
  static void beforeClass() throws IOException, AeriusException {
    final PreSRMData data = new PreSRMData();
    final AeriusSRMVersion stable = MockAeriusSRMVersion.mockAeriusSRMVersionCIMLK();
    final File directory = new File(WindRoseConcentrationBuilderTest.class.getResource(stable.getAeriusPreSrmVersion()).getFile());

    BUILDER.loadWindRoseConcentration(stable, data, directory, SRMConstants.CIMLK_SRM2_CALCULATION_OPTIONS);
    grid = data.getWindRoseConcentrationGrid(stable, new Meteo(TEST_YEAR), TEST_YEAR);
  }

  @Test
  void testReading() throws IOException, AeriusException {
    assertNotNull(grid.get(191500, 306300), "Reading boundery should return value");
    assertEquals(47.435, grid.get(116999, 454999).getBackgroundO3(), 0.001, "O3 @ 116999, 454999");
    assertEquals(47.302, grid.get(116000, 454000).getBackgroundO3(), 0.001, "O3 @ 116000, 454000");
    assertEquals(47.435, grid.get(116560, 454807).getBackgroundO3(), 0.001, "O3 @ 116560, 454807");
    assertEquals(45.174, grid.get(121617, 454546).getBackgroundO3(), 0.001, "O3 @ 121617, 454546");
    assertEquals(46.566, grid.get(127717, 454861).getBackgroundO3(), 0.001, "O3 @ 127717, 454861");
  }

  @Test
  void testBelowGrid() {
    final WindRoseConcentration wrc = grid.get(100000, 305999);
    assertNotNull(wrc, "Value is outside grid, and should return the no data object, but was: ");
    assertEquals(Double.NaN, wrc.getBackgroundO3(), 0.0001, "Should have no data value");
  }
}
