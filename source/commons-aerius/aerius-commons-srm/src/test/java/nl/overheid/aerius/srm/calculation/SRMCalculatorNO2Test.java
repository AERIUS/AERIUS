/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link SRMCalculatorNO2}.
 */
class SRMCalculatorNO2Test {

  private static final double MAX_DELTA = 1E-8;
  private final SRMCalculatorNO2 calculator = new SRMCalculatorNO2();

  @Test
  void test1_18ab() {
    final double f = 0.2;
    final double c = 4;
    assertEquals(3.2, calculator.cbjmSRMxNO(f, c), MAX_DELTA, "Test 1.18a/b");
  }

  @Test
  void test1_18c() {
    final double c = 3;
    assertEquals(5.102040815, calculator.cbjmSRM2eqNO(0.0, c), MAX_DELTA, "Test 1.18c");
  }

  @Test
  void test1_18d() {
    final double c = 4;
    assertEquals(0.064102564, calculator.epsilon(c), MAX_DELTA, "Test 1.18d");
  }

  @Test
  void test1_18e() {
    final double c1 = 3;
    final double c2 = 4;
    assertEquals(9.849315068, calculator.cbjmtotaalNO(0.0, c1, 0.0, c2), MAX_DELTA, "Test 1.18e");
  }

  @Test
  void test1_18f() {
    final double o3 = 100;
    final double c = 22;
    assertEquals(10.819672131, calculator.ccbjmNO2(o3, c), MAX_DELTA, "Test 1.18f");
  }

  @Test
  void test1_18g1() {
    final double f1nox = 0.5;
    final double c1nox = 4.0;
    final double c2nod = 3.0;
    final double c2nox = 5.0;
    final double o3 = 100;
    assertEquals(7.361423694525224, calculator.cbjmtotaalNO2(f1nox, c1nox, c2nod, c2nox, o3), MAX_DELTA, "Test 1.18g");
  }

  @Test
  void test1_18g2() {
    final double f1nox = 3.5337294315;
    final double c1nox = 39.6202118809;
    final double c2nod = 18.9602051812;
    final double c2nox = 83.0734806562;
    final double o3 = 42.555;
    assertEquals(40.1148305962, calculator.cbjmtotaalNO2(f1nox, c1nox, c2nod, c2nox, o3), MAX_DELTA, "Test 1.18g");
  }

  @Test
  void test1_18g3() {
    final double c1nod = 0.7259747734;
    final double c1nox = 8.4445176792;
    final double c2nod = 4.2984341344;
    final double c2nox = 18.8334399075;
    final double o3 = 42.555;
    assertEquals(11.5798085149, calculator.cbjmtotaalNO2(c1nod, c1nox, c2nod, c2nox, o3), MAX_DELTA, "Test 1.18g");
  }

}
