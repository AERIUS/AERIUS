/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.DoubleBuffer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opencl.CLBuffer;

import nl.overheid.aerius.srm.SRMWorkerTestUtil;
import nl.overheid.aerius.srm.domain.SRMConfiguration;

/**
 * Unit test for {@link DefaultContext}.
 */
class DefaultContextTest {

  private SRMConfiguration config;

  @BeforeEach
  void setUp() throws Exception {
    OpenCLTestUtil.assumeOpenCLAvailable();
    config = SRMWorkerTestUtil.getTestSRMConfiguration();
  }

  @Test
  void testInitClose() {
    final Context context = new DefaultContext(true, config.isForceCpu());

    try {
      assertTrue(context.getPoolSize() > 0, "At least 1 connection should be available.");
      assertEquals(context.getPoolSize(), context.getCurrentPoolSize(), "No connection taken yet.");
    } finally {
      context.close();
    }
  }

  @Test
  void testGetConnection() {
    final Context context = new DefaultContext(true, config.isForceCpu());
    Connection connection = null;

    try {
      connection = context.getConnection();
      assertNotNull(connection, "No connection received.");
      assertEquals(context.getPoolSize() - 1, context.getCurrentPoolSize(), "One connection taken.");
    } finally {
      if (connection != null) {
        connection.close();
      }
      context.close();
    }
  }

  @Test
  void testGetMultipleConnections() {
    final Context context = new DefaultContext(true, config.isForceCpu());
    // Make certain there is some contention.
    final int threadCount = context.getPoolSize() * 10;

    final CountDownLatch startSignal = new CountDownLatch(1);

    final ReentrantLock counterLock = new ReentrantLock();
    final Condition counterMod = counterLock.newCondition();

    // Atomic integer used here because it is mutable, unlike the regular Integer class.
    // The object must be final for the anonymous runnable classes to access it.
    final AtomicInteger counter = new AtomicInteger();

    for (int i = 0; i < threadCount; i++) {
      final Thread t = new Thread(() -> {
        try {
          startSignal.await();
        } catch (final InterruptedException e) {
          // Should not happen
        }

        final Connection connection = context.getConnection();

        try {
          Thread.sleep(100);
        } catch (final InterruptedException e) {
          // Should not happen
        }

        connection.close();

        counterLock.lock();

        try {
          counter.incrementAndGet();
          counterMod.signalAll();
        } finally {
          counterLock.unlock();
        }
      });

      startSignal.countDown();

      t.setName("t" + i);
      t.start();
    }

    // 100 ms waiting per thread + 10% margin, converted to nanoseconds
    long timeout = threadCount * 110000000;

    counterLock.lock();

    try {
      while (counter.get() < threadCount) {
        try {
          timeout = counterMod.awaitNanos(timeout);

          if (timeout <= 0) {
            break;
          }
        } catch (final InterruptedException e) {
          // Should not happen
        }
      }

      assertEquals(threadCount, counter.get(), "Not all connections closed.");
    } finally {
      counterLock.unlock();
      context.close();
    }
  }

  @Test
  void testCloseConnectionWait() throws InterruptedException {
    final Context context = new DefaultContext(true, config.isForceCpu());
    final Connection connection = context.getConnection();

    final AtomicBoolean closeDone = new AtomicBoolean();

    final Thread closeThread = new Thread(() -> {
      context.close();
      closeDone.set(true);
    });

    closeThread.start();

    while (!context.isClosed()) {
      try {
        Thread.sleep(10);
      } catch (final InterruptedException e) {
        e.printStackTrace();
      }
    }

    try {
      assertFalse(closeDone.get(), "Context already closed!");
    } finally {
      connection.close();

      closeThread.join(1000);
      assertTrue(closeDone.get(), "Context not closed!");
    }
  }

  @Test
  void testConnectionRefusalAfterClose() {
    final Context context = new DefaultContext(true, config.isForceCpu());
    context.close();

    assertThrows(IllegalStateException.class, () -> context.getConnection(), "Expected exception when context is closed.");
  }

  @Test
  void testBufferReleaseOnConnectionClose() {
    final Context context = new DefaultContext(true, config.isForceCpu());

    try {
      final Connection connection = context.getConnection();

      final CLBuffer<DoubleBuffer> buffer1 = connection.createBuffer(1, DoubleBuffer.class, MemoryUsage.READ_ONLY);
      final CLBuffer<DoubleBuffer> buffer2 = connection.createCLBuffer(Buffers.newDirectDoubleBuffer(1), MemoryUsage.READ_ONLY);
      final CLBuffer<DoubleBuffer> buffer3 = connection.createBuffer(1, DoubleBuffer.class, MemoryUsage.READ_ONLY);
      buffer3.release();

      assertTrue(buffer3.isReleased(), "Buffer not released.");

      connection.close();

      assertTrue(buffer1.isReleased(), "New buffer not released.");
      assertTrue(buffer2.isReleased(), "Wrapped buffer not released.");
      assertTrue(buffer3.isReleased(), "Already released buffer not released.");
    } finally {
      context.close();
    }
  }
}
