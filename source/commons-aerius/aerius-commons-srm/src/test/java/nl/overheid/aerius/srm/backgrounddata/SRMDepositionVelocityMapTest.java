/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.backgrounddata.DepositionVelocityReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Test class for {@link SRMDepositionVelocityMap}.
 */
class SRMDepositionVelocityMapTest {

  private static Map<Substance, SRMDepositionVelocityMap> depositionVelocityMaps;

  @BeforeAll
  static void init() throws IOException {
    try (final InputStream is = SRMDepositionVelocityMapTest.class.getResourceAsStream(
        MockAeriusSRMVersion.DEPOSITION_VELOCITY_FILE +"/" + MockAeriusSRMVersion.DEPOSITION_VELOCITY_FILE + ".csv")) {
      depositionVelocityMaps = DepositionVelocityReader.read(is, SRMConstants.HEX_HOR, SRMConstants.getReceptorUtil(),
          SRMDepositionVelocityMap.class);
    }
  }

  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        {Substance.NO2, 1135584, 0.00108037},
        {Substance.NO2, 1135585, 0.001073595},
        {Substance.NO2, 7039267, 0.0014909},
        {Substance.NO2, 2200483, 0.001192695},
        {Substance.NH3, 1135584, 0.00568685},
        {Substance.NH3, 7039267, 0.007896245},
        {Substance.NH3, 2200483, 0.0158611}
    });
  }

  @ParameterizedTest
  @MethodSource("data")
  void testDepositionVelocity(final Substance substance, final int receptorId, final double expectedValue) throws IOException {
    final SRMDepositionVelocityMap SRMDepositionVelocityMap = depositionVelocityMaps.get(substance);
    assertNotNull(SRMDepositionVelocityMap, "No deposition velocity map found for substance.");

    final Double depositionVelocity = SRMDepositionVelocityMap.getDepositionVelocityById(receptorId);
    assertNotNull(depositionVelocity, "No deposition velocity found for receptor.");
    assertEquals(expectedValue, depositionVelocity, 1E-10, "Invalid deposition velocity.");
  }
}
