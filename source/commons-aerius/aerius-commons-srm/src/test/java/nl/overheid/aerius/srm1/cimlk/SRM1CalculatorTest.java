/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm1.cimlk;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.EnumSet;
import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.srm.AbstractSRMTestBase;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.io.CIMLKResult;
import nl.overheid.aerius.srm.io.CIMLKResultWriter;
import nl.overheid.aerius.srm1.calculation.SRM1Calculator;
import nl.overheid.aerius.srm2.conversion.SRM1SRMConverter;
import nl.overheid.aerius.srm2.io.SRM2EmissionsWriter;

/**
 * Test class for {@link SRM1Calculator}.
 */
class SRM1CalculatorTest extends AbstractSRMTestBase<CIMLKResult> {

  private static final double ROUDING_RESOLUTION = 1E-5;
  private static final String RELATIVE_RESOURCES_PATH = ".";

  private static final int YEAR = 2018;
  private static final Substance[] SUBSTANCES = new Substance[] {Substance.NOX, Substance.NO2, Substance.PM10, Substance.PM25};
  private static final EnumSet<EmissionResultKey> EMISSION_RESULT_KEYS = EnumSet.of(
      EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NO2_CONCENTRATION, EmissionResultKey.NO2_DIRECT_CONCENTRATION,
      EmissionResultKey.PM10_CONCENTRATION, EmissionResultKey.PM25_CONCENTRATION);
  private static final Theme THEME = Theme.RBL;

  static List<Object[]> data() throws IOException {
    return dataItem(new File(SRM1CalculatorTest.class.getResource(RELATIVE_RESOURCES_PATH).getFile()), RELATIVE_RESOURCES_PATH,
        "segment", "receptor", "rekenresultaten");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testSRM1Calculator(final String name, final String srcFile, final String receptorFile, final String resultFile) throws Throwable {
    logger.info("Run test '{}'", name);
    init(name, srcFile, receptorFile, resultFile, null, new SRM1SRMConverter());
    assertCalculate(THEME, YEAR, SUBSTANCES, EMISSION_RESULT_KEYS, MockAeriusSRMVersion.mockAeriusSRMVersionCIMLK(),
        SubReceptorCalculation.DISABLED);
  }

  @Override
  protected void writeResultsToFile(final List<AeriusResultPoint> results) throws IOException {
    final Path resultFile = new File(getClass().getResource(RELATIVE_RESOURCES_PATH).getFile(), "Rekenresultaten_" + name + ".csv").toPath();
    new CIMLKResultWriter(10).write(resultFile.toFile(), results, 2019, "Test-version", "Test-database");
    logger.info("File: {}", resultFile);
  }

  @Override
  protected void writeEmissions(final List<EmissionSourceFeature> emissionSources2) throws IOException {
    SRM2EmissionsWriter.writeEmissions(new File(getClass().getResource(RELATIVE_RESOURCES_PATH).getFile(),
        "Emissions_" + name), YEAR, emissionSources);
  }

  @Override
  protected int assertResult(final AeriusResultPoint arp) {
    final CIMLKResultPoint rp = (CIMLKResultPoint) arp;
    final CIMLKResult er = expectedResults.get(rp.getGmlId());
    assertEquals(rp.getClass(), CIMLKResultPoint.class, "Expect NLSReceptorPoint");
    assertNotNull(er, "Expect to have result for id: " + rp.getGmlId());
    assertEquals(er.getPoint().getRoundedX(), rp.getRoundedX(), "X");
    assertEquals(er.getPoint().getRoundedY(), rp.getRoundedY(), "Y");
    final EmissionResults srm1 = rp.getSrm1Results();
    int failed = 0;
    failed += assertEqualsLog("NOX", er, er.getSrm1ConcentrationNOx(), srm1.get(EmissionResultKey.NOX_CONCENTRATION), ROUDING_RESOLUTION);
    failed += assertEqualsLog("NO2DU", er, er.getSrm1DirectConcentrationNO2(), srm1.get(EmissionResultKey.NO2_DIRECT_CONCENTRATION),
        ROUDING_RESOLUTION);
    failed += assertEqualsLog("PM10", er, er.getSrm1ConcentrationPM10(), srm1.get(EmissionResultKey.PM10_CONCENTRATION), ROUDING_RESOLUTION);
    return failed;
  }
}
