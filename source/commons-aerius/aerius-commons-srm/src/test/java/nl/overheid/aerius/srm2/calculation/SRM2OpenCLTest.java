/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import static nl.overheid.aerius.opencl.MemoryUsage.READ_ONLY;
import static nl.overheid.aerius.opencl.MemoryUsage.WRITE_ONLY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.nio.Buffer;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLKernel;

import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.opencl.Connection;
import nl.overheid.aerius.opencl.Context;
import nl.overheid.aerius.opencl.DefaultContext;
import nl.overheid.aerius.opencl.OpenCLTestUtil;
import nl.overheid.aerius.opencl.Source;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.exception.InvalidInputException;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.SRMWorkerTestUtil;
import nl.overheid.aerius.srm.backgrounddata.LandUseData.LandUseType;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm.backgrounddata.PreSRMDataBuilder;
import nl.overheid.aerius.srm.domain.SRMConfiguration;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions.Option;

/**
 * Test class for SRM2Algorithm.cl.
 */
class SRM2OpenCLTest {

  private static final double[] POS_SCHIPHOL = new double[] {114500, 481000};
  private static final double[] POS_EINDHOVEN = new double[] {154500, 384500};
  // Position halfway between Schiphol and Eindhoven
  private static final double[] POS_MIDPOINT = new double[] {134500, 432750};
  // Position at equal distance to Schiphol and Eindhoven, but not directly on the line between them.
  private static final double[] POS_MIDPOINT_SIDE = new double[] {144500, 444850};
  // Position north of Schiphol
  private static final double[] POS_SCHIPHOL_NORTH = new double[] {114500, 500000};
  // Position south of Eindhoven
  private static final double[] POS_EINDHOVEN_SOUTH = new double[] {154500, 300000};

  private static final SRM2CalculationOptions SRM2_CALCULATION_OPTIONS = new SRM2CalculationOptions(Theme.RBL,
      Set.of(Option.ALL_BACKGROUND, Option.CALCULATE_SRM1, Option.TEST)) {};

  private static AeriusSRMVersion aeriusVersion;
  private static PreSRMData preSRMData;

  private static Context clContext;
  private static Source programSource;

  private Connection connection;

  @BeforeAll
  static void initContext() throws IOException, InvalidInputException {
    OpenCLTestUtil.assumeOpenCLAvailable();
    final SRMConfiguration config = SRMWorkerTestUtil.getTestSRMConfiguration();
    aeriusVersion = MockAeriusSRMVersion.mockAeriusSRMVersionStable();
    preSRMData = loadPreSRMData(aeriusVersion);

    clContext = new DefaultContext(true, config.isForceCpu());
    programSource = SRM2Calculator.createSource(aeriusVersion, preSRMData, SRM2_CALCULATION_OPTIONS);
  }

  private static PreSRMData loadPreSRMData(final AeriusSRMVersion aeriusVersion) throws IOException, InvalidInputException {
    final SRMConfiguration config = SRMWorkerTestUtil.getTestSRMConfiguration(mock(HttpClientProxy.class));
    final PreSRMData data = new PreSRMData();

    PreSRMDataBuilder.loadAdditionalData(data, config, aeriusVersion);
    return data;
  }

  @AfterAll
  static void destroyContext() {
    if (clContext != null) {
      clContext.close();
    }
  }

  @BeforeEach
  void initConnection() {
    connection = clContext.getConnection(10, TimeUnit.SECONDS);
  }

  @AfterEach
  void destroyConnection() {
    connection.close();
  }

  @Test
  void testDetermineRoughnessClassification() {
    // Prepare test data
    final List<RoughnessClassTestData> testData = new ArrayList<>();

    // Test values
    testData.add(new RoughnessClassTestData(0.05, new double[] {0.2221, 0.6574, 60.0, 0.7000}));
    testData.add(new RoughnessClassTestData(0.15, new double[] {0.2745, 0.6688, 60.0, 0.7050}));
    testData.add(new RoughnessClassTestData(0.20, new double[] {0.3613, 0.6680, 100.0, 0.6525}));
    testData.add(new RoughnessClassTestData(0.55, new double[] {0.7054, 0.6207, 400.0, 0.7400}));

    final int testCount = testData.size();

    final OpenCLProgram program = new OpenCLProgram(connection, programSource, "testDetermineRoughnessClassification");

    // Initialize buffers
    program.setArg(0, testCount);
    final CLBuffer<DoubleBuffer> roughnessBuffer = program.setBuffer(1, testCount);
    // Fill buffers with test data

    for (final RoughnessClassTestData data : testData) {
      roughnessBuffer.getBuffer().put(data.roughness);
    }

    roughnessBuffer.getBuffer().rewind();

    // Run tests
    final CLBuffer<DoubleBuffer> resultBuffer = program.execute(testCount, 2, testCount * 4);

    // Verify data
    for (int i = 0; i < testCount; i++) {
      final RoughnessClassTestData data = testData.get(i);

      assertEquals(data.expectedResult[0], resultBuffer.getBuffer().get(i * 4), 0.0001, "Invalid roughness class value 'a' for " + i);
      assertEquals(data.expectedResult[1], resultBuffer.getBuffer().get(i * 4 + 1), 0.0001, "Invalid roughness class value 'b' for " + i);
      assertEquals(data.expectedResult[2], resultBuffer.getBuffer().get(i * 4 + 2), 0.0001, "Invalid Monin-Obukhof length for " + i);
      assertEquals(data.expectedResult[3], resultBuffer.getBuffer().get(i * 4 + 3), 0.0001, "Invalid correction factor for meteo for " + i);
    }
  }

  @Test
  void testDetermineMeteoCorrection() {
    // Prepare test data
    final List<MeteoCorrectionTestData> testData = new ArrayList<>();

    // Test values
    testData.add(new MeteoCorrectionTestData(POS_SCHIPHOL, 0.7400, 0.7400));
    testData.add(new MeteoCorrectionTestData(POS_EINDHOVEN, 0.7400, 0.666));
    testData.add(new MeteoCorrectionTestData(POS_MIDPOINT, 0.7400, 0.7030));
    testData.add(new MeteoCorrectionTestData(POS_MIDPOINT_SIDE, 0.7400, 0.7030));
    testData.add(new MeteoCorrectionTestData(POS_SCHIPHOL_NORTH, 0.7400, 0.7400));
    testData.add(new MeteoCorrectionTestData(POS_EINDHOVEN_SOUTH, 0.7400, 0.666));

    final int testCount = testData.size();

    final OpenCLProgram program = new OpenCLProgram(connection, programSource, "testDetermineMeteoCorrection");

    // Initialize buffers
    program.setArg(0, testCount);
    final CLBuffer<DoubleBuffer> pointBuffer = program.setBuffer(1, testCount * 2);
    final CLBuffer<DoubleBuffer> correctionBuffer = program.setBuffer(2, testCount);

    // Fill buffers with test data

    for (final MeteoCorrectionTestData data : testData) {
      pointBuffer.getBuffer().put(data.point);
      correctionBuffer.getBuffer().put(data.correctionSchiphol);
    }

    pointBuffer.getBuffer().rewind();
    correctionBuffer.getBuffer().rewind();

    // Run tests
    final CLBuffer<DoubleBuffer> resultBuffer = program.execute(testCount, 3, testCount);

    // Verify data
    for (int i = 0; i < testCount; i++) {
      final MeteoCorrectionTestData data = testData.get(i);
      assertEquals(data.expectedResult, resultBuffer.getBuffer().get(i), 0.0001, "Invalid correction factor for " + i);
    }
  }

  @Test
  void testDetermineWindSectorIndex() {
    // Prepare test data, expects 36 wind sectors
    final List<WindSectorTestData> testData = new ArrayList<WindSectorTestData>();

    testData.add(new WindSectorTestData(1, 12, 0));
    testData.add(new WindSectorTestData(1, 11, 1));
    testData.add(new WindSectorTestData(-1, 12, 0));
    testData.add(new WindSectorTestData(-1, 11, 35));
    testData.add(new WindSectorTestData(0, 100, 0));
    testData.add(new WindSectorTestData(100, 0, 9));
    testData.add(new WindSectorTestData(0, -100, 18));
    testData.add(new WindSectorTestData(-100, 0, 27));
    // Tests the half sector offset
    testData.add(new WindSectorTestData(-8.7, 99.62, 0));
    testData.add(new WindSectorTestData(8.7, 99.62, 0));

    final int testCount = testData.size();

    final OpenCLProgram program = new OpenCLProgram(connection, programSource, "testDetermineWindSectorIndex");

    // Initialize buffers
    program.setArg(0, testCount);
    final CLBuffer<DoubleBuffer> directionBuffer = program.setBuffer(1, testCount * 2);

    // Fill buffers
    for (final WindSectorTestData data : testData) {
      directionBuffer.getBuffer().put(data.x);
      directionBuffer.getBuffer().put(data.y);
    }

    directionBuffer.getBuffer().rewind();

    // Run tests
    final CLBuffer<IntBuffer> resultBuffer = program.execute(testCount, 2, Buffers.newDirectIntBuffer(testCount));

    for (int i = 0; i < testCount; i++) {
      assertEquals(testData.get(i).expectedIndex, resultBuffer.getBuffer().get(i), "Invalid wind sector index for " + i);
    }
  }

  @Test
  void testLookupRoughnessSource() {
    final List<RoughnessTestData> testData = new ArrayList<RoughnessTestData>();

    testData.add(new RoughnessTestData(350000, 350000, -1.0, false));
    testData.add(new RoughnessTestData(8000, 624000, 0.030, false));
    testData.add(new RoughnessTestData(287000, 606000, 0.0427, false));
    testData.add(new RoughnessTestData(0, 0, -1.0, false));
    testData.add(new RoughnessTestData(98338, 486466, 0.5979, false));
    testData.add(new RoughnessTestData(168500, 440500, 0.4592, false));
    testData.add(new RoughnessTestData(169500, 440500, 0.4630, false));

    final int testCount = testData.size();

    final OpenCLProgram program = new OpenCLProgram(connection, programSource, "testLookupRoughnessSource");

    // Initialize buffers
    program.setArg(0, testCount);
    final CLBuffer<DoubleBuffer> landUseBuffer = program.setBuffer(1,
        preSRMData.getLandUse(aeriusVersion, LandUseType.MED).getData());
    final CLBuffer<DoubleBuffer> sourcePointBuffer = program.setBuffer(2, testCount * 2);

    // Fill buffers
    for (final RoughnessTestData data : testData) {
      sourcePointBuffer.getBuffer().put(data.x);
      sourcePointBuffer.getBuffer().put(data.y);
    }

    landUseBuffer.getBuffer().rewind();
    sourcePointBuffer.getBuffer().rewind();

    // Run tests
    final CLBuffer<DoubleBuffer> resultBuffer = program.execute(testCount, 3, testCount);

    for (int i = 0; i < testCount; i++) {
      assertResult(testData.get(i), resultBuffer.getBuffer().get(i));
    }
  }

  void assertResult(final RoughnessTestData rtd, final double result) {
    // Test against a known correct value.
    assertEquals(rtd.expected, result, 0.0002, "Invalid roughness for [" + rtd.x + ", " + rtd.y + "]");
    // Tests against the java implementation of the algorithm.
    assertEquals(preSRMData.getLandUseValueSource(aeriusVersion, rtd.x, rtd.y), result, 0.0001,
        "Invalid roughness for [" + rtd.x + ", " + rtd.y + "]");
  }

  @Test
  void testCalculateVerticalDiffusion() {
    final double[][] input = {
        {0, 1, 1, 1, 1, 0, 0}, // 0.14085908577 0
        {1, 1, 1, 1, 1, 0, 1}, // 0.14085902199 - 7.09929677089
        {1, 1, 1, 1, 1, 1, 2}, // 0.14085902199 - 7.09929677089
        {1000, 1, 1, 1, 1, 0, 943.5068232392}, //
        {10000, 1, 1, 1, 1, 0, 6666.67308345}, // -6.23669193464
    };
    final OpenCLProgram program = new OpenCLProgram(connection, programSource, "testCalculateVerticalDiffusion");
    final int testCount = input.length;
    program.setArg(0, testCount);
    final CLBuffer<DoubleBuffer> distanceBuffer = program.setBuffer(1, testCount);
    final CLBuffer<DoubleBuffer> roughnessClassBuffer = program.setBuffer(2, testCount * 4);
    final CLBuffer<DoubleBuffer> sigmaz0Buffer = program.setBuffer(3, testCount);

    for (int i = 0; i < input.length; i++) {
      distanceBuffer.getBuffer().put(input[i][0]);
      roughnessClassBuffer.getBuffer().put(input[i][1]);
      roughnessClassBuffer.getBuffer().put(input[i][2]);
      roughnessClassBuffer.getBuffer().put(input[i][3]);
      roughnessClassBuffer.getBuffer().put(input[i][4]);
      sigmaz0Buffer.getBuffer().put(input[i][5]);
    }

    distanceBuffer.getBuffer().rewind();
    roughnessClassBuffer.getBuffer().rewind();
    sigmaz0Buffer.getBuffer().rewind();

    // Run tests
    final CLBuffer<DoubleBuffer> resultBuffer = program.execute(testCount, 4, testCount);

    for (int i = 0; i < testCount; i++) {
      assertEquals(input[i][6], resultBuffer.getBuffer().get(i), input[i][6] * 0.0001, "Invalid vertical diffusion for:"
          + Arrays.toString(input[i]));
    }
  }

  @Test
  void testCalculateDepletionFactorNH3() {
    final double[][] input = {
        //distance, z0, background, result
        {30, 0.0005, 1, 0.9944487788059911},
        {1400, 0.0005, 8, 0.7522386590630664},
        {850, 0.6, 12, 0.7518907285830457},
    };

    final OpenCLProgram program = new OpenCLProgram(connection, programSource, "testCalculateDepletionFactorNH3");
    final int testCount = input.length;

    program.setArg(0, testCount);
    final CLBuffer<DoubleBuffer> distanceBuffer = program.setBuffer(1, testCount);
    final CLBuffer<DoubleBuffer> z0Buffer = program.setBuffer(2, testCount);
    final CLBuffer<DoubleBuffer> backgroundConcentrationBuffer = program.setBuffer(3, testCount);

    for (int i = 0; i < input.length; i++) {
      distanceBuffer.getBuffer().put(input[i][0]);
      z0Buffer.getBuffer().put(input[i][1]);
      backgroundConcentrationBuffer.getBuffer().put(input[i][2]);
    }

    distanceBuffer.getBuffer().rewind();
    z0Buffer.getBuffer().rewind();
    backgroundConcentrationBuffer.getBuffer().rewind();

    // Run tests
    final CLBuffer<DoubleBuffer> resultBuffer = program.execute(testCount, 4, testCount);

    for (int i = 0; i < testCount; i++) {
      assertEquals(input[i][3], resultBuffer.getBuffer().get(i), input[i][3] * 0.0001, "Invalid Depletion Factor NH3 for:"
          + Arrays.toString(input[i]));
    }
  }

  @Test
  void testCalculateDepletionFactorNOx() {
    final double[][] input = {
        //distance, z0, result
        {3000, 0.05, 0.8522054434377123},
        {200, 0.55, 0.9848405462893954},
    };

    final OpenCLProgram program = new OpenCLProgram(connection, programSource, "testCalculateDepletionFactorNOx");
    final int testCount = input.length;

    program.setArg(0, testCount);
    final CLBuffer<DoubleBuffer> distanceBuffer = program.setBuffer(1, testCount);
    final CLBuffer<DoubleBuffer> z0Buffer = program.setBuffer(2, testCount);

    for (int i = 0; i < input.length; i++) {
      distanceBuffer.getBuffer().put(input[i][0]);
      z0Buffer.getBuffer().put(input[i][1]);
    }

    distanceBuffer.getBuffer().rewind();
    z0Buffer.getBuffer().rewind();

    // Run tests
    final CLBuffer<DoubleBuffer> resultBuffer = program.execute(testCount, 3, testCount);

    for (int i = 0; i < testCount; i++) {
      assertEquals(input[i][2], resultBuffer.getBuffer().get(i), input[i][2] * 0.0001, "Invalid Depletion Factor NOx for:"
          + Arrays.toString(input[i]));
    }
  }

  @Test
  void testSubReceptorCalculationNeeded() {
    // Prepare test data
    final List<SubReceptorCalculationNeededTestData> testData = new ArrayList<>();

    // Test values
    testData.add(new SubReceptorCalculationNeededTestData(new double[] {0.0, 0.0}, new double[] {0.0, 0.0}, new double[] {0.0, 0.0}, true));
    testData.add(new SubReceptorCalculationNeededTestData(new double[] {0.0, 0.0}, new double[] {-10.0, 0.0}, new double[] {0.0, 10.0}, true));
    testData.add(new SubReceptorCalculationNeededTestData(new double[] {0.0, 0.0}, new double[] {60.0, 0.0}, new double[] {63.0, 0.0}, true));
    testData.add(new SubReceptorCalculationNeededTestData(new double[] {0.0, 0.0}, new double[] {60.0, 0.0}, new double[] {66.0, 0.0}, false));
    testData.add(new SubReceptorCalculationNeededTestData(new double[] {0.0, 0.0}, new double[] {0.0, 50.0}, new double[] {0.0, 57.0}, true));
    testData.add(new SubReceptorCalculationNeededTestData(new double[] {0.0, 0.0}, new double[] {0.0, 50.0}, new double[] {0.0, 58.0}, false));
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        final int xMod = i % 2 == 0 ? 1 : -1;
        final int yMod = j % 2 == 0 ? 1 : -1;
        testData.add(new SubReceptorCalculationNeededTestData(new double[] {0.0, 0.0},
            new double[] {xMod * 44.0, yMod * 28.0}, new double[] {xMod * 46.0, yMod * 28.0}, true));
        testData.add(new SubReceptorCalculationNeededTestData(new double[] {0.0, 0.0},
            new double[] {xMod * 45.0, yMod * 28.0}, new double[] {xMod * 47.0, yMod * 28.0}, false));
      }
    }
    testData.add(new SubReceptorCalculationNeededTestData(new double[] {0.0, 0.0}, new double[] {100.0, 100.0}, new double[] {120.0, 100.0}, false));
    testData.add(
        new SubReceptorCalculationNeededTestData(new double[] {1000.0, 1050.0}, new double[] {1010.0, 1040.0}, new double[] {1020.0, 1030.0}, true));
    testData.add(
        new SubReceptorCalculationNeededTestData(new double[] {-5000.0, 100.0}, new double[] {-4950.0, 100.0}, new double[] {-4952.0, 100.0}, true));

    final int testCount = testData.size();

    final OpenCLProgram program = new OpenCLProgram(connection, programSource, "testSubReceptorCalculationNeeded");

    // Initialize buffers
    program.setArg(0, testCount);
    final CLBuffer<DoubleBuffer> receptorBuffer = program.setBuffer(1, testCount * 2);
    final CLBuffer<DoubleBuffer> segmentStartBuffer = program.setBuffer(2, testCount * 2);
    final CLBuffer<DoubleBuffer> segmentEndBuffer = program.setBuffer(3, testCount * 2);
    // Fill buffers with test data

    for (final SubReceptorCalculationNeededTestData data : testData) {
      receptorBuffer.getBuffer().put(data.receptorPoint);
      segmentStartBuffer.getBuffer().put(data.segmentStart);
      segmentEndBuffer.getBuffer().put(data.segmentEnd);
    }

    receptorBuffer.getBuffer().rewind();
    segmentStartBuffer.getBuffer().rewind();
    segmentEndBuffer.getBuffer().rewind();

    // Run tests
    final CLBuffer<ShortBuffer> resultBuffer = program.execute(testCount, 4, Buffers.newDirectShortBuffer(testCount));

    // Verify data
    for (int i = 0; i < testCount; i++) {
      final SubReceptorCalculationNeededTestData data = testData.get(i);

      assertEquals(data.expectedResult,
          resultBuffer.getBuffer().get(i) == 1, "Invalid subreceptor calculation needed for receptor:" + Arrays.toString(data.receptorPoint)
              + ",  segmentStart:" + Arrays.toString(data.segmentStart) + ",  segmentEnd:" + Arrays.toString(data.segmentEnd));
    }
  }

  private static class WindSectorTestData {
    private final double x;
    private final double y;
    private final int expectedIndex;

    WindSectorTestData(final double x, final double y, final int expectedIndex) {
      this.x = x;
      this.y = y;
      this.expectedIndex = expectedIndex;
    }
  }

  // Used to improve readability of test data.
  private static class RoughnessClassTestData {
    private final double roughness;
    private final double[] expectedResult;

    RoughnessClassTestData(final double roughness, final double[] expectedResult) {
      this.roughness = roughness;
      this.expectedResult = expectedResult;
    }
  }

  private static class RoughnessTestData {
    private final double x;
    private final double y;
    private final double expected;
    private final boolean useOro;

    RoughnessTestData(final double x, final double y, final double expected, final boolean useOro) {
      this.x = x;
      this.y = y;
      this.expected = expected;
      this.useOro = useOro;
    }
  }

  private static class MeteoCorrectionTestData {
    private final double[] point;
    private final double correctionSchiphol;
    private final double expectedResult;

    MeteoCorrectionTestData(final double[] point, final double correctionSchiphol, final double expectedResult) {
      this.point = point;
      this.correctionSchiphol = correctionSchiphol;
      this.expectedResult = expectedResult;
    }
  }

  private static class SubReceptorCalculationNeededTestData {
    private final double[] receptorPoint;
    private final double[] segmentStart;
    private final double[] segmentEnd;
    private final boolean expectedResult;

    SubReceptorCalculationNeededTestData(final double[] receptorPoint,
        final double[] segmentStart, final double[] segmentEnd, final boolean expectedResult) {
      this.receptorPoint = receptorPoint;
      this.segmentStart = segmentStart;
      this.segmentEnd = segmentEnd;
      this.expectedResult = expectedResult;
    }

  }

  /**
   * Wrapper class to simplify usage of kernel in tests.
   */
  private static class OpenCLProgram {
    private final CLKernel kernel;
    private final Connection connection;
    private final List<CLBuffer<? extends Buffer>> buffers = new ArrayList<>();

    OpenCLProgram(final Connection connection, final Source source, final String programName) {
      this.connection = connection;
      kernel = connection.getKernel(source, programName);
    }

    /**
     * @param arg
     * @param data
     * @return
     */
    CLBuffer<DoubleBuffer> setBuffer(final int arg, final DoubleBuffer data) {
      final CLBuffer<DoubleBuffer> buffer = connection.createCLBuffer(data, WRITE_ONLY);
      kernel.setArg(arg, buffer);
      buffers.add(buffer);
      return buffer;
    }

    CLBuffer<DoubleBuffer> execute(final int count, final int arg, final int size) {
      return execute(count, arg, Buffers.newDirectDoubleBuffer(size));
    }

    <B extends Buffer> CLBuffer<B> execute(final int count, final int arg, final B directBuffer) {
      final CLBuffer<B> resultBuffer = connection.createCLBuffer(directBuffer, WRITE_ONLY);
      kernel.setArg(arg, resultBuffer);
      for (final CLBuffer<? extends Buffer> buffer : buffers) {
        connection.queueBufferWrite(buffer, false);
      }
      connection.queue1DKernelExecution(kernel, count);
      connection.queueBufferRead(resultBuffer, true);
      resultBuffer.getBuffer().rewind();
      return resultBuffer;
    }

    /**
     * Creates a buffer with given size and sets it at given arg on the kernel. It returns the buffer.
     * @param arg set buffer at given kernel arg
     * @param size size of the buffer
     * @return buffer
     */
    CLBuffer<DoubleBuffer> setBuffer(final int arg, final int size) {
      final CLBuffer<DoubleBuffer> buffer = connection.createCLBuffer(Buffers.newDirectDoubleBuffer(size), READ_ONLY);
      kernel.setArg(arg, buffer);
      buffers.add(buffer);
      return buffer;
    }

    /**
     * Sets the given value at the kernal argument index
     * @param arg kernel argument index
     * @param value value to set
     */
    void setArg(final int arg, final int value) {
      kernel.setArg(arg, value);
    }
  }
}
