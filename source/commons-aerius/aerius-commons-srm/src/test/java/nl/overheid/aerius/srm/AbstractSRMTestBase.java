/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.emissions.CategoryBasedEmissionFactorSupplier;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.opencl.OpenCLTestUtil;
import nl.overheid.aerius.receptor.SubReceptorCreator;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.EmissionsUpdater;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.srm.io.CIMLKResult;
import nl.overheid.aerius.srm.io.LegacyNSLImportReader;
import nl.overheid.aerius.srm.io.PreCalculationUtil;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.conversion.AbstractSRMConverter;
import nl.overheid.aerius.srm2.conversion.EmissionSourceSRMConverter;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.util.FeaturePointToEnginePointUtil;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.GeometryCalculatorImpl;
import nl.overheid.aerius.worker.Worker;

/**
 * Base class for performing srm test calculations.
 */
public abstract class AbstractSRMTestBase<R extends CIMLKResult> {

  private static final String FILE_EXTENSION = "csv";

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  // Make fields expensive to initialize static so it only has to be done once.
  protected static SectorCategories sectorCategories;
  protected static Worker<SRMInputData<EngineSource>, ?> workerHandler;

  private final LegacyNSLImportReader reader = new LegacyNSLImportReader(true);
  private final GeometryCalculator geometryCalculator = new GeometryCalculatorImpl();
  private EmissionSourceSRMConverter converter;

  protected final Map<String, R> expectedResults = new HashMap<>();

  protected String name;
  protected String srcFile;
  protected String receptorFile;
  protected String correctionsFile;
  protected String resultFile;

  protected Map<Integer, List<EngineSource>> sources = new HashMap<>();
  protected List<AeriusPoint> receptors;
  protected List<EmissionSourceFeature> emissionSources;

  private List<CIMLKCorrection> corrections;

  protected void init(final String name, final String srcFile, final String receptorFile, final String resultFile,
      final String correctionsFile, final AbstractSRMConverter<? extends EngineSource> firstConverter) throws IOException, AeriusException {
    this.name = name;
    this.srcFile = srcFile;
    this.receptorFile = receptorFile;
    this.resultFile = resultFile;
    this.correctionsFile = correctionsFile;
    converter = new EmissionSourceSRMConverter(firstConverter);

    if (resultFile != null) {
      try (final InputStream is = getClass().getResourceAsStream(resultFile)) {
        final LineReaderResult<R> results = readResultFile(is);

        if (!results.getExceptions().isEmpty()) {
          throw results.getExceptions().get(0);
        }
        results.getObjects().forEach(r -> expectedResults.put(r.getCalculationPointId(), r));
      }
    }
  }

  @BeforeEach
  public void setUp() throws Exception {
    OpenCLTestUtil.assumeOpenCLAvailable();

    if (workerHandler == null) {
      sectorCategories = new TestDomain().getCategories();
      workerHandler = SRMWorkerTestUtil.createWorkerHandler();
    }
  }

  protected LineReaderResult<R> readResultFile(final InputStream is) throws IOException {
    return (LineReaderResult<R>) reader.readResultFile(is);
  }

  protected static List<Object[]> dataItem(final File root, final String relativePath, final String... filePatterns) throws IOException {
    final List<Object[]> data = new ArrayList<>();

    for (final File path : root.listFiles()) {
      final Object[] testcase = new Object[filePatterns.length + 1];
      final List<File> files = FileUtil.getFilesWithExtension(path, FILE_EXTENSION);

      testcase[0] = FileUtil.getFileWithoutExtension(path);
      for (int i = 0; i < filePatterns.length; i++) {
        testcase[i + 1] = findFileName(files, filePatterns[i], root.getAbsolutePath(), relativePath);
      }
      if (testcase[1] != null && testcase[2] != null) {
        data.add(testcase);
      }
    }
    if (data.isEmpty()) {
      fail("No testcases found for directory: " + root);
    }
    return data;
  }

  protected static String findFileName(final List<File> files, final String pattern, final String strip, final String relativePath) {
    final Optional<File> file = files.stream().filter(f -> f.getName().contains(pattern)).findFirst();
    if (file.isPresent()) {
      final String filename = file.get().getAbsolutePath();
      return filename.replace(strip, relativePath);
    } else {
      return null;
    }
  }

  /**
   * Performs a calculation
   */
  protected void assertCalculate(final Theme theme, final int year, final Substance[] substances, final EnumSet<EmissionResultKey> resultKeys,
      final AeriusSRMVersion version, final SubReceptorCalculation subReceptorCalculation) throws Exception {
    sources = loadSources(srcFile, year, substances);
    receptors = loadReceptors(receptorFile, subReceptorCalculation, version);
    corrections = correctionsFile == null ? Collections.emptyList() : loadCorrections(correctionsFile);

    final SRMInputData<EngineSource> input = new SRMInputData<>(theme, version);
    input.setCorrections(corrections);
    input.setSubstances(Arrays.asList(substances));
    input.setEmissionResultKeys(resultKeys);
    input.setSubReceptorCalculation(subReceptorCalculation);
    input.setYear(year);
    for (final Entry<Integer, List<EngineSource>> entry : sources.entrySet()) {
      input.setEmissionSources(entry.getKey(), entry.getValue());
    }
    input.setReceptors(receptors);
    prepare(input);

    final Serializable output = workerHandler.run(input, null, null);
    assertNotNull(output, "SRM calculation returned 'null' result");
    if (output instanceof Exception) {
      throw (Exception) output;
    }
    assertEquals(CalculationResult.class, output.getClass(), "Result not of expected type");
    final Map<Integer, List<AeriusResultPoint>> results = ((CalculationResult) output).getResults();
    final List<AeriusResultPoint> list = results.entrySet().stream()
        .flatMap(e -> e.getValue().stream())
        .sorted((x, y) -> Integer.compare(x.getId(), y.getId()))
        .collect(Collectors.toList());

    writeResultsToFile(list);
    writeSectorResultsToFile(results);
    assertSectorResults(results);

    if (theme == Theme.RBL) {
      logger.info("max:{}", list.stream().mapToDouble(r -> r.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION)).max());
      final int sumFailedResults = list.stream().map(CIMLKResultPoint.class::cast).mapToInt(this::assertResult).sum();
      if (sumFailedResults > 0) {
        logger.error("Expected 0 calculation results to be different but was: {}", sumFailedResults);
      }
      assertEquals(0, sumFailedResults, "Expected 0 calculation results to be different");
    }
  }

  protected void prepare(final SRMInputData<EngineSource> input) {}

  protected void writeEmissions(final List<EmissionSourceFeature> emissionSources) throws IOException {}

  protected void writeResultsToFile(final List<AeriusResultPoint> results) throws IOException {}

  protected void writeSectorResultsToFile(final Map<Integer, List<AeriusResultPoint>> results) throws IOException {}

  protected void assertSectorResults(final Map<Integer, List<AeriusResultPoint>> results) {}

  /**
   * Asserts if the results of the given result point match the expected results.
   *
   * @param rp result to check
   * @return number of results that are different
   */
  protected abstract int assertResult(final AeriusResultPoint rp);

  protected int assertEqualsLog(final String message, final CIMLKResult er, final double ex, final double re, final double roudingResolution) {
    try {
      assertEquals(ex, re, roudingResolution, message + "(" + er.getPoint().getRoundedCmX() + ", " + er.getPoint().getRoundedCmY() + ")");
      return 0;
    } catch (final AssertionError e) {
      logger.error(e.getMessage());
      return 1;
    }
  }

  private Map<Integer, List<EngineSource>> loadSources(final String fileName, final int year, final Substance[] substances)
      throws IOException, AeriusException {
    final ImportParcel results = loadData(fileName);
    emissionSources = results.getSituation().getEmissionSourcesList();
    final EmissionFactorSupplier emissionFactorSupplier = new CategoryBasedEmissionFactorSupplier(sectorCategories, null, year);
    final EmissionsUpdater emissionsUpdater = new EmissionsUpdater(emissionFactorSupplier, geometryCalculator);
    emissionsUpdater.updateEmissions(emissionSources);
    final List<Substance> substancesList = List.of(substances);

    logger.info("Loaded sources: {}", emissionSources.size());
    assertFalse(emissionSources.isEmpty(), "Sources list is empty for some reason");
    writeEmissions(emissionSources);
    final Map<Integer, List<EngineSource>> map = new HashMap<>();
    for (final EmissionSourceFeature source : emissionSources) {
      map.computeIfAbsent(source.getProperties().getSectorId(), e -> new ArrayList<>());
      map.get(source.getProperties().getSectorId()).addAll(converter.convert(source, substancesList));
    }
    return map;
  }

  private List<AeriusPoint> loadReceptors(final String filename, final SubReceptorCalculation subReceptorCalculation,
      final AeriusSRMVersion version) throws IOException, AeriusException {
    final ImportParcel receptorResult = loadData(filename);

    final List<CalculationPointFeature> receptors = receptorResult.getCalculationPointsList();
    final List<CIMLKDispersionLineFeature> dispersionLines = receptorResult.getSituation().getCimlkDispersionLinesList();

    logger.info("Loaded {} receptors.", receptors.size());
    logger.info("Loaded {} dispersion lines.", dispersionLines.size());

    final List<AeriusPoint> converted = FeaturePointToEnginePointUtil.convert(receptors);

    PreCalculationUtil.assignDispersionLines(Collections.emptyList(), converted, dispersionLines);

    return fakeSubReceptorsIfNeeded(converted, subReceptorCalculation, version);
  }

  private List<AeriusPoint> fakeSubReceptorsIfNeeded(final List<AeriusPoint> points, final SubReceptorCalculation subReceptorCalculation,
      final AeriusSRMVersion version) throws AeriusException {
    if (subReceptorCalculation != SubReceptorCalculation.PROVIDED) {
      return points;
    }
    final ReceptorUtil receptorUtil = new ReceptorUtil(TestDomain.getExampleGridSettings());

    final List<AeriusPoint> receptorsToCalculate = new ArrayList<>();
    final Set<Integer> addedReceptorIds = new HashSet<>();
    final TestSRMSubReceptorCreator subReceptorCreator = new TestSRMSubReceptorCreator();
    for (final AeriusPoint receptor : points) {
      // Normally we're using points of type RECEPTOR, but when read from rcp they are considered POINT
      // Fake them being receptors by setting the type and ID.
      // However, since the calculation points in our test case can reference the same receptor based on coordinates...
      final int receptorId = receptorUtil.getReceptorIdFromPoint(receptor);
      if (addedReceptorIds.contains(receptorId)) {
        continue;
      } else {
        addedReceptorIds.add(receptorId);
      }
      receptor.setId(receptorId);
      receptor.setPointType(AeriusPointType.RECEPTOR);
      // Fake only the first couple of points having multiple subreceptors, others just being themselves.
      final TestSRMSubPoint receptorSubPoint = subReceptorCreator.copyToSubReceptorPoint(receptor, receptor.getPointType(), 0);

      if (receptorsToCalculate.size() > 3) {
        receptorsToCalculate.add(receptorSubPoint);
        final TestSRMSubPoint subPoint = subReceptorCreator.copyToSubReceptorPoint(receptor, AeriusPointType.SUB_RECEPTOR, 0);
        subPoint.setParent(receptor);
        subPoint.setId(0);
        receptorsToCalculate.add(subPoint);
      } else {
        final TestSRMSubPoint centerReceptor = receptorSubPoint;
        receptorsToCalculate.add(centerReceptor);
        final Collection<TestSRMSubPoint> subReceptors = subReceptorCreator.computePoints(centerReceptor, 1, null);
        receptorsToCalculate.addAll(subReceptors);
      }
    }

    return receptorsToCalculate;
  }

  private List<CIMLKCorrection> loadCorrections(final String filename) throws IOException, AeriusException {
    final ImportParcel result = loadData(filename);

    return result.getSituation().getCimlkCorrections();
  }

  private ImportParcel loadData(final String fileName) throws IOException, AeriusException {
    final ImportParcel results = new ImportParcel();

    try (final InputStream stream = getClass().getResourceAsStream(fileName)) {
      reader.read(fileName, stream, sectorCategories, null, results);
    }

    assertEquals(0, results.getExceptions().size(), "Should have no exceptions reading sources " + results.getExceptions());
    return results;
  }

  private static class TestSRMSubPoint extends AeriusPoint implements IsSubPoint {

    private static final long serialVersionUID = 1L;

    private AeriusPointType parentPointType;

    TestSRMSubPoint(final AeriusPoint original, final AeriusPointType type) {
      super(original.getId(), original.getParentId(), type, original.getX(), original.getY());
      super.setGmlId(original.getGmlId());
    }

    @Override
    public AeriusPointType getParentPointType() {
      return parentPointType;
    }

    @Override
    public void setParent(final AeriusPoint parentPoint) {
      setParentId(parentPoint.getId());
      this.parentPointType = parentPoint.getPointType();
    }

    @Override
    public int getLevel() {
      throw new IllegalStateException("Not expected to be used in this test");
    }

    @Override
    public Polygon getArea() {
      throw new IllegalStateException("Not expected to be used in this test");
    }

    @Override
    public void setArea(final Polygon geometry) {
      // NO-OP
    }

  }

  private static class TestSRMSubReceptorCreator extends SubReceptorCreator<TestSRMSubPoint> {

    protected TestSRMSubReceptorCreator() {
      super(AeriusPointType.SUB_RECEPTOR, TestDomain.getExampleGridSettings().getZoomLevel1().getHexagonRadius(), List.of(300D, 150D), 1);
    }

    @Override
    public TestSRMSubPoint copyToSubReceptorPoint(final AeriusPoint aeriusPoint, final AeriusPointType pointType, final int subLevel) {
      return new TestSRMSubPoint(aeriusPoint, pointType);
    }
  }
}
