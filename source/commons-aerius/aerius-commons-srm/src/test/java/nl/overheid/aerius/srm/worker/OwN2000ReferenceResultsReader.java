/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import nl.overheid.aerius.io.AbstractLineReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Reader to read the OwN2000 SRM2 reference results.
 * It's based on extended NSLResult data object but with deposition added because that is not part of CIMLK.
 */
class OwN2000ReferenceResultsReader extends AbstractLineReader<OwN2000Result> {

  @Override
  public LineReaderResult<OwN2000Result> readObjects(final InputStream inputStream) throws IOException {
    try (final InputStreamReader reader = new InputStreamReader(inputStream)) {
      return super.readObjects(reader, 1);
    }
  }

  @Override
  protected OwN2000Result parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final String[] split = line.split(";");
    final OwN2000Result result = new OwN2000Result();

    final String calculationPointId = split[0];
    final int sectorId = Integer.valueOf(split[1]);

    result.setCalculationPointId(calculationPointId + "_" + sectorId);
    result.setSectorId(sectorId);
    result.setSrm2NOxDeposition(Double.valueOf(split[2]));
    result.setSrm2NH3Deposition(Double.valueOf(split[3]));
    return result;
  }
}
