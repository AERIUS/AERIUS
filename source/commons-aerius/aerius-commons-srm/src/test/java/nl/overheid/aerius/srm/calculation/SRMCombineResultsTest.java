/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.theme.cimlk.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;

/**
 * Test class for {@link SRMCombineResults}.
 */
class SRMCombineResultsTest {

  @Test
  void testPreprocessResults() {
    final List<CIMLKResultPoint> srm1Results = new Builder<CIMLKResultPoint>()
        .add(1, 10, 10, 0, 11)
        .add(2, 20, 20, 21, 21)
        .add(3, 30, 30, 31, 0)
        .build();
    final List<AeriusResultPoint> srm2ResultPoints = new Builder<AeriusResultPoint>()
        .add(1, 10, 10, 0, 10)
        .add(2, 20, 20, 20, 20)
        .add(3, 30, 30, 30, 0)
        .build();
    SRMCombineResults.preprocessResults(srm1Results, srm2ResultPoints);
    final List<CIMLKResultPoint> results = srm2ResultPoints.stream().map(CIMLKResultPoint.class::cast).collect(Collectors.toList());

    assertEquals(3, results.size(), "should have correct nr of results");
    assertEquals(4 , collect(results, CIMLKResultPoint::getSrm1Results, this::count).mapToInt(Integer::intValue).sum(),
        "should have correct nr of srm1");
    assertEquals(4 , collect(results, CIMLKResultPoint::getSrm2Results, this::count).mapToInt(Integer::intValue).sum(),
        "should have correct nr of srm2");
    assertEquals(4 , collect(results, CIMLKResultPoint::getEmissionResults, this::count).mapToInt(Integer::intValue).sum(),
        "should have correct nr of srm2");
    assertEquals(84 , collect(results, CIMLKResultPoint::getSrm1Results, this::sum).mapToInt(Integer::intValue).sum(),
        "should have correct sum srm1");
    assertEquals(80 , collect(results, CIMLKResultPoint::getSrm2Results, this::sum).mapToInt(Integer::intValue).sum(),
        "should have correct sum srm2");
  }

  private Integer count(final EmissionResults result) {
    return result.getHashMap().size();
  }

  private Integer sum(final EmissionResults result) {
    return result.getHashMap().entrySet().stream().mapToInt(e -> e.getValue().intValue()).sum();
  }

  private <T> Stream<T> collect(final List<CIMLKResultPoint> results, final Function<CIMLKResultPoint, EmissionResults> supplier,
      final Function<EmissionResults, T> consumer) {
    return results.stream().map(supplier).map(consumer);
  }

  private static class Builder<T extends AeriusResultPoint> {
    final List<T> list = new ArrayList<>();

    Builder<T> add(final int id, final int x, final int y, final double nox, final double no2) {
      final CIMLKResultPoint point = new CIMLKResultPoint(new CIMLKCalculationPoint(id, "Some ID " + id, x, y));

      if (nox > 0) {
        point.getEmissionResults().put(EmissionResultKey.NOX_CONCENTRATION, nox);
      }
      if (no2 > 0) {
        point.getEmissionResults().put(EmissionResultKey.NO2_CONCENTRATION, no2);
      }
      list.add((T) point);
      return this;
    }

    List<T> build() {
      return list;
    }
  }
}
