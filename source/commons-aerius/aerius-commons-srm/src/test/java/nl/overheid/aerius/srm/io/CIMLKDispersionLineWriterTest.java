/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLine;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRoadProfile;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKTreeProfile;

/**
 * Test class for {@link CIMLKDispersionLineWriter}.
 */
class CIMLKDispersionLineWriterTest {

  @Test
  void testNormal() throws IOException {
    final CIMLKDispersionLineWriter resultWriter = new CIMLKDispersionLineWriter();

    final File outputFile = new File(Files.createTempDirectory("CIMLKDispersionLineWriterTest").toFile(), "normalTestFile.csv");

    final List<CIMLKDispersionLineFeature> lines = new ArrayList<>();
    lines.add(testCase(35));

    resultWriter.write(outputFile, lines);

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_dispersionlines.csv"));

    assertEquals(expected, result, "Result for normal CIMLK points export");
  }

  @Test
  void testWriteSeparateActions() throws IOException {
    final CIMLKDispersionLineWriter resultWriter = new CIMLKDispersionLineWriter();

    final File outputFile = new File(Files.createTempDirectory("CIMLKDispersionLineWriterTest").toFile(), "extendedTestFile.csv");

    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeHeader(writer);
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(77));
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      final CIMLKDispersionLineFeature feature = testCase(300);
      final LineString geometry = new LineString();
      geometry.setCoordinates(new double[][] {{1.0231, 23.0234}, {52.1207, 94.3304}});
      feature.setGeometry(geometry);
      resultWriter.writeRow(writer, feature);
    }

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_cimlk_dispersionlines_separate_writing.csv"));

    assertEquals(expected, result, "Result for separate writing CIMLK points export");
  }

  private static CIMLKDispersionLineFeature testCase(final int id) {
    final CIMLKDispersionLine dispersionLine = new CIMLKDispersionLine();
    dispersionLine.setCalculationPointGmlId("PREFIX." + id);
    dispersionLine.setRoadGmlId("PREFIX." + (id + 1000));
    dispersionLine.setLabel("Mooi label voor " + id);
    dispersionLine.setDescription("Mooie lijn tussen weg en rekenpunt");
    dispersionLine.setRoadProfile(CIMLKRoadProfile.NARROW_STREET_CANYON);
    dispersionLine.setTreeProfile(CIMLKTreeProfile.SEPARATED);

    if (id % 2 == 1) {
      dispersionLine.setJurisdictionId(4);
    }

    final CIMLKDispersionLineFeature feature = new CIMLKDispersionLineFeature();
    feature.setProperties(dispersionLine);
    return feature;
  }

}
