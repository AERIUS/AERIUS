/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Tests reading a CIMLK file and writing it back to a file.
 */
class CIMLKRoundtripTest {

  private static final int DEFAULT_SCALE = 4;
  private static final String DEFAULT_TEST_TARGET = "normalTestFile.csv";
  private static final String SRM1_TEST_FILE = "srm1_roads_example.csv";
  private static final String SRM2_TEST_FILE = "srm2_roads_example.csv";
  private static final String CALCULATION_POINT_TEST_FILE = "calculation_point_example.csv";
  private static final String CORRECTION_TEST_FILE = "correction_example.csv";
  private static final String DISPERSION_LINE_TEST_FILE = "dispersion_line_optionals_example.csv";
  private static final String MEASURE_TEST_FILE = "measure_example.csv";

  private SectorCategories categories;

  @BeforeEach
  void setUp() throws Exception {
    categories = new TestDomain().getCategories();
  }

  @Test
  void testSrm1() throws Exception {
    final CIMLKSRM1RoadFileReader reader = new CIMLKSRM1RoadFileReader(categories);
    final CIMLKSRM1RoadWriter writer = new CIMLKSRM1RoadWriter(DEFAULT_SCALE);

    final File outputFile = newFile();

    final List<EmissionSourceFeature> roads = getObjects(SRM1_TEST_FILE, reader);
    writer.write(outputFile, new ArrayList<>(roads));

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile(SRM1_TEST_FILE));

    assertEquals(expected, result, "Result for SRM1 roundtrip");
  }

  @Test
  void testSrm2() throws Exception {
    final CIMLKSRM2RoadFileReader reader = new CIMLKSRM2RoadFileReader(categories);
    final CIMLKSRM2RoadWriter writer = new CIMLKSRM2RoadWriter(DEFAULT_SCALE);

    final File outputFile = newFile();

    final List<EmissionSourceFeature> roads = getObjects(SRM2_TEST_FILE, reader);
    writer.write(outputFile, new ArrayList<>(roads));

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile(SRM2_TEST_FILE));

    assertEquals(expected, result, "Result for SRM2 roundtrip");
  }

  @Test
  void testCalculationPoint() throws Exception {
    final CIMLKCalculationPointFileReader reader = new CIMLKCalculationPointFileReader();
    final CIMLKCalculationPointWriter writer = new CIMLKCalculationPointWriter();

    final File outputFile = newFile();

    final List<CalculationPointFeature> points = getObjects(CALCULATION_POINT_TEST_FILE, reader);
    writer.write(outputFile, new ArrayList<>(points));

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile(CALCULATION_POINT_TEST_FILE));

    assertEquals(expected, result, "Result for calculation point roundtrip");
  }

  @Test
  void testCorrection() throws Exception {
    final CIMLKCorrectionFileReader reader = new CIMLKCorrectionFileReader();
    final CIMLKCorrectionWriter writer = new CIMLKCorrectionWriter(DEFAULT_SCALE);

    final File outputFile = newFile();

    final List<CIMLKCorrection> corrections = getObjects(CORRECTION_TEST_FILE, reader);
    writer.write(outputFile, corrections);

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile(CORRECTION_TEST_FILE));

    assertEquals(expected, result, "Result for correction roundtrip");
  }

  @Test
  void testDispersionLine() throws Exception {
    final CIMLKDispersionLineFileReader reader = new CIMLKDispersionLineFileReader();
    final CIMLKDispersionLineWriter writer = new CIMLKDispersionLineWriter();

    final File outputFile = newFile();

    final List<CIMLKDispersionLineFeature> dispersionLines = getObjects(DISPERSION_LINE_TEST_FILE, reader);
    writer.write(outputFile, dispersionLines);

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile(DISPERSION_LINE_TEST_FILE));

    assertEquals(expected, result, "Result for dispersion line roundtrip");
  }

  @Test
  void testMeasure() throws Exception {
    final CIMLKMeasureFileReader reader = new CIMLKMeasureFileReader();
    final CIMLKMeasureWriter writer = new CIMLKMeasureWriter(DEFAULT_SCALE);

    final File outputFile = newFile();

    final List<CIMLKMeasureFeature> measures = getObjects(MEASURE_TEST_FILE, reader);
    writer.write(outputFile, measures);

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile(MEASURE_TEST_FILE));

    assertEquals(expected, result, "Result for measure roundtrip");
  }

  private <T> List<T> getObjects(final String fileName, final AbstractCIMLKFileReader<T> reader) throws Exception {
    try (InputStream inputStream = AssertCSV.getFileInputStream(fileName);
        InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {
      final String header = bufferedReader.readLine();
      final AeriusException parsingHeaderException = reader.parseHeader(header);
      assertNull(parsingHeaderException, "Header should be correctly parsed");

      final LineReaderResult<T> result = reader.readObjects(bufferedReader);

      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
      return result.getObjects();
    }
  }

  private File newFile() throws IOException {
    return new File(Files.createTempDirectory("CIMLKRoundtripTest").toFile(), DEFAULT_TEST_TARGET);
  }
}
