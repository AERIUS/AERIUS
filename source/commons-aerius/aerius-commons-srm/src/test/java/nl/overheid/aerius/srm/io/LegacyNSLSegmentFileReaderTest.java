/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.Vehicles;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link LegacyNSLSegmentFileReader}.
 */
class LegacyNSLSegmentFileReaderTest {

  private static final String SPECIFIC_TEST_FILE = "specific_test_case.csv";
  private final LegacyNSLImportReader reader = new LegacyNSLImportReader();
  private SectorCategories categories;

  @BeforeEach
  void setUp() throws Exception {
    categories = new TestDomain().getCategories();
  }

  @ParameterizedTest
  @CsvSource({"legacy_srm2_roads_example.csv, 160", "line_at_first_column.csv, 3", "geometry_3d.csv, 2"})
  void testReader(final String fileName, final int numberOfResults) throws Exception {
    final File file = new File(getClass().getResource(fileName).getFile());

    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportParcel results = new ImportParcel();
      reader.read(fileName, inputStream, categories, null, results);

      if (!results.getExceptions().isEmpty()) {
        throw results.getExceptions().get(0);
      }
      final List<EmissionSourceFeature> sources = results.getSituation().getEmissionSourcesList();
      assertEquals(numberOfResults, sources.size(), "Count nr. of roads");

      for (final EmissionSourceFeature feature : sources) {
        assertEquals(GeometryType.LINESTRING, feature.getGeometry().type(), "Road geom is line");
        //for some reason, the 121th and 129th rows in the csv have no intensities at all...
        final EmissionSource source = feature.getProperties();
        if (!"-1312707".equals(source.getLabel()) && !"-1312716".equals(source.getLabel())) {
          assertNotEquals(0.0, source.getEmissions().get(Substance.NOX), "Emission for source " + source);
          assertTrue(feature.getGeometry() instanceof LineString, "Geometry should be a linestring");
          final LineString lineString = (LineString) feature.getGeometry();
          assertTrue(lineString.getCoordinates().length > 1, "Geometry should contain at least 2 coordinates for source " + source.getLabel());
          for (final double[] coordinate : lineString.getCoordinates()) {
            assertTrue(coordinate.length == 2, "Geometry should contain both x and y for source " + source.getLabel());
          }
        }
      }
    }
  }

  @Test
  void testSpecificCase() throws Exception {
    final String fileName = SPECIFIC_TEST_FILE;
    final File file = new File(getClass().getResource(fileName).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportParcel results = new ImportParcel();
      reader.read(fileName, inputStream, categories, null, results);

      if (!results.getExceptions().isEmpty()) {
        throw results.getExceptions().get(0);
      }
      final List<EmissionSourceFeature> esl = results.getSituation().getEmissionSourcesList();
      assertEquals(3, esl.size(), "Count nr. of roads");
      for (final EmissionSourceFeature source : esl) {
        if (source.getProperties().getGmlId().equals("ES.303")) {
          assertTrue(source.getProperties() instanceof SRM1RoadEmissionSource, "Should be instance of SRM1RoadEmissionSource");
        } else {
          assertTrue(source.getProperties() instanceof SRM2RoadEmissionSource, "Should be instance of SRM2RoadEmissionSource");
        }
      }
      validateFirstSource((SRM2RoadEmissionSource) esl.get(0).getProperties());
      validateSecondSource((SRM2RoadEmissionSource) esl.get(1).getProperties());
      validateThirdSource((SRM1RoadEmissionSource) esl.get(2).getProperties());

    }
  }

  private void validateFirstSource(final SRM2RoadEmissionSource source) {
    assertEquals("10101-101", source.getLabel(), "Label");
    assertEquals(TestDomain.ROAD_SECTOR, source.getSectorId(), "Sector ID");
    assertNull(source.getBarrierRight(), "Shouldn't have a right barrier");
    assertNotNull(source.getBarrierLeft(), "Should have a left barrier");
    validateBarrier(source.getBarrierLeft(), 5.0, 3.0);
    assertEquals(0.0, source.getElevationHeight(), 1E-4, "Elevation height");
    assertEquals(RoadElevation.NORMAL, source.getElevation(), "Elevation type");
    assertEquals(1.0, source.getTunnelFactor(), 1E-4, "tunnel factor");
    assertEquals("NON_URBAN_ROAD_NATIONAL", source.getRoadTypeCode(), "road type");
    assertEquals(1, source.getSubSources().size(), "Number of subsources");
    final Vehicles vehicles = source.getSubSources().get(0);
    validateSubSource(vehicles, VehicleType.LIGHT_TRAFFIC, 5000, 0.1, 120, false, RoadType.NON_URBAN_ROAD);
    validateSubSource(vehicles, VehicleType.NORMAL_FREIGHT, 200, 0.0, 120, false, RoadType.NON_URBAN_ROAD);
    validateSubSource(vehicles, VehicleType.HEAVY_FREIGHT, 150, 0.0, 120, false, RoadType.NON_URBAN_ROAD);
    validateSubSource(vehicles, VehicleType.AUTO_BUS, 75, 0.3, 120, false, RoadType.NON_URBAN_ROAD);
  }

  private void validateSecondSource(final SRM2RoadEmissionSource source) {
    assertEquals("20202-202", source.getLabel(), "Label");
    assertEquals(TestDomain.ROAD_SECTOR, source.getSectorId(), "Sector ID");
    assertNotNull(source.getBarrierRight(), "Should have a right barrier");
    assertNull(source.getBarrierLeft(), "Shouldn't have a left barrier");
    validateBarrier(source.getBarrierRight(), 4.0, 6.0);
    assertEquals(1.0, source.getElevationHeight(), 1E-4, "Elevation height");
    assertEquals(RoadElevation.STEEP_DYKE, source.getElevation(), "Elevation type");
    assertEquals(1.2, source.getTunnelFactor(), 1E-4, "tunnel factor");
    assertEquals("FREEWAY", source.getRoadTypeCode());
    //2 sources, as the dynamic vehicle column is added as a 2nd light_traffic source with different speed.
    assertEquals(2, source.getSubSources().size(), "Number of subsources");
    final Vehicles vehicles = source.getSubSources().get(0);
    final Vehicles dynamicVehicles = source.getSubSources().get(1);
    validateSubSource(vehicles, VehicleType.LIGHT_TRAFFIC, 10000, 0.3, 100, true, RoadType.FREEWAY);
    validateSubSource(dynamicVehicles, VehicleType.LIGHT_TRAFFIC, 2000, 0.0, 130, true, RoadType.FREEWAY);
    validateSubSource(vehicles, VehicleType.NORMAL_FREIGHT, 300, 0.2, 100, true, RoadType.FREEWAY);
    validateSubSource(vehicles, VehicleType.HEAVY_FREIGHT, 250, 0.0, 100, true, RoadType.FREEWAY);
    validateSubSource(vehicles, VehicleType.AUTO_BUS, 175, 0.0, 100, true, RoadType.FREEWAY);
  }

  private void validateThirdSource(final SRM1RoadEmissionSource source) {
    assertEquals("30303-303", source.getLabel(), "Label");
    assertEquals(TestDomain.ROAD_SECTOR, source.getSectorId(), "Sector ID");
    assertEquals(1.0, source.getTunnelFactor(), 1E-4, "tunnel factor");
    assertEquals("URBAN_ROAD_FREE_FLOW", source.getRoadTypeCode(), "Road type");
    assertEquals(1, source.getSubSources().size(), "Number of subsources");
    final Vehicles vehicles = source.getSubSources().get(0);
    validateSubSource(vehicles, VehicleType.LIGHT_TRAFFIC, 5000, 0.0, 50, false, RoadType.URBAN_ROAD);
    validateSubSource(vehicles, VehicleType.NORMAL_FREIGHT, 400, 0.0, 50, false, RoadType.URBAN_ROAD);
    validateSubSource(vehicles, VehicleType.HEAVY_FREIGHT, 350, 0.0, 50, false, RoadType.URBAN_ROAD);
    validateSubSource(vehicles, VehicleType.AUTO_BUS, 275, 0.0, 50, false, RoadType.URBAN_ROAD);
  }

  private void validateBarrier(final SRM2RoadSideBarrier barrier, final double expectedDistance, final double expectedHeight) {
    assertEquals(SRM2RoadSideBarrierType.SCREEN, barrier.getBarrierType(), "Barrier type");
    assertEquals(expectedDistance, barrier.getDistance(), 1E-4, "Barrier distance");
    assertEquals(expectedHeight, barrier.getHeight(), 1E-4, "Barrier height");
  }

  private void validateSubSource(final Vehicles vehicleEmissions, final VehicleType vehicleType, final double expectedVehicles,
      final double expectedStagnationFraction, final int expectedMaxSpeed, final boolean expectedStrictEnforcement, final RoadType roadType) {
    assertTrue(vehicleEmissions instanceof StandardVehicles, vehicleEmissions + ": Should be standard emissions");
    final StandardVehicles standardEmissions = (StandardVehicles) vehicleEmissions;
    assertTrue(standardEmissions.getValuesPerVehicleTypes().containsKey(vehicleType.getStandardVehicleCode()), vehicleEmissions + ": vehicle type");
    final ValuesPerVehicleType valuesPerVehicleType = standardEmissions.getValuesPerVehicleTypes().get(vehicleType.getStandardVehicleCode());
    assertEquals(expectedVehicles, vehicleEmissions.getTimeUnit().toUnit(valuesPerVehicleType.getVehiclesPerTimeUnit(), TimeUnit.DAY),
        1E-4, vehicleEmissions + ": Nr of vehicles");
    if (roadType == RoadType.FREEWAY) {
      assertEquals(expectedMaxSpeed,
          standardEmissions.getMaximumSpeed().intValue(), vehicleEmissions + ": max speed");
      assertEquals(expectedStrictEnforcement,
          standardEmissions.getStrictEnforcement(), vehicleEmissions + ": strict enforcement");
    } else {
      assertEquals(expectedMaxSpeed, standardEmissions.getMaximumSpeed().intValue(), vehicleEmissions + ": max speed");
      assertFalse(standardEmissions.getStrictEnforcement(), vehicleEmissions + ": strict enforcement");
    }
    assertEquals(expectedStagnationFraction, valuesPerVehicleType.getStagnationFraction(), 1E-4, vehicleEmissions + ": stagnation factor");
  }

  @Test
  void testIncorrectColumnHeader() throws Exception {
    final String fileName = "incorrect_column_header.csv";
    final File file = new File(getClass().getResource(fileName).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportParcel results = new ImportParcel();
      reader.read(fileName, inputStream, categories, null, results);

      if (results.getExceptions().size() == 1) {
        if (results.getExceptions().get(0) instanceof AeriusException) {
          final AeriusException ae = results.getExceptions().get(0);
          assertEquals(AeriusExceptionReason.SRM2_MISSING_COLUMN_HEADER, ae.getReason(), "Reason for exception");
          assertEquals(1, ae.getArgs().length, "Size of arguments");
          assertEquals("SEGMENT_ID, GEOMET_WKT", ae.getArgs()[0], "Argument for exception SRM2_MISSING_COLUMN_HEADER");
        } else {
          throw results.getExceptions().get(0);
        }
      } else {
        fail("Expected 1 exception, got " + results.getExceptions().size());
      }
    }
  }

  @Test
  void testMissingWKT() throws Exception {
    final String fileName = "incorrect_wkt_value.csv";
    final File file = new File(getClass().getResource(fileName).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportParcel results = new ImportParcel();
      reader.read(fileName, inputStream, categories, null, results);

      if (results.getExceptions().size() == 2) {
        if (results.getExceptions().get(0) instanceof AeriusException) {
          final AeriusException ae = results.getExceptions().get(0);
          assertEquals(AeriusExceptionReason.SRM2_INCORRECT_EXPECTED_VALUE, ae.getReason(), "Reason for exception");
          assertEquals(2, ae.getArgs().length, "Size of arguments");
          assertEquals("2", ae.getArgs()[0], "Argument for exception SRM2_INCORRECT_EXPECTED_VALUE count");
          assertEquals("GEOMET_WKT", ae.getArgs()[1], "Argument for exception SRM2_INCORRECT_EXPECTED_VALUE");
        } else {
          throw results.getExceptions().get(0);
        }
        if (results.getExceptions().get(1) instanceof AeriusException) {
          final AeriusException ae = results.getExceptions().get(1);
          assertEquals(AeriusExceptionReason.SRM2_INCORRECT_WKT_VALUE, ae.getReason(), "Reason for exception");
          assertEquals(2, ae.getArgs().length, "Size of arguments");
          assertEquals("3", ae.getArgs()[0], "Argument for exception SRM2_INCORRECT_WKT_VALUE count");
          assertEquals("POINT(130829 459381)", ae.getArgs()[1], "Argument for exception SRM2_INCORRECT_WKT_VALUE");
        } else {
          throw results.getExceptions().get(1);
        }
      } else {
        fail("Expected 2 exceptions, got " + results.getExceptions().size());
      }
    }
  }

  @Test
  void testParseError() throws Exception {
    final String fileName = "incorrect_parse_value.csv";
    final File file = new File(getClass().getResource(fileName).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportParcel results = new ImportParcel();
      reader.read(fileName, inputStream, categories, null, results);

      if (results.getExceptions().size() == 2) {
        if (results.getExceptions().get(0) instanceof AeriusException) {
          final AeriusException ae = results.getExceptions().get(0);
          assertEquals(AeriusExceptionReason.IO_EXCEPTION_NUMBER_FORMAT, ae.getReason(), "Reason for exception");
          assertEquals(3, ae.getArgs().length, "Size of arguments");
          assertEquals("2", ae.getArgs()[0], "Argument for exception IO_EXCEPTION_NUMBER_FORMAT linenr");
          assertEquals("MAXSNELH_P", ae.getArgs()[1], "Argument for exception IO_EXCEPTION_NUMBER_FORMAT column");
          assertEquals("geen_snelheid", ae.getArgs()[2], "Argument for exception IO_EXCEPTION_NUMBER_FORMAT value");
        } else {
          throw results.getExceptions().get(0);
        }
        if (results.getExceptions().get(1) instanceof AeriusException) {
          final AeriusException ae = results.getExceptions().get(1);
          assertEquals(AeriusExceptionReason.IO_EXCEPTION_NOT_ENOUGH_FIELDS, ae.getReason(), "Reason for exception");
          assertEquals(1, ae.getArgs().length, "Size of arguments");
          assertEquals("4", ae.getArgs()[0], "Argument for exception IO_EXCEPTION_NOT_ENOUGH_FIELDS linenr");
        } else {
          throw results.getExceptions().get(1);
        }
      } else {
        fail("Expected 3 exception, got " + results.getExceptions().size());
      }
    }
  }
}
