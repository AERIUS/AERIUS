/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import nl.overheid.aerius.srm.io.CIMLKResult;

/**
 * Data object for reading references results for OwN2000 SRM2 calculation tests.
 */
class OwN2000Result extends CIMLKResult {

  private int sectorId;
  private double srm2NOxDeposition;
  private double srm2NH3Deposition;

  public int getSectorId() {
    return sectorId;
  }

  public void setSectorId(final int sectorId) {
    this.sectorId = sectorId;
  }

  public double getSrm2NOxDeposition() {
    return srm2NOxDeposition;
  }

  public void setSrm2NOxDeposition(final double srm2nOxDeposition) {
    srm2NOxDeposition = srm2nOxDeposition;
  }

  public double getSrm2NH3Deposition() {
    return srm2NH3Deposition;
  }

  public void setSrm2NH3Deposition(final double srm2nh3Deposition) {
    srm2NH3Deposition = srm2nh3Deposition;
  }
}
