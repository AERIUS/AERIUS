/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.srm.MockAeriusSRMVersion;

/**
 * Test class for load {@link LandUseData} with {@link LandUseDataFileReader}.
 */
class LandUseDataFileReaderTest {
  private static final String PREFIX = MockAeriusSRMVersion.PRESRM_LU + "/";

  @Test
  void testReadFile() throws IOException {
    MapData mapData = null;

    try (final InputStream is = LandUseDataFileReaderTest.class.getResourceAsStream(PREFIX + "z0nl1000-test.asc")) {
      mapData = LandUseDataFileReader.read(is);
    }

    assertNotNull(mapData, "Map data should not be null");
    final int width = mapData.getWidth();
    assertEquals(2, mapData.getHeight(), "Invalid datagrid height.");
    assertEquals(291, width, "Invalid datagrid width.");
    assertEquals(0, mapData.getOffsetX(), 0.1, "Invalid offset x.");
    assertEquals(300000, mapData.getOffsetY(), 0.1, "Invalid offset y.");
    assertEquals(1000, mapData.getDiameter(), 0.1, "Invalid diameter.");
    assertEquals(-1.0, mapData.getNoDataValue(), 0.0001, "Invalid default value.");
    assertEquals(0.2231, getCell(mapData, width, 290, 1), 0.0001, "Invalid value for cell [290, 1].");
    assertEquals(0.0082, getCell(mapData, width, 263, 0), 0.0001, "Invalid value for cell [263, 0].");
  }

  @Test
  void testUnexpectedEOF() throws IOException {
    try (final InputStream is = LandUseDataFileReaderTest.class.getResourceAsStream(PREFIX + "z0nl1000-test-unexpected-EOF.asc")) {
      assertThrows(IOException.class, () -> LandUseDataFileReader.read(is), "Expected exception on Unexpected OEF");
    }
  }

  @Test
  void testInvalidDataLine() throws IOException {
    try (final InputStream is = LandUseDataFileReaderTest.class.getResourceAsStream(PREFIX + "z0nl1000-test-invalid-data-line.asc")) {
      assertThrows(IOException.class, () -> LandUseDataFileReader.read(is), "Expected exception on invalid data");
    }
  }

  // x and y are 0 based indices.
  private double getCell(final MapData mapData, final int width, final int x, final int y) {
    return mapData.getData().get(y * width + x);
  }
}
