/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMonitorSubstance;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRejectionGrounds;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link CIMLKCalculationPointFileReader}.
 */
class CIMLKCalculationPointFileReaderTest {

  private static final String SPECIFIC_TEST_FILE = "calculation_point_example.csv";

  @Test
  void testSpecificCase() throws Exception {
    final CIMLKCalculationPointFileReader reader = new CIMLKCalculationPointFileReader();
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {

      final String header = bufferedReader.readLine();
      final AeriusException parsingHeaderException = reader.parseHeader(header);

      assertNull(parsingHeaderException, "Header should be correctly parsed");

      final LineReaderResult<CalculationPointFeature> result = reader.readObjects(bufferedReader);

      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
      final List<CalculationPointFeature> points = result.getObjects();
      validateResults(points);
    }
  }

  @Test
  void testSpecificCaseWithMainImporter() throws Exception {
    final LegacyNSLImportReader reader = new LegacyNSLImportReader();
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {

      final SectorCategories categories = Mockito.mock(SectorCategories.class);
      final Substance substance = Substance.NOX;
      final ImportParcel importResult = new ImportParcel();

      reader.read("some name", inputStream, categories, substance, importResult);

      if (!importResult.getExceptions().isEmpty()) {
        throw importResult.getExceptions().get(0);
      }
      final List<CalculationPointFeature> calculationPointList = importResult.getCalculationPointsList();
      validateResults(calculationPointList);
    }
  }

  private void validateResults(final List<CalculationPointFeature> points) throws AeriusException {
    assertEquals(1, points.size(), "Count nr. of points");
    final CalculationPointFeature feature = points.get(0);
    assertEquals("PREFIX.32", feature.getId(), "Calculationpoint id");
    assertEquals(130950, feature.getGeometry().getX(), 1E-10, "X coord");
    assertEquals(459330, feature.getGeometry().getY(), 1E-10, "Y coord");
    assertTrue(points.get(0).getProperties() instanceof CIMLKCalculationPoint, "point correct type");
    final CIMLKCalculationPoint point = (CIMLKCalculationPoint) points.get(0).getProperties();
    assertEquals("PREFIX.32", point.getGmlId(), "Calculationpoint GML id");
    assertEquals(Integer.valueOf(39), point.getJurisdictionId(), "Jurisdiction id");
    assertEquals("een leuk label", point.getLabel(), "Label");
    assertEquals(CIMLKMonitorSubstance.ALL, point.getMonitorSubstance(), "Monitor substance");
    assertEquals(CIMLKRejectionGrounds.NONE, point.getRejectionGrounds(), "Rejection ground");
    assertEquals("Dit kan iets uitgebreider", point.getDescription(), "Description");

  }

}
