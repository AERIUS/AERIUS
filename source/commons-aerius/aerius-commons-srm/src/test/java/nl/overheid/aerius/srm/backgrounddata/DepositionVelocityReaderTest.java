/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.backgrounddata.DepositionVelocityReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Test class for {@link DepositionVelocityReader}.
 */
class DepositionVelocityReaderTest {

  private static final String DEPOSITION_VELOCITY_FILE = MockAeriusSRMVersion.DEPOSITION_VELOCITY_FILE;

  @Test
  void testReadFile() throws IOException {
    final Map<Substance, SRMDepositionVelocityMap> data;

    try (final InputStream is = DepositionVelocityReaderTest.class.getResourceAsStream(
        DEPOSITION_VELOCITY_FILE + "/" + DEPOSITION_VELOCITY_FILE + ".csv")) {
      data = DepositionVelocityReader.read(is, SRMConstants.HEX_HOR, SRMConstants.getReceptorUtil(), SRMDepositionVelocityMap.class);
    }

    assertNotNull(data, "Map should not be null.");
    assertEquals(2, data.size(), "Unexpected number of substances.");

    final SRMDepositionVelocityMap depositionVelocitiesNO2 = data.get(Substance.NO2);

    assertNotNull(depositionVelocitiesNO2, "NOX map should not be null.");
    assertEquals(1694571, depositionVelocitiesNO2.size(), "Invalid number of receptors.");

    final Double depositionVelocityNO2 = depositionVelocitiesNO2.getDepositionVelocityById(801827);
    assertNotNull(depositionVelocityNO2, "NOX value should not be null.");
    assertEquals(0.00116458, depositionVelocityNO2, 0, "Invalid NOX value.");

    final SRMDepositionVelocityMap depositionVelocitiesNH3 = data.get(Substance.NH3);

    assertNotNull(depositionVelocitiesNO2, "NH3 map should not be null.");
    assertEquals(1694571, depositionVelocitiesNH3.size(), "Invalid number of receptors.");

    final Double depositionVelocityNH3 = depositionVelocitiesNH3.getDepositionVelocityById(801827);
    assertNotNull(depositionVelocityNH3, "NH3 value should not be null.");
    assertEquals(0.00834448, depositionVelocityNH3, 0, "Invalid NH3 value.");
  }
}
