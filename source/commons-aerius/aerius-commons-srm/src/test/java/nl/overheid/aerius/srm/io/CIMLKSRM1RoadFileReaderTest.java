/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map.Entry;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadManager;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.Vehicles;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link CIMLKSRM1RoadFileReader}.
 */
class CIMLKSRM1RoadFileReaderTest {

  private static final String SPECIFIC_TEST_FILE = "srm1_roads_example.csv";

  private SectorCategories categories;

  @BeforeEach
  void setUp() throws Exception {
    categories = new TestDomain().getCategories();
  }

  @Test
  void testSpecificCase() throws Exception {
    final CIMLKSRM1RoadFileReader reader = new CIMLKSRM1RoadFileReader(categories);
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {

      final String header = bufferedReader.readLine();
      final AeriusException parsingHeaderException = reader.parseHeader(header);

      assertNull(parsingHeaderException, "Header should be correctly parsed");

      final LineReaderResult<EmissionSourceFeature> result = reader.readObjects(bufferedReader);

      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
      final List<EmissionSourceFeature> roads = result.getObjects();
      validateResults(roads);
    }
  }

  @Test
  void testSpecificCaseWithMainImporter() throws Exception {
    final LegacyNSLImportReader reader = new LegacyNSLImportReader();
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {

      final Substance substance = Substance.NOX;
      final ImportParcel importResult = new ImportParcel();

      reader.read("some name", inputStream, categories, substance, importResult);

      if (!importResult.getExceptions().isEmpty()) {
        throw importResult.getExceptions().get(0);
      }
      final List<EmissionSourceFeature> emissionSourceList = importResult.getSituation().getEmissionSourcesList();
      validateResults(emissionSourceList);
    }
  }

  private void validateResults(final List<EmissionSourceFeature> roads) throws AeriusException {
    assertEquals(2, roads.size(), "Count nr. of roads");
    final EmissionSourceFeature feature = roads.get(0);
    assertTrue(feature.getProperties() instanceof SRM1RoadEmissionSource, "road correct type");
    final SRM1RoadEmissionSource road = (SRM1RoadEmissionSource) roads.get(0).getProperties();
    assertEquals("PREFIX.9323", feature.getId(), "Road id");
    assertEquals("PREFIX.9323", road.getGmlId(), "Road segment id");
    assertEquals("Leuke label", road.getLabel(), "Label");
    assertEquals(Integer.valueOf(238), road.getJurisdictionId(), "Jurisdiction id");
    assertEquals(RoadManager.STATE, road.getRoadManager(), "Road manager");
    assertEquals("Bedenk een goede omschrijving", road.getDescription(), "description");

    assertEquals(RoadSpeedType.URBAN_TRAFFIC_STAGNATING.getRoadTypeCode(), road.getRoadTypeCode(), "Speed profile");
    assertEquals(TestDomain.ROAD_SECTOR, road.getSectorId(), "sector");

    assertEquals(1, road.getTunnelFactor(), 1E-10, "Tunnel factor");

    assertEquals(GeometryType.LINESTRING, feature.getGeometry().type(), "Road segment type");
    assertArrayEquals(new double[][] {{130835, 459390}, {130800, 459200}}, ((LineString) feature.getGeometry()).getCoordinates(),
        "Road segment geometry");

    validateTraffic(road.getSubSources());
  }

  private void validateTraffic(final List<Vehicles> trafficSources) throws AeriusException {
    assertEquals(1, trafficSources.size(), "Road traffic sources");
    for (final Vehicles vehicleEmissions : trafficSources) {
      assertEquals(TimeUnit.DAY, vehicleEmissions.getTimeUnit(), "Vehicles per year");
      assertTrue(vehicleEmissions instanceof StandardVehicles, "road correct type");
      final StandardVehicles standardEmissions = (StandardVehicles) vehicleEmissions;
      assertEquals(4, standardEmissions.getValuesPerVehicleTypes().size(), "Vehicle types");
      assertEquals(0, standardEmissions.getMaximumSpeed().intValue(), "Maximum speed");
      for (final Entry<String, ValuesPerVehicleType> entry : standardEmissions.getValuesPerVehicleTypes().entrySet()) {
        if (VehicleType.LIGHT_TRAFFIC.getStandardVehicleCode().equalsIgnoreCase(entry.getKey())) {
          assertEquals(844610.0, TimeUnit.DAY.toUnit(entry.getValue().getVehiclesPerTimeUnit(), TimeUnit.YEAR), 1E-10, "Vehicles per year");
          assertEquals(0.23, entry.getValue().getStagnationFraction(), 1E-10, "Stagnation fraction");
        } else if (VehicleType.NORMAL_FREIGHT.getStandardVehicleCode().equalsIgnoreCase(entry.getKey())) {
          assertEquals(0, standardEmissions.getMaximumSpeed().intValue(), "Maximum speed");
          assertEquals(74095, TimeUnit.DAY.toUnit(entry.getValue().getVehiclesPerTimeUnit(), TimeUnit.YEAR), 1E-10, "Vehicles per year");
          assertEquals(0.31, entry.getValue().getStagnationFraction(), 1E-10, "Stagnation fraction");
        } else if (VehicleType.HEAVY_FREIGHT.getStandardVehicleCode().equalsIgnoreCase(entry.getKey())) {
          assertEquals(0, standardEmissions.getMaximumSpeed().intValue(), "Maximum speed");
          assertEquals(65700, TimeUnit.DAY.toUnit(entry.getValue().getVehiclesPerTimeUnit(), TimeUnit.YEAR), 1E-10, "Vehicles per year");
          assertEquals(0.33, entry.getValue().getStagnationFraction(), 1E-10, "Stagnation fraction");
        } else if (VehicleType.AUTO_BUS.getStandardVehicleCode().equalsIgnoreCase(entry.getKey())) {
          assertEquals(0, standardEmissions.getMaximumSpeed().intValue(), "Maximum speed");
          assertEquals(7300, TimeUnit.DAY.toUnit(entry.getValue().getVehiclesPerTimeUnit(), TimeUnit.YEAR), 1E-10, "Vehicles per year");
          assertEquals(0.1, entry.getValue().getStagnationFraction(), 1E-10, "Stagnation fraction");
        } else {
          fail("Unexpected vehicle type " + entry.getKey());
        }
      }
    }
  }

}
