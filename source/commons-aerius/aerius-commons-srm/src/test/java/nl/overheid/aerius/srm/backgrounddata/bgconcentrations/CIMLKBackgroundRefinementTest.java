/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata.bgconcentrations;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.HasGridValue;
import nl.overheid.aerius.srm.backgrounddata.PointGrid;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentrationBuilder;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentrationBuilderTest;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;

/**
 * Test class to test if background values for CIMLK are correctly loaded.
 */
class CIMLKBackgroundRefinementTest {

  private static final int TEST_YEAR = 2018;
  private static final WindRoseConcentrationBuilder BUILDER = new WindRoseConcentrationBuilder();

  private static HasGridValue<WindRoseConcentration> grid;

  @BeforeAll
  static void beforeClass() throws IOException, AeriusException {
    final PreSRMData data = new PreSRMData();
    final AeriusSRMVersion aeriusVersion = MockAeriusSRMVersion.mockAeriusSRMVersionCIMLK();
    final File directory = new File(WindRoseConcentrationBuilderTest.class.getResource(aeriusVersion.getAeriusPreSrmVersion()).getFile());

    BUILDER.loadWindRoseConcentration(aeriusVersion, data, directory, SRMConstants.CIMLK_SRM2_CALCULATION_OPTIONS);
    grid = data.getWindRoseConcentrationGrid(aeriusVersion, new Meteo(TEST_YEAR), TEST_YEAR);
  }

  @Test
  void testIjmond() {
    final WindRoseConcentration wrc = grid.get(102211, 498534);
    assertEquals(36.943, wrc.getBackground(Substance.PM10), 0.0001, "PM10 ijmond background should match corrected value");
    assertEquals(13.956, wrc.getBackground(Substance.PM25), 0.0001, "PM25 ijmond background should match corrected value");
    assertEquals(0.598, wrc.getBackground(Substance.EC), 0.00001, "EC ijmond background should match corrected value");
  }

  @Test
  void testSchiphol() {
    final PointGrid<WindRoseConcentration> schipholGrid = ((CIMLKWindRoseConcentrationGrid) grid).getSchipholCorrections();
    assertEquals(100, schipholGrid.getDiameter(), "Schiphol diameter not as expected");
    final WindRoseConcentration wrc1 = grid.get(112253, 480004);
    assertEquals(27.849, wrc1.getBackground(Substance.NO2), 0.0001, "NO2 schiphol background should match corrected value");
    final WindRoseConcentration wrc2 = grid.get(110332, 480603);
    assertEquals(33.687, wrc2.getBackground(Substance.NO2), 0.0001, "NO2 schiphol background should match corrected value");
    final WindRoseConcentration wrc3 = grid.get(119582.201651247, 484184.300412812);
    assertEquals(26.16, wrc3.getGCN(Substance.NO2), 0.0001, "NO2 schiphol background should match corrected value");
    // Rounded x
    final WindRoseConcentration wrc4 = grid.get(126109, 481000);
    assertEquals(21.442, wrc4.getBackground(Substance.NO2), 0.0001, "NO2 schiphol background should match corrected value");
    final WindRoseConcentration wrc5b = grid.get(123900, 477726);
    assertEquals(17.25, wrc5b.getBackground(Substance.NO2), 0.0001, "NO2 schiphol background should match corrected value");
    final WindRoseConcentration wrc5 = grid.get(123999.99, 477726);
    assertEquals(17.25, wrc5.getBackground(Substance.NO2), 0.0001, "NO2 schiphol background should match corrected value");
    // Rounded y
    final WindRoseConcentration wrc6 = grid.get(119881, 481999.682259);
    assertEquals(21.314, wrc6.getBackground(Substance.NO2), 0.0001, "NO2 schiphol background should match corrected value");
    final WindRoseConcentration wrc7 = grid.get(119881, 482000);
    assertEquals(21.314, wrc7.getBackground(Substance.NO2), 0.0001, "NO2 schiphol background should match corrected value");
  }

  @Test
  void testBackground() {
    final WindRoseConcentration wrc1 = grid.get(94187.74, 444000.01);
    assertEquals(22.248, wrc1.getBackground(Substance.NO2), 0.0001, "NO2 background should match corrected value");
    final WindRoseConcentration wrc2 = grid.get(94187.74, 444000.004);
    assertEquals(22.248, wrc2.getBackground(Substance.NO2), 0.0001, "NO2 background should match corrected value");
  }
}
