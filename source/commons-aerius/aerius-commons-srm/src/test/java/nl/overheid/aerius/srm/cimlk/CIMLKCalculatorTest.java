/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.cimlk;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.srm.AbstractSRMTestBase;
import nl.overheid.aerius.srm.MockAeriusSRMVersion;
import nl.overheid.aerius.srm.io.CIMLKResult;
import nl.overheid.aerius.srm.io.CIMLKResultWriter;
import nl.overheid.aerius.srm2.conversion.SRM1SRMConverter;

/**
 * Test class for performing specific CIMLK roundtrip calculations.
 */
class CIMLKCalculatorTest extends AbstractSRMTestBase<CIMLKResult> {

  private static final int YEAR = 2018;
  private static final Substance[] SUBSTANCES = new Substance[] {Substance.NOX, Substance.NO2, Substance.PM10, Substance.PM25};
  private static final EnumSet<EmissionResultKey> EMISSION_RESULT_KEYS = EnumSet.of(
      EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NO2_CONCENTRATION, EmissionResultKey.NO2_DIRECT_CONCENTRATION,
      EmissionResultKey.PM10_CONCENTRATION, EmissionResultKey.PM25_CONCENTRATION, EmissionResultKey.NO2_EXCEEDANCE_HOURS,
      EmissionResultKey.PM10_EXCEEDANCE_DAYS);
  private static final Theme THEME = Theme.RBL;

  static List<Object[]> data() throws IOException {
    return dataItem(new File(CIMLKCalculatorTest.class.getResource(".").getFile()), ".",
        "segment", "receptor", "results", "1_correction");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testNSLCalculator(final String name, final String srcFile, final String receptorFile, final String resultFile, final String correctionsFile)
      throws Throwable {
    logger.info("Run test '{}'", name);
    init(name, srcFile, receptorFile, resultFile, correctionsFile, new SRM1SRMConverter());
    assertCalculate(THEME, YEAR, SUBSTANCES, EMISSION_RESULT_KEYS, MockAeriusSRMVersion.mockAeriusSRMVersionCIMLK(),
        SubReceptorCalculation.DISABLED);
  }

  @Override
  protected int assertResult(final AeriusResultPoint rp) {
    return 0;
  }

  @Override
  protected void assertSectorResults(final Map<Integer, List<AeriusResultPoint>> results) {
    final File calculatedResultsFile = calculatedResultsFile();

    try {
      final String calculatedResults = Files.readAllLines(calculatedResultsFile.toPath()).stream().collect(Collectors.joining());
      final String expectedResults = Files.readAllLines(new File(getClass().getResource(resultFile).toURI()).toPath())
          .stream().collect(Collectors.joining());

      assertEquals(expectedResults, calculatedResults, "Expected Results should be the same");
    } catch (final IOException | URISyntaxException e) {
      fail("Reading calculated results failed :" + e.getMessage());
    }
  }

  @Override
  protected void writeResultsToFile(final List<AeriusResultPoint> results) throws IOException {
    final File resultFile = calculatedResultsFile();

    new CIMLKResultWriter(10).write(resultFile, results, 2019, "Test-version", "Test-database");
    logger.info("File: {}", resultFile);
  }

  private File calculatedResultsFile() {
    return new File(getClass().getResource(name).getFile(), "calculatedResults.csv");
  }
}
