/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import nl.overheid.aerius.srm.version.AeriusSRMVersion;

/**
 * Mocks an {@link AeriusSRMVersion} object so we don't have to update the data in test resources every time the data version change.
 */
public final class MockAeriusSRMVersion {

  public static final String PRESRM_VERSION = "2.202-1";
  public static final String AERIUS_PRESRM_VERSION = "aerius-presrm-" + PRESRM_VERSION;
  public static final String PRESRM_LU = "presrm-lu";
  public static final String DEPOSITION_VELOCITY_FILE = "depositionvelocity-vd_zoomlevel-2_all";

  private MockAeriusSRMVersion() {
    // Util class
  }

  /**
   * Mocks {@link AeriusSRMVersion} that contains the same version as {@link AeriusSRMVersion#STABLE}.
   *
   * @return mocked {@link AeriusSRMVersion} object
   */
  public static AeriusSRMVersion mockAeriusSRMVersionStable() {
    return mockAeriusSRMVersion(AeriusSRMVersion.STABLE);
  }

  /**
   * Mocks {@link AeriusSRMVersion} that contains the same version as {@link AeriusSRMVersion#LATEST}.
   *
   * @return mocked {@link AeriusSRMVersion} object
   */
  public static AeriusSRMVersion mockAeriusSRMVersionLatest() {
    return mockAeriusSRMVersion(AeriusSRMVersion.LATEST);
  }

  /**
   * Mocks {@link AeriusSRMVersion} that contains the same version as {@link AeriusSRMVersion#RBL_LATEST}.
   *
   * @return mocked {@link AeriusSRMVersion} object
   */
  public static AeriusSRMVersion mockAeriusSRMVersionCIMLK() {
    return mockAeriusSRMVersion(AeriusSRMVersion.RBL_LATEST);
  }

  private static AeriusSRMVersion mockAeriusSRMVersion(final AeriusSRMVersion originalVersion) {
    final AeriusSRMVersion mock = mock(AeriusSRMVersion.class);
    doReturn(originalVersion.getVersion()).when(mock).getVersion();
    doReturn(originalVersion.getTheme()).when(mock).getTheme();
    doReturn("SRM2022").when(mock).getSrmConstantsVersion();
    doReturn(DEPOSITION_VELOCITY_FILE).when(mock).getDepositionVelocityVersion();
    doReturn(DEPOSITION_VELOCITY_FILE + ".csv").when(mock).getDepositionVelocityFileName();
    doReturn(PRESRM_LU).when(mock).getLandUseVersion();
    doReturn(AERIUS_PRESRM_VERSION).when(mock).getAeriusPreSrmVersion();
    doReturn(PRESRM_VERSION).when(mock).getPreSrmVersionNumber();
    doAnswer(a -> originalVersion.getMeteoForYear((int) a.getArguments()[0])).when(mock).getMeteoForYear(anyInt());
    doReturn("windvelden").when(mock).getWindveldenVersion();

    return mock;
  }
}
