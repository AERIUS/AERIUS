/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link LegacyNSLImportReader}.
 */
class LegacyNSLImportReaderTest {

  @Test
  void testSRM2Import() throws Exception {
    final String filename = "legacy_srm2_roads_example.csv";
    final File file = new File(getClass().getResource(filename).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportParcel result = new ImportParcel();
      new LegacyNSLImportReader().read(
          filename, inputStream, new TestDomain().getCategories(), null, result);

      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
      assertEquals(160, result.getSituation().getEmissionSourcesList().size(), "Count nr. of roads");
    }
  }
}
