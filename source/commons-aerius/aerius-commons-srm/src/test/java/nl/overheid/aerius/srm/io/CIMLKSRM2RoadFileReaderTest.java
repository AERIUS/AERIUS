/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map.Entry;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadManager;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.Vehicles;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link CIMLKSRM2RoadFileReader}.
 */
class CIMLKSRM2RoadFileReaderTest {

  private static final String SPECIFIC_TEST_FILE = "srm2_roads_example.csv";

  private SectorCategories categories;

  @BeforeEach
  void setUp() throws Exception {
    categories = new TestDomain().getCategories();
  }

  @Test
  void testSpecificCase() throws Exception {
    final CIMLKSRM2RoadFileReader reader = new CIMLKSRM2RoadFileReader(categories);
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {

      final String header = bufferedReader.readLine();
      final AeriusException parsingHeaderException = reader.parseHeader(header);

      assertNull(parsingHeaderException, "Header should be correctly parsed");

      final LineReaderResult<EmissionSourceFeature> result = reader.readObjects(bufferedReader);

      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
      final List<EmissionSourceFeature> roads = result.getObjects();
      validateResults(roads);
    }
  }

  @Test
  void testSpecificCaseWithMainImporter() throws Exception {
    final LegacyNSLImportReader reader = new LegacyNSLImportReader();
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {

      final Substance substance = Substance.NOX;
      final ImportParcel importResult = new ImportParcel();

      reader.read("some name", inputStream, categories, substance, importResult);

      if (!importResult.getExceptions().isEmpty()) {
        throw importResult.getExceptions().get(0);
      }
      final List<EmissionSourceFeature> emissionSourceList = importResult.getSituation().getEmissionSourcesList();
      validateResults(emissionSourceList);
    }
  }

  private void validateResults(final List<EmissionSourceFeature> roads) throws AeriusException {
    assertEquals(3, roads.size(), "Count nr. of roads");
    final EmissionSourceFeature firstFeature = roads.get(0);
    assertTrue(firstFeature.getProperties() instanceof SRM2RoadEmissionSource, "road correct type");
    final SRM2RoadEmissionSource firstRoad = (SRM2RoadEmissionSource) firstFeature.getProperties();
    assertEquals("PREFIX.9187", firstFeature.getId(), "Road id");
    assertEquals("PREFIX.9187", firstRoad.getGmlId(), "Road segment id");
    assertEquals("Floraweg", firstRoad.getLabel(), "Label");
    assertEquals(Integer.valueOf(1904), firstRoad.getJurisdictionId(), "Jurisdiction id");
    assertEquals(RoadManager.MUNICIPALITY, firstRoad.getRoadManager(), "Road manager");
    assertEquals("Extra beschrijving", firstRoad.getDescription(), "description");
    assertEquals("FREEWAY", firstRoad.getRoadTypeCode(), "Road is a freeway");

    assertEquals(RoadElevation.STEEP_DYKE, firstRoad.getElevation(), "Elevation type");
    assertEquals(3, firstRoad.getElevationHeight(), "Elevation height");

    assertEquals(1, firstRoad.getTunnelFactor(), 1E-10, "Tunnel factor");

    assertNotNull(firstRoad.getBarrierLeft(), "Barrier left");
    assertEquals(11.2, firstRoad.getBarrierLeft().getDistance(), 1E-10, "Barrier left distance");
    assertEquals(2.4, firstRoad.getBarrierLeft().getHeight(), 1E-10, "Barrier left height");
    assertEquals(SRM2RoadSideBarrierType.SCREEN, firstRoad.getBarrierLeft().getBarrierType(), "Barrier left type");

    assertNull(firstRoad.getBarrierRight(), "Barrier right");

    assertEquals(GeometryType.LINESTRING, firstFeature.getGeometry().type(), "Road segment type");
    assertArrayEquals(new double[][] {{130835, 459390}, {130923, 459309}}, ((LineString) firstFeature.getGeometry()).getCoordinates(),
        "Road segment geometry");

    validateTraffic(firstRoad.getSubSources());
    final EmissionSourceFeature secondFeature = roads.get(1);
    assertTrue(secondFeature.getProperties() instanceof SRM2RoadEmissionSource, "second road correct type");
    final SRM2RoadEmissionSource secondRoad = (SRM2RoadEmissionSource) secondFeature.getProperties();
    assertEquals(RoadElevation.TUNNEL, secondRoad.getElevation(), "Elevation type");
    assertEquals(-3, secondRoad.getElevationHeight(), "Elevation height");
    assertNotNull(secondRoad.getBarrierRight(), "Barrier right second road");
    assertEquals(9.5, secondRoad.getBarrierRight().getDistance(), 1E-10, "Barrier right second road distance");
    assertEquals(8.1, secondRoad.getBarrierRight().getHeight(), 1E-10, "Barrier right second road height");
    assertEquals(SRM2RoadSideBarrierType.SCREEN, secondRoad.getBarrierLeft().getBarrierType(), "Barrier right second road type");
  }

  private void validateTraffic(final List<Vehicles> trafficSources) throws AeriusException {
    assertEquals(1, trafficSources.size(), "Road traffic sources");
    for (final Vehicles vehicleEmissions : trafficSources) {
      assertTrue(vehicleEmissions instanceof StandardVehicles, "road correct type");
      final StandardVehicles standardEmissions = (StandardVehicles) vehicleEmissions;
      assertEquals(110, standardEmissions.getMaximumSpeed().intValue(), "Maximum speed of vehicles");
      for (final Entry<String, ValuesPerVehicleType> entry : standardEmissions.getValuesPerVehicleTypes().entrySet()) {
        if (VehicleType.LIGHT_TRAFFIC.getStandardVehicleCode().equalsIgnoreCase(entry.getKey())) {
          assertEquals(2948105.0, TimeUnit.DAY.toUnit(entry.getValue().getVehiclesPerTimeUnit(), TimeUnit.YEAR), 1E-10, "Vehicles per year");
          assertEquals(0.0, entry.getValue().getStagnationFraction(), 1E-10, "Stagnation fraction");
        } else if (VehicleType.NORMAL_FREIGHT.getStandardVehicleCode().equalsIgnoreCase(entry.getKey())) {
          assertEquals(79205, TimeUnit.DAY.toUnit(entry.getValue().getVehiclesPerTimeUnit(), TimeUnit.YEAR), 1E-10, "Vehicles per year");
          assertEquals(0.0, entry.getValue().getStagnationFraction(), 1E-10, "Stagnation fraction");
        } else if (VehicleType.HEAVY_FREIGHT.getStandardVehicleCode().equalsIgnoreCase(entry.getKey())) {
          assertEquals(62050, TimeUnit.DAY.toUnit(entry.getValue().getVehiclesPerTimeUnit(), TimeUnit.YEAR), 1E-10, "Vehicles per year");
          assertEquals(0.0, entry.getValue().getStagnationFraction(), 1E-10, "Stagnation fraction");
        } else if (VehicleType.AUTO_BUS.getStandardVehicleCode().equalsIgnoreCase(entry.getKey())) {
          assertEquals(7300.0, TimeUnit.DAY.toUnit(entry.getValue().getVehiclesPerTimeUnit(), TimeUnit.YEAR), 1E-10, "Vehicles per year");
          assertEquals(0.0, entry.getValue().getStagnationFraction(), 1E-10, "Stagnation fraction");
        } else {
          fail("Unexpected vehicle type " + entry.getKey());
        }
      }
    }
  }

}
