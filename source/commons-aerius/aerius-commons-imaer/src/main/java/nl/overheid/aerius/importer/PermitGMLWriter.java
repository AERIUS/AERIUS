/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.importer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.scenario.IsScenario;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;
import nl.overheid.aerius.util.FileUtil;

/**
 * Extended GMLWriter for generating a Permit Reference.
 */
public class PermitGMLWriter extends GMLWriter {

  private static final String FILE_PREFIX = "gml_";
  private static final String GML_ENCODING = StandardCharsets.UTF_8.name();

  /**
   *
   */
  public PermitGMLWriter(final PMF pmf) throws SQLException {
    super(ReceptorGridSettingsRepository.getReceptorGridSettings(pmf), new PermitReferenceGenerator(false), AeriusGMLVersion.safeValueOf(
        ConstantRepository.getString(pmf, ConstantsEnum.IMAER_VERSION)));
  }

  /**
   * Build GML and convert it to Strings.
   * Be wary to use this method with large sets of results (or large sets of sources).
   *
   * @param scenario Scenario containing all source data
   * @param metaData meta data of the scenario
   * @param filename the filename to associate the gml with
   * @return The objects representing one or more GMLs.
   * @throws AeriusException When exception occurred generating the GML.
   */
  public StringDataSource writeToString(final IsScenario scenario, final MetaDataInput metaData, final String filename) throws AeriusException {
    final StringDataSource data;

    try (ByteArrayOutputStream boas = new ByteArrayOutputStream()) {
      write(boas, scenario, metaData);
      data = new StringDataSource(boas.toString(GML_ENCODING), filename, GML_MIMETYPE);
    } catch (final IOException e) {
      // catch any exception and put them in a GML exception.
      LOG.error("Internal error occurred.", e);
      throw new AeriusException(ImaerExceptionReason.INTERNAL_ERROR);
    }

    if (LOG.isTraceEnabled()) {
      try {
        FileUtil.toFile(FILE_PREFIX, data.filename(), data.data());
      } catch (final IOException e) {
        LOG.error("Problems creating gml file: {}", data.filename(), e);
      }
    }
    return data;
  }

  /**
   * Ensure all the situations in the scenario get a new permit reference.
   */
  public void ensureAllSituationsHaveNewReferences(final Scenario scenario, final ProductProfile productProfile) {
    for (final ScenarioSituation situation : scenario.getSituations()) {
      situation.setReference(ReferenceUtil.generateReference(productProfile));
    }
  }
}
