/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import nl.overheid.aerius.shared.domain.ops.OPSCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;

/**
 * Object mapper util that can handle subtypes of scenario classes that are not defined in IMAER.
 */
public final class ScenarioObjectMapperUtil {
  private static final ObjectMapper SCENARIO_OBJECT_MAPPER = getScenarioObjectMapper();

  private ScenarioObjectMapperUtil() {
  }

  public static Scenario getScenario(final String scenarioString) throws IOException {
    return SCENARIO_OBJECT_MAPPER.readValue(scenarioString, Scenario.class);
  }

  public static ObjectMapper getScenarioObjectMapper() {
    final ObjectMapper mapper = new ObjectMapper();

    mapper.registerSubtypes(new NamedType(OPSCustomCalculationPoint.class, OPSCustomCalculationPoint.class.getSimpleName()));
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    // Ensure (java 8) dates and times are properly (de)serialized
    mapper.registerModule(new JavaTimeModule());
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    // Do not serialize null values
    mapper.setSerializationInclusion(Include.NON_NULL);

    return mapper;
  }
}
