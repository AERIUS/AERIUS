/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.GroupedSourcesKey;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.characteristics.CharacteristicsSupplier;
import nl.overheid.aerius.db.common.sector.InlandShippingRoutePoint;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.ops.conversion.OPSBuildingTracker;
import nl.overheid.aerius.ops.domain.OPSCopyFactory;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.InlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringInlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.CustomInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.CustomInlandShippingEmissionProperties;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.CustomMooringInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.MooringInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.StandardInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.StandardMooringInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.emissions.SubSourceEmissionsCalculator;
import nl.overheid.aerius.shared.emissions.shipping.ShippingLaden;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;

final class InlandShipExpander {

  private static final Logger LOGGER = LoggerFactory.getLogger(InlandShipExpander.class);

  private static final int MAX_PERCENTAGE = 100;

  private static final double INLAND_LOCK_HEAT_CONTENT_FACTOR = 0.15;

  private static final int DEFAULT_PARTICLE_SIZE_DISTRIBUTION = 3861;
  private static final DiurnalVariation DEFAULT_DIURNAL_VARIATION = DiurnalVariation.INDUSTRIAL_ACTIVITY;

  private final Connection con;
  private final SourceConverter gse;
  private final SubSourceEmissionsCalculator subSourceEmissionsCalculator;
  private final CharacteristicsSupplier characteristicsSupplier;
  // Ships don't have buildings, use null as building tracker
  private final OPSBuildingTracker buildingTracker = null;

  public InlandShipExpander(final Connection con, final SourceConverter gse, final SourceConversionHelper sourceConversionHelper) {
    this.con = con;
    this.gse = gse;
    this.subSourceEmissionsCalculator = sourceConversionHelper.getSubSourceEmissionsCalculator();
    this.characteristicsSupplier = sourceConversionHelper.getCharacteristicsSupplier();
  }

  /**
   * Converts a inland ship object (if presented as a line) to individual point objects with the emission values per object.
   *
   * @param expanded expand emission source to separate sources
   * @param emissionSource source to expand
   * @param substances
   * @return returns true if any expansion has been done, if no processing has been done false is returned.
   * @throws AeriusException
   */
  public void inlandMooringToPoints(final List<EngineSource> expanded, final MooringInlandShippingEmissionSource sev, final Geometry geometry,
      final List<Substance> substances) throws AeriusException {
    // Calculate the emission points on the geometry. For docked ships this
    // is the emission of stationary vessels over the length of the dock.
    for (final MooringInlandShipping vgev : sev.getSubSources()) {
      // In the old days the routes would be contained in this object as well,
      // but since that is no longer the case all we need is the docked part.
      try {
        expanded.addAll(getInlandMooringDockedEmissionSources(sev.getSectorId(), geometry, vgev, substances));
      } catch (final SQLException e) {
        LOGGER.error("SQLException in InlandShipExpander#inlandMooringToPoints", e);
        throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
      }
    }
  }

  /**
   * Converts a inland ship object (if presented as a line) to individual point objects with the emission values per object.
   *
   * @param expanded expand emission source to separate sources
   * @param emissionSource source to expand
   * @param substances
   * @return returns true if any expansion has been done, if no processing has been done false is returned.
   * @throws AeriusException
   */
  public void inlandRouteToPoints(final List<EngineSource> expanded, final InlandShippingEmissionSource sev, final Geometry geometry,
      final List<Substance> substances) throws AeriusException {
    try {
      final List<InlandShippingRoutePoint> routePoints = getRoutePoints(geometry, sev.getWaterway());

      for (final InlandShipping ivg : sev.getSubSources()) {
        expanded.addAll(getInlandRouteEmissionSources(routePoints, ivg, substances));
      }
    } catch (final SQLException e) {
      LOGGER.error("SQLException in InlandShipExpander#inlandRouteToPoints", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private List<InlandShippingRoutePoint> getRoutePoints(final Geometry geometry,
      final InlandWaterway waterway) throws SQLException, AeriusException {
    return ShippingRepository.getInlandShippingRoutePoints(con, geometry, Optional.ofNullable(waterway));
  }

  private List<EngineSource> getInlandMooringDockedEmissionSources(final int sectorId, final Geometry geometry, final MooringInlandShipping vgev,
      final List<Substance> substances) throws AeriusException, SQLException {
    final List<EngineSource> converted = new ArrayList<>();
    // get the characteristics (different by laden / unladen!)
    // query DB for right characteristics: heatcontent/height for movement type = DOCK.
    final Map<ShippingLaden, OPSSourceCharacteristics> characteristicsMap = determineDockedCharacteristics(vgev);

    // convert the split between arriving and departing to a split between laden and unladen.
    setPointSources(converted, characteristicsMap.get(ShippingLaden.LADEN),
        dockToPoints(sectorId, substances, vgev, geometry, ShippingLaden.LADEN));
    setPointSources(converted, characteristicsMap.get(ShippingLaden.UNLADEN),
        dockToPoints(sectorId, substances, vgev, geometry, ShippingLaden.UNLADEN));
    return converted;
  }

  private final Map<ShippingLaden, OPSSourceCharacteristics> determineDockedCharacteristics(final MooringInlandShipping vgev) throws SQLException {
    Map<ShippingLaden, OPSSourceCharacteristics> characteristics;
    if (vgev instanceof final StandardMooringInlandShipping smis) {
      characteristics = characteristicsSupplier.getInlandDockedCharacteristics(smis);
    } else if (vgev instanceof final CustomMooringInlandShipping cmis) {
      final CustomInlandShippingEmissionProperties customProperties = cmis.getEmissionProperties();
      characteristics = new EnumMap<>(ShippingLaden.class);
      characteristics.put(ShippingLaden.LADEN, getCharacteristics(customProperties, ShippingLaden.LADEN));
      characteristics.put(ShippingLaden.UNLADEN, getCharacteristics(customProperties, ShippingLaden.UNLADEN));
    } else {
      throw new IllegalArgumentException("Unexpected MooringInlandShipping type");
    }
    return characteristics;
  }

  private Collection<EngineSource> dockToPoints(final int sectorId, final List<Substance> substances, final MooringInlandShipping vgev,
      final Geometry geometry, final ShippingLaden laden) throws AeriusException {
    final GroupedSourcesPacketMap pointSources = new GroupedSourcesPacketMap();
    final GroupedSourcesKey key = new GroupedSourcesKey(CalculationEngine.OPS, sectorId);
    final GenericEmissionSource copy = new GenericEmissionSource();
    copy.setSectorId(key.id());
    copy.setEmissions(subSourceEmissionsCalculator.calculateInlandDockedEmissions(vgev, laden));
    gse.convert(con, pointSources, copy, geometry, substances);
    return pointSources.getMap().get(key);
  }

  private void setPointSources(final List<EngineSource> converted, final OPSSourceCharacteristics characteristicsAtDock,
      final Collection<EngineSource> pointSources) throws AeriusException {
    for (final EngineSource pointES : pointSources) {
      if (pointES instanceof final OPSSource opsSource) {
        OPSCopyFactory.toOpsSource(characteristicsAtDock, opsSource, buildingTracker);
      }
    }
    converted.addAll(pointSources);
  }

  private List<EngineSource> getInlandRouteEmissionSources(final List<InlandShippingRoutePoint> routePoints,
      final InlandShipping inlandShipping, final List<Substance> substances) throws SQLException, AeriusException {
    final List<EngineSource> pointSources = new ArrayList<>();

    for (final InlandShippingRoutePoint routePoint : routePoints) {
      // add laden A to B emissionsources
      if (inlandShipping.getPercentageLadenAtoB() != null && inlandShipping.getPercentageLadenAtoB() > 0) {
        addInlandRouteOpsSSource(pointSources, substances, routePoint, inlandShipping, false, ShippingLaden.LADEN);
      }
      // add laden B to A emissionsources
      if (inlandShipping.getPercentageLadenBtoA() != null && inlandShipping.getPercentageLadenBtoA() > 0) {
        addInlandRouteOpsSSource(pointSources, substances, routePoint, inlandShipping, true, ShippingLaden.LADEN);
      }
      // add unladen A to B emissionsources
      if (inlandShipping.getPercentageLadenAtoB() != null && inlandShipping.getPercentageLadenAtoB() < MAX_PERCENTAGE) {
        addInlandRouteOpsSSource(pointSources, substances, routePoint, inlandShipping, false, ShippingLaden.UNLADEN);
      }
      // add unladen B to A emissionsources
      if (inlandShipping.getPercentageLadenBtoA() != null && inlandShipping.getPercentageLadenBtoA() < MAX_PERCENTAGE) {
        addInlandRouteOpsSSource(pointSources, substances, routePoint, inlandShipping, true, ShippingLaden.UNLADEN);
      }
    }
    return pointSources;
  }

  private void addInlandRouteOpsSSource(final List<EngineSource> pointSources, final List<Substance> substances,
      final InlandShippingRoutePoint routePoint, final InlandShipping inlandShipping, final boolean reverse, final ShippingLaden laden)
      throws AeriusException, SQLException {
    // determine the right direction and # ships per year based on reverse & laden
    final WaterwayDirection direction = getDirection(routePoint, reverse);

    if ((reverse ? inlandShipping.getMovementsBtoAPerTimeUnit() : inlandShipping.getMovementsAtoBPerTimeUnit()) > 0) {
      // emission values and characteristics can be different based on direction and laden/unladen.
      // this causes quite some sources, but these can be (and are) aggregated with the EmissionSourceAggregator.
      final OPSSource source = getInlandRouteEmissionValues(routePoint, inlandShipping, reverse, laden, substances);
      source.getPoint().setX(routePoint.getX());
      source.getPoint().setY(routePoint.getY());
      final InlandWaterway waterway = new InlandWaterway();
      waterway.setWaterwayCode(routePoint.getWaterwayCategoryCode());
      waterway.setDirection(direction);
      final OPSSourceCharacteristics characteristics = determineRouteCharacteristics(inlandShipping, waterway, laden, reverse);
      OPSCopyFactory.toOpsSource(characteristics, source, buildingTracker);
      if (routePoint.getLockFactor() > 1.0) {
        // for ships in a lock, the heatcontent is 15% of normal.
        source.setHeatContent(source.getHeatContent() * INLAND_LOCK_HEAT_CONTENT_FACTOR);
      }
      pointSources.add(source);
    }
  }

  private OPSSourceCharacteristics determineRouteCharacteristics(final InlandShipping inlandShipping,
      final InlandWaterway waterway, final ShippingLaden laden, final boolean reverse) throws SQLException {
    OPSSourceCharacteristics characteristics;
    if (inlandShipping instanceof final StandardInlandShipping sis) {
      characteristics = characteristicsSupplier.getInlandRouteCharacteristics(sis, waterway, laden);
    } else if (inlandShipping instanceof final CustomInlandShipping customShipping) {
      if (reverse) {
        characteristics = getCharacteristics(customShipping.getEmissionPropertiesBtoA(), laden);
      } else {
        characteristics = getCharacteristics(customShipping.getEmissionPropertiesAtoB(), laden);
      }
    } else {
      throw new IllegalArgumentException("Unexpected InlandShipping type");
    }
    return characteristics;
  }

  private static WaterwayDirection getDirection(final InlandShippingRoutePoint routePoint, final boolean reverse) {
    final WaterwayDirection d = routePoint.getDirection();
    return d != null && reverse ? d.getOpposite() : d;
  }

  private OPSSource getInlandRouteEmissionValues(final InlandShippingRoutePoint routePoint, final InlandShipping inlandShipping,
      final boolean reverse, final ShippingLaden laden, final List<Substance> substances) throws AeriusException {
    final OPSSource emissionSource = new OPSSource();
    final Map<Substance, Double> emissions = subSourceEmissionsCalculator.calculateInlandRouteEmissions(routePoint, inlandShipping, reverse, laden);
    for (final Substance substance : substances) {
      emissionSource.setEmission(substance, emissions.getOrDefault(substance, 0.0));
    }
    // if the supplied key list is empty, there are never any emissions (like when clustering sources). In that case we're not expecting them.
    if (!substances.isEmpty() && !emissionSource.hasEmission()) {
      final String waterwayCategoryCode = routePoint.getWaterwayCategoryCode();
      LOGGER.trace("No emissionfactor found for routePoint:{}, reverse:{}", routePoint, reverse);
      throw new AeriusException(ImaerExceptionReason.INLAND_SHIPPING_SHIP_TYPE_NOT_ALLOWED, getLabel(inlandShipping), waterwayCategoryCode);
    }
    return emissionSource;
  }

  private static String getLabel(final InlandShipping inlandShipping) {
    String label;
    if (inlandShipping instanceof final StandardInlandShipping sis) {
      label = sis.getShipCode();
    } else {
      label = inlandShipping.getDescription();
    }
    return label;
  }

  private static OPSSourceCharacteristics getCharacteristics(final CustomInlandShippingEmissionProperties emissionProperties,
      final ShippingLaden laden) {
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    characteristics.setHeatContent(laden == ShippingLaden.LADEN
        ? emissionProperties.getHeatContentLaden()
        : emissionProperties.getHeatContentEmpty());
    final double emissionHeight = laden == ShippingLaden.LADEN
        ? emissionProperties.getEmissionHeightLaden()
        : emissionProperties.getEmissionHeightEmpty();
    characteristics.setEmissionHeight(emissionHeight);
    characteristics.setSpread(emissionHeight / 2);
    characteristics.setDiurnalVariation(DEFAULT_DIURNAL_VARIATION);
    characteristics.setParticleSizeDistribution(DEFAULT_PARTICLE_SIZE_DISTRIBUTION);
    return characteristics;
  }
}
