/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.ArrayList;
import java.util.List;

import org.locationtech.jts.simplify.DouglasPeuckerSimplifier;

import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Util class to simplify line sources and remove line sources that are too short.
 */
public final class LineSimplifier {

  private static final double DOUGLAS_PEUCKER_TOLERANCE_METERS = 1.1;

  private LineSimplifier() {
    // Util class
  }

  /**
   * Remove line sources smaller than {{@link #DOUGLAS_PEUCKER_TOLERANCE_METERS} and simplify the other line sources.
   *
   * @param scenario Scenario with all sources
   * @throws AeriusException
   */
  public static void cleanupLineSources(final Scenario scenario) throws AeriusException {
    for (final ScenarioSituation situation : scenario.getSituations())
      cleanUpSituation(situation);
  }

  private static void cleanUpSituation(final ScenarioSituation situation) throws AeriusException {
    final List<EmissionSourceFeature> list = situation.getEmissionSourcesList();
    final List<Integer> removeList = new ArrayList<>();

    for (int i = 0; i < list.size(); i++) {
      final EmissionSourceFeature esf = list.get(i);

      if (esf.getGeometry() instanceof LineString) {
        final org.locationtech.jts.geom.LineString jtsLineString =
            (org.locationtech.jts.geom.LineString) GeometryUtil.getGeometry(esf.getGeometry());

        if (jtsLineString.getLength() > DOUGLAS_PEUCKER_TOLERANCE_METERS) {
          esf.setGeometry(simplify(esf.getGeometry()));
        } else {
          // Add in reverse order to remove the last first, because removing an entry changes the index position.
          removeList.add(0, i);
        }
      }
    }
    removeFromList(list, removeList);
  }

  private static void removeFromList(final List<EmissionSourceFeature> list, final List<Integer> removeList) {
    for (final Integer index : removeList) {
      list.remove(index.intValue());
    }
  }

  private static Geometry simplify(final Geometry geometry) throws AeriusException {
    final DouglasPeuckerSimplifier simplifier = new DouglasPeuckerSimplifier(GeometryUtil.getGeometry(geometry));
    simplifier.setDistanceTolerance(DOUGLAS_PEUCKER_TOLERANCE_METERS);

    return GeometryUtil.getAeriusGeometry(simplifier.getResultGeometry());
  }
}
