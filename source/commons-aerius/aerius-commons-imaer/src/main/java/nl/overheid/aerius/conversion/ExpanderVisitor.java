/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.function.AeriusConsumer;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.geojson.IsFeature;
import nl.overheid.aerius.shared.domain.v2.source.ADMSRoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.ColdStartEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceVisitor;
import nl.overheid.aerius.shared.domain.v2.source.FarmAnimalHousingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.FarmLodgingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.FarmlandEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.InlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.ManureStorageEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringInlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringMaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Expands {@link EmissionSource} objects into point objects using the visitor pattern.
 *
 * <p>This visitor uses a {@link SourceCollector} to pass the converted results and therefore only traverses the sources lists.
 * The collecting via the interface design pattern is used because sources can be handled differently for different themes.
 */
public class ExpanderVisitor implements EmissionSourceVisitor<Void> {

  private final Connection con;
  private final List<Substance> substances;
  private final GroupedSourcesPacketMap expandedSources = new GroupedSourcesPacketMap();
  private final SourceConverter sourceConverter;
  private final InlandShipExpander shipSourceExpander;
  private final MaritimeShipExpander maritimeShipExpander;
  private final OffRoadMobileSourceExpander offRoadMobileSourceExpander;
  private final ColdStartSourceExpander coldStartSourceExpander;
  private final SourceConversionHelper sourceConversionHelper;

  /**
   * Constructor.
   *
   * @param con Connection to use for queries
   * @param sourceConverter source converter
   * @param substances emissions substances to convert
   * @param sourceConversionHelper
   * @param sources The emission sources to convert to engine sources.
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException
   */
  public ExpanderVisitor(final Connection con, final SourceConverter sourceConverter, final List<Substance> substances,
      final SourceConversionHelper sourceConversionHelper) throws SQLException {
    this.con = con;
    this.sourceConverter = sourceConverter;
    this.substances = substances;
    this.sourceConversionHelper = sourceConversionHelper;
    shipSourceExpander = new InlandShipExpander(con, this.sourceConverter, sourceConversionHelper);
    maritimeShipExpander = new MaritimeShipExpander(con, this.sourceConverter, sourceConversionHelper);
    offRoadMobileSourceExpander = new OffRoadMobileSourceExpander(this.sourceConverter, sourceConversionHelper);
    coldStartSourceExpander = new ColdStartSourceExpander(sourceConverter, sourceConversionHelper);
  }

  public GroupedSourcesPacketMap getExpandedSources() {
    return expandedSources;
  }

  @Override
  public Void visit(final FarmLodgingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    return null;
  }

  @Override
  public Void visit(final FarmAnimalHousingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    return null;
  }

  @Override
  public Void visit(final FarmlandEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    return null;
  }

  @Override
  public Void visit(final ManureStorageEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    return null;
  }

  @Override
  public Void visit(final GenericEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    return null;
  }

  @Override
  public Void visit(final MooringInlandShippingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    return wrap(emissionSource, expanded -> shipSourceExpander.inlandMooringToPoints(expanded, emissionSource, feature.getGeometry(), substances));
  }

  @Override
  public Void visit(final InlandShippingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    return wrap(emissionSource, expanded -> shipSourceExpander.inlandRouteToPoints(expanded, emissionSource, feature.getGeometry(), substances));
  }

  @Override
  public Void visit(final MooringMaritimeShippingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    return wrap(emissionSource,
        expanded -> maritimeShipExpander.maritimeMooringShipsToPoints(expanded, emissionSource, feature.getGeometry(), substances));
  }

  @Override
  public Void visit(final MaritimeShippingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    if (feature.getGeometry().type() == GeometryType.LINESTRING) {
      wrap(emissionSource, expanded -> maritimeShipExpander.maritimeRouteToPoints(expanded, emissionSource, feature.getGeometry(), substances));
    } else {
      sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    }
    return null;
  }

  private Void wrap(final EmissionSource emissionSource, final AeriusConsumer<List<EngineSource>> consumer) throws AeriusException {
    final List<EngineSource> expanded = new ArrayList<>();

    consumer.apply(expanded);
    expandedSources.addAll(emissionSource.getSectorId(), CalculationEngine.OPS, expanded);
    return null;
  }

  @Override
  public Void visit(final OffRoadMobileEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    offRoadMobileSourceExpander.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    return null;
  }

  @Override
  public Void visit(final ColdStartEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    coldStartSourceExpander.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    return null;
  }

  @Override
  public Void visit(final SRM1RoadEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    emissionSource.setCharacteristics(sourceConversionHelper.getCharacteristicsSupplier().getSectorCharacteristics(emissionSource.getSectorId()));
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    return null;
  }

  @Override
  public Void visit(final SRM2RoadEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    emissionSource.setCharacteristics(sourceConversionHelper.getCharacteristicsSupplier().getSectorCharacteristics(emissionSource.getSectorId()));
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    return null;
  }

  @Override
  public Void visit(final ADMSRoadEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances);
    return null;
  }

}
