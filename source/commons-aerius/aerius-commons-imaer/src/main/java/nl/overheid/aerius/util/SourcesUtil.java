/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.Collection;
import java.util.List;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;

/**
 * Util class to deal with common operations on (lists of) sources.
 */
public final class SourcesUtil {

  private SourcesUtil() {
    // Util class
  }

  /**
   * Return a copy of the list with only sources that have any emission.
   * That is, any source with only emissions 0 are filtered out.
   *
   * @param sources The collection of sources to filter.
   * @return A list of sources that will have emission > 0 for at least one substance.
   */
  public static List<EmissionSourceFeature> filterSourcesWithEmission(final Collection<EmissionSourceFeature> sources) {
    return sources.stream()
        .filter(x -> x.getProperties().getEmissions().values().stream().anyMatch(emission -> emission != 0)).toList();
  }

}
