/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import java.util.List;

import nl.overheid.aerius.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.shared.domain.validation.FileValidationStatus;
import nl.overheid.aerius.shared.domain.validation.ValidationError;
import nl.overheid.aerius.shared.domain.validation.ValidationStatus;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Utility class to create {@link FileValidationStatus} objects.
 */
public final class ValidationFactory {

  private ValidationFactory() {
    // Util class
  }

  /**
   * Creates a failed file validation status given a exception and constructs a localized user friendly error message.
   *
   * @param messages resource to get user friendly error messages
   * @param uuid uuid this status will be refereed too
   * @param exception exception that needs to be put in the status
   * @return {@link FileValidationStatus} object
   */
  public static FileValidationStatus createFailedFileValidationStatus(final AeriusExceptionMessages messages, final String uuid,
      final Exception exception) {
    final AeriusException aeriusException;

    if (exception instanceof AeriusException) {
      aeriusException = (AeriusException) exception;
    } else if (exception.getCause() instanceof AeriusException) {
      aeriusException = (AeriusException) exception.getCause();
    } else {
      aeriusException = new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    final FileValidationStatus status = new FileValidationStatus(uuid, "", ValidationStatus.FAILED, "", null, null, List.of(), null);

    status.getErrors().add(mapAeriusExceptionToValidationError(messages, aeriusException));
    return status;
  }

  /**
   * Converts {@link AeriusException} to a {@link ValidationError} and constructs a localized user friendly error message.
   *
   * @param messages resource to get user friendly error messages
   * @param exception exception that needs to be put in the status
   * @return {@link ValidationError} object
   */
  public static ValidationError mapAeriusExceptionToValidationError(final AeriusExceptionMessages messages, final AeriusException exception) {
    return new ValidationError(exception.getReason().getErrorCode(), messages.getString(exception));
  }

  /**
   * Sets {@link ValidationStatus} in given status object based on the presence of errors, warnings or current state.
   *
   * @param status status object to update
   */
  public static void updateValidationStatus(final FileValidationStatus status) {
    if (!status.getErrors().isEmpty()) {
      status.setValidationStatus(ValidationStatus.INVALID);
    } else if (!status.getWarnings().isEmpty()) {
      status.setValidationStatus(ValidationStatus.VALID_WITH_WARNINGS);
    } else if (status.getValidationStatus() != ValidationStatus.FAILED) {
      status.setValidationStatus(ValidationStatus.VALID);
    } // else don't update the status.
  }
}
