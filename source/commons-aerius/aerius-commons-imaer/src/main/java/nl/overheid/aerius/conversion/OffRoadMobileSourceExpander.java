/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.function.AeriusConsumer;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.offroad.CustomOffRoadMobileSource;
import nl.overheid.aerius.shared.domain.v2.source.offroad.OffRoadMobileSource;
import nl.overheid.aerius.shared.domain.v2.source.offroad.StandardOffRoadMobileSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Converts {@link OffRoadMobileEmissionSource} objects to individual objects with their own characteristics.
 */
class OffRoadMobileSourceExpander {

  private final SourceConverter sourceConverter;
  private final SourceConversionHelper sourceConversionHelper;

  public OffRoadMobileSourceExpander(final SourceConverter sourceConverter, final SourceConversionHelper sourceConversionHelper) {
    this.sourceConverter = sourceConverter;
    this.sourceConversionHelper = sourceConversionHelper;
  }

  /**
   * Converts a {@link OffRoadMobileEmissionValues} object to points object.
   * The default and custom categories are split, where custom categories will result in a
   * emission source each, due to the differences in OPS characteristics.
   *
   * @param emissionSource EmissionSource to convert
   * @throws AeriusException throws AeriusException in case of other problems
   */
  public void convert(final Connection con, final GroupedSourcesPacketMap expandedSources, final OffRoadMobileEmissionSource emissionSource,
      final Geometry geometry, final List<Substance> substances) throws AeriusException {
    //split the source: customs get their own source (own characteristics).
    final List<StandardOffRoadMobileSource> defaultVehicles = new ArrayList<>();
    final List<CustomOffRoadMobileSource> customVehicles = new ArrayList<>();
    for (final OffRoadMobileSource vehicle : emissionSource.getSubSources()) {
      if (vehicle instanceof final CustomOffRoadMobileSource customVehicle) {
        customVehicles.add(customVehicle);
      } else if (vehicle instanceof final StandardOffRoadMobileSource standardVehicle) {
        defaultVehicles.add(standardVehicle);
      }
    }
    final int sectorId = emissionSource.getSectorId();
    final OPSSourceCharacteristics sectorCharacteristics = sourceConversionHelper.getCharacteristicsSupplier().getSectorCharacteristics(sectorId);
    final AeriusConsumer<OffRoadMobileEmissionSource> converter = v -> sourceConverter.convert(con, expandedSources, v, geometry, substances);

    convertCustomVehicles(converter, sectorId, customVehicles, sectorCharacteristics);
    convertStandardVehicles(converter, sectorId, defaultVehicles, sectorCharacteristics);
  }

  private void convertCustomVehicles(final AeriusConsumer<OffRoadMobileEmissionSource> converter, final int sectorId,
      final List<CustomOffRoadMobileSource> customVehicles, final OPSSourceCharacteristics sectorCharacteristics) throws AeriusException {
    //handle the 'custom' vehicles
    for (final CustomOffRoadMobileSource vehicle : customVehicles) {
      final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
      final OffRoadMobileEmissionSource customVehicleSource = createEmissionSource(sectorId, characteristics);

      customVehicleSource.getSubSources().add(vehicle);
      customVehicleSource.setEmissions(sourceConversionHelper.getSubSourceEmissionsCalculator().calculateOffRoadEmissions(customVehicleSource));
      sectorCharacteristics.copyTo(characteristics);
      if (vehicle.getCharacteristics() instanceof final OPSSourceCharacteristics vehicleCharacteristics) {
        characteristics.setEmissionHeight(vehicleCharacteristics.getEmissionHeight());
        characteristics.setSpread(vehicleCharacteristics.getSpread());
        characteristics.setHeatContent(vehicleCharacteristics.getHeatContent());
      }
      converter.apply(customVehicleSource);
    }
  }

  private void convertStandardVehicles(final AeriusConsumer<OffRoadMobileEmissionSource> converter, final int sectorId,
      final List<StandardOffRoadMobileSource> defaultVehicles, final OPSSourceCharacteristics sectorCharacteristics) throws AeriusException {
    if (defaultVehicles.isEmpty()) {
      return;
    }
    final OffRoadMobileEmissionSource defaultVehiclesSource = createEmissionSource(sectorId, sectorCharacteristics);

    defaultVehiclesSource.getSubSources().addAll(defaultVehicles);
    defaultVehiclesSource.setEmissions(sourceConversionHelper.getSubSourceEmissionsCalculator().calculateOffRoadEmissions(defaultVehiclesSource));
    converter.apply(defaultVehiclesSource);
  }

  private static OffRoadMobileEmissionSource createEmissionSource(final int sectorId, final OPSSourceCharacteristics characteristics) {
    final OffRoadMobileEmissionSource es = new OffRoadMobileEmissionSource();

    es.setSectorId(sectorId);
    es.setCharacteristics(characteristics);
    return es;
  }
}
