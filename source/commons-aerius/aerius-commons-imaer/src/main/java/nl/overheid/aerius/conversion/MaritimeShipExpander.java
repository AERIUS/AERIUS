/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.GroupedSourcesKey;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.characteristics.CharacteristicsSupplier;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.sector.MaritimeShippingRoutePoint;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.ops.conversion.OPSBuildingTracker;
import nl.overheid.aerius.ops.domain.OPSCopyFactory;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringMaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.CustomMaritimeShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.CustomMaritimeShippingEmissionProperties;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.CustomMooringMaritimeShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.MaritimeShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.MooringMaritimeShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.ShippingMovementType;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.StandardMaritimeShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.StandardMooringMaritimeShipping;
import nl.overheid.aerius.shared.emissions.SubSourceEmissionsCalculator;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Util class to expand {@link MooringMaritimeShippingEmissionSource} or {@link MaritimeShippingEmissionSource} to single emission points.
 */
final class MaritimeShipExpander {

  private static final Logger LOGGER = LoggerFactory.getLogger(MaritimeShipExpander.class);

  // GCN sector "Verkeer-Zeescheepvaart-binnengaats voor anker, Overige schepen" (only important for certain substances like PM10)
  private static final int DEFAULT_PARTICLE_SIZE_DISTRIBUTION = 3839;
  private static final DiurnalVariation DEFAULT_DIURNAL_VARIATION = DiurnalVariation.CONTINUOUS;

  private final Connection con;
  private final SourceConverter gse;
  private final SubSourceEmissionsCalculator subSourceEmissionsCalculator;
  private final CharacteristicsSupplier characteristicsSupplier;
  // Ships don't have buildings, use null as building tracker
  private final OPSBuildingTracker buildingTracker = null;
  private double lineSegmentSize = -1;

  public MaritimeShipExpander(final Connection con, final SourceConverter gse, final SourceConversionHelper sourceConversionHelper)
      throws SQLException {
    this.con = con;
    this.gse = gse;
    this.subSourceEmissionsCalculator = sourceConversionHelper.getSubSourceEmissionsCalculator();
    this.characteristicsSupplier = sourceConversionHelper.getCharacteristicsSupplier();
  }

  /**
   * Converts a {@link MooringMaritimeShippingEmissionSource} object to points object. If a ship is docked the emission is based on the docked emission, the
   * transfer route emission and the emission on the main route. For ships on a route the emission is the emission on the route.
   *
   * @param expanded Add new emission sources to this list
   * @param sev Maritime Emission Source
   * @param geometry of the dock
   * @param substances
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case of other problems
   */
  public void maritimeMooringShipsToPoints(final List<EngineSource> expanded, final MooringMaritimeShippingEmissionSource sev,
      final Geometry geometry, final List<Substance> substances) throws AeriusException {
    // Calculate the emission points on the geometry. For docked ships this
    // is the emission of stationary vessels over the length of the dock.
    try {
      for (final MooringMaritimeShipping vgev : sev.getSubSources()) {
        //if there aren't any ships, don't even bother to handle them. The operations are relatively expensive.
        if (vgev.getShipsPerTimeUnit() > 0) {
          // In the old days the routes would be contained in this object as well,
          // but since that is no longer the case all we need is the docked part.
          expanded.addAll(getMaritimeMooringDockedEmissionSources(sev.getSectorId(), vgev, geometry, substances));
        }
      }
    } catch (final SQLException e) {
      LOGGER.error("SQLException in MaritimeShipExpander#maritimeMooringShipsToPoints", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Expands maritime ships on a route to points.
   * @param con
   * @param expanded
   * @param serv
   * @return
   * @throws SQLException
   * @throws AeriusException
   */
  public void maritimeRouteToPoints(final List<EngineSource> expanded, final MaritimeShippingEmissionSource serv, final Geometry geometry,
      final List<Substance> substances) throws AeriusException {
    try {
      if (serv.getMooringAId() == null && serv.getMooringBId() == null) {
        maritimeStandaloneRouteToPoints(expanded, serv, geometry, substances);
      } else {
        maritimeMooringRouteToPoints(expanded, serv, geometry, substances);
      }
    } catch (final SQLException e) {
      LOGGER.error("SQLException in MaritimeShipExpander#maritimeRouteToPoints", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private void maritimeStandaloneRouteToPoints(final List<EngineSource> expanded, final MaritimeShippingEmissionSource serv, final Geometry geometry,
      final List<Substance> substances) throws SQLException, AeriusException {
    // This is a shipping route, use the supplied geometry to determine all route points.
    final List<MaritimeShippingRoutePoint> routePoints = ShippingRepository.getMaritimeShippingRoutePoints(con, geometry, getLineSegmentSize());
    for (final MaritimeShipping maritimeShipping : serv.getSubSources()) {
      //The emissionsource itself represents the route, use its movement type.
      final int numberOfShips = maritimeShipping.getTimeUnit().getPerYear(maritimeShipping.getMovementsPerTimeUnit());
      expanded.addAll(
          convertMaritimePointsToEmissionSources(routePoints, serv.getMovementType(), numberOfShips, maritimeShipping, substances));
    }
  }

  private void maritimeMooringRouteToPoints(final List<EngineSource> expanded, final MaritimeShippingEmissionSource serv, final Geometry geometry,
      final List<Substance> substances) throws SQLException, AeriusException {
    // This is a shipping route, use the supplied geometry to determine all route points.
    for (final MaritimeShipping maritimeShipping : serv.getSubSources()) {
      final List<MaritimeShippingRoutePoint> routePoints = determineRoutePoints(geometry, maritimeShipping, serv.getMooringAId() != null,
          serv.getMooringBId() != null);
      //The emissionsource itself represents the route, use its movement type.
      final int numberOfShips = maritimeShipping.getTimeUnit().getPerYear(maritimeShipping.getMovementsPerTimeUnit());
      expanded.addAll(
          convertMaritimePointsToEmissionSources(routePoints, serv.getMovementType(), numberOfShips, maritimeShipping, substances));
    }
  }

  private List<MaritimeShippingRoutePoint> determineRoutePoints(final Geometry geometry, final MaritimeShipping vgev, final boolean mooringOnA,
      final boolean mooringOnB) throws SQLException, AeriusException {
    if (vgev instanceof final StandardMaritimeShipping sms) {
      return ShippingRepository.getMaritimeMooringShippingInlandRoute(con, geometry, sms.getShipCode(),
          getLineSegmentSize(), mooringOnA, mooringOnB);
    } else if (vgev instanceof final CustomMaritimeShipping cms) {
      return ShippingRepository.getMaritimeMooringShippingInlandRouteForGrossTonnage(con, geometry, cms.getGrossTonnage(),
          getLineSegmentSize(), mooringOnA, mooringOnB);
    } else {
      throw new IllegalArgumentException("Unexpected MaritimeShipping type");
    }
  }

  private synchronized double getLineSegmentSize() throws SQLException {
    if (lineSegmentSize < 0) {
      lineSegmentSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_LINE_TO_POINTS_SEGMENT_SIZE, Double.class);
    }
    return lineSegmentSize;
  }

  /**
   * Expands the ship emission values at the dock to points.
   */
  private Collection<EngineSource> getMaritimeMooringDockedEmissionSources(final int sectorId, final MooringMaritimeShipping shipping,
      final Geometry geometry, final List<Substance> substances) throws AeriusException, SQLException {
    final GroupedSourcesPacketMap pointSources = new GroupedSourcesPacketMap();
    //Dock can be a line, a polygon or a point. Ensure it's converted no matter what.
    //ensure only the emission for this vesselgroup is used
    final GroupedSourcesKey key = new GroupedSourcesKey(CalculationEngine.OPS, sectorId);
    final GenericEmissionSource copy = new GenericEmissionSource();
    copy.setSectorId(key.id());
    copy.setEmissions(subSourceEmissionsCalculator.calculateMaritimeDockedEmissions(shipping));
    gse.convert(con, pointSources, copy, geometry, substances);
    final OPSSourceCharacteristics characteristicsAtDock = determineDockedCharacteristics(shipping);
    final Collection<EngineSource> pointCollection = pointSources.getMap().get(key);

    for (final EngineSource pointES : pointCollection) {
      OPSCopyFactory.toOpsSource(characteristicsAtDock, (OPSSource) pointES, buildingTracker);
    }
    return pointCollection;
  }

  private final OPSSourceCharacteristics determineDockedCharacteristics(final MooringMaritimeShipping shipping) throws SQLException {
    OPSSourceCharacteristics characteristics;
    if (shipping instanceof final StandardMooringMaritimeShipping smms) {
      characteristics = characteristicsSupplier.getMaritimeDockedCharacteristics(smms);
    } else if (shipping instanceof final CustomMooringMaritimeShipping cmms) {
      characteristics = getCharacteristics(cmms.getEmissionProperties());
    } else {
      throw new IllegalArgumentException("Unexpected MooringMaritimeShipping type");
    }
    return characteristics;
  }

  private List<EngineSource> convertMaritimePointsToEmissionSources(
      final List<MaritimeShippingRoutePoint> routePoints, final ShippingMovementType movementType, final int numberOfShips,
      final MaritimeShipping maritimeShipping, final List<Substance> substances) throws AeriusException, SQLException {
    final List<EngineSource> expanded = new ArrayList<>();

    for (final MaritimeShippingRoutePoint routePoint : routePoints) {
      final EngineSource pointES = getMaritimeRouteEmissionSource(routePoint, movementType, numberOfShips, maritimeShipping, substances);
      expanded.add(pointES);
    }
    return expanded;
  }

  private EngineSource getMaritimeRouteEmissionSource(final MaritimeShippingRoutePoint routePoint, final ShippingMovementType movementType,
      final int numberOfShips, final MaritimeShipping maritimeShipping, final List<Substance> substances) throws AeriusException, SQLException {
    final OPSSourceCharacteristics emissionCharacteristics = determineRouteCharacteristics(maritimeShipping, movementType);
    final OPSSource source = new OPSSource(0, routePoint.getSrid(), routePoint.getX(), routePoint.getY());
    setEmissionValuesPerPointForRoute(source, routePoint, movementType, numberOfShips, maritimeShipping, substances);
    OPSCopyFactory.toOpsSource(emissionCharacteristics, source, buildingTracker);
    return source;
  }

  private final OPSSourceCharacteristics determineRouteCharacteristics(final MaritimeShipping shipping, final ShippingMovementType movementType)
      throws SQLException {
    OPSSourceCharacteristics characteristics;
    if (shipping instanceof final StandardMaritimeShipping sms) {
      characteristics = characteristicsSupplier.getMaritimeRouteCharacteristics(sms, movementType);
    } else if (shipping instanceof final CustomMaritimeShipping cms) {
      characteristics = getCharacteristics(cms.getEmissionProperties());
    } else {
      throw new IllegalArgumentException("Unexpected MaritimeShipping type");
    }
    return characteristics;
  }

  private void setEmissionValuesPerPointForRoute(final EngineEmissionSource gev, final MaritimeShippingRoutePoint routePoint,
      final ShippingMovementType movementType, final int numberOfMovements, final MaritimeShipping maritimeShipping,
      final List<Substance> substances) throws AeriusException {
    final Map<Substance, Double> emissions = subSourceEmissionsCalculator
        .calculateMaritimeRouteEmissions(routePoint, movementType, numberOfMovements, maritimeShipping);
    for (final Substance substance : substances) {
      if (emissions.containsKey(substance)) {
        gev.setEmission(substance, emissions.get(substance));
      }
    }
  }

  private OPSSourceCharacteristics getCharacteristics(final CustomMaritimeShippingEmissionProperties emissionProperties) {
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    characteristics.setHeatContent(emissionProperties.getHeatContent());
    final double emissionHeight = emissionProperties.getEmissionHeight();
    characteristics.setEmissionHeight(emissionHeight);
    characteristics.setSpread(emissionHeight / 2);
    characteristics.setDiurnalVariation(DEFAULT_DIURNAL_VARIATION);
    characteristics.setParticleSizeDistribution(DEFAULT_PARTICLE_SIZE_DISTRIBUTION);
    return characteristics;
  }
}
