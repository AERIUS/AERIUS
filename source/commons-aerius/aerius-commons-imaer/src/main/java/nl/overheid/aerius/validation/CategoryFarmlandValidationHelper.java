/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.validation;

import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

class CategoryFarmlandValidationHelper implements FarmlandValidationHelper {

  private final SectorCategories categories;

  CategoryFarmlandValidationHelper(final SectorCategories categories) {
    this.categories = categories;
  }

  @Override
  public boolean isValidFarmlandActivityCode(final String activityCode) {
    return categories.determineFarmlandCategoryByCode(activityCode) != null;
  }

  @Override
  public boolean isValidFarmlandStandardActivityCode(final String activityCode) {
    return categories.determineFarmSourceByCode(activityCode) != null;
  }

  @Override
  public FarmEmissionFactorType getFarmSourceEmissionFactorType(final String farmSourceCategoryCode) {
    return categories.determineFarmSourceByCode(farmSourceCategoryCode).getFarmEmissionFactorType();
  }

}
