/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.validation;

import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;

/**
 * Validation utility class for generic and generic source validations.
 */
public final class GenericSourcesValidator {

  private GenericSourcesValidator() {
    // Util class
  }

  /**
   * If validateStrict is true warning validations will be reported as errors.
   *
   * @param importParcel
   * @param validateStrict
   */
  public static void validateStrict(final ImportParcel importParcel, final boolean validateStrict) {
    if (validateStrict) {
      // move warnings to exceptions list
      importParcel.getExceptions().addAll(importParcel.getWarnings());
      importParcel.getWarnings().clear();
    }
  }
}
