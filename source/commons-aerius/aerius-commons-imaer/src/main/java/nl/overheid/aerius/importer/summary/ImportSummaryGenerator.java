/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.summary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.locationtech.jts.algorithm.ConvexHull;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.PrecisionModel;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.shared.domain.importer.summary.ImportSummary;
import nl.overheid.aerius.shared.domain.importer.summary.SituationResults;
import nl.overheid.aerius.shared.domain.importer.summary.SituationSummary;
import nl.overheid.aerius.shared.domain.importer.summary.SourcesSummary;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.v2.geojson.Feature;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.util.ScenarioObjectMapperUtil;

/**
 * Generates a summary for an AERIUS-file, as can be requested using Connect.
 */
public class ImportSummaryGenerator {

  private static final ObjectMapper OBJECT_MAPPER = ScenarioObjectMapperUtil.getScenarioObjectMapper();

  private final Map<Locale, AeriusExceptionMessages> exceptionMessages = LocaleUtils.getLocales().stream()
      .collect(Collectors.toMap(Function.identity(), AeriusExceptionMessages::new));

  /**
   * Convert a set of `ImportParcel`s to a summary
   * @param locale the locale of the error messages
   * @param parcels the parcels to convert
   * @return the summary
   * @throws AeriusException
   */
  public ImportSummary generateImportSummary(final Locale locale, final List<ImportParcel> parcels) throws AeriusException {
    final List<ImportSummary.ValidationMessage> errors = parcels.stream()
        .flatMap(ip -> ip.getExceptions().stream()).map(ae -> convertMessage(locale, ae)).collect(Collectors.toList());
    final List<ImportSummary.ValidationMessage> warnings = parcels.stream()
        .flatMap(ip -> ip.getWarnings().stream()).map(ae -> convertMessage(locale, ae)).collect(Collectors.toList());
    final boolean success = errors.isEmpty();

    final ImportSummary summaryResponse = new ImportSummary();
    summaryResponse.setSuccessful(success);
    summaryResponse.setErrors(errors);
    summaryResponse.setWarnings(warnings);
    summaryResponse.setSituations(new ArrayList<>());

    if (success) {
      for (final ImportParcel importParcel : parcels) {
        summaryResponse.getSituations().add(createSituationSummary(importParcel));
      }
    }

    return summaryResponse;
  }

  private ImportSummary.ValidationMessage convertMessage(final Locale locale, final AeriusException e) {
    final String message = exceptionMessages.get(locale).getString(e);
    return new ImportSummary.ValidationMessage(e.getReason().getErrorCode(), message);
  }

  private static SituationSummary createSituationSummary(final ImportParcel importParcel) throws AeriusException {
    final SituationSummary summary = new SituationSummary();
    final ScenarioSituation situation = importParcel.getSituation();

    summary.setName(importParcel.getSituation().getName());
    if (situation.getType() != null) {
      summary.setType(importParcel.getSituation().getType().name());
    }
    summary.setYear(importParcel.getSituation().getYear());

    // Reference is on the situation-level and no longer present in ScenarioMetaData
    // Register however expects it in the metadata, so add it here.
    summary.setMetadata(new HashMap<>());
    summary.getMetadata().put("reference", situation.getReference());

    if (importParcel.getImportedMetaData() != null) {
      convertMetadata(summary.getMetadata(), importParcel.getImportedMetaData());
    }

    summary.setResults(new ArrayList<>());
    if (importParcel.getSituationResults() != null) {
      importParcel.getSituationResults().getResults().stream()
          .map(Feature::getProperties)
          .flatMap(ImportSummaryGenerator::convertSituationResults)
          .forEach(ri -> summary.getResults().add(ri));
    }

    summary.setVersion(importParcel.getVersion());
    summary.setDatabaseVersion(importParcel.getDatabaseVersion());

    final List<String> centroidIncludedSources = Optional.ofNullable(importParcel.getCalculationSetOptions())
        .map(CalculationSetOptions::getNcaCalculationOptions)
        .map(NCACalculationOptions::getDevelopmentPressureSourceIds)
        .orElse(List.of());
    summary.setSources(convertEmissionSource(importParcel.getSituation().getEmissionSourcesList(), centroidIncludedSources));

    return summary;
  }

  private static SourcesSummary convertEmissionSource(final List<EmissionSourceFeature> emissionSource, final List<String> centroidIncludedSources)
      throws AeriusException {
    final SourcesSummary summary = new SourcesSummary();
    summary.setTotalEmissions(new HashMap<>());
    summary.setNumSources(emissionSource.size());

    final Map<Integer, BigDecimal> emissionsPerSector = new HashMap<>();
    final List<Geometry> geometries = new ArrayList<>();

    for (final EmissionSourceFeature source : emissionSource) {
      // sum emissions
      source.getProperties().getEmissions()
          .forEach((substance, emission) -> summary.getTotalEmissions().merge(substance.getName(), BigDecimal.valueOf(emission), BigDecimal::add));

      // sum emissions per sector to determine main sector
      final int sectorId = source.getProperties().getSectorId();
      final BigDecimal emission = BigDecimal.valueOf(getEmission(source.getProperties()));
      emissionsPerSector.merge(sectorId, emission, BigDecimal::add);

      // If the list of sources to be included for centroid determiniation is empty, include all sources.
      if (centroidIncludedSources.isEmpty() || centroidIncludedSources.contains(source.getProperties().getGmlId())) {
        geometries.add(GeometryUtil.getGeometry(source.getGeometry()));
      }
    }

    setSectorInformation(summary, emissionsPerSector);
    setCentroid(summary, geometries);

    return summary;
  }

  private static void setSectorInformation(final SourcesSummary summary, final Map<Integer, BigDecimal> emissionsPerSector) {
    summary.setNumSectors(emissionsPerSector.size());
    if (emissionsPerSector.isEmpty()) {
      summary.setMainSectorId(Sector.DEFAULT_SECTOR_ID);
    } else {
      final List<Integer> sectorIds = emissionsPerSector.entrySet().stream()
          .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue())) // order by descending emissions
          .map(Map.Entry::getKey)
          .collect(Collectors.toList());

      summary.setMainSectorId(sectorIds.get(0));
    }
  }

  private static void setCentroid(final SourcesSummary summary, final List<Geometry> geometries) {
    if (geometries.isEmpty()) {
      summary.setCentroidX(0);
      summary.setCentroidY(0);
    } else {
      final GeometryCollection collection = new GeometryCollection(geometries.toArray(new Geometry[0]),
          new GeometryFactory(new PrecisionModel()));
      final ConvexHull convexHull = new ConvexHull(collection);
      final Point point = convexHull.getConvexHull().getCentroid();

      summary.setCentroidX(point.getX());
      summary.setCentroidY(point.getY());
    }
  }

  private static double getEmission(final EmissionSource source) {
    return source.getEmissions().values().stream().mapToDouble(Double::doubleValue).sum();
  }

  private static Stream<SituationResults> convertSituationResults(final CalculationPoint calculationPoint) {

    // first, group the results by resultType
    // ideally each result has its own line, but this will not be backwards compatible with the current API spec
    return calculationPoint.getResults().entrySet().stream()
        .collect(Collectors.groupingBy(x -> x.getKey().getEmissionResultType()))
        .entrySet().stream().map(entry -> {

          SituationResults situationResults;
          if (calculationPoint instanceof ReceptorPoint) {
            situationResults = convertSituationResults((ReceptorPoint) calculationPoint);
          } else if (calculationPoint instanceof SubPoint) {
            situationResults = convertSituationResults((SubPoint) calculationPoint);
          } else {
            return null;
          }

          final Map<String, BigDecimal> results = entry.getValue().stream()
              .collect(Collectors.toMap(row -> row.getKey().getSubstance().getName(), row -> BigDecimal.valueOf(row.getValue())));
          situationResults.setResultType(entry.getKey().name().toUpperCase(Locale.ROOT));
          situationResults.setResults(results);
          return situationResults;
        })
        .filter(Objects::nonNull);
  }

  private static SituationResults convertSituationResults(final ReceptorPoint receptorPoint) {
    final SituationResults sr = new SituationResults();
    sr.setReceptorId(receptorPoint.getReceptorId());
    return sr;
  }

  private static SituationResults convertSituationResults(final SubPoint subPoint) {
    final SituationResults sr = new SituationResults();

    sr.setReceptorId(subPoint.getReceptorId());
    sr.setSubPointId(subPoint.getSubPointId());
    return sr;
  }

  private static void convertMetadata(final Map<String, String> metaData, final ScenarioMetaData scenarioMetaData) {
    OBJECT_MAPPER.valueToTree(scenarioMetaData).fields().forEachRemaining(es -> metaData.put(es.getKey(), es.getValue().asText()));
  }

}
