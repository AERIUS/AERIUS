/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.validation;

import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Helper class implementing validation of farm animal housing via {@link SectorCategories}.
 */
class CategoryFarmAnimalHousingValidationHelper implements FarmAnimalHousingValidationHelper {

  private final FarmAnimalHousingCategories categories;

  public CategoryFarmAnimalHousingValidationHelper(final SectorCategories categories) {
    this.categories = categories.getFarmAnimalHousingCategories();
  }

  @Override
  public boolean isValidFarmAnimalCode(final String animalCode) {
    return categories.determineAnimalCategoryByCode(animalCode) != null;
  }

  @Override
  public boolean isValidFarmAnimalHousingCode(final String animalHousingCode) {
    return categories.determineHousingCategoryByCode(animalHousingCode) != null;
  }

  @Override
  public boolean isValidFarmAnimalHousingCombination(final String animalCode, final String animalHousingCode) {
    final FarmAnimalHousingCategory housing = categories.determineHousingCategoryByCode(animalHousingCode);
    return housing != null && housing.getAnimalCategoryCode().equalsIgnoreCase(animalCode);
  }

  @Override
  public boolean isValidFarmAdditionalSystemCode(final String systemCode) {
    return categories.determineAdditionalHousingCategoryByCode(systemCode) != null;
  }

  @Override
  public boolean isValidFarmAdditionalSystemCombination(final String housingCode, final String systemCode) {
    return categories.getHousingAllowedAdditionalSystems().containsKey(housingCode)
        && categories.getHousingAllowedAdditionalSystems().get(housingCode).contains(systemCode);
  }

  @Override
  public FarmEmissionFactorType getAnimalHousingEmissionFactorType(final String animalHousingCode) {
    return categories.determineHousingCategoryByCode(animalHousingCode).getFarmEmissionFactorType();
  }
}
