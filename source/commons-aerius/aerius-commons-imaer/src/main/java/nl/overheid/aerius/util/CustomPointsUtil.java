/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.List;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;

/**
 * Util class for assigning proper id's to a list of custom calculation points.
 */
public final class CustomPointsUtil {

  private CustomPointsUtil(){
  }

  /**
   * Assigns new id's to custom calculation points based on their index.
   * @param customPointList a list of calculation point features, can contain features other than custom calculation points.
   * @param strict if strict, assign new id's when duplicates exist, otherwise assign new id's when all custom calculation point ids are 0.
   */
  public static void ensureProperIds(final List<CalculationPointFeature> customPointList, final boolean strict) {
    if (shouldReassignCalculationPoints(customPointList, strict)) {
      for (final CalculationPointFeature feature : customPointList) {
        if (feature.getProperties() instanceof CustomCalculationPoint) {
          ((CustomCalculationPoint) feature.getProperties()).setCustomPointId(customPointList.indexOf(feature) + 1);
        }
      }
    }
  }

  private static boolean shouldReassignCalculationPoints(final List<CalculationPointFeature> customPointList, final boolean strict) {
    List<CalculationPoint> customCalculationPoints = customPointList.stream().map(CalculationPointFeature::getProperties)
        .filter(CustomCalculationPoint.class::isInstance)
        .collect(Collectors.toList());
    if (strict) {
      return customCalculationPoints.size() != customCalculationPoints.stream().map(CalculationPoint::getId).distinct().count();
    } else {
      return customCalculationPoints.stream().allMatch(cp -> cp.getId() == 0);
    }
  }
}
