/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.db.common.sector.SourceCharacteristicsUtil;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.Conversion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.GMLLegacyCodeType;
import nl.overheid.aerius.gml.base.conversion.FarmLodgingConversion;
import nl.overheid.aerius.gml.base.conversion.MobileSourceOffRoadConversion;
import nl.overheid.aerius.gml.base.conversion.PlanConversion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;

/**
 *
 */
final class GMLConversionRepository {

  private enum RepositoryAttribute implements Attribute {
    TYPE,
    OLD_VALUE,
    NEW_VALUE,
    ISSUE_WARNING,

    GML_VERSION,

    OLD_CODE,
    NEW_FUEL_CONSUMPTION,

    ANIMAL_TYPE_CODE,
    ANIMAL_HOUSING_CODE,
    ADDITIONAL_SYSTEM_CODE;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ROOT);
    }

  }

  private static final Query SELECT_GML_LEGACY_CODES =
      QueryBuilder.from("system.gml_conversions")
          .select(RepositoryAttribute.TYPE, RepositoryAttribute.OLD_VALUE, RepositoryAttribute.NEW_VALUE, RepositoryAttribute.ISSUE_WARNING)
          .where(RepositoryAttribute.GML_VERSION).getQuery();

  private static final Query SELECT_GML_LEGACY_MOBILE_SOURCE_OFF_ROAD_CONVERSIONS =
      QueryBuilder.from("system.gml_mobile_source_off_road_conversions")
          .select(RepositoryAttribute.OLD_CODE, RepositoryAttribute.NEW_FUEL_CONSUMPTION)
          .getQuery();

  private static final Query SELECT_GML_LEGACY_PLAN_CONVERSIONS =
      QueryBuilder.from("system.gml_plan_conversions_view")
          .join(new JoinClause("emission_diurnal_variations", QueryAttribute.EMISSION_DIURNAL_VARIATION_ID))
          .select(RepositoryAttribute.OLD_CODE, QueryAttribute.EMISSION_FACTOR)
          .select(SourceCharacteristicsUtil.OPS_CHARACTERISTICS_ATTRIBUTES)
          .getQuery();

  private static final Query SELECT_GML_LEGACY_LODGING_CONVERSIONS =
      QueryBuilder.from("system.gml_farm_lodging_conversions")
          .select(RepositoryAttribute.OLD_CODE, RepositoryAttribute.ANIMAL_TYPE_CODE, RepositoryAttribute.ANIMAL_HOUSING_CODE,
              RepositoryAttribute.ADDITIONAL_SYSTEM_CODE)
          .getQuery();

  private GMLConversionRepository() {
    //do not instantiate.
  }

  /**
   * Get legacy codes specific for the given AERIUS GML version.
   * @param con database connection
   * @param version AERIUS GML version
   * @return map containing the proper conversions.
   * @throws SQLException in case something went wrong with the database
   */
  static Map<GMLLegacyCodeType, Map<String, Conversion>> getLegacyCodes(final Connection con, final AeriusGMLVersion version)
      throws SQLException {
    final Map<GMLLegacyCodeType, Map<String, Conversion>> codeMaps = new HashMap<>();
    try (final PreparedStatement statement = con.prepareStatement(SELECT_GML_LEGACY_CODES.get())) {
      SELECT_GML_LEGACY_CODES.setParameter(statement, RepositoryAttribute.GML_VERSION, version.name());

      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        final GMLLegacyCodeType codeType = QueryUtil.getEnum(rs, RepositoryAttribute.TYPE, GMLLegacyCodeType.class);
        if (!codeMaps.containsKey(codeType)) {
          codeMaps.put(codeType, new HashMap<>());
        }
        codeMaps.get(codeType).put(QueryUtil.getString(rs, RepositoryAttribute.OLD_VALUE),
            new Conversion(QueryUtil.getString(rs, RepositoryAttribute.NEW_VALUE), QueryUtil.getBoolean(rs, RepositoryAttribute.ISSUE_WARNING)));
      }
    }
    return codeMaps;
  }

  /**
   * Get legacy mobile source off road conversions
   * @param con database connection
   * @return map containing the proper conversions.
   * @throws SQLException in case something went wrong with the database
   */
  static Map<String, MobileSourceOffRoadConversion> getLegacyMobileSourceOffRoadConversions(final Connection con)
      throws SQLException {
    final Map<String, MobileSourceOffRoadConversion> codeMap = new HashMap<>();
    try (final PreparedStatement statement = con.prepareStatement(SELECT_GML_LEGACY_MOBILE_SOURCE_OFF_ROAD_CONVERSIONS.get())) {

      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        codeMap.put(QueryUtil.getString(rs, RepositoryAttribute.OLD_CODE),
            new MobileSourceOffRoadConversion(
                QueryUtil.getDouble(rs, RepositoryAttribute.NEW_FUEL_CONSUMPTION)));
      }
    }
    return codeMap;
  }

  /**
   * Get legacy plan conversions.
   * @param con database connection
   * @return map containing the proper conversions.
   * @throws SQLException in case something went wrong with the database
   */
  static Map<String, PlanConversion> getLegacyPlanConversions(final Connection con)
      throws SQLException {
    final Map<String, PlanConversion> codeMap = new HashMap<>();
    try (final PreparedStatement statement = con.prepareStatement(SELECT_GML_LEGACY_PLAN_CONVERSIONS.get())) {

      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        // Emission factors in database for this conversion are for NOx by definition
        final Map<Substance, Double> emissionFactors = Map.of(Substance.NOX, QueryAttribute.EMISSION_FACTOR.getDouble(rs));
        final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
        SourceCharacteristicsUtil.setSourceCharacteristicsFromResultSet(characteristics, rs);
        codeMap.put(QueryUtil.getString(rs, RepositoryAttribute.OLD_CODE),
            new PlanConversion(emissionFactors, characteristics));
      }
    }
    return codeMap;
  }

  /**
   * Get legacy farm lodging conversions
   * @param con database connection
   * @return map containing the proper conversions.
   * @throws SQLException in case something went wrong with the database
   */
  static Map<String, FarmLodgingConversion> getLegacyFarmLodgingConversions(final Connection con)
      throws SQLException {
    final Map<String, FarmLodgingConversion> codeMap = new HashMap<>();
    try (final PreparedStatement statement = con.prepareStatement(SELECT_GML_LEGACY_LODGING_CONVERSIONS.get())) {

      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        codeMap.put(QueryUtil.getString(rs, RepositoryAttribute.OLD_CODE),
            new FarmLodgingConversion(
                QueryUtil.getString(rs, RepositoryAttribute.ANIMAL_TYPE_CODE),
                QueryUtil.getString(rs, RepositoryAttribute.ANIMAL_HOUSING_CODE),
                QueryUtil.getString(rs, RepositoryAttribute.ADDITIONAL_SYSTEM_CODE)));
      }
    }
    return codeMap;
  }
}
