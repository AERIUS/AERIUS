/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Year;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.opentelemetry.api.trace.Span;

import nl.aerius.adms.io.AplImportReader;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.importer.domain.PdfGMLs;
import nl.overheid.aerius.ops.importer.BrnImportReader;
import nl.overheid.aerius.ops.importer.RcpImportReader;
import nl.overheid.aerius.paa.PAAImport;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.io.LegacyNSLImportReader;
import nl.overheid.aerius.validation.EmissionSourceValidator;
import nl.overheid.aerius.validation.GenericSourcesValidator;

/**
 * Util class to convert an input stream to AERIUS data structures.
 */
public class ImporterFacade {

  private static final Logger LOGGER = LoggerFactory.getLogger(ImporterFacade.class);

  private static final int MAX_FILES_IN_ZIP = 35;

  private final int minYear;
  private final int maxYear;
  private final double nettingFactorDefault;
  private final SectorCategories categories;
  private final GMLHelperImpl gmlHelper;

  private final List<ImportParcel> importParcels = new ArrayList<>();
  private final EnumSet<ImportOption> importOptions = ImportOption.getDefaultOptions();
  private ImportParcel importParcel;

  public ImporterFacade(final PMF pmf, final SectorCategories categories) throws AeriusException {
    this.categories = categories;
    gmlHelper = new GMLHelperImpl(pmf, categories);

    try (final Connection con = pmf.getConnection()) {
      this.minYear = ConstantRepository.getInteger(con, ConstantsEnum.MIN_YEAR);
      this.maxYear = ConstantRepository.getInteger(con, ConstantsEnum.MAX_YEAR);
      this.nettingFactorDefault = ConstantRepository.getDouble(con, SharedConstantsEnum.DEFAULT_NETTING_FACTOR);
    } catch (final SQLException e) {
      LOGGER.error("SQLException in ImportFacade#ImporterFacade", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public boolean isValidateStrict() {
    return ImportOption.VALIDATE_STRICT.in(importOptions);
  }

  /**
   * Sets all import options. Clears any previous set or default options.
   */
  public void setImportOptions(final Set<ImportOption> importOptions) {
    this.importOptions.clear();
    this.importOptions.addAll(importOptions);
  }

  public void setImportOption(final ImportOption option, final boolean set) {
    if (set) {
      importOptions.add(option);
    } else {
      importOptions.remove(option);
    }
  }

  /**
   * Read the data from the input stream and return a ImportParcel containing useable data.
   *
   * @param filename
   *   name of the input stream to import, used to determine type
   * @param inputStream
   *   Read data from this stream
   * @return {@link ImportParcel}
   * @throws AeriusException
   *   incase of an internal AERIUS specific error
   * @throws IOException
   *   on IO error
   */
  public List<ImportParcel> convertInputStream2ImportResult(final String filename, final InputStream inputStream)
      throws IOException, AeriusException {
    return convertInputStream2ImportResult(filename, inputStream, new ImportProperties());
  }

  /**
   * Read the data from the input stream and return a ImportParcel containing useable data.
   *
   * @param filename name of file to import
   * @param inputStream Read data from this stream
   * @param importProperties The import properties to use during import
   * @return {@link ImportParcel}
   * @throws AeriusException in case of an internal AERIUS specific error
   * @throws IOException on IO error
   */
  public List<ImportParcel> convertInputStream2ImportResult(final String filename, final InputStream inputStream,
      final ImportProperties importProperties) throws IOException, AeriusException {
    final ImportType type = checkFileType(filename);
    if (importParcel == null) {
      importParcel = new ImportParcel();
      importParcels.add(importParcel);
    }
    if (!importProperties.containsImportType(type)) {
      importParcel.getExceptions().add(new AeriusException(AeriusExceptionReason.IMPORT_FILE_UNSUPPORTED, filename));
      return importParcels;
    }
    Span.current().setAttribute("aer.import.filetype", type.name());
    switch (type) {
    case BRN:
      final BrnImportReader brnImportReader = new BrnImportReader(ImportOption.USE_VALID_SECTORS.in(importOptions),
          ImportOption.IMPORT_SOURCE_DIAMETER.in(importOptions));
      brnImportReader.read(filename, inputStream, categories, importProperties.getSubstance(), importParcel);
      break;
    case RCP:
      final RcpImportReader rcpImportReader = new RcpImportReader(
          ImportOption.USE_IMPORTED_LANDUSES.in(importOptions),
          ImportOption.USE_IMPORTED_RECEPTOR_HEIGHT.in(importOptions));
      rcpImportReader.read(filename, inputStream, categories, importProperties.getSubstance(), importParcel);
      break;
    case SRM2:
      convertCSV2ImportResult(filename, inputStream, importProperties);
      break;
    case GML:
      convertGML2ImportResult(inputStream, importProperties);
      break;
    case PAA:
      convertPAA2ImportResult(inputStream, importProperties);
      break;
    case APL, UPL:
      convertAPL2ImportResult(filename, inputStream);
      break;
    case ZIP:
      convertZIP2ImportResult(inputStream, importProperties);
      break;
    default:
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    GenericSourcesValidator.validateStrict(importParcel, isValidateStrict());
    return importParcels;
  }

  private void updateValuesIfNeeded() throws AeriusException {
    useDefaultNettingFactorIfNeeded();
    enforceYearWithinSupportedRange();
  }

  private void useDefaultNettingFactorIfNeeded() {
    for (final ImportParcel parcel : importParcels) {
      if (parcel.getSituation().getType() == SituationType.OFF_SITE_REDUCTION && parcel.getSituation().getNettingFactor() == null) {
        parcel.getSituation().setNettingFactor(nettingFactorDefault);
      }
    }
  }

  private void enforceYearWithinSupportedRange() throws AeriusException {
    for (final ImportParcel parcel : parcelsWithoutErrors()) {
      final int inputYear = parcel.getSituation().getYear();
      final int targetYear = Math.max(minYear, Math.min(maxYear, inputYear));

      if (inputYear != targetYear) {
        addYearNotWithinRangeWarning(inputYear);
        parcel.getSituation().setYear(targetYear);
        gmlHelper.enforceEmissions(parcel.getSituation().getYear(), parcel.getSituation().getEmissionSourcesList());
      }
    }
  }

  private void addYearNotWithinRangeWarning(final int initialYear) {
    importParcel.getWarnings().add(new AeriusException(AeriusExceptionReason.IMPORTED_YEAR_OUTSIDE_RANGE, Integer.toString(initialYear)));
  }

  private static ImportType checkFileType(final String filename) throws AeriusException {
    final ImportType type = ImportType.determineByFilename(filename);
    if (type == null) {
      throw new AeriusException(AeriusExceptionReason.IMPORT_FILE_UNSUPPORTED, filename);
    }
    return type;
  }

  private void convertPAA2ImportResult(final InputStream inputStream, final ImportProperties importProperties) throws IOException, AeriusException {
    // Need context to set default values when needed during conversion.
    // retrieve GML strings from paa.
    final PdfGMLs scenarioGMLs = PAAImport.importPAAFromStream(inputStream);

    if (scenarioGMLs.getAttachment() != null) {
      convertZIP2ImportResult(new ByteArrayInputStream(scenarioGMLs.getAttachment()), importProperties);
    } else {
      final boolean originalIncludeCalculationPoints = ImportOption.INCLUDE_CALCULATION_POINTS.in(importOptions);
      importOptions.remove(ImportOption.INCLUDE_CALCULATION_POINTS);

      final ImaerImporter imaerImporter = new ImaerImporter(gmlHelper);
      if (scenarioGMLs.isLegacy()) {
        convertLegacyPAA2ImportResults(scenarioGMLs, originalIncludeCalculationPoints, imaerImporter);
      } else if (scenarioGMLs.isLegacyMultiGml()) {
        convertMultipleSituationPAA2ImportResult(scenarioGMLs, originalIncludeCalculationPoints, imaerImporter);
      }
    }
    updateValuesIfNeeded();
  }

  private void convertMultipleSituationPAA2ImportResult(final PdfGMLs scenarioGMLs, final boolean originalIncludeCalculationPoints,
      final ImaerImporter imaerImporter) throws IOException, AeriusException {

    final Iterator<String> gmls = scenarioGMLs.getGmls().iterator();
    while (gmls.hasNext()) {
      final String gml = gmls.next();

      // Only include calculation points from the last GML. It doesn't matter which one really, because they all contain the same set.
      if (originalIncludeCalculationPoints && !gmls.hasNext()) {
        importOptions.add(ImportOption.INCLUDE_CALCULATION_POINTS);
      }

      try (final InputStream gmlInputStream = new ByteArrayInputStream(gml.getBytes(StandardCharsets.UTF_8))) {
        imaerImporter.importStream(gmlInputStream, safePdfOptions(), importParcel);
      }

      if (gmls.hasNext()) {
        importParcel = new ImportParcel();
        importParcels.add(importParcel);
      }
    }
  }

  private void convertLegacyPAA2ImportResults(final PdfGMLs scenarioGMLs, final boolean originalIncludeCalculationPoints,
      final ImaerImporter imaerImporter) throws IOException, AeriusException {
    // current situation first (if available).
    if (scenarioGMLs.getReferenceGML() != null) {
      try (final InputStream gmlInputStream = new ByteArrayInputStream(scenarioGMLs.getReferenceGML().getBytes(StandardCharsets.UTF_8))) {
        imaerImporter.importStream(gmlInputStream, safePdfOptions(), importParcel);
        importParcel.getSituation().setType(SituationType.REFERENCE);
        // Ensure proposed gets imported in different parcel
        importParcel = new ImportParcel();
        importParcels.add(importParcel);
      }
    }
    if (originalIncludeCalculationPoints) {
      importOptions.add(ImportOption.INCLUDE_CALCULATION_POINTS);
    }
    // proposed situation.
    try (final InputStream gmlInputStream = new ByteArrayInputStream(scenarioGMLs.getProposedGML().getBytes(StandardCharsets.UTF_8))) {
      imaerImporter.importStream(gmlInputStream, safePdfOptions(), importParcel);
      importParcel.getSituation().setType(SituationType.PROPOSED);
    }
  }

  private EnumSet<ImportOption> safePdfOptions() {
    // Ensure any results present that happen to be in these GMLs don't get imported.
    // This was possible at some point, but was unexpected and it makes no sense to import them.
    final EnumSet<ImportOption> pdfImportOptions = EnumSet.copyOf(importOptions);
    pdfImportOptions.remove(ImportOption.INCLUDE_RESULTS);
    return pdfImportOptions;
  }

  private void convertCSV2ImportResult(final String filename, final InputStream inputStream, final ImportProperties importProperties)
      throws IOException, AeriusException {
    final LegacyNSLImportReader srm2Importer = new LegacyNSLImportReader();
    srm2Importer.read(filename, inputStream, categories, importProperties.getSubstance(), importParcel);
    EmissionSourceValidator.checkIntersections(importParcel.getSituation(), importParcel.getExceptions());
    // If a year was supplied, use that to enforce emissions. Otherwise no year to base it on for these files, so use current year.
    final int enforceEmissionsYear = importProperties.getYear().orElse(Year.now().getValue());
    gmlHelper.enforceEmissions(enforceEmissionsYear, importParcel.getSituation().getEmissionSourcesList());
    // Do not set the year for the situation:
    // if we would do that, Connect can't throw an exception if calculation year hasn't been supplied for a job with just these files.
  }

  private void convertAPL2ImportResult(final String filename, final InputStream inputStream) throws IOException, AeriusException {
    final AplImportReader aplImportReader = new AplImportReader();
    aplImportReader.read(filename, inputStream, categories, null, importParcel);
  }

  private void convertZIP2ImportResult(final InputStream inputStream, final ImportProperties importProperties)
      throws IOException, AeriusException {
    boolean foundFiles = false;
    try (final ZipInputStream zipStream = new ZipInputStream(inputStream)) {
      ZipEntry zipEntry;
      int convertedFileInZip = 0;
      boolean onlyGML = true;

      while (convertedFileInZip <= MAX_FILES_IN_ZIP && (zipEntry = zipStream.getNextEntry()) != null) {
        final String name = zipEntry.getName();
        final ImportType entryMatchType = ImportType.determineByFilename(name);

        onlyGML &= ImportType.GML == entryMatchType;
        if (entryMatchType != null && convertZIPEntry(zipStream, zipEntry, convertedFileInZip > 0, importProperties)) {
          foundFiles = true;
          convertedFileInZip++;
        }
        // else skip this file, not supported.
      }

      // If we exceeded max size, remove the last parcel and report a warning on last parcel.
      if (convertedFileInZip > MAX_FILES_IN_ZIP) {
        importParcels.remove(importParcels.size() - 1);
        importParcel = importParcels.get(importParcels.size() - 1);
        importParcel.getWarnings()
        .add(new AeriusException(AeriusExceptionReason.ZIP_TOO_MANY_USABLE_FILES_WARNING, String.valueOf(MAX_FILES_IN_ZIP)));
      }

      if (convertedFileInZip == 2
          && onlyGML
          && importParcels.get(0).getSituation().getType() == null
          && importParcels.get(1).getSituation().getType() == null) {
        // Assume an old zip file, where order determined the type of the situation. First one reference, second proposed.
        importParcels.get(0).getSituation().setType(SituationType.REFERENCE);
        importParcels.get(1).getSituation().setType(SituationType.PROPOSED);
      }
    } catch (final EOFException e) {
      LOGGER.debug("Zip looks corrupt", e);
      throw new AeriusException(AeriusExceptionReason.ZIP_WITHOUT_USABLE_FILES);
    }
    if (!foundFiles) {
      throw new AeriusException(AeriusExceptionReason.ZIP_WITHOUT_USABLE_FILES);
    }
  }

  private boolean convertZIPEntry(final ZipInputStream zipStream, final ZipEntry zipEntry, final boolean notFirst,
      final ImportProperties importProperties) throws IOException, AeriusException {
    boolean converted = false;
    final String name = zipEntry.getName();
    final ImportType entryMatchType = ImportType.determineByFilename(name);

    if (entryMatchType != null) {
      if (notFirst) {
        importParcel = new ImportParcel();
        importParcels.add(importParcel);
      }
      try {
        convertInputStream2ImportResult(name, IOUtils.toBufferedInputStream(zipStream), importProperties);
        converted = true;
      } catch (final AeriusException e) {
        // non-AERIUS PDFs in a zip may be ignored, other errors must be passed on
        if (e.getReason() != AeriusExceptionReason.IMPORTED_PDF_NOT_AERIUS_PAA) {
          throw e;
        } else if (notFirst) {
          importParcels.remove(importParcel);
        }
      }
    }
    return converted;
  }

  private void convertGML2ImportResult(final InputStream inputStream, final ImportProperties importProperties) throws AeriusException {
    final ImaerImporter imaerImporter = new ImaerImporter(gmlHelper);

    // Need context to set default values when needed during conversion.
    imaerImporter.importStream(inputStream, importOptions, importParcel, importProperties.getYear(), importProperties.getTheme());

    Span.current().setAttribute("aer.import.imaer.aeriusVersion", importParcel.getVersion());
    Span.current().setAttribute("aer.import.imaer.gmlCreator", importParcel.getGmlCreator());

    updateValuesIfNeeded();
  }

  private List<ImportParcel> parcelsWithoutErrors() {
    return importParcels.stream()
        .filter(parcel -> parcel.getExceptions().isEmpty())
        .toList();
  }
}
