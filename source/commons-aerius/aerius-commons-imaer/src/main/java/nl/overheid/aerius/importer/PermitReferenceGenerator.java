/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.importer;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.gml.ReferenceGenerator;
import nl.overheid.aerius.shared.reference.ReferenceUtil;

/**
 *
 */
public class PermitReferenceGenerator implements ReferenceGenerator {

  private boolean overrideReference;

  public PermitReferenceGenerator() {
    this(true);
  }

  public PermitReferenceGenerator(final boolean overrideReference) {
    this.overrideReference = overrideReference;
  }

  @Override
  public Optional<String> updateReferenceIfNeeded(final String reference) {
    if (overrideReference || StringUtils.isEmpty(reference)) {
      return Optional.of(ReferenceUtil.generatePermitReference());
    } else {
      return Optional.empty();
    }
  }

  public void setOverrideReference(final boolean overrideReference) {
    this.overrideReference = overrideReference;
  }

}
