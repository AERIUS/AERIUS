/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius;

/**
 * Contains PAA constants to be shared across projects.
 */
public final class PAAConstants {

  public static final String CALC_PDF_METADATA_GML_KEY = "calculation";
  public static final String CALC_PDF_METADATA_2ND_GML_KEY = "secondCalculation";
  public static final String CALC_PDF_METADATA_GML_KEY_FORMAT = "calculation[%d]";
  public static final String CALC_PDF_METADATA_HASH_KEY = "validationKey";
  public static final String CALC_PDF_METADATA_HASH_SALT = "-=[(Hil1=PëPPér]=-";

  private PAAConstants() {
    // Default constructor utility class.
  }

}
