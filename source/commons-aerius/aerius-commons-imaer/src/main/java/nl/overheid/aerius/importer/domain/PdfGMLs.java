/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Object contains the GML strings/bytes from a PDF.
 */
public class PdfGMLs {

  // Legacy named strings
  private String proposedGML;
  private String referenceGML;

  // Legcay multiple strings. These strings contain the type of situation, so no named properties (or ordering) is needed.
  private List<String> gmls = new ArrayList<>();

  // Embedded zip attachement in pdf.
  private byte[] attachment;

  private String metadataHash;

  public PdfGMLs() {
  }

  public PdfGMLs(final List<String> gmls) {
    this.gmls = gmls;
  }

  public PdfGMLs(final byte[] attachment) {
    this.attachment = attachment;
  }

  public boolean isLegacy() {
    return proposedGML != null;
  }

  public boolean isLegacyMultiGml() {
    return !gmls.isEmpty();
  }

  public String getProposedGML() {
    return proposedGML;
  }

  public void setProposedGML(final String proposedGML) {
    this.proposedGML = proposedGML;
  }

  public String getReferenceGML() {
    return referenceGML;
  }

  public void setReferenceGML(final String referenceGML) {
    this.referenceGML = referenceGML;
  }

  public List<String> getGmls() {
    return gmls;
  }

  public void setGmls(final List<String> gmls) {
    this.gmls = gmls;
  }

  public byte[] getAttachment() {
    return attachment;
  }

  public String getMetadataHash() {
    return metadataHash;
  }

  public void setMetaDataHash(final String metadataHash) {
    this.metadataHash = metadataHash;
  }
}
