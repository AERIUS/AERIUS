/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.domain;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;

/**
 * Input class for an ImporterWorker. Causes earlier imported results to be saved to the database.
 */
public class SaveImportedResultsTaskInput implements ImporterInput {

  private static final long serialVersionUID = 2L;

  private final String jobKey;
  private final Scenario scenario;
  private final List<SituationWithFileId> situationFileIds;
  private final String archiveContributionFileId;

  /**
   * Constructor.
   *
   * @param jobKey The uuid of the task used to store results.
   * @param scenario The scenario to save results for.
   * @param situationFileIds The situations with their respective file IDs that were returned on initial import.
   * @param archiveContributionFileId The file ID for the archive contribution, if any should be imported.
   */
  public SaveImportedResultsTaskInput(final String jobKey, final Scenario scenario, final List<SituationWithFileId> situationFileIds,
      final String archiveContributionFileId) {
    this.jobKey = jobKey;
    this.scenario = scenario;
    this.situationFileIds = situationFileIds;
    this.archiveContributionFileId = archiveContributionFileId;
  }

  public String getJobKey() {
    return jobKey;
  }

  public Scenario getScenario() {
    return scenario;
  }

  public List<SituationWithFileId> getSituationFileIds() {
    return situationFileIds;
  }

  public String getArchiveContributionFileId() {
    return archiveContributionFileId;
  }

  @Override
  public List<String> fileIdsToCleanupOnSuccess() {
    final List<String> fileIds = new ArrayList<>();
    for (final SituationWithFileId situationWithFileId : situationFileIds) {
      fileIds.add(situationWithFileId.getFileId());
    }
    if (archiveContributionFileId != null) {
      fileIds.add(archiveContributionFileId);
    }
    return fileIds;
  }

  @Override
  public List<String> fileIdsToCleanupOnError() {
    return fileIdsToCleanupOnSuccess();
  }

}
