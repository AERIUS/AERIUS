/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import java.util.List;

import nl.overheid.aerius.characteristics.CharacteristicsSupplier;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.emissions.CategoryBasedEmissionFactorSupplier;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.EmissionsUpdater;
import nl.overheid.aerius.shared.emissions.SubSourceEmissionsCalculator;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.util.GeometryCalculatorImpl;
import nl.overheid.aerius.validation.CategoryBasedValidationHelper;
import nl.overheid.aerius.validation.ValidationHelper;

/**
 * Helper class to provide classes for emission (factor) calculations.
 */
public class SourceConversionHelper {

  private final GeometryCalculator geometryCalculator = new GeometryCalculatorImpl();
  private final ValidationHelper validationHelper;
  private final EmissionFactorSupplier emissionFactorSupplier;
  private final SubSourceEmissionsCalculator subSourceEmissionsCalculator;
  private final CharacteristicsSupplier characteristicsSupplier;

  public SourceConversionHelper(final PMF pmf, final SectorCategories sectorCategories, final int year) {
    final CharacteristicsType characteristicsType = ConstantRepository.getEnum(pmf, ConstantsEnum.CHARACTERISTICS_TYPE, CharacteristicsType.class);
    validationHelper = new CategoryBasedValidationHelper(sectorCategories, characteristicsType);
    this.emissionFactorSupplier = new CategoryBasedEmissionFactorSupplier(sectorCategories, pmf, year);
    this.subSourceEmissionsCalculator = new SubSourceEmissionsCalculator(emissionFactorSupplier);
    this.characteristicsSupplier = new CharacteristicsSupplier(sectorCategories, pmf, year);
  }

  public ValidationHelper getValidationHelper() {
    return validationHelper;
  }

  public SubSourceEmissionsCalculator getSubSourceEmissionsCalculator() {
    return subSourceEmissionsCalculator;
  }

  public CharacteristicsSupplier getCharacteristicsSupplier() {
    return characteristicsSupplier;
  }

  public void enforceEmissions(final List<EmissionSourceFeature> sources) throws AeriusException {
    final EmissionsUpdater updater = new EmissionsUpdater(emissionFactorSupplier, geometryCalculator);
    updater.updateEmissions(sources);
  }

}
