/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.validation;

import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.building.BuildingLimits;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;

/**
 * SectorCategories based ValidationHelper.
 */
public class CategoryBasedValidationHelper implements ValidationHelper {

  private final SectorCategories categories;
  private final BuildingLimits buildingLimits;

  public CategoryBasedValidationHelper(final SectorCategories categories, final CharacteristicsType characteristicsType) {
    this.categories = categories;
    this.buildingLimits = characteristicsType == CharacteristicsType.ADMS ? ADMSLimits.INSTANCE : OPSLimits.INSTANCE;
  }

  @Override
  public BuildingLimits buildingLimits() {
    return buildingLimits;
  }

  @Override
  public FarmLodgingValidationHelper farmLodgingValidation() {
    return new CategoryFarmLodgingValidationHelper(categories);
  }

  @Override
  public FarmAnimalHousingValidationHelper farmAnimalHousingValidation() {
    return new CategoryFarmAnimalHousingValidationHelper(categories);
  }

  @Override
  public FarmlandValidationHelper farmlandValidation() {
    return new CategoryFarmlandValidationHelper(categories);
  }

  @Override
  public ManureStorageValidationHelper manureStorageValidation() {
    return new CategoryManureStorageValidationHelper(categories);
  }

  @Override
  public OffRoadValidationHelper offRoadMobileValidation() {
    return new CategoryOffRoadValidationHelper(categories);
  }

  @Override
  public ColdStartValidationHelper coldStartValidation() {
    return new CategoryColdStartValidationHelper(categories);
  }

  @Override
  public RoadValidationHelper roadValidation() {
    return new CategoryRoadValidationHelper(categories);
  }

  @Override
  public InlandShippingValidationHelper inlandShippingValidation() {
    return new CategoryInlandShippingValidationHelper(categories);
  }

  @Override
  public MaritimeShippingValidationHelper maritimeShippingValidation() {
    return new CategoryMaritimeShippingValidationHelper(categories);
  }
}
