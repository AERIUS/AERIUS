/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.characteristics;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.InlandCategoryKey;
import nl.overheid.aerius.db.common.sector.category.ColdStartCategoryRepository;
import nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository;
import nl.overheid.aerius.shared.domain.emissions.InlandRouteKey;
import nl.overheid.aerius.shared.domain.emissions.MaritimeRouteKey;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.source.shipping.base.IsStandardShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.StandardInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.StandardMooringInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.ShippingMovementType;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.StandardMooringMaritimeShipping;
import nl.overheid.aerius.shared.emissions.shipping.ShippingLaden;

public class CharacteristicsSupplier {

  private final Map<InlandRouteKey, OPSSourceCharacteristics> cacheInlandRouteCharacteristics = new HashMap<>();
  private final Map<String, Map<ShippingLaden, OPSSourceCharacteristics>> cacheInlandDockedCharacteristics = new HashMap<>();
  private final Map<MaritimeRouteKey, OPSSourceCharacteristics> cacheMaritimeRouteCharacteristics = new HashMap<>();
  private final Map<String, OPSSourceCharacteristics> cacheMaritimeDockedCharacteristics = new HashMap<>();
  // Map<code, OPSSourceCharacteristics>
  private final Map<String, OPSSourceCharacteristics> cachedColdStartSpecific = new HashMap<>();
  // Map<code, OPSSourceCharacteristics>
  private final Map<String, OPSSourceCharacteristics> cachedColdStartStandard = new HashMap<>();

  private final SectorCategories categories;
  private final PMF pmf;
  private final int year;

  public CharacteristicsSupplier(final SectorCategories categories, final PMF pmf, final int year) {
    this.categories = categories;
    this.pmf = pmf;
    this.year = year;
  }

  public OPSSourceCharacteristics getSectorCharacteristics(final int sectorId) {
    return safe(categories.determineSectorById(sectorId).getDefaultCharacteristics());
  }

  public OPSSourceCharacteristics getColdStartSpecific(final String code) throws SQLException {
    if (!cachedColdStartSpecific.containsKey(code)) {
      try (final Connection con = pmf.getConnection()) {
        cachedColdStartSpecific.put(code, ColdStartCategoryRepository.findSpecificColdStartCharacteristics(con, code));
      }
    }
    return safe(cachedColdStartSpecific.get(code));
  }

  public OPSSourceCharacteristics getColdStartStandard(final String code) throws SQLException {
    if (!cachedColdStartStandard.containsKey(code)) {
      try (final Connection con = pmf.getConnection()) {
        cachedColdStartStandard.put(code, ColdStartCategoryRepository.findStandardColdStartCharacteristics(con, code));
      }
    }
    return safe(cachedColdStartStandard.get(code));
  }

  public Map<ShippingLaden, OPSSourceCharacteristics> getInlandDockedCharacteristics(final StandardMooringInlandShipping mooringInlandShipping)
      throws SQLException {
    final String shipCode = mooringInlandShipping.getShipCode();
    if (!cacheInlandDockedCharacteristics.containsKey(shipCode)) {
      cacheInlandDockedCharacteristics.put(shipCode, getInlandDockedCharacteristicsFromDB(shipCode));
    }
    return safe(cacheInlandDockedCharacteristics.get(shipCode));
  }

  private Map<ShippingLaden, OPSSourceCharacteristics> getInlandDockedCharacteristicsFromDB(final String shipCode)
      throws SQLException {
    final InlandShippingCategory category = inlandShipCategory(shipCode);
    try (final Connection con = pmf.getConnection()) {
      return ShippingCategoryRepository.getInlandCategoryMooringCharacteristics(con, category);
    }
  }

  public OPSSourceCharacteristics getInlandRouteCharacteristics(final StandardInlandShipping inlandShipping, final InlandWaterway waterway,
      final ShippingLaden laden) throws SQLException {
    final InlandRouteKey routeKey = new InlandRouteKey(waterway.getWaterwayCode(), waterway.getDirection(), laden, inlandShipping.getShipCode());
    if (!cacheInlandRouteCharacteristics.containsKey(routeKey)) {
      cacheInlandRouteCharacteristics.put(routeKey, getInlandRouteCharacteristicsFromDB(routeKey));
    }
    return safe(cacheInlandRouteCharacteristics.get(routeKey));
  }

  private OPSSourceCharacteristics getInlandRouteCharacteristicsFromDB(final InlandRouteKey routeKey) throws SQLException {
    final InlandShippingCategory shipCategory = inlandShipCategory(routeKey.getShipCode());
    final InlandWaterwayCategory waterwayCategory = inlandWaterwayCategory(routeKey.getWaterwayCode());
    try (final Connection con = pmf.getConnection()) {
      final Map<InlandCategoryKey, OPSSourceCharacteristics> characteristics = ShippingCategoryRepository.getInlandCategoryRouteCharacteristics(con,
          shipCategory);
      return characteristics.get(new InlandCategoryKey(routeKey.getDirection(), routeKey.getLaden(), waterwayCategory.getId()));
    }
  }

  public OPSSourceCharacteristics getMaritimeDockedCharacteristics(final StandardMooringMaritimeShipping mooringMaritimeShipping)
      throws SQLException {
    final String shipCode = mooringMaritimeShipping.getShipCode();
    if (!cacheMaritimeDockedCharacteristics.containsKey(shipCode)) {
      cacheMaritimeDockedCharacteristics.put(shipCode, getMaritimeDockedCharacteristicsFromDB(shipCode));
    }
    return safe(cacheMaritimeDockedCharacteristics.get(shipCode));
  }

  private OPSSourceCharacteristics getMaritimeDockedCharacteristicsFromDB(final String shipCode) throws SQLException {
    final MaritimeShippingCategory maritimeShip = maritimeShipCategory(shipCode);
    try (final Connection con = pmf.getConnection()) {
      return ShippingCategoryRepository.getDockedCharacteristics(con, maritimeShip, year);
    }
  }

  public OPSSourceCharacteristics getMaritimeRouteCharacteristics(final IsStandardShipping maritimeShipping, final ShippingMovementType movementType)
      throws SQLException {
    final MaritimeRouteKey routeKey = new MaritimeRouteKey(movementType, maritimeShipping.getShipCode());
    if (!cacheMaritimeRouteCharacteristics.containsKey(routeKey)) {
      cacheMaritimeRouteCharacteristics.put(routeKey, getMaritimeRouteCharacteristicsFromDB(routeKey));
    }
    return safe(cacheMaritimeRouteCharacteristics.get(routeKey));
  }

  private OPSSourceCharacteristics getMaritimeRouteCharacteristicsFromDB(final MaritimeRouteKey routeKey)
      throws SQLException {
    final MaritimeShippingCategory maritimeShip = maritimeShipCategory(routeKey.getShipCode());
    try (final Connection con = pmf.getConnection()) {
      return ShippingCategoryRepository.getCharacteristics(con, maritimeShip, routeKey.getMovementType(), year);
    }
  }

  private InlandShippingCategory inlandShipCategory(final String shipCode) {
    return categories.getInlandShippingCategories().getShipCategoryByCode(shipCode);
  }

  private InlandWaterwayCategory inlandWaterwayCategory(final String waterwayCode) {
    return categories.getInlandShippingCategories().getWaterwayCategoryByCode(waterwayCode);
  }

  private MaritimeShippingCategory maritimeShipCategory(final String shipCode) {
    return categories.determineMaritimeShippingCategoryByCode(shipCode);
  }

  private static Map<ShippingLaden, OPSSourceCharacteristics> safe(final Map<ShippingLaden, OPSSourceCharacteristics> original) {
    final Map<ShippingLaden, OPSSourceCharacteristics> copiedMap = new EnumMap<>(ShippingLaden.class);
    original.forEach((key, value) -> copiedMap.put(key, safe(value)));
    return copiedMap;
  }

  private static OPSSourceCharacteristics safe(final OPSSourceCharacteristics original) {
    return original == null ? null : original.copyTo(new OPSSourceCharacteristics());
  }

}
