/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.characteristics.CharacteristicsSupplier;
import nl.overheid.aerius.function.AeriusConsumer;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.ColdStartEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.CustomVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.SpecificVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardColdStartVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.Vehicles;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Converts {@link ColdStartEmissionSource} objects to individual objects with their own characteristics.
 */
public class ColdStartSourceExpander {

  private static final Logger LOGGER = LoggerFactory.getLogger(ColdStartSourceExpander.class);

  private final SourceConverter sourceConverter;
  private final SourceConversionHelper sourceConversionHelper;

  public ColdStartSourceExpander(final SourceConverter sourceConverter, final SourceConversionHelper sourceConversionHelper) {
    this.sourceConverter = sourceConverter;
    this.sourceConversionHelper = sourceConversionHelper;
  }

  /**
   * Expand the given cold start emission source to separate emission sources that have each their own specific characteristics.
   * Emissions are recalculated on the individual emission sources.
   *
   * @param con
   * @param expandedSources
   * @param emissionSource
   * @param geometry
   * @param substances
   * @param buildingTracker
   * @throws AeriusException
   */
  public void convert(final Connection con, final GroupedSourcesPacketMap expandedSources, final ColdStartEmissionSource emissionSource,
      final Geometry geometry, final List<Substance> substances) throws AeriusException {
    if (emissionSource.isVehicleBasedCharacteristics()) {
      convertOpenParking(con, expandedSources, emissionSource, geometry, substances);
    } else {
      convertParkingGarage(con, expandedSources, emissionSource, geometry, substances);
    }
  }

  // Convert open parking sources

  /**
   * Expands open parking sources to individual source with each different characteristics.
   */
  private void convertOpenParking(final Connection con, final GroupedSourcesPacketMap expandedSources, final ColdStartEmissionSource emissionSource,
      final Geometry geometry, final List<Substance> substances) throws AeriusException {
    final CharacteristicsSupplier characteristicsSupplier = sourceConversionHelper.getCharacteristicsSupplier();
    final AeriusConsumer<EmissionSource> converter = v -> sourceConverter.convert(con, expandedSources, v, geometry, substances);

    try {
      convertOpenParkingCustomVehicle(converter, characteristicsSupplier, emissionSource);
      convertOpenParkingSpecificVehicle(converter, characteristicsSupplier, emissionSource);
      convertOpenParkingStandardColdStartVehicle(converter, characteristicsSupplier, emissionSource);
    } catch (final SQLException e) {
      LOGGER.warn("SQLException when expanding cold start source {} to OPS sources", emissionSource.getGmlId(), e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Converts a open parking {@link CustomVehicles} sub sources to a new emission source.
   *
   * {@link OPSSourceCharacteristics} should be based on the vehicleType, copied over sector specific characteristics.
   */
  private void convertOpenParkingCustomVehicle(final AeriusConsumer<EmissionSource> converter, final CharacteristicsSupplier characteristicsSupplier,
      final ColdStartEmissionSource emissionSource) throws AeriusException, SQLException {
    final List<CustomVehicles> customVehicles = filter(emissionSource, CustomVehicles.class);

    for (final CustomVehicles vehicle : customVehicles) {
      final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();

      characteristicsSupplier.getColdStartStandard(vehicle.getVehicleType()).copyTo(characteristics);
      convertVehicle(converter, emissionSource, vehicle, characteristics);
    }
  }

  /**
   * Converts open parking {@link SpecificVehicles} sub sources to a new emission source.
   *
   * {@link OPSSourceCharacteristics} should be based on the vehicle code, copied over sector specific characteristics.
   */
  private void convertOpenParkingSpecificVehicle(final AeriusConsumer<EmissionSource> converter,
      final CharacteristicsSupplier characteristicsSupplier, final ColdStartEmissionSource emissionSource) throws AeriusException, SQLException {
    final List<SpecificVehicles> specificVehicles = filter(emissionSource, SpecificVehicles.class);

    for (final SpecificVehicles vehicle : specificVehicles) {
      final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();

      characteristicsSupplier.getColdStartSpecific(vehicle.getVehicleCode()).copyTo(characteristics);
      convertVehicle(converter, emissionSource, vehicle, characteristics);
    }
  }

  /**
   * Converts a open parking {@link StandardColdStartVehicles} sub sources to a new emission source.
   *
   * {@link OPSSourceCharacteristics} should be based on the vehicleType, copied over sector specific characteristics.
   */
  private void convertOpenParkingStandardColdStartVehicle(final AeriusConsumer<EmissionSource> converter,
      final CharacteristicsSupplier characteristicsSupplier, final ColdStartEmissionSource emissionSource) throws AeriusException, SQLException {
    final List<StandardColdStartVehicles> standardColdStartVehicles = filter(emissionSource, StandardColdStartVehicles.class);

    for (final StandardColdStartVehicles vehicle : standardColdStartVehicles) {
      for (final Entry<String, Double> standardVehicle : vehicle.getValuesPerVehicleTypes().entrySet()) {
        final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();

        characteristicsSupplier.getColdStartStandard(standardVehicle.getKey()).copyTo(characteristics);
        final ColdStartEmissionSource vehicleEmissionSource = createEmissionSource(emissionSource.getSectorId(), characteristics);

        vehicleEmissionSource
            .setEmissions(sourceConversionHelper.getSubSourceEmissionsCalculator().calculateColdStartEmissions(vehicle, standardVehicle.getKey()));
        converter.apply(vehicleEmissionSource);
      }
    }
  }

  private static <T extends Vehicles> List<T> filter(final ColdStartEmissionSource emissionSource, final Class<T> clazz) {
    return emissionSource.getSubSources().stream().filter(clazz::isInstance).map(clazz::cast).toList();
  }

  private static ColdStartEmissionSource createEmissionSource(final int sectorId, final OPSSourceCharacteristics characteristics) {
    final ColdStartEmissionSource es = new ColdStartEmissionSource();

    es.setSectorId(sectorId);
    es.setCharacteristics(characteristics);
    return es;
  }

  private void convertVehicle(final AeriusConsumer<EmissionSource> converter, final ColdStartEmissionSource emissionSource,
      final Vehicles vehicle, final OPSSourceCharacteristics characteristics) throws AeriusException {
    final ColdStartEmissionSource vehicleEmissionSource = createEmissionSource(emissionSource.getSectorId(), characteristics);

    vehicleEmissionSource.setEmissions(sourceConversionHelper.getSubSourceEmissionsCalculator().calculateColdStartEmissions(vehicle));
    converter.apply(vehicleEmissionSource);
  }

  // Convert parking garage sources

  /**
   * Expands a parking garage source.
   *
   * Parking garage sources have all the same characteristics, those from the parking garage.
   * It should just use the characteristics supplied by the user for the main source.
   */
  private void convertParkingGarage(final Connection con, final GroupedSourcesPacketMap expandedSources, final ColdStartEmissionSource emissionSource,
      final Geometry geometry, final List<Substance> substances) throws AeriusException {
    sourceConverter.convert(con, expandedSources, emissionSource, geometry, substances);
  }
}
