/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfDocumentInfo;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfObject;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfStream;

import nl.overheid.aerius.PAAConstants;
import nl.overheid.aerius.importer.domain.PdfGMLs;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.HashUtil;

/**
 * Util class to read a PAA import data from a stream and convert them to internal data structure objects.
 */
public final class PAAImport {
  private static final Logger LOG = LoggerFactory.getLogger(PAAImport.class);

  private PAAImport() {
  }

  public static PdfGMLs importPAAFromStream(final InputStream inputStream) throws AeriusException {
    final List<String> gmlStrings;
    final String metadataHash;
    final PdfGMLs scenarioGMLs;

    // Open pdf
    try (final PdfDocument pdfDoc = new PdfDocument(new PdfReader(inputStream))) {
      final PdfDocumentInfo info = pdfDoc.getDocumentInfo();

      if (LegacyPAAImport.isLegacyPAAImport(info)) {
        gmlStrings = LegacyPAAImport.readGMLStrings(info);
        scenarioGMLs = LegacyPAAImport.toScenarioGMLs(gmlStrings);
      } else if (MultiSituationPAAImport.isMultiSituationPAAImport(info)) {
        gmlStrings = MultiSituationPAAImport.readGMLStrings(info);
        scenarioGMLs = MultiSituationPAAImport.toScenarioGMLS(gmlStrings);
      } else {
        final byte[] attachment = getAttachment(pdfDoc);

        if (attachment == null) {
          throw new AeriusException(AeriusExceptionReason.IMPORTED_PDF_NOT_AERIUS_PAA);
        }
        // gmlStrings here only used to validate hash, and not passed to
        gmlStrings = List.of(new String(attachment, StandardCharsets.UTF_8));
        scenarioGMLs = new PdfGMLs(attachment);
      }

      // Get metadata hash
      metadataHash = readMetadataHash(info);
    } catch (final IOException e) {
      LOG.error("IOException while importing PAA", e);
      throw new AeriusException(AeriusExceptionReason.IMPORT_FILE_COULD_NOT_BE_READ);
    }

    scenarioGMLs.setMetaDataHash(metadataHash);
    // Validate hash
    validate(gmlStrings, metadataHash);

    return scenarioGMLs;
  }

  public static byte[] getAttachment(final PdfDocument pdfDoc) {
    for (int i = 1; i <= pdfDoc.getNumberOfPdfObjects(); i++) {
      final PdfObject obj = pdfDoc.getPdfObject(i);

      if (obj != null && obj.isStream() && PdfName.EmbeddedFile.equals(((PdfStream) obj).get(PdfName.Type))) {
        return ((PdfStream) obj).getBytes();
      }
    }
    return null;
  }

  private static String readMetadataHash(final PdfDocumentInfo info) throws AeriusException {
    // Try to get validation key
    final String calcPdfMetaDataHash = info.getMoreInfo(PAAConstants.CALC_PDF_METADATA_HASH_KEY);

    if (calcPdfMetaDataHash == null) {
      throw new AeriusException(AeriusExceptionReason.IMPORTED_PDF_NOT_AERIUS_PAA);
    }
    return calcPdfMetaDataHash;
  }

  public static void validate(final List<String> gmlStrings, final String metadataHash) throws AeriusException {
    final String hash = HashUtil.generateLegacySaltedHash(PAAConstants.CALC_PDF_METADATA_HASH_SALT, gmlStrings);

    if (!hash.equals(metadataHash)) {
      throw new AeriusException(AeriusExceptionReason.PAA_VALIDATION_FAILED);
    }
  }
}
