/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import java.sql.Connection;
import java.util.List;

import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface class to implement engine source specific ways to convert {@link EmissionSource} to {@link EngineSource} objects.
 */
public interface SourceConverter {

  /**
   * Expand line or polygon sources to lists of single point sources. A point source is just passed as is.
   * @param con Database connection
   * @param expanded Add converted engine sources to this map
   * @param originalSource EmissionSource to convert
   * @param geometry The geometry to convert
   * @return true if source was converted
   * @throws AeriusException
   */
  boolean convert(Connection con, GroupedSourcesPacketMap expanded, EmissionSource originalSource, final Geometry geometry,
      final List<Substance> substances) throws AeriusException;

  /**
   * Perform additional validation checks on an {@link EmissionSource} that are related to expanding the source to an {@link EngineSource}.
   *
   * The default implementation does nothing
   * @param emissionSource emission source to check
   * @throws AeriusException
   */
  default void validate(final EmissionSourceFeature emissionSource) throws AeriusException {
  }

  /**
   * Converts buildings. Should be called before converting emission sources.
   * Should keep track of converted buildings. Default empty implementation for when no buildings are supported.
   *
   * @throws AeriusException exception when conversion failed
   */
  default void convertBuildings() throws AeriusException {
  }

  /**
   * Called after sources are converted to {@link EngineEmissionSource} objects.
   * @param map with grouped sources.
   */
  default void postProcess(final GroupedSourcesPacketMap map) {
  }

  /**
   * Called before sources are converted to {@link EngineEmissionSource} objects.
   * @param situation the situation.
   * @throws AeriusException when pre-processing fails.
   */
  default void preProcess(final ScenarioSituation situation) throws AeriusException {
  }

}
