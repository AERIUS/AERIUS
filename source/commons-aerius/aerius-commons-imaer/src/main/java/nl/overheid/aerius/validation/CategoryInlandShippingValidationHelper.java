/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.validation;

import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;

class CategoryInlandShippingValidationHelper implements InlandShippingValidationHelper {

  private final InlandShippingCategories categories;

  CategoryInlandShippingValidationHelper(final SectorCategories categories) {
    this.categories = categories.getInlandShippingCategories();
  }

  @Override
  public boolean isValidInlandShipCode(final String shipCode) {
    return categories.getShipCategoryByCode(shipCode) != null;
  }

  @Override
  public boolean isValidInlandWaterwayCode(final String waterwayCode) {
    return categories.getWaterwayCategoryByCode(waterwayCode) != null;
  }

  @Override
  public boolean isValidInlandShipWaterwayCombination(final String shipCode, final String waterwayCode) {
    final InlandShippingCategory shippingCategory = categories.getShipCategoryByCode(shipCode);
    final InlandWaterwayCategory waterwayCategory = categories.getWaterwayCategoryByCode(waterwayCode);
    return shippingCategory != null && waterwayCategory != null && categories.isValidCombination(shippingCategory, waterwayCategory);
  }

}
