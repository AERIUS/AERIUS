/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.paa;

import java.util.ArrayList;
import java.util.List;

import com.itextpdf.kernel.pdf.PdfDocumentInfo;

import nl.overheid.aerius.PAAConstants;
import nl.overheid.aerius.importer.domain.PdfGMLs;

/**
 * Util class to read multi situation PAA import data and convert them to internal data structure objects.
 */
final class MultiSituationPAAImport {

  private MultiSituationPAAImport() {
  }

  protected static List<String> readGMLStrings(final PdfDocumentInfo info) {
    final List<String> gmlStrings = new ArrayList<>();

    int i = 0;
    String key = String.format(PAAConstants.CALC_PDF_METADATA_GML_KEY_FORMAT, i);
    String gmlString;
    while ((gmlString = info.getMoreInfo(key)) != null) {
      gmlStrings.add(gmlString);
      i += 1;
      key = String.format(PAAConstants.CALC_PDF_METADATA_GML_KEY_FORMAT, i);
    }

    return gmlStrings;
  }

  protected static PdfGMLs toScenarioGMLS(final List<String> gmls) {
    return new PdfGMLs(gmls);
  }

  protected static boolean isMultiSituationPAAImport(final PdfDocumentInfo info) {
    return info.getMoreInfo(String.format(PAAConstants.CALC_PDF_METADATA_GML_KEY_FORMAT, 0)) != null;
  }
}
