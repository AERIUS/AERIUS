/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static nl.overheid.aerius.shared.domain.Substance.NH3;
import static nl.overheid.aerius.shared.domain.Substance.NO2;
import static nl.overheid.aerius.shared.domain.Substance.NOX;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NH3_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NH3_DEPOSITION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NO2_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_DEPOSITION;

import java.sql.SQLException;
import java.util.List;

import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.adms.MetSiteRepository;
import nl.overheid.aerius.db.adms.MetSiteSurfaceCharacteristics;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.MetSurfaceCharacteristics;
import nl.overheid.aerius.shared.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Util class to set the default calculation options for a NCA {@link CalculationMethod#FORMAL_ASSESSMENT} calculation.
 */
public class NCACalculationOptionsUtil {

  private static final List<Substance> PERMIT_SUBSTANCES = List.of(NH3, NOX, NO2);
  private static final List<EmissionResultKey> PERMIT_EMISSION_RESULTKEYS = List.of(NOX_CONCENTRATION, NO2_CONCENTRATION, NH3_CONCENTRATION,
      NH3_DEPOSITION, NOX_DEPOSITION);
  private static final double MAX_DISTANCE_M = 15_000;
  // Default values in case something wasn't supplied properly.
  private static final String DEFAULT_METEO_YEAR = String.valueOf(2020);

  private final MetSiteRepository repository;
  private final ADMSVersion admsVersion;

  public NCACalculationOptionsUtil(final PMF pmf) {
    this(new MetSiteRepository(pmf),
        ADMSVersion.getADMSVersionByLabel(ConstantRepository.getString(pmf, ConstantsEnum.MODEL_VERSION_ADMS, false)));
  }

  public NCACalculationOptionsUtil(final MetSiteRepository repository, final ADMSVersion admsVersion) {
    this.repository = repository;
    this.admsVersion = admsVersion;
  }

  /**
   * Returns a {@link CalculationSetOptions} object configured to do a NCA calculation.
   *
   * @param options orignal options to enhance
   * @param useCustomPoints if true calculation method will be CUSTOM_POINTS
   * @return CalculationSetOptions object
   * @throws AeriusException
   */
  public CalculationSetOptions createCalculationSetOptions(final CalculationSetOptions options, final boolean useCustomPoints,
      final boolean demoMode) throws AeriusException {
    final CalculationSetOptions newOptions = new CalculationSetOptions();
    newOptions.getSubstances().addAll(PERMIT_SUBSTANCES);
    newOptions.getEmissionResultKeys().addAll(PERMIT_EMISSION_RESULTKEYS);
    newOptions.setCalculationMethod(options.getCalculationMethod());
    newOptions.setCalculationJobType(options.getCalculationJobType());
    newOptions.setUseInCombinationArchive(options.isUseInCombinationArchive());
    newOptions.setStacking(false);

    final NCACalculationOptions ncaOptions = options.getNcaCalculationOptions();
    final NCACalculationOptions newNcaOptions = newOptions.getNcaCalculationOptions();
    newNcaOptions.setPermitArea(ncaOptions.getPermitArea());
    newNcaOptions.setProjectCategory(ncaOptions.getProjectCategory());
    newNcaOptions.setDevelopmentPressureSourceIds(ncaOptions.getDevelopmentPressureSourceIds());
    newNcaOptions.setAdmsVersion(admsVersion.getVersion());
    final CalculationMethod ct = useCustomPoints ? CalculationMethod.CUSTOM_POINTS : options.getCalculationMethod();

    newOptions.setCalculationMethod(ct);
    if (ct == CalculationMethod.FORMAL_ASSESSMENT) {
      newNcaOptions.setAdmsOptions(defaultADMSOptions(ncaOptions.getAdmsOptions()));
      newOptions.setCalculateMaximumRange(MAX_DISTANCE_M);
    } else {
      newNcaOptions.setAdmsOptions(ncaOptions.getAdmsOptions());
      newNcaOptions.getAdmsOptions().setSpatiallyVaryingRoughness(true);
      newOptions.setCalculateMaximumRange(Math.max(0, Math.min(options.getCalculateMaximumRange(), MAX_DISTANCE_M)));
    }
    setMetSiteSurfaceCharacteristics(newNcaOptions.getAdmsOptions());
    setFNO2Options(newNcaOptions, ncaOptions);
    if (demoMode) {
      setADMSDemoMode(newNcaOptions.getAdmsOptions());
    }
    return newOptions;
  }

  private static ADMSOptions defaultADMSOptions(final ADMSOptions userAdmsOptions) {
    final ADMSOptions admsOptions = new ADMSOptions();

    admsOptions.setMinMoninObukhovLength(userAdmsOptions.getMinMoninObukhovLength());
    admsOptions.setSurfaceAlbedo(userAdmsOptions.getSurfaceAlbedo());
    admsOptions.setPriestleyTaylorParameter(userAdmsOptions.getPriestleyTaylorParameter());

    admsOptions.putMetSiteCharacteristics(DEFAULT_METEO_YEAR,
        MetSurfaceCharacteristics.builder()
        .roughness(ADMSLimits.ROUGHNESS_DEFAULT)
        .minMoninObukhovLength(ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT)
        .surfaceAlbedo(ADMSLimits.SURFACE_ALBEDO_DEFAULT)
        .priestleyTaylorParameter(ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_DEFAULT)
        .build());
    admsOptions.setPlumeDepletionNOX(userAdmsOptions.isPlumeDepletionNOX());
    admsOptions.setPlumeDepletionNH3(userAdmsOptions.isPlumeDepletionNH3());
    admsOptions.setSpatiallyVaryingRoughness(true);
    admsOptions.setComplexTerrain(userAdmsOptions.isComplexTerrain());

    admsOptions.setMetSiteId(userAdmsOptions.getMetSiteId());
    admsOptions.setMetDatasetType(userAdmsOptions.getMetDatasetType());
    admsOptions.setMetYears(userAdmsOptions.getMetYears().isEmpty() ? List.of(DEFAULT_METEO_YEAR) : userAdmsOptions.getMetYears());
    return admsOptions;
  }

  private static void setFNO2Options(final NCACalculationOptions newNcaOptions, final NCACalculationOptions ncaOptions) {
    newNcaOptions.setRoadLocalFractionNO2ReceptorsOption(ncaOptions.getRoadLocalFractionNO2ReceptorsOption());
    newNcaOptions.setRoadLocalFractionNO2PointsOption(ncaOptions.getRoadLocalFractionNO2PointsOption());
    newNcaOptions.setRoadLocalFractionNO2(ncaOptions.getRoadLocalFractionNO2());
  }

  /**
   * Disable Plume Depletion, Spatially Varying Roughness, and Complex Terrain when running in demo mode.
   *
   * @param admsOptions
   */
  private static void setADMSDemoMode(final ADMSOptions admsOptions) {
    admsOptions.setPlumeDepletionNOX(false);
    admsOptions.setPlumeDepletionNH3(false);
    admsOptions.setSpatiallyVaryingRoughness(false);
    admsOptions.setComplexTerrain(false);
  }

  /**
   * Set additional input, like Met characteristics, based on supplied options and the database.
   */
  private void setMetSiteSurfaceCharacteristics(final ADMSOptions admsOptions) throws AeriusException {
    final int metSiteId = admsOptions.getMetSiteId();
    if (metSiteId > 0) {
      for (final String meteoYear : admsOptions.getMetYears()) {
        try {
          final MetSiteSurfaceCharacteristics msc = repository.getMetSiteSurfaceCharacteristics(metSiteId, admsOptions.getMetDatasetType(), meteoYear)
              .orElseThrow(() -> new AeriusException(AeriusExceptionReason.ADMS_CALCULATION_OPTIONS_MET_SITE_ID_MISSING, String.valueOf(metSiteId)));

          admsOptions.putMetSiteCharacteristics(meteoYear, msc.metSurfaceCharacteristics());
          admsOptions.setMetSiteLatitude(msc.latitude());
        } catch (final SQLException e) {
          throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
        }
      }
    } else {
      // Allow it here, might trigger an exception when actually calculating but this way exports without calculation aren't hampered.
    }
  }
}
