/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.ops.conversion.EmissionSourceOPSConverter;
import nl.overheid.aerius.ops.conversion.OPSBuildingTracker;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.PolygonToPointsConverter;
import nl.overheid.aerius.util.WeightedPoint;

/**
 * Class to implement to convert {@link EmissionSource} to {@link OPSSource} objects.
 */
public class OPSSourceConverter implements SourceConverter {

  private static final Logger LOGGER = LoggerFactory.getLogger(OPSSourceConverter.class);

  private final double maxSegmentSize;
  private final Double maxPolygonToPointsGridSize;
  private final ScenarioSituation situation;
  private final PolygonToPointsConverter polygonConverter;

  private OPSBuildingTracker buildingTracker;

  public OPSSourceConverter(final Connection con, final ScenarioSituation situation) throws SQLException {
    maxSegmentSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_LINE_TO_POINTS_SEGMENT_SIZE, Double.class);
    maxPolygonToPointsGridSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_POLYGON_TO_POINTS_GRID_SIZE, Double.class);
    this.situation = situation;
    polygonConverter = new PolygonToPointsConverter(maxPolygonToPointsGridSize);
  }

  @Override
  public void convertBuildings() {
    buildingTracker = new OPSBuildingTracker(situation.getBuildingsList());
  }

  @Override
  public boolean convert(final Connection con, final GroupedSourcesPacketMap map, final EmissionSource originalSource, final Geometry geometry,
      final List<Substance> substances) throws AeriusException {
    try {
      final Collection<EngineSource> expanded = new ArrayList<>();
      final boolean converted = lineToPoints(expanded, originalSource, geometry, substances, buildingTracker)
          || polygonToPoints(con, expanded, originalSource, geometry, substances, buildingTracker)
          || pointToPoints(expanded, originalSource, geometry, substances, buildingTracker);

      map.addAll(originalSource.getSectorId(), CalculationEngine.OPS, expanded);
      return converted;
    } catch (final SQLException e) {
      LOGGER.error("SQLError in convertGenericGeometries", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private boolean pointToPoints(final Collection<EngineSource> expanded, final EmissionSource originalSource, final Geometry geometry,
      final List<Substance> substances, final OPSBuildingTracker buildingTracker) throws AeriusException {
    if (geometry instanceof final Point point) {
      expanded.addAll(
          EmissionSourceOPSConverter.convert(originalSource, point, originalSource.getEmissions(), expanded.size(), substances, buildingTracker,
              situation));
      return true;
    } else {
      return false;
    }
  }

  /**
   * Converts a line EmissionSource to a list of EmissionSources with points. The emission of the total line is evenly divided over the points.
   * <p>
   * This method checks if the emission source contains a line, if not this method does nothing.
   *
   * @param expanded Add new emission sources to this list
   * @param emissionSource EmissionSource to convert
   * @return returns true if emission source matched condition and was converted to points
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case converting the Geometry of the source to points failed.
   */
  boolean lineToPoints(final Collection<EngineSource> expanded, final EmissionSource originalSource, final Geometry geometry,
      final List<Substance> substances, final OPSBuildingTracker buildingTracker) throws SQLException, AeriusException {
    boolean converted = false;
    if (geometry instanceof final LineString lineString) {
      final List<Point> points = GeometryUtil.convertToPoints(lineString, maxSegmentSize);
      final Map<Substance, Double> pointEmissionValues = getEmissionValuesPerPoint(originalSource, points.size(), substances);

      for (final Point point : points) {
        expanded.addAll(
            EmissionSourceOPSConverter.convert(originalSource, point, pointEmissionValues, expanded.size(), substances, buildingTracker, situation));
      }
      converted = true;
    }
    return converted;
  }

  /**
   * Converts a polygon EmissionSource to a list of EmissionSources with points. The emission of the total polygon is evenly divided over the points.
   * <p>
   * This method checks if the emission source contains a polygon, if not this method does nothing.
   *
   * @param expanded Add new emission sources to this list
   * @param emissionSource EmissionSource to convert
   * @return returns true if emission source matched condition and was converted to points
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException
   */
  boolean polygonToPoints(final Connection con, final Collection<EngineSource> expanded, final EmissionSource emissionSource, final Geometry geometry,
      final List<Substance> substances, final OPSBuildingTracker buildingTracker) throws SQLException, AeriusException {
    boolean converted = false;
    if (geometry instanceof final Polygon polygon) {
      final List<WeightedPoint> weightedPoints = polygonConverter.convert(emissionSource.getLabel(), polygon);
      final double totalSurfaceRatio = calculateTotalSurfaceRatio(weightedPoints);

      for (final WeightedPoint weightedPoint : weightedPoints) {
        final Map<Substance, Double> emissionValues = calculatePolygonEmission(emissionSource, substances, totalSurfaceRatio, weightedPoint);
        setDiameter(emissionSource, weightedPoint);
        final Point point = new Point(weightedPoint.getX(), weightedPoint.getY());
        final List<? extends EngineSource> es = EmissionSourceOPSConverter.convert(emissionSource, point, emissionValues, expanded.size(), substances,
            buildingTracker, situation);
        expanded.addAll(es);
      }
      converted = true;
    }
    return converted;
  }

  private void setDiameter(final EmissionSource emissionSource, final WeightedPoint point) {
    if (emissionSource.getCharacteristics() instanceof final OPSSourceCharacteristics opsSourceCharacteristics) {
      opsSourceCharacteristics.setDiameter((int) Math.round(maxPolygonToPointsGridSize * Math.sqrt(point.getFactor())));
    }
  }

  private Map<Substance, Double> calculatePolygonEmission(final EmissionSource emissionSource, final List<Substance> substances,
      final double totalSurfaceRatio, final WeightedPoint point) {
    final Map<Substance, Double> emissionValues = new EnumMap<>(Substance.class);
    for (final Substance substance : substances) {
      final double totalWeightedEmission = emissionSource.getEmissions().getOrDefault(substance, 0.0) * point.getFactor();
      emissionValues.put(substance, totalWeightedEmission / totalSurfaceRatio);
    }
    return emissionValues;
  }

  private static double calculateTotalSurfaceRatio(final List<WeightedPoint> weightedPoints) {
    double totalSurfaceRatio = 0.0;
    for (final WeightedPoint weightedPoint : weightedPoints) {
      totalSurfaceRatio += weightedPoint.getFactor();
    }
    return totalSurfaceRatio;
  }

  private static Map<Substance, Double> getEmissionValuesPerPoint(final EmissionSource originalEmissionSource, final int numberOfPoints,
      final List<Substance> substances) {
    final Map<Substance, Double> ev = new EnumMap<>(Substance.class);
    for (final Substance key : substances) {
      // The result of getEmission of the emissionsource is a total, not per unit.
      ev.put(key, originalEmissionSource.getEmissions().getOrDefault(key, 0.0) / numberOfPoints);
    }
    return ev;
  }
}
