/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;

/**
 * Util class to stack similar sources.
 */
final class SourceStacker {

  private SourceStacker() {
    // Util class
  }

  public static Collection<EngineSource> stack(final Collection<EngineSource> sources) {
    final Map<Integer, List<EngineSource>> map = new HashMap<>();

    for (final EngineSource engineSource : sources) {
      putEngineSource(map, engineSource);
    }
    return map.entrySet().stream().flatMap(e -> e.getValue().stream()).collect(Collectors.toList());
  }

  /**
   * Add the source to the map. If the source already was in the map it was same source and thus the emission of that source needs to be added to
   * the newly inserted source as that source replaces the source in the map.
   *
   * @param map map with uniquely sources
   * @param src source to add to the map
   */
  private static void putEngineSource(final Map<Integer, List<EngineSource>> map, final EngineSource src) {
    final int hashCode = src.hashCode();
    final List<EngineSource> list = map.get(hashCode);

    if (list == null) {
      final List<EngineSource> srcList = new ArrayList<>();

      srcList.add(src);
      map.put(hashCode, srcList);
    } else {
      // Checks if the content of src matches the content of the elements in the list. This to not rely on hashcode alone.
      final Optional<EngineSource> match = list.stream().filter(e -> e.equals(src)).findAny();

      if (match.isPresent()) {
        final EngineSource prev = match.get();

        if (src instanceof EngineEmissionSource && prev instanceof EngineEmissionSource) {
          ((EngineEmissionSource) src).getEmissions().forEach(
              (s, e) -> ((EngineEmissionSource) prev).setEmission(s, ((EngineEmissionSource) prev).getEmission(s) + e));
        }
      } else {
        list.add(src);
      }
    }
  }
}
