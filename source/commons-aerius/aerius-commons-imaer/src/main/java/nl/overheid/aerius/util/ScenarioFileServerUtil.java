/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.io.IOException;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Util class for communicating with the file server on scenario file content.
 */
public class ScenarioFileServerUtil {

  private static final Logger LOG = LoggerFactory.getLogger(ScenarioFileServerUtil.class);

  private final FileServerProxy fileServerProxy;

  public ScenarioFileServerUtil(final FileServerProxy fileServerProxy) {
    this.fileServerProxy = fileServerProxy;
  }

  /**
   * Retrieves a Scenario object, given the id, from the file server.
   *
   * @param id id of scenario to retrieve
   * @return Scenario object
   * @throws AeriusException exception in casse scenario could not be retrieved
   */
  public Scenario retrieveScenario(final String id) throws AeriusException {
    try {
      LOG.debug("Reading file from fileservice with id '{}'", id);
      final String scenarioString = fileServerProxy.getContent(FileServerFile.DATA.format(id));

      return ScenarioObjectMapperUtil.getScenario(scenarioString);
    } catch (final URISyntaxException | HttpException | IOException e) {
      LOG.error("Unable to retrieve scenario file '{}' from fileservice: {}", id, e.getMessage());
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  /**
   * Removes the scenario, and related files, from the file server, but not the output file (like zipped GML).
   *
   * @param id id of scenario to delete
   */
  public void removeScenario(final String id) {
    LOG.debug("Cleaning up file from fileservice with fileCode '{}'", id);
    try {
      fileServerProxy.deleteRemoteContent(FileServerFile.DATA.format(id));
      fileServerProxy.deleteRemoteContent(FileServerFile.VALIDATION.format(id));
      fileServerProxy.deleteRemoteContent(FileServerFile.RESULTS.format(id));
    } catch (final IOException | HttpException | URISyntaxException e) {
      LOG.warn("Error cleaning up scenario with id '{}' from fileservice: {}", id, e.getMessage());
    }
  }
}
