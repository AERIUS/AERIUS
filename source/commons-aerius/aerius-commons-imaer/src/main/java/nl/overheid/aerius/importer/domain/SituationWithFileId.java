/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.domain;

import java.io.Serializable;

/**
 * Object to reference both a situation and a file ID.
 */
public class SituationWithFileId implements Serializable {

  private static final long serialVersionUID = 1L;

  private final String situationId;
  private final String fileId;

  /**
   * Constructor required by jackson.
   */
  SituationWithFileId() {
    this(null, null);
  }

  public SituationWithFileId(final String situationId, final String fileId) {
    this.situationId = situationId;
    this.fileId = fileId;
  }

  public String getSituationId() {
    return situationId;
  }

  public String getFileId() {
    return fileId;
  }

}
