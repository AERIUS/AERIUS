/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.validation;

import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

class CategoryFarmLodgingValidationHelper implements FarmLodgingValidationHelper {

  private final FarmLodgingCategories categories;

  CategoryFarmLodgingValidationHelper(final SectorCategories categories) {
    this.categories = categories.getFarmLodgingCategories();
  }

  @Override
  public boolean isValidFarmLodgingCode(final String lodgingCode) {
    return categories.determineFarmLodgingCategoryByCode(lodgingCode) != null;
  }

  @Override
  public boolean isValidFarmLodgingAdditionalSystemCode(final String systemCode) {
    return categories.determineAdditionalLodgingSystemByCode(systemCode) != null;
  }

  @Override
  public boolean isValidFarmLodgingReductiveSystemCode(final String systemCode) {
    return categories.determineReductiveLodgingSystemByCode(systemCode) != null;
  }

  @Override
  public boolean isValidFarmLodgingFodderMeasureCode(final String fodderMeasureCode) {
    return categories.determineLodgingFodderMeasureByCode(fodderMeasureCode) != null;
  }

  @Override
  public boolean canFodderApplyToLodging(final String fodderMeasureCode, final String lodgingCode) {
    final FarmLodgingCategory lodgingCategory = categories.determineFarmLodgingCategoryByCode(lodgingCode);
    final FarmLodgingFodderMeasureCategory fodderMeasureCategory = categories.determineLodgingFodderMeasureByCode(fodderMeasureCode);
    return lodgingCategory != null && fodderMeasureCategory != null && fodderMeasureCategory.canApplyToFarmLodgingCategory(lodgingCategory);
  }

  @Override
  public FarmEmissionFactorType getLodgingEmissionFactorType(final String lodgingCode) {
    return categories.determineFarmLodgingCategoryByCode(lodgingCode).getFarmEmissionFactorType();
  }

}
