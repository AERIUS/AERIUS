/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.domain;

import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;

/**
 * Input class for an import using the ImporterWorker. Causes the supplied file to be imported.
 */
public class ImportTaskInput implements ImporterInput {

  private static final long serialVersionUID = 1L;

  private final String uuid;
  private final String importFilename;
  private final String originalFilename;
  private final ImportProperties importProperties;
  private final FileServerExpireTag expire;
  private final Locale locale;

  /**
   * Constructor.
   *
   * @param uuid uuid of the task used to get map of the data related to this import task
   * @param importFilename name of the file on the file service to import
   * @param originalFilename original filename of the import
   * @param importProperties properties of the import
   * @param expire expire tags of the import on the file server
   * @param locale locale of the import
   */
  public ImportTaskInput(final String uuid, final String importFilename, final String originalFilename, final ImportProperties importProperties,
      final FileServerExpireTag expire, final Locale locale) {
    this.uuid = uuid;
    this.importFilename = importFilename;
    this.originalFilename = originalFilename;
    this.importProperties = importProperties;
    this.expire = expire;
    this.locale = locale;
  }

  public String getUuid() {
    return uuid;
  }

  public String getImportFilename() {
    return importFilename;
  }

  public String getOriginalFilename() {
    return originalFilename;
  }

  public ImportProperties getImportProperties() {
    return importProperties;
  }

  public FileServerExpireTag getExpire() {
    return expire;
  }

  public Locale getLocale() {
    return locale;
  }

  @Override
  public List<String> fileIdsToCleanupOnSuccess() {
    // Nothing to clean up, files should stay for UI purposes
    return List.of();
  }

  @Override
  public List<String> fileIdsToCleanupOnError() {
    // Nothing to clean up, files should stay for UI purposes
    return List.of();
  }

}
