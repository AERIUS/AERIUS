/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.domain;

import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;

/**
 * Input class for an import using the ImporterWorker. Causes the supplied file to be summarized.
 */
public class SummarizeTaskInput implements ImporterInput {

  private static final long serialVersionUID = 2L;

  private final String jobKey;
  private final String importFilename;
  private final String originalFilename;
  private final ImportProperties importProperties;
  private final FileServerExpireTag expire;
  private final Locale locale;
  private final Integer maximumResultsDistance;
  private final boolean decisionFramework;

  /**
   * Constructor.
   *
   * @param jobKey job key of the import job
   * @param importFilename name of the file on the file service to import
   * @param originalFilename original filename of the import
   * @param importProperties properties of the import
   * @param expire expire tags of the import on the file server
   * @param locale locale of the import
   * @param maximumResultsDistance maximum distance of included results
   * @param decisionFramework Indication if decision framework related information has to be included
   */
  public SummarizeTaskInput(final String jobKey, final String importFilename, final String originalFilename,
      final ImportProperties importProperties, final FileServerExpireTag expire, final Locale locale, final Integer maximumResultsDistance,
      final boolean decisionFramework) {
    this.jobKey = jobKey;
    this.importFilename = importFilename;
    this.originalFilename = originalFilename;
    this.importProperties = importProperties;
    this.expire = expire;
    this.locale = locale;
    this.maximumResultsDistance = maximumResultsDistance;
    this.decisionFramework = decisionFramework;
  }

  public String getJobKey() {
    return jobKey;
  }

  public String getImportFilename() {
    return importFilename;
  }

  public String getOriginalFilename() {
    return originalFilename;
  }

  public ImportProperties getImportProperties() {
    return importProperties;
  }

  public FileServerExpireTag getExpire() {
    return expire;
  }

  public Locale getLocale() {
    return locale;
  }

  public Integer getMaximumResultsDistance() {
    return maximumResultsDistance;
  }

  public boolean isDecisionFramework() {
    return decisionFramework;
  }

  @Override
  public List<String> fileIdsToCleanupOnSuccess() {
    // Normally don't clean up, as the result is stored under jobkey as well.
    return List.of();
  }

  @Override
  public List<String> fileIdsToCleanupOnError() {
    return List.of(jobKey);
  }

}
