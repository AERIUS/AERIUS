/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.importer.ImporterFacade;
import nl.overheid.aerius.io.ImportableFile;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.LocaleUtils;

public class ImaerController {

  private static final Logger LOG = LoggerFactory.getLogger(ImaerController.class);

  private final PMF pmf;
  private final Map<Locale, SectorCategories> sectorCategoriesMap;

  public ImaerController(final PMF pmf) {
    this.pmf = pmf;
    sectorCategoriesMap = LocaleUtils.getLocales().stream()
        .collect(Collectors.toMap(Function.identity(), locale -> {
          try {
            return SectorRepository.getSectorCategories(pmf, locale);
          } catch (final SQLException e) {
            throw new RuntimeException(e);
          }
        }));
  }

  public List<ImportParcel> processFile(final ImportableFile inputFile, final Set<ImportOption> options,
      final ImportProperties importProperties, final Locale locale) throws AeriusException {
    try {
      return importParcel(inputFile, importProperties, options, locale);
    } catch (final IOException e) {
      LOG.error("Error during processing file", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private List<ImportParcel> importParcel(final ImportableFile inputFile, final ImportProperties importProperties,
      final Set<ImportOption> options, final Locale locale) throws IOException, AeriusException {
    final ImporterFacade facade = new ImporterFacade(pmf, sectorCategoriesMap.get(locale));

    facade.setImportOptions(options);
    try (InputStream is = inputFile.asInputStream()) {
      return facade.convertInputStream2ImportResult(inputFile.getFileName(), is, importProperties);
    }
  }
}
