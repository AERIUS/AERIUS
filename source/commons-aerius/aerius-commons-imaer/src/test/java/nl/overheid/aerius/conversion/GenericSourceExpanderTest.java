/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link GenericSourceExpander}.
 */
class GenericSourceExpanderTest extends BaseDBTest {

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
  }

  @Test
  void testLineToPoints() throws SQLException, AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setGeometry(getExampleLineWKTGeometry());

    final GenericEmissionSource emissionSource = new GenericEmissionSource();
    feature.setProperties(emissionSource);
    TestDomain.getGenericEmissionSource(emissionSource);

    final List<EmissionSourceFeature> sources = new ArrayList<>();
    sources.add(feature);
    final List<EngineSource> pointSources = expand(sources);
    double calculatedEmission = 0d;
    for (final EngineSource es : pointSources) {
      calculatedEmission += ((EngineEmissionSource) es).getEmission(Substance.NH3);
    }
    assertEquals(emissionSource.getEmissions().get(Substance.NH3), calculatedEmission, 0.0001d,
        "Compare emission of # points:" + pointSources.size() + ", emission per point:"
            + ((EngineEmissionSource) pointSources.get(0)).getEmission(Substance.NH3));
  }

  @Test
  void testPolygonToPoints() throws SQLException, AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setGeometry(getExamplePolygonWKTGeometry());

    final EmissionSource emissionSource = TestDomain.getGenericEmissionSource();
    feature.setProperties(emissionSource);

    final List<EmissionSourceFeature> sources = new ArrayList<>();
    sources.add(feature);
    final List<EngineSource> pointSources = expand(sources);
    double calculatedEmission = 0d;
    for (final EngineSource es : pointSources) {
      calculatedEmission += ((EngineEmissionSource) es).getEmission(Substance.NH3);
    }
    assertEquals(emissionSource.getEmissions().get(Substance.NH3),
        calculatedEmission, 0.0001d, "Compare emission of # points:" + pointSources.size() + ", emission per point:"
            + ((EngineEmissionSource) pointSources.get(0)).getEmission(Substance.NH3));
    pointSources.forEach(p -> assertTrue(((OPSSource) p).getDiameter() > 0, "Diameter should be set"));
  }

  private List<EngineSource> expand(final List<EmissionSourceFeature> sourceList) throws SQLException, AeriusException {
    final SourceConversionHelper conversionHelper = new SourceConversionHelper(getPMF(), null, 0);
    final ScenarioSituation situation = new ScenarioSituation();
    final SourceConverter geometryExpander = new OPSSourceConverter(getPMF().getConnection(), situation);

    situation.getEmissionSourcesList().addAll(sourceList);
    return EngineSourceExpander.toEngineSources(getConnection(), geometryExpander, conversionHelper, situation, List.of(Substance.NH3), true);
  }

  public static Geometry getExampleLineWKTGeometry() {
    final LineString lineString = new LineString();
    lineString.setCoordinates(new double[][] {
      {166430, 471689},
      {167214, 464592},
    });

    return lineString;
  }

  private static Geometry getExamplePolygonWKTGeometry() {
    final Polygon polygon = new Polygon();

    polygon.setCoordinates(new double[][][] {{
      {166430, 471689},
      {167214, 464592},
      {167284, 469592},
      {166430, 471689}
    }});
    return polygon;
  }
}
