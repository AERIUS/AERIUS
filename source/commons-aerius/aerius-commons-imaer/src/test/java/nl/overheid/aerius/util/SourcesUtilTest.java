/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;

/**
 *
 */
class SourcesUtilTest {

  @ParameterizedTest
  @MethodSource("casesFilterSourcesWithEmission")
  void testFilterSourcesWithEmissionEmptyList(final List<EmissionSourceFeature> sources, final int expectedSourcesAfterFilter) {
    final List<EmissionSourceFeature> filteredSources = SourcesUtil.filterSourcesWithEmission(sources);

    assertNotSame(sources, filteredSources, "List shouldn't be identical");
    assertEquals(expectedSourcesAfterFilter, filteredSources.size(), "Number of sources after filtering");
  }

  private static Stream<Arguments> casesFilterSourcesWithEmission() {
    return Stream.of(
        Arguments.of(List.of(), 0),
        Arguments.of(List.of(createSource(0)), 0),
        Arguments.of(List.of(createSource(2.2)), 1),
        Arguments.of(List.of(createSource(2.2), createSource(0)), 1));
  }

  private static EmissionSourceFeature createSource(final double emission) {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final GenericEmissionSource source = new GenericEmissionSource();
    source.getEmissions().put(Substance.NOX, emission);
    source.getEmissions().put(Substance.NH3, 0.0);
    feature.setProperties(source);
    return feature;
  }

}
