/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import static nl.overheid.aerius.shared.exception.AeriusExceptionReason.CSV_ID_ADJUSTED;
import static nl.overheid.aerius.shared.exception.AeriusExceptionReason.IMPORTED_PDF_NOT_AERIUS_PAA;
import static nl.overheid.aerius.shared.exception.AeriusExceptionReason.NSL_LEGACY_FILESUPPORT_WILL_BE_REMOVED;
import static nl.overheid.aerius.shared.exception.AeriusExceptionReason.ZIP_WITHOUT_USABLE_FILES;
import static nl.overheid.aerius.shared.exception.ImaerExceptionReason.GML_GEOMETRY_INTERSECTS;
import static nl.overheid.aerius.shared.exception.ImaerExceptionReason.LIMIT_LINE_LENGTH_ZERO;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.ops.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.EmissionSourceLimits;

/**
 * Test class for {@link ImporterFacade}.
 */
class ImporterFacadeTest extends BaseDBTest {

  private static final int EXPECTED_IMPORT_YEAR = 2024;

  private ImporterFacade importer;

  @BeforeEach
  void before() throws SQLException, AeriusException {
    importer = new ImporterFacade(getPMF(), getCategories(getPMF()));
  }

  @Test
  void testZeroLineLength() throws AeriusException, IOException {
    final EmissionSourceLimits limits = new EmissionSourceLimits();
    final String filename = "zero_line_length.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      limits.setMaxSources(1000);
      limits.setMaxLineLength(10);
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(filename, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel result = results.get(0);
      assertEquals(1, result.getExceptions().size(), "Nr of exceptions");
      final AeriusException e = result.getExceptions().get(0);
      assertEquals(LIMIT_LINE_LENGTH_ZERO, e.getReason(), "Exception " + LIMIT_LINE_LENGTH_ZERO);
    }
  }

  @Test
  void testSMR2ZeroLineLength() throws AeriusException, IOException {
    final EmissionSourceLimits limits = new EmissionSourceLimits();
    final String filename = "zero_line_length.csv";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      limits.setMaxSources(1000);
      limits.setMaxLineLength(10);
      importer.setImportOption(ImportOption.VALIDATE_SOURCES, true);
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(filename, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel result = results.get(0);
      assertEquals(2, result.getExceptions().size(), "Nr of exceptions");
      for (final AeriusException e : result.getExceptions()) {
        assertEquals(LIMIT_LINE_LENGTH_ZERO, e.getReason(), "Exception " + LIMIT_LINE_LENGTH_ZERO);
      }
    }
  }

  @Test
  void testLineIntersects() throws IOException {
    final String filename = "line_intersects.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      importer.convertInputStream2ImportResult(filename, inputStream);
    } catch (final AeriusException e) {
      assertEquals(GML_GEOMETRY_INTERSECTS, e.getReason(), "LineIntersects error code");
      assertEquals("source 2", e.getArgs()[0], "LineIntersects name check");
    }
  }

  @Test
  void testPolygonIntersects() throws AeriusException, IOException {
    final String filename = "geometry_intersects.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(filename, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel result = results.get(0);
      assertEquals(1, result.getExceptions().size(), "Nr of exceptions");
      final AeriusException e = result.getExceptions().get(0);
      assertEquals(GML_GEOMETRY_INTERSECTS, e.getReason(), "PolygonIntersects error code");
      assertEquals("source 2", e.getArgs()[0], "PolygonIntersects name check");
    }
  }

  @Test
  void testGMLImportWithCustomPointsWithoutResults() throws IOException, AeriusException {
    final String filename = "custom_calculation_points_with_results.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(filename, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel result = results.get(0);

      assertNoExceptions(result);
      assertEquals(15, result.getCalculationPointsList().size(), "Count nr. of calculation points");
      assertNull(result.getSituationResults(), "Shouldn't be any results imported");
    }
  }

  @Test
  void testGMLImportWithCustomPointsWithResults() throws IOException, AeriusException {
    final String filename = "custom_calculation_points_with_results.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      importer.setImportOption(ImportOption.INCLUDE_RESULTS, true);
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(filename, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel result = results.get(0);

      assertNoExceptions(result);
      assertNotNull(result.getSituationResults(), "Should be results imported");
      assertFalse(result.getSituationResults().getResults().isEmpty(), "Should have result points");
      assertFalse(result.getSituationResults().getResults().get(0).getProperties().getResults().isEmpty(), "AeriusResultPoint should have results");
    }
  }

  @Test
  void testGMLImportWithCalculationResults() throws IOException, AeriusException {
    final String filename = "with_calculation_results.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(filename, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel result = results.get(0);

      assertNoExceptions(result);
      assertTrue(result.getCalculationPointsList().isEmpty(), "Count nr. of calculation points");
    }
  }

  @Test
  void testPDFImportLegacy() throws AeriusException, IOException {
    final String fileName = "AERIUS_bijlage_test.pdf";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(fileName, inputStream);
      assertEquals(2, results.size(), "Number of results");
      for (final ImportParcel result : results) {
        assertNoExceptions(result);
        assertEquals(0, result.getCalculationPointsList().size(), "Number of custom calculation points");
        assertEquals(EXPECTED_IMPORT_YEAR, result.getSituation().getYear(), "Imported year");
      }
      final ScenarioSituation situation1 = results.get(0).getSituation();
      assertEquals(1, situation1.getEmissionSourcesList().size(), "Number of sources");
      assertEquals("AERIUS", situation1.getEmissionSourcesList().get(0).getProperties().getLabel(), "Name of source");
      final ScenarioSituation situation2 = results.get(1).getSituation();
      assertEquals(1, situation2.getEmissionSourcesList().size(), "Number of sources in variant");
    }
  }

  @Test
  void testPDFImportMultiSituation() throws IOException, AeriusException {
    final String fileName = "AERIUS_bijlage_test_2.pdf";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(fileName, inputStream);
      assertEquals(3, results.size(), "Number of results");
      final ImportParcel firstParcel = results.get(0);
      assertEquals("dummy", firstParcel.getSituation().getName(), "Name of first parcel");
      assertEquals(SituationType.PROPOSED, firstParcel.getSituation().getType(), "Type of first situation");

      final ImportParcel secondParcel = results.get(1);
      assertEquals("Situatie 3", secondParcel.getSituation().getName(), "Name of second parcel");
      assertEquals(SituationType.REFERENCE, secondParcel.getSituation().getType(), "Type of second situation");

      final ImportParcel thirdParcel = results.get(2);
      assertEquals("Situatie 4", thirdParcel.getSituation().getName(), "Name of third parcel");
      assertEquals(SituationType.OFF_SITE_REDUCTION, thirdParcel.getSituation().getType(), "Type of third situation");
    }
  }

  @Test
  void testPDFImportResultsPresent() throws IOException, AeriusException {
    final String fileName = "AERIUS_bijlage_test_3.pdf";
    importer.setImportOption(ImportOption.INCLUDE_RESULTS, true);
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(fileName, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel parcel = results.get(0);
      assertEquals(1, parcel.getCalculationPointsList().size(), "Number of custom calculation points");
      final CalculationPointFeature feature = parcel.getCalculationPointsList().get(0);
      assertEquals(83675, feature.getGeometry().getRoundedX(), "X of point");
      assertEquals(462920, feature.getGeometry().getRoundedY(), "Y of point");
      assertEquals("mijn punt", feature.getProperties().getLabel(), "Label of point");
      assertTrue(feature.getProperties().getResults().isEmpty(),
          "Point should have no results imported, even though these are present and import option is used.");
    }
  }

  /**
   * Import pdf with gml as attachment.
   */
  @Test
  void testPDFWithAttachmentImport() throws AeriusException, IOException {
    final String fileName = "AERIUS_bijlagen_test_4_with_attachment.pdf";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(fileName, inputStream);

      for (final ImportParcel result : results) {
        assertNoExceptions(result);
      }
      final ScenarioSituation situation1 = results.get(0).getSituation();
      assertEquals(2, situation1.getEmissionSourcesList().size(), "Not the expected number of sources");
      assertEquals(1, results.get(0).getCalculationPoints().getFeatures().size(), "Not the expected number of calculation points");
    }
  }

  @Test
  void testPDFNotAerius() throws IOException {
    final String fileName = "unusable-pdf.pdf";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final AeriusException exception = assertThrows(AeriusException.class, () -> importer.convertInputStream2ImportResult(fileName, inputStream),
          "Should thrown an AeriusException because no valid pdf");
      assertEquals(IMPORTED_PDF_NOT_AERIUS_PAA, exception.getReason(), "Error code for invalid files exception");
    }
  }

  @Test
  void testZIPImportComparison() throws AeriusException, IOException {
    final String fileName = "AERIUS_gml_test_comparison.zip";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(fileName, inputStream);
      assertEquals(2, results.size(), "Number of results");
      for (final ImportParcel result : results) {
        assertNoExceptions(result);
        assertEquals(0, result.getCalculationPointsList().size(), "Number of custom calculation points");
        assertEquals(EXPECTED_IMPORT_YEAR, result.getSituation().getYear(), "Imported year");
      }
      final ScenarioSituation situation1 = results.get(0).getSituation();
      assertEquals(1, situation1.getEmissionSourcesList().size(), "Number of sources");
      assertEquals("voor verhuizing", situation1.getName(), "Name of original list");
      final ScenarioSituation situation2 = results.get(1).getSituation();
      assertEquals(1, situation2.getEmissionSourcesList().size(), "Number of sources in variant");
      assertEquals("na verhuizing", situation2.getName(), "Name of variant list");

    }
  }

  @Test
  void testZIPImportSingle() throws AeriusException, IOException {
    final String fileName = "AERIUS_gml_test_single.zip";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(fileName, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel result = results.get(0);
      assertNoExceptions(result);
      final ScenarioSituation situation = result.getSituation();
      assertEquals(1, situation.getEmissionSourcesList().size(), "Number of sources");
      assertEquals("na verhuizing", situation.getName(), "Name of variant list");
      assertEquals(0, result.getCalculationPointsList().size(), "Number of custom calculation points");
      assertEquals(EXPECTED_IMPORT_YEAR, result.getSituation().getYear(), "Imported year");
    }
  }

  /**
   * case 1: empty zip file
   */
  @Test
  void testZIPImportIncorrectEmptyFile() throws IOException {
    final String fileName1 = "AERIUS_incorrect_import_1.zip";
    try (final InputStream inputStream = getFileInputStream(fileName1)) {
      importer.convertInputStream2ImportResult(fileName1, inputStream);
      fail("Should get an exception on empty zip file");
    } catch (final AeriusException e) {
      assertEquals(ZIP_WITHOUT_USABLE_FILES, e.getReason(), "Error code for empty zip exception");
    }
  }

  /**
   * case 2: zip file containing no aerius import-able files (by extension).
   */
  @Test
  void testZIPImportIncorrectNoValidFiles() throws IOException {
    final String fileName2 = "AERIUS_incorrect_import_2.zip";
    try (final InputStream inputStream = getFileInputStream(fileName2)) {
      importer.convertInputStream2ImportResult(fileName2, inputStream);
      fail("Should get an exception on zip file with no import-able files");
    } catch (final AeriusException e) {
      assertEquals(ZIP_WITHOUT_USABLE_FILES, e.getReason(), "Error code for unusable files in zip exception");
    }
  }

  /**
   * case 3: zip file containing aerius importable files (by extension) but not an actual aerius file.
   */
  @Test
  void testZIPImportIncorrectNoAeriusFile() throws IOException {
    final String fileName3 = "AERIUS_incorrect_import_3.zip";
    try (final InputStream inputStream = getFileInputStream(fileName3)) {
      importer.convertInputStream2ImportResult(fileName3, inputStream);
      fail("Should get an exception with valid file extension but file content incorrect");
    } catch (final AeriusException e) {
      assertEquals(ZIP_WITHOUT_USABLE_FILES, e.getReason(), "Error code for invalid files exception");
    }
  }

  /**
   * case 4: zip file containing 3 GML files.
   */
  @Test
  void testZIPImportWith3GML() throws IOException, AeriusException {
    final String fileName = "AERIUS_incorrect_import_4.zip";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(fileName, inputStream);
      // While in older versions of AERIUS it was only possible to import 2 GMLs, we now accept any number.
      assertEquals(3, results.size(), "Should import all gml files");
    }
  }

  @Test
  void testRCPImportWithLandUses() throws AeriusException, IOException {
    final String fileName = "receptors_with_landuse.rcp";
    importer.setImportOption(ImportOption.USE_IMPORTED_LANDUSES, true);
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(fileName, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel result = results.get(0);
      assertNoExceptions(result);
      assertEquals(239, result.getCalculationPointsList().size(), "Number of receptors");

      for (final CalculationPointFeature point : result.getCalculationPointsList()) {
        assertTrue(point.getProperties() instanceof OPSCustomCalculationPoint, "Point should be OPS receptor");
        assertAeriusPoint(point);
        final OPSCustomCalculationPoint opsPoint = (OPSCustomCalculationPoint) point.getProperties();
        assertNotNull(opsPoint.getTerrainProperties().getAverageRoughness(), "Average roughness for point " + opsPoint.getId());
        assertNotNull(opsPoint.getTerrainProperties().getLandUse(), "LandUse for point " + opsPoint.getId());
        assertNotNull(opsPoint.getTerrainProperties().getLandUses(), "LandUse array for point " + opsPoint.getId());
      }
      assertSpecificPoint(result.getCalculationPointsList().get(0), "4245393");
    }
  }

  @Test
  void testRCPImportWithoutLandUses() throws AeriusException, IOException {
    final String fileName = "receptors_with_landuse.rcp";
    importer.setImportOption(ImportOption.USE_IMPORTED_LANDUSES, false);
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(fileName, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel result = results.get(0);
      assertNoExceptions(result);
      assertEquals(239, result.getCalculationPointsList().size(), "Number of receptors");
      for (final CalculationPointFeature point : result.getCalculationPointsList()) {
        assertSame(CustomCalculationPoint.class, point.getProperties().getClass(), "Point class should be AeriusPoint (without landuses and such)");
        assertAeriusPoint(point);
      }
      assertSpecificPoint(result.getCalculationPointsList().get(0), "1");
    }
  }

  @Test
  void testSRM2ConvertExceptions() throws IOException, AeriusException {
    final String fileName = "srm2_roads_example_exeception.csv";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      importer.setImportOption(ImportOption.VALIDATE_SOURCES, true);
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(fileName, inputStream);
      assertEquals(1, results.size(), "Number of results");
      final ImportParcel result = results.get(0);

      assertFalse(result.getWarnings().isEmpty(), "Warning list should not be empty");
      assertEquals(2, result.getWarnings().size(), "Warning list length");
      assertFalse(result.getExceptions().isEmpty(), "Exception list should not be empty");
      assertEquals(1, result.getExceptions().size(), "Exception list length");

      final List<Reason> allowedWarnings = Arrays.asList(NSL_LEGACY_FILESUPPORT_WILL_BE_REMOVED, CSV_ID_ADJUSTED);
      for (final AeriusException warning : result.getWarnings()) {
        assertTrue(allowedWarnings.contains(warning.getReason()), "Warning should be one of" + allowedWarnings);
      }

      for (final AeriusException exception : result.getExceptions()) {
        assertEquals(AeriusExceptionReason.IO_EXCEPTION_NUMBER_FORMAT, exception.getReason(),
            "Exception should be " + AeriusExceptionReason.IO_EXCEPTION_NUMBER_FORMAT);
      }
    }
  }

  private void assertNoExceptions(final ImportParcel result) {
    assertEquals(0, result.getExceptions().size(), "No exceptions:" + ArrayUtils.toString(result.getExceptions()));
  }

  private void assertAeriusPoint(final CalculationPointFeature point) {
    assertFalse(point.getId() == null || point.getId().isBlank(), "ID of point");
    assertNotEquals(0, point.getGeometry().getCoordinates()[0], 1E-5, "X-coord of point " + point.getId());
    assertNotEquals(0, point.getGeometry().getCoordinates()[1], 1E-5, "y-coord of point " + point.getId());
    assertNotNull(point.getProperties().getLabel(), "Label for point " + point.getId());
  }

  private void assertSpecificPoint(final CalculationPointFeature point, final String id) {
    assertEquals(id, point.getId(), "ID of specific point");
    assertEquals(168879, point.getGeometry().getCoordinates()[0], 1E-5, "X-coord of specific point");
    assertEquals(445950, point.getGeometry().getCoordinates()[1], 1E-5, "y-coord of specific point");
    assertEquals("4245393", point.getProperties().getLabel(), "Label for specific point");
    if (point.getProperties() instanceof OPSCustomCalculationPoint) {
      final OPSCustomCalculationPoint opsPoint = (OPSCustomCalculationPoint) point.getProperties();
      assertEquals(0.028959, opsPoint.getTerrainProperties().getAverageRoughness(), 1E-5, "Average roughness for specific point");
      assertEquals(LandUse.GRASS, opsPoint.getTerrainProperties().getLandUse(), "LandUse for specific point");
      assertArrayEquals(new int[] {98, 0, 0, 0, 0, 2, 0, 0, 0}, opsPoint.getTerrainProperties().getLandUses(), "LandUse array values");
    }
  }

  @Override
  protected InputStream getFileInputStream(final String fileName) throws FileNotFoundException {
    return super.getFileInputStream("/importer/" + fileName);
  }
}
