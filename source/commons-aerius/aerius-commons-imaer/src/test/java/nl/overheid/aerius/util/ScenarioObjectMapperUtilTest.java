/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.ops.OPSCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.source.FarmAnimalHousingEmissionSource;

class ScenarioObjectMapperUtilTest {

  @Test
  void testGetScenarioWithOPSCustomCalculationPoint() throws IOException, URISyntaxException {
    final Scenario scenario = ScenarioObjectMapperUtil.getScenario(readContent("ops_custom_calculation_points"));

    assertEquals(OPSCustomCalculationPoint.class, scenario.getCustomPoints().getFeatures().get(0).getProperties().getClass(),
        "The calculation point should be an OPSCustomCalculationPoint");
  }

  @Test
  void testGetScenarioWithLocalDate() throws IOException, URISyntaxException {
    final Scenario scenario = ScenarioObjectMapperUtil.getScenario(readContent("FarmAnimalHousingEmissionSource.json"));

    assertEquals(1, scenario.getSituations().size(),
        "The scenario should contain 1 situation");
    assertEquals(1, scenario.getSituations().get(0).getEmissionSourcesList().size(),
        "The situation should contain 1 source");
    assertEquals(FarmAnimalHousingEmissionSource.class, scenario.getSituations().get(0).getEmissionSourcesList().get(0).getProperties().getClass(),
        "The scenario should contain a farm lodging emission source");
    final FarmAnimalHousingEmissionSource source = (FarmAnimalHousingEmissionSource) scenario.getSituations().get(0).getEmissionSourcesList().get(0)
        .getProperties();
    assertEquals(LocalDate.of(2021, 6, 9), source.getEstablished(), "Read local date");
  }

  private String readContent(final String filename) throws IOException, URISyntaxException {
    return String.join(System.lineSeparator(), Files.readAllLines(Path.of(getClass().getResource(filename).toURI())));
  }
}
