/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.lenient;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.db.adms.MetSiteRepository;
import nl.overheid.aerius.db.adms.MetSiteSurfaceCharacteristics;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.MetDatasetType;
import nl.overheid.aerius.shared.domain.calculation.MetSurfaceCharacteristics;
import nl.overheid.aerius.shared.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link NCACalculationOptionsUtil}.
 */
@ExtendWith(MockitoExtension.class)
class NCACalculationOptionsUtilTest {

  private static final double MIN_MONIN_OBUKHOV_LENGTH = (float) (ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT + 10.0);
  private static final int MET_SITE_ID = 123;
  private static final int NON_EXISTING_MET_SITE_ID = 1;

  @Mock private MetSiteRepository repository;
  private final ADMSVersion version = ADMSVersion.NCA_LATEST;
  @Mock private MetSiteSurfaceCharacteristics metSiteSurfaceCharacteristics;

  private NCACalculationOptionsUtil util;
  private CalculationSetOptions options;

  @BeforeEach
  void before() throws SQLException {
    util = new NCACalculationOptionsUtil(repository, version);
    options = new CalculationSetOptions();
    final MetSurfaceCharacteristics msc = MetSurfaceCharacteristics.builder().minMoninObukhovLength(MIN_MONIN_OBUKHOV_LENGTH).build();
    lenient().doReturn(msc).when(metSiteSurfaceCharacteristics).metSurfaceCharacteristics();
    lenient().doReturn(Optional.ofNullable(metSiteSurfaceCharacteristics)).when(repository)
        .getMetSiteSurfaceCharacteristics(MET_SITE_ID, MetDatasetType.NWP_3KM2, "2020");
  }

  @Test
  void testPermitOptions() throws AeriusException {
    setCommon(CalculationMethod.FORMAL_ASSESSMENT, MET_SITE_ID);
    final CalculationSetOptions newOptions = util.createCalculationSetOptions(options, false, false);

    assertEquals(15_000, newOptions.getCalculateMaximumRange(), "Check if maximum range is set.");
    assertEquals(MET_SITE_ID, newOptions.getNcaCalculationOptions().getAdmsOptions().getMetSiteId(), "Check if met site id is set.");
  }

  @Test
  void testOtherNatureArea() throws AeriusException {
    setCommon(CalculationMethod.NATURE_AREA, MET_SITE_ID);
    options.setCalculateMaximumRange(30_000);
    final CalculationSetOptions newOptions = util.createCalculationSetOptions(options, false, false);

    assertEquals(15_000, newOptions.getCalculateMaximumRange(), "Check if maximum range is set.");
  }

  @Test
  void testPermitArea() throws AeriusException {
    setCommon(CalculationMethod.NATURE_AREA, MET_SITE_ID);
    final NCACalculationOptions ncaOptions = options.getNcaCalculationOptions();
    final String area = "OurArea";
    ncaOptions.setPermitArea(area);
    final CalculationSetOptions newOptions = util.createCalculationSetOptions(options, false, false);
    assertEquals(area, newOptions.getNcaCalculationOptions().getPermitArea(),
        "Should have value for permit area");
  }

  @Test
  void testProjectCategory() throws AeriusException {
    setCommon(CalculationMethod.NATURE_AREA, MET_SITE_ID);
    final String projectCategory = "OurCategory";
    options.getNcaCalculationOptions().setProjectCategory(projectCategory);
    final CalculationSetOptions newOptions = util.createCalculationSetOptions(options, false, false);
    assertEquals(projectCategory, newOptions.getNcaCalculationOptions().getProjectCategory(),
        "Should have value for project category");
  }

  @Test
  void testDevelopmentPressureSourceIds() throws AeriusException {
    setCommon(CalculationMethod.NATURE_AREA, MET_SITE_ID);
    final List<String> sourceIds = List.of("SomeSource", "OtherSource");
    options.getNcaCalculationOptions().setDevelopmentPressureSourceIds(sourceIds);
    final CalculationSetOptions newOptions = util.createCalculationSetOptions(options, false, false);
    assertEquals(sourceIds, newOptions.getNcaCalculationOptions().getDevelopmentPressureSourceIds(),
        "Should have value for development pressure source ids");
  }

  @Test
  void testFNO2Options() throws AeriusException {
    setCommon(CalculationMethod.NATURE_AREA, MET_SITE_ID);
    final NCACalculationOptions ncaOptions = options.getNcaCalculationOptions();
    ncaOptions.setRoadLocalFractionNO2ReceptorsOption(RoadLocalFractionNO2Option.ONE_CUSTOM_VALUE);
    ncaOptions.setRoadLocalFractionNO2PointsOption(RoadLocalFractionNO2Option.INDIVIDUAL_CUSTOM_VALUES);
    ncaOptions.setRoadLocalFractionNO2(10.0);
    final CalculationSetOptions newOptions = util.createCalculationSetOptions(options, false, false);
    assertEquals(RoadLocalFractionNO2Option.ONE_CUSTOM_VALUE, newOptions.getNcaCalculationOptions().getRoadLocalFractionNO2ReceptorsOption(),
        "Should have value for fNO2 receptors");
    assertEquals(RoadLocalFractionNO2Option.INDIVIDUAL_CUSTOM_VALUES, newOptions.getNcaCalculationOptions().getRoadLocalFractionNO2PointsOption(),
        "Should have value for fNO2 points");
    assertEquals(10.0, newOptions.getNcaCalculationOptions().getRoadLocalFractionNO2(), 0.1, "Should have value for fNO2 factor");
  }

  @Test
  void testOtherForceCustomPoints() throws AeriusException {
    setCommon(CalculationMethod.NATURE_AREA, MET_SITE_ID);
    options.setCalculateMaximumRange(30_000);
    final CalculationSetOptions newOptions = util.createCalculationSetOptions(options, true, false);

    assertEquals(15_000, newOptions.getCalculateMaximumRange(), "Check if maximum range is set.");
    assertEquals(CalculationMethod.CUSTOM_POINTS, newOptions.getCalculationMethod(), "Calculation type should be custom if forced.");
  }

  @Test
  void testMetSiteIdDBOptions() throws AeriusException {
    setCommon(CalculationMethod.NATURE_AREA, MET_SITE_ID);
    options.setCalculateMaximumRange(30_000);
    final CalculationSetOptions newOptions = util.createCalculationSetOptions(options, false, false);

    final ADMSOptions admsOptions = newOptions.getNcaCalculationOptions().getAdmsOptions();
    assertEquals(MIN_MONIN_OBUKHOV_LENGTH, admsOptions.getMetSiteCharacteristics("2020").getMinMoninObukhovLength(),
        "Check if min_monin_obukhov_length is retrieved from the database.");
  }

  @Test
  void testUnsetMetSiteID() {
    options.setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
    assertThrowsMetSiteIdInvalid();
  }

  @Test
  void testInvalidMetSiteID() {
    assertThrowsMetSiteIdInvalid();
  }

  @Test
  void testUnsetMetDataSetType() {
    options.setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
    options.getNcaCalculationOptions().getAdmsOptions().setMetSiteId(MET_SITE_ID);
    assertThrowsMetSiteIdInvalid();
  }

  @Test
  void testUnsetMetYear() {
    options.setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
    options.getNcaCalculationOptions().getAdmsOptions().setMetSiteId(MET_SITE_ID);
    options.getNcaCalculationOptions().getAdmsOptions().setMetDatasetType(MetDatasetType.NWP_3KM2);
    assertThrowsMetSiteIdInvalid();
  }

  private void assertThrowsMetSiteIdInvalid() {
    // Set MetSiteId to a non-exisiting id. Should trigger exception.
    options.getNcaCalculationOptions().getAdmsOptions().setMetSiteId(NON_EXISTING_MET_SITE_ID);
    final AeriusException exception = assertThrows(AeriusException.class, () -> util.createCalculationSetOptions(options, false, false), "");

    assertEquals(AeriusExceptionReason.ADMS_CALCULATION_OPTIONS_MET_SITE_ID_MISSING, exception.getReason(),
        "Should throw exception met site id incorrect");
  }

  private void setCommon(final CalculationMethod calculationMethod, final int metSiteId) {
    options.setCalculationMethod(calculationMethod);
    options.getNcaCalculationOptions().getAdmsOptions().setMetSiteId(metSiteId);
    options.getNcaCalculationOptions().getAdmsOptions().setMetDatasetType(MetDatasetType.NWP_3KM2);
    options.getNcaCalculationOptions().getAdmsOptions().setMetYears(List.of("2020"));
  }
}
