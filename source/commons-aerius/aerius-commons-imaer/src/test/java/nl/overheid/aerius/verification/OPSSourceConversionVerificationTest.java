/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.verification;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.conversion.EngineSourceExpander;
import nl.overheid.aerius.conversion.OPSSourceConverter;
import nl.overheid.aerius.conversion.SourceConversionHelper;
import nl.overheid.aerius.conversion.SourceConverter;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.importer.ImporterFacade;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.BrnFileReader;
import nl.overheid.aerius.ops.io.BrnFileWriter;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.SourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.OSUtils;

/**
 * Unit test to verify the conversion of AERIUS sources to OPS sources. This unit test reads a set of gml input files
 * and converts them to OPS source files. Those files are tested against reference OPS source files.
 * Each gml file verification is run as a single test.
 */
class OPSSourceConversionVerificationTest extends BaseDBTest {

  private static final String UNITTEST_RESOURCE_PATH = OPSSourceConversionVerificationTest.class.getResource(".").getPath();
  private static final Logger LOG = LoggerFactory.getLogger(OPSSourceConversionVerificationTest.class);
  private static final boolean CREATE_MISSING_FILE = false;

  private SectorCategories sectorCategories;

  @TempDir File tempDir;

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
  }

  @BeforeEach
  void beforeEach() throws SQLException {
    sectorCategories = getCategories(getPMF());
  }

  /**
   * Constant used to check if all available verification files have been handled. This is just a check if a file
   * might be missed because somewhere in the test something happened, causing the file to be skipped.
   */
  private static final int EXPECTED_COUNT = 28;

  private static final Predicate<String> GML_FILENAME_FILTER = name -> name != null && ImportType.determineByFilename(name) == ImportType.GML;

  /**
   * Comparator for sorting ops source objects so they can be compared item by item.
   */
  private static final Comparator<OPSSource> OPS_SORT_COMPARATOR = new Comparator<OPSSource>() {
    @Override
    public int compare(final OPSSource o1, final OPSSource o2) {
      final int cx = Double.compare(o1.getPoint().getX(), o2.getPoint().getX());
      final int cy = cx == 0 ? Double.compare(o1.getPoint().getY(), o2.getPoint().getY()) : cx;
      final int d = cy == 0 ? Integer.compare(o1.getDiameter(), o2.getDiameter()) : cy;
      final int dv = d == 0 ? Integer.compare(o1.getDiurnalVariation(), o2.getDiurnalVariation()) : d;
      final int hc = dv == 0 ? Double.compare(o1.getHeatContent(), o2.getHeatContent()) : dv;
      final int s = hc == 0 ? Double.compare(o1.getSpread(), o2.getSpread()) : hc;
      final int psd = s == 0 ? Integer.compare(o1.getParticleSizeDistribution(), o2.getParticleSizeDistribution()) : s;
      final int enox = psd == 0 ? Double.compare(o1.getEmission(Substance.NOX), o2.getEmission(Substance.NOX)) : psd;
      final int enh3 = enox == 0 ? Double.compare(o1.getEmission(Substance.NH3), o2.getEmission(Substance.NH3)) : enox;
      return enh3 == 0 ? Double.compare(o1.getEmissionHeight(), o2.getEmissionHeight()) : enh3;
    }
  };
  private static final List<Substance> RESULT_SUBSTANCES = List.of(Substance.NH3, Substance.NOX);

  private static List<File> getGMLFiles() throws IOException {
    return FileUtil.getFilteredFiles(new File(UNITTEST_RESOURCE_PATH), GML_FILENAME_FILTER);
  }

  /**
   * Initialize test with a specific file. testId is used to check if number of GML files found is as expected.
   */
  static List<Object[]> data() throws IOException {
    final List<Object[]> fileNames = new ArrayList<>();
    int i = 0;
    for (final File file : getGMLFiles()) {
      final String filename = file.getAbsolutePath().substring(UNITTEST_RESOURCE_PATH.length() - (OSUtils.isWindows() ? 1 : 0));

      for (final Substance substance : RESULT_SUBSTANCES) {
        final Object[] f = new Object[3];
        f[0] = i++;
        f[1] = filename;
        f[2] = substance;
        fileNames.add(f);
      }
    }
    return fileNames;
  }

  @Test
  void testNumberofGMLFiles() throws IOException {
    assertEquals(EXPECTED_COUNT, getGMLFiles().size(), "Number of gml files tested");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testConversionValidation(final Integer testId, final String testFilename, final Substance substance)
      throws SQLException, IOException, AeriusException {
    final File file = new File(UNITTEST_RESOURCE_PATH, testFilename);
    final String fileWithoutExt = file.getPath().substring(0, file.getPath().lastIndexOf("."));
    // Read and import reference input GML sources file
    final OPSInputData input = readReferenceDataFRomGML(substance, file);
    final File opsFile = new File(fileWithoutExt + "_" + substance.getName().toLowerCase(Locale.ROOT) + ".brn");
    // Get reference OPS file, if exists for substance check results
    if (opsFile.exists()) {
      LOG.info("Verify {}", opsFile);
      // Write computed OPS sources to file to make sure it matches exactly the expected ops sources, including roundings due to file format.
      final File computedTempPath = Files.createDirectories(new File(tempDir, UUID.randomUUID().toString()).toPath()).toFile();
      BrnFileWriter.writeFile(computedTempPath, input.getEmissionSources().get(1).stream().sorted(OPS_SORT_COMPARATOR).toList(),
          substance, null);
      final File computedTempFile = new File(computedTempPath, BrnFileWriter.getFileName());
      LOG.trace("Converted output file: {}", computedTempFile);
      // Also copy input source to central place to be able easier update files in case something changes.
      Files.copy(computedTempFile.toPath(), new File(tempDir, opsFile.getName()).toPath());

      try (InputStream cs = new FileInputStream(computedTempFile);
          InputStream is = new FileInputStream(opsFile)) {
        // Read reference file
        final List<OPSSource> expectedObjects = readReferenceFile(substance, file, is);
        // Read computed sources from file again.
        final List<OPSSource> actualObjects = readComputedBrnFile(substance, file, cs, expectedObjects);
        for (int i = 0; i < expectedObjects.size(); i++) {
          final OPSSource actualOPSSource = actualObjects.get(i);
          actualOPSSource.setId(0); //reset id to avoid checking it.
          final OPSSource opsSource = expectedObjects.get(i);
          final SridPoint point = opsSource.getPoint();
          final SridPoint opsPoint = actualOPSSource.getPoint();

          assertEquals(opsSource, actualOPSSource, "Check if object (" + i + ") equal. GML file: " + file.getName());
          assertEquals(point.getRoundedX() + "," + point.getRoundedY(),
              opsPoint.getRoundedX() + "," + opsPoint.getRoundedY(),
              "Check if x & y equal (" + opsSource + "). GML file: " + file.getName());
          assertEquals(opsSource.getEmission(substance),
              actualOPSSource.getEmission(substance), 0.001, "Check emission equals (" + opsSource + "). GML file: " + file.getName());
          assertEquals(opsSource, actualOPSSource,
              "Check if object ops characteristics (x:" + opsPoint.getX() + ",y:" + opsPoint.getY() + ") equal. GML file: "
                  + file.getName());
        }
      }
    } else if ((input.getEmissionSources() != null) && !input.getEmissionSources().isEmpty()) {
      // if not exists, but we found results it's an error.
      LOG.warn("No OPS reference file found while source file has emissions for substance {} ({}), missing file: {}.",
          substance, input.getEmissionSources().size(), opsFile.getName());
      // Help part to create files if a lot has changed and you don't want to replace individual tests
      // Remove the existing .brn file in the test resources to generate in target/test-classes/nl/overheid/aerius/verification/scenario/missing/
      // Also fails/creates files for substances where there are no emissions, those can be ignored.
      if (CREATE_MISSING_FILE) {
        final File computedTempPath = Files.createDirectories(new File(tempDir, UUID.randomUUID().toString()).toPath()).toFile();
        BrnFileWriter.writeFile(computedTempPath, input.getEmissionSources().get(1).stream().sorted(OPS_SORT_COMPARATOR).toList(),
            substance, null);
        final File computedTempFile = new File(computedTempPath, BrnFileWriter.getFileName());
        final File missingFolder = new File(file.getParent(), "missing/");
        Files.createDirectories(missingFolder.toPath());
        final File missingOpsFile = new File(missingFolder,
            fileWithoutExt.replace(file.getParent(), "") + "_" + substance.getName().toLowerCase(Locale.ROOT) + ".brn");
        Files.copy(computedTempFile.toPath(), missingOpsFile.toPath());
        LOG.info("File written to: {}", missingOpsFile);
        fail("OPS file was missing, check the target folder for created suggestion.");
      }
    } // else no reference file and no results for substance, so nothing to check just ignore substance
  }

  private List<OPSSource> readComputedBrnFile(final Substance substance, final File file, final InputStream cs, final List<OPSSource> expectedObjects)
      throws IOException {
    final BrnFileReader comfr = new BrnFileReader(substance);
    final LineReaderResult<OPSSource> computedReader = comfr.readObjects(cs);
    final List<OPSSource> actualObjects = computedReader.getObjects();
    Collections.sort(actualObjects, OPS_SORT_COMPARATOR);
    assertFalse(expectedObjects.isEmpty(), "Check if sources list is not empty. GML file: " + file.getName());
    assertEquals(expectedObjects.size(),
        actualObjects.size(), "Check if same number of ops sources in reference file. GML file: " + file.getName());
    return actualObjects;
  }

  private OPSInputData readReferenceDataFRomGML(final Substance substance, final File file) throws IOException, AeriusException, SQLException {
    final ImportParcel result = readReferenceGML(file);
    // Convert imported input to OPSSources
    final List<OPSSource> opsSources = new ArrayList<>();
    try (final Connection con = getConnection()) {
      // Convert sources to ops points
      checkInput(result.getSituation());
      convert2OpsSources(result, opsSources, con);
    }
    assertNotNull(opsSources, "Sources may never be null. GML file: " + file.getName());
    LOG.trace("Calculate emission for key: {}", substance.hatch());
    final OPSInputData input = createOPSInputDataObject(substance, result, opsSources);
    return input;
  }

  private void checkInput(final ScenarioSituation situation) {
    for (final EmissionSourceFeature es : situation.getEmissionSourcesList()) {
      final SourceCharacteristics chrs = es.getProperties().getCharacteristics();
      if (chrs instanceof OPSSourceCharacteristics) {
        final DiurnalVariation diurnalVariation = ((OPSSourceCharacteristics) chrs).getDiurnalVariation();
        final int sectorId = es.getProperties().getSectorId();
        LOG.debug("Source DiurnalVariation: '{}', Sector({}) diurnalvariation: '{}'", diurnalVariation, sectorId,
            sectorCategories.getSectorById(sectorId).getDefaultCharacteristics().getDiurnalVariation());
      }
    }
  }

  private ImportParcel readReferenceGML(final File file) throws IOException, AeriusException, FileNotFoundException {
    final ImporterFacade importer = new ImporterFacade(getPMF(), sectorCategories);
    final ImportParcel result;
    try (final InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
      result = importer.convertInputStream2ImportResult(file.getName(), inputStream).get(0);
    }
    if (!result.getExceptions().isEmpty()) {
      throw result.getExceptions().get(0);
    }
    return result;
  }

  private OPSInputData createOPSInputDataObject(final Substance substance, final ImportParcel result, final List<OPSSource> opsSources) {
    final OPSInputData input = new OPSInputData(OPSVersion.LATEST, CommandType.CALCULATE, RECEPTOR_GRID_SETTINGS.getZoomLevel1().getSurfaceLevel1());
    input.setSubstances(substance.hatch());
    input.setYear(result.getSituation().getYear());
    input.setEmissionResultKeys(
        EnumSet.copyOf(Stream.of(EmissionResultKey.values()).filter(erk -> erk.getSubstance() == substance).collect(Collectors.toSet())));
    input.setEmissionSources(1, opsSources);
    return input;
  }

  private List<OPSSource> readReferenceFile(final Substance substance, final File file, final InputStream is) throws IOException {
    final BrnFileReader edfr = new BrnFileReader(substance);
    final LineReaderResult<OPSSource> expectedReader = edfr.readObjects(is);

    assertTrue(expectedReader.getExceptions().isEmpty(), "Check if no errors in ops reference file. GML file: " + file.getName() + ". Exceptions: "
        + Arrays.toString(expectedReader.getExceptions().toArray()));
    final List<OPSSource> expectedObjects = expectedReader.getObjects();
    // Sort sources and reference sources for easy comparing.
    Collections.sort(expectedObjects, OPS_SORT_COMPARATOR);
    return expectedObjects;
  }

  private void convert2OpsSources(final ImportParcel result, final List<OPSSource> opsSources, final Connection con)
      throws SQLException, AeriusException {
    final ScenarioSituation situation = result.getSituation();
    final SourceConversionHelper conversionHelper = new SourceConversionHelper(getPMF(), sectorCategories, situation.getYear());
    final SourceConverter geometryExpander = new OPSSourceConverter(getPMF().getConnection(), situation);

    for (final EngineSource es : EngineSourceExpander.toEngineSources(con, geometryExpander, conversionHelper,
        situation, RESULT_SUBSTANCES, true)) {
      if (es instanceof OPSSource) {
        opsSources.add((OPSSource) es);
      }
    }
  }
}
