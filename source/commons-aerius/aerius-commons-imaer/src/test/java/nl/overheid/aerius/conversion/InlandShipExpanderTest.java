/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.InlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.StandardInlandShipping;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link InlandShipExpander}.
 */
class InlandShipExpanderTest extends BaseDBTest {

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
  }

  @Test
  void testInlandShipsToPoints() throws SQLException, AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setId("1");
    feature.setGeometry(GenericSourceExpanderTest.getExampleLineWKTGeometry());

    final StandardInlandShipping is = new StandardInlandShipping();
    is.setShipCode("BI");
    is.setMovementsAtoBPerTimeUnit(10);
    is.setTimeUnitAtoB(TimeUnit.DAY);
    is.setPercentageLadenAtoB(70);

    final InlandShippingEmissionSource emissionSource = new InlandShippingEmissionSource();
    emissionSource.setEmissions(Map.of(Substance.NOX, 20.0));
    emissionSource.setSubSources(List.of(is));
    feature.setProperties(emissionSource);
    final ScenarioSituation situation = new ScenarioSituation();
    situation.getEmissionSourcesList().add(feature);
    final SectorCategories sectorCategories = getCategories(getPMF());
    final SourceConversionHelper conversionHelper = new SourceConversionHelper(getPMF(), sectorCategories, TestDomain.YEAR);
    final SourceConverter geometryExpander = new OPSSourceConverter(getPMF().getConnection(), situation);
    final List<EngineSource> pointSources = EngineSourceExpander.toEngineSources(getConnection(), geometryExpander, conversionHelper, situation,
        List.of(Substance.NOX), true);
    assertEquals(572, pointSources.size(), "Should return all points on the line");
    assertEquals(10974, (int) pointSources.stream().mapToDouble(e -> ((EngineEmissionSource) e).getEmission(Substance.NOX)).sum(),
        "Should have correct summaries emission");
  }
}
