/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ScenarioFileServerUtil}
 */
@ExtendWith(MockitoExtension.class)
class ScenarioFileServerUtilTest {
  private static final String FILE_CODE = "1234";
  private static final String VALIDATION_PATH_SEGMENT = FILE_CODE + "/validation.json";
  private static final String DATA_PATH_SEGMENT = FILE_CODE + "/data.json";

  @Test
  void testRetrieveScenario() throws AeriusException, IOException, HttpException, URISyntaxException {
    final String scenarioString = "{\"theme\":\"OWN2000\"}";
    final FileServerProxy fileServerProxy = mock(FileServerProxy.class);
    doReturn(scenarioString).when(fileServerProxy).getContent(any());
    final ScenarioFileServerUtil scenarioFileServiceUtil = new ScenarioFileServerUtil(fileServerProxy);

    final Scenario actualScenario = scenarioFileServiceUtil.retrieveScenario(FILE_CODE);
    verify(fileServerProxy).getContent(DATA_PATH_SEGMENT);
    assertEquals(Theme.OWN2000, actualScenario.getTheme(), "The scenario should match the one retrieved via http request");
  }

  @Test
  void testRetrieveScenarioHttpException() throws IOException, HttpException, URISyntaxException {
    final FileServerProxy fileServerProxy = mock(FileServerProxy.class);
    doReturn("").when(fileServerProxy).getContent(any());
    final ScenarioFileServerUtil scenarioFileServiceUtil = new ScenarioFileServerUtil(fileServerProxy);

    assertThrows(AeriusException.class, () -> scenarioFileServiceUtil.retrieveScenario(FILE_CODE), "Http error should throw exception");
    verify(fileServerProxy).getContent(DATA_PATH_SEGMENT);
  }

  @Test
  void testRetrieveScenarioUriSyntaxException() throws IOException, HttpException, URISyntaxException {
    final FileServerProxy fileServerProxy = mock(FileServerProxy.class);
    doReturn("").when(fileServerProxy).getContent(any());
    final ScenarioFileServerUtil scenarioFileServiceUtil = new ScenarioFileServerUtil(fileServerProxy);

    assertThrows(AeriusException.class, () -> scenarioFileServiceUtil.retrieveScenario(FILE_CODE), "URI syntax error should throw exception");
    verify(fileServerProxy).getContent(DATA_PATH_SEGMENT);
  }

  @Test
  void testRetrieveScenarioIOException() throws IOException, HttpException, URISyntaxException {
    final String scenarioString = "{\"theme\":\"INVALID_THEME\"}";
    final FileServerProxy fileServerProxy = mock(FileServerProxy.class);
    doReturn(scenarioString).when(fileServerProxy).getContent(any());
    final ScenarioFileServerUtil scenarioFileServiceUtil = new ScenarioFileServerUtil(fileServerProxy);

    assertThrows(AeriusException.class, () -> scenarioFileServiceUtil.retrieveScenario(FILE_CODE), "Scenario parse error should throw exception");
    verify(fileServerProxy).getContent(DATA_PATH_SEGMENT);
  }

  @Test
  void testRemoveScenario() throws IOException, HttpException, URISyntaxException {
    final FileServerProxy fileServerProxy = mock(FileServerProxy.class);
    doReturn("").when(fileServerProxy).deleteRemoteContent(any());
    final ScenarioFileServerUtil scenarioFileServiceUtil = new ScenarioFileServerUtil(fileServerProxy);

    assertDoesNotThrow(() -> scenarioFileServiceUtil.removeScenario(FILE_CODE), "Scenario cleanup should proceed without exception");
    verify(fileServerProxy).deleteRemoteContent(DATA_PATH_SEGMENT);
    verify(fileServerProxy).deleteRemoteContent(VALIDATION_PATH_SEGMENT);
  }

  @Test
  void testRemoveScenarioWhenHttpException() throws IOException, HttpException, URISyntaxException {
    final FileServerProxy fileServerProxy = mock(FileServerProxy.class);
    lenient().doThrow(IOException.class).when(fileServerProxy).deleteRemoteContent(eq(VALIDATION_PATH_SEGMENT));
    final ScenarioFileServerUtil scenarioFileServiceUtil = new ScenarioFileServerUtil(fileServerProxy);

    assertDoesNotThrow(() -> scenarioFileServiceUtil.removeScenario(FILE_CODE),
        "Scenario cleanup should proceed without exception, even when an http error occurs");
    verify(fileServerProxy).deleteRemoteContent(DATA_PATH_SEGMENT);
    verify(fileServerProxy).deleteRemoteContent(VALIDATION_PATH_SEGMENT);
  }
}
