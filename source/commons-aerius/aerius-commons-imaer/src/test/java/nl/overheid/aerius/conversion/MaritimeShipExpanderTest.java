/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.MooringMaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.StandardMooringMaritimeShipping;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link MaritimeShipExpander}
 */
class MaritimeShipExpanderTest extends BaseDBTest {

  private static SourceConverter geometryExpander;
  private static TestDomain testDomain;

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    testDomain = new TestDomain(getCategories(getPMF()));
    geometryExpander = new OPSSourceConverter(getPMF().getConnection(), null);
  }

  @Test
  void testMaritimeShipsToPoints() throws SQLException, AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setGeometry(GenericSourceExpanderTest.getExampleLineWKTGeometry());
    final MooringMaritimeShippingEmissionSource emissionSource = new MooringMaritimeShippingEmissionSource();
    feature.setProperties(emissionSource);

    testDomain.getMooringMaritimeShipEmissionSource(emissionSource);
    //ensure the 2nd boat has a maneuver part on the inland route.
    ((StandardMooringMaritimeShipping) emissionSource.getSubSources().get(1)).setShipCode("OO10000");
    final SourceConversionHelper conversionHelper = new SourceConversionHelper(getPMF(), testDomain.getCategories(), TestDomain.YEAR);
    final ExpanderVisitor visitor = new ExpanderVisitor(getConnection(), geometryExpander, List.of(Substance.NOX), conversionHelper);
    feature.accept(visitor);
    final int dockPointSources = 286;
    assertPoints(dockPointSources, visitor, 76420, 4774);
  }

  @Test
  void testMaritimeMaritimeShipsToPoints() throws SQLException, AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setGeometry(GenericSourceExpanderTest.getExampleLineWKTGeometry());
    feature.setProperties(testDomain.getShipEmissionRouteSource());

    final SourceConversionHelper conversionHelper = new SourceConversionHelper(getPMF(), testDomain.getCategories(), TestDomain.YEAR);
    final ExpanderVisitor visitor = new ExpanderVisitor(getConnection(), geometryExpander, List.of(Substance.NOX), conversionHelper);
    feature.accept(visitor);

    final int dockPointSources = 286;
    assertPoints(dockPointSources, visitor, 243, 301);
  }

  private static void assertPoints(final int dockPointSources, final ExpanderVisitor visitor, final double puntertjeEmissions,
      final double motorBootEmissions) {
    final List<EngineSource> pointSources = (List<EngineSource>) visitor.getExpandedSources().toGroupedSourcesPacket().get(0).getSources();
    //    assertEquals(dockPointSources * 2, pointSources.size(), "Number of points should be 572 (dock = 286, 2 types of ships)");
    int pointNr = 0;
    final OPSSource engineSource0 = (OPSSource) pointSources.get(0);
    assertNotEquals(0.0, engineSource0.getHeatContent(), 1E-3, "Characteristics shouldn't have 0 heat content");
    assertNotEquals(0.0, engineSource0.getEmissionHeight(), 1E-3, "Characteristics shouldn't have 0 height");
    assertNotEquals(0.0, engineSource0.getSpread(), 1E-3, "Characteristics shouldn't have 0 spread");
    boolean puntertje = false;
    boolean motorboot = false;
    for (final EngineSource s : pointSources) {
      final OPSSource es = (OPSSource) s;
      final double emission = es.getEmission(Substance.NOX);
      final double x = es.getPoint().getX();
      final double y = es.getPoint().getY();

      if ((int) emission == puntertjeEmissions) {
        puntertje = true;
      } else if ((int) emission == motorBootEmissions) {
        motorboot = true;
      }
      assertNotEquals(0, x, 0.0001d, "X coordinate of (point:" + pointNr + ", xy:" + x + " " + y + ", emission of point:" + emission + "):");
      assertNotEquals(0, y, 0.0001d, "Y coordinate of (point:" + pointNr + ", xy:" + x + " " + y + ", emission of point:" + emission + "):");
      assertNotEquals(0, emission, 0.0001d, "emission of (point:" + pointNr + ", xy:" + x + " " + y + ", emission of point:" + emission + "):");
      pointNr++;
    }
    assertTrue(puntertje, "Puntertje found");
    assertTrue(motorboot, "Motorboot found");
  }
}
