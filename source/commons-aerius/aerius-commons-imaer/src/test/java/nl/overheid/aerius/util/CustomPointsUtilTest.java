/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;

/**
 * Test class for {@link CustomPointsUtilTest}
 */
class CustomPointsUtilTest {

  @Test
  void testWithEmptyList() {
    assertDoesNotThrow(() -> CustomPointsUtil.ensureProperIds(new ArrayList<>(), true), "Ensuring id's on an empty list should run succesfully in strict mode.");
    assertDoesNotThrow(() -> CustomPointsUtil.ensureProperIds(new ArrayList<>(), false), "Ensuring id's on an empty list should run succesfully in non-strict mode.");
  }

  @Test
  void testWithDuplicateNonZeroIdsStrict() {
    List<CalculationPointFeature> input = new ArrayList<>();
    CalculationPointFeature cpf1 = new CalculationPointFeature();
    CustomCalculationPoint ccp1 = new CustomCalculationPoint();
    ccp1.setCustomPointId(12);
    cpf1.setProperties(ccp1);
    input.add(cpf1);

    CalculationPointFeature cpf2 = new CalculationPointFeature();
    CustomCalculationPoint ccp2 = new CustomCalculationPoint();
    ccp2.setCustomPointId(12);
    cpf2.setProperties(ccp2);
    input.add(cpf2);

    CustomPointsUtil.ensureProperIds(input, true);

    assertEquals(2, input.stream()
            .map(CalculationPointFeature::getProperties)
            .map(CalculationPoint::getId)
            .distinct()
            .count(),
        "When two CustomCalculationPoints with the same id are given as input, then after the operation they should have distinct id's.");
    assertEquals(1, input.get(0).getProperties().getId(), "In strict mode, when duplicates exist, custom calculation points should get an id equal to their index + 1");
    assertEquals(2, input.get(1).getProperties().getId(), "In strict mode, when duplicates exist, custom calculation points should get an id equal to their index + 1");
  }

  @Test
  void testWithDuplicateNonZeroIdsLenient() {
    List<CalculationPointFeature> input = new ArrayList<>();
    CalculationPointFeature cpf1 = new CalculationPointFeature();
    CustomCalculationPoint ccp1 = new CustomCalculationPoint();
    ccp1.setCustomPointId(12);
    cpf1.setProperties(ccp1);
    input.add(cpf1);

    CalculationPointFeature cpf2 = new CalculationPointFeature();
    CustomCalculationPoint ccp2 = new CustomCalculationPoint();
    ccp2.setCustomPointId(12);
    cpf2.setProperties(ccp2);
    input.add(cpf2);

    CustomPointsUtil.ensureProperIds(input, false);

    assertEquals(12, input.get(0).getProperties().getId(), "In lenient mode, duplicates may exist as long as not all of the ids are 0.");
    assertEquals(12, input.get(1).getProperties().getId(), "In lenient mode, duplicates may exist as long as not all of the ids are 0.");
  }

  @Test
  void testWithOnlyZeroIdsLenient() {
    List<CalculationPointFeature> input = new ArrayList<>();
    CalculationPointFeature cpf1 = new CalculationPointFeature();
    CustomCalculationPoint ccp1 = new CustomCalculationPoint();
    ccp1.setCustomPointId(0);
    cpf1.setProperties(ccp1);
    input.add(cpf1);

    CalculationPointFeature cpf2 = new CalculationPointFeature();
    CustomCalculationPoint ccp2 = new CustomCalculationPoint();
    ccp2.setCustomPointId(0);
    cpf2.setProperties(ccp2);
    input.add(cpf2);

    CustomPointsUtil.ensureProperIds(input, false);

    assertEquals(2, input.stream()
            .map(CalculationPointFeature::getProperties)
            .map(CalculationPoint::getId)
            .distinct()
            .count(),
        "When two CustomCalculationPoints with the same id are given as input, then after the operation they should have distinct id's.");
    assertEquals(1, input.get(0).getProperties().getId(), "In lenient mode, when all ids are 0, custom calculation points should get an id equal to their index + 1.");
    assertEquals(2, input.get(1).getProperties().getId(), "In lenient mode, when all ids are 0, Custom calculation points should get an id equal to their index + 1.");
  }

  @Test
  void testReceptorPoints() {
    List<CalculationPointFeature> input = new ArrayList<>();
    CalculationPointFeature cpf1 = new CalculationPointFeature();
    ReceptorPoint rp1 = new ReceptorPoint();
    final int receptorId = 1234;
    rp1.setReceptorId(receptorId);
    cpf1.setProperties(rp1);
    input.add(cpf1);

    CustomPointsUtil.ensureProperIds(input, true);

    assertEquals(receptorId, input.get(0).getProperties().getId(), "Id's of receptors should stay unaffected");
  }

  @Test
  void testSubPoint() {
    List<CalculationPointFeature> input = new ArrayList<>();
    CalculationPointFeature cpf1 = new CalculationPointFeature();
    SubPoint sp1 = new SubPoint();
    final int receptorId = 1234;
    final int subPointId = 2345;
    sp1.setReceptorId(receptorId);
    sp1.setSubPointId(subPointId);
    cpf1.setProperties(sp1);
    input.add(cpf1);

    CustomPointsUtil.ensureProperIds(input, true);

    assertEquals(subPointId, input.get(0).getProperties().getId(), "Id's of subpoints should stay unaffected");
  }
}