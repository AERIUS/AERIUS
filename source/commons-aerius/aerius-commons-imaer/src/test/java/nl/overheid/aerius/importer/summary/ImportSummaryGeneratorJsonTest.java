/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.summary;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.importer.ImporterFacade;
import nl.overheid.aerius.shared.domain.importer.summary.ImportSummary;
import nl.overheid.aerius.shared.domain.importer.summary.SituationResults;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.ScenarioObjectMapperUtil;

/**
 * Tests whether the Summary JSON is not accidentally changed,
 * given a static import file.
 */
class ImportSummaryGeneratorJsonTest extends BaseDBTest {

  private ImporterFacade importer;

  @BeforeEach
  void before() throws SQLException, AeriusException {
    importer = new ImporterFacade(getPMF(), getCategories(getPMF()));
    importer.setImportOption(ImportOption.INCLUDE_RESULTS, true);
  }

  private final ImportSummaryGenerator importSummaryGenerator = new ImportSummaryGenerator();
  private final ObjectMapper objectMapper = ScenarioObjectMapperUtil.getScenarioObjectMapper();

  @Test
  void testJsonRepresentation() throws IOException, AeriusException {
    final String gmlFileName = "with_calculation_results.gml";
    final String jsonFileName = "summary.json";
    try (final InputStream pdfStream = getFileInputStream(gmlFileName); final InputStream jsonStream = getFileInputStream(jsonFileName)) {
      final List<ImportParcel> results = importer.convertInputStream2ImportResult(gmlFileName, pdfStream);
      final ImportSummary importSummary = importSummaryGenerator.generateImportSummary(Locale.ENGLISH, results);

      // ensure the order is static
      importSummary.getSituations().get(0).getResults().sort(
          Comparator.comparing(SituationResults::getReceptorId)
              .thenComparing(SituationResults::getSubPointId, Comparator.nullsFirst(Comparator.naturalOrder()))
              .thenComparing(SituationResults::getResultType)
      );

      final String generatedJson = objectMapper.writeValueAsString(importSummary);
      final String referenceJson = IOUtils.toString(jsonStream);

      assertEquals(referenceJson, generatedJson, "Should match reference summary JSON");
    }
  }

  @Override
  protected InputStream getFileInputStream(final String fileName) throws FileNotFoundException {
    return super.getFileInputStream("/importer/" + fileName);
  }
}
