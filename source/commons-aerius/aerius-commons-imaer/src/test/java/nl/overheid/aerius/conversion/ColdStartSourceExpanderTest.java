/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.characteristics.CharacteristicsSupplier;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.v2.characteristics.HeatContentType;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.source.ColdStartEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.Vehicles;
import nl.overheid.aerius.shared.emissions.ColdStartEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.SubSourceEmissionsCalculator;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link ColdStartSourceExpander}.
 */
@ExtendWith(MockitoExtension.class)
class ColdStartSourceExpanderTest {

  private static final int SECTOR_ID = 3160;
  private static final double ASSERT_DELTA = 0.0001;
  private static final Point POINT = new Point(4000, 50000);
  private static final List<Substance> KEY_NOX = List.of(Substance.NOX);
  private static final boolean OPEN_PARKING = true;
  private static final boolean PARKING_GARAGE = false;
  private static final int NR_LIGHT_TRAFFIC = 12345;
  private static final int NR_HEAVY_TRAFFIC = 1234;
  private static final String EURO_1 = "EURO 1";
  private static final double EMISSION_NOX = 10101.0;

  private @Mock Connection con;
  private @Mock SourceConverter sourceConverter;
  private @Mock SourceConversionHelper sourceConversionHelper;
  private @Mock CharacteristicsSupplier characteristicsSupplier;
  private @Mock EmissionFactorSupplier emissionFactorSupplier;
  private @Mock ColdStartEmissionFactorSupplier coldStartEmissionFactorSupplier;
  private @Captor ArgumentCaptor<ColdStartEmissionSource> emissionSourceCaptor;

  private SubSourceEmissionsCalculator subSourceEmissionsCalculator;
  private ColdStartSourceExpander expander;

  @BeforeEach
  void beforeEach() throws SQLException {
    lenient().doReturn(Map.of(Substance.NOX, 0.01)).when(coldStartEmissionFactorSupplier).getColdStartSpecificVehicleEmissionFactors(any());
    lenient().doReturn(Map.of(Substance.NOX, 0.02)).when(coldStartEmissionFactorSupplier).getColdStartStandardVehicleEmissionFactors(any());
    lenient().doReturn(coldStartEmissionFactorSupplier).when(emissionFactorSupplier).coldStart();
    subSourceEmissionsCalculator = new SubSourceEmissionsCalculator(emissionFactorSupplier);
    lenient().doReturn(subSourceEmissionsCalculator).when(sourceConversionHelper).getSubSourceEmissionsCalculator();
    lenient().doReturn(TestDomain.createOPSCharacteristics(HeatContentType.FORCED, 101.0, 201.0, 301, 401.0, DiurnalVariation.TRAFFIC, 0))
        .when(characteristicsSupplier).getSectorCharacteristics(SECTOR_ID);
    lenient().doReturn(characteristicsSupplier).when(sourceConversionHelper).getCharacteristicsSupplier();
    lenient().doReturn(TestDomain.createSimpleOPSCharacteristics(13, 23, 33)).when(characteristicsSupplier)
        .getColdStartStandard(VehicleType.LIGHT_TRAFFIC.getStandardVehicleCode());
    lenient().doReturn(TestDomain.createSimpleOPSCharacteristics(14, 24, 34)).when(characteristicsSupplier)
        .getColdStartStandard(VehicleType.HEAVY_FREIGHT.getStandardVehicleCode());
    lenient().doReturn(TestDomain.createSimpleOPSCharacteristics(15, 25, 35)).when(characteristicsSupplier)
        .getColdStartSpecific(EURO_1);

    expander = new ColdStartSourceExpander(sourceConverter, sourceConversionHelper);
  }

  // Open parking tests

  @Test
  void testOpenParkingCustom() throws AeriusException {
    final List<ColdStartEmissionSource> sources = verifyConvert(OPEN_PARKING, 2,
        TestDomain.createCustomVehicle(VehicleType.LIGHT_TRAFFIC, NR_LIGHT_TRAFFIC),
        TestDomain.createCustomVehicle(VehicleType.HEAVY_FREIGHT, NR_HEAVY_TRAFFIC));

    assertEquals(emissionInKgPerYear(NR_LIGHT_TRAFFIC, 0.04), findByHeatContent(sources, 13).getEmissions().get(Substance.NOX), ASSERT_DELTA,
        "Not the expected emission for light traffic vehicles");
    assertEquals(emissionInKgPerYear(NR_HEAVY_TRAFFIC, 0.04), findByHeatContent(sources, 14).getEmissions().get(Substance.NOX), ASSERT_DELTA,
        "Not the expected emission for heavy traffic vehicles");
  }

  @Test
  void testOpenParkingSpecific() throws AeriusException {
    final List<ColdStartEmissionSource> sources = verifyConvert(OPEN_PARKING, 1, TestDomain.createSpecificVehicle(EURO_1, 1001));

    assertEquals(emissionInKgPerYear(1001, 0.01), findByHeatContent(sources, 15).getEmissions().get(Substance.NOX), ASSERT_DELTA,
        "Not the expected emission for specifc vehicles");
  }

  @Test
  void testOpenParkingStandard() throws AeriusException {
    final List<ColdStartEmissionSource> sources = verifyConvert(OPEN_PARKING, 1, TestDomain.createColdStartStandardVehicle(2002));

    assertEquals(emissionInKgPerYear(2002, 0.02), findByHeatContent(sources, 13).getEmissions().get(Substance.NOX), ASSERT_DELTA,
        "Not the expected emission for specifc vehicles");
  }

  private static double emissionInKgPerYear(final int numberOfVehicles, final double emissionGramPerColdstart) {
    return numberOfVehicles * 365 * (emissionGramPerColdstart / 1000);
  }

  private static ColdStartEmissionSource findByHeatContent(final List<ColdStartEmissionSource> sources, final double heatContent) {
    return sources.stream().filter(s -> heatContent == ((OPSSourceCharacteristics) s.getCharacteristics()).getHeatContent().doubleValue()).findAny()
        .orElse(null);
  }

  // Parking garage tests

  @Test
  void testParkingGarageCustom() throws AeriusException {
    final List<ColdStartEmissionSource> sources = verifyConvert(PARKING_GARAGE, 1,
        TestDomain.createCustomVehicle(VehicleType.LIGHT_TRAFFIC, NR_LIGHT_TRAFFIC),
        TestDomain.createCustomVehicle(VehicleType.HEAVY_FREIGHT, NR_HEAVY_TRAFFIC));

    assertPartkingGarageOnlyOneSource(sources);
  }

  @Test
  void testParkingGarageSpecific() throws AeriusException {
    final List<ColdStartEmissionSource> sources = verifyConvert(PARKING_GARAGE, 1, TestDomain.createSpecificVehicle(EURO_1, 1001),
        TestDomain.createSpecificVehicle("EURO 2", 3030));

    assertPartkingGarageOnlyOneSource(sources);
  }

  @Test
  void testParkingGarageStandard() throws AeriusException {
    final List<ColdStartEmissionSource> sources = verifyConvert(PARKING_GARAGE, 1, TestDomain.createColdStartStandardVehicle(2002));

    assertPartkingGarageOnlyOneSource(sources);
  }

  private List<ColdStartEmissionSource> verifyConvert(final boolean vehicleBasedCharacteristics, final int expetedNumberOfSources,
      final Vehicles... vehicles) throws AeriusException {
    final ColdStartEmissionSource emissionSource = new ColdStartEmissionSource();
    emissionSource.setVehicleBasedCharacteristics(vehicleBasedCharacteristics);
    emissionSource.setSectorId(SECTOR_ID);
    emissionSource.setCharacteristics(TestDomain.createSimpleOPSCharacteristics(18.0, 28.0, 38.0));
    emissionSource.getEmissions().put(Substance.NOX, EMISSION_NOX);
    emissionSource.getSubSources().addAll(List.of(vehicles));
    final GroupedSourcesPacketMap map = new GroupedSourcesPacketMap();

    expander.convert(con, map, emissionSource, POINT, KEY_NOX);
    verify(sourceConverter, times(expetedNumberOfSources)).convert(eq(con), eq(map), emissionSourceCaptor.capture(), eq(POINT), eq(KEY_NOX));
    return emissionSourceCaptor.getAllValues();
  }

  private static void assertPartkingGarageOnlyOneSource(final List<ColdStartEmissionSource> sources) {
    assertEquals(1, sources.size(), "Parking garage should result in 1 single source");
    final ColdStartEmissionSource es = sources.get(0);

    assertOPSCharacteristics((OPSSourceCharacteristics) es.getCharacteristics(), 18.0, 28.0, 38.0);
    assertEquals(EMISSION_NOX, es.getEmissions().get(Substance.NOX), ASSERT_DELTA, "Should not have changed emissions");
  }

  private static void assertOPSCharacteristics(final OPSSourceCharacteristics chars, final double expectedHeatContent,
      final double expectedEmissionHeight, final double expectedSpread) {
    assertEquals(expectedHeatContent, chars.getHeatContent(), ASSERT_DELTA, "Heat content not as expected");
    assertEquals(expectedEmissionHeight, chars.getEmissionHeight(), ASSERT_DELTA, "Emission height not as expected");
    assertEquals(expectedSpread, chars.getSpread(), ASSERT_DELTA, "Spread not as expected");
  }
}
