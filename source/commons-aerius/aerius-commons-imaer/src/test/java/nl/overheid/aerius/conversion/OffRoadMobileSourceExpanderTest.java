/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.characteristics.CharacteristicsSupplier;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.offroad.CustomOffRoadMobileSource;
import nl.overheid.aerius.shared.domain.v2.source.offroad.OffRoadMobileSource;
import nl.overheid.aerius.shared.domain.v2.source.offroad.StandardOffRoadMobileSource;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.OffRoadMobileEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.SubSourceEmissionsCalculator;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link OffRoadMobileSourceExpander}.
 */
@ExtendWith(MockitoExtension.class)
class OffRoadMobileSourceExpanderTest {

  private static final double ASSERT_DELTA = 0.0001;
  private static final int SECTOR_ID = 3220;
  private static final Point POINT = new Point(4000, 50000);
  private static final List<Substance> KEY_NOX = List.of(Substance.NOX);

  private @Mock Connection con;
  private @Mock SourceConverter sourceConverter;
  private @Mock SourceConversionHelper sourceConversionHelper;
  private @Mock CharacteristicsSupplier characteristicsSupplier;
  private @Mock EmissionFactorSupplier emissionFactorSupplier;
  private @Mock OffRoadMobileEmissionFactorSupplier offRoadMobileEmissionFactorSupplier;
  private @Captor ArgumentCaptor<OffRoadMobileEmissionSource> emissionSourceCaptor;

  private SubSourceEmissionsCalculator subSourceEmissionsCalculator;
  private OffRoadMobileSourceExpander expander;
  private final TestDomain testDomain = new TestDomain();

  @BeforeEach
  void beforeEach() {
    doReturn(Map.of(Substance.NOX, 10.0)).when(offRoadMobileEmissionFactorSupplier).getOffRoadMobileEmissionFactorsPerLiterFuel(any());
    doReturn(Map.of(Substance.NOX, 20.0)).when(offRoadMobileEmissionFactorSupplier).getOffRoadMobileEmissionFactorsPerOperatingHour(any());
    doReturn(offRoadMobileEmissionFactorSupplier).when(emissionFactorSupplier).offRoadMobile();
    subSourceEmissionsCalculator = new SubSourceEmissionsCalculator(emissionFactorSupplier);
    doReturn(subSourceEmissionsCalculator).when(sourceConversionHelper).getSubSourceEmissionsCalculator();
    doReturn(characteristicsSupplier).when(sourceConversionHelper).getCharacteristicsSupplier();
    expander = new OffRoadMobileSourceExpander(sourceConverter, sourceConversionHelper);
  }

  @Test
  void testMobileSourceExpander() throws SQLException, AeriusException {
    final Sector sector = new Sector(SECTOR_ID);
    final OPSSourceCharacteristics sectorChars = TestDomain.createSimpleOPSCharacteristics(20.0, 30.0, 40.0);
    doReturn(sectorChars).when(characteristicsSupplier).getSectorCharacteristics(3220);
    sector.setDefaultCharacteristics(sectorChars);

    final OffRoadMobileEmissionSource emissionSource = new OffRoadMobileEmissionSource();
    emissionSource.setSectorId(sector.getSectorId());
    emissionSource.setCharacteristics(sectorChars);

    testDomain.getOffRoadMobileEmissionSource(emissionSource);
    final GroupedSourcesPacketMap map = new GroupedSourcesPacketMap();

    expander.convert(con, map, emissionSource, POINT, KEY_NOX);
    verify(sourceConverter, times(2)).convert(eq(con), eq(map), emissionSourceCaptor.capture(), eq(POINT), eq(KEY_NOX));

    final List<OffRoadMobileEmissionSource> pointSources = emissionSourceCaptor.getAllValues();
    assertEquals(2, pointSources.size(), "Number of point sources");
    int countCustom = 0;
    int countStandard = 0;
    for (final OffRoadMobileEmissionSource es : pointSources) {
      final OPSSourceCharacteristics chars = (OPSSourceCharacteristics) es.getCharacteristics();

      final OffRoadMobileSource offRoadMobileSource = es.getSubSources().get(0);
      if (es.getSubSources().get(0) instanceof CustomOffRoadMobileSource) {
        countCustom++;
        assertEquals(101010.0, es.getEmissions().get(Substance.NOX), 0.00001, "Emission not as expected");
        assertEquals(10, chars.getEmissionHeight(), 0.001, "Emission height custom source");
        assertEquals(5, chars.getSpread(), 0.001, "Spread custom source");
        assertEquals(20, chars.getHeatContent(), 0.001, "Heat content custom source");
        assertEquals(sectorChars.getParticleSizeDistribution(), chars.getParticleSizeDistribution(), 0.001,
            "Particle size distribution");
      } else if (offRoadMobileSource instanceof StandardOffRoadMobileSource) {
        countStandard++;
        assertEquals(302000.0, es.getEmissions().get(Substance.NOX), ASSERT_DELTA, "Emission not as expected");
        assertEquals(30.0, chars.getEmissionHeight(), 0.001, "Emission height standard source not as expected");
        assertEquals(40.0, chars.getSpread(), 0.001, "Spread standard source not as expected");
        assertEquals(20.0, chars.getHeatContent(), 0.001, "Heat content standard source not as expected");
        assertEquals(0.0, chars.getParticleSizeDistribution(), 0.001,
            "Particle size distribution");
      } else {
        fail("Unexpected sub source found:" + offRoadMobileSource);
      }
    }
    assertEquals(1, countCustom, "Should have 1 custom vehicle");
    assertEquals(1, countStandard, "Should have 1 standard vehicle");
  }
}
