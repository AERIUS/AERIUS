/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link LineSimplifier}.
 */
class LineSimplifierTest {

  @Test
  void testSimplify() throws AeriusException {
    final Scenario scenario = createScenario(List.of(createFeature(new double[][] {{100, 100}, {100, 100.5}, {105, 105}})));

    LineSimplifier.cleanupLineSources(scenario);
    assertEquals(2, getSourceCoordinates(scenario, 0).length, "Expected geometry to be simplified");
  }

  @Test
  void testRemoveToSmall() throws AeriusException {
    final double[][] coordinatesOk = new double[][] {{100, 100}, {105, 105}};
    final double[][] coordinatesTooSmall = new double[][] {{100, 100}, {100, 100.5}};
    final Scenario scenario = createScenario(List.of(
        createFeature(coordinatesOk),
        createFeature(coordinatesTooSmall),
        createFeature(coordinatesTooSmall),
        createFeature(coordinatesOk),
        createFeature(coordinatesTooSmall),
        createFeature(coordinatesOk)
        ));

    LineSimplifier.cleanupLineSources(scenario);
    assertEquals(3, getSources(scenario).size(), "Expected 3 sources to be removed.");
    for (int i = 0; i < 3; i++) {
      assertArrayEquals(coordinatesOk, getSourceCoordinates(scenario, i), "Expected geometry to be ok");
    }
  }

  private static double[][] getSourceCoordinates(final Scenario scenario, final int i) {
    return ((LineString) getSources(scenario).get(i).getGeometry()).getCoordinates();
  }

  private static List<EmissionSourceFeature> getSources(final Scenario scenario) {
    return scenario.getSituations().get(0).getEmissionSourcesList();
  }

  private static Scenario createScenario(final List<EmissionSourceFeature> features) {
    final Scenario scenario = new Scenario();
    final ScenarioSituation situation = new ScenarioSituation();

    scenario.getSituations().add(situation);
    situation.getEmissionSourcesList().addAll(features);
    return scenario;
  }

  private static EmissionSourceFeature createFeature(final double[][] coordinates) {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final LineString lineString = new LineString();

    lineString.setCoordinates(coordinates);
    feature.setGeometry(lineString);
    return feature;
  }
}
