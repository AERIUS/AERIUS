/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.Conversion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.GMLLegacyCodeType;
import nl.overheid.aerius.gml.base.conversion.FarmLodgingConversion;
import nl.overheid.aerius.gml.base.conversion.MobileSourceOffRoadConversion;
import nl.overheid.aerius.gml.base.conversion.PlanConversion;

/**
 * Test class for {@link GMLConversionRepository}.
 */
class GMLConversionRepositoryTest extends BaseDBTest {

  @Test
  void testGetLegacyCodes() throws SQLException {
    try (final Connection connection = getConnection()) {
      //only testing if query works, nothing more.
      final Map<GMLLegacyCodeType, Map<String, Conversion>> conversionMap =
          GMLConversionRepository.getLegacyCodes(connection, AeriusGMLVersion.V0_5);
      assertNotNull(conversionMap, "conversionMap");
    }
  }

  @Test
  void testGetLegacyMobileSourceOffRoadConversions() throws SQLException {
    try (final Connection connection = getConnection()) {
      //only testing if query works, nothing more.
      final Map<String, MobileSourceOffRoadConversion> conversions =
          GMLConversionRepository.getLegacyMobileSourceOffRoadConversions(connection);
      assertNotNull(conversions, "legacy mobile source off road conversions");
    }
  }

  @Test
  void testGetLegacyPlanConversions() throws SQLException {
    try (final Connection connection = getConnection()) {
      //only testing if query works, nothing more.
      final Map<String, PlanConversion> conversions =
          GMLConversionRepository.getLegacyPlanConversions(connection);
      assertNotNull(conversions, "legacy plan conversions");
    }
  }

  @Test
  void testGetLegacyFarmLodgingConversions() throws SQLException {
    try (final Connection connection = getConnection()) {
      //only testing if query works, nothing more.
      final Map<String, FarmLodgingConversion> conversions =
          GMLConversionRepository.getLegacyFarmLodgingConversions(connection);

      assertNotNull(conversions, "legacy farm lodging conversions");
    }
  }

  @Test
  void testGetLegacyFarmLodgingConversionsUnknownAnimalCodes() throws SQLException {
    assertFarmLodingConversionsContent("animal_type_code", "farm_animal_categories");
  }

  @Test
  void testGetLegacyFarmLodgingConversionsUnknownAnimalHousingCodes() throws SQLException {
    assertFarmLodingConversionsContent("animal_housing_code", "farm_animal_housing_categories");
  }

  private void assertFarmLodingConversionsContent(final String column, final String referenceTable) throws SQLException {
    try (final Connection connection = getConnection()) {
      final String nonExistingAnimalTypes =
          "select * from system.gml_farm_lodging_conversions where " + column + " NOT IN (SELECT code from " + referenceTable + ")";
      try (final PreparedStatement statement = connection.prepareStatement(nonExistingAnimalTypes)) {
        final ResultSet rs = statement.executeQuery();

        if (rs.next()) {
          fail("Unknown " + column + " in system.gml_farm_lodging_conversions table: " + rs.getString(column));
        }
      }
    }
  }
}
