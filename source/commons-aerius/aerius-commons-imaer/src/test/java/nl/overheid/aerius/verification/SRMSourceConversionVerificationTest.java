/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.verification;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.importer.ImporterFacade;
import nl.overheid.aerius.io.AbstractLineColumnReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.srm2.conversion.EmissionSourceSRMConverter;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.util.FileUtil;

/**
 * Unit test to verify the conversion of AERIUS road sources to SRM2RoadSegments.
 */
class SRMSourceConversionVerificationTest extends BaseDBTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(SRMSourceConversionVerificationTest.class);

  private static final String TEST_RESOURCES_DIRECTORY = "srm2";

  private static final Predicate<String> FILENAME_FILTER = name -> name != null && (ImportType.determineByFilename(name) == ImportType.GML
      || ImportType.determineByFilename(name) == ImportType.SRM2);
  private static final List<Substance> SUBSTANCES = List.of(Substance.NOX, Substance.NO2, Substance.NH3);

  static List<Object[]> data() throws IOException {
    final List<Object[]> parameters = new ArrayList<>();

    for (final File file : getFiles()) {
      parameters.add(new Object[] {
          file.getName(),
          FileUtil.getFileWithoutExtension(file) + ".txt",
      });
    }
    return parameters;
  }

  private static List<File> getFiles() throws IOException {
    return FileUtil.getFilteredFiles(
        new File(SRMSourceConversionVerificationTest.class.getResource(TEST_RESOURCES_DIRECTORY).getPath()), FILENAME_FILTER);
  }

  @ParameterizedTest
  @MethodSource("data")
  void testConversionValidation(final String testInput, final String testReference)
      throws SQLException, IOException, AeriusException {
    LOGGER.info("Validate: {}", testInput);
    final AbstractLineColumnReaderExtension refResultReader = new AbstractLineColumnReaderExtension(testReference);
    final List<SRM2RoadSegment> referenceResults = refResultReader.readObjects().getObjects();

    referenceResults.forEach(s -> logSource("Reference", s));
    final ImporterFacade importer = new ImporterFacade(getPMF(), new TestDomain().getCategories());
    final ImportParcel result;

    try (final InputStream fis = getFileInputStreamTestResources(testInput)) {
      result = importer.convertInputStream2ImportResult(testInput, fis).get(0);
    }

    assertEquals(0, result.getExceptions().size(), "Exceptions during import expected no exceptions: " + result.getExceptions().toString());
    assertEquals(1, result.getWarnings().size(), "Exceptions during import expected warnings: ");
    if (testInput.endsWith(ImportType.SRM2.getExtension())) {
      assertEquals(AeriusExceptionReason.NSL_LEGACY_FILESUPPORT_WILL_BE_REMOVED, result.getWarnings().get(0).getReason(),
          "Exceptions should be warning " + result.getWarnings().toString());
    } else {
      assertEquals(ImaerExceptionReason.GML_VERSION_NOT_LATEST, result.getWarnings().get(0).getReason(),
          "Exceptions should be warning " + result.getWarnings().toString());
    }

    final EmissionSourceSRMConverter converter = new EmissionSourceSRMConverter();
    final List<EngineSource> convertedResults = converter.convert(result.getSituation().getEmissionSourcesList(), SUBSTANCES);

    convertedResults.forEach(source -> {
      assertEquals(SRM2RoadSegment.class, source.getClass(), "Check if source is of expected class type");
      final SRM2RoadSegment e = (SRM2RoadSegment) source;
      // Clear segment id and srm2 because it's not in the reference data
      e.setSegmentId("");
      logSource("Converted", e);
    });
    assertEquals(referenceResults.size(), convertedResults.size(), "Expect same amount of results");
    for (int i = 0; i < convertedResults.size(); i++) {
      assertEquals(referenceResults.get(i), convertedResults.get(i), "Result for row " + (i + 1) + ", message");
    }
  }

  private void logSource(final String type, final SRM2RoadSegment e) {
    LOGGER.debug("{}: {};{};{};{};{};{};{};{};{}", type,
        e.getStartX(), e.getStartY(), e.getEndX(), e.getEndY(), e.getElevationHeight(), e.getSigma0(),
        e.getEmission(Substance.NOX), e.getEmission(Substance.NO2), e.getEmission(Substance.NH3));
  }

  private InputStream getFileInputStreamTestResources(final String filename) throws IOException {
    return getFileInputStream(TEST_RESOURCES_DIRECTORY + "/" + filename);
  }

  /**
   * Reader for reference results.
   */
  private final class AbstractLineColumnReaderExtension extends AbstractLineColumnReader<SRM2RoadSegment> {
    private final String testReference;

    AbstractLineColumnReaderExtension(final String testReference) {
      super(";");
      this.testReference = testReference;
    }

    LineReaderResult<SRM2RoadSegment> readObjects() throws IOException {
      return readObjects(getFileInputStreamTestResources(testReference));
    }

    @Override
    public LineReaderResult<SRM2RoadSegment> readObjects(final InputStream inputStream) throws IOException {
      try (final InputStreamReader reader = new InputStreamReader(inputStream)) {
        return super.readObjects(reader, 1);
      }
    }

    @Override
    protected SRM2RoadSegment parseLine(final String line, final List<AeriusException> warnings) {
      final SRM2RoadSegment segment = new SRM2RoadSegment();
      segment.setSegmentId("");
      segment.setStartX(getDouble("startX", 0));
      segment.setStartY(getDouble("endY", 1));
      segment.setEndX(getDouble("endX", 2));
      segment.setEndY(getDouble("endY", 3));
      segment.setElevationHeight(getInt("elevationHeight", 4));
      segment.setSigma0(getDouble("sigma0", 5));
      segment.setEmission(Substance.NOX, getDouble("NOx", 6));
      segment.setEmission(Substance.NO2, getDouble("NO2", 7));
      segment.setEmission(Substance.NH3, getDouble("NH3", 8));
      return segment;
    }
  }
}
