# Email users via GOV UK Notify

GOV UK Notify is a service by the UK government to send, among other things, emails to people from the Government via a centralized service.
The services works with an API key for access to send the emails,
and emails can be sent using a preconfigured template.
More information about the service can be found on the website: [https://www.notifications.service.gov.uk/](https://www.notifications.service.gov.uk/).

The Notify worker is an alternative for the Email worker.
They should not be used both together.

The worker uses the GOV.Uk notify Java API to communicate with the service.

## Worker configuration

To enable the notify worker set the number of processes.
In general 1 process should be sufficient.
```
notify.processes = 1
```

The worker requires a number of notify specific properties to work correctly.
It requires the api key to access the service:

```
notify.apikey = <gov uk notify api key>
```

To sent a specific email it uses dedicated email templates.
These templates are linked with a template id.
The configuration requires configuration for all templates that are used:

```
notify.templateid.connect_api_key =
notify.templateid.engine_input =
notify.templateid.error =
notify.templateid.gml_download =
notify.templateid.pdf_download =
```
