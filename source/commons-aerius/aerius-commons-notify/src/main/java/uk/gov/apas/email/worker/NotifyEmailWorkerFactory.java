/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package uk.gov.apas.email.worker;

import java.io.Serializable;
import java.util.Properties;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.DatabaseWorkerFactory;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

import uk.gov.service.notify.NotificationClient;

/**
 * Factory to create Notify email worker.
 */
public class NotifyEmailWorkerFactory extends DatabaseWorkerFactory<NotifyConfiguration> {

  public NotifyEmailWorkerFactory() {
    super(WorkerType.NOTIFY);
  }

  @Override
  public ConfigurationBuilder<NotifyConfiguration> configurationBuilder(final Properties properties) {
    return new NofifyConfigurationBuilder(properties);
  }

  @Override
  protected Worker<Serializable, Serializable> createWorkerHandlerWithPMF(final NotifyConfiguration configuration,
      final WorkerConnectionHelper workerConnectionHelper) throws Exception {
    return new NofifyWorker(workerConnectionHelper.getPMF(), createNotificationClient(configuration), configuration);
  }

  protected NotificationClient createNotificationClient(final NotifyConfiguration configuration) {
    return new NotificationClient(configuration.getApiKey());
  }

  private static class NofifyWorker implements Worker<Serializable, Serializable> {
    private final PMF pmf;
    private final NotificationClient notificationClient;
    private final NotifyConfiguration configuration;

    public NofifyWorker(final PMF pmf, final NotificationClient notificationClient, final NotifyConfiguration configuration) {
      this.pmf = pmf;
      this.notificationClient = notificationClient;
      this.configuration = configuration;
    }

    @Override
    public Serializable run(final Serializable input, final JobIdentifier jobIdentifier,
        final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
      if (input instanceof final MailMessageData mmd) {
        return new NotifyEmailWorkerHandler(pmf, notificationClient, configuration).run(mmd, jobIdentifier, workerIntermediateResultSender);
      } else {
        throw new UnsupportedOperationException("Worker does not know how to handle the input:" + input.getClass().getName());
      }
    }
  }
}
