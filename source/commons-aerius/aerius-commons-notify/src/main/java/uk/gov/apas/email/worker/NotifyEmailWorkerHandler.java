/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package uk.gov.apas.email.worker;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.worker.AbstractMessageWorker;

import uk.gov.service.notify.NotificationClient;

/**
 * Notify Worker to pick up tasks from the message queue and submit the message to be sent as email via the GOV UK Notify service.
 */
class NotifyEmailWorkerHandler extends AbstractMessageWorker {

  private static final Logger LOG = LoggerFactory.getLogger(NotifyEmailWorkerHandler.class);

  static final String ABSENT_TOKEN_OR_EMPTY_REPLACEMENT = "not applicable";

  private final NotificationClient notifyClient;
  private final NotifyConfiguration configuration;

  public NotifyEmailWorkerHandler(final PMF pmf, final NotificationClient notifyClient, final NotifyConfiguration configuration) {
    super(pmf, "GOV.UK Notify");
    this.notifyClient = notifyClient;
    this.configuration = configuration;
  }

  @Override
  protected void sendMessage(final MailMessageData input, final String jobKey) throws Exception {
    final Map<String, String> map = constructTokenMap(input);

    LOG.info("Sending {} email via UK Notify for job: {}", input.getEmailType(), jobKey);
    notifyClient.sendEmail(configuration.getTemplateId(input.getEmailType()), input.getMailTo().getToAddresses().get(0), map, jobKey);
  }

  private static Map<String, String> constructTokenMap(final MailMessageData input) {
    setExportFileType(input);
    Arrays.stream(ReplacementToken.values())
        .forEach(t -> input.getReplacements().compute(t, (k, v) -> v == null || v.isEmpty() ? ABSENT_TOKEN_OR_EMPTY_REPLACEMENT : v));
    return input.getReplacements().entrySet().stream().collect(Collectors.toMap(e -> e.getKey().name(), Map.Entry::getValue));
  }

  private static void setExportFileType(final MailMessageData input) {
    final Enum<?> exportType = input.getEnumReplacementTokens().get(ReplacementToken.EXPORT_TYPE_FILE);

    if (!input.getReplacements().containsKey(ReplacementToken.EXPORT_TYPE_FILE) && exportType instanceof final EmailMessagesEnum eme) {
      input.setReplacement(ReplacementToken.EXPORT_TYPE_FILE,
          switch (eme) {
          case EXPORT_TYPE_FILE_CSV -> "";
          case EXPORT_TYPE_FILE_ENGINE_INPUT -> "Model input files";
          case EXPORT_TYPE_FILE_GML -> "GML file";
          case EXPORT_TYPE_FILE_PAA -> "PDF file";
          default -> "";
          });
    }
  }
}
