/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package uk.gov.apas.email.worker;

import java.util.EnumMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.email.EmailType;
import nl.overheid.aerius.worker.DatabaseConfiguration;

/**
 * Configuration for Notify worker.
 */
class NotifyConfiguration extends DatabaseConfiguration {

  private String apiKey;
  private final Map<EmailType, String> templateIds = new EnumMap<>(EmailType.class);

  public NotifyConfiguration(final int processes) {
    super(processes);
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(final String apiKey) {
    this.apiKey = apiKey;
  }

  public void putTemplateId(final EmailType emailType, final String templateId) {
    templateIds.put(emailType, templateId);
  }

  public String getTemplateId(final EmailType emailType) {
    return templateIds.get(emailType);
  }
}
