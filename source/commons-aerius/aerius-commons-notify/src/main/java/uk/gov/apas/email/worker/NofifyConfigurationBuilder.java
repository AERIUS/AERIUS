/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package uk.gov.apas.email.worker;

import java.util.List;
import java.util.Locale;
import java.util.Properties;

import nl.overheid.aerius.shared.domain.email.EmailType;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.ConfigurationValidator;
import nl.overheid.aerius.worker.DatabaseConfigurationBuilder;

/**
 * Configuration Builder for Notify Worker.
 */
class NofifyConfigurationBuilder extends ConfigurationBuilder<NotifyConfiguration> {

  private static final String NOTIFY_PREFIX = "notify";
  private static final String API_KEY = "apikey";
  private static final List<EmailType> SUPPORTED_EMAIL_TYPES = List.of(EmailType.CONNECT_API_KEY, EmailType.ENGINE_INPUT, EmailType.ERROR,
      EmailType.GML_DOWNLOAD, EmailType.PDF_DOWNLOAD);

  public NofifyConfigurationBuilder(final Properties properties) {
    super(properties, NOTIFY_PREFIX);
    addBuilder(new DatabaseConfigurationBuilder<>(properties));
  }

  @Override
  protected boolean isActive() {
    return workerProperties.isActive();
  }

  @Override
  protected NotifyConfiguration create() {
    return new NotifyConfiguration(workerProperties.getProcesses());
  }

  @Override
  protected void build(final NotifyConfiguration configuration) {
    configuration.setApiKey(workerProperties.getProperty(API_KEY));
    SUPPORTED_EMAIL_TYPES.stream().forEach(et -> configuration.putTemplateId(et, workerProperties.getProperty(templateIdKey(et))));
  }

  @Override
  protected List<String> validate(final NotifyConfiguration configuration) {
    final ConfigurationValidator validator = new ConfigurationValidator();

    validator.validateRequiredProperty(workerProperties.getFullPropertyName(API_KEY), configuration.getApiKey());
    SUPPORTED_EMAIL_TYPES.stream()
        .forEach(et -> validator.validateRequiredProperty(workerProperties.getFullPropertyName(templateIdKey(et)), configuration.getTemplateId(et)));
    return validator.getValidationErrors();
  }

  private String templateIdKey(final EmailType et) {
    return "templateid." + et.name().toLowerCase(Locale.ROOT);
  }
}
