/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package uk.gov.apas.email.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.EmailType;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.shared.domain.email.MailTo;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.PropertiesUtil;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

import uk.gov.service.notify.NotificationClient;

/**
 * Test class for {@link NotifyEmailWorkerHandler}.
 */
@ExtendWith(MockitoExtension.class)
class NotifyEmailWorkerHandlerTest {

  private static final String APAS_EXAMPLE_COM = "apas@example.com";
  private static final String REFERENCE = "reference";
  private static final String ENGINE_INPUT_TEMPLATE_ID = "B1234";
  private static final String CREATION_DATE = "CREATION_DATE";

  private @Mock ResultSet resultSet;
  private @Mock PreparedStatement prepareStatement;
  private @Mock Connection connection;
  private @Mock PMF pmf;
  private @Mock WorkerConnectionHelper workerConnectionHelper;
  private @Mock NotificationClient notificationClient;

  private @Captor ArgumentCaptor<Map<String, String>> captor;

  @BeforeEach
  void beforeEach() throws SQLException {
    doReturn(resultSet).when(prepareStatement).executeQuery();
    doReturn(prepareStatement).when(connection).prepareStatement(any());
    doReturn(connection).when(pmf).getConnection();
    doReturn(pmf).when(workerConnectionHelper).getPMF();
  }

  @Test
  void testRun() throws Exception {
    final NotifyEmailWorkerFactory factory = new TestNotifyEmailWorkerFactory();
    final ConfigurationBuilder<NotifyConfiguration> builder = factory
        .configurationBuilder(PropertiesUtil.getFromPropertyFile("worker-notify-test.properties"));
    final NotifyConfiguration configuration = builder.buildAndValidate().get();

    final Worker<Serializable, Serializable> worker = factory.createWorkerHandler(configuration, workerConnectionHelper);
    final MailMessageData data = new MailMessageData(EmailType.ENGINE_INPUT, Locale.ENGLISH, new MailTo(APAS_EXAMPLE_COM));
    data.getReplacements().put(ReplacementToken.CREATION_DATE, CREATION_DATE);
    data.getReplacements().put(ReplacementToken.PROJECT_NAME, "");
    data.getEnumReplacementTokens().put(ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_ENGINE_INPUT);

    assertEquals(Boolean.TRUE, worker.run(data, new JobIdentifier(REFERENCE), null), "Should return true when call was successful");
    verify(notificationClient).sendEmail(eq(ENGINE_INPUT_TEMPLATE_ID), eq(APAS_EXAMPLE_COM), captor.capture(), eq(REFERENCE));
    for (final ReplacementToken token : ReplacementToken.values()) {
      assertTrue(captor.getValue().containsKey(token.name()), "Each token should be present in the map, even if not supplied");
    }
    assertEquals(CREATION_DATE, captor.getValue().get(ReplacementToken.CREATION_DATE.name()), "Should contain value for CREATION_DATE");
    assertEquals(NotifyEmailWorkerHandler.ABSENT_TOKEN_OR_EMPTY_REPLACEMENT, captor.getValue().get(ReplacementToken.PROJECT_NAME.name()), "Empty string should be replaced");
    assertEquals(NotifyEmailWorkerHandler.ABSENT_TOKEN_OR_EMPTY_REPLACEMENT, captor.getValue().get(ReplacementToken.JOB.name()), "Absent token should be replaced");
    assertEquals("Model input files", captor.getValue().get(ReplacementToken.EXPORT_TYPE_FILE.name()), "Should contain value for EXPORT_FILE_TYPE");
  }

  private class TestNotifyEmailWorkerFactory extends NotifyEmailWorkerFactory {

    @Override
    public Worker<Serializable, Serializable> createWorkerHandler(final NotifyConfiguration configuration,
        final WorkerConnectionHelper workerConnectionHelper) throws Exception {
      return createWorkerHandlerWithPMF(configuration, workerConnectionHelper);
    }

    @Override
    protected NotificationClient createNotificationClient(final NotifyConfiguration configuration) {
      return notificationClient;
    }
  }
}
