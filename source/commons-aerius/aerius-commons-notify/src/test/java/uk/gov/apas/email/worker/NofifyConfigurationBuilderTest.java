/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package uk.gov.apas.email.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.email.EmailType;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.PropertiesUtil;

/**
 * Test class for {@link NofifyConfigurationBuilder}
 */
class NofifyConfigurationBuilderTest {

  private static final List<String> REQUIRED_PROPERTIES = List.of("apikey", "templateid.connect_api_key", "templateid.engine_input",
      "templateid.error", "templateid.gml_download", "templateid.pdf_download");

  @Test
  void testBuilder() throws IOException {
    final NotifyEmailWorkerFactory factory = new NotifyEmailWorkerFactory();
    final ConfigurationBuilder<NotifyConfiguration> builder = factory
        .configurationBuilder(PropertiesUtil.getFromPropertyFile("worker-notify-test.properties"));
    final Optional<NotifyConfiguration> opt = builder.buildAndValidate();

    assertTrue(opt.isPresent(), "Configuration has processes, therefor should be active");
    final NotifyConfiguration configuration = opt.get();

    assertEquals(1, configuration.getProcesses(), "Configuration should have expected number of processes");
    // If admsRoot exists there should be no validation errors. If not exists it should have at most 1 warning, which will be about adms directory.
    assertTrue(builder.getValidationErrors().isEmpty(), "Should not have validation errors:"
        + String.join(",", builder.getValidationErrors()));

    assertEquals("1234", configuration.getApiKey(), "Should have expected API KEY");
    assertEquals("C1234", configuration.getTemplateId(EmailType.ERROR), "Should have the expected email error template id");
  }

  @Test
  void testValidator() throws IOException {
    final NotifyEmailWorkerFactory factory = new NotifyEmailWorkerFactory();
    final Properties properties = new Properties();
    properties.setProperty("notify.processes", "1"); // Set to 1 to read configuration
    final ConfigurationBuilder<NotifyConfiguration> builder = factory.configurationBuilder(properties);
    final NotifyConfiguration configuration = builder.buildAndValidate().get();
    final int expectedValidationErrors = expectedNumberOfValidationErrors(configuration);
    final List<String> validationErrors = builder.getValidationErrors();

    assertEquals(expectedValidationErrors, validationErrors.size(), "Should fail on every required notify property");
    REQUIRED_PROPERTIES.forEach(rp -> assertRequiredProperty(validationErrors, rp));
  }

  private int expectedNumberOfValidationErrors(final NotifyConfiguration configuration) {
    int count = 6;
    // Count database configuration validations per property because in some test environments these values could be set via environment variables.
    count += configuration.getDatabaseUrl() == null ? 1 : 0;
    count += configuration.getDatabaseUsername() == null ? 1 : 0;
    count += configuration.getDatabasePassword() == null ? 1 : 0;
    return count;
  }

  private void assertRequiredProperty(final List<String> validationErrors, final String requiredProperty) {
    final String rp = "notify." + requiredProperty;

    assertTrue(validationErrors.stream().anyMatch(v -> v.contains(rp)), "Missing validation for '" + rp + "'");
  }
}
