The files 'spatially_varying_dry_deposition_velocity_NO2_500m_20220614.tif' and 'spatially_varying_dry_deposition_velocity_NH3_500m_20220614.tif'
in this directory is just a dummy files.
This file is only present here for unit testing and should not be used in calculations.
