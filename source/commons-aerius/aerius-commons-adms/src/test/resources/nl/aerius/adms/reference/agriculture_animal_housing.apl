&ADMS_HEADER
Comment = "This is an ADMS parameter file"
Model = "ADMS"
Version = 5.2
FileVersion = 8
Complete = 1
/

&ADMS_PARAMETERS_SUP
SupSiteName                    = "Agriculture - housing (volume source)"
SupProjectName                 = " "
SupUseAddInput                 = 0
SupAddInputPath                = " "
SupReleaseType                 = 0
SupModelBuildings              = 0
SupModelComplexTerrain         = 0
SupModelCoastline              = 0
SupPufType                     = 0
SupCalcChm                     = 0
SupCalcDryDep                  = 0
SupCalcWetDep                  = 0
SupCalcPlumeVisibility         = 0
SupModelFluctuations           = 0
SupModelRadioactivity          = 0
SupModelOdours                 = 0
SupOdourUnits                  = "ou_e"
SupPaletteType                 = 1
SupUseTimeVaryingEmissions     = 0
SupTimeVaryingEmissionsType    = 2
SupTimeVaryingVARPath          = " "
SupTimeVaryingFACPath          = " "
SupTimeVaryingEmissionFactorsWeekday =
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
SupTimeVaryingEmissionFactorsSaturday =
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
SupTimeVaryingEmissionFactorsSunday =
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
/
&ADMS_PARAMETERS_MET
MetLatitude               = 5.2e+1
MetDataSource             = 0
MetDataFileWellFormedPath = "C:\RP\AERIUS Examples\example_met.met"
MetWindHeight             = 1.0e+1
MetWindInSectors          = 0
MetWindSectorSizeDegrees  = 1.0e+1
MetDataIsSequential       = 1
MetUseSubset              = 0
MetSubsetHourStart        = 1
MetSubsetDayStart         = 1
MetSubsetMonthStart       = 1
MetSubsetYearStart        = 2021
MetSubsetHourEnd          = 0
MetSubsetDayEnd           = 1
MetSubsetMonthEnd         = 1
MetSubsetYearEnd          = 2022
MetUseVerticalProfile     = 0
MetVerticalProfilePath    = " "
Met_DS_RoughnessMode      = 1
Met_DS_Roughness          = 2.0e-2
Met_DS_UseAdvancedMet     = 0
Met_DS_SurfaceAlbedoMode  = 0
Met_DS_SurfaceAlbedo      = 2.3e-1
Met_DS_PriestlyTaylorMode = 0
Met_DS_PriestlyTaylor     = 1.0e+0
Met_DS_MinLmoMode         = 0
Met_DS_MinLmo             = 1.0e+0
Met_DS_PrecipFactorMode   = 0
Met_DS_PrecipFactor       = 1.0e+0
Met_MS_RoughnessMode      = 1
Met_MS_Roughness          = 2.0e-2
Met_MS_UseAdvancedMet     = 0
Met_MS_SurfaceAlbedoMode  = 3
Met_MS_SurfaceAlbedo      = 2.3e-1
Met_MS_PriestlyTaylorMode = 3
Met_MS_PriestlyTaylor     = 1.0e+0
Met_MS_MinLmoMode         = 3
Met_MS_MinLmo             = 1.0e+0
MetHeatFluxType           = 0
MetInclBoundaryLyrHt      = 1
MetInclSurfaceTemp        = 0
MetInclLateralSpread      = 0
MetInclRelHumidity        = 0
MetHandNumEntries         = 0
/
&ADMS_PARAMETERS_BLD
BldNumBuildings = 0
/
&ADMS_PARAMETERS_HIL
HilGridSize          = 2
HilUseTerFile        = 1
HilUseRoughFile      = 0
HilTerrainPath       = " "
HilRoughPath         = " "
HilCreateFlowField   = 0
/
&ADMS_PARAMETERS_CST
CstPoint1X           = 0.0e+0
CstPoint1Y           = 0.0e+0
CstPoint2X           = -1.000e+3
CstPoint2Y           = 1.000e+3
CstLandPointX        = 5.00e+2
CstLandPointY        = 5.00e+2
/
&ADMS_PARAMETERS_GRD
GrdType                = 1
GrdCoordSysType        = 0
GrdSpacingType         = 0
GrdRegularMin          = 
  -1.000e+3 -1.000e+3 0.0e+0
  1.0e+1 0.0e+0 0.0e+0
GrdRegularMax          = 
  1.000e+3 1.000e+3 0.0e+0
  1.000e+3 3.30e+2 0.0e+0
GrdRegularNumPoints    = 
  31 31 1
  10 12 1
GrdVarSpaceNumPointsX  = 0
GrdVarSpaceNumPointsY  = 0
GrdVarSpaceNumPointsZ  = 0
GrdVarSpaceNumPointsR  = 0
GrdVarSpaceNumPointsTh = 0
GrdVarSpaceNumPointsZp = 0
GrdPtsNumPoints        = 1 0
GrdPtsPointNames =
  "Test"
GrdPtsPointsX =
  4.123925e+5
GrdPtsPointsY =
  1.2019273e+5
GrdPtsPointsZ =
  0.0e+0
GrdPolarCentreX = 0.0e+0
GrdPolarCentreY = 0.0e+0
GrdPtsUsePointsFile  = 0
GrdPtsPointsFilePath = " "
/
&ADMS_PARAMETERS_PUF
PufStart            = 1.00e+2
PufStep             = 1.00e+2
PufNumSteps         = 10
/
&ADMS_PARAMETERS_GAM
GamCalcDose         = 0
/
&ADMS_PARAMETERS_OPT
OptNumOutputs               = 1
OptPolName                  =
  "NH3"
OptInclude                  =
  1
OptShortOrLong              =
  1
OptSamplingTime             =
  1.0e+0
OptSamplingTimeUnits        =
  3
OptCondition                =
  0
OptNumPercentiles           =
  0
OptNumExceedences           =
  0
OptPercentiles              =
  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0
OptExceedences              =
  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0
OptUnits                    =
  "ug/m3"
OptGroupsOrSource           = 0
OptAllSources               = 1
OptNumGroups                = 0
OptIncludedSource           = "Source_1"
OptCreateComprehensiveFile  = 0
/
&ADMS_PARAMETERS_CHM
ChmScheme            = 2
/
&ADMS_PARAMETERS_BKG
BkgFilePath     = " "
BkgFixedLevels  = 2
/
&ADMS_PARAMETERS_ETC
SrcNumSources    = 1
PolNumPollutants = 13
PolNumIsotopes   = 0
/
&ADMS_COORDINATESYSTEM
ProjectedEPSG               = 27700
/
&ADMS_MAPPERPROJECT
ProjectFilePath               = " "
/

&ADMS_POLLUTANT_DETAILS
PolName                  = "NOx"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 5.2e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/
PolName                  = "NO2"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 5.2e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/

&ADMS_POLLUTANT_DETAILS
PolName                  = "NO"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 8.0e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/

&ADMS_POLLUTANT_DETAILS
PolName                  = "O3"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 5.0e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/

&ADMS_POLLUTANT_DETAILS
PolName                  = "VOC"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 3.1e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/

&ADMS_POLLUTANT_DETAILS
PolName                  = "SO2"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 3.7e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/

&ADMS_POLLUTANT_DETAILS
PolName                  = "PM10"
PolPollutantType         = 1
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-5
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 1.0e+0
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ug/m3"
/

&ADMS_POLLUTANT_DETAILS
PolName                  = "PM2.5"
PolPollutantType         = 1
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  2.5e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 1.0e+0
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ug/m3"
/

&ADMS_POLLUTANT_DETAILS
PolName                  = "NH3"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 2.0e-2
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 1.41e+0
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/

&ADMS_SOURCE_DETAILS
SrcName          = "Source_1"
SrcMainBuilding  = "(Main)"
SrcHeight        = 2.0e+0
SrcDiameter      = 0.0e+0
SrcVolFlowRate   = 0.0e+0
SrcVertVeloc     = 0.0e+0
SrcTemperature   = 0.0e+0
SrcMolWeight     = 2.8966e+1
SrcDensity       = 0.0e+0
SrcSpecHeatCap   = 1.012e+3
SrcSourceType    = 2
SrcReleaseAtNTP  = 0
SrcEffluxType    = 0
SrcBuoyancyType  = 2
SrcPercentNOxAsNO2 = 5.0e+0
SrcX1            = 0.0e+0
SrcY1            = 0.0e+0
SrcL1            = 4.0e+0
SrcL2            = 0.0e+0
SrcFm            = 0.0e+0
SrcFb            = 0.0e+0
SrcMassFlux      = 0.0e+0
SrcAngle1        = 0.0e+0
SrcAngle2        = 0.0e+0
SrcMassH2O       = 0.0e+0
SrcUseVARFile    = 0
SrcNumGroups     = 0
SrcNumVertices = 4
SrcTraNumTrafficFlows = 0
SrcNumPollutants      = 1
SrcPollutants =
  "NH3"
SrcPolEmissionRate =
  3.02116e-5
SrcPolTotalemission =
  1.0e+0
SrcPolStartTime =
  0.0e+0
SrcPolDuration =
  0.0e+0
SrcNumIsotopes        = 0
/
  &ADMS_SOURCE_VERTEX
  SourceVertexX = 4.12436672e+5
  SourceVertexY = 1.20011158e+5
  /

  &ADMS_SOURCE_VERTEX
  SourceVertexX = 4.1239981e+5
  SourceVertexY = 1.20072424e+5
  /

  &ADMS_SOURCE_VERTEX
  SourceVertexX = 4.12426192e+5
  SourceVertexY = 1.20088319e+5
  /

  &ADMS_SOURCE_VERTEX
  SourceVertexX = 4.12462926e+5
  SourceVertexY = 1.20026975e+5
  /
