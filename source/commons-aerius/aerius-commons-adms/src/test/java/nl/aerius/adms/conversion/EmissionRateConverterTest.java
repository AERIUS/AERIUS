/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link EmissionRateConverter}.
 */
class EmissionRateConverterTest {

  private static final int SOURCE_HEIGHT = 50;
  private static final double VERTICAL_DIMENSION = 100;
  private static final List<Substance> SUBSTANCES = List.of(Substance.NOX);
  private static final Point POINT = new Point(100, 200);

  @ParameterizedTest
  @MethodSource("data")
  void testES2ADMS(final SourceType sourceType, final Geometry geometry, final double admsEmission, final double emissionSourceEmission)
      throws AeriusException {
    final Map<Substance, Double> emissions = Map.of(Substance.NOX, emissionSourceEmission);
    final ADMSSource<Geometry> admsSource = createADMSSource(sourceType, geometry);

    EmissionRateConverter.emissionSource2ADMSEmission(SUBSTANCES, emissions, admsSource);
    assertEquals(admsEmission, admsSource.getEmission(Substance.NOX), 1.0E-10, "Not the expected the ADMS emission rate.");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testADMS2ES(final SourceType sourceType, final Geometry geometry, final double admsEmission, final double emissionSourceEmission)
      throws AeriusException {
    final ADMSSource<Geometry> admsSource = createADMSSource(sourceType, geometry);
    admsSource.setEmission(Substance.NOX, admsEmission);
    final Map<Substance, Double> emissions = new HashMap<>();

    EmissionRateConverter.adms2EmissionSourceEmissions(admsSource, emissions);
    assertEquals(emissionSourceEmission, emissions.get(Substance.NOX), 1.0E-10, "Not the expected emssion source emission rate.");
  }

  private ADMSSource<Geometry> createADMSSource(final SourceType sourceType, final Geometry geometry) {
    final ADMSSource<Geometry> admsSource = new ADMSSource<>();
    admsSource.setSourceType(sourceType);
    admsSource.setGeometry(geometry);
    admsSource.setHeight(SOURCE_HEIGHT);
    admsSource.setL1(VERTICAL_DIMENSION);
    return admsSource;
  }

  // Data: SourceType, geometry, adms emission, emission source emission
  private static Object[] data() {
    return new Object[][] {
      // Point
      {SourceType.POINT, POINT, 10.0, 315360.0},
      // Jet
      {SourceType.JET, POINT, 10.0, 315360.0},
      // Area
      {SourceType.AREA, polygon(), 1.0E-1, 315360.0},
      // Volume
      {SourceType.VOLUME, polygon(), 3.17097919837646E-05, 10_000.0},
      // Line
      {SourceType.LINE, line(), 10.0, 78840000.0},
      // Road
      {SourceType.ROAD, line(), 10.0, 78840.0},
    };
  }

  private static Polygon polygon() {
    final Polygon polygon = new Polygon();
    polygon.setCoordinates(new double[][][] {{{10, 10}, {20, 10}, {20, 20}, {10, 20}, {10, 10}}});
    return polygon;
  }

  private static LineString line() {
    final LineString line = new LineString();

    line.setCoordinates(new double[][] {{100, 200}, {350, 200}});
    return line;
  }
}
