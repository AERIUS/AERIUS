/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.nio.file.Files;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.util.ModelDataUtil;
import nl.overheid.aerius.util.OSUtils;

@ExtendWith(MockitoExtension.class)
class ADMSModelDataUtilTest {

  private static final String PATH_TO_MODEL_VERSIONS_ROOT = OSUtils.isWindows()
      ? "C:\\path\\to\\model\\root"
      : "/path/to/model/root";
  private static final String LICENSE_FILE_CONTENTS = "This is a test";
  private static final String BASE_64_ENCODED_FILE_CONTENTS = Base64.getEncoder().encodeToString(LICENSE_FILE_CONTENTS.getBytes());
  private static final String URL = "url";

  private @Mock ModelDataUtil modelDataUtil;
  private ADMSModelDataUtil admsModelDataUtil;

  private @Mock ADMSConfiguration config;

  @BeforeEach
  void before() {
    admsModelDataUtil = new ADMSModelDataUtil(modelDataUtil);
  }

  @Test
  void testModelDataUtil() throws Exception {
    doReturn(URL).when(config).getModelDataUrl();
    doReturn(List.of(ADMSVersion.NCA_LATEST)).when(config).getModelPreloadVersions();
    doReturn(new File(PATH_TO_MODEL_VERSIONS_ROOT)).when(config).getVersionsRoot();

    admsModelDataUtil.ensureModelDataPresence(config);

    final Set<String> expectedDatas = Set.of(
        ADMSVersion.NCA_LATEST.getVersion(),
        ADMSVersion.NCA_LATEST.getDepositionVelocityVersion(),
        ADMSVersion.NCA_LATEST.getSurfaceRoughnessVersion(),
        ADMSVersion.NCA_LATEST.getPlumeDepletionVersionFormat(),
        ADMSVersion.NCA_LATEST.getTerrainHeightVersionFormat());

    for (final String expectedData : expectedDatas) {
      verify(modelDataUtil).ensureModelDataAvailable(eq(URL), eq(ADMSModelDataUtil.MODEL), eq(expectedData),
          eq(PATH_TO_MODEL_VERSIONS_ROOT), any(), any());
    }
  }

  @Test
  void testModelDataUtilWithLicense(@TempDir final File versionsRoot) throws Exception {
    final File admsRoot = new File(versionsRoot, ADMSVersion.NCA_LATEST.getVersion());
    admsRoot.mkdir();

    doReturn(URL).when(config).getModelDataUrl();
    doReturn(List.of(ADMSVersion.NCA_LATEST)).when(config).getModelPreloadVersions();
    doReturn(versionsRoot).when(config).getVersionsRoot();
    doReturn(Optional.of(BASE_64_ENCODED_FILE_CONTENTS)).when(config).getAdmsLicenseBase64();

    admsModelDataUtil.ensureModelDataPresence(config);

    final File expectedLicenseFile = new File(admsRoot, ADMSExecutor.ADMS_URBAN_LIC_NAME);
    assertTrue(expectedLicenseFile.exists(), "When a base64 encoded license is present in config, a license file should be written.");
    assertEquals(LICENSE_FILE_CONTENTS, Files.readString(expectedLicenseFile.toPath()),
        "License from config should be decoded and written to license file.");
  }
}
