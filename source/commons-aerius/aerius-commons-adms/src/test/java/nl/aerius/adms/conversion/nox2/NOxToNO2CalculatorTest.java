/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion.nox2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.aerius.adms.conversion.nox2.NOxToNO2Calculator.NO2Results;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData.FractionNO2;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData.LACodeBackgroundConcentration;
import nl.aerius.adms.domain.ADMSPointNO2Data;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Test class for {@link NOxToNO2Calculator} calculator.
 * The data used in this unit test is referenced by entering the values in the NOx to NO2 calculator spreadsheet and use the output of the spreadsheet
 * as reference output in this unit test.
 */
class NOxToNO2CalculatorTest {

  // Background values of Cheshire East year 2030:
  // Background Ozone
  private static final double BACKGROUND_O3 = 61.446;
  // Background Oxides of nitrogen
  private static final double BACKGROUND_NOX = 5.76;
  // Background Nitrogen dioxide
  private static final double BACKGROUND_NO2 = 4.601;

  private static final LACodeBackgroundConcentration BACKGROUND_DATA =
      new LACodeBackgroundConcentration().put(Substance.O3, BACKGROUND_O3).put(Substance.NOX, BACKGROUND_NOX).put(Substance.NO2, BACKGROUND_NO2);

  //  All non-urban UK traffic:
  //  Local fraction NOx emitted as as NO2
  private static final double LOCAL_FNO2 = 0.261062089095881;
  //  Regional fraction NOx emitted as NO2
  private static final double REGIONAL_FNO2 = 0.261062089095881;

  private static final FractionNO2 FRACTION_NO2 = new FractionNO2(LOCAL_FNO2, REGIONAL_FNO2);

  @ParameterizedTest
  @MethodSource("cheshireEast")
  void testRunNOxToNO2(final double roadNOx, final double backgroundNOx, final double expectedTotalNO2, final double expectedRoadNO2) {
    final ADMSPointNO2Data data = new ADMSPointNO2Data(backgroundNOx, BACKGROUND_DATA.getNOx(), BACKGROUND_DATA.getNO2(), BACKGROUND_DATA.getO3(),
        FRACTION_NO2.local(), FRACTION_NO2.regional());
    final NO2Results result = NOxToNO2Calculator.calculateNOxToNO2(data, roadNOx);
    assertEquals(expectedTotalNO2, result.getBackground(), 1.0, "Expected total");
    assertEquals(expectedRoadNO2, result.getRoad(), 0.000000001, "Expected road");
  }

  static Object[][] cheshireEast() {
    return new Object[][] {
      //  Road increment NOx |  Total NO2 | Road NO2
      //  mg m-3 | mg m-3 | mg m-3
      // @formatter:off
      // background 0
      {  0.005, 0,   2.17415309811085,  0.00158359074474923 },
      {  0.5,   0,   2.34132855516193,  0.168759047795821 },
      {  1,     0,   2.53076036249779,  0.35819085513168 },
      {  10,    0,   7.40045699283964,  5.22788748547353 },
      {  50,    0,  26.9642075126365,  24.7916380052704 },
      {  100,   0,  46.8178763577087,  44.6453068503426 },
      {  200,   0,  78.4368370069727,  76.2642674996066 },
      {  1000,  0, 290.73900962992,   288.566440122554 },
      // background 100
      {  0.005, 100,  51.7580618210256,  0.00221630311870769 },
      {  0.5,   100,  51.9772123099366,  0.221366792029684 },
      {  1,     100,  52.1980480095784,  0.442202491671473 },
      {  10,    100,  56.0845414227169,  4.328695904810063 },
      {  50,    100,  71.609514362979,  19.8536688450722 },
      {  100,   100,  88.244659223147,  36.4888137052401 },
      {  200,   100, 117.387363636412,  65.631518118505 },
      {  1000,  100, 328.605298894814, 276.849453376908 },
      // @formatter:on
    };
  }
}
