/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.function.BiConsumer;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;

import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.domain.ADMSPointSource;
import nl.aerius.adms.io.AplExportWriter;
import nl.aerius.adms.util.ADMSProgressCollector;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;

/**
 * Test class for {@link ProcessADMS}.
 */
class ProcessADMSTest extends BaseADMSRunnerTest {

  public ProcessADMSTest() {
    super(false);
  }

  /**
   * Test is with or without quick run enabled the correct extent is used.
   * <ul>
   * <li>Quick run enabled: Should have extent based on the input
   * <li>Quick run disabled: Should use an extent derived from the input
   * </ul>
   *
   * @param quickRun if quick run is enabled
   * @param sameExtent if it is expected the same extent as the input is used
   */
  @ParameterizedTest
  @CsvSource({"true,false", "false,true"})
  void testExtent(final boolean quickRun, final boolean sameExtent) throws Exception {
    final ADMSRunner<Boolean> runner = mock(ADMSRunner.class);
    final AplExportWriter aplExportWriter = mock(AplExportWriter.class);
    final ProcessADMS<Boolean> process = new ProcessADMS<Boolean>(executor, runner, admsData, getADMSConfiguration(), false) {
      @Override
      protected AplExportWriter createAplExportWriter(final ADMSVersion admsVersion, final String newline) {
        return aplExportWriter;
      }
    };
    final BiConsumer<ADMSGroupKey, Boolean> consumer = mock(BiConsumer.class);
    final ADMSProgressCollector progressCollector = mock(ADMSProgressCollector.class);
    final ADMSInputData input = ADMSTestDataGenerator.newADMSInputData(CommandType.CALCULATE);

    input.setQuickRun(quickRun);
    input.setExtent(new BBox(1E6, 1E6, 1E4));
    input.setAdmsOptions(new ADMSOptions());
    input.getAdmsOptions().setMetYears(List.of(ADMSTestDataGenerator.TEST_MET_YEAR));
    final ADMSPointSource source = new ADMSPointSource();
    source.setSourceType(SourceType.POINT);
    source.setGeometry(new Point(1E5, 1E5));
    input.setEmissionSources(1, List.of(source));
    final ADMSCalculationPoint receptor = new ADMSCalculationPoint();
    receptor.setCoordinates(new double[] {1.1E5, 1.1E5});
    input.setReceptors(List.of(receptor));
    process.run(input, consumer, progressCollector, null, System.lineSeparator());

    final ArgumentCaptor<BBox> extentCaptor = ArgumentCaptor.forClass(BBox.class);
    verify(aplExportWriter).write(any(), eq(input), any(), any(), any(), extentCaptor.capture());
    if (sameExtent) {
      assertEquals(input.getExtent(), extentCaptor.getValue(), "Should have the same extent");
    } else {
      assertNotEquals(input.getExtent(), extentCaptor.getValue(), "Should not have the same extent");
    }
  }
}
