/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.backgrounddata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.geo.shared.BBox;

/**
 * Test class for {@link GridResolution}.
 */
class GridResolutionTest {

  @ParameterizedTest
  @MethodSource("data")
  void testGetGridResolutionFromExtent(final double diameter, final GridResolution expectedGridResolution) {
    final BBox extent = new BBox(0, 0, diameter, diameter);

    assertEquals(expectedGridResolution, GridResolution.getGridResolutionFromExtent(extent), "Not the expected grid resolution");
  }

  private static List<Arguments> data() {
    return List.of(
        Arguments.of(1_000, GridResolution.GR_1_50),
        Arguments.of(3_000, GridResolution.GR_2_50),
        Arguments.of(15_000, GridResolution.GR_2_200),
        Arguments.of(50_000, GridResolution.GR_4_150));
  }
}
