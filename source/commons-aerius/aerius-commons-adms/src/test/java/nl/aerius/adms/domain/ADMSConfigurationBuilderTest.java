/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import nl.aerius.adms.ADMSWorkerFactory;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.PropertiesUtil;

/**
 * Test class for {@link ADMSConfigurationBuilder}.
 */
class ADMSConfigurationBuilderTest {

  @Test
  void testBuilder() throws IOException {
    final ADMSWorkerFactory factory = new ADMSWorkerFactory();
    final ConfigurationBuilder<ADMSConfiguration> builder = factory.configurationBuilder(PropertiesUtil.getFromTestPropertiesFile("adms"));
    final Optional<ADMSConfiguration> opt = builder.buildAndValidate();

    assertTrue(opt.isPresent(), "Configuration has processes, therefor should be active");
    final ADMSConfiguration configuration = opt.get();
    final File admsRoot = configuration.getADMSRoot(ADMSVersion.NCA_LATEST);

    assertEquals(4, configuration.getProcesses(), "Configuration should have expected number of processes");
    // If admsRoot exists there should be no validation errors. If not exists it should have at most 1 warning, which will be about adms directory.
    assertEquals(admsRoot.exists() ? 0 : 1, builder.getValidationErrors().size(), "Should not have validation errors:"
        + String.join(",", builder.getValidationErrors()));
    assertNotNull(admsRoot, "Should return a none null root");
    assertNotNull(configuration.getRunFilesDirectory(), "Should return a run directory");
  }
}
