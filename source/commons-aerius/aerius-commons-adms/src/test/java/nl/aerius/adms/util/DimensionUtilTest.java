/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Named;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.aerius.adms.ADMSTestDataGenerator;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;

/**
 * Test class for {@link DimensionUtil}.
 */
class DimensionUtilTest {

  @Test
  void testDetermineMaxDimension() {
    assertEquals(0.0, DimensionUtil.determineMaxDimension((Collection<EngineSource>) null), "Max dimension no collection");
    assertEquals(0.0, DimensionUtil.determineMaxDimension(List.of()), "Max dimension no source");
    assertEquals(5.0, DimensionUtil.determineMaxDimension(List.of(simplePointSource(5.0, 1.0))), "Max dimension 1 source");
    assertEquals(7.0, DimensionUtil.determineMaxDimension(
        List.of(simplePointSource(5.0, 1.0), simplePointSource(1.0, 7.0), simplePointSource(4.0, 4.0))),
        "Max dimension 3 sources");
  }

  @ParameterizedTest
  @MethodSource("casesForSingleSource")
  void testToMaxDimension(final ADMSSource<?> engineSource, final double expectedMax) {
    assertEquals(expectedMax, DimensionUtil.toMaxDimension(engineSource), "Max dimension for source");
  }

  private static Stream<Arguments> casesForSingleSource() {
    return Stream.of(
        Arguments.of(Named.of("Null source", null), 0.0),
        Arguments.of(Named.of("Point with height max", simplePointSource(5.0, 1.0)), 5.0),
        Arguments.of(Named.of("Point with diameter max", simplePointSource(1.0, 5.0)), 5.0),
        Arguments.of(Named.of("Jet with height max", simpleJetSource(7.0, 1.0)), 7.0),
        Arguments.of(Named.of("Jet with diameter max", simpleJetSource(1.0, 7.0)), 7.0),
        Arguments.of(Named.of("Line with geometry max", simpleLineSource(new double[][] {{10, 10}, {10, 20}, {10, 100}}, 1.0, 1.0)), 80.0),
        Arguments.of(Named.of("Line with empty geometry", simpleLineSource(new double[][] {{}}, 1.0, 1.0)), 1.0),
        Arguments.of(Named.of("Line with incorrect geometry", simpleLineSource(new double[][] {{10, 10}}, 1.0, 1.0)), 1.0),
        Arguments.of(Named.of("Line with height max", simpleLineSource(new double[][] {{1, 1}, {1, 2}, {1, 10}}, 20.0, 1.0)), 20.0),
        Arguments.of(Named.of("Line with width max", simpleLineSource(new double[][] {{1, 1}, {1, 2}, {1, 10}}, 1.0, 20.0)), 20.0),
        Arguments.of(Named.of("Road with geometry max", simpleRoadSource(new double[][] {{10, 10}, {10, 20}, {10, 100}}, 1.0, 1.0)), 80.0),
        Arguments.of(Named.of("Road with height max", simpleRoadSource(new double[][] {{1, 1}, {1, 2}, {1, 10}}, 20.0, 1.0)), 20.0),
        Arguments.of(Named.of("Road with width max", simpleRoadSource(new double[][] {{1, 1}, {1, 2}, {1, 10}}, 1.0, 20.0)), 20.0),
        Arguments.of(Named.of("Area with geometry max", simpleAreaSource(new double[][][] {{{10, 10}, {10, 20}, {10, 100}, {20, 50}}}, 1.0)), 90.0),
        Arguments.of(Named.of("Area with height max", simpleAreaSource(new double[][][] {{{1, 1}, {1, 2}, {1, 10}, {2, 5}}}, 20.0)), 20.0),
        Arguments.of(Named.of("Volume with geometry max", simpleVolumeSource(new double[][][] {{{10, 10}, {10, 20}, {10, 100}, {20, 50}}}, 1.0, 1.0)),
            90.0),
        Arguments.of(Named.of("Volume with height max", simpleVolumeSource(new double[][][] {{{1, 1}, {1, 2}, {1, 10}, {2, 5}}}, 30.0, 1.0)), 30.0),
        Arguments.of(
            Named.of("Volume with vertical dimension max", simpleVolumeSource(new double[][][] {{{1, 1}, {1, 2}, {1, 10}, {2, 5}}}, 1.0, 30.0)),
            30.0));
  }

  private static ADMSSource<?> simplePointSource(final double height, final double diameter) {
    final ADMSSource<?> source = ADMSTestDataGenerator.simplePointSource(3, 2);

    source.setHeight(height);
    source.setDiameter(diameter);
    return source;
  }

  private static ADMSSource<?> simpleJetSource(final double height, final double diameter) {
    final ADMSSource<Point> source = new ADMSSource<>();
    source.setSourceType(SourceType.JET);
    source.setGeometry(new Point(6, 7));
    source.setHeight(height);
    source.setDiameter(diameter);
    return source;
  }

  private static ADMSSource<?> simpleLineSource(final double[][] coordinates, final double height, final double width) {
    final ADMSSource<LineString> source = new ADMSSource<>();
    source.setSourceType(SourceType.LINE);
    final LineString lineString = new LineString();
    lineString.setCoordinates(coordinates);
    source.setGeometry(lineString);
    source.setHeight(height);
    source.setL1(width);
    return source;
  }

  private static ADMSSource<?> simpleRoadSource(final double[][] coordinates, final double height, final double width) {
    final ADMSSource<LineString> source = new ADMSSource<>();
    source.setSourceType(SourceType.ROAD);
    final LineString lineString = new LineString();
    lineString.setCoordinates(coordinates);
    source.setGeometry(lineString);
    source.setHeight(height);
    source.setL1(width);
    return source;
  }

  private static ADMSSource<?> simpleAreaSource(final double[][][] coordinates, final double height) {
    final ADMSSource<Polygon> source = new ADMSSource<>();
    source.setSourceType(SourceType.AREA);
    final Polygon polygon = new Polygon();
    polygon.setCoordinates(coordinates);
    source.setGeometry(polygon);
    source.setHeight(height);
    return source;
  }

  private static ADMSSource<?> simpleVolumeSource(final double[][][] coordinates, final double height, final double verticalDimension) {
    final ADMSSource<Polygon> source = new ADMSSource<>();
    source.setSourceType(SourceType.VOLUME);
    final Polygon polygon = new Polygon();
    polygon.setCoordinates(coordinates);
    source.setGeometry(polygon);
    source.setHeight(height);
    source.setL1(verticalDimension);
    return source;
  }
}
