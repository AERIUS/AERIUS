/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static nl.aerius.adms.BaseADMSRunnerTest.ADMS_VERSION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NO2_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_DEPOSITION;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.opengis.referencing.FactoryException;

import nl.aerius.adms.backgrounddata.MetDataFileKey;
import nl.aerius.adms.domain.ADMSEngineDataKey;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.domain.ADMSRoadSource;
import nl.aerius.adms.domain.ADMSSource;
import nl.aerius.adms.domain.ADMSSubReceptor;
import nl.aerius.adms.domain.ADMSSubReceptorCreator;
import nl.aerius.adms.io.ADMSInputReaderUtil;
import nl.aerius.adms.io.ADMSSourceFileType;
import nl.overheid.aerius.calculation.EngineInputData.ChunkStats;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.calculation.MetDatasetType;
import nl.overheid.aerius.shared.domain.calculation.MetSurfaceCharacteristics;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.OSUtils;

/**
 * Util class to generate test data.
 */
public final class ADMSTestDataGenerator {

  public static final int TEST_MET_SITE_ID = 0;
  public static final MetDatasetType TEST_MET_DATASET_TYPE = MetDatasetType.OBS_RAW_GT_90PCT;
  public static final String TEST_MET_YEAR = "2017";
  public static final MetDataFileKey TEST_MET_DATA_FILE_KEY = new MetDataFileKey(String.valueOf(TEST_MET_SITE_ID), TEST_MET_DATASET_TYPE, TEST_MET_YEAR);

  public static final ADMSEngineDataKey DEFAULT_ENGINE_DATA_KEY = new ADMSEngineDataKey(TEST_MET_YEAR);

  private static final String TIME_ZONE = "Europe/London";
  private static final String STREET_NAME = "Street";
  private static final Object FILE_BREAK_PATTERN = "&ADMS_SOURCE_DETAILS";

  private ADMSTestDataGenerator() {
    // Util class
  }

  /**
   * @return Returns the names of the files in the reference directory.
   */
  public static List<String> referenceFiles() throws URISyntaxException, IOException {
    return FileUtil.getFilteredFiles(
        new File(Path.of(ADMSTestDataGenerator.class.getResource(".").toURI()).toFile(), "reference"), s -> true)
        .stream().map(File::getName).collect(Collectors.toList());
  }

  /**
   * Read the lines of a file being a UPL file (file name passed to be expected not having extension in the name itself).
   *
   * @param file UPL file path
   * @return content of the UPL file
   * @throws IOException
   */
  public static List<String> readLinesUplFile(final File file) throws IOException {
    return Files.readAllLines(ADMSSourceFileType.UPL.fileWithExtension(file).toPath());
  }

  /**
   * Finds the value trimmed related to key (e.g. everything after the =) or if nothing found an empty string.
   * @param lines lines to search
   * @param key key to find
   * @return matched trimmed value or empty string
   */
  public static String findKeyValue(final List<String> lines, final String key) {
    return lines.stream().filter(line -> line.contains(key)).findAny().orElse("").replaceAll(key + "\s+=", "").trim();
  }

  /**
   * Find the key in the lines and returns the next line trimmed.
   *
   * @param lines lines to search
   * @param key key to find
   * @return matched trimmed data of next line or empty string
   */
  public static String findKeyNextLineValue(final List<String> lines, final String key) {
    for (final Iterator<String> iterator = lines.iterator(); iterator.hasNext();) {
      if (iterator.next().contains(key)) {
        return iterator.next().trim(); // return value on next line
      }
    }
    return "";
  }

  public static ADMSInputData newADMSInputData(final CommandType commandType) {
    return new ADMSInputData(DEFAULT_ENGINE_DATA_KEY, commandType);
  }

  public static ADMSInputData createInput(final ADMSGroupKey admsGroupKey) throws IOException, AeriusException, FactoryException {
    final ADMSInputData input = new ADMSInputData(DEFAULT_ENGINE_DATA_KEY, CommandType.CALCULATE);

    input.setChunkStats(new ChunkStats("1", 1));
    input.setAdmsVersion(ADMS_VERSION);
    input.setAdmsOptions(createDefaultADMSOptions());
    input.setFileType(ADMSSourceFileType.UPL);
    input.setTimeZone(TIME_ZONE);
    input.setEmissionResultKeys(EnumSet.of(NOX_CONCENTRATION, NO2_CONCENTRATION, NOX_DEPOSITION));
    if (admsGroupKey == ADMSGroupKey.NOX_ROAD) {
      input.setSubstances(Stream.concat(admsGroupKey.getEmissionSubstances(false).stream(), Stream.of(Substance.NO2)).toList());
    } else {
      input.setSubstances(admsGroupKey.getEmissionSubstances(true));
    }
    setMetSiteOptions(input);
    ADMSInputReaderUtil.read2ADMSInputData("pointsource.upl", admsGroupKey.getIndex(), input);

    return input;
  }

  private static ADMSOptions createDefaultADMSOptions() {
    final ADMSOptions options = new ADMSOptions();

    options.setMetSiteId(TEST_MET_SITE_ID);
    options.setMetDatasetType(TEST_MET_DATASET_TYPE);
    options.setMetYears(List.of(TEST_MET_YEAR));
    options.setSurfaceAlbedo(ADMSLimits.SURFACE_ALBEDO_DEFAULT);
    options.setPriestleyTaylorParameter(ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_DEFAULT);
    options.setMinMoninObukhovLength(ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT);

    options.setSpatiallyVaryingRoughness(false);
    return options;
  }

  private static void setMetSiteOptions(final ADMSInputData input) {
    final ADMSOptions options = input.getAdmsOptions();
    final MetSurfaceCharacteristics msc = MetSurfaceCharacteristics.builder()
        .surfaceAlbedo(ADMSLimits.SURFACE_ALBEDO_DEFAULT)
        .priestleyTaylorParameter(ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_DEFAULT)
        .minMoninObukhovLength(ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT)
        .roughness(0.5)
        .windInSectors(false)
        .build();

    options.putMetSiteCharacteristics(TEST_MET_YEAR, msc);
    options.setMetSiteLatitude(52.0);
    input.setMeteoYear(TEST_MET_YEAR);
  }

  public static void setReceptors(final ADMSInputData input) {
    final Collection<ADMSCalculationPoint> receptors = new ArrayList<>();
    final ADMSCalculationPoint point = createSimpleADMSCalculationPoint(544400.0, 258900.0);

    point.setHeight(1.0);
    receptors.add(point);
    input.setReceptors(receptors);
  }

  public static ADMSCalculationPoint createSimpleADMSCalculationPoint(final double x, final double y) {
    return new ADMSCalculationPoint(new AeriusPoint(1, 0, AeriusPointType.POINT, x, y), AeriusPointType.POINT);
  }

  public static void setSubReceptors(final ADMSInputData input, final int level) throws AeriusException {
    final ADMSSubReceptorCreator creator = new ADMSSubReceptorCreator(100D);
    final Collection<ADMSCalculationPoint> receptors = new ArrayList<>();

    final AeriusPoint point = new AeriusPoint(1000, 0, AeriusPointType.RECEPTOR, 544400.0, 258900.0);
    point.setHeight(1.0);
    final ADMSSubReceptor admsReceptor = new ADMSSubReceptor(point, AeriusPointType.RECEPTOR, 0);
    receptors.add(admsReceptor);
    receptors.addAll(creator.computePoints(admsReceptor, level, p -> true));
    input.setReceptors(receptors);
  }

  /**
   * Filter out the given data of input that can be different, but should not make a difference when textual comparing generated file content with
   * reference file content.
   * The normalize also ignores the first part up till the FILE_BREAK_PATTERN.
   *
   * @param bytes bytes data as read from a file.
   * @return normalized and skipped content.
   */
  public static String normalize(final byte[] bytes) {
    final String filteredString = new String(bytes, StandardCharsets.UTF_8)
        .replaceAll("\\s{1,20}=", "=")
        .replaceAll(OSUtils.NL + OSUtils.NL, OSUtils.NL)
        // filter out dynamic output path of asp file.
        .replaceFirst("GrdPtsPointsFilePath= \"[^\\.]+\\.asp\"", "GrdPtsPointsFilePath= \"file.asp\"")
        // filter out dynamic output path of dummy .met file.
        .replaceFirst("MetDataFileWellFormedPath= \"[^\\.]+\\.met\"", "MetDataFileWellFormedPath= \"\"");

    return skip(filteredString);
  }

  /**
   * Filter out the lines before the expected pattern. This is used to filter out the first part of the input files.
   * This allows for gradually improve the tests. Because the first part of the file needs to be researched what it should contain
   * and skipping that part makes it possible to create tests that are useful because they run successful for the part we want to test.
   *
   * @param input string to filter
   * @return filtered or original in case no match found
   */
  private static String skip(final String input) {
    final String[] lines = input.split(OSUtils.NL);

    for (int i = 0; i < lines.length; i++) {
      if (FILE_BREAK_PATTERN.equals(lines[i])) {
        return IntStream.range(i, lines.length).mapToObj(j -> lines[j]).collect(Collectors.joining(OSUtils.NL));
      }
    }
    return input;
  }

  public static ADMSSource<?> simplePointSource(final int x, final int y) {
    final ADMSSource<Point> source = new ADMSSource<>();
    source.setSourceType(SourceType.POINT);
    source.setGeometry(new Point(x, y));
    return source;
  }

  /**
   * Generate dummy road source objects.
   */
  public static List<ADMSSource<?>> createRoadSources() {
    final double[][][] coordinates = new double[][][] {{{544788.0, 258880.0}, {544910.92, 258725.07}, {544999, 258999}},
      {{544606.0, 259056.0}, {544571.7, 259092.54}, {544599, 259999}},
      {{545326.67, 259480.59}, {545122.91, 259393.41}, {545199, 259999}},
      {{544910.92, 258725.07}, {544979.72, 258792.88}, {544999, 259999}},
      {{544606.0, 259056.0}, {544681.91, 258977.08}, {544699, 259999}},
      {{544606.0, 259056.0}, {544568.94, 259037.1}, {544599, 259999}}};
      final List<ADMSSource<?>> sources = new ArrayList<>();

      for (int i = 0; i < coordinates.length; i++) {
        sources.add(createRoadSource(name(i + 1), coordinates[i]));
      }
      return sources;
  }

  private static String name(final int i) {
    return STREET_NAME + "-" + i;
  }

  public static ADMSSource<?> createRoadSource(final String name, final double[][] coordinates) {
    final ADMSRoadSource roadSource = new ADMSRoadSource();
    roadSource.setName(name);
    roadSource.setSourceType(SourceType.ROAD);
    roadSource.setL1(2.5);
    roadSource.setEmission(Substance.NOX, 43.2);

    final LineString line = new LineString();
    line.setCoordinates(coordinates);
    roadSource.setGeometry(line);

    return roadSource;
  }

  public static ADMSGroupKey getADMSGroupKey(final List<Substance> substances) {
    final boolean nh3 = substances.contains(Substance.NH3);
    final boolean nox = substances.contains(Substance.NOX);

    return nh3 && nox ? ADMSGroupKey.NOX_NH3 : (nh3 ? ADMSGroupKey.NH3 : ADMSGroupKey.NOX_OTHER);
  }
}
