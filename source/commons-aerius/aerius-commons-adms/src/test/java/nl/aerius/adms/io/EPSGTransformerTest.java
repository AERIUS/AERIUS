/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;
import org.opengis.referencing.FactoryException;

import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link EPSGTransformer}.
 */
class EPSGTransformerTest {

  private static final double BNG_X = 114740.0;
  private static final double BNG_X2 = 125564.0;
  private static final double BNG_Y = 558276.0;
  private static final double BNG_Y2 = 567387.0;

  @Test
  void testEPSGTransformBuilding() throws IOException, AeriusException, FactoryException  {
    final AplFileContent content = readContent29903();
    final Point buildingCentre = content.getBuildings().get(0).getBuildingCentre();
    assertEquals(BNG_X, (int) buildingCentre.getX(), "TM75 X-coordinate of building not correctly converted to BNG");
    assertEquals(BNG_Y, (int) buildingCentre.getY(), "TM75 Y-coordinate of building not correctly converted to BNG");
  }

  @Test
  void testEPSGTransformPointSource() throws IOException, AeriusException, FactoryException {
    final AplFileContent content = readContent29903();
    final Point pointSource = (Point) content.getObjects().get(0).getGeometry();
    assertEquals(BNG_X, (int) pointSource.getX(), "TM75 X-coordinate of point source not correctly converted to BNG");
    assertEquals(BNG_Y, (int) pointSource.getY(), "TM75 Y-coordinate of point source not correctly converted to BNG");
  }

  @Test
  void testEPSGTransformLineSource() throws IOException, AeriusException, FactoryException {
    final AplFileContent content = readContent29903();
    final LineString lineSource = (LineString) content.getObjects().get(1).getGeometry();
    final double[][] coordinates = lineSource.getCoordinates();
    assertEquals(BNG_X, (int) coordinates[0][0], "TM75 X-coordinate of first coordinate line source not correctly converted to BNG");
    assertEquals(BNG_Y, (int) coordinates[0][1], "TM75 Y-coordinate of first coordinate line source not correctly converted to BNG");
    assertEquals(BNG_X2, (int) coordinates[1][0], "TM75 X-coordinate of second coordinate line source not correctly converted to BNG");
    assertEquals(BNG_Y2, (int) coordinates[1][1], "TM75 Y-coordinate of second coordinate line source not correctly converted to BNG");
  }

  @Test
  void testEPSGTransformPolygon() throws IOException, AeriusException, FactoryException {
    final AplFileContent content = readContent29903();
    final Polygon polygonSource = (Polygon) content.getObjects().get(2).getGeometry();
    final double[][][] coordinates = polygonSource.getCoordinates();
    assertEquals(BNG_X, (int) coordinates[0][0][0], "TM75 X-coordinate of first coordinate polygon source not correctly converted to BNG");
    assertEquals(BNG_Y, (int) coordinates[0][0][1], "TM75 Y-coordinate of first coordinate polygon source not correctly converted to BNG");
    assertEquals(BNG_X2, (int) coordinates[0][1][0], "TM75 X-coordinate of second coordinate polygon source not correctly converted to BNG");
    assertEquals(BNG_Y2, (int) coordinates[0][1][1], "TM75 Y-coordinate of second coordinate polygon source not correctly converted to BNG");
    assertEquals(BNG_X, (int) coordinates[0][2][0], "TM75 X-coordinate of third coordinate polygon source not correctly converted to BNG");
    assertEquals(BNG_Y, (int) coordinates[0][2][1], "TM75 Y-coordinate of third coordinate polygon source not correctly converted to BNG");
  }

  @Test
  void testWarning0EPSG() throws IOException, AeriusException, FactoryException {
    final AplFileContent content = readContent("epsg_0_source.upl");

    assertTrue(
        content.getWarnings().stream().filter(e -> e.getReason() == AeriusExceptionReason.ADMS_DEFAULT_EPSG).findAny().isPresent(),
        "Should have warning about using default EPSG");
  }

  private static AplFileContent readContent29903() throws IOException, AeriusException, FactoryException {
    return readContent("epsg_29903_source.upl");
  }

  private static AplFileContent readContent(final String filename) throws IOException, AeriusException, FactoryException {
    final ADMSSourceFileType fileType = ADMSSourceFileType.fileTypeFromExtension(filename);
    final AplFileReader reader = new AplFileReader(fileType);

    try (InputStream is = EPSGTransformerTest.class.getResourceAsStream(filename)) {
      return reader.readObjects(is);
    }
  }

}
