/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import nl.aerius.adms.ADMSTestDataGenerator;
import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSBuilding.BuildingShape;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Unit test, to test specific cases in {@link AplFileWriter}.
 */
class AplFileWriterTest {

  private static final String BUILDING_NAME = "MyBuilding";
  @TempDir
  File tempDir;

  /**
   * Tests
   * null building name => 0
   * "" (empty string => 0
   * "name" => 1
   */
  @ParameterizedTest
  @CsvSource({", 0", "'' , 0", BUILDING_NAME + ", 1"})
  void testSupModelBuildings(final String buildingName, final String expectedOutput) throws IOException, AeriusException {
    final AplFileContent content = new AplFileContent();

    content.setOptions(new ADMSOptions());
    content.setMeteoYear(ADMSTestDataGenerator.TEST_MET_YEAR);
    content.setMetDataFile("");
    content.setAspFile(new File("dummy.asp")); // Set dummy asp filename as the name is not read from the apl/upl file.

    final ADMSBuilding building = new ADMSBuilding();
    building.setShape(BuildingShape.RECTANGULAR);
    building.setBuildingCentre(new Point(100, 100));
    building.setName(BUILDING_NAME);
    content.addBuilding(building);

    final ADMSSource<Point> source = new ADMSSource<Point>();
    source.setGeometry(new Point(200, 200));
    source.setSourceType(SourceType.POINT);
    source.setMainBuilding(buildingName);
    content.addObject(source);
    final File outputFile = new File(tempDir, "noFiles");
    final AplFileWriter writer = new AplFileWriter(ADMSSourceFileType.UPL, System.lineSeparator());
    writer.writeFile(outputFile, content, ADMSGroupKey.NH3);

    final List<String> lines = ADMSTestDataGenerator.readLinesUplFile(outputFile);
    final String buildingsLine = ADMSTestDataGenerator.findKeyValue(lines, "SupModelBuildings");
    assertTrue(buildingsLine.endsWith(expectedOutput) , "Should have correct set of SubModelBuildings, but was: '" + buildingsLine + "'");
    assertTrue(lines.stream().anyMatch(s -> s.contains("BldName")), "Should contains building information");
  }
}
