/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.util.OSUtils;

/**
 * Test class for {@link GeoTiffGridDataFileWriter}.
 */
class GeoTiffGridDataFileWriterTest {

  private static final String BEN_NEVIS_TIF = "ben_nevis.tif";
  private static final String BEN_NEVIS_TER = "ben_nevis.ter";

  @TempDir
  File tempDir;

  @Test
  void testWrite() throws NoSuchAuthorityCodeException, IOException, FactoryException, URISyntaxException {
    final List<String> actual = writeLines(new BBox(216600, 771200, 216700, 771400));
    final List<String> expected = Files.readAllLines(getFileName(BEN_NEVIS_TER));

    assertArrayEquals(expected.toArray(new String[0]), actual.toArray(new String[0]), "File should have expected ter content");
  }

  @ParameterizedTest
  @MethodSource("getBBoxes")
  void testBounderies(final BBox bbox, final int expectedCount) throws NoSuchAuthorityCodeException, IOException, FactoryException, URISyntaxException {
    final List<String> actual = writeLines(bbox);

    assertEquals(expectedCount, actual.size(), "Not the expected number of lines are written.");
  }

  static Object[][] getBBoxes() {
    return new Object[][] {
      // @formatter:off
      //                                        , width * height
      { new BBox(210000, 761200, 226700, 781400), 20 * 20 }, // Box larger than envelop
      { new BBox(216600, 771200, 216700, 771400), 4 * 6 }, // Box inside
      { new BBox(216600, 771480, 216700, 772400), 4 * 13 }, // Box exceeds top
      { new BBox(216600, 770800, 216700, 771200), 4 * 4 }, // Box exceeds bottom
      { new BBox(211600, 771200, 216200, 771400), 6 * 6 }, // Box exceeds left side
      { new BBox(216600, 771200, 217700, 771400), 8 * 6 }, // Box exceeds right side
      { new BBox(216600, 781200, 216700, 781400), 0 }, // Box above top
      { new BBox(216600, 171200, 216700, 171400), 0 }, // Box below bottom
      { new BBox(116600, 771200, 116700, 771400), 0 }, // Box beyond left side
      { new BBox(266600, 771200, 266700, 771400), 0 }, // Box beyond right side
      // @formatter:on
    };
  }

  private List<String> writeLines(final BBox bbox) throws URISyntaxException, IOException, NoSuchAuthorityCodeException, FactoryException {
    final File tifFile = getFileName(BEN_NEVIS_TIF).toFile();
    final GeoTiffGridDataFileWriter writer = GeoTiffLoader.loadRaster("EPSG:27700", tifFile);
    final File tmpFile = new File(tempDir, "test.ter");
    writer.write(tmpFile, bbox, OSUtils.NL);
    final List<String> actual = tmpFile.exists() ? Files.readAllLines(tmpFile.toPath()) : List.of();
    return actual;
  }

  private Path getFileName(final String name) throws URISyntaxException {
    return Path.of(GeoTiffGridDataFileWriterTest.class.getResource(name).toURI());
  }
}
