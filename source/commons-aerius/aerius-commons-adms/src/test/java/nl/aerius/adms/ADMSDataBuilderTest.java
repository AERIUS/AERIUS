/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.adms.backgrounddata.ADMSDataBuilder;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Test class for {@link ADMSDataBuilder}.
 */
@ExtendWith(MockitoExtension.class)
class ADMSDataBuilderTest extends BaseADMSRunnerTest {

  public ADMSDataBuilderTest() {
    super(false);
  }

  @Test
  void testBuild() throws IOException {
    assertTrue(admsData.containsDepositionVelocityMap(ADMS_VERSION), "ADMSData should contain deposition velocities for version " + ADMS_VERSION);
    assertEquals(0.0014, admsData.getDepositionVelocityMap(ADMS_VERSION).get(Substance.NO2).getDepositionVelocityById(1),
        "NO2 deposition velocity should be read from file");
    assertEquals(0.0196, admsData.getDepositionVelocityMap(ADMS_VERSION).get(Substance.NH3).getDepositionVelocityById(1),
        "NH3 deposition velocity should be read from file");

    assertEquals(ADMSConstants.DEFAULT_NO2_DEPOSITION_FACTOR,
        admsData.getDepositionVelocityMap(ADMS_VERSION).get(Substance.NO2).getDepositionVelocityById(-1),
        "Default deposition velocity for NH3 should be returned for non existing receptors");
    assertEquals(ADMSConstants.DEFAULT_NH3_DEPOSITION_FACTOR,
        admsData.getDepositionVelocityMap(ADMS_VERSION).get(Substance.NH3).getDepositionVelocityById(-1),
        "Default deposition velocity for NH3 should be returned for non existing receptors");

    assertTrue(admsData.containsMetFileStore(ADMS_VERSION), "ADMSData should contain met file store for version " + ADMS_VERSION);
  }
}
