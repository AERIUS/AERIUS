/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.spy;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.io.TempDir;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.backgrounddata.ADMSData;
import nl.aerius.adms.backgrounddata.ADMSDataBuilder;
import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.domain.ADMSConfigurationBuilder;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.worker.PropertiesUtil;

/**
 * Base class for tests that run the ADMS tool.
 */
public class BaseADMSRunnerTest {

  private static final Logger LOG = LoggerFactory.getLogger(BaseADMSRunnerTest.class);
  private static final File ADMS_VERSIONS_ROOT = new File("./target/test-classes/nl/aerius/adms/backgrounddata/");

  protected static final ADMSVersion ADMS_VERSION = ADMSVersionMock.mockADMSVersion();
  protected static final String TIME_ZONE = "Europe/London";
  protected static ExecutorService executor;

  // Static boolean to avoid retrying download for every test.
  private static boolean attemptedDownload;

  protected ADMSConfiguration config;
  protected ADMSData admsData;

  private final boolean needADMSInstalled;

  protected @TempDir File tempDir;

  protected BaseADMSRunnerTest(final boolean needADMSInstalled) {
    this.needADMSInstalled = needADMSInstalled;
  }

  @BeforeAll
  static void beforeAll() {
    if (executor == null) {
      executor = Executors.newCachedThreadPool();
    }
  }

  @BeforeEach
  public void setUp() throws Exception {
    config = spy(getADMSConfiguration());
    if (needADMSInstalled) {
      tryDownload(config);
      assumeTrue(checkADMSAvailability(config), "ADMS not installed.");
    }
    // Override the adms root to the location where adms is installed (location from .properties file)
    final File useRoot = config.getADMSRoot(ADMS_VERSION);
    lenient().doReturn(useRoot).when(config).getADMSRoot(any());
    // Override the versions root to the local directory with reduced background data files.
    lenient().doReturn(ADMS_VERSIONS_ROOT).when(config).getVersionsRoot();
    admsData = new ADMSData();
    ADMSDataBuilder.loadAdditionalData(config, ADMS_VERSION, admsData);
  }

  // Synchronized to avoid issues when running in parallel
  private static synchronized void tryDownload(final ADMSConfiguration config) throws Exception {
    if (!attemptedDownload && !checkADMSAvailability(config)) {
      try {
        new ADMSModelDataUtil(new HttpClientProxy(new HttpClientManager()))
            .ensureModelDataPresence(config);
      } catch (final Exception e) {
        LOG.error("Tried to download ADMS for testing purposes, but got an exception", e);
      }
      attemptedDownload = true;
    }
  }

  @AfterAll
  static void afterAll() throws InterruptedException {
    if (executor == null) {
      return;
    }
    executor.shutdown();
    if (!executor.awaitTermination(1, TimeUnit.SECONDS)) {
      LOG.warn("Executor was still running something!");
    } else {
      LOG.debug("Executor clean after shutdown.");
    }
    executor = null;
  }

  protected ADMSConfiguration getADMSConfiguration() throws IOException {
    final Properties properties = PropertiesUtil.getFromTestPropertiesFile("adms");

    properties.setProperty("adms.runfiles.directory", tempDir.getAbsolutePath());
    return new ADMSConfigurationBuilder(properties, null).buildAndValidate().get();
  }

  private static boolean checkADMSAvailability(final ADMSConfiguration config) {
    final File admsRoot = config.getADMSRoot(ADMS_VERSION);
    final boolean available = admsRoot != null && admsRoot.exists();

    if (!available) {
      LOG.warn("Could not find ADMS root directory ({}). {}", admsRoot, Thread.currentThread().getStackTrace()[2]);
    }
    return available;
  }
}
