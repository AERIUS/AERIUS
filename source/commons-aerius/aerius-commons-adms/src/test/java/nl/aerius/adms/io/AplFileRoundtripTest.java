/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.opengis.referencing.FactoryException;

import nl.aerius.adms.ADMSTestDataGenerator;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Test reads AP/UPL files and writes them back and verifies if the content is still the same.
 */
class AplFileRoundtripTest {

  @TempDir
  File tempDir;

  @ParameterizedTest
  @MethodSource("nl.aerius.adms.ADMSTestDataGenerator#referenceFiles")
  void testRoundtrip(final String name) throws IOException, AeriusException, FactoryException {
    final String referenceFile = "../reference/" + name;
    final ADMSSourceFileType fileType = ADMSSourceFileType.fileTypeFromExtension(name);
    final AplFileReader reader = new AplFileReader(fileType);
    final AplFileContent content;

    try (InputStream is = getClass().getResourceAsStream(referenceFile)) {
      content = reader.readObjects(is);
    }
    content.setMetDataFile("");
    content.setAspFile(new File("dummy.asp")); // Set dummy asp filename as the name is not read from the apl/upl file.
    content.getSubstances().sort(Substance::compareTo);
    final int idx = name.indexOf('.');
    final File file = new File(tempDir, name.substring(0, idx));
    final AplFileWriter writer = new AplFileWriter(fileType, OSUtils.NL);
    writer.writeFile(file, content, ADMSTestDataGenerator.getADMSGroupKey(content.getSubstances()));

    final String output = ADMSTestDataGenerator.normalize(Files.readAllBytes(fileType.fileWithExtension(file).toPath()));
    final String reference = ADMSTestDataGenerator.normalize(getClass().getResourceAsStream(referenceFile).readAllBytes());

    assertEquals(reference, output, "Read and written of file '" + name + "' should be the same");
  }
}
