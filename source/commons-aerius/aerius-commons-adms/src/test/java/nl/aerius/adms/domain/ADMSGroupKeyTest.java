/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Test class for {@link ADMSGroupKey}.
 */
class ADMSGroupKeyTest {

  @Test
  void testSubstancesRoadSource() {
    assertEquals(List.of(Substance.NH3), ADMSGroupKey.NH3.getEmissionSubstances(false), "When no road source just return NH3");
    assertEquals(List.of(Substance.NH3, Substance.NOX), ADMSGroupKey.NH3.getEmissionSubstances(true),
        "When road source is should return both NH3 and NOx");
  }

}
