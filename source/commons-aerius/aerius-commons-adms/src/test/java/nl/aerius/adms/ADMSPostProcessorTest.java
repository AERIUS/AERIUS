/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static nl.aerius.adms.domain.ADMSGroupKey.NH3;
import static nl.aerius.adms.domain.ADMSGroupKey.NOX_OTHER;
import static nl.aerius.adms.domain.ADMSGroupKey.NOX_ROAD;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.aerius.adms.conversion.ADMSPostProcessor;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSPointNO2Data;
import nl.aerius.adms.domain.NCACalculationResultPoint;
import nl.aerius.adms.util.ADMSUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * Test class for {@link ADMSPostProcessor}.
 */
class ADMSPostProcessorTest {

  private static final int RECEPTOR_ID = 1000;

  /**
   * Test if aggregation is correctly called.
   */
  @Test
  void testPostProcessAggregation() {
    final NCACalculationResultPoint receptor = new NCACalculationResultPoint(RECEPTOR_ID, null, AeriusPointType.RECEPTOR, 1.0, 1.0);
    final NCACalculationResultPoint point1 = new NCACalculationResultPoint(1, RECEPTOR_ID, AeriusPointType.SUB_RECEPTOR, 1.0, 1.0);
    final NCACalculationResultPoint point2 = new NCACalculationResultPoint(2, RECEPTOR_ID, AeriusPointType.SUB_RECEPTOR, 2.0, 2.0);
    final EmissionResultKey concentration = EmissionResultKey.NH3_CONCENTRATION;
    final Substance substance = Substance.NH3;
    // Add enriched deposition velocity to receptor itself.
    receptor.putDepositionVelocity(substance, 0.7);
    final List<NCACalculationResultPoint> results = ADMSPostProcessor.postprocess(NH3,
        Set.of(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION),
        List.of(receptor, createResultPoint(point1, concentration, substance), createResultPoint(point2, concentration, substance)));
    assertEquals(3, results.size(), "Not the expected number of results.");

    final AeriusResultPoint resultPoint = results.get(0);
    assertEquals(10.0, resultPoint.getEmissionResult(concentration), 1E-5, "Not the expected concentration for:" + concentration);
    final EmissionResultKey depositionKey = EmissionResultKey.safeValueOf(substance, EmissionResultType.DEPOSITION);
    assertEquals(1820.0, resultPoint.getEmissionResult(depositionKey), 1E-5, "Not the expected deposition for: " + depositionKey);
  }

  /**
   * Tests various {@link ADMSGroupKey} / {@link EmissionResultKey} combinations.
   *
   * @param groupKey group key to test
   * @param erk emission result key to check the computed result
   * @param concentration expected concentration result
   * @param deposition expected deposition result
   */
  @ParameterizedTest
  @MethodSource("data")
  void testPostProcess(final ADMSGroupKey groupKey, final EmissionResultKey erk, final double concentation, final double deposition) {
    final EmissionResultKey concentration = EmissionResultKey.safeValueOf(erk.getSubstance(), EmissionResultType.CONCENTRATION);
    final Substance substance = ADMSUtil.toConcentrationSubstance(erk);
    final EmissionResultKey erkConcentration = EmissionResultKey.safeValueOf(substance, EmissionResultType.CONCENTRATION);

    final List<NCACalculationResultPoint> results = ADMSPostProcessor.postprocess(groupKey,
        erkConcentration == erk ? Set.of(erk) : Set.of(erkConcentration, erk), List.of(createResultPoint(concentration, substance)));
    final AeriusResultPoint resultPoint = results.get(0);

    assertEquals(10.0, resultPoint.getEmissionResult(concentration), 1E-5, "Not the expected concentration for:" + erkConcentration);
    assertEquals(concentation, resultPoint.getEmissionResult(erkConcentration), 1E-5,
        "Not the expected calculated concentration for:" + erkConcentration);
    final EmissionResultKey depositionKey = EmissionResultKey.safeValueOf(erk.getSubstance(), EmissionResultType.DEPOSITION);
    assertEquals(deposition, resultPoint.getEmissionResult(depositionKey), 1E-5, "Not the expected deposition for: " + depositionKey);
  }

  private static List<Arguments> data() {
    return List.of(
        Arguments.of(NH3, EmissionResultKey.NH3_CONCENTRATION, 10.0, 0.0),
        Arguments.of(NH3, EmissionResultKey.NH3_DEPOSITION, 10.0, 1820.0),
        Arguments.of(NOX_OTHER, EmissionResultKey.NOX_DEPOSITION, 7.0, 469.91),
        Arguments.of(NOX_ROAD, EmissionResultKey.NOX_DEPOSITION, 9.374601471062533, 629.3169967524278));
  }

  private static NCACalculationResultPoint createResultPoint(final EmissionResultKey key, final Substance substance) {
    final NCACalculationResultPoint point = new NCACalculationResultPoint(RECEPTOR_ID, null, AeriusPointType.RECEPTOR, 1.0, 1.0);

    return createResultPoint(point, key, substance);
  }

  private static NCACalculationResultPoint createResultPoint(final NCACalculationResultPoint point, final EmissionResultKey key,
      final Substance substance) {
    point.setEmissionResult(key, 10.0);
    point.setNO2BackgroundData(new ADMSPointNO2Data(1000.0, 2000.0, 3000.0, 4000.0, 0.5, 0.6));
    point.putDepositionVelocity(substance, 0.7);
    return point;
  }
}
