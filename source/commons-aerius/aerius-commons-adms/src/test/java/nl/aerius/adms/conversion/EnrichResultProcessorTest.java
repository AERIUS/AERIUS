/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import static nl.aerius.adms.domain.ADMSGroupKey.NH3;
import static nl.aerius.adms.domain.ADMSGroupKey.NOX_OTHER;
import static nl.aerius.adms.domain.ADMSGroupKey.NOX_ROAD;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NH3_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NH3_DEPOSITION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NO2_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_DEPOSITION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.lenient;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.adms.ADMSTestDataGenerator;
import nl.aerius.adms.backgrounddata.ADMSData;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData.LACodeBackgroundConcentration;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.domain.NCACalculationResultPoint;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.backgrounddata.DepositionVelocityMap;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;

/**
 * Test class for {@link EnrichResultProcessor}.
 */
@ExtendWith(MockitoExtension.class)
class EnrichResultProcessorTest {

  private static final int RECEPTOR_ID = 1000;
  private static final Double DV_NH3_RECEPTOR_ID = 5.5;
  private static final Double DV_NO2_RECEPTOR_ID = 4.4;

  private @Mock NOxToNO2CalculatorData noxToNO2CalculatorData;

  /**
   * Test if data is enriched correctly for different {@link ADMSGroupKey} and {@link EmissionResultKey} values.
   */
  @ParameterizedTest
  @MethodSource("data")
  void testEnrichResults(final ADMSGroupKey groupKey, final EmissionResultKey erk) {
    final ADMSData admsData = createADMSData();
    final EnrichResultProcessor processor = new EnrichResultProcessor(admsData);
    final ADMSInputData input = ADMSTestDataGenerator.newADMSInputData(CommandType.CALCULATE);
    input.setAdmsVersion(ADMSVersion.NCA_LATEST);
    input.setEmissionResultKeys(EnumSet.of(erk));
    final List<NCACalculationResultPoint> results = new ArrayList<>();
    results.add(createResultPoint(erk));
    input.setReceptors((results.stream().map(r -> new ADMSCalculationPoint(r, AeriusPointType.RECEPTOR)).toList()));
    processor.enrichResults(input, groupKey, results);
    final NCACalculationResultPoint resultPoint = results.get(0);

    // Assert if NO2 background set.
    if (groupKey == NOX_ROAD) {
      assertNotNull(resultPoint.getNO2BackgroundData(), "Should have NO2 background data");
    } else {
      assertNull(resultPoint.getNO2BackgroundData(), "Should not have NO2 background data");
    }
    // Assert if deposition velocity added.
    final Substance substance = erk.getSubstance() == Substance.NOX ? Substance.NO2 : erk.getSubstance();
    if (erk.getEmissionResultType() == EmissionResultType.DEPOSITION) {
      final double dv = substance == Substance.NH3 ? DV_NH3_RECEPTOR_ID : DV_NO2_RECEPTOR_ID;
      assertEquals(dv,  resultPoint.getDepositionVelocity(substance), "Should have expected deposition velocity");
    } else {
      assertEquals(0.0, resultPoint.getDepositionVelocity(substance), 0.01, "Should not have a deposition velocity");
    }
  }

  private ADMSData createADMSData() {
    final ADMSData data = new ADMSData();
    // Add noxToNO2CalculatorData data
    data.addNOxToNO2CalculatorData(ADMSVersion.NCA_LATEST, noxToNO2CalculatorData);
    final LACodeBackgroundConcentration bc = new LACodeBackgroundConcentration();
    lenient().doReturn(bc).when(noxToNO2CalculatorData).getBackgroundConcentration(anyInt(), any());
    // Add deposition velocity data
    final DepositionVelocityMap dvmNH3 = new DepositionVelocityMap(0, null, null);
    dvmNH3.put(RECEPTOR_ID, DV_NH3_RECEPTOR_ID);
    final DepositionVelocityMap dvmNO2 = new DepositionVelocityMap(0, null, null);
    dvmNO2.put(RECEPTOR_ID, DV_NO2_RECEPTOR_ID);
    data.addDepositionVelocityMap(ADMSVersion.NCA_LATEST, Map.of(Substance.NH3, dvmNH3, Substance.NO2, dvmNO2));
    return data;
  }

  private static List<Arguments> data() {
    return List.of(
        Arguments.of(NH3, NH3_CONCENTRATION),
        Arguments.of(NH3, NH3_DEPOSITION),
        Arguments.of(NOX_OTHER, NO2_CONCENTRATION),
        Arguments.of(NOX_OTHER, NOX_DEPOSITION),
        Arguments.of(NOX_ROAD, NO2_CONCENTRATION),
        Arguments.of(NOX_ROAD, NOX_DEPOSITION)
        );
  }

  private static NCACalculationResultPoint createResultPoint(final EmissionResultKey key) {
    final NCACalculationResultPoint point = new NCACalculationResultPoint(RECEPTOR_ID, null, AeriusPointType.RECEPTOR, 1.0, 1.0);

    point.setEmissionResult(key, 10.0);
    return point;
  }
}
