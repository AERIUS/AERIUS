/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import nl.aerius.adms.ADMSTestDataGenerator;
import nl.aerius.adms.conversion.ADMS2EmissionSourceConverter;
import nl.aerius.adms.conversion.EmissionSource2ADMSConverter;
import nl.aerius.adms.domain.ADMSSource;
import nl.aerius.adms.domain.ADMSTimeVaryingProfile;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Roundtrip test for {@link FacFileReader} and {@link FacFileWriter}.
 */
class FacFileWriterTest {

  @TempDir
  File tempDir;

  @ParameterizedTest
  @ValueSource(strings = {"tvp_3days.fac", "tvp_monthly.fac", "tvp_both.fac"})
  void testRoundTrip(final String facName) throws IOException, AeriusException {
    final List<ADMSSource<?>> sources = ADMSTestDataGenerator.createRoadSources();
    // Read the fac file
    final FacFileReader reader = new FacFileReader();

    assertFACReader(reader, facName, sources);
    final Collection<ADMSTimeVaryingProfile> timeVaryingProfiles = EmissionSource2ADMSConverter
        .convertCustomTimeVaryingProfiles(ADMS2EmissionSourceConverter.convert(reader.getProfiles()));
    // Write the fac File
    final FacFileWriter writer = new FacFileWriter(new File(tempDir, facName), OSUtils.NL);

    assertTrue(writer.write(sources, timeVaryingProfiles), "Should have written fac file.");
    final String actualTimeVaryingProfileOutput = Files.readString(writer.getOutputFile().toPath());
    // Read reference file
    final String referenceTimeVaryingProfileOutput;
    try (final InputStream is = getClass().getResourceAsStream(facName)) {
      referenceTimeVaryingProfileOutput = new String(is.readAllBytes(), StandardCharsets.UTF_8);
    }
    assertEquals(referenceTimeVaryingProfileOutput, actualTimeVaryingProfileOutput,
        "Expected time-varying profile output to be the same");
  }

  private static void assertFACReader(final FacFileReader reader, final String facName, final List<ADMSSource<?>> sources) throws IOException {
    try (final InputStream is = FacFileWriterTest.class.getResourceAsStream(facName)) {

      final LineReaderResult<Void> results = reader.readObjects(is, sources);
      final Object[] exceptions = results.getExceptions().toArray();
      assertArrayEquals(new Object[0], exceptions, "Should have no exceptions: " + Arrays.toString(exceptions));
    }
  }

  @Test
  void testNoSourcesUsingProfile() throws IOException {
    final ADMSSource<?> source = ADMSTestDataGenerator.simplePointSource(1, 1);
    final ADMSTimeVaryingProfile timeVaryingProfile = new ADMSTimeVaryingProfile();
    timeVaryingProfile.setType(CustomTimeVaryingProfileType.MONTHLY);
    // Write the fac File
    final FacFileWriter writer = new FacFileWriter(new File(tempDir, "tmp"), OSUtils.NL);

    assertFalse(writer.write(List.of(source), List.of(timeVaryingProfile)), "When sources are not linked it shouldn't write the fac file");
    assertFalse(writer.getOutputFile().exists(), "Fac file should not exist when no sources using time-varying profile");
  }
}
