/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;

import nl.aerius.adms.domain.NCACalculationResultPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Test class for {@link ADMSSubReceptorAggregator}.
 *
 * While most testing is done on the base class AbstractSubReceptorAggregator, these tests validate ADMS specific behaviour:
 * Not supplying the original receptor in results and adding aggregated results to original list of (subreceptor) results.
 */
class ADMSSubReceptorAggregatorTest {

  @Test
  void testAggregation() {
    final NCACalculationResultPoint center = createADMSReceptor(0, 0, 0, AeriusPointType.RECEPTOR);
    final Collection<NCACalculationResultPoint> receptors = createPoints(center);
    final Set<EmissionResultKey> erks = EnumSet.of(EmissionResultKey.NOX_CONCENTRATION);
    final Collection<NCACalculationResultPoint> allReceptors = new ArrayList<>(receptors);
    allReceptors.add(center);
    final ADMSSubReceptorAggregator aggregator = new ADMSSubReceptorAggregator(allReceptors, erks);
    final List<NCACalculationResultPoint> originalResults = createResults(receptors);
    assertEquals(10, originalResults.size(), "originalResults list should now contain 10 results");
    final List<NCACalculationResultPoint> aggregate = aggregator.aggregate(originalResults);

    assertEquals(11, aggregate.size(),
        "aggregate list should now contain both the original supplied set (10x) and the aggregated result");
    final Optional<NCACalculationResultPoint> receptor = aggregate.stream().filter(p -> p.getPointType() == AeriusPointType.RECEPTOR).findAny();

    assertTrue(receptor.isPresent(), "Results should contain the receptor again");
    assertEquals(5.5, receptor.get().getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1, "Should have aggregated concentration");

    aggregate.stream().filter(p -> p.getPointType().isSubReceptor())
        .forEach(x -> assertEquals(x.getId(), x.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1,
            "Should have original concentration"));
  }

  private Collection<NCACalculationResultPoint> createPoints(final NCACalculationResultPoint center) {
    final List<NCACalculationResultPoint> points = new ArrayList<>();

    for (int i = 0; i < 10; i++) {
      final NCACalculationResultPoint rp = createADMSReceptor(i + 1, i, i, AeriusPointType.SUB_RECEPTOR);
      rp.setParentId(center.getId());
      points.add(rp);
    }
    return points;
  }

  private List<NCACalculationResultPoint> createResults(final Collection<NCACalculationResultPoint> receptors) {
    return receptors.stream().map(p -> {
      final NCACalculationResultPoint rp = new NCACalculationResultPoint(p);
      rp.setEmissionResult(EmissionResultKey.NOX_CONCENTRATION, Double.valueOf(p.getId()));
      return rp;
    }).toList();
  }

  private NCACalculationResultPoint createADMSReceptor(final int id, final double x, final double y, final AeriusPointType type) {
    return new NCACalculationResultPoint(new AeriusPoint(id, null, type, x, y));
  }
}
