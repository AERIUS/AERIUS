/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.opengis.referencing.FactoryException;

import nl.aerius.adms.domain.ADMSInputData;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Util class to read the content of a ADMS project file into an {@link ADMSInputData} object.
 */
public final class ADMSInputReaderUtil {

  private ADMSInputReaderUtil() {
    // Util class
  }

  public static void read2ADMSInputData(final String filename, final int key, final ADMSInputData input)
      throws IOException, AeriusException, FactoryException {
    final AplFileReader reader = new AplFileReader(ADMSSourceFileType.UPL);

    // Read file
    try (final InputStream is = ADMSInputReaderUtil.class.getResourceAsStream(filename)) {
      final AplFileContent content = reader.readObjects(is);

      input.getEmissionSources().computeIfAbsent(key, k -> new ArrayList<>()).addAll(content.getObjects());
      input.getEmissionSources().get(key).addAll(content.getBuildings());
    }
  }
}
