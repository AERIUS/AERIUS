/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.aerius.adms.ADMSTestDataGenerator;
import nl.aerius.adms.backgrounddata.MetDataFileKey;

class MetFileLoaderTest {

  @Test
  void testLoadMetFiles() throws URISyntaxException {
    final File expectedFile = new File(MetFileLoaderTest.class.getResource(ADMSTestDataGenerator.TEST_MET_SITE_ID + "_obs-raw-gt-90pct_2017_oneday.met").toURI());
    final File testFolder = expectedFile.getParentFile();
    final Map<MetDataFileKey, File> mappedFiles = MetFileLoader.loadMetFiles(testFolder);
    assertEquals(1, mappedFiles.size(), "Number of met files");
    assertTrue(mappedFiles.containsKey(ADMSTestDataGenerator.TEST_MET_DATA_FILE_KEY), "Met site ID found");
    assertEquals(expectedFile, mappedFiles.get(ADMSTestDataGenerator.TEST_MET_DATA_FILE_KEY), "Corresponding file");
  }

  @Test
  void testLoadMetFilesUnavailableFolder() {
    assertThrows(IllegalArgumentException.class, () -> MetFileLoader.loadMetFiles(null),
        "Should throw an exception if folder isn't supplied");

    final File expectedFile = new File("SomeTotallyUnexpectedFolder/ourfile.met");
    final File testFolder = expectedFile.getParentFile();
    assertThrows(IllegalArgumentException.class, () -> MetFileLoader.loadMetFiles(testFolder),
        "Should throw an exception if folder doesn't exist");
  }
}
