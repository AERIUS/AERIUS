/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion.nox2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.adms.ADMSVersionMock;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class testing if data is correctly read into the {@link NOxToNO2CalculatorData} object.
 */
@ExtendWith(MockitoExtension.class)
class NOxToNO2CalculatorDataTest {

  private NOxToNO2CalculatorData backgroundData;

  @BeforeEach
  void beforeEach() throws IOException, URISyntaxException {
    final File dataRootDirectory = new File(Path.of(getClass().getResource("../../backgrounddata").toURI()).toFile(),
        ADMSVersionMock.mockADMSVersion().getNOxToNO2Version().getDataDirectory());

    backgroundData = NOxToNO2CalculatorDataReader.loadData(ADMSVersionMock.mockNOxToNO2Version(), dataRootDirectory);
  }

  @Test
  void testShapeReader() {
    assertEquals(5.946, backgroundData.getBackgroundConcentration(2029, new Point(146673, 528443)).getNOx(), 0.1,
        "Should return NOx for Belfast 2029");
  }

  @Test
  void testBackgroundConcentrationReader() {
    assertEquals(5.946, backgroundData.getBackgroundConcentration(2029, 450L).getNOx(), 0.1, "Should return NOx for Belfast 2029");
  }

  @Test
  void testFractionShapeReaderOther() {
    assertEquals(0.264036302545494, backgroundData.getFractionNO2(2029, new Point(146673, 528443)).local(), 0.1,
        "Should return fNO2 for All non-urban UK traffic 2029");
  }

  @Test
  void testFractionShapeReaderNonUrban() throws AeriusException {
    assertEquals("All non-urban UK traffic", backgroundData.getFractionNO2Area(new Point(458399.12,453219.48)),
        "Should return fNO2 for All non-urban UK traffic 2029");
  }

  @Test
  void testFractionShapeReader() {
    assertEquals(0.222122838699971, backgroundData.getFractionNO2(2029, new Point(533703, 180337)).local(), 0.1,
        "Should return fNO2 for All London traffic 2029");
  }

  @Test
  void testFractionReader() {
    assertEquals(0.222122838699971, backgroundData.getFractionNO2(2029, "All London traffic").local(), 0.1,
        "Should return fNO2 for All London traffic 2029");
  }
}
