/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NO2_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_DEPOSITION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.opengis.referencing.FactoryException;

import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test to perform an ADMS run.
 */
class ADMSProcessRunnerTest extends BaseADMSRunnerTest {

  private static final Base64.Decoder BASE64_DECODER = Base64.getDecoder();

  private static final ADMSGroupKey GROUP_KEY = ADMSGroupKey.NOX_ROAD;
  private static final int LEVEL = 4;

  public ADMSProcessRunnerTest() {
    super(true);
  }

  @Test
  void testFailureRun() {
    final AeriusException thrownException = assertThrows(AeriusException.class, () -> {
      final ADMSProcessRunner runner = new ADMSProcessRunner(executor, getADMSConfiguration(), admsData, ADMS_VERSION);
      final ADMSInputData input = ADMSTestDataGenerator.createInput(GROUP_KEY);

      ensureLicense(config, input);

      runner.run(input, null, null);
      fail("ADMS run should not run succesfull, but throw an exception.");
    }, "Expected an exception because it should give an error because no receptors were available.");

    assertEquals(AeriusExceptionReason.INTERNAL_ERROR, thrownException.getReason(), "Expected reason");
  }

  @Test
  void testOutsideGeoExtent() {
    final AeriusException thrownException = assertThrows(AeriusException.class, () -> {
      final ADMSConfiguration admsConfig = getADMSConfiguration();
      final ADMSProcessRunner runner = new ADMSProcessRunner(executor, admsConfig, admsData, ADMS_VERSION);
      final ADMSInputData input = createInput(ADMSGroupKey.NH3);
      input.getAdmsOptions().setSpatiallyVaryingRoughness(true);

      ensureLicense(config, input);

      runner.run(input, null, null);

      fail("ADMS run should not run successful, but throw an exception.");
    }, "Expected an exception because it should give an error because no receptors were available.");

    assertEquals(AeriusExceptionReason.ADMS_GEO_DATA_OUTSIDE_EXTENT, thrownException.getReason(), "Expected reason");
  }

  @Test
  void testRunWithNO2Emission() throws Exception {
    final ADMSInputData input = createInput(GROUP_KEY);
    final AeriusResultPoint rp = assertRun(GROUP_KEY, input, 1).get(0);

    assertEquals(rp.getEmissionResult(NOX_CONCENTRATION) * 0.7, rp.getEmissionResult(NO2_CONCENTRATION), 0.01,
        "Result NO2 should be larger than derived NO2 from NOx because it should be sum calculated NO2 and derived NOx");
  }

  @Test
  void testRunWithoutNO2Emission() throws Exception {
    final ADMSInputData input = createInput(GROUP_KEY);
    final AeriusResultPoint rp = assertRun(GROUP_KEY, input, 1).get(0);
    assertEquals(rp.getEmissionResult(NOX_CONCENTRATION) * 0.7, rp.getEmissionResult(NO2_CONCENTRATION), 1E-12, "Should have NOX derived NO2 result");
  }

  @Test
  void testRunWithSubreceptors() throws Exception {
    final ADMSInputData input = createInput(GROUP_KEY);
    ADMSTestDataGenerator.setSubReceptors(input, LEVEL);
    final Optional<AeriusResultPoint> rp = assertRun(GROUP_KEY, input, 385).stream().filter(p -> p.hasEmissionResult(NOX_DEPOSITION)).findAny();

    assertTrue(rp.isPresent(), "Should have a receptor point with deposition results");
    assertNotEquals(0.0, rp.get().getEmissionResult(NOX_DEPOSITION), 1.0E-8, "Should have depostion NOX");
    assertNotEquals(0.0, rp.get().getEmissionResult(NO2_CONCENTRATION), 1.0E-8, "Should have concentration NO2");
  }

  @Test
  void testRunWithSubreceptorsAndNoDeposition() throws Exception {
    final ADMSInputData input = createInput(GROUP_KEY);
    ADMSTestDataGenerator.setSubReceptors(input, LEVEL);
    input.getEmissionResultKeys().remove(EmissionResultKey.NOX_DEPOSITION);
    final List<AeriusResultPoint> results = assertRun(GROUP_KEY, input, 385);

    assertFalse(results.stream().anyMatch(p -> p.hasEmissionResult(NOX_DEPOSITION)), "Should not have a receptor point with deposition results");
    assertEquals(385, results.stream().filter(p -> p.hasEmissionResult(NOX_CONCENTRATION)).count(), "Should have concentration NOX");
    assertEquals(385, results.stream().filter(p -> p.hasEmissionResult(NO2_CONCENTRATION)).count(), "Should have concentration NO2");
  }

  private static ADMSInputData createInput(final ADMSGroupKey groupKey) throws IOException, URISyntaxException, AeriusException, FactoryException {
    final ADMSInputData input = ADMSTestDataGenerator.createInput(groupKey);

    ADMSTestDataGenerator.setReceptors(input);
    return input;
  }

  private List<AeriusResultPoint> assertRun(final ADMSGroupKey groupKey, final ADMSInputData input, final int expectedResults) throws Exception {
    final ADMSConfiguration config = getADMSConfiguration();
    final ADMSProcessRunner runner = new ADMSProcessRunner(executor, config, admsData, ADMS_VERSION);
    ensureLicense(config, input);

    final CalculationResult result = runner.run(input, null, null);
    final List<AeriusResultPoint> list = result.getResults().get(groupKey.getIndex());
    assertEquals(expectedResults, list.size(), "Should have " + expectedResults + " result(s): " + list);
    final AeriusResultPoint rp = list.get(0);
    assertNotEquals(0.0, rp.getEmissionResult(NOX_CONCENTRATION), 1.0E-10, "Should have result for NOx concentration");
    assertNotEquals(0.0, rp.getEmissionResult(NO2_CONCENTRATION), 1.0E-10, "Should have result for NO2 concentration");
    return list;
  }

  private static void ensureLicense(final ADMSConfiguration config, final ADMSInputData input) {
    if (config.getAdmsLicenseBase64().isPresent()) {
      input.setLicense(BASE64_DECODER.decode(config.getAdmsLicenseBase64().get()));
    }
  }
}
