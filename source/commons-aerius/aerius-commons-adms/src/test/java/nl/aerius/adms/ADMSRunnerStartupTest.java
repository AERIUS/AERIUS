/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import nl.aerius.adms.exception.ADMSInvalidLicense;
import nl.aerius.adms.exception.ADMSInvalidVersionException;

/**
 * Test class for {@link ADMSExecutor} to test if ADMS errors are detected.
 */
class ADMSRunnerStartupTest extends BaseADMSRunnerTest {

  private static final Base64.Decoder BASE64_DECODER = Base64.getDecoder();

  public ADMSRunnerStartupTest() {
    super(true);
  }

  /**
   * Should run without exceptions being thrown.
   */
  @Test
  void testValidInstallation() {
    Assertions.assertDoesNotThrow(() -> new ADMSProcessRunner(executor, getADMSConfiguration(), admsData, ADMS_VERSION),
        "ADMS startup should not throw an exception that ADMS could not be found.");
  }

  /**
   * Runs with license and executable copied to target directory.
   */
  @Test
  void testValidPassedLicense() throws Exception {
    final File preinstalledAdmsRoot = new File(getADMSConfiguration().getVersionsRoot(), ADMS_VERSION.getVersion());

    // Runs

    final byte[] license;
    if (config.getAdmsLicenseBase64().isPresent()) {
      license = BASE64_DECODER.decode(config.getAdmsLicenseBase64().get());
    } else {
      final File file = new File(config.getADMSRoot(ADMS_VERSION), ADMSExecutor.ADMS_URBAN_LIC_NAME);
      license = Files.readAllBytes(file.toPath());
    }
    final File targetDirectory = run(new ADMSExecutor(ADMS_VERSION, preinstalledAdmsRoot), license);

    assertNotEquals(config.getADMSRoot(ADMS_VERSION), targetDirectory, "Target directory should not be the same as configured directory.");
    assertFalse(new File(targetDirectory, ADMSExecutor.ADMS_URBAN_LIC_NAME).exists(), "License file be removed.");
  }

  /**
   * Should thrown an error that the wrong ADMS version is used to run.
   */
  @Test
  @Disabled("Disabled until an additional ADMS version is supported and this unit test becomes relevant again.")
  void testInvalidADMSVersion() throws IOException {
    final File preinstalledAdmsRoot = new File(getADMSConfiguration().getVersionsRoot(), ADMS_VERSION.getVersion());

    Assertions.assertThrows(ADMSInvalidVersionException.class, () -> run(new ADMSExecutor(ADMS_VERSION, preinstalledAdmsRoot), null));
  }

  /**
   * Pass an empty license to use. This should trigger an invalid license error.
   */
  @Test
  void testInvalidLicense() throws IOException {
    // Reset versionsRoot for this specific test.
    final File preinstalledAdmsRoot = new File(getADMSConfiguration().getVersionsRoot(), ADMS_VERSION.getVersion());

    assertThrows(ADMSInvalidLicense.class, () -> run(new ADMSExecutor(ADMS_VERSION, preinstalledAdmsRoot), new byte[0]),
        "Should throw an exception the license is invalid.");
  }

  private File run(final ADMSExecutor runner, final byte[] license) throws Exception {
    return runner.run(tempDir, List.of(), license, null, null);
  }
}
