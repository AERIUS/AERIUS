/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.aerius.adms.conversion.EmissionSource2ADMSConverter;
import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.io.ADMSSourceFileType;
import nl.aerius.adms.io.AplExportWriter;
import nl.aerius.adms.io.AplImportReader;
import nl.aerius.adms.util.ADMSEngineSources;
import nl.aerius.adms.util.BoundingBoxUtil;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geo.EPSG;
import nl.overheid.aerius.util.OSUtils;

/**
 * Round trip test for reading ADMS input, convert to AERIUS internal. Than convert back to ADMS input and write back to file.
 */
class ADMSConverterRoundtripTest extends BaseADMSRunnerTest {

  private static final int YEAR = 2030;

  private AplImportReader reader;

  public ADMSConverterRoundtripTest() {
    super(false);
  }

  @BeforeEach
  void beforeEach() throws Exception {
    super.setUp();
    reader = new AplImportReader();
  }

  @ParameterizedTest
  @MethodSource("nl.aerius.adms.ADMSTestDataGenerator#referenceFiles")
  void testWrite(final String name) throws IOException, AeriusException {
    final String referenceFile = "reference/" + name;
    // Read the reference file
    final ImportParcel importParcel = readReferenceFile(referenceFile);
    final ADMSGroupKey sourcesKey = getADMSGroupKey(importParcel.getSituation().getEmissionSourcesList());
    // Write the reference file data back to a new file
    final ADMSInputData content = convert2ADMS(importParcel, sourcesKey);
    content.setFileType(ADMSSourceFileType.fileTypeFromExtension(name));
    final int idx = name.lastIndexOf('.');
    final File tmpFile = new File(tempDir, name.substring(0, idx));

    final List<EngineSource> engineSources = content.getEmissionSources().get(sourcesKey.getIndex()).stream().toList();
    final ADMSEngineSources sources = ADMSEngineSources.builder().set(engineSources).build();
    final BBox extent = BoundingBoxUtil.getExtendedBoundingBox(engineSources, content.getCalculateReceptors().stream().map(AeriusPoint.class::cast).toList());

    new AplExportWriter(admsData, ADMSVersion.NCA_LATEST, OSUtils.NL).write(tmpFile, content, sourcesKey, sources,
        content.getCalculateReceptors(), extent);

    final String output = ADMSTestDataGenerator.normalize(Files.readAllBytes(content.getFileType().fileWithExtension(tmpFile).toPath()));
    final String reference = ADMSTestDataGenerator.normalize(getClass().getResourceAsStream(referenceFile).readAllBytes());
    assertEquals(reference, output, "Read and written file should be the same");
  }

  private ADMSGroupKey getADMSGroupKey(final List<EmissionSourceFeature> sources) {
    return ADMSTestDataGenerator
        .getADMSGroupKey(sources.get(0).getProperties().getEmissions().keySet().stream().sorted().collect(Collectors.toList()));
  }

  private ImportParcel readReferenceFile(final String referenceFile) throws IOException, AeriusException {
    final ImportParcel importParcel = new ImportParcel();
    try (InputStream is = getClass().getResourceAsStream(referenceFile)) {
      reader.read(referenceFile, is, null, null, importParcel);
    }
    return importParcel;
  }

  private ADMSInputData convert2ADMS(final ImportParcel importParcel, final ADMSGroupKey sourcesKey) throws AeriusException {
    final List<Substance> substances = sourcesKey.getEmissionSubstances(true);
    final ADMSInputData inputData = new ADMSInputData(ADMSTestDataGenerator.DEFAULT_ENGINE_DATA_KEY, CommandType.CALCULATE);

    inputData.setAdmsOptions(new ADMSOptions());
    inputData.getAdmsOptions().setMetSiteId(ADMSTestDataGenerator.TEST_MET_SITE_ID);
    inputData.getAdmsOptions().setMetDatasetType(ADMSTestDataGenerator.TEST_MET_DATASET_TYPE);
    inputData.getAdmsOptions().setMetYears(List.of(ADMSTestDataGenerator.TEST_MET_YEAR));
    inputData.setYear(importParcel.getSituation().getYear());
    inputData.setMeteoYear(ADMSTestDataGenerator.TEST_MET_YEAR);
    inputData.setSubstances(substances);
    inputData.setTimeZone(TIME_ZONE);
    // Convert Emission Sources
    final EmissionSource2ADMSConverter converter = new EmissionSource2ADMSConverter(EPSG.BNG.getSrid(), YEAR, new RoadEmissionCategories());

    final List<ADMSBuilding> buildings = new ArrayList<>();
    for (final BuildingFeature building : importParcel.getSituation().getBuildingsList()) {
      buildings.add(EmissionSource2ADMSConverter.convert(building));
    }

    inputData.setEmissionSources(sourcesKey.getIndex(),
        Stream.concat(importParcel.getSituation().getEmissionSourcesList().stream()
            .flatMap(esf -> {
              try {
                return converter.convert(esf.getProperties(), esf.getGeometry(), substances).stream();
              } catch (final AeriusException e) {
                throw new IllegalArgumentException(e);
              }
            }), buildings.stream()).toList());
    // Convert Buildings
    return inputData;
  }
}
