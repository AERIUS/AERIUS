/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.domain.ADMSRoadSource;
import nl.aerius.adms.io.ADMSInputReaderUtil;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.shared.domain.result.ExportResult;

/**
 * Test class for {@link ADMSExportRunner}.
 */
@ExtendWith(MockitoExtension.class)
class ADMSExportRunnerTest extends BaseADMSRunnerTest {

  private static final String FILE_SERVER_EXTERNAL_URL_BASEEXTERNAL = "http://localhost/external";
  private static final String FILE_CODE = "1234";

  private @Captor ArgumentCaptor<File> zipfileCapture;

  public ADMSExportRunnerTest() {
    super(false);
  }

  @Test
  void testExportADMS() throws Exception {
    config.setKeepGeneratedFiles(true);
    final FileServerProxy fileServerProxy = mock(FileServerProxy.class);
    doReturn(FILE_SERVER_EXTERNAL_URL_BASEEXTERNAL).when(fileServerProxy).processFile(eq(FILE_CODE), any(), zipfileCapture.capture(), any());
    final ADMSExportRunner exporter = new ADMSExportRunner(executor, config, admsData, fileServerProxy);
    final ADMSInputData input = ADMSTestDataGenerator.createInput(ADMSGroupKey.NOX_OTHER);
    // Add a second data set, that should result in an additional set of input files.
    ADMSInputReaderUtil.read2ADMSInputData("../reference/road_transport.upl", ADMSGroupKey.NOX_ROAD.getIndex(), input);
    ADMSTestDataGenerator.setReceptors(input);

    final ExportResult result = exporter.run(input, FILE_CODE);
    verify(fileServerProxy).processFile(eq(FILE_CODE), any(), any(File.class), any());
    final int count = zipfileCapture.getAllValues().stream().mapToInt(this::countNumberOfFilesInZip).sum();

    assertEquals(9, count, "Not the number of expected files in the zip.");
    assertEquals(FILE_SERVER_EXTERNAL_URL_BASEEXTERNAL, result.getFileUrl(), "Should have return external expected url");
  }

  @Test
  void testExportADMSWithoutKeepingGeneratedFiles() throws Exception {
    config.setKeepGeneratedFiles(false);
    final FileServerProxy fileServerProxy = mock(FileServerProxy.class);
    doReturn(FILE_SERVER_EXTERNAL_URL_BASEEXTERNAL).when(fileServerProxy).processFile(eq(FILE_CODE), any(), zipfileCapture.capture(), any());
    final ADMSExportRunner exporter = new ADMSExportRunner(executor, config, admsData, fileServerProxy);
    final ADMSInputData input = ADMSTestDataGenerator.createInput(ADMSGroupKey.NOX_OTHER);
    // Add a second data set, that should result in an additional set of input files.
    ADMSInputReaderUtil.read2ADMSInputData("../reference/road_transport.upl", ADMSGroupKey.NOX_ROAD.getIndex(), input);
    ADMSTestDataGenerator.setReceptors(input);

    final ExportResult result = exporter.run(input, FILE_CODE);
    verify(fileServerProxy).processFile(eq(FILE_CODE), any(), any(File.class), any());
    assertEquals(FILE_SERVER_EXTERNAL_URL_BASEEXTERNAL, result.getFileUrl(), "Should have return external expected url");
    assertFalse(zipfileCapture.getValue().exists(), "File should have been removed at this point");
  }

  @Test
  void testExportADMSRoadTypeBehaviour() throws Exception {
    config.setKeepGeneratedFiles(true);
    final FileServerProxy fileServerProxy = mock(FileServerProxy.class);
    doReturn(FILE_SERVER_EXTERNAL_URL_BASEEXTERNAL).when(fileServerProxy).processFile(eq(FILE_CODE), any(), zipfileCapture.capture(), any());
    final ADMSExportRunner exporter = new ADMSExportRunner(executor, config, admsData, fileServerProxy);
    final ADMSInputData input = ADMSTestDataGenerator.createInput(ADMSGroupKey.NOX_OTHER);
    // Add a data set containing road sources.
    ADMSInputReaderUtil.read2ADMSInputData("../reference/road_transport.upl", ADMSGroupKey.NOX_ROAD.getIndex(), input);

    // Overwrite the road type that was read, to validate that this is not used when exporting again.
    final String roadTypeSet = "SomethingWeird";
    input.getEmissionSources().values().stream().flatMap(x -> x.stream())
        .filter(ADMSRoadSource.class::isInstance)
        .map(ADMSRoadSource.class::cast)
        .forEach(x -> x.setTraRoadType(roadTypeSet));

    exporter.run(input, FILE_CODE);

    assertNotNull(zipfileCapture.getValue(), "zip file should be present");

    final String output = getUplContentInZip(zipfileCapture.getValue());
    assertFalse(output.contains(roadTypeSet), "Check that the generated file does not contain the road type that was set");
    assertTrue(output.contains("London (central)"), "Sanity check that output does contain road sources (value is intentionally hardcoded)");
  }

  private int countNumberOfFilesInZip(final File zipFile) {
    int count = 0;

    try (final ZipInputStream zipStream = new ZipInputStream(new FileInputStream(zipFile))) {
      while (zipStream.getNextEntry() != null) {
        count++;
      }
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }
    return count;
  }

  private String getUplContentInZip(final File file) {
    // Zip might/will contain multiple UPLs, for this test just add em all together
    final StringBuilder uplContent = new StringBuilder();
    try (final ZipInputStream zipStream = new ZipInputStream(new FileInputStream(file))) {
      ZipEntry zipEntry;
      while ((zipEntry = zipStream.getNextEntry()) != null) {
        if (zipEntry.getName().endsWith(".upl")) {
          uplContent.append(ADMSTestDataGenerator.normalize(zipStream.readAllBytes()));
        }
      }
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }
    return uplContent.toString();
  }
}
