/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ADMSPorosityCalculator}.
 */
class ADMSPorosityCalculatorTest {

  /**
   * <ol>
   * <li>Expected porosity = 100 * (1 - (50.0 / 200) = 75.0
   * <li>Expected porosity = 100 * (1 - (0.0 / 200) = 100.0
   * <li>Expected porosity = 100 * (1 - (200.0 / 200) = 0.0
   * <li>Expected porosity = 100 * (1 - (400.0 / 200) = 0.0
   */
  @ParameterizedTest
  @CsvSource({"50.0, 75.0", "0.0, 100.0", "200.0, 0.0", "400.0, 0.0"})
  void testToPorosity(final double roadLengthWithBuildings, final double expectedPorosity) throws AeriusException {
    final LineString road = createLineString();
    final double porosity = ADMSPorosityCalculator.toPorosity(road, roadLengthWithBuildings);
    assertEquals(expectedPorosity, porosity, 0.0, "Calculated pository incorrect");
  }

  /**
   * <ol>
   * <li>Expected roadLengthWithBuildings = (1 - (75.0 / 100)) * 200 = 50
   * <li>Expected roadLengthWithBuildings = (1 - (100.0 / 100)) * 200 = 0
   * <li>Expected roadLengthWithBuildings = (1 - (0.0 / 100)) * 200 = 200.0
   */
  @ParameterizedTest
  @CsvSource({"75.0, 50.0", "100.0, 0.0", "0.0, 200.0"})
  void testToRoadLengthWithBuildings(final double porosity, final double expectedRoadLengthWithBuildings) throws AeriusException {
    final LineString road = createLineString();
    final double roadLengthWithBuildings = ADMSPorosityCalculator.toRoadLengthWithBuildings(road, porosity);
    assertEquals(expectedRoadLengthWithBuildings, roadLengthWithBuildings, 0.0, "Calculated Building Length incorrect");
  }

  private LineString createLineString() {
    final LineString line = new LineString();
    line.setCoordinates(new double[][] {{ 100, 100}, {200, 100}, {200, 200}});
    return line;
  }
}
