/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.adms.ADMSTestDataGenerator;
import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSBuilding.BuildingShape;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Unit test for {@link BoundingBoxUtil}.
 */
@ExtendWith(MockitoExtension.class)
class BoundingBoxUtilTest {

  @Test
  void testGetBoundingBox() throws AeriusException {
    final BBox bbox = new BBox(0, 0, 1, 1);

    assertEquals(bbox, BoundingBoxUtil.determineBoundingBox(List.of(createPolygon())),
        "The bounding box of an angled polygon should align with grid");
  }

  @Test
  void testDetermineBoundingBoxEmptyList() throws AeriusException {
    assertNull(BoundingBoxUtil.determineBoundingBox(List.of()), "Bounding box of empty list should be null");
  }

  @Test
  void testDetermineBoundingBoxForPoint() throws AeriusException {
    final Point p = new Point(0, 0);
    final BBox bbox = new BBox(0, 0, 0, 0);

    assertEquals(bbox, BoundingBoxUtil.determineBoundingBox(List.of(p)), "The bounding box of a single point should be that point.");
  }

  @Test
  void testDetermineBoundingBoxForLine() throws AeriusException {
    final LineString l = new LineString();
    l.setCoordinates(new double[][] {{0, 0}, {1, 1}});

    final BBox bbox = new BBox(0, 0, 1, 1);

    assertEquals(bbox, BoundingBoxUtil.determineBoundingBox(List.of(l)),
        "The bounding box of a line should be a polygon around that line aligned with the grid");
  }

  @Test
  void testDetermineBoundingBoxForPolygon() throws AeriusException {
    final Polygon p = createPolygon();
    final BBox bbox = new BBox(0, 0, 1, 1);

    assertEquals(bbox, BoundingBoxUtil.determineBoundingBox(List.of(p)), "The bounding box of an angled polygon should align with grid");
  }

  @Test
  void testDetermineBoundingBoxForCircularBuilding() throws AeriusException {
    final BBox buildingBBox = createBuilding(BuildingShape.CIRCULAR);
    final BBox bbox = new BBox(950, 950, 1050, 1050);

    assertEquals(bbox, buildingBBox, "The bounding box of an cicular building should align with grid");
  }

  @Test
  void testDetermineBoundingBoxForSquareBuilding() throws AeriusException {
    final BBox buildingBBox = createBuilding(BuildingShape.RECTANGULAR);
    final BBox bbox = new BBox(929.2893218813452, 929.2893218813452, 1070.7106781186549, 1070.7106781186549);

    assertEquals(bbox, buildingBBox, "The bounding box of an cicular building should align with grid");
  }

  private BBox createBuilding(final BuildingShape shape) throws AeriusException {
    final ADMSBuilding building = new ADMSBuilding();

    building.setShape(shape);
    building.setBuildingCentre(new Point(1000, 1000));
    building.setBuildingLength(100);
    building.setBuildingWidth(100);
    building.setBuildingOrientation(45);
    final BBox buildingBBox= BoundingBoxUtil.determineBoundingBox(List.of(BoundingBoxUtil.toGeometry(building)));
    return buildingBBox;
  }


  private static Polygon createPolygon() {
    final Polygon p = new Polygon();
    p.setCoordinates(new double[][][] {{{0.5, 0}, {0, 0.5}, {0.5, 1}, {1, 0.5}, {0.5, 0}}});
    return p;
  }

  @Test
  void testDetermineBoundingBoxForGeometryCombination() throws AeriusException {
    final Point p = new Point(0, 0);

    final LineString l = new LineString();
    l.setCoordinates(new double[][] {{1, 1}, {2, 2}});

    final Polygon polygon = new Polygon();
    polygon.setCoordinates(new double[][][] {{{2, 2}, {2, 3}, {3, 3}, {3, 2}, {2, 2}}});

    final BBox bbox = new BBox(0, 0, 3, 3);

    assertEquals(bbox, BoundingBoxUtil.determineBoundingBox(List.of(p, l, polygon)),
        "The bounding box of multiple geometries should align with grid and contain all geometries.");
  }

  @ParameterizedTest
  @MethodSource("expandBoundingBox")
  void testExpandBoundingBox(final int source, final int receptor, final int boxMin, final int boxMax)
      throws AeriusException, URISyntaxException, IOException {
    final List<EngineSource> sources = List.of(ADMSTestDataGenerator.simplePointSource(source, source));
    final List<AeriusPoint> receptors = List.of(ADMSTestDataGenerator.createSimpleADMSCalculationPoint(receptor, receptor));
    final BBox extent = BoundingBoxUtil.getExtendedBoundingBox(sources, receptors);

    assertEquals(new BBox(boxMin, boxMin, boxMax, boxMax), extent, "Not the expected bounding box");
  }

  private static List<Arguments> expandBoundingBox() {
    return List.of(
        // Minimum extend should apply been applied to bounding box
        Arguments.of(1000, 1200, 1000 - 500, 1200 + 500),
        // Expansion factor should have been applied to bounding box
        Arguments.of(20000, 30000, 20000 - 1510, 30000 + 1510));
  }

  @Test
  void testExpandBoundingBox() {
    final BBox original = new BBox(10, 10, 20, 30);
    final BBox expanded = BoundingBoxUtil.expandBoundingBox(original, 0.5, 0.0);

    final BBox expected = new BBox(5, 0, 25, 40);
    assertEquals(expected, expanded, "The bounding box should be extended from width 10, height 20 to width 20, height 40");
  }

  @Test
  void testExpandBoundingBoxWithMinimum() {
    final BBox original = new BBox(10, 10, 20, 30);
    final BBox expanded = BoundingBoxUtil.expandBoundingBox(original, 0.5, 10.0);

    final BBox expected = new BBox(0, 0, 30, 40);
    assertEquals(expected, expanded, "The bounding box should be extended from width 10, height 20 to width 30, height 40");
  }
}
