/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.util.OSUtils;

/**
 * Test class for {@link UaiFileWriter}.
 */
class UaiFileWriterTest {

  private static final String TEST_FILENAME = "test";
  private static final String TESTUAI = TEST_FILENAME + ".uai";
  private static final String TESTUAI_QUICK_RUN = TEST_FILENAME + "_quickrun.uai";
  private static final String CANYON_CSV_FILE = "Adv_Canyon.csv";
  private static final String CANYON_CSV_FILE_PATH_PLACEHOLDER = "{CANYON_PATH}";
  private static final String VAR_DEPOSITION_FILE = "var_deposition.csv";
  private static final String VAR_DEPOSITION_FILE_PATH_PLACEHOLDER = "{DEPOSITION_PATH}";

  @TempDir
  File tempDir;

  @ParameterizedTest
  @CsvSource({"true", "false"})
  void testWriteUaiFile(final boolean quickRun) throws IOException, URISyntaxException {
    final File generatedFile = new File(tempDir, TEST_FILENAME);
    final UaiFileWriter writer = new UaiFileWriter(generatedFile, "Europe/London", OSUtils.NL);
    final File advCanyonCsvFile = new File(CANYON_CSV_FILE);

    writer.setDinFile(VAR_DEPOSITION_FILE);
    writer.setQuickRun(quickRun);
    writer.setSubstance(Substance.NOX);
    writer.setASC(advCanyonCsvFile);
    writer.writeUaiFile();
    final String testuai = quickRun ? TESTUAI_QUICK_RUN : TESTUAI;
    final String referenceUai = Files.readString(Path.of(getClass().getResource(testuai).toURI()));
    final String generatedUai = Files.readString(new File(tempDir, TESTUAI).toPath());

    final String patchedReferenceUai = referenceUai
        .replace(CANYON_CSV_FILE_PATH_PLACEHOLDER, advCanyonCsvFile.getAbsolutePath())
        .replace(VAR_DEPOSITION_FILE_PATH_PLACEHOLDER, VAR_DEPOSITION_FILE);
    assertEquals(patchedReferenceUai, generatedUai, "Generated UAI file should match reference file.");
  }
}
