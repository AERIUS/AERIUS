/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

import nl.aerius.adms.ADMSTestDataGenerator;
import nl.aerius.adms.domain.ADMSInputData;
import nl.overheid.aerius.calculation.EngineInputData.ChunkStats;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;

/**
 * Test class for {@link ADMSProgressCollector}.
 */
class ADMSProgressCollectorTest {

  // 1000 points *
  private static final String QUICK_RUN_LINE = " Long term : met line    48 of    774";
  private static final String QUICK_RUN_LINE_ALL = "Long term : met line    774 of    774";

  @Test
  void testQuickRunPogress() {
    final ADMSInputData input = ADMSTestDataGenerator.newADMSInputData(CommandType.CALCULATE);
    final ADMSOptions options = new ADMSOptions();
    options.setMetYears(List.of("2020"));
    input.setAdmsOptions(options);
    input.setChunkStats(new ChunkStats("1", 1000));
    input.setEmissionSources(1, List.of());
    input.setEmissionSources(2, List.of());

    final ADMSProgressCollector collector = new ADMSProgressCollector(input);
    final Consumer<String> consumer1 = collector.registerProgressConsumer();
    final Consumer<String> consumer2 = collector.registerProgressConsumer();

    consumer1.accept(QUICK_RUN_LINE);
    assertEquals(31, (int) collector.progress(), "");
    consumer1.accept(QUICK_RUN_LINE_ALL);
    assertEquals(500, collector.progress(), "");

    consumer2.accept(QUICK_RUN_LINE);
    assertEquals(531, (int) collector.progress(), "");
    consumer2.accept(QUICK_RUN_LINE_ALL);
    assertEquals(1000, collector.progress(), "");
  }
}
