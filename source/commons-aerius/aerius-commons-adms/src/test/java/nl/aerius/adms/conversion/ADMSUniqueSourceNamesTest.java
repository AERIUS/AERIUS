/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.HasName;

/**
 * Test class for {@link ADMSUniqueSourceNames}.
 */
class ADMSUniqueSourceNamesTest {

  private static final String LONG_NAME = "1234567890123456789012345678_01234567890";
  private static final String CUT_OFF_NAME = "1234567890123456789012345678_0";
  private static final String CUT_OFF_NAME_100 = "12345678901234567890123456";
  private static final String CUT_OFF_NAME_10 = CUT_OFF_NAME_100 + "7";
  private static final String CUT_OFF_NAME_1 = CUT_OFF_NAME_10 + "8";

  @ParameterizedTest
  @MethodSource("data")
  void testUniqueName(final List<ADMSSource<?>> sources, final List<String> expectedNames) {
    final ADMSUniqueSourceNames un = new ADMSUniqueSourceNames();

    sources.forEach(s -> un.setUniqueName(s, s.getName()));
    final List<String> renamedNames = sources.stream().map(HasName::getName).toList();

    assertEquals(expectedNames, renamedNames, "Should only rename duplicated");
  }

  private static List<Arguments> data() {
    return List.of(
        // Test if correctly renamed
        Arguments.of(
            List.of(createSource("name1"), createSource("name2"), createSource("name1"), createSource("name3"), createSource("name1")),
            List.of("name1", "name2", "name1.1", "name3", "name1.2")),
        // Test if new name also already exists.
        Arguments.of(
            List.of(createSource("name1"), createSource("name1.1"), createSource("name1"), createSource("name1.2"), createSource("name1.2")),
            List.of("name1", "name1.1", "name1.3", "name1.2", "name1.2.1")),
        // Test if original name that matches a duplicated name is also renamed correctly.
        Arguments.of(
            List.of(createSource("name1"), createSource("name1"), createSource("name1"), createSource("name1.1"), createSource("name1.3")),
            List.of("name1", "name1.4", "name1.2", "name1.1", "name1.3")),
        Arguments.of(longNames(), cutOffLongName()));
  }

  private static List<?> longNames() {
    return IntStream.range(0, 110).mapToObj(i -> createSource(LONG_NAME)).toList();
  }

  private static List<String> cutOffLongName() {
    final List<String> cutOffNamesList = new ArrayList<>();
    cutOffNamesList.add(CUT_OFF_NAME);
    cutOffNamesList.addAll(IntStream.range(1, 10).mapToObj(i -> CUT_OFF_NAME_1 + "." + i).toList());
    cutOffNamesList.addAll(IntStream.range(0, 90).mapToObj(i -> CUT_OFF_NAME_10 + "." + (i + 10)).toList());
    cutOffNamesList.addAll(IntStream.range(0, 10).mapToObj(i -> CUT_OFF_NAME_100 + "." + (i + 100)).toList());
    return cutOffNamesList;
  }


  private static ADMSSource<?> createSource(final String name) {
    final ADMSSource<?> source = new ADMSSource<>();
    source.setName(name);
    return source;
  }
}
