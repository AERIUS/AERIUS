/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.util.OSUtils;

/**
 * Test reads and writes .asp files and verifies if the content is still the same.
 */
class AspFileRoundtripTest {

  private static final String ASP_FILE = "points.asp";

  @TempDir
  File tempDir;

  @Test
  void testRoundtrip() throws IOException {
    final LineReaderResult<AeriusPoint> content;
    final AspFileReader reader = new AspFileReader();

    try (InputStream is = getClass().getResourceAsStream(ASP_FILE)) {
      content = reader.readObjects(is);
    }
    final File outputFile = new File(tempDir, ASP_FILE);

    AspFileWriter.writeFile(outputFile, content.getObjects(), OSUtils.NL);
    final String output = new String(Files.readAllBytes(outputFile.toPath()), StandardCharsets.UTF_8);
    final String reference = new String(getClass().getResourceAsStream(ASP_FILE).readAllBytes(), StandardCharsets.UTF_8);

    assertEquals(reference, output, "Read and written asp file should be the same");
  }
}
