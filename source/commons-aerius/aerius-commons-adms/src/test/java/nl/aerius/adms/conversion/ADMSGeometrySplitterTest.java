/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;

/**
 * Test class for {@link AplGeometrySplitter}.
 */
class ADMSGeometrySplitterTest {

  @ParameterizedTest
  @MethodSource("referenceLineStrings")
  void testSplit(final LineString lineString, final int splitCount) {
    final List<Geometry> geometries = ADMSGeometrySplitter.split(lineString);
    assertEquals(splitCount, geometries.size(), "Should be split in this number of coordinates");
    // each split should contain 1 overlapping coordinates with another line, meaning total should be plus split count minus 1.
    final int expectedNumberOfCoordinates = lineString.getCoordinates().length + splitCount - 1;
    final int totalSplitCoordinates = geometries.stream().mapToInt(g -> ((LineString) g).getCoordinates().length).sum();
    assertEquals(expectedNumberOfCoordinates, totalSplitCoordinates, "Expected number of coordinates after split doesn't match");
    for (int i = 0; i < splitCount - 1; i++) {
      final double[][] first = coor(geometries, i);
      final double[][] second = coor(geometries, i + 1);

      assertArrayEquals(first[first.length - 1], second[0], "Last and first coordinate of next should be the same.");
    }
  }

  private double[][] coor(final List<Geometry> geometries, final int i) {
    return ((LineString) geometries.get(i)).getCoordinates();
  }

  private static Object[][] referenceLineStrings() {
    return new Object[][] {
      {generateExampleLineString(20), 1},
      {generateExampleLineString(50), 1},
      {generateExampleLineString(51), 1},
      {generateExampleLineString(52), 2},
      {generateExampleLineString(300), 6},
    };
  }

  private static LineString generateExampleLineString(final int numberOfCoordinates) {
    final double[][] coordinates = new double[numberOfCoordinates][2];

    for (int i = 0; i < numberOfCoordinates; i++) {
      coordinates[i] = new double[] {i * 10, i * 10};
    }
    final LineString ls = new LineString();
    ls.setCoordinates(coordinates);
    return ls;
  }
}
