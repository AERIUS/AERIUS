/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.spy;

import java.util.List;

import nl.aerius.adms.version.ADMSVersion;
import nl.aerius.adms.version.NOxToNO2Version;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Mocks the ADMSVersion enum. This allows to use the same filenames for testing independent if this changes in the actual version.
 */
public class ADMSVersionMock {

  public static ADMSVersion mockADMSVersion() {
    final ADMSVersion version = spy(ADMSVersion.NCA_LATEST);
    lenient().doReturn("spatially_varying_roughness_500m").when(version).getSurfaceRoughnessVersion();
    lenient().doReturn("spatially_varying_dry_deposition_velocity_500m").when(version).getPlumeDepletionVersionFormat();
    lenient().doReturn("spatially_varying_dry_deposition_velocity_NH3_500m").when(version).getPlumeDepletionFileName(eq(Substance.NH3));
    lenient().doReturn("spatially_varying_dry_deposition_velocity_NO2_500m").when(version).getPlumeDepletionFileName(eq(Substance.NO2));
    lenient().doReturn("uk_terrain").when(version).getTerrainHeightVersionFormat();
    lenient().doReturn("uk_terrain").when(version).getTerrainHeightFileName(anyInt());
    lenient().doReturn(List.of("uk_met_files")).when(version).getMetFilesVersion();
    lenient().doReturn("deposition_velocity_2022").when(version).getDepositionVelocityVersion();

    doReturn(mockNOxToNO2Version()).when(version).getNOxToNO2Version();
    return version;
  }

  public static NOxToNO2Version mockNOxToNO2Version() {
    final NOxToNO2Version version = spy(NOxToNO2Version.V8_1);

    lenient().doReturn("nox_to_no2_calculator").when(version).getDataDirectory();
    lenient().doReturn("lacodes.csv").when(version).getLaCodesFilename(eq(Substance.NO2));
    lenient().doReturn("lacodes.csv").when(version).getLaCodesFilename(eq(Substance.NOX));
    lenient().doReturn("lacodes.csv").when(version).getLaCodesFilename(eq(Substance.O3));
    lenient().doReturn("lacodes.shp").when(version).getUweShapeFilename();
    lenient().doReturn("fno2_lookup_test.shp").when(version).getfNO2ShapeFilename();
    lenient().doReturn("fno2_factors.csv").when(version).getfNO2FactorsFilename();
    return version;
  }
}
