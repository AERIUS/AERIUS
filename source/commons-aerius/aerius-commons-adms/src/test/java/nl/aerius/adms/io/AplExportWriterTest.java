/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileKeys.OPT_NUM_OUTPUTS;
import static nl.aerius.adms.io.AplFileKeys.OPT_POL_NAME;
import static nl.aerius.adms.io.AplFileKeys.SRC_NUM_POLLUTANTS;
import static nl.aerius.adms.io.AplFileKeys.SRC_POLLUTANTS;
import static nl.aerius.adms.io.AplFileKeys.SRC_POL_EMISSION_RATE;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.aerius.adms.ADMSTestDataGenerator;
import nl.aerius.adms.BaseADMSRunnerTest;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.domain.ADMSSource;
import nl.aerius.adms.util.ADMSEngineSources;
import nl.aerius.adms.util.BoundingBoxUtil;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Test class for {@link AplExportWriter}.
 */
class AplExportWriterTest extends BaseADMSRunnerTest {

  public AplExportWriterTest() {
    super(false);
  }

  /**
   * @param demoMode true if demo mode activated, in that case SpatiallyVaryingRoughness and PlumeDepletionNH3 is set to false
   * @param hasTerrain true if expected to have terrain data files
   * @param hasDin true if expected to have din file
   */
  @ParameterizedTest
  @MethodSource("fastModeData")
  void testFastMode(final boolean demoMode, final boolean hasTerrain, final boolean hasDin)
      throws IOException, AeriusException {
    final File tmpFile = new File(tempDir, "AplExportWriterTest_testFastMode");

    final ADMSSource<Point> source1 = new ADMSSource<>();
    source1.setSourceType(SourceType.POINT);
    source1.setGeometry(new Point(216000, 771500));
    final List<EngineSource> sources = List.of(source1);

    write(demoMode, sources, tmpFile);

    assertEquals(hasTerrain, new File(tmpFile.getParentFile(), tmpFile.getName() + ".ruf").exists(), "Unexpected .ruf file");
    assertEquals(hasTerrain, new File(tmpFile.getParentFile(), tmpFile.getName() + ".ter").exists(), "Unexpected .ter file");
    assertEquals(hasDin, new File(tmpFile.getParentFile(), tmpFile.getName() + ".din").exists(), "Unexpected .din file");
  }

  private static List<Arguments> fastModeData() {
    // @formatter:off
    return List.of(
        //           demo,  has terrain, has din
        Arguments.of(false, true,        true),
        Arguments.of(true,  false,       false),
        Arguments.of(true,  false,       false)
        );
    // @formatter:on
  }

  @Test
  void testRoadNH3AplFile() throws IOException, AeriusException {
    final File tmpFile = new File(tempDir, "AplExportWriterTest_testFastMode");

    final ADMSSource<?> source = ADMSTestDataGenerator.createRoadSource("road1",
        new double[][] {{544788.0, 258880.0}, {544910.92, 258725.07}, {544999, 258999}});

    source.getEmissions().put(Substance.NH3, 3.0);
    source.getEmissions().put(Substance.NOX, 4.0);
    final List<EngineSource> sources = List.of(source);

    write(true, sources, tmpFile);
    final List<String> lines = ADMSTestDataGenerator.readLinesUplFile(tmpFile);
    assertEquals("1", ADMSTestDataGenerator.findKeyValue(lines, OPT_NUM_OUTPUTS.getKey()), "Should contain only 1 substance");
    assertEquals("\"NH3\"", ADMSTestDataGenerator.findKeyNextLineValue(lines, OPT_POL_NAME.getKey()).trim(), "Should only contain NH3");
    assertEquals("2", ADMSTestDataGenerator.findKeyValue(lines, SRC_NUM_POLLUTANTS.getKey()), "Should contain 2 substances");
    assertEquals("\"NH3\" \"NOx\"", ADMSTestDataGenerator.findKeyNextLineValue(lines, SRC_POLLUTANTS.getKey()).trim(),
        "Should contain both substances");
    assertEquals("3.0e+0 4.0e+0", ADMSTestDataGenerator.findKeyNextLineValue(lines, SRC_POL_EMISSION_RATE.getKey()).trim(),
        "Should contain both substances");
  }

  private void write(final boolean demoMode, final List<EngineSource> sources, final File tmpFile)
      throws AeriusException, IOException {
    final ADMSInputData content = ADMSTestDataGenerator.newADMSInputData(CommandType.EXPORT);

    content.setEmissionSources(1, sources);
    content.setSubstances(List.of(Substance.NH3));
    content.setTimeZone(TIME_ZONE);
    content.setFileType(ADMSSourceFileType.UPL);
    content.setMeteoYear(ADMSTestDataGenerator.TEST_MET_YEAR);
    final ADMSOptions admsOptions = new ADMSOptions();
    admsOptions.setMetSiteId(ADMSTestDataGenerator.TEST_MET_SITE_ID);
    admsOptions.setMetDatasetType(ADMSTestDataGenerator.TEST_MET_DATASET_TYPE);
    admsOptions.setMetYears(List.of(ADMSTestDataGenerator.TEST_MET_YEAR));
    admsOptions.setComplexTerrain(true);
    admsOptions.setSpatiallyVaryingRoughness(!demoMode);
    admsOptions.setPlumeDepletionNH3(!demoMode);
    content.setAdmsOptions(admsOptions);

    final ADMSEngineSources admsEngineSources = ADMSEngineSources.builder().set(content.getEmissionSources().get(ADMSGroupKey.NH3.getIndex()))
        .build();
    final BBox extent = BoundingBoxUtil.getExtendedBoundingBox(sources,
        content.getCalculateReceptors().stream().map(AeriusPoint.class::cast).toList());

    new AplExportWriter(admsData, ADMSVersion.NCA_LATEST, OSUtils.NL).write(tmpFile, content, ADMSGroupKey.NH3,
        admsEngineSources, content.getCalculateReceptors(), extent);
  }
}
