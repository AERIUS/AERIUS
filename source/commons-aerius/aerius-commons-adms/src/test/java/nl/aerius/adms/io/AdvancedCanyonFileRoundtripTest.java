/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.aerius.adms.ADMSTestDataGenerator;
import nl.aerius.adms.conversion.ADMS2EmissionSourceConverter;
import nl.aerius.adms.conversion.EmissionSource2ADMSConverter;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Roundtrip test for {@link AdvancedCanyonFileReader} and {@link AdvancedCanyonFileWriter}.
 */
class AdvancedCanyonFileRoundtripTest {

  private static final int YEAR = 2030;
  private static final List<Substance> SUBSTANCES = List.of(Substance.NOX);
  private static final String ADVANCED_CANYON_TEST = "advanced_canyon_test";
  private static final String ADVANCED_CANYON_TEST_CSV = ADVANCED_CANYON_TEST + ".csv";

  @TempDir
  File tempDir;

  @Test
  void testRoundTrip() throws IOException, AeriusException {
    final List<ADMSSource<?>> originalSources = ADMSTestDataGenerator.createRoadSources();
    // Read the Advanced Canyon File
    final AdvancedCanyonFileReader reader = new AdvancedCanyonFileReader(originalSources);

    assertACFReader(reader);
    final List<ADMSSource<?>> sources = roundTripSources(originalSources);
    // Write the Advanced Canyon File
    final AdvancedCanyonFileWriter writer = new AdvancedCanyonFileWriter(new File(tempDir, ADVANCED_CANYON_TEST));

    writer.write(sources);
    final String actualAdvancedCanyonOutput = new String(Files.readAllBytes(writer.getOutputFile().toPath()), StandardCharsets.UTF_8);
    // Read reference file
    final String referenceAdvancedCanyonOutput;
    try (final InputStream is = getClass().getResourceAsStream(ADVANCED_CANYON_TEST_CSV)) {
      referenceAdvancedCanyonOutput = new String(is.readAllBytes(), StandardCharsets.UTF_8);
    }
    assertEquals(referenceAdvancedCanyonOutput, actualAdvancedCanyonOutput, "Expected Advanced Canyon output to be the same");
  }

  private static List<ADMSSource<?>> roundTripSources(final List<ADMSSource<?>> sources) throws AeriusException {
    final EmissionSource2ADMSConverter converter = new EmissionSource2ADMSConverter(EPSG.BNG.getSrid(), YEAR, new RoadEmissionCategories());
    final List<ADMSSource<?>> convertedSources = new ArrayList<>();

    for (final ADMSSource<?> admsSource : sources) {
      final EmissionSourceFeature esf = ADMS2EmissionSourceConverter.convert(admsSource, admsSource.getName());
      convertedSources.add((ADMSSource<?>) converter.convert(esf.getProperties(), esf.getGeometry(), SUBSTANCES).get(0));
    }
    return convertedSources;
  }

  private static void assertACFReader(final AdvancedCanyonFileReader reader) throws IOException {
    try (final InputStream is = AdvancedCanyonFileRoundtripTest.class.getResourceAsStream(ADVANCED_CANYON_TEST_CSV)) {
      final LineReaderResult<Void> results = reader.readObjects(is);
      final Object[] exceptions = results.getExceptions().toArray();
      assertArrayEquals(new Object[0], exceptions, "Should have no exceptions: " + Arrays.toString(exceptions));
    }
  }
}
