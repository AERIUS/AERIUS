&ADMS_HEADER
Comment     = "This is an ADMS parameter file"
Model       = "ADMS"
Version     = 5.2
FileVersion = 12
Complete    = 1
/
&ADMS_PARAMETERS_SUP
SupSiteName                 = "{SupSiteName}"
SupProjectName              = "{SupProjectName}"
SupUseAddInput              = {SupUseAddInput}
SupAddInputPath             = "{SupAddInputPath}"
SupReleaseType              = 0
SupModelBuildings           = {SupModelBuildings}
SupModelComplexTerrain      = {SupModelComplexTerrain}
SupModelCoastline           = 0
SupPufType                  = 0
SupCalcChm                  = 0
SupCalcDryDep               = 0
SupCalcWetDep               = 0
SupModelOdours              = 0
SupPaletteType              = 1
SupUseSpatialSplitting      = 0
SupNumSplitRegions          = 0
SupUseSrcExcByRec           = 0
SupUseSrcExcByEmRate        = 0
SupUseTimeVaryingEmissions  = {SupUseTimeVaryingEmissions}
SupTimeVaryingEmissionsType = {SupTimeVaryingEmissionsType}
SupUseTimeVaryingFAC        = {SupUseTimeVaryingFAC}
SupTimeVaryingFACPath       = "{SupTimeVaryingFACPath}"
SupUseTimeVaryingHFC        = 0
SupTimeVaryingHFCPath       = " "
SupUseTimeVaryingScreenBySource= "00001100"
SupTimeVaryingEmissionFactorsWeekday =
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
SupTimeVaryingEmissionFactorsSaturday =
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
SupTimeVaryingEmissionFactorsSunday =
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
SupTrafficEmissionsDataset  = "EFT v9.0 (2 VC)"
SupModelAircraft            = 0
SupAircraftDataPath         = " "
SupAircraftUseSpeedEmissionFile= 0
SupAircraftSpeedEmissionPath= " "
SupAircraftNumGroups        = 0
SupNO2PercentIndustrial     = 5
SupNO2PercentRoad           = 23.8
SupNO2PercentGrid           = 12
SupNO2PercentAircraft       = 5
/
&ADMS_PARAMETERS_MET
MetLatitude               = {MetLatitude}
MetDataSource             = 0
MetDataFileWellFormedPath = "{MetDataFileWellFormedPath}"
MetWindHeight             = 1.0e+1
MetWindInSectors          = {MetWindInSectors}
MetWindSectorSizeDegrees  = 1.0e+1
MetDataIsSequential       = {MetDataIsSequential}
MetUseSubset              = 0
MetSubsetHourStart        = 1
MetSubsetDayStart         = 1
MetSubsetMonthStart       = 1
MetSubsetYearStart        = {MetSubsetYearStart}
MetSubsetHourEnd          = 24
MetSubsetDayEnd           = 31
MetSubsetMonthEnd         = 12
MetSubsetYearEnd          = {MetSubsetYearEnd}
MetUseVerticalProfile     = 0
MetVerticalProfilePath    = " "
{Met_DS_MS_Parameters}
MetHeatFluxType           = 0
MetInclBoundaryLyrHt      = 1
MetInclSurfaceTemp        = 0
MetInclLateralSpread      = 0
MetInclRelHumidity        = 0
MetHandNumEntries         = 0
/
{ADMS_PARAMETERS_BLD}
&ADMS_PARAMETERS_HIL
HilGridSize        = {HilGridSize}
HilUseTerFile      = {HilUseTerFile}
HilUseRoughFile    = {HilUseRoughFile}
HilTerrainPath     = "{HilTerrainPath}"
HilRoughPath       = "{HilRoughPath}"
HilCreateFlowField = 0
/
&ADMS_PARAMETERS_CST
CstPoint1X         = 0.0e+0
CstPoint1Y         = 0.0e+0
CstPoint2X         = -1.000e+3
CstPoint2Y         = 1.000e+3
CstLandPointX      = 5.00e+2
CstLandPointY      = 5.00e+2
/
&ADMS_PARAMETERS_GRD
GrdType            = 1
GrdCoordSysType    = 0
GrdSpacingType     = 0
GrdUseSourceOrientedGrids = 
 1
 0
GrdRegularMin      = 
 -1.000e+3 -1.000e+3 0.0e+0
GrdRegularMax      = 
 1.000e+3 1.000e+3 0.0e+0
GrdRegularNumPoints= 
 31 31 1
GrdVarSpaceNumPointsX  = 0
GrdVarSpaceNumPointsY  = 0
GrdVarSpaceNumPointsZ  = 0
GrdPtsNumPoints        = 0
GrdPtsUsePointsFile    = 1
GrdPtsPointsFilePath   = "{GrdPtsPointsFilePath}"
/
&ADMS_PARAMETERS_OPT
{ADMS_PARAMETERS_OPT}
&ADMS_PARAMETERS_CHM
ChmScheme       = 1
/
&ADMS_PARAMETERS_BKG
BkgFilePath     = " "
BkgFixedLevels  = 2
/
&ADMS_COORDINATESYSTEM
ProjectedEPSG               = 27700
/
&ADMS_PARAMETERS_ETC
SrcNumSources   = {SrcNumSources}
PolNumPollutants= {PolNumPollutants}
PolNumIsotopes  = 1
/
&ADMS_MAPPERPROJECT
ProjectFilePath             = " "
/
{ADMS_POLLUTANT_DETAILS}
&ADMS_ISOTOPE_DETAILS
IsoName                  = "(unnamed-isotope)"
IsoPollutantType         = 0
IsoGasDepVelocityKnown   = 1
IsoGasDepositionVelocity = 0.0e+0
IsoGasType               = 0
IsoParDepVelocityKnown   = 1
IsoParTermVelocityKnown  = 1
IsoParNumDepositionData  = 1
IsoParDepositionVelocity =
  0.0e+0
IsoParTerminalVelocity =
  0.0e+0
IsoParDiameter =
  1.0e-6
IsoParDensity =
  1.000e+3
IsoParMassFraction =
  1.0e+0
IsoWetWashoutKnown = 1
IsoWetWashout      = 0.0e+0
IsoWetWashoutA     = 1.0e-4
IsoWetWashoutB     = 6.4e-1
IsoConvFactor      = 1.0e+0
IsoBkgLevel        = 0.0e+0
IsoBkgUnits        = "Bq/m3"
/
