/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.aerius.adms.conversion.ADMSPorosityCalculator;
import nl.aerius.adms.domain.ADMSRoadSource;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.io.AbstractLineColumnReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrier;
import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrierType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Reads an advanced canyon csv file and attaches the information to the given road sources.
 */
class AdvancedCanyonFileReader extends AbstractLineColumnReader<Void> {

  private static final String SPLIT_PATTERN = ",";
  private static final int HEADER_COUNT = 2;

  private enum Columns implements HasColumnName {
    NAME_COLUMN("name"),
    X1_COLUMN("X1"),
    Y1_COLUMN("Y1"),
    X2_COLUMN("X2"),
    Y2_COLUMN("X2"),
    WIDTH_L("width_L"),
    AVG_HEIGHT_L("avgHeight_L"),
    MIN_HEIGHT_L("minHeight_L"),
    MAX_HEIGHT_L("maxHeight_L"),
    BUILD_LENGTH_L("buildLength_L"),
    WIDTH_R("width_R"),
    AVG_HEIGHT_R("avgHeight_R"),
    MIN_HEIGHT_R("minHeight_R"),
    MAX_HEIGHT_R("maxHeight_R"),
    BUILD_LENGTH_R("buildLength_R"),
    FRAC_COVERED("fracCovered");

    private final String columnName;

    Columns(final String columnName){
      this.columnName = columnName;
    }

    @Override
    public String columnName() {
      return columnName;
    }
  }

  private final Map<String, ADMSRoadSource> map = new HashMap<>();

  public AdvancedCanyonFileReader(final List<ADMSSource<?>> sources) {
    super(SPLIT_PATTERN);
    sources.stream().filter(s -> s instanceof ADMSRoadSource).map(ADMSRoadSource.class::cast).forEach(this::initSourceMap);
  }

  private void initSourceMap(final ADMSRoadSource roadSource) {
    final double[][] c = roadSource.getGeometry().getCoordinates();
    map.put(getMapKey((int) c[0][0], (int) c[0][1], (int) c[1][0], (int) c[1][1]), roadSource);
  }

  @Override
  public LineReaderResult<Void> readObjects(final InputStream inputStream) throws IOException {
    try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
      return readObjects(reader, HEADER_COUNT);
    }
  }

  @Override
  protected Void parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final ADMSRoadSource roadSource = findRoadSource();

    if (roadSource == null) {
      throw new AeriusException(AeriusExceptionReason.ADMS_NO_ROAD_FOR_BARRIER, getString(Columns.NAME_COLUMN));
    }
    roadSource.setLeftRoadSideBarrier(readBarrier(roadSource.getGeometry(), Columns.WIDTH_L, Columns.AVG_HEIGHT_L, Columns.MIN_HEIGHT_L,
        Columns.MAX_HEIGHT_L, Columns.BUILD_LENGTH_L));
    roadSource.setRightRoadSideBarrier(readBarrier(roadSource.getGeometry(), Columns.WIDTH_R, Columns.AVG_HEIGHT_R, Columns.MIN_HEIGHT_R,
        Columns.MAX_HEIGHT_R, Columns.BUILD_LENGTH_R));
    roadSource.setFractionCovered(getDouble(Columns.FRAC_COVERED));
    return null;
  }

  @Override
  protected void addObject(final LineReaderResult<Void> result, final Void object) {
    // barrier information is not returned but directly attached to the a road source object. Therefore nothing to add.
  }

  private ADMSRoadSource findRoadSource() {
    return map.get(getMapKey(getInt(Columns.X1_COLUMN), getInt(Columns.Y1_COLUMN), getInt(Columns.X2_COLUMN), getInt(Columns.Y2_COLUMN)));
  }

  private ADMSRoadSideBarrier readBarrier(final LineString road, final Columns width, final Columns avgHeight, final Columns minHeight,
      final Columns maxHeight, final Columns buildLength) throws AeriusException {
    final ADMSRoadSideBarrier barrier = new ADMSRoadSideBarrier();

    barrier.setBarrierType(ADMSRoadSideBarrierType.OTHER);
    barrier.setWidth(getDouble(width));
    barrier.setAverageHeight(getDouble(avgHeight));
    barrier.setMinimumHeight(getDouble(minHeight));
    barrier.setMaximumHeight(getDouble(maxHeight));
    barrier.setPorosity(ADMSPorosityCalculator.toPorosity(road, getDouble(buildLength)));
    return barrier;
  }

  /**
   * Use the x-y coordinates of the first 2 coordinates of a road segment as key to find the sources.
   * source coordinates are cast to int values to avoid rounding errors. Resolution of int should be enough to distingish between roads.
   *
   * @param x1
   * @param y1
   * @param x2
   * @param y2
   * @return concatenated string of coordinates
   */
  private String getMapKey(final int x1, final int y1, final int x2, final int y2) {
    return String.format("%d-%d-%d-%d", x1, y1, x2, y2);
  }
}
