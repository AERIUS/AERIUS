/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.util;

import java.io.Closeable;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.calculation.domain.ModelProgressReport;

/**
 * Collects the progress of multiple concurrent runs. All runs use the same met file, and therefore will have the same total number of met lines
 * to process. Therefore the total only needs to be determined once. For each run the progress is collected and the partial progress is
 * calculated by summing the progress of each individual run. Divide that by the expected total times the number of runs.
 *
 * This class also sends the progress at a regular interval via the intermediate results handler back to the chunker worker.
 */
public class ADMSProgressCollector implements Closeable {

  private static final Logger LOGGER = LoggerFactory.getLogger(ADMSProgressCollector.class);
  private static final int INITIAL_PROGRESS_REPORT_STARTUP_DELAY_SECONDS = 10;
  private static final Pattern PROGRESS_PATTERN = Pattern.compile(".*Long term : met line\\s+(\\d+) of\\s+(\\d+)", Pattern.UNICODE_CHARACTER_CLASS);
  private static final int TOTAL_PROGRESS_SCALE = 16;

  private final WorkerIntermediateResultSender workerIntermediateResultSender;
  private final ScheduledExecutorService service;
  private final List<ProgressConsumer> progressConsumers = new ArrayList<>();

  private final String chunkId;
  private final BigDecimal nrOfPoints;
  private final BigDecimal nrOfRuns;

  private BigDecimal total;

  /**
   * Progress collector that doesn't sent intermediate progress reports via a WorkerIntermediateResultSender. Only to be used in unit tests.
   *
   * @param input
   */
  public ADMSProgressCollector(final ADMSInputData input) {
    this(null, 0, input);
  }

  public ADMSProgressCollector(final WorkerIntermediateResultSender workerIntermediateResultSender, final int progressIntervalSeconds,
      final ADMSInputData input) {
    this.workerIntermediateResultSender = workerIntermediateResultSender;
    chunkId = input.getChunkStats().chunkId();
    this.nrOfRuns = BigDecimal.valueOf(input.getEmissionSources().size());
    this.nrOfPoints = BigDecimal.valueOf(input.getChunkStats().nrOfPointsToCalculate());

    if (workerIntermediateResultSender != null) {
      service = Executors.newScheduledThreadPool(1);
      service.scheduleWithFixedDelay(this::sendProgress, INITIAL_PROGRESS_REPORT_STARTUP_DELAY_SECONDS, progressIntervalSeconds, TimeUnit.SECONDS);
    } else {
      // When no intermediate result handler provided no data is send back. Used in unit tests.
      service = null;
    }
  }

  public Consumer<String> registerProgressConsumer() {
    final ProgressConsumer progressConsumer = new ProgressConsumer();

    progressConsumers.add(progressConsumer);
    return progressConsumer::parse;
  }

  @Override
  public void close() throws IOException {
    if (service != null) {
      service.shutdown();
    }
  }

  private void sendProgress() {
    try {
      workerIntermediateResultSender.sendIntermediateResult(new ModelProgressReport(chunkId, progress()));
    } catch (final IOException e) {
      LOGGER.warn("Unable to sent intermediate progress results", e);
    }
  }

  double progress() {
    if (total == null) {
      return 0.0;
    }
    final BigDecimal progressAllRuns = BigDecimal.valueOf(progressConsumers.stream().mapToInt(ProgressConsumer::getProgress).sum());

    return progressAllRuns.divide(total, TOTAL_PROGRESS_SCALE, RoundingMode.HALF_UP).multiply(nrOfPoints).doubleValue();
  }

  private class ProgressConsumer {
    private int progress;

    public int getProgress() {
      return progress;
    }

    /**
     * Parses the line from the ADMS output and extracts the progress based on number of met data processed.
     *
     * @param line
     */
    public void parse(final String line) {
      final Matcher matcher = PROGRESS_PATTERN.matcher(line);

      if (matcher.find()) {
        progress = Integer.parseInt(matcher.group(1));
        synchronized (ADMSProgressCollector.this) {
          if (total == null) {
            total = BigDecimal.valueOf(Integer.parseInt(matcher.group(2))).multiply(nrOfRuns);
          }
        }
      }
    }
  }
}
