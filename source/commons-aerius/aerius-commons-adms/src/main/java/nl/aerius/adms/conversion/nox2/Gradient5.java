/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion.nox2;

import static nl.aerius.adms.conversion.nox2.NOxToNO2Constants.MG_M3_TO_PPM;

/**
 * Converts NOx to NO2 using finite difference model
 *
 * NOTE: The algorithm implemtend here is taken from the spreadsheet NOx_to_NO2_Calculator_v8.1_noprotection.xlsm
 * It has been converted from Excell Visual Basic to Java. However the algorithm has been kept as much as possible to be the same as
 * the original Visual Basic code to keep it as transparent as possible. This means it's a long method, not compliant with the coding standards.
 */
class Gradient5 {

  private static final double LARGE_NUMBER = 2.59378E+16;
  private static final int NITER = 100;

  /**
   * This subroutine calculates NO2 concentrations at roadside from local NOx concentration (NOx_site) and from regional background concs _back.
   * The calculation implements a finite difference model of the surface boundary layer.
   * It assumes neutral stability  conditions (in this version) with turbulence characterised by friction velocity, ustar and surface roughness, z0
   * It assumes NO+O3 reaction rate k1 and NO2 photooxidation rate j
   * h is the height of the surface boundary layer
   * almo the MOnin-Obukhov length is not used in this version.
   */
  public static Gradient5Result gradient5(final double afac, final double noxSite, final double no2Back, final double noxBack, final double o3Back,
      final double ustar, final double z0, final double k1, final double j, final double h, final double almo) {
    // Preliminaries
    final double[] nox = new double[11];
    final double[] o3 = new double[11];
    final double[] no2 = new double[11];
    final double[] tempo = new double[11];
    final double[] no = new double[11];
    final double[] a = new double[11];
    final double[] b = new double[11];
    final double[] c = new double[11];
    final double[] d = new double[11];
    final double[] b2 = new double[11];
    final double[] d2 = new double[11];

    final double rbrcO3 = 500D;
    final double hb10 = h / 10D;

    // Set initial concentrations at finite difference nodes

    final int nr = 11;
    final int nrLast = nr - 1;

    for (int n = 0; n < nr; n++) { // For n = 1 To nr
      nox[n] = LARGE_NUMBER * noxBack;
      o3[n] = LARGE_NUMBER * o3Back;
      no2[n] = LARGE_NUMBER * no2Back;
      no[n] = LARGE_NUMBER * (noxBack - no2Back);
    }

    nox[0] = LARGE_NUMBER * noxSite;

    // More preliminaries

    final double eO3 = 0D; //?? unused
    final double ku = 0.41 * ustar;
    final double ra = 1 / ku * Math.log(hb10 / z0);
    final double dr = ra / 10D;
    final double alpha = 1D / ku / dr / dr;

    // Calculate surface emission rates of NO2 and NO from NOx concentration gradient
    // The finite differnce model  is solved using Gaussian elimination algorithm for a tridiagonal matrix

    // NOx
    a[0] = 0D;
    b[0] = 1D;
    c[0] = 0D;
    d[0] = nox[0];
    b2[0] = b[0];
    d2[0] = d[0];


    for (int n = 1; n < nr - 1; n++) { // For n = 2 To nr - 1
      // double an = n - 1D;
      final double r = n * dr;
      final double z = z0 * Math.exp(ku * r);

      a[n] = alpha;
      b[n] = -2D * alpha;
      c[n] = alpha;
      d[n] = 0;
    }

    for (int n = 1; n < nr - 1; n++) { // For n = 2 To nr - 1
      b2[n] = b[n] - a[n] * c[n - 1] / b2[n - 1];
      d2[n] = d[n] - a[n] / b2[n - 1] * d2[n - 1];
    }

    nox[nrLast - 1] = (d2[nrLast - 1] - c[nrLast - 1] * nox[nrLast]) / b2[nrLast - 1];
    if (no[nrLast - 1] < 0D) {
      no[nrLast - 1] = 0D;
    }

    for (int i = 1; i < nr - 2; i++) { // For i = 2 To nr - 2
      final int n = (nrLast-1) - i;
      nox[n] = d[n + 1] / a[n + 1] - b[n + 1] / a[n + 1] * nox[n + 1] - c[n + 1] / a[n + 1] * nox[n + 2];
      if (nox[n] < 0D) {
        nox[n] = 0D;
      }
    }

    final double eno2 = afac * (nox[0] - nox[1]) / dr;
    final double eno = eno2 / afac * (1D - afac);

    for (int iter = 0; iter < NITER;  iter++) { // For iter = 1 To niter
      // Calculate NO concentration profile with height using finite differnce model
      // Uses Gaussian elimination algorithm for tri diagonal matrix

      //NO
      a[0] = 0D;
      b[0] = -1D / dr;
      c[0] = 1D / dr;
      d[0] = -eno;
      b2[0] = b[0];
      d2[0] = d[0];


      for (int n = 1; n < nr - 1; n++) { // For n = 2 To nr - 1
        //an = n - 1D
        final double r = n * dr;
        final double z = z0 * Math.exp(ku * r);

        a[n] = alpha;
        b[n] = -2D * alpha - k1 * z * o3[n];
        c[n] = alpha;
        d[n] = -j * z * no2[n];

      }

      for (int n = 1; n < nr - 1; n++) { // For n = 2 To nr - 1
        b2[n] = b[n] - a[n] * c[n - 1] / b2[n - 1];
        d2[n] = d[n] - a[n] / b2[n - 1] * d2[n - 1];
      }

      no[nrLast - 1] = (d2[nrLast - 1] - c[nrLast - 1] * no[nrLast]) / b2[nrLast - 1];
      if (no[nrLast - 1] < 0D) {
        no[nrLast - 1] = 0D;
      }

      for (int i = 1; i < nr - 2; i++) { // For i = 2 To nr - 2
        final int n = (nrLast-1) - i;
        no[n] = d[n + 1] / a[n + 1] - b[n + 1] / a[n + 1] * no[n + 1] - c[n + 1] / a[n + 1] * no[n + 2];
        if (no[n] < 0D) {
          no[n] = 0D;
        }
      }

      no[0] = d[0] / b[0] - c[0] / b[0] * no[1];
      if (no[0] < 0D) {
        no[0] = 0D;
      }

      // Calculate NO2 concentration profile with height using finite difference model
      // Uses Gaussian elimination algorithm for tri diagonal matrix

      // NO2
      a[0] = 0D;
      b[0] = -1D / dr;
      c[0] = 1D / dr;
      d[0] = -eno2;
      b2[0] = b[0];
      d2[0] = d[0];

      for (int i = 0; i < nr; i++) { // For i = 1 To 11
        tempo[i] = no2[i];
      }

      for (int n = 1; n < nr - 1; n++) { // For n = 2 To nr - 1
        //an = n - 1D
        final double r = n * dr;
        final double z = z0 * Math.exp(ku * r);


        a[n] = alpha;
        b[n] = -2D * alpha - j * z;
        c[n] = alpha;
        d[n] = -k1 * z * o3[n] * no[n];
      }

      for (int n = 1; n < nr - 1; n++) { // For n = 2 To nr - 1
        b2[n] = b[n] - a[n] * c[n - 1] / b2[n - 1];
        d2[n] = d[n] - a[n] / b2[n - 1] * d2[n - 1];
      }

      no2[nrLast - 1] = (d2[nrLast - 1] - c[nrLast - 1] * no2[nrLast]) / b2[nrLast - 1];
      if (no2[nrLast - 1] < 0D) {
        no2[nrLast - 1] = 0D;
      }

      for (int i = 1; i < nr - 2; i++) { // For i = 2 To nr - 2
        final int n = (nrLast-1) - i;
        no2[n] = d[n + 1] / a[n + 1] - b[n + 1] / a[n + 1] * no2[n + 1] - c[n + 1] / a[n + 1] * no2[n + 2];
        if (no2[n] < 0D) {
          no2[n] = 0D;
        }
      }

      no2[0] = d[0] / b[0] - c[0] / b[0] * no2[1];
      if (no2[0] < 0D) {
        no2[0] = 0D;
      }

      // Calculate O3 concentration profile with height using finite differnce model
      // Uses Gaussian elimination algorithm for tri diagonal matrix

      // O3
      a[0] = 0D;
      b[0] = -1D / dr - 1D / rbrcO3;
      c[0] = 1D / dr;
      d[0] = 0;
      b2[0] = b[0];
      d2[0] = d[0];


      for (int n = 1; n < nr - 1; n++) { // For n = 2 To nr - 1
        //            an = n - 1D;
        final double r = n * dr;
        final double z = z0 * Math.exp(ku * r);

        a[n] = alpha;
        b[n] = -2D * alpha - k1 * z * no[n];
        c[n] = alpha;
        d[n] = -j * z * no2[n];
      }

      for (int n = 1; n < nr - 1; n++) { // For n = 2 To nr - 1
        b2[n] = b[n] - a[n] * c[n - 1] / b2[n - 1];
        d2[n] = d[n] - a[n] / b2[n - 1] * d2[n - 1];
      }

      o3[nrLast - 1] = (d2[nrLast - 1] - c[nrLast - 1] * o3[nrLast]) / b2[nrLast - 1];
      if (o3[nrLast - 1] < 0D) {
        o3[nrLast - 1] = 0D;
      }

      for (int i = 1; i < nr - 2; i++) { // For i = 2 To nr - 2
        final int n = (nrLast-1) - i;
        o3[n] = d[n + 1] / a[n + 1] - b[n + 1] / a[n + 1] * o3[n + 1] - c[n + 1] / a[n + 1] * o3[n + 2];
        if (o3[n] < 0D) {
          o3[n] = 0D;
        }
      }

      o3[0] = d[0] / b[0] - c[0] / b[0] * o3[1];
      if (o3[0] < 0D) {
        o3[0] = 0D;
      }

      // Convergence control.Checks that sum of absolute changes between iterations less than convergence criterion

      if (no2[0] > 0D) {
        double errorsum = 0D;
        for (int i = 0;i < nrLast; i++) { // For i = 1 To 10
          errorsum += Math.abs(no2[i] - tempo[i]) / LARGE_NUMBER * MG_M3_TO_PPM;
        }
        if (errorsum < 0.0001) {
          return new Gradient5Result(no2[0] / LARGE_NUMBER, iter);
        }
      }
    } // Next iter

    return new Gradient5Result(no2[0] / LARGE_NUMBER, NITER);
  }

  static class Gradient5Result {
    private final double no2;
    private final double iter;

    public Gradient5Result(final double no2, final double iter) {
      this.no2 = no2;
      this.iter = iter;
    }

    public double getNO2() {
      return no2;
    }

    public double getIter() {
      return iter;
    }
  }
}
