/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.backgrounddata;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData;
import nl.aerius.adms.io.GeoTiffGridDataFileWriter;
import nl.aerius.adms.io.MetFileStore;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.backgrounddata.DepositionVelocityMap;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Object to store background data loaded to be used with ADMS.
 */
public class ADMSData {

  private final Map<ADMSVersion, Map<Substance, DepositionVelocityMap>> depositionVelocities = new EnumMap<>(ADMSVersion.class);
  private final Map<ADMSVersion, GeoTiffGridDataFileWriter> surfaceRoughness = new EnumMap<>(ADMSVersion.class);
  private final Map<ADMSVersion, Map<Integer, GeoTiffGridDataFileWriter>> terrainHeight = new EnumMap<>(ADMSVersion.class);
  private final Map<ADMSVersion, Map<Substance, GeoTiffGridDataFileWriter>> plumeDepletion = new EnumMap<>(ADMSVersion.class);
  private final Map<ADMSVersion, NOxToNO2CalculatorData> noxToNO2CalculatorData = new EnumMap<>(ADMSVersion.class);
  private final Map<ADMSVersion, MetFileStore> metFileStores = new EnumMap<>(ADMSVersion.class);

  public void addDepositionVelocityMap(final ADMSVersion admsVersion, final Map<Substance, DepositionVelocityMap> depositionVelocityMap) {
    this.depositionVelocities.put(admsVersion, depositionVelocityMap);
  }

  public boolean containsDepositionVelocityMap(final ADMSVersion admsVersion) {
    return depositionVelocities.containsKey(admsVersion);
  }

  public Map<Substance, DepositionVelocityMap> getDepositionVelocityMap(final ADMSVersion admsVersion) {
    return depositionVelocities.get(admsVersion);
  }

  public void addSurfaceRoughnessWriter(final ADMSVersion admsVersion, final GeoTiffGridDataFileWriter surfaceRoughnessWriter) {
    surfaceRoughness.put(admsVersion, surfaceRoughnessWriter);
  }

  public boolean containsSurfaceRoughnessWriter(final ADMSVersion admsVersion) {
    return surfaceRoughness.containsKey(admsVersion);
  }

  public GeoTiffGridDataFileWriter getSurfaceRoughnessWriter(final ADMSVersion admsVersion) {
    return surfaceRoughness.get(admsVersion);
  }

  public void addTerrainHeightWriter(final ADMSVersion admsVersion, final Integer resolution, final GeoTiffGridDataFileWriter complexTerrainWriter) {
    terrainHeight.computeIfAbsent(admsVersion, k -> new HashMap<>()).put(resolution, complexTerrainWriter);
  }

  public boolean containsTerrainHeightWriter(final ADMSVersion admsVersion, final Integer resolution) {
    return terrainHeight.containsKey(admsVersion) && terrainHeight.get(admsVersion).containsKey(resolution);
  }

  public GeoTiffGridDataFileWriter getTerrainHeightWriter(final ADMSVersion admsVersion, final Integer resolution) {
    return terrainHeight.get(admsVersion).get(resolution);
  }

  public void addPlumeDepletionWriter(final ADMSVersion admsVersion, final Substance substance,
      final GeoTiffGridDataFileWriter plumeDepletionWriter) {
    plumeDepletion.computeIfAbsent(admsVersion, (version) -> new EnumMap<>(Substance.class)).put(substance, plumeDepletionWriter);
  }

  public boolean containsPlumeDepletionWriters(final ADMSVersion admsVersion, final Substance substance) {
    return plumeDepletion.containsKey(admsVersion) && plumeDepletion.get(admsVersion).containsKey(substance);
  }

  public final GeoTiffGridDataFileWriter getPlumeDepletionWriter(final ADMSVersion admsVersion, final Substance substance) {
    return plumeDepletion.computeIfAbsent(admsVersion, (version) -> new EnumMap<>(Substance.class)).get(substance);
  }

  public void addMetFileStore(final ADMSVersion admsVersion, final MetFileStore metFileStore) {
    this.metFileStores.put(admsVersion, metFileStore);
  }

  public boolean containsMetFileStore(final ADMSVersion admsVersion) {
    return metFileStores.containsKey(admsVersion);
  }

  public MetFileStore getMetFileStore(final ADMSVersion admsVersion) {
    return metFileStores.get(admsVersion);
  }

  public void addNOxToNO2CalculatorData(final ADMSVersion admsVersion, final NOxToNO2CalculatorData noxToNO2CalculatorData) {
    this.noxToNO2CalculatorData.put(admsVersion, noxToNO2CalculatorData);
  }

  public boolean containsNOxToNO2CalculatorData(final ADMSVersion admsVersion) {
    return noxToNO2CalculatorData.containsKey(admsVersion);
  }

  public NOxToNO2CalculatorData getNOxToNO2CalculatorData(final ADMSVersion admsVersion) {
    return noxToNO2CalculatorData.get(admsVersion);
  }
}
