/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.math.BigDecimal;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Calculator to calculate deposition from concentration.
 */
public final class ADMSDepositionCalculator {

  // Conversion factor for eutrophication (final nitrogen deposition) in (µg/m3 to kg N/ha/yr)
  private static final BigDecimal EUTROPHICATION_FACTOR_NO2 = BigDecimal.valueOf(95.9);
  private static final BigDecimal EUTROPHICATION_FACTOR_NH3 = BigDecimal.valueOf(260);
  private static final Map<Substance, BigDecimal> EUTROPHICATION_FACTOR_MAP = Map.of(
      Substance.NO2, EUTROPHICATION_FACTOR_NO2,
      Substance.NH3, EUTROPHICATION_FACTOR_NH3);

  // Conversion factor for acidification (acid deposition) (µg/m3 to keq/ha/yr)
  private static final BigDecimal ACIDIFICATION_FACTOR_NO2 = BigDecimal.valueOf(6.84);
  private static final BigDecimal ACIDIFICATION_FACTOR_NH3 = BigDecimal.valueOf(18.5);
  private static final Map<Substance, BigDecimal> ACIDIFICATION_FACTOR_MAP = Map.of(
      Substance.NO2, ACIDIFICATION_FACTOR_NO2,
      Substance.NH3, ACIDIFICATION_FACTOR_NH3);

  private ADMSDepositionCalculator() {
    // Util class
  }

  /**
   * Calculates nutrient nitrogen deposition
   *
   * @param substance substance
   * @param concentration concentration in ug/m3
   * @return nutrient nitrogen deposition
   */
  public static double calculateNutrientNitrogenDeposition(final Substance substance, final double depositionVelocity,
      final double concentration) {
    return compute(concentration, depositionVelocity, EUTROPHICATION_FACTOR_MAP, substance);
  }

  /**
   * Calculates acid deposition
   *
   * @param substance substance
   * @param concentration concentration in ug/m3
   * @return acid deposition
   */
  public static double calculateAcidDeposition(final Substance substance, final double depositionVelocity, final double concentration) {
    return compute(concentration, depositionVelocity, ACIDIFICATION_FACTOR_MAP, substance);
  }

  private static double compute(final double concentration, final double depositionVelocity, final Map<Substance, BigDecimal> depositionFactorMap,
      final Substance substance) {
    if (!depositionFactorMap.containsKey(substance)) {
      return 0.0;
    } else {
      return BigDecimal.valueOf(concentration)
          .multiply(BigDecimal.valueOf(depositionVelocity))
          .multiply(depositionFactorMap.get(substance)).doubleValue();
    }
  }
}
