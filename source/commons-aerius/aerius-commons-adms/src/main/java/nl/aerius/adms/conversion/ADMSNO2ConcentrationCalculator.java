/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NO2_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_CONCENTRATION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_DEPOSITION;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

import nl.aerius.adms.conversion.nox2.NOxToNO2Calculator;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSPointNO2Data;
import nl.aerius.adms.domain.NCACalculationResultPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.util.AeriusPointUtil;

/**
 * Computes the NO2 concentration given the NOX2 concentration resutls.
 */
public final class ADMSNO2ConcentrationCalculator {
  private static final BigDecimal NOX_CONVERSION_FACTOR = BigDecimal.valueOf(0.7);

  private ADMSNO2ConcentrationCalculator() {
    // Util class
  }

  /**
   * Adds {@link ADMSPointNO2Data} to all result points if NO2 concentration or NOx deposition is to be calculated.
   * 
   * @param no2ToNO2CalculatorData
   * @param year
   * @param results
   * @param resultsKeys
   * @param calculationPoints
   */
  public static void enrich(final NOxToNO2CalculatorData no2ToNO2CalculatorData, final int year, final List<NCACalculationResultPoint> results,
      final Set<EmissionResultKey> resultsKeys, final Collection<ADMSCalculationPoint> calculationPoints) {
    if (resultsKeys.contains(NO2_CONCENTRATION) || resultsKeys.contains(NOX_DEPOSITION)) {
      final Map<String, ADMSCalculationPoint> calculationPointsMap = calculationPoints.stream()
          .collect(Collectors.toMap(AeriusPointUtil::encodeName, Function.identity()));

      for (final NCACalculationResultPoint result : results) {
        result.setNO2BackgroundData(
            NOxToNO2Calculator.createADMSPointNO2Data(no2ToNO2CalculatorData, year, calculationPointsMap.get(AeriusPointUtil.encodeName(result))));
      }
    }
  }

  /**
  /**
   * Computes the NO2 concentration as 70% of NOX or if road sources use the NOx to NO2 calcualtor if NOX concentration should be in results data.
   *
   * @param groupKey type of the results
   * @param resultsKeys results key of which results should be present in the results data
   * @param results results to compute NO2 concentration
   */
  public static void computeNO2Concentrations(final ADMSGroupKey groupKey, final Set<EmissionResultKey> resultsKeys,
      final List<NCACalculationResultPoint> results) {
    if (resultsKeys.contains(NO2_CONCENTRATION) || resultsKeys.contains(NOX_DEPOSITION)) {
      final ToDoubleFunction<NCACalculationResultPoint> function = ADMSGroupKey.NOX_ROAD == groupKey
          ? ADMSNO2ConcentrationCalculator::calculatorRoadNO2
          : ADMSNO2ConcentrationCalculator::calculateDefaultNO2;

      results.stream().forEach(r -> r.setEmissionResult(NO2_CONCENTRATION, function.applyAsDouble(r)));
    }
  }

  private static Double calculatorRoadNO2(final NCACalculationResultPoint r) {
    final NOxToNO2Calculator.NO2Results result = NOxToNO2Calculator.calculateNOxToNO2(r.getNO2BackgroundData(),
        r.getEmissionResult(NOX_CONCENTRATION));

    return result == null ? calculateDefaultNO2(r) : result.getRoad();
  }

  private static double calculateDefaultNO2(final AeriusResultPoint r) {
    return BigDecimal.valueOf(r.getEmissionResult(NOX_CONCENTRATION)).multiply(NOX_CONVERSION_FACTOR).doubleValue();
  }
}
