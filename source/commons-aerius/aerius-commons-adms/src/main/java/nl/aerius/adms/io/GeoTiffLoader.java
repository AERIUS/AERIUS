/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;

import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.geotools.referencing.CRS;
import org.geotools.util.factory.Hints;
import org.opengis.parameter.GeneralParameterValue;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

/**
 * Utility class to load GeoTiff file into a {@link GeoTiffGridDataFileWriter} object.
 */
public class GeoTiffLoader {

  /**
   * Loads a GeotTiff file into a {@link GeoTiffGridDataFileWriter} object.
   *
   * @param epsg EPSG of the GeoTiff
   * @param file GeoTiff file
   * @return {@link GeoTiffGridDataFileWriter} object with GeoTiff raster loaded.
   * @throws IOException
   * @throws FactoryException
   */
  public static GeoTiffGridDataFileWriter loadRaster(final String epsg, final File file) throws IOException, FactoryException {
    final CoordinateReferenceSystem decode = CRS.decode(epsg);
    final Hints hints = new Hints(Hints.CRS, decode);
    final GeoTiffReader reader = new GeoTiffReader(file, hints);
    final GridCoverage2D grid = reader.read(new GeneralParameterValue[0]);
    final Raster raster = grid.getRenderedImage().getData();

    return new GeoTiffGridDataFileWriter(raster, grid.getEnvelope2D());
  }
}
