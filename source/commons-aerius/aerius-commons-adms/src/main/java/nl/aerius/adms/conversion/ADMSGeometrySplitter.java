/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;

/**
 * If a geometry is too big for ADMS to calculate it's split into smaller geometries.
 */
final class ADMSGeometrySplitter {

  private static final int MAX_LINE_VERTICES = 51;

  private ADMSGeometrySplitter() {
    // Util class
  }

  /**
   * When a geometry (line string) has to many vertices for ADMS to calculate the line string is split into line strings that contain the allowed
   * maximum number of vertices.
   *
   * @param geometry geometry to check
   * @return returns list of geometry or split geometries.
   */
  public static List<Geometry> split(final Geometry geometry) {
    final List<Geometry> geometries;

    if (geometry instanceof final LineString line && line.getCoordinates().length > MAX_LINE_VERTICES) {
      geometries = splitLineString((LineString) geometry);
    } else {
      geometries = List.of(geometry);
    }
    return geometries;
  }

  private static List<Geometry> splitLineString(final LineString geometry) {
    final List<Geometry> geometries = new ArrayList<>();
    final int length = geometry.getCoordinates().length;

    for (int i = 0; i < length; i+= MAX_LINE_VERTICES  - 1) {
      final LineString ls = new LineString();

      ls.setCoordinates(Arrays.copyOfRange(geometry.getCoordinates(), i, Math.min(length, i + MAX_LINE_VERTICES)));
      geometries.add(ls);
    }
    return geometries;
  }
}
