/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion.nox2;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.io.AbstractLineColumnReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Reader to read the local authority background concentration data.
 */
class LABackgroundReader extends AbstractLineColumnReader<Void> {

  private static final String UWE_CODE = "uwe_code";
  private static final int UWE_CODE_INDEX = 1;
  private static final String YEARS_PREFIX = "reg";
  private static final int YEARS_FIRST_INDEX = 2;

  private static final String SPLIT_PATTERN = ",";
  private static final int HEADER_ROWS = 1;

  private final NOxToNO2CalculatorData backgroundData;
  private final String yearColumnPrefix;
  private final Substance substance;

  /**
   * Constructor.
   *
   * @param substance substance that is read
   * @param backgroundData data object to store the read data in
   */
  public LABackgroundReader(final Substance substance, final NOxToNO2CalculatorData backgroundData) {
    super(SPLIT_PATTERN);
    this.substance = substance;
    this.backgroundData = backgroundData;
    yearColumnPrefix = YEARS_PREFIX + substance.getName().toLowerCase(Locale.ROOT);
  }

  @Override
  public LineReaderResult<Void> readObjects(final InputStream inputStream) throws IOException {
    try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8.name())) {
      return super.readObjects(reader, HEADER_ROWS);
    }
  }

  @Override
  protected Void parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final Long uweCode = Long.valueOf(getInt(UWE_CODE, UWE_CODE_INDEX));
    final int firstYear = backgroundData.getVersion().getLACodeFirstYear();
    final int yearIndexOffset = firstYear - YEARS_FIRST_INDEX;

    for (int year = firstYear; year <= backgroundData.getVersion().getLACodeLastYear(); year++) {
      backgroundData.putBackground(uweCode, year, substance, getDouble(yearColumnPrefix + year, year - yearIndexOffset));
    }
    return null;
  }
}
