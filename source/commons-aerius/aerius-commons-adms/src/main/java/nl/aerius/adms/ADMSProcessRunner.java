/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import nl.aerius.adms.backgrounddata.ADMSData;
import nl.aerius.adms.conversion.ADMSPostProcessor;
import nl.aerius.adms.conversion.EnrichResultProcessor;
import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.domain.NCACalculationResultPoint;
import nl.aerius.adms.io.ErrorFileChecker;
import nl.aerius.adms.io.PltFileReader;
import nl.aerius.adms.util.ADMSProgressCollector;
import nl.aerius.adms.util.ADMSUtil;
import nl.aerius.adms.version.ADMSVersion;
import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.exec.ProcessListener;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Class to construct input for ADMS and start the tool and return the output.
 */
class ADMSProcessRunner implements ADMSRunner<List<NCACalculationResultPoint>> {

  private static final String EXIT_MODE = OSUtils.isWindows() ? "/e2" : "-E2";

  private static final Base64.Decoder BASE64_DECODER = Base64.getDecoder();

  private final ExecutorService executor;
  private final ADMSConfiguration configuration;
  private final ADMSExecutor admsExecutor;
  private final ADMSData admsData;
  private final EnrichResultProcessor enricher;

  public ADMSProcessRunner(final ExecutorService executor, final ADMSConfiguration configuration, final ADMSData admsData,
      final ADMSVersion admsVersion) throws Exception {
    this.executor = executor;
    this.configuration = configuration;
    this.admsData = admsData;
    admsExecutor = new ADMSExecutor(admsVersion, configuration.getADMSRoot(admsVersion));
    startupRun();
    enricher = new EnrichResultProcessor(admsData);
  }

  /**
   * Runs ADMS with no arguments to check for valid installation (version, license).
   *
   * @throws Exception Exception thrown in case of invalid version, license
   */
  private void startupRun() throws Exception {
    byte[] license = null;
    final Optional<String> base64License = configuration.getAdmsLicenseBase64();
    if (base64License.isPresent()) {
      license = BASE64_DECODER.decode(base64License.get());
    }
    admsExecutor.run(configuration.createOutputDirectory(), List.of(EXIT_MODE), license, line -> {
    }, null);
  }

  public CalculationResult run(final ADMSInputData input, final WorkerIntermediateResultSender workerIntermediateResultSender,
      final ProcessListener processListener) throws Exception {
    final ProcessADMS<List<NCACalculationResultPoint>> processor = new ProcessADMS<>(executor, this, admsData, configuration, true);
    final CalculationResult cr = new CalculationResult(input.getChunkStats(), input.getEngineDataKey());
    final ResultCollector resultsCollector = new ResultCollector();
    try (final ADMSProgressCollector progressCollector = new ADMSProgressCollector(workerIntermediateResultSender,
        configuration.getProgressReportIntervalSeconds(), input)) {
      processor.run(input, resultsCollector::add, progressCollector, processListener, OSUtils.NL);
      final EnumSet<EmissionResultKey> erks = input.getEmissionResultKeys();

      resultsCollector.getResultsMap().forEach((k, v) -> cr.put(k.getIndex(), new ArrayList<>(postprocess(input, k, erks, v.get(0)))));
    }
    return cr;
  }

  @Override
  public List<NCACalculationResultPoint> run(final ADMSProgressCollector progressCollector, final ProcessListener processListener,
      final ADMSInputData input, final ADMSGroupKey groupKey, final File outputDirectory, final File outputFile, final File projectFile)
          throws Exception {
    admsExecutor.run(outputDirectory, List.of(projectFile.getAbsolutePath(), EXIT_MODE), input.getLicense(),
        progressCollector.registerProgressConsumer(), processListener);

    ErrorFileChecker.checkErrors(outputFile);
    ErrorFileChecker.checkWarnings(outputFile);

    final EnumSet<EmissionResultKey> erks = input.getEmissionResultKeys();
    final Set<EmissionResultKey> concentrationKeys = ADMSUtil.filterKeys(erks, EmissionResultType.CONCENTRATION, Collectors.toSet());

    return readOutputResults(outputFile, concentrationKeys);
  }

  private List<NCACalculationResultPoint> postprocess(final ADMSInputData input, final ADMSGroupKey groupKey, final EnumSet<EmissionResultKey> erks,
      final List<NCACalculationResultPoint> results) {
    enricher.enrichResults(input, groupKey, results);
    return ADMSPostProcessor.postprocess(groupKey, erks, results);
  }

  private static List<NCACalculationResultPoint> readOutputResults(final File outputFile, final Set<EmissionResultKey> keys)
      throws IOException, AeriusException {
    final PltFileReader reader = new PltFileReader(keys);
    final LineReaderResult<NCACalculationResultPoint> results = reader.read(outputFile);

    if (results.getExceptions().isEmpty()) {
      return results.getObjects();
    }
    throw results.getExceptions().get(0);
  }

  private static class ResultCollector {
    private final Map<ADMSGroupKey, List<List<NCACalculationResultPoint>>> resultsMap = new EnumMap<>(ADMSGroupKey.class);

    public synchronized void add(final ADMSGroupKey key, final List<NCACalculationResultPoint> results) {
      resultsMap.computeIfAbsent(key, k -> new ArrayList<>()).add(results);
    }

    public Map<ADMSGroupKey, List<List<NCACalculationResultPoint>>> getResultsMap() {
      return resultsMap;
    }
  }
}
