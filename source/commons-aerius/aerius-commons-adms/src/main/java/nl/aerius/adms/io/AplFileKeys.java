/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileConstants.NOT_ACTIVE;

import nl.aerius.adms.io.AplFileConstants.KeyWithDefault;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Keys for parameters in .apl/.upl files.
 */
public enum AplFileKeys implements KeyWithDefault {
  // @formatter:off

  // ADMS_COORDINATESYSTEM
  PROJECTED_EPSG("ProjectedEPSG", EPSG.BNG.getSrid()),

  // ADMS_PARAMETERS_SUP

  SUP_SITE_NAME("SupSiteName", ""),
  SUP_CALC_DRY_DEP("SupCalcDryDep", NOT_ACTIVE),

  SUP_PROJECT_NAME("SupProjectName", ""),

  // ADMS_PARAMETERS_MET

  MET_SUBSET_YEAR_START("MetSubsetYearStart", 0),

  MET_DS_SURFACE_ALBEDO_MODE("Met_DS_SurfaceAlbedoMode", 0),
  MET_DS_SURFACE_ALBEDO("Met_DS_SurfaceAlbedo", ADMSLimits.SURFACE_ALBEDO_DEFAULT),
  MET_DS_PRIESTLY_TAYLOR_MODE("Met_DS_PriestlyTaylorMode", 0),
  MET_DS_PRIESTLY_TAYLOR("Met_DS_PriestlyTaylor", ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_DEFAULT),
  MET_DS_MIN_LMO_MODE("Met_DS_MinLmoMode", 0),
  MET_DS_MIN_LMO("Met_DS_MinLmo", ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT),

  MET_MS_ROUGHNESS_MODE("Met_MS_RoughnessMode", 3),
  MET_MS_ROUGHNESS("Met_MS_Roughness", ADMSLimits.ROUGHNESS_DEFAULT),
  MET_MS_SURFACE_ALBEDO_MODE("Met_MS_SurfaceAlbedoMode", 3),
  MET_MS_SURFACE_ALBEDO("Met_MS_SurfaceAlbedo", ADMSLimits.SURFACE_ALBEDO_DEFAULT),
  MET_MS_PRIESTLY_TAYLOR_MODE("Met_MS_PriestlyTaylorMode", 3),
  MET_MS_PRIESTLY_TAYLOR("Met_MS_PriestlyTaylor", ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_DEFAULT),
  MET_MS_MIN_LMO_MODE("Met_MS_MinLmoMode", 3),
  MET_MS_MIN_LMO("Met_MS_MinLmo", ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT),

  // ADMS_PARAMETERS_BLD

  /**
   * Number of buildings.
   */
  BLD_NUM_BUILDINGS("BldNumBuildings", 0),
  /**
   * Names of buildings.
   */
  BLD_NAME("BldName", ""),
  /**
   * Type of building (0=rectangular, 1 = circular).
   */
  BLD_TYPE("BldType", 0),
  /**
   * X coordinate of building centre.
   */
  BLD_X("BldX", 0.0),
  /**
   * Y coordinate of building centre.
   */
  BLD_Y("BldY", 0.0),
  /**
   * height of buildings (m).
   */
  BLD_HEIGHT("BldHeight", 0.0),
  /**
   * Length of buildings (m).
   */
  BLD_LENGTH("BldLength", 0.0),
  /**
   * Width of rectangular building (m), set to same as length for circular building.
   */
  BLD_WIDTH("BldWidth", 0.0),
  /**
   * Angle of rectangular building, set to 0 for circular building.
   */
  BLD_ANGLE("BldAngle", 0.0),

  // ADMS_POLLUTANT_DETAILS

  POL_NAME("PolName", ""),

  // ADMS_PARAMETERS_OPT
  /**
   * Number of pollutants being output.
   */
  OPT_NUM_OUTPUTS("OptNumOutputs", 0),
  /**
   * Name of pollutants being output.
   */
  OPT_POL_NAME("OptPolName", null),
  /**
   * Set to 1 to actually output the pollutant.
   */
  OPT_INCLUDE("OptInclude", 1),
  /**
   * Set to 1 to get period average.
   */
  OPT_SHORT_OR_LONG("OptShortOrLong", 1),
  /**
   * Set to 1.0e+0 to indicate averaging hourly data.
   */
  OPT_SAMPLING_TIME("OptSamplingTime", 1.0),
  /**
   * Set to 3 to indicate averaging hourly data.
   */
  OPT_SAMPLING_TIME_UNITS("OptSamplingTimeUnits", 3),
  /**
   * Set to 0 to indicate no additional condition.
   */
  OPT_CONDITION("OptCondition", 0),
  /**
   * Set to 0 as not calculating percentiles
   */
  OPT_NUM_PERCENTILES("OptNumPercentiles", 0),
  /**
   * Set to 0 as not calculating exceedences.
   */
  OPT_NUM_EXCEEDENCES("OptNumExceedences", 0),
  /**
   * ?
   */
  OPT_PERCENTILES("OptPercentiles", null),
  /**
   * ?
   */
  OPT_EXCEEDENCES("OptExceedences", null),
  /**
   * Units of output.
   */
  OPT_UNITS("OptUnits", null),
  /**
   * Percentage of hours required to be valid for average to be considered valid.
   */
  OPT_VALIDITY("OptValidity", 7.5e+1),
  /**
   * Set to 0 to indicate group output
   */
  OPT_GROUPS_OR_SOURCE("OptGroupsOrSource", 0),
  /**
   * Set to 1 to output a group containing all sources.
   */
  OPT_ALL_SOURCES("OptAllSources", 1),
  /**
   * Set to 0 as no other groups to be output.
   */
  OPT_NUM_GROUPS("OptNumGroups", 0),
  /**
   * Set to a valid source name.
   */
  OPT_INCLUDED_SOURCE("OptIncludedSource", ""),
  /**
   * Set to 0, not creating comprehensive output file.
   */
  OPT_CREATE_COMPREHENSIVE_FILE("OptCreateComprehensiveFile", 0),
  /**
   * Set to 0, not outputting per source.
   */
  OPT_OUTPUT_PER_SOURCE("OptOutputPerSource", 0),

  // ADMS_SOURCE_DETAILS
  /**
   * Source name.
   */
  SRC_NAME("SrcName", ""),
  /**
   * Choice of main building – see ADMS-Urban user guide for what this means.
   */
  SRC_MAIN_BUILDING("SrcMainBuilding", "(None)"),
  /**
   * Hheight of source (m).
   */
  SRC_HEIGHT("SrcHeight", 0.0),
  /**
   * Diameter of point source (m).
   */
  SRC_DIAMETER("SrcDiameter", 0.0),
  /**
   * Volume flow rate for source (m3/s), used if SrcEffluxType=1, otherwise leave as default.
   */
  SRC_VOLUMETRIC_FLOW_RATE("SrcVolFlowRate", 1.1781e+1),
  /**
   * Exit velocity of source (m/s), used if SrcEffluxType=0, otherwise leave as default.
   */
  SRC_VERTICAL_VELOCITY("SrcVertVeloc", 1.5e+1),
  /**
   * Exit temperature of the source ( o C), used if SrcBuoyancyType=0 otherwise leave as default
   */
  SRC_TEMPERATURE("SrcTemperature", 1.5e+1),
  /**
   * Overall molecular weight of material leaving source, should be fine to leave as default (i.e. air).
   */
  SRC_MOL_WEIGHT("SrcMolWeight", 2.8966e+1),
  /**
   * Density of source, used if SrcBuoyancyType=1, otherwise leave as default.
   */
  SRC_DENSITY("SrcDensity", 1.225e+0),
  /**
   * Specific heat capacity of material leaving source, should be fine to leave as default.
   */
  SRC_SPECIFIC_HEAT_CAPACITY("SrcSpecHeatCap", 1.012e+3),
  /**
   * Point source = 0, Area source = 1, Volume source = 2, Line source = 3, road source = 4.
   */
  SRC_SOURCE_TYPE("SrcSourceType", 0),
  /**
   * Emission parameters given at actual temperature and pressure or normal temperature and pressure (1atm, 273.15K): 0=actual,1=NTP.
   */
  SRC_RELEASE_AT_NTP("SrcReleaseAtNTP", 0),
  /**
   * Efflux specified as: 0=velocity,1=volume flow rate.
   */
  SRC_EFFLUX_TYPE("SrcEffluxType", 0),
  /**
   * Buoyancy specified as: 0=temperature, 1=density.
   */
  SRC_BUOYANCY_TYPE("SrcBuoyancyType", 0),
  /**
   *
   */
  SRC_PERCENT_NOX_AS_NO2("SrcPercentNOxAsNO2", 0.0),
  /**
   * X coordinate of point source.
   */
  SRC_X1("SrcX1", 0.0),
  /**
   * Y coordinate of point source.
   */
  SRC_Y1("SrcY1", 0.0),
  /**
   * Width of line/road source, depth of volume source.
   */
  SRC_L1("SrcL1", 1.0),
  /**
   * Canyon height for basic street canyons.
   */
  SRC_L2("SrcL2", 1.0),
  SRC_FM("SrcFm", 0.0),
  SRC_FB("SrcFb", 0.0),

  SRC_MASS_FLUX("SrcMassFlux", 0.0),
  SRC_ANGLE_1("SrcAngle1", 0.0),
  SRC_ANGLE_2("SrcAngle2", 0.0),
  SRC_MASS_H2O("SrcMassH2O", 0.0),
  SRC_USE_VAR_FILE("SrcUseVARFile", 0),

  /**
   * Set to 0, just creating 1 group containing all sources.
   */
  SRC_NUM_GROUPS("SrcNumGroups", 0),
  /**
   * 0 = from user-specified traffic flows, 1 = user-defined emission rates
   */
  SRC_TRA_EMISSIONS_MODE("SrcTraEmissionsMode", 1),
  SRC_TRA_YEAR("SrcTraYear", 0),
  SRC_TRA_ROAD_TYPE("SrcTraRoadType", "London (central)"),
  SRC_TRA_GRADIENT("SrcTraGradient", 0.0),
  /**
   * Number of vertices, set to 0 for point source.
   */
  SRC_NUM_VERTICES("SrcNumVertices", 0),
  /**
   * Number of traffic flows, set to 0 for point source.
   */
  SRC_TRA_NUM_TRAFFIC_FLOWS("SrcTraNumTrafficFlows", 0),
  /**
   * Number of emitted pollutants
   */
  SRC_NUM_POLLUTANTS("SrcNumPollutants", 0),
  /**
   * Names of emitted pollutants.
   */
  SRC_POLLUTANTS("SrcPollutants", ""),
  /**
   * Emission rate of emitted pollutants (g/s for point source).
   */
  SRC_POL_EMISSION_RATE("SrcPolEmissionRate", 0.0),
  /**
   * Set to default (entry per pollutant), related to non-plume releases.
   */
  SRC_POL_TOTALEMISSION("SrcPolTotalemission", 1.0),
  /**
   * Set to default (entry per pollutant), related to non-plume releases.
   */
  SRC_POL_START_TIME("SrcPolStartTime", 0.0),
  /**
   * Set to default (entry per pollutant), related to non-plume releases.
   */
  SRC_POL_DURATION("SrcPolDuration", 0.0),
  /**
   * Set to 0, not modelling isotopes
   */
  SRC_NUM_ISOTOPES("SrcNumIsotopes", 0),
  /**
   * X-coordinate of vertex point
   */
  SOURCE_VERTEX_X("SourceVertexX", 0.0),
  /**
   * Y-coordinate of vertex point
   */
  SOURCE_VERTEX_Y("SourceVertexY", 0.0);


  // @formatter:on

  private final String key;
  private final Object defaultValue;

  AplFileKeys(final String key, final Object defaultValue) {
    this.key = key;
    this.defaultValue = defaultValue;
  }

  @Override
  public String getKey() {
    return key;
  }

  @Override
  public Object getValueOrDefault(final Object value) {
    return value == null ? defaultValue : value;
  }

  @Override
  public Object getDefaultValue() {
    return defaultValue;
  }

}
