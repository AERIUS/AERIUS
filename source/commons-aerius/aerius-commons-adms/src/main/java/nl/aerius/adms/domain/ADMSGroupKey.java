/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import java.util.List;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Key to split sources into different groups.
 */
public enum ADMSGroupKey {
  /**
   * When calculating NH3, no traffic-induced turbulence will be added, which is inconsistent with the NOx calculation for roads.
   * Therefore include the NOx emission in the NH3 upl when calculating road sources.
   */
  NH3(1, List.of(Substance.NH3), List.of(Substance.NH3, Substance.NOX)),
  NOX_OTHER(2, Substance.NOX),
  NOX_ROAD(3, Substance.NOX /*, Substance.NO2 */),
  /**
   * Group that can be used when no plume depletion is needed.
   */
  NOX_NH3(4, Substance.NOX, Substance.NH3);

  private final int index;
  private final List<Substance> outputSubstances;
  private final List<Substance> roadSubstances;

  ADMSGroupKey(final int index, final Substance... substances) {
    this(index, List.of(substances), List.of(substances));
  }

  ADMSGroupKey(final int index, final List<Substance> substances, final List<Substance> roadSubstances) {
    this.index = index;
    this.outputSubstances = substances;
    this.roadSubstances = roadSubstances;
  }

  public static ADMSGroupKey safeValueOf(final int index) {
    for (final ADMSGroupKey key : values()) {
      if (key.getIndex() == index) {
        return key;
      }
    }
    return null;
  }

  public int getIndex() {
    return index;
  }

  public List<Substance> getOutputSubstances() {
    return outputSubstances;
  }

  public List<Substance> getEmissionSubstances(final boolean roadSource) {
    return roadSource ? roadSubstances : outputSubstances;
  }
}
