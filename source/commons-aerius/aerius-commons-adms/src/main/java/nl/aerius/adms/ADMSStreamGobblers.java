/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.aerius.adms.exception.ADMSInvalidLicense;
import nl.aerius.adms.exception.ADMSInvalidVersionException;
import nl.overheid.aerius.exec.ExecuteWrapper.StreamGobblers;
import nl.overheid.aerius.exec.StreamGobbler;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.OSUtils;

/**
 * Class to check if the ADMS version matches and if the license is valid.
 */
class ADMSStreamGobblers implements StreamGobblers {

  private enum VersionCheck {
    VERSION,
    BUILD
  }

  private static final String VERSION_SEPARATOR = "-";
  private static final Pattern VERSION_PATTERN = Pattern.compile("Version ([\\d\\.]+)");
  private static final Pattern BUILD_VERSION_PATTERN = Pattern.compile("Build number ([\\d\\.]+)");
  private static final String INVALID_LICENSE_STRING = " ERROR  : Your licence is invalid. This program will terminate.";
  private static final String ERROR_LINE = " Error ";
  /**
   * Output when ADMS crashes with a core dump.
   */
  private static final String FORRTL_LINE = "forrtl";

  private final List<Exception> exceptions = new ArrayList<>();
  private final String admsVersion;
  private final String buildVersion;
  private final Set<VersionCheck> versionInfoFound = EnumSet.noneOf(VersionCheck.class);
  private final Consumer<String> additionalStreamLogger;

  /**
   *
   * @param admsFullVersion
   * @param additionalStreamLogger Optional additional stream logger to which output will be passed, set to null if not used
   */
  public ADMSStreamGobblers(final String admsFullVersion, final Consumer<String> additionalStreamLogger) {
    this.additionalStreamLogger = additionalStreamLogger;
    final String[] split = admsFullVersion.split(VERSION_SEPARATOR);

    if (split.length == 2) {
      this.admsVersion = split[0];
      this.buildVersion = split[1];
    } else {
      this.admsVersion = admsFullVersion;
      this.buildVersion = null;
    }
  }

  @Override
  public StreamGobbler errorStreamGobbler(final String type, final String parentId) {
    return new StreamGobbler(type, parentId, this::logErrorOuput);
  }

  @Override
  public StreamGobbler outputStreamGobbler(final String type, final String parentId) {
    return new StreamGobbler(type, parentId, this::logErrors);
  }

  public List<Exception> getExceptions() {
    if (exceptions.isEmpty() && versionInfoFound.size() != 2 && !OSUtils.isWindows()) {
      exceptions.add(new ADMSInvalidVersionException(admsVersion, ": no version information found"));
    }
    return exceptions;
  }

  private boolean logErrors(final String line) {
    if (versionError(line, VERSION_PATTERN, admsVersion, VersionCheck.VERSION)) {
      return true;
    }
    if (buildVersion != null && versionError(line, BUILD_VERSION_PATTERN, buildVersion, VersionCheck.BUILD)) {
      return true;
    }
    if (INVALID_LICENSE_STRING.equals(line)) {
      exceptions.add(new ADMSInvalidLicense(line));
      return true;
    }
    if (additionalStreamLogger != null) {
      additionalStreamLogger.accept(line);
    }
    // log any line that starts with Error
    return line.startsWith(ERROR_LINE);
  }

  private boolean logErrorOuput(final String line) {
    if (line.startsWith(FORRTL_LINE)) {
      exceptions.add(new AeriusException(AeriusExceptionReason.ADMS_INTERNAL_EXCEPTION, line));
      return true;
    }
    return false;
  }

  private boolean versionError(final String line, final Pattern pattern, final String version, final VersionCheck versionCheck) {
    final Matcher matcher = pattern.matcher(line);

    if (matcher.find()) {
      versionInfoFound.add(versionCheck);
      if (!version.equals(matcher.group(1))) {
        exceptions.add(new ADMSInvalidVersionException(admsVersion, matcher.group(1)));
        return true;
      }
    }
    return false;
  }
}
