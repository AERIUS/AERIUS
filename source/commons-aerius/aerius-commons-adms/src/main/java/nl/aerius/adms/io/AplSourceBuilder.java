/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileConstants.ADMS_SOURCE_DETAILS;
import static nl.aerius.adms.io.AplFileConstants.ADMS_SOURCE_VERTEX;
import static nl.aerius.adms.io.AplFileConstants.NO_BUILDING;
import static nl.aerius.adms.io.AplFileKeys.SOURCE_VERTEX_X;
import static nl.aerius.adms.io.AplFileKeys.SOURCE_VERTEX_Y;
import static nl.aerius.adms.io.AplFileKeys.SRC_ANGLE_1;
import static nl.aerius.adms.io.AplFileKeys.SRC_ANGLE_2;
import static nl.aerius.adms.io.AplFileKeys.SRC_BUOYANCY_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_DENSITY;
import static nl.aerius.adms.io.AplFileKeys.SRC_DIAMETER;
import static nl.aerius.adms.io.AplFileKeys.SRC_EFFLUX_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_FB;
import static nl.aerius.adms.io.AplFileKeys.SRC_FM;
import static nl.aerius.adms.io.AplFileKeys.SRC_HEIGHT;
import static nl.aerius.adms.io.AplFileKeys.SRC_L1;
import static nl.aerius.adms.io.AplFileKeys.SRC_L2;
import static nl.aerius.adms.io.AplFileKeys.SRC_MAIN_BUILDING;
import static nl.aerius.adms.io.AplFileKeys.SRC_MASS_FLUX;
import static nl.aerius.adms.io.AplFileKeys.SRC_MASS_H2O;
import static nl.aerius.adms.io.AplFileKeys.SRC_MOL_WEIGHT;
import static nl.aerius.adms.io.AplFileKeys.SRC_NAME;
import static nl.aerius.adms.io.AplFileKeys.SRC_NUM_GROUPS;
import static nl.aerius.adms.io.AplFileKeys.SRC_NUM_ISOTOPES;
import static nl.aerius.adms.io.AplFileKeys.SRC_NUM_POLLUTANTS;
import static nl.aerius.adms.io.AplFileKeys.SRC_NUM_VERTICES;
import static nl.aerius.adms.io.AplFileKeys.SRC_PERCENT_NOX_AS_NO2;
import static nl.aerius.adms.io.AplFileKeys.SRC_POLLUTANTS;
import static nl.aerius.adms.io.AplFileKeys.SRC_POL_DURATION;
import static nl.aerius.adms.io.AplFileKeys.SRC_POL_EMISSION_RATE;
import static nl.aerius.adms.io.AplFileKeys.SRC_POL_START_TIME;
import static nl.aerius.adms.io.AplFileKeys.SRC_POL_TOTALEMISSION;
import static nl.aerius.adms.io.AplFileKeys.SRC_RELEASE_AT_NTP;
import static nl.aerius.adms.io.AplFileKeys.SRC_SOURCE_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_SPECIFIC_HEAT_CAPACITY;
import static nl.aerius.adms.io.AplFileKeys.SRC_TEMPERATURE;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_EMISSIONS_MODE;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_GRADIENT;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_NUM_TRAFFIC_FLOWS;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_ROAD_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_YEAR;
import static nl.aerius.adms.io.AplFileKeys.SRC_USE_VAR_FILE;
import static nl.aerius.adms.io.AplFileKeys.SRC_VERTICAL_VELOCITY;
import static nl.aerius.adms.io.AplFileKeys.SRC_VOLUMETRIC_FLOW_RATE;
import static nl.aerius.adms.io.AplFileKeys.SRC_X1;
import static nl.aerius.adms.io.AplFileKeys.SRC_Y1;

import java.util.List;

import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSRoadSource;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Builds an {@link ADMSSource} into a APL/UPL formatted ADMS_SOURCE_DETAILS string.
 */
final class AplSourceBuilder extends AplBuilder {

  private final ADMSSourceFileType fileType;

  private AplSourceBuilder(final ADMSSourceFileType fileType) {
    this.fileType = fileType;
    // Builder should be used through the static build method.
  }

  /**
   * Builds a APL/UPL formatted ADMS_SOURCE_DETAILS from the given {@link ADMSSource}.
   *
   * @param fileType ADMS type of the file
   * @param source source to get formatted string from
   * @param admsGroupKey emission substances to be included
   * @return APL/UPL formatted ADMS_SOURCE_DETAILS string
   * @throws AeriusException
   */
  public static String build(final ADMSSourceFileType fileType, final ADMSSource<?> source, final ADMSGroupKey admsGroupKey) {
    final AplSourceBuilder builder = new AplSourceBuilder(fileType);

    return builder.buildSource(source, admsGroupKey);
  }

  private String buildSource(final ADMSSource<?> source, final ADMSGroupKey admsGroupKey) {
    final Geometry geometry = source.getGeometry();

    buildHeader(ADMS_SOURCE_DETAILS);
    buildRow(SRC_NAME, source.getName());
    final String mainBuilding = source.getMainBuilding();
    buildRow(SRC_MAIN_BUILDING, mainBuilding == null || mainBuilding.isBlank() ? NO_BUILDING : mainBuilding);
    buildRow(SRC_HEIGHT, source.getHeight());
    buildRow(SRC_DIAMETER, source.getDiameter());
    buildRow(SRC_VOLUMETRIC_FLOW_RATE, source.getVolumetricFlowRate());
    buildRow(SRC_VERTICAL_VELOCITY, source.getVerticalVelocity());
    buildRow(SRC_TEMPERATURE, source.getTemperature());
    buildRow(SRC_MOL_WEIGHT, source.getMolWeight());
    buildRow(SRC_DENSITY, source.getDensity());
    buildRow(SRC_SPECIFIC_HEAT_CAPACITY, source.getSpecificHeatCapacity());
    buildRow(SRC_SOURCE_TYPE, source.getSourceType().getType());
    buildRow(SRC_RELEASE_AT_NTP, source.getReleaseAtNTP());
    buildRow(SRC_EFFLUX_TYPE, source.getEffluxType());
    buildRow(SRC_BUOYANCY_TYPE, source.getBuoyancyType());
    if (ADMSSourceFileType.APL == fileType) {
      buildRow(SRC_PERCENT_NOX_AS_NO2, source.getPercentNOxAsNO2());
    }
    if (geometry instanceof final Point point) {
      buildRow(SRC_X1, point.getX());
      buildRow(SRC_Y1, point.getY());
    } else {
      buildRow(SRC_X1, 0.0);
      buildRow(SRC_Y1, 0.0);
      //
    }
    buildRow(SRC_L1, source.getL1());
    buildRow(SRC_L2, source.getL2());
    if (ADMSSourceFileType.APL == fileType) {
      buildRow(SRC_FM, source.getMomentumFlux());
      buildRow(SRC_FB, source.getBuoyancyFlux());
      buildRow(SRC_MASS_FLUX, source.getMassFlux());
      buildRow(SRC_ANGLE_1, source.getAngle1());
      buildRow(SRC_ANGLE_2, source.getAngle2());
      buildRow(SRC_MASS_H2O, source.getMassH2O());
      buildRow(SRC_USE_VAR_FILE, 0);
    }
    buildRow(SRC_NUM_GROUPS);
    if (source instanceof final ADMSRoadSource roadSource) {
      buildRow(SRC_TRA_EMISSIONS_MODE);
      buildRow(SRC_TRA_YEAR, roadSource.getTraYear());
      // Don't use road type from source, but use hardcoded value
      // ADMS only supports certain values, and we don't want to link our (database) values to ADMS unless required
      // As we're not supplying intensities for the source, this road type shouldn't matter anyway.
      // This hardcoded value just prevents an error in ADMS UI if you'd resulting file directly in ADMS.
      buildRow(SRC_TRA_ROAD_TYPE);
      buildRow(SRC_TRA_GRADIENT, roadSource.getTraGradient());
    }
    buildRow(SRC_NUM_VERTICES, computeNrOfVertices(geometry));
    buildRow(SRC_TRA_NUM_TRAFFIC_FLOWS); // TODO should be changed when other geometry support added.

    final List<Substance> substances = admsGroupKey.getEmissionSubstances(source instanceof ADMSRoadSource);
    final List<String> subs = substances.stream().map(Substance::getName).toList();
    final int size = subs.size();
    buildRow(SRC_NUM_POLLUTANTS, size);
    buildRowList(SRC_POLLUTANTS, subs);
    buildRowListDouble(SRC_POL_EMISSION_RATE, substances.stream().map(source::getEmission).toList());
    buildRowListDefaultDouble(SRC_POL_TOTALEMISSION, size);
    buildRowListDefaultDouble(SRC_POL_START_TIME, size);
    buildRowListDefaultDouble(SRC_POL_DURATION, size);
    buildRow(SRC_NUM_ISOTOPES, 0);
    buildEndMarker();
    buildOptionalVertices(geometry);
    return build();
  }

  private int computeNrOfVertices(final Geometry geometry) {
    if (geometry instanceof final LineString linestring) {
      return linestring.getCoordinates().length;
    } else if (geometry instanceof final Polygon polygon) {
      return polygon.getCoordinates()[0].length - 1;
    } else {
      return 0;
    }
  }

  private void buildOptionalVertices(final Geometry geometry) {
    final double[][] coordinates;
    final int nrOfVertices;

    if (geometry instanceof final LineString linestring) {
      coordinates = linestring.getCoordinates();
      nrOfVertices = coordinates.length;
    } else if (geometry instanceof final Polygon polygon) {
      coordinates = polygon.getCoordinates()[0];
      nrOfVertices = coordinates.length - 1;
    } else {
      coordinates = null;
      nrOfVertices = 0;
    }
    for (int i = 0; i < nrOfVertices; i++) {
      appendVerticesPrefix();
      buildHeader(ADMS_SOURCE_VERTEX);
      appendVerticesPrefix();
      buildRow(SOURCE_VERTEX_X, coordinates[i][0]);
      appendVerticesPrefix();
      buildRow(SOURCE_VERTEX_Y, coordinates[i][1]);
      appendVerticesPrefix();
      buildEndMarker();
    }
  }

  private void appendVerticesPrefix() {
    buildPrefix("  ");
  }
}
