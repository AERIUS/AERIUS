/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileKeys.OPT_ALL_SOURCES;
import static nl.aerius.adms.io.AplFileKeys.OPT_CONDITION;
import static nl.aerius.adms.io.AplFileKeys.OPT_CREATE_COMPREHENSIVE_FILE;
import static nl.aerius.adms.io.AplFileKeys.OPT_GROUPS_OR_SOURCE;
import static nl.aerius.adms.io.AplFileKeys.OPT_INCLUDE;
import static nl.aerius.adms.io.AplFileKeys.OPT_INCLUDED_SOURCE;
import static nl.aerius.adms.io.AplFileKeys.OPT_NUM_EXCEEDENCES;
import static nl.aerius.adms.io.AplFileKeys.OPT_NUM_GROUPS;
import static nl.aerius.adms.io.AplFileKeys.OPT_NUM_OUTPUTS;
import static nl.aerius.adms.io.AplFileKeys.OPT_NUM_PERCENTILES;
import static nl.aerius.adms.io.AplFileKeys.OPT_OUTPUT_PER_SOURCE;
import static nl.aerius.adms.io.AplFileKeys.OPT_POL_NAME;
import static nl.aerius.adms.io.AplFileKeys.OPT_SAMPLING_TIME;
import static nl.aerius.adms.io.AplFileKeys.OPT_SAMPLING_TIME_UNITS;
import static nl.aerius.adms.io.AplFileKeys.OPT_SHORT_OR_LONG;
import static nl.aerius.adms.io.AplFileKeys.OPT_UNITS;
import static nl.aerius.adms.io.AplFileKeys.OPT_VALIDITY;

import java.util.Collections;
import java.util.List;

import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.util.OSUtils;

/**
 * Builder class for ADMS APL/UPL pollutant details.
 */
final class AplOutputPollutantsBuilder extends AplBuilder {
  private static String OPT_PERCENTILES = "OptPercentiles              =" + OSUtils.NL
      + "  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0"
      + OSUtils.NL
      + "  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0";
  private static String OPT_EXCEEDENCES = "OptExceedences              =" + OSUtils.NL
      + "  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0"
      + OSUtils.NL
      + "  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0";

  private AplOutputPollutantsBuilder() {
    // Builder should be used through the static build method.
  }

  /**
   * Builds a APL/UPL formatted ADMS_SOURCE_DETAILS from the given {@link ADMSSource}.
   *
   * @param source source to get formatted string from
   * @param substances emission substances to be included
   * @return APL/UPL formatted ADMS_SOURCE_DETAILS string
   */
  public static String build(final List<Substance> substances) {
    final AplOutputPollutantsBuilder builder = new AplOutputPollutantsBuilder();

    return builder.buildOutputPollutant(substances);
  }

  private String buildOutputPollutant(final List<Substance> substances) {
    final int size = substances.size();
    final List<String> subs = substances.stream().map(Substance::getName).toList();

    buildRow(OPT_NUM_OUTPUTS, size);
    buildRowList(OPT_POL_NAME, subs);
    buildRowListDefaultInt(OPT_INCLUDE, size);
    buildRowListDefaultInt(OPT_SHORT_OR_LONG, size);
    buildRowListDefaultDouble(OPT_SAMPLING_TIME, size);
    buildRowListDefaultInt(OPT_SAMPLING_TIME_UNITS, size);
    buildRowListDefaultInt(OPT_CONDITION, size);
    buildRowListDefaultInt(OPT_NUM_PERCENTILES, size);
    buildRowListDefaultInt(OPT_NUM_EXCEEDENCES, size);
    buildRaw(OPT_PERCENTILES);
    buildRaw(OPT_EXCEEDENCES);
    buildUnits(substances);
    buildRowListDefaultDouble(OPT_VALIDITY, size);
    buildRow(OPT_GROUPS_OR_SOURCE);
    buildRow(OPT_ALL_SOURCES);
    buildRow(OPT_NUM_GROUPS);
    buildRow(OPT_INCLUDED_SOURCE, "");
    buildRow(OPT_CREATE_COMPREHENSIVE_FILE);
    buildRow(OPT_OUTPUT_PER_SOURCE);
    buildEndMarker();
    return build();
  }

  private void buildUnits(final List<Substance> substances) {
    // TODO Use correct units for substances
    buildRowList(OPT_UNITS, Collections.nCopies(substances.size(), "ug/m3"));
  }
}
