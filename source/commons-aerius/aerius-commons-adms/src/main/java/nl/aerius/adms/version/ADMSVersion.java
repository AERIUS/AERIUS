/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.version;

import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.calculation.EngineVersion;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * ADMS executable and data versions.
 */
public enum ADMSVersion implements EngineVersion {
  NCA_LATEST("5.0.3.0-9166", // ADMS executable version and build version
      "VarDepVERSION1", // ADMS din file version number
      "receptor_average_dry_deposition_velocities_20240529", // Deposition velocity receptor grid
      "spatially_varying_roughness_500m_20240528", // Surface roughness background data
      "uk_terrain%s_20240603", // Terrain height data
      "spatially_varying_dry_deposition_velocity%s_500m_20240528", // Deposition velocity specifically for plume depletion
      List.of("uk_met_files", "v1"), // Meteorology files
      NOxToNO2Version.V8_1 // NOx to NO2 calculator data
  );

  private final String version;
  private final String dinFileVersion;
  private final String depositionVelocityVersion;
  private final String surfaceRoughnessVersion;
  private final String terrainHeightVersionFormat;
  private final String plumeDepletionVersionFormat;
  private final List<String> metFilesVersion;
  private final NOxToNO2Version noxToNO2Version;

  ADMSVersion(final String version, final String dinFileVersion, final String depositionVelocityVersion, final String surfaceRoughnessVersion,
      final String terrainHeightVersionFormat, final String plumeDepletionVersionFormat, final List<String> metFilesVersion,
      final NOxToNO2Version noxToNO2Version) {
    this.version = version;
    this.dinFileVersion = dinFileVersion;
    this.depositionVelocityVersion = depositionVelocityVersion;
    this.surfaceRoughnessVersion = surfaceRoughnessVersion;
    this.terrainHeightVersionFormat = terrainHeightVersionFormat;
    this.plumeDepletionVersionFormat = plumeDepletionVersionFormat;
    this.metFilesVersion = metFilesVersion;
    this.noxToNO2Version = noxToNO2Version;
  }

  @Override
  public String getVersion() {
    return version;
  }

  public String getDinFileVersion() {
    return dinFileVersion;
  }

  public String getDepositionVelocityVersion() {
    return depositionVelocityVersion;
  }

  public String getSurfaceRoughnessVersion() {
    return surfaceRoughnessVersion;
  }

  public String getTerrainHeightVersionFormat() {
    return String.format(terrainHeightVersionFormat, "");
  }

  public String getTerrainHeightFileName(final int resolution) {
    return String.format(terrainHeightVersionFormat, "_" + resolution + "m");
  }

  public String getPlumeDepletionVersionFormat() {
    return String.format(plumeDepletionVersionFormat, "");
  }

  public String getPlumeDepletionFileName(final Substance substance) {
    return String.format(plumeDepletionVersionFormat, '_' + substance.name().toLowerCase(Locale.ROOT));
  }

  public List<String> getMetFilesVersion() {
    return metFilesVersion;
  }

  public NOxToNO2Version getNOxToNO2Version() {
    return noxToNO2Version;
  }

  public static ADMSVersion getADMSVersionByLabel(final String versionLabel) {
    return EngineVersion.getByVersionLabel(ADMSVersion.values(), "ADMS", versionLabel);
  }
}
