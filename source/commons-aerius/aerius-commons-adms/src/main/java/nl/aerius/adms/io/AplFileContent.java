/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;

/**
 * Object to store the input/output of an ADMS APL/UPL file.
 */
class AplFileContent extends LineReaderResult<ADMSSource<?>> {

  private ScenarioMetaData metaData;
  private String timeZone;
  private boolean quickRun;
  private final List<Substance> substances = new ArrayList<>();
  private List<ADMSBuilding> buildings = new ArrayList<>();
  private File aspFile;
  private ADMSOptions options;
  private boolean supCalcDryDep;
  private String metDataFile;
  private String uaiDataFile;
  private String facDataFile;
  private int hilGridSize;
  private String rufDataFile;
  private String terDataFile;
  private boolean metDataIsSequential;
  private String meteoYear;

  public ScenarioMetaData getMetaData() {
    return metaData;
  }

  public void setMetaData(final ScenarioMetaData metaData) {
    this.metaData = metaData;
  }

  public String getTimeZone() {
    return timeZone;
  }

  public void setTimeZone(final String timeZone) {
    this.timeZone = timeZone;
  }

  public boolean isQuickRun() {
    return quickRun;
  }

  public void setQuickRun(final boolean quickRun) {
    this.quickRun = quickRun;
  }

  public ADMSOptions getOptions() {
    return options;
  }

  public void setOptions(final ADMSOptions options) {
    this.options = options;
  }

  public List<Substance> getSubstances() {
    return substances;
  }

  public void addSubstance(final Substance substance) {
    substances.add(substance);
  }

  public List<ADMSBuilding> getBuildings() {
    return buildings;
  }

  public void addBuilding(final ADMSBuilding building) {
    buildings.add(building);
  }

  public void setBuildings(final List<ADMSBuilding> buildings) {
    this.buildings = buildings;
  }

  public File getAspFile() {
    return aspFile;
  }

  public void setAspFile(final File aspFile) {
    this.aspFile = aspFile;
  }

  public String getMetDataFile() {
    return metDataFile;
  }

  public void setMetDataFile(final String metDataFile) {
    this.metDataFile = metDataFile;
  }

  public String getUaiDataFile() {
    return uaiDataFile;
  }

  public void setUaiDataFile(final String uaiDataFile) {
    this.uaiDataFile = uaiDataFile;
  }

  public String getFacDataFile() {
    return facDataFile;
  }

  public void setFacFile(final String facDataFile) {
    this.facDataFile = facDataFile;
  }

  public int getHilGridSize() {
    return hilGridSize;
  }

  public void setHilGridSize(final int hilGridSize) {
    this.hilGridSize = hilGridSize;
  }

  public String getRufDataFile() {
    return rufDataFile;
  }

  public void setRufDataFile(final String rufDataFile) {
    this.rufDataFile = rufDataFile;
  }

  public String getTerDataFile() {
    return terDataFile;
  }

  public void setTerDataFile(final String terDataFile) {
    this.terDataFile = terDataFile;
  }

  public boolean isSupCalcDryDep() {
    return supCalcDryDep;
  }

  public void setSupCalcDryDep(final boolean supCalcDryDep) {
    this.supCalcDryDep = supCalcDryDep;
  }

  public boolean isMetDataIsSequential() {
    return metDataIsSequential;
  }

  public void setMetDataIsSequential(final boolean metDataIsSequential) {
    this.metDataIsSequential = metDataIsSequential;
  }

  public String getMeteoYear() {
    return meteoYear;
  }

  public void setMeteoYear(final String meteoYear) {
    this.meteoYear = meteoYear;
  }
}
