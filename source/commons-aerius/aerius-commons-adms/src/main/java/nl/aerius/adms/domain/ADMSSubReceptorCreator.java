/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import nl.overheid.aerius.receptor.HexagonEquilateralTrianglesMap;
import nl.overheid.aerius.receptor.SubReceptorCreator;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.domain.theme.nca.NcaSubReceptorConstants;

/**
 * Class to build a {@link HexagonEquilateralTrianglesMap} specific for creating sub receptors to use with ADMS.
 */
public class ADMSSubReceptorCreator extends SubReceptorCreator<ADMSSubReceptor> {

  /**
   * The id of the first level in the list of distances.
   */
  private static final int LEVEL_OFFSET = 1;

  public ADMSSubReceptorCreator(final double hexagonRadius) {
    super(AeriusPointType.SUB_RECEPTOR, hexagonRadius, NcaSubReceptorConstants.SUB_RECEPTOR_DISTANCES, LEVEL_OFFSET);
  }

  @Override
  public ADMSSubReceptor copyToSubReceptorPoint(final AeriusPoint aeriusPoint, final AeriusPointType pointType, final int subLevel) {
    final ADMSSubReceptor newPoint = new ADMSSubReceptor(aeriusPoint, pointType, subLevel);

    if (aeriusPoint instanceof final ADMSCalculationPoint admsPoint) {
      newPoint.setADMSCalculationPointDataFrom(admsPoint);
    }
    return newPoint;
  }
}
