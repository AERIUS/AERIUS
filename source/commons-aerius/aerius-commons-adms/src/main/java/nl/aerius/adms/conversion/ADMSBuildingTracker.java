/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.overheid.aerius.building.BuildingTracker;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;

/**
 * BuildingTracker for {@link ADMSBuilding} objects.
 */
public class ADMSBuildingTracker implements BuildingTracker {

  private final Map<String, EngineSource> allBuildings = new HashMap<>();

  public ADMSBuildingTracker(final List<BuildingFeature> buildings) throws AeriusException {
    for (final BuildingFeature buildingFeature : buildings) {
      final ADMSBuilding building = EmissionSource2ADMSConverter.convert(buildingFeature);

      allBuildings.put(buildingFeature.getProperties().getGmlId(), building);
    }
  }

  /**
   * Returns the first building present in the buildings list or throws an exception when not present.
   *
   * @param buildingId id of the building to get, should not be null
   * @throws AeriusException throws an exception in case the building requested was not found
   */
  public EngineSource getBuilding(final String buildingId) throws AeriusException {
    final EngineSource building = allBuildings.get(buildingId);

    if (building == null) {
      throw new AeriusException(ImaerExceptionReason.COHESION_REFERENCE_MISSING_BUILDING, buildingId);
    }
    return building;
  }

  public Map<String, EngineSource> getAllBuildings() {
    return allBuildings;
  }
}
