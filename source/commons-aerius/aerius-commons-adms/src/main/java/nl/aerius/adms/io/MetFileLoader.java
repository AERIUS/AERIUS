/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.aerius.adms.backgrounddata.MetDataFileKey;
import nl.overheid.aerius.shared.domain.calculation.MetDatasetType;

/**
 * Utility class to load meteorology files from a folder.
 * Does not actually load the files in memory, but maps each file to a met station based on file name.
 */
public final class MetFileLoader {

  private static final String MET_FILE_EXTENSION = ".met";
  private static final Pattern PATTERN = Pattern.compile("([^_]+)_([^_]+)_([^_]+)[\\._]", Pattern.UNICODE_CHARACTER_CLASS);

  private MetFileLoader() {
    // Util class
  }

  public static Map<MetDataFileKey, File> loadMetFiles(final File folder) {
    if (folder == null || !folder.exists()) {
      throw new IllegalArgumentException("Supplied folder does not exist: " + folder);
    }
    final Map<MetDataFileKey, File> mappedFiles = new HashMap<>();
    for (final File file : folder.listFiles(x -> x.getName().endsWith(MET_FILE_EXTENSION))) {
      final Matcher matcher = PATTERN.matcher(file.getName());

      if (matcher.find()) {
        final MetDatasetType datasetType = MetDatasetType.safeValueOf(matcher.group(2).replace('-', '_'));
        if (datasetType != null) {
          final MetDataFileKey key = new MetDataFileKey(matcher.group(1), datasetType, matcher.group(3));
          mappedFiles.put(key, file);
        }
      }
    }
    return mappedFiles;
  }

}
