/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileConstants.ADMS_PARAMETERS_BLD;
import static nl.aerius.adms.io.AplFileKeys.BLD_ANGLE;
import static nl.aerius.adms.io.AplFileKeys.BLD_HEIGHT;
import static nl.aerius.adms.io.AplFileKeys.BLD_LENGTH;
import static nl.aerius.adms.io.AplFileKeys.BLD_NAME;
import static nl.aerius.adms.io.AplFileKeys.BLD_NUM_BUILDINGS;
import static nl.aerius.adms.io.AplFileKeys.BLD_TYPE;
import static nl.aerius.adms.io.AplFileKeys.BLD_WIDTH;
import static nl.aerius.adms.io.AplFileKeys.BLD_X;
import static nl.aerius.adms.io.AplFileKeys.BLD_Y;

import java.util.List;
import java.util.function.Function;

import nl.aerius.adms.domain.ADMSBuilding;

/**
 * Builds the ADMS APL/UPL content of the Buildings data.
 */
final class AplBuildingsBuilder extends AplBuilder {

  private AplBuildingsBuilder() {
    // Builder should be used through the static build method.
  }

  /**
   * Builds a APL/UPL formatted ADMS_PARAMETERS_BLD from the given content.
   *
   * @param buildings buildings
   * @return APL/UPL formatted ADMS_PARAMETERS_BLD
   */
  public static String build(final List<ADMSBuilding> buildings) {
    final AplBuildingsBuilder builder = new AplBuildingsBuilder();

    return builder.buildBuildings(buildings);
  }

  private String buildBuildings(final List<ADMSBuilding> buildings) {
    buildHeader(ADMS_PARAMETERS_BLD);
    buildRow(BLD_NUM_BUILDINGS, buildings.size());
    if (!buildings.isEmpty()) {
      buildRowList(BLD_NAME, toListString(buildings, ADMSBuilding::getName));
      buildRowListInteger(BLD_TYPE, toListString(buildings, b -> String.valueOf(b.getShape().getCode())));
      buildRowListDouble(BLD_X, toListDouble(buildings, b -> b.getBuildingCentre().getX()));
      buildRowListDouble(BLD_Y, toListDouble(buildings, b -> b.getBuildingCentre().getY()));
      buildRowListDouble(BLD_HEIGHT, toListDouble(buildings, ADMSBuilding::getBuildingHeight));
      buildRowListDouble(BLD_LENGTH, toListDouble(buildings, ADMSBuilding::getBuildingLength));
      buildRowListDouble(BLD_WIDTH, toListDouble(buildings, ADMSBuilding::getBuildingWidth));
      buildRowListDouble(BLD_ANGLE, toListDouble(buildings, ADMSBuilding::getBuildingOrientation));
    }
    buildEndMarker();
    return build();
  }

  private static List<String> toListString(final List<ADMSBuilding> buildings, final Function<ADMSBuilding, String> mapper) {
    return buildings.stream().map(mapper).toList();
  }

  private static List<Double> toListDouble(final List<ADMSBuilding> buildings, final Function<ADMSBuilding, Double> mapper) {
    return buildings.stream().map(mapper).toList();
  }
}
