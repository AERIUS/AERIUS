/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.aerius.adms.backgrounddata.ADMSData;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.domain.NCACalculationResultPoint;
import nl.aerius.adms.util.ADMSUtil;
import nl.overheid.aerius.backgrounddata.DepositionVelocityMap;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.util.AeriusPointUtil;

/**
 * Utility class to enrich the results with additional (background) data that is needed calculate NO2 concentration, and depositions
 * from the calculation results returned by ADMS.
 */
public class EnrichResultProcessor {

  private final ADMSData admsData;

  public EnrichResultProcessor(final ADMSData admsData) {
    this.admsData = admsData;
  }

  /**
   * Enrich the results with NO2 background data in case of road sources, and enrich with deposition velocity data.
   *
   * @param input input data
   * @param groupKey type of input data
   * @param results results to enrich
   */
  public void enrichResults(final ADMSInputData input, final ADMSGroupKey groupKey, final List<NCACalculationResultPoint> results) {
    addAggregatedReceptors(input, results);
    if (groupKey == ADMSGroupKey.NOX_ROAD) {
      enrichWithNO2Data(input, results);
    }
    if (input.getEmissionResultKeys().stream().anyMatch(e -> e.getEmissionResultType() == EmissionResultType.DEPOSITION)) {
      enrichWithDepositionVelocity(input, results);
    }
  }

  private void addAggregatedReceptors(final ADMSInputData input, final List<NCACalculationResultPoint> results) {
    final Map<String, NCACalculationResultPoint> resultsMap = results.stream()
        .collect(Collectors.toMap(AeriusPointUtil::encodeName, Function.identity()));

    // Add all receptors that are calculated via subreceptor points, and therefore are not present in the results.
    results.addAll(input.getReceptors().stream().filter(r -> !resultsMap.containsKey(AeriusPointUtil.encodeName(r)))
        .map(NCACalculationResultPoint::new).toList());
  }

  private void enrichWithNO2Data(final ADMSInputData input, final List<NCACalculationResultPoint> results) {
    final NOxToNO2CalculatorData no2ToNO2CalculatorData = admsData.getNOxToNO2CalculatorData(input.getAdmsVersion());

    ADMSNO2ConcentrationCalculator.enrich(no2ToNO2CalculatorData, input.getYear(), results, input.getEmissionResultKeys(), input.getReceptors());
  }

  private void enrichWithDepositionVelocity(final ADMSInputData input, final List<NCACalculationResultPoint> results) {
    final EnumSet<EmissionResultKey> erks = input.getEmissionResultKeys();
    final Map<Substance, DepositionVelocityMap> depositionVelocities = admsData.getDepositionVelocityMap(input.getAdmsVersion());
    final Map<EmissionResultKey, EmissionResultKey> depositionKeys = ADMSUtil.filterKeys(erks, EmissionResultType.DEPOSITION,
        Collectors.toMap(Function.identity(), k -> EmissionResultKey.valueOf(toConcentrationSubstance(k), EmissionResultType.CONCENTRATION)));

    results.stream().filter(p -> !p.getPointType().isSubReceptor())
        .forEach(r -> setDepositionVelocity(depositionKeys, depositionVelocities, r));
  }

  private static void setDepositionVelocity(final Map<EmissionResultKey, EmissionResultKey> depositionKeys,
      final Map<Substance, DepositionVelocityMap> depositionVelocities, final NCACalculationResultPoint resultPoint) {
    depositionKeys.forEach(
        (d, c) -> resultPoint.putDepositionVelocity(c.getSubstance(), depositionVelocities.get(c.getSubstance()).getDepositionVelocity(resultPoint)));
  }


  /**
   * Get the concentration substance to use to calculate deposition.
   * Specifically for NOx deposition NO2 concentration is used.
   *
   * @param key Deposition result key
   * @return Concentration substance to use as key to get the concentration results to calculate deposition
   */
  private static Substance toConcentrationSubstance(final EmissionResultKey key) {
    return key.getSubstance() == Substance.NOX ? Substance.NO2 : key.getSubstance();
  }
}
