/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;

import nl.aerius.adms.backgrounddata.ADMSData;
import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.io.AplExportWriter;
import nl.aerius.adms.util.ADMSEngineSources;
import nl.aerius.adms.util.ADMSProgressCollector;
import nl.aerius.adms.util.BoundingBoxUtil;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.exec.ProcessListener;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.worker.InputValidatorCollector;

/**
 * Class for processing ADMS. This will be either to calculate with ADMS or export the ADMS input files.
 *
 * @param <T> results data type.
 */
class ProcessADMS<T> {

  private final ADMSRunner<T> runner;
  private final ADMSData admsData;
  private final ADMSConfiguration configuration;
  private final boolean keepGeneratedFiles;
  private final InputValidatorCollector<EngineSource> inputValidatorCollector;
  private final ExecutorService executor;

  /**
   * Constructor
   *
   * @param executor Executor service to run ADMS concurrently
   * @param runner Runner performing the specific process
   * @param admsData ADMS background data
   * @param configuration ADMS configuration
   * @param validationErrorsToFile if true validations errors are stored in a file instead of thrown
   */
  public ProcessADMS(final ExecutorService executor, final ADMSRunner<T> runner, final ADMSData admsData, final ADMSConfiguration configuration,
      final boolean validationErrorsToFile) {
    this(executor, runner, admsData, configuration, validationErrorsToFile, configuration.isKeepGeneratedFiles());
  }

  /**
   * Constructor
   *
   * @param executor Executor service to run ADMS concurrently
   * @param runner Runner performing the specific process
   * @param admsData ADMS background data
   * @param configuration ADMS configuration
   * @param validationErrorsToFile if true validations errors are stored in a file instead of thrown
   * @param keepGeneratedFiles if true, generated files are kept no matter what. Otherwise, they're only kept when an exception occurs.
   */
  public ProcessADMS(final ExecutorService executor, final ADMSRunner<T> runner, final ADMSData admsData, final ADMSConfiguration configuration,
      final boolean validationErrorsToFile, final boolean keepGeneratedFiles) {
    this.executor = executor;
    this.runner = runner;
    this.admsData = admsData;
    this.configuration = configuration;
    this.keepGeneratedFiles = keepGeneratedFiles;
    inputValidatorCollector = new InputValidatorCollector<>(validationErrorsToFile, AeriusExceptionReason.ADMS_INPUT_VALIDATION);
  }

  /**
   * Performs the run iterative over the sources sets by calling the consumer for each list of sources in the input.
   *
   * @param input ADMS input
   * @param consumer method to perform the actual run
   * @param progressCollector
   * @param newline new line string to be used at the end of lines in the files
   * @param processListener
   * @throws Exception throws exception in case something fails
   */
  protected void run(final ADMSInputData input, final BiConsumer<ADMSGroupKey, T> consumer, final ADMSProgressCollector progressCollector,
      final ProcessListener processListener, final String newline) throws Exception {
    inputValidatorCollector.collect(input, input.getEmissionSources());
    final List<Future<Object>> futures = new ArrayList<>();
    final AplExportWriter writer = createAplExportWriter(ADMSVersion.NCA_LATEST, newline);

    for (final Entry<Integer, Collection<EngineSource>> entry : input.getEmissionSources().entrySet()) {
      futures.add(executor.submit(() -> {
        run(progressCollector, processListener, writer, consumer, ADMSGroupKey.safeValueOf(entry.getKey()), input, newline);
        return null;
      }));
    }
    // Wait for all runs to finish.
    for (final Future<Object> future : futures) {
      try {
        future.get();
      } catch (final ExecutionException ee) {
        if (ee.getCause() instanceof final Exception e) {
          throw e;
        } else {
          throw ee;
        }
      }
    }
  }

  /**
   * Protected method to overwrite creating AplExportWriter in unit tests.
   * @param admsVersion
   * @param newline
   * @return a new AplExportWriter
   */
  protected AplExportWriter createAplExportWriter(final ADMSVersion admsVersion, final String newline) {
    return new AplExportWriter(admsData, admsVersion, newline);
  }

  private void run(final ADMSProgressCollector progressCollector, final ProcessListener processListener, final AplExportWriter writer,
      final BiConsumer<ADMSGroupKey, T> consumer, final ADMSGroupKey admsGroupKey, final ADMSInputData input, final String newline) throws Exception {
    final int sourcesKeyIndex = admsGroupKey.getIndex();
    final BBox extent = getExtent(input, sourcesKeyIndex);
    final File outputDirectory = configuration.createOutputDirectory();
    // outputFile is a random filename in a random directory. Each input file type creates a file by appending it's extension.
    final File outputFile = new File(outputDirectory, outputDirectory.getName());

    inputValidatorCollector.writeErrorsToFile(sourcesKeyIndex, outputFile.getParentFile(), newline);
    final ADMSEngineSources admsEngineSources = ADMSEngineSources.builder().set(input.getEmissionSources().get(sourcesKeyIndex)).build();
    final File projectFile = writer.write(outputFile, input, admsGroupKey, admsEngineSources, input.getCalculateReceptors(), extent);

    consumer.accept(admsGroupKey, runner.run(progressCollector, processListener, input, admsGroupKey, outputDirectory, outputFile, projectFile));
    removeGeneratedFiles(outputDirectory);
  }

  private static BBox getExtent(final ADMSInputData input, final int sourcesKeyIndex) throws AeriusException {
    if (input.isQuickRun()) {
      return BoundingBoxUtil.getExtendedBoundingBox((List<EngineSource>) input.getEmissionSources().get(sourcesKeyIndex),
          input.getReceptors().stream().map(AeriusPoint.class::cast).toList());
    } else {
      return input.getExtent();
    }
  }

  private void removeGeneratedFiles(final File outputDirectory) {
    if (!keepGeneratedFiles) {
      FileUtil.removeDirectoryRecursively(outputDirectory.toPath());
    }
  }
}
