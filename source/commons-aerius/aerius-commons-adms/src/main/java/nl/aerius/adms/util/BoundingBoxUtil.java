/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.GeometryFactory;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSBuilding.BuildingShape;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Util class for calculating the bounding box around a set of emissionSources and calculation points.
 */
public final class BoundingBoxUtil {

  /**
   * When using terrain height or surface roughness, the extent needs to be expanded by at least this fraction.
   */
  private static final double TERRAIN_EXTENT_EXPAND_FRACTION = 0.151;
  /**
   * When using terrain height or surface roughness, the extent needs to be expanded by at least this amount.
   */
  private static final double TERRAIN_EXTENT_EXPAND_MINIMUM = 500;

  private BoundingBoxUtil() {
  }

  /**
   * Returns the bounding box around the given sources and receptors with some additional margin.
   *
   * @param engineSources engine source
   * @param calculationPoints points
   * @return bounding box
   * @throws AeriusException
   */
  public static BBox getExtendedBoundingBox(final List<EngineSource> sources, final List<AeriusPoint> calculationPoints)
      throws AeriusException {
    return Optional.ofNullable(determineBoundingBox(Stream.concat(BoundingBoxUtil.toGeometries(sources), calculationPoints.stream()).toList()))
        .map(bbox1 -> expandBoundingBox(bbox1, 0, DimensionUtil.determineMaxDimension(sources)))
        .map(bbox2 -> expandBoundingBox(bbox2, TERRAIN_EXTENT_EXPAND_FRACTION, TERRAIN_EXTENT_EXPAND_MINIMUM))
        .orElse(null);
  }

  private static Stream<Geometry> toGeometries(final List<EngineSource> sources) {
    return sources.stream().filter(BoundingBoxUtil::hasGeometry).map(BoundingBoxUtil::toGeometry);
  }

  private static boolean hasGeometry(final EngineSource engineSource) {
    return engineSource instanceof ADMSBuilding || engineSource instanceof EngineEmissionSource;
  }

  private static Geometry toGeometry(final EngineSource engineSource) {
    if (engineSource instanceof final ADMSBuilding building) {
      return toGeometry(building);
    } else if (engineSource instanceof final ADMSSource admsSource) {
      return admsSource.getGeometry();
    } else {
      throw new IllegalArgumentException("Unknown engine source; can't determine geometry of it");
    }
  }

  static Geometry toGeometry(final ADMSBuilding building) {
    if (building.getShape() == BuildingShape.CIRCULAR) {
      final Point c = building.getBuildingCentre();
      final double r = building.getBuildingLength() / 2;
      final double x = c.getX();
      final double y = c.getY();
      final double minX = x - r;
      final double maxX = x + r;
      final double minY = y - r;
      final double maxY = y + r;
      final Polygon polygon = new Polygon();

      polygon.setCoordinates(new double[][][] {{{minX, minY}, {minX, maxY}, {maxX, maxY}, {maxX, minY}, {minX, minY}}});
      return polygon;
    } else {
      try {
        return GeometryUtil.constructPolygonFromDimensions(building.getBuildingCentre(), building.getBuildingLength(), building.getBuildingWidth(),
            building.getBuildingOrientation());
      } catch (final AeriusException e) {
        throw new IllegalArgumentException("Can't determine dimensions of building.", e);
      }
    }
  }

  /**
   * Determine the bounding box for a list of geometries.
   *
   * A single point or a set of points at the same coordinates returns a Point.
   * All other combinations of geometries return a rectangle that bounds all geometries.
   */
  static BBox determineBoundingBox(final List<Geometry> geometries) throws AeriusException {
    if (geometries.isEmpty()) {
      return null;
    }

    final GeometryCollection geometryCollection = toGeometryCollection(geometries);
    final Envelope envelope = geometryCollection.getEnvelopeInternal();

    return new BBox(envelope.getMinX(), envelope.getMinY(), envelope.getMaxX(), envelope.getMaxY());
  }

  private static GeometryCollection toGeometryCollection(final List<Geometry> geometries) throws AeriusException {
    final List<org.locationtech.jts.geom.Geometry> list = new ArrayList<>();

    for (final Geometry geometry : geometries) {
      list.add(GeometryUtil.getGeometry(geometry));
    }

    final org.locationtech.jts.geom.Geometry[] objects = list.toArray(new org.locationtech.jts.geom.Geometry[0]);
    return new GeometryCollection(objects, new GeometryFactory());
  }

  /**
   * Expands a bounding box by a fraction and a minimum amount
   *
   * @param originalBBox the bbox to expand
   * @param expansionFraction the fraction to expand (in each direction, i.e. 25% means the total width will be 50% larger)
   * @param minExpansion the minimum amount to expand (in each direction, i.e. 500 means the total width will be 1000 larger)
   * @return the expanded bounding box
   */
  public static BBox expandBoundingBox(final BBox originalBBox, final double expansionFraction, final double minExpansion) {
    final double expansionX = Math.max(originalBBox.getWidth() * expansionFraction, minExpansion);
    final double expansionY = Math.max(originalBBox.getHeight() * expansionFraction, minExpansion);

    return new BBox(originalBBox.getMinX() - expansionX, originalBBox.getMinY() - expansionY,
        originalBBox.getMaxX() + expansionX, originalBBox.getMaxY() + expansionY);
  }
}
