/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.aerius.adms.io.AplFileConstants.KeyWithDefault;

/**
 * Reads blocks of ADMS APL/UPL files and returns the content of a block in a map.
 */
class AplBlockReader {

  private static final String APL_BLOCK_NAME = "APL_BLOCK_NAME";
  private static final Pattern STRING_PATTERN = Pattern.compile("\"([^\"]+)\"");

  private final Map<String, String> map;
  private final BufferedReader reader;

  AplBlockReader(final Map<String, String> map, final BufferedReader reader) {
    this.map = map;
    this.reader = reader;
  }

  /**
   * Reads the next block found in the reader and returns the content in a map.
   * The name of the block is stored in the map using the key {@link #APL_BLOCK_NAME}.
   *
   * @param reader reader to read the next block from
   * @return map with the data from the read block or empty if no block found.
   * @throws IOException
   */
  public static AplBlockReader readBlock(final BufferedReader reader) throws IOException {
    final Map<String, String> map = new HashMap<>();

    if (readTillStartBlock(reader, map)) {
      String line = reader.readLine();
      String key = null;
      String value = "";
      int indent = 0;
      while (line != null) {
        line = line.stripTrailing();
        // key is set when when multi detected.
        // It also sets the indent (indentation of key)
        // If the indentation of the next line is > ident of key assume it's part of the multiline value.
        // Check for > and not == because if line is new key/value but starts at begin ident, while previous was indented,
        // line check indent < indent and that should return false here.
        // Summarizing if line is indented more than key line it's part of multiline, else it's the next key/value
        if (key != null && (line.length() - line.stripLeading().length()) > indent) {
          // line is part of multiline value result
          value += line;
        } else {
          if (key != null) {
            // line is now next value, but previous was multiline so store multiline value.
            map.put(key.trim(), value.trim());
            key = null;
            value = null;
            indent = 0;
          }
          if (isEndOfBlock(line)) {
            break;
          }
          if (line.endsWith("=")) {
            // multi line value
            key = line.substring(0, line.length() - 1).stripTrailing();
            value = "";
            indent = key.length() - key.stripLeading().length();
            key = key.trim();
          } else {
            // single line value
            final String[] split = splitLine(line);
            if (split.length == 2) {
              map.put(split[0].trim(), split[1].trim());
              key = null;
              value = null;
              indent = 0;
            } else {
              // TODO error
            }
          }
        }
        line = reader.readLine();
      }
    }
    return new AplBlockReader(map, reader);
  }

  private static String[] splitLine(final String line) {
    return line.split("\\s{0,20}=\\s*");
  }

  /**
   * Reads till it finds a start marker or the end of the reader.
   *
   * @param reader reader to read lines from
   * @param map map to put name of the block in using {@link #APL_BLOCK_NAME}
   * @return true if start marker was found, false if end of the reader found
   * @throws IOException read errors
   */
  private static boolean readTillStartBlock(final BufferedReader reader, final Map<String, String> map) throws IOException {
    final String blockName = readTillStartBlock(reader);
    final boolean found;
    if (blockName == null) {
      found = false;
    } else {
      map.put(APL_BLOCK_NAME, blockName);
      found = true;
    }
    return found;
  }

  private static String readTillStartBlock(final BufferedReader reader) throws IOException {
    String blockName = null;
    do {
      final String line = reader.readLine();

      if (line == null) {
        return blockName;
      }
      blockName = readStartOfBlock(line.stripLeading());
    } while (blockName == null);
    return blockName;
  }

  /**
   * @return Returns the name of the block as stored in the map
   */
  public String getBlockName() {
    return map.get(APL_BLOCK_NAME);
  }

  private static String readStartOfBlock(final String line) {
    return line.startsWith("&") ? line.substring(1) : null;
  }

  private static boolean isEndOfBlock(final String line) {
    return "/".equals(line.stripLeading());
  }

  public int parseInt(final KeyWithDefault key) {
    return Integer.parseInt(mapValue(key));
  }

  public double parseDouble(final KeyWithDefault key) {
    final String value = map.get(key.getKey());

    return value == null ? (double) key.getDefaultValue() : Double.parseDouble(mapValue(key));
  }

  /**
   * @return Returns the double values in the String as a List.
   */
  public List<Double> parseDoubleList(final KeyWithDefault key) {
    return Stream.of(mapValue(key).split("\\s+"))
        .map(Double::valueOf)
        .toList();
  }

  /**
   * @return Returns the integer values in the String as a List.
   */
  public List<Integer> parseIntegerList(final KeyWithDefault key) {
    return Stream.of(mapValue(key).split("\\s+"))
        .map(Integer::valueOf)
        .toList();
  }

  /**
   * @return Return String with quotes stripped.
   */
  public String parseString(final KeyWithDefault key) {
    final String value = mapValue(key);

    return value.substring(1, value.length() - 1);
  }

  /**
   * @return Returns the Strings stripped of quotes as a List.
   */
  public List<String> parseStringList(final KeyWithDefault key) {
    final Matcher matcher = STRING_PATTERN.matcher(mapValue(key));
    final List<String> list = new ArrayList<>();

    while (matcher.find()) {
      list.add(matcher.group(1));
    }
    return list;
  }

  public String mapValue(final KeyWithDefault key) {
    final String value = map.get(key.getKey());

    if (value == null) {
      throw new IllegalArgumentException("Could not find value for " + key);
    }
    return value;
  }

  /**
   *
   * @param nrOfVertices
   * @param coordinates
   * @return
   * @throws IOException
   */
  public double[][] readVertices(final int nrOfVertices, final double[][] coordinates) throws IOException {
    for (int i = 0; i < nrOfVertices; i++) {
      final String blockName = readTillStartBlock(reader);

      if (blockName != null) {
        coordinates[i][0] = Double.parseDouble(splitLine(reader.readLine().strip())[1]);
        coordinates[i][1] = Double.parseDouble(splitLine(reader.readLine().strip())[1]);
      }
    }
    return coordinates;
  }
}
