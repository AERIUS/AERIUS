/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileConstants.DEFAULT_HIL_GRID_SIZE;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.backgrounddata.ADMSData;
import nl.aerius.adms.backgrounddata.GridResolution;
import nl.aerius.adms.backgrounddata.MetDataFileKey;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.util.ADMSEngineSources;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Writes all data to ADMS input files.
 */
public class AplExportWriter {

  private static final Logger LOG = LoggerFactory.getLogger(AplExportWriter.class);

  private final ADMSData admsData;
  private final ADMSVersion admsVersion;
  private final String newline;
  private final DinFileWriter dinFileWriter;

  public AplExportWriter(final ADMSData admsData, final ADMSVersion admsVersion, final String newline) {
    this.admsData = admsData;
    this.admsVersion = admsVersion;
    this.newline = newline;
    dinFileWriter = new DinFileWriter(admsVersion, newline, admsData);
  }

  /**
   * Write all input files for ADMS from the input data.
   *
   * @param outputFile generic output filename, specific input types will append their extension and write to those files
   * @param inputData input data to get input files from
   * @param admsGroupKey key for the sources to take from the input data
   * @return
   * @throws IOException
   * @throws AeriusException
   */
  public File write(final File outputFile, final ADMSInputData inputData, final ADMSGroupKey admsGroupKey, final ADMSEngineSources sources,
      final Collection<ADMSCalculationPoint> receptors, final BBox extent) throws IOException, AeriusException {
    final AplFileContent content = new AplFileContent();

    prepareAplContent(content, inputData, sources);
    // Write all input files.
    writeAspFile(outputFile, content, receptors);
    writeMetDataFile(outputFile, content, inputData);
    final Substance plumeDepletionSubstance = dinFileWriter.substanceForPlumeDepletion(inputData, admsGroupKey);
    final String dinFile = writeDinFile(outputFile, content, extent, plumeDepletionSubstance);
    writeUaiFiles(outputFile, content, dinFile, plumeDepletionSubstance);

    writeFacFile(outputFile, content, sources);
    if (inputData.getAdmsOptions().isSpatiallyVaryingRoughness()) {
      final GridResolution gridResolution = GridResolution.getGridResolutionFromExtent(extent);

      content.setHilGridSize(gridResolution.getHilGridSize());
      writeRufFile(outputFile, content, extent);
      if (inputData.getAdmsOptions().isComplexTerrain()) {
        writeTerFile(outputFile, content, extent, gridResolution.getResolution());
      }
    } else {
      content.setHilGridSize(DEFAULT_HIL_GRID_SIZE);
    }
    return writeAplFile(outputFile, content, inputData.getFileType(), admsGroupKey);
  }

  private static void prepareAplContent(final AplFileContent content, final ADMSInputData inputData, final ADMSEngineSources sources) {
    inputData.getSubstances().forEach(content::addSubstance);
    content.getObjects().addAll(sources.sources());
    content.setTimeZone(inputData.getTimeZone());
    content.setQuickRun(inputData.isQuickRun());
    content.setOptions(inputData.getAdmsOptions());
    content.setBuildings(sources.buildings());
  }

  private <R extends AeriusPoint> void writeAspFile(final File outputFile, final AplFileContent content, final Collection<R> receptors)
      throws IOException {
    final File aspFile = AspFileWriter.fileWithExtension(outputFile);

    content.setAspFile(aspFile);
    AspFileWriter.writeFile(aspFile, receptors, newline);
  }

  private void writeMetDataFile(final File outputFile, final AplFileContent content, final ADMSInputData inputData)
      throws IOException, AeriusException {
    final ADMSOptions admsOptions = inputData.getAdmsOptions();
    final int metSiteId = admsOptions.getMetSiteId();
    final String meteoYear = inputData.getMeteoYear();
    final MetDataFileKey fileKey = new MetDataFileKey(String.valueOf(metSiteId), admsOptions.getMetDatasetType(), meteoYear);
    final File correspondingMetFile = admsData.getMetFileStore(admsVersion).getFile(fileKey);

    // Set MetDataIsSequential to 1 in case we use an legal met site id. For unit testing metSiteId 0 is used.
    // Setting MetDataIsSequential  to 0 because test met data is not sequential.
    content.setMetDataIsSequential(metSiteId > 0);
    content.setMeteoYear(meteoYear);
    if (correspondingMetFile == null) {
      LOG.debug("Could not find Met file for {}", fileKey);
      throw new AeriusException(AeriusExceptionReason.ADMS_CALCULATION_OPTIONS_MET_SITE_ID_MISSING, String.valueOf(metSiteId));
    }
    final File tempMetFile = new File(outputFile.getParentFile(), correspondingMetFile.getName());
    Files.copy(correspondingMetFile.toPath(), tempMetFile.toPath());
    content.setMetDataFile(tempMetFile.getAbsolutePath());
  }

  /**
   * Write the additional input files.
   * If additional input present the name of the uai file will be set in the {@link AplFileContent} object.
   *
   * @param outputFile name of the output file
   * @param content content of the apl file
   * @param dinFile location of the din file or null if no din file
   * @param substance substance to calculate dy deposition for.
   * @throws AeriusException
   * @throws IOException
   */
  private void writeUaiFiles(final File outputFile, final AplFileContent content, final String dinFile, final Substance substance)
      throws IOException, AeriusException {
    final AdvancedCanyonFileWriter acfWriter = new AdvancedCanyonFileWriter(outputFile);
    final UaiFileWriter uaiWriter = new UaiFileWriter(outputFile, content.getTimeZone(), newline);

    final boolean writeASC = acfWriter.write(content.getObjects());
    uaiWriter.setASC(writeASC ? acfWriter.getOutputFile() : null);
    uaiWriter.setDinFile(dinFile);
    uaiWriter.setSubstance(substance);
    uaiWriter.setQuickRun(content.isQuickRun());
    content.setUaiDataFile(uaiWriter.writeUaiFile().getAbsolutePath());
  }

  private void writeFacFile(final File outputFile, final AplFileContent content, final ADMSEngineSources sources) throws IOException {
    final FacFileWriter facFileWriter = new FacFileWriter(outputFile, newline);

    if (facFileWriter.write(sources.sources(), sources.timeVaryingProfiles())) {
      content.setFacFile(facFileWriter.getOutputFile().getAbsolutePath());
    }
  }

  private void writeRufFile(final File outputFile, final AplFileContent content, final BBox extent) throws IOException, AeriusException {
    final File rufFile = ADMSFileConstants.constructDataFilename(outputFile, ADMSFileConstants.RUF_EXTENSION);
    final GeoTiffGridDataFileWriter writer = admsData.getSurfaceRoughnessWriter(admsVersion);

    if (writer != null && writer.write(rufFile, extent, newline)) {
      content.setRufDataFile(rufFile.getAbsolutePath());
    } else {
      throw new AeriusException(AeriusExceptionReason.ADMS_GEO_DATA_OUTSIDE_EXTENT, "spatially varying roughness");
    }
  }

  private void writeTerFile(final File outputFile, final AplFileContent content, final BBox extent, final Integer resolution)
      throws IOException, AeriusException {
    final File terFile = ADMSFileConstants.constructDataFilename(outputFile, ADMSFileConstants.TER_EXTENSION);
    final GeoTiffGridDataFileWriter writer = admsData.getTerrainHeightWriter(admsVersion, resolution);

    if (writer != null && writer.write(terFile, extent, newline)) {
      content.setTerDataFile(terFile.getAbsolutePath());
    } else {
      throw new AeriusException(AeriusExceptionReason.ADMS_GEO_DATA_OUTSIDE_EXTENT, "terrain height");
    }
  }

  private String writeDinFile(final File outputFile, final AplFileContent content, final BBox extent, final Substance substance) throws IOException {
    return substance == null ? null : dinFileWriter.writeDinFile(outputFile, content, extent, substance);
  }

  /**
   * Write APL/UPL file.
   *
   * @param outputFile name of the output file
   * @param content content of the apl file
   * @param fileType original input data
   * @param admsGroupKey
   * @return
   * @throws IOException
   * @throws AeriusException
   */
  private File writeAplFile(final File outputFile, final AplFileContent content, final ADMSSourceFileType fileType, final ADMSGroupKey admsGroupKey)
      throws IOException {
    return new AplFileWriter(fileType, newline).writeFile(outputFile, content, admsGroupKey);
  }
}
