/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import nl.overheid.aerius.io.AbstractLineColumnReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Abstract reader class to read comma separated files with point location data.
 *
 * @param <T> data object of the points
 */
abstract class AbstractPointReader<T extends AeriusPoint> extends AbstractLineColumnReader<T> {

  private static final String COLUM_SPEARATOR = ",";
  private static final String NAME_COLUMN = "Receptor name";
  private static final int NAME_COLUMN_INDEX = 0;
  private static final String X_COLUMN = "X(m)";
  private static final int X_COLUMN_INDEX = 1;
  private static final String Y_COLUMN = "Y(m)";
  private static final int Y_COLUMN_INDEX = 2;
  private static final String Z_COLUMN = "Z(m)";
  private static final int Z_COLUMN_INDEX = 3;
  private final int headerSize;

  public AbstractPointReader(final int headerSize) {
    super(COLUM_SPEARATOR);
    this.headerSize = headerSize;
  }

  @Override
  public LineReaderResult<T> readObjects(final InputStream inputStream) throws IOException {
    try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
      return super.readObjects(reader, headerSize);
    }
  }

  /**
   * Set generic fields: name and height.
   *
   * @param point
   */
  protected void setFields(final T point) {
    point.setLabel(getString(NAME_COLUMN, NAME_COLUMN_INDEX));
    point.setHeight(getDouble(Z_COLUMN, Z_COLUMN_INDEX));
  }

  protected String getId() {
    return getString(NAME_COLUMN, NAME_COLUMN_INDEX);
  }

  protected double getX() {
    return getDouble(X_COLUMN, X_COLUMN_INDEX);
  }

  protected double getY() {
    return getDouble(Y_COLUMN, Y_COLUMN_INDEX);
  }
}
