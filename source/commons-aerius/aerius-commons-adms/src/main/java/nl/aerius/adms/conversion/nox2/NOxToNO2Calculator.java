/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion.nox2;

import static nl.aerius.adms.conversion.nox2.NOxToNO2Constants.MG_M3_TO_PPM;

import java.util.Optional;

import nl.aerius.adms.conversion.nox2.Gradient5.Gradient5Result;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData.FractionNO2;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData.LACodeBackgroundConcentration;
import nl.aerius.adms.conversion.nox2.Photochemical.PhotoResult;
import nl.aerius.adms.domain.ADMSPointNO2Data;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;

/**
 * Calculates the NO2 concentration from NOx.
 * This algorithm is specific for road sources.
 * The algorithm is taken from the spreadsheet NOx_to_NO2_Calculator_v8.1.xlsm
 */
public final class NOxToNO2Calculator {

  private NOxToNO2Calculator() {
    // Util class
  }

  /**
   * Creates a ADMSPointNO2Data object with all necessary data to calculate NO2 from NOx related to road sources.
   * It takes the localNO2factor and backgroundNOx from the point, and gets the other data from the provided data object.
   *
   * @param backgroundData Background data
   * @param year year to calculate the data for
   * @param point point to calculate the data for
   * @return ADMSPointNO2Data object
   */
  public static ADMSPointNO2Data createADMSPointNO2Data(final NOxToNO2CalculatorData backgroundData, final int year,
      final ADMSCalculationPoint point) {
    final LACodeBackgroundConcentration bd = backgroundData.getBackgroundConcentration(year, point);
    if (bd == null) {
      return null;
    }
    final double backgroundNOx = point.getBackgroundNOx(year);
    final double laNOxBackground = bd.getNOx();
    final double laNO2Background = bd.getNO2();
    final double laO3Background = bd.getO3();

    // Calculates the fNO2 fractions to use. If localfNO2 > 0 use the specified fNO2 otherwise use the build in.
    final FractionNO2 fractionNO2 = backgroundData.getFractionNO2(year, point);
    final double localFNO2 = point.getRoadLocalFNO2() > 0 ? point.getRoadLocalFNO2() :
      Optional.ofNullable(fractionNO2).map(FractionNO2::local).orElse(0.0);
    final double regionalFNO2 = Optional.ofNullable(fractionNO2).map(FractionNO2::regional).orElse(0.0);

    return new ADMSPointNO2Data(backgroundNOx, laNOxBackground, laNO2Background, laO3Background, localFNO2, regionalFNO2);
  }

  /**
   * Calculates the NO2 from NOx on a point. It returns the calculated NO2 background and road specific NO2.
   *
   * Implementation of the Visual Basic algorithm NOxToNO2 from the excell sheet.
   * Documentation from the Visual Basic function:
   *
   * This subroutine is operated by the Run NOx to NO2 button on the NOx to NO2 worksheet
   * It calculates RoadNO2 concentrations from modelled RoadNOx
   * Local background conditions can be input as BackNOx or BackNO2
   * The subroutine calls the following subroutines:
   * photono2tonox  converts NO2 to NOx assuming photostationary state
   *                  only used if background concnetrations are less than the regional background
   * photonoxtono2   converts nox to no2 assuming photostationary state
   *                 only used if background concnetrations are less than the regional background
   * gradient5       converts NOx to NO2 using finite difference model
   * gradient7       converts NO2 to NOx using finite difference model
   *
   *
   * @param nox NOx concentation to convert to NO2
   * @return NO2 road specific background or null if no data provided
   *
   */
  public static NO2Results calculateNOxToNO2(final ADMSPointNO2Data no2Data, final double nox) {
    if (no2Data == null) {
      return null;
    }
    // Input general inputs as ppb
    final double o3back = no2Data.laO3Background() / 2D; // Worksheets("General Inputs").Cells(18, 5) / 2D;
    final double noxback = no2Data.laNOxBackground() / MG_M3_TO_PPM; // Worksheets("General Inputs").Cells(19, 5) / 1.91;
    final double no2back = no2Data.laNO2Background() / MG_M3_TO_PPM; // Worksheets("General Inputs").Cells(20, 5) / 1.91;

    final double fractionno2_default = no2Data.localFNO2(); // Worksheets("General Inputs").Cells(29, 6);
    final double fractionno2_back = no2Data.regionalFNO2(); //Worksheets("General Inputs").Cells(31, 6);
    final double backnox;

    // Calculate backNO2 from backnox if no backNO2 input and local background less then regional background
    // It then recalculates  regional background concs
    double o3back2 = o3back;
    double noxback2 = noxback;
    double no2back2 = no2back;

    if (no2Data.backgroundNOx() > 0) {
      backnox = no2Data.backgroundNOx() / MG_M3_TO_PPM;
      if (backnox < noxback) {
        noxback2 = backnox;
        final PhotoResult photoNOxtoNO2 = Photochemical.photoNOxtoNO2(fractionno2_back, backnox, no2back, noxback, o3back, 1.42E-20, 0.002);

        no2back2 = photoNOxtoNO2.getNO();
        o3back2 = photoNOxtoNO2.getO3();
      }
    } else {
      backnox = 0D;
    }

    // Assume receptor is on leeward side of road half the time and calculate NOx when on leeward side
    final double roadNOx = nox / MG_M3_TO_PPM; // Worksheets(thisone).Cells(n, 5) / 1.91;
    final double highNOx = backnox + 2D * roadNOx;

    // Calculate fraction no2 when receptor is on leeward side  of road
    final double fractionno2 = fractionno2_default;
    final double fractionno2_high = (fractionno2_back * backnox + 2D * roadNOx * fractionno2) / highNOx;

    // Calculate NO2 when receptor is on windward side
    final Gradient5Result windWardNO2 = Gradient5.gradient5(fractionno2_back, backnox, no2back2, noxback2, o3back2, 0.4, 1D, 1.42E-20, 0.002, 800, 0);
    final double backNO2 = windWardNO2.getNO2() * MG_M3_TO_PPM; // Worksheets("intermediate").Cells(1, 1) * MG_M3_TO_PPM;

    // Calculate NO2 when receptor is on leeward side
    final Gradient5Result leewardNO2 = Gradient5.gradient5(fractionno2_high, highNOx, no2back2, noxback2, o3back2, 0.4, 1D, 1.42E-20, 0.002, 800, 0);
    final double highno2 = leewardNO2.getNO2() * MG_M3_TO_PPM; // Worksheets("intermediate").Cells(1, 1) * MG_M3_TO_PPM;
    // Outputs
    final double roadNO2 = (highno2 - backNO2) / 2D;

    return new NO2Results(roadNO2, backNO2 + roadNO2);
  }

  public static class NO2Results {
    double road;
    double background;

    public NO2Results(final double road, final double background) {
      this.road = road;
      this.background = background;
    }

    public double getRoad() {
      return road;
    }

    public double getBackground() {
      return background;
    }
  }
}
