/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileKeys.MET_DS_MIN_LMO;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_MIN_LMO_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_PRIESTLY_TAYLOR;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_PRIESTLY_TAYLOR_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_SURFACE_ALBEDO;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_SURFACE_ALBEDO_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_MIN_LMO;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_MIN_LMO_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_PRIESTLY_TAYLOR;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_PRIESTLY_TAYLOR_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_ROUGHNESS;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_ROUGHNESS_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_SURFACE_ALBEDO;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_SURFACE_ALBEDO_MODE;

import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.calculation.MetSurfaceCharacteristics;

/**
 * Builder for Met_DS_* and Met_MS_* parameters.
 */
class AplMetBuilder extends AplBuilder {

  private static final int MET_DISPERSION_SITE_MODE_CONSTANT_VALUE = 1;
  private static final int MET_MS_MODE_ENTER_VALUE = 1;

  private AplMetBuilder() {
    // Builder should be used through the static build method.
  }

  public static String build(final ADMSOptions options, final MetSurfaceCharacteristics mssc) {
    final AplMetBuilder builder = new AplMetBuilder();

    return builder.buildMet(options, mssc);
  }

  private String buildMet(final ADMSOptions options, final MetSurfaceCharacteristics mssc) {
    // Builds Met_DS and Met_MS values. Not all values can be set and therefore are hard-coded here.
    buildRaw("Met_DS_RoughnessMode    = " + MET_DISPERSION_SITE_MODE_CONSTANT_VALUE);
    buildRaw("Met_DS_Roughness        = 5.0e-1");
    buildRaw("Met_DS_UseAdvancedMet   = " + MET_DISPERSION_SITE_MODE_CONSTANT_VALUE);
    buildRow(MET_DS_SURFACE_ALBEDO_MODE, MET_DISPERSION_SITE_MODE_CONSTANT_VALUE);
    buildRow(MET_DS_SURFACE_ALBEDO, options.getSurfaceAlbedo());
    buildRow(MET_DS_PRIESTLY_TAYLOR_MODE, MET_DISPERSION_SITE_MODE_CONSTANT_VALUE);
    buildRow(MET_DS_PRIESTLY_TAYLOR, options.getPriestleyTaylorParameter());
    buildRow(MET_DS_MIN_LMO_MODE, MET_DISPERSION_SITE_MODE_CONSTANT_VALUE);
    buildRow(MET_DS_MIN_LMO, options.getMinMoninObukhovLength());
    buildRaw("Met_DS_PrecipFactorMode = 0");
    buildRaw("Met_DS_PrecipFactor     = 1.0e+0");
    buildRaw("Met_MS_UseAdvancedMet   = 1");
    buildRow(MET_MS_ROUGHNESS_MODE, MET_MS_MODE_ENTER_VALUE);
    buildRow(MET_MS_ROUGHNESS, mssc.getRoughness());
    buildRow(MET_MS_SURFACE_ALBEDO_MODE, MET_MS_MODE_ENTER_VALUE);
    buildRow(MET_MS_SURFACE_ALBEDO, mssc.getSurfaceAlbedo());
    buildRow(MET_MS_PRIESTLY_TAYLOR_MODE, MET_MS_MODE_ENTER_VALUE);
    buildRow(MET_MS_PRIESTLY_TAYLOR, mssc.getPriestleyTaylorParameter());
    buildRow(MET_MS_MIN_LMO_MODE, MET_MS_MODE_ENTER_VALUE);
    buildRow(MET_MS_MIN_LMO, mssc.getMinMoninObukhovLength());
    return build();
  }
}
