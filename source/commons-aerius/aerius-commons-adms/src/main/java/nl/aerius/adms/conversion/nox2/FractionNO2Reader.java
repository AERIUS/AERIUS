/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion.nox2;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import nl.overheid.aerius.io.AbstractLineColumnReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Reader to read the fNO2 data table.
 */
class FractionNO2Reader extends AbstractLineColumnReader<Void> {

  private static final String SPLIT_PATTERN = ",";
  private static final int HEADER_ROWS = 1;
  private static final String AREA = "Area";
  private static final int AREA_INDEX = 0;
  private static final int YEARS_FIRST_INDEX = 1;

  private final NOxToNO2CalculatorData backgroundData;

  /**
   * Constructor.
   *
   * @param backgroundData data object to store the read data in
   */
  public FractionNO2Reader(final NOxToNO2CalculatorData backgroundData) {
    super(SPLIT_PATTERN);
    this.backgroundData = backgroundData;
  }

  @Override
  public LineReaderResult<Void> readObjects(final InputStream inputStream) throws IOException {
    try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8.name())) {
      return super.readObjects(reader, HEADER_ROWS);
    }
  }

  @Override
  protected Void parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final String area = getString(AREA, AREA_INDEX);
    final int firstYear = backgroundData.getVersion().getfNO2FirstYear();
    final int yearIndexOffset = firstYear - YEARS_FIRST_INDEX;

    for (int year = firstYear; year <= backgroundData.getVersion().getfNO2LastYear(); year++) {
      backgroundData.putFractionNO2(area, year, getDouble(String.valueOf(year), year - yearIndexOffset));
    }
    return null;
  }

}
