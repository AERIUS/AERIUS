/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import nl.aerius.adms.io.AplFileConstants.KeyWithDefault;
import nl.overheid.aerius.util.OSUtils;

/**
 * Base class for building ADMS apl blocks. This class contains a set of convenience methods to build rows.
 * To get the final block as String call {@link #build()}.
 */
class AplBuilder {

  private static final String MULTI_ROW_PREFIX = "  ";
  private static final String MULTI_ROW_SEPARATOR = " ";
  private static final String SCIENTIFIC_NOTATION = "%.8e";
  private final StringBuilder builder = new StringBuilder();

  protected AplBuilder() {
    // Util class, only for extending.
  }

  /**
   * Builds a row of String values, with strings wrapped with quotes.
   *
   * @param key the row key
   * @param list list of string values
   */
  protected void buildRowList(final KeyWithDefault key, final List<String> list) {
    builder.append(key.getKey()).append(" =").append(OSUtils.NL).append(MULTI_ROW_PREFIX);
    builder.append('"').append(String.join("\" \"", list)).append('"').append(OSUtils.NL);
  }

  /**
   * Builds a row of Double values, with doubles are formatted using scientific notation.
   *
   * @param key the row key
   * @param list list of double values
   */
  protected void buildRowListDouble(final KeyWithDefault key, final List<Double> list) {
    final List<String> values = list.stream().map(AplBuilder::formatScientific).collect(Collectors.toList());

    builder.append(key.getKey()).append(" =").append(OSUtils.NL).append(MULTI_ROW_PREFIX);
    builder.append(String.join(MULTI_ROW_SEPARATOR, values)).append(OSUtils.NL);
  }

  /**
   * Builds a row by initializing the list with the default double values.
   *
   * @param key key name, including default value
   * @param size number of values that should be put in.
   */
  protected void buildRowListDefaultDouble(final KeyWithDefault key, final int size) {
    buildRowListDouble(key, Collections.nCopies(size, (Double) key.getDefaultValue()));
  }

  protected void buildRowListInteger(final KeyWithDefault key, final List<String> values) {
    builder.append(key.getKey()).append(" =").append(OSUtils.NL).append(MULTI_ROW_PREFIX);
    builder.append(String.join(MULTI_ROW_SEPARATOR, values)).append(OSUtils.NL);
  }

  protected void buildRowListDefaultInt(final KeyWithDefault key, final int size) {
    buildRowListInteger(key, Collections.nCopies(size, String.valueOf(key.getDefaultValue())));
  }

  /**
   * Builds the row with the default value of the key. Depending on the data type of the default value it will be formatted.
   * @param key key with default value
   */
  protected void buildRow(final KeyWithDefault key) {
    final Object defaultValue = key.getDefaultValue();

    if (defaultValue instanceof String) {
      buildRow(key, (String) defaultValue);
    } else if (defaultValue instanceof Double) {
      buildRow(key, (Double) defaultValue);
    } else {
      buildKey(key).append(defaultValue).append(OSUtils.NL);
    }
  }

  protected void buildHeader(final String header) {
    builder.append('&').append(header).append(OSUtils.NL);
  }

  protected void buildRaw(final String string) {
    builder.append(string).append(OSUtils.NL);
  }

  protected void buildRow(final KeyWithDefault key, final int value) {
    buildKey(key).append(value).append(OSUtils.NL);
  }

  protected void buildRow(final KeyWithDefault key, final double value) {
    buildKey(key).append(formatScientific(value)).append(OSUtils.NL);
  }

  private static String formatScientific(final double value) {
    return String.format(Locale.ROOT, SCIENTIFIC_NOTATION, value)
        .replaceAll("0{1,10}e", "e").replace("e+0", "e+").replace("e-0", "e-").replace(".e", ".0e");
  }

  protected void buildRow(final KeyWithDefault key, final String value) {
    buildKey(key).append('"').append(value == null ? key.getDefaultValue() : value).append('"').append(OSUtils.NL);
  }

  private StringBuilder buildKey(final KeyWithDefault key) {
    return builder.append(key.getKey()).append(" = ");
  }

  protected void buildEndMarker() {
    builder.append("/").append(OSUtils.NL);
  }

  protected void buildPrefix(final String prefix) {
    builder.append(prefix);
  }

  protected String build() {
    return builder.toString();
  }
}
