/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.util;

import java.util.Set;
import java.util.stream.Collector;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * Utility class for getting specific {@link EmissionResultKey} or {@link Substance} objects.
 */
public final class ADMSUtil {

  private ADMSUtil() {
    // Util class
  }

  /**
   * Filters all keys with the given {@link EmissionResultType} from the set of keys and returns them using the given collector.
   *
   * @param erks keys to filter
   * @param ert result type to filter
   * @param collector collector to use to return collected keys.
   * @return Collected keys
   */
  public static <T> T filterKeys(final Set<EmissionResultKey> erks, final EmissionResultType ert,
      final Collector<? super EmissionResultKey, ?, T> collector) {
    return erks.stream().filter(e -> e.getEmissionResultType() == ert).collect(collector);
  }

  /**
   * Get the concentration substance to use to calculate deposition.
   * Specifically for NOx deposition NO2 concentration is used.
   *
   * @param key Deposition result key
   * @return Concentration substance to use as key to get the concentration results to calculate deposition
   */
  public static Substance toConcentrationSubstance(final EmissionResultKey key) {
    return key.getSubstance() == Substance.NOX ? Substance.NO2 : key.getSubstance();
  }

}
