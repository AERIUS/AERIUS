/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Locale;

import org.geotools.geometry.Envelope2D;

import nl.overheid.aerius.geo.shared.BBox;

/**
 * Writer class to write an extent snapped out of a GeoTiff raster to a file.
 * The file is a line based format: &lt;number>, &lt;x>, &lt;y>, &lt;value>
 */
public class GeoTiffGridDataFileWriter {

  public static final double DEFAULT_VALUE_SCALAR = 1.0;

  enum DataFileType {
    DIN("%d,%d,%d,%.2e,0,0,0,0"),
    DEFAULT("%d,%d,%d,%.2e");

    private final String format;

    DataFileType(final String format) {
      this.format = format;
    }
  }

  private final Raster raster;
  private final BBox rasterBBox;
  private final int coordinateOffsetX;
  private final int coordinateOffsetY;
  private final double inverseY;
  private final int blockSize;

  /**
   * Constructs a Writer class containing the complete GeoTiff Raster.
   *
   * @param raster GeoTiff raster with the data
   * @param envelope The envelop of the GeoTiff raster
   */
  public GeoTiffGridDataFileWriter(final Raster raster, final Envelope2D envelope) {
    this.raster = raster;
    this.blockSize = (int) Math.round(envelope.getWidth() / raster.getWidth());
    rasterBBox = new BBox(envelope.getMinX(), envelope.getMinY(), envelope.getMaxX(), envelope.getMaxY());
    final int halfBlockSize = blockSize / 2;

    coordinateOffsetX = (int) Math.round(envelope.getMinX() + halfBlockSize);
    coordinateOffsetY = halfBlockSize;
    inverseY = rasterBBox.getMaxY() - rasterBBox.getMinY();
  }

  public boolean write(final File file, final BBox bbox, final String newLine) throws IOException {
    return write(file, bbox, DataFileType.DEFAULT, newLine, DEFAULT_VALUE_SCALAR, null);
  }

  /**
   * Writes data inside the extent by cutting the extent out of the GeoTiff data.
   *
   * @param file file to write the data to
   * @param bbox bounding box of the extent
   * @param newLine new line character to use to write at the end of the lines in the file
   * @param valueScalar scale the value of a tiff grid point.
   * @param header an optional header string
   */
  public boolean write(final File file, final BBox bbox, final DataFileType dataFileType, final String newLine, final double valueScalar,
      final String header) throws IOException {

    if (bbox == null || isOutside(bbox)) {
      return false;
    }
    final BBox envelope = snap(bbox);
    try (final Writer writer = Files.newBufferedWriter(file.toPath(), StandardCharsets.UTF_8)) {
      if (header != null && !header.isBlank()) {
        writer.write(header + newLine);
      }

      final GeoTiffValueDataFileWriter dataWriter = new GeoTiffValueDataFileWriter(envelope, writer, dataFileType, newLine, valueScalar);
      return dataWriter.write();
    }
  }

  /**
   * Snaps the given envelope to be inside the GeoTiff raster.
   * If it would be completely outside the raster it will result in a bounding box on a single point.
   *
   * @param envelope envelope to snap inside the GeoTiff raster
   * @return snapped envelope
   */
  private BBox snap(final BBox envelope) {
    return new BBox(
        Math.min(rasterBBox.getMaxX(), Math.max(rasterBBox.getMinX(), envelope.getMinX())),
        Math.min(rasterBBox.getMaxY(), Math.max(rasterBBox.getMinY(), envelope.getMinY())),
        Math.min(rasterBBox.getMaxX(), Math.max(rasterBBox.getMinX(), envelope.getMaxX())),
        Math.min(rasterBBox.getMaxY(), Math.max(rasterBBox.getMinY(), envelope.getMaxY())));
  }

  private boolean isOutside(final BBox bbox) {
    return (bbox.getMaxX() < rasterBBox.getMinX() || bbox.getMinX() > rasterBBox.getMaxX() ||
        bbox.getMaxY() < rasterBBox.getMinY() || bbox.getMinY() > rasterBBox.getMaxY());
  }

  private class GeoTiffValueDataFileWriter {

    private final Writer writer;
    private final String format;
    private final double[] buffer;
    private final int minPixelX;
    private final int maxPixelX;
    private final int maxPixelY;
    private final double valueScalar;

    private int pixelX;
    private int pixelY;
    private int idx = 0;

    public GeoTiffValueDataFileWriter(final BBox bbox, final Writer writer, final DataFileType dataFileType, final String newLine,
        final double valueScalar) {
      this.writer = writer;
      this.valueScalar = valueScalar;
      format = dataFileType.format + newLine;
      minPixelX = Math.max((int) Math.round((bbox.getMinX() - rasterBBox.getMinX()) / blockSize) - 1, 0);
      maxPixelX = Math.min((int) Math.ceil((bbox.getMaxX() - rasterBBox.getMinX()) / blockSize), raster.getWidth() - 1);
      // Because image is upside down y coordinate must be reversed.
      maxPixelY = Math.min((int) Math.ceil((inverseY - (bbox.getMinY() - rasterBBox.getMinY())) / blockSize) + 1, raster.getHeight() - 1);
      final int minPixelY = Math.max((int) Math.round((inverseY - (bbox.getMaxY() - rasterBBox.getMinY())) / blockSize), 0);
      final int width = Math.min(maxPixelX - minPixelX + 1, raster.getWidth());
      final int height = Math.min(maxPixelY - minPixelY + 1, raster.getHeight());

      buffer = new double[width * height];
      raster.getPixels(minPixelX, minPixelY, width, height, buffer);
      pixelX = minPixelX;
      pixelY = minPixelY;
    }

    /**
     * Writes the geotiff data in the extent to the file.
     *
     * @return returns true if any data is written to file.
     * @throws IOException
     */
    public boolean write() throws IOException {
      if (buffer.length == 0) {
        return false;
      }
      while (writeAndNext()) {
        idx++;
      }
      return true;
    }

    private boolean writeAndNext() throws IOException {
      writer.append(String.format(Locale.ROOT, format, idx + 1,
          coordinateOffsetX + (pixelX * blockSize),  // reprojected x-pixel to X-coordinate
          (int) Math.round(rasterBBox.getMaxY()) - (pixelY * blockSize) + coordinateOffsetY, // reprojected y-pixel to Y-coordinate
          buffer[idx] * valueScalar));
      pixelX++;
      if (pixelX > maxPixelX) {
        pixelX = minPixelX;
        pixelY++;
        if (pixelY > maxPixelY) {
          // Finished
          return false;
        }
      }
      return true;
    }
  }
}
