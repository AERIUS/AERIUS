/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import nl.aerius.adms.domain.ADMSSubReceptor;
import nl.aerius.adms.domain.NCACalculationResultPoint;
import nl.overheid.aerius.receptor.AbstractSubReceptorAggregator;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * ADMS specific implementation of {@link AbstractSubReceptorAggregator}
 */
public class ADMSSubReceptorAggregator extends AbstractSubReceptorAggregator<NCACalculationResultPoint, NCACalculationResultPoint> {

  /**
   * Initializes the aggregator with the receptors.
   * It filters out all {@link ADMSSubReceptor} points as those points are only relevant for subreceptors.
   *
   * @param receptors all receptors
   * @param erks emission result keys for which results are requested.
   */
  public ADMSSubReceptorAggregator(final Collection<NCACalculationResultPoint> receptors, final Set<EmissionResultKey> erks) {
    super(receptors, erks, false, NCACalculationResultPoint::new);
  }

  @Override
  protected Collection<NCACalculationResultPoint> combine(final Collection<NCACalculationResultPoint> receptorResults,
      final Collection<NCACalculationResultPoint> subReceptorResults) {
    final List<NCACalculationResultPoint> collectedResults = new ArrayList<>();

    collectedResults.addAll(receptorResults);
    collectedResults.addAll(subReceptorResults);
    return collectedResults;
  }
}
