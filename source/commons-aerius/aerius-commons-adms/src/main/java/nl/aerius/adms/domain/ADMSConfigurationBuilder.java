/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import java.util.Properties;

import nl.aerius.adms.ADMSModelDataUtil;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.worker.EngineWorkerConfigurationBuilder;
import nl.overheid.aerius.worker.FileServerConfigurationBuilder;

/**
 * Builder for ADMS worker configuration.
 */
public class ADMSConfigurationBuilder extends EngineWorkerConfigurationBuilder<ADMSVersion, ADMSConfiguration> {

  private static final String ADMS_WORKER_PREFIX = "adms";
  private static final String ADMS_LICENSE_BASE64 = "license.base64";
  private static final String ADMS_PROGRESS_INTERVAL = "progress.interval";
  private static final int ADMS_PROGRESS_INTERVAL_DEFAULT_SECONDS = 5;

  public ADMSConfigurationBuilder(final Properties properties, final ADMSModelDataUtil preloader) {
    super(properties, ADMS_WORKER_PREFIX, preloader, true);
    addBuilder(new FileServerConfigurationBuilder<>(properties));
  }

  @Override
  protected ADMSConfiguration create() {
    return new ADMSConfiguration(workerProperties.getProcesses());
  }

  @Override
  protected void buildEngine(final ADMSConfiguration configuration) {
    configuration.setAdmsLicenseBase64(workerProperties.getPropertyOptional(ADMS_LICENSE_BASE64));
    configuration.setProgressReportIntervalSeconds(
        workerProperties.getPropertyOrDefault(ADMS_PROGRESS_INTERVAL, ADMS_PROGRESS_INTERVAL_DEFAULT_SECONDS));
  }

  @Override
  protected ADMSVersion getVersionByLabel(final String version) {
    return ADMSVersion.getADMSVersionByLabel(version);
  }
}
