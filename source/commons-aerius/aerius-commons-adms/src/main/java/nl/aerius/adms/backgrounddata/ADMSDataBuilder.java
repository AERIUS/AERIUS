/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.backgrounddata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.opengis.referencing.FactoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.ADMSConstants;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorDataReader;
import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.io.GeoTiffLoader;
import nl.aerius.adms.io.MetFileStore;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.backgrounddata.DepositionVelocityMap;
import nl.overheid.aerius.backgrounddata.DepositionVelocityReader;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.util.HttpClientManager;

/**
 * Util class to populate {@link ADMSData} background data.
 */
public final class ADMSDataBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(ADMSDataBuilder.class);
  private static final List<Substance> PLUME_DEPLETION_DEPOSITION_VELOCITY_SUBSTANCES = List.of(Substance.NO2, Substance.NH3);

  private ADMSDataBuilder() {
  }

  /**
   * Create a new and populated instance of {@link ADMSData}
   * @param configuration configuration of the ADMS worker
   * @return a new and populated instance of {@link ADMSData}
   * @throws IOException
   * @throws FactoryException
   */
  public static synchronized ADMSData build(final ADMSConfiguration configuration) throws IOException, FactoryException {
    final ADMSData admsData = new ADMSData();
    for (final ADMSVersion admsVersion : configuration.getModelPreloadVersions()) {
      loadAdditionalData(configuration, admsVersion, admsData);
    }
    return admsData;
  }

  /**
   * Check if more data is needed, and load into the passed instance of {@link ADMSData}
   * @param config configuration of the ADMS worker
   * @param admsVersion the admsVersion for which to load data
   * @param admsData the admsData to load data into
   * @param resolution the resolution of the file
   * @return a new and populated instance of {@link ADMSData}
   */
  public static synchronized void loadAdditionalData(final ADMSConfiguration config, final ADMSVersion admsVersion, final ADMSData admsData)
      throws IOException, FactoryException {
    loadDepositionVelocities(admsVersion, admsData, config.getDepositionVelocityFile(admsVersion));
    loadSurfaceRoughness(admsVersion, admsData, config.getSurfaceRoughnessFile(admsVersion));
    for (final Integer resolution : GridResolution.resolutions()) {
      loadTerrainHeight(admsVersion, admsData, config.getTerrainHeightFile(admsVersion, resolution), resolution);
    }
    loadMetFiles(admsVersion, admsData, config);
    for (final Substance substance : PLUME_DEPLETION_DEPOSITION_VELOCITY_SUBSTANCES) {
      loadDepositionVelocitiesForPlumeDepletion(admsVersion, admsData, config.getPlumeDepletionFile(admsVersion, substance), substance);
    }
    loadNOxToNO2CalculatorData(admsVersion, admsData, config.getNOxToNO2CalculatorFolder(admsVersion));
  }

  private static void loadDepositionVelocities(final ADMSVersion admsVersion, final ADMSData data, final File depositionVelocityFilepath)
      throws IOException {
    if (!data.containsDepositionVelocityMap(admsVersion)) {
      LOG.info("Loading deposition velocity data from {}.", depositionVelocityFilepath);
      try (final InputStream is = new FileInputStream(depositionVelocityFilepath)) {

        final Map<Substance, Supplier<Double>> defaultValueSuppliers = new EnumMap<>(Substance.class);
        defaultValueSuppliers.put(Substance.NO2, () -> ADMSConstants.DEFAULT_NO2_DEPOSITION_FACTOR);
        defaultValueSuppliers.put(Substance.NH3, () -> ADMSConstants.DEFAULT_NH3_DEPOSITION_FACTOR);
        data.addDepositionVelocityMap(admsVersion,
            DepositionVelocityReader.read(is, ADMSConstants.HEX_HOR, ADMSConstants.getReceptorUtil(), DepositionVelocityMap.class,
                defaultValueSuppliers));
      }
    }
  }

  private static void loadSurfaceRoughness(final ADMSVersion admsVersion, final ADMSData data, final File surfaceRoughnessFilepath)
      throws IOException, FactoryException {
    if (!data.containsSurfaceRoughnessWriter(admsVersion)) {
      LOG.info("Loading surface roughness data from {}.", surfaceRoughnessFilepath);
      data.addSurfaceRoughnessWriter(admsVersion, GeoTiffLoader.loadRaster(ADMSConstants.EPSG.getEpsgCode(), surfaceRoughnessFilepath));
    }
  }

  private static void loadTerrainHeight(final ADMSVersion admsVersion, final ADMSData data, final File terrainHeightFilePath,
      final Integer resolution) throws IOException, FactoryException {
    if (!data.containsTerrainHeightWriter(admsVersion, resolution)) {
      LOG.info("Loading terrain height data from {}.", terrainHeightFilePath);
      data.addTerrainHeightWriter(admsVersion, resolution, GeoTiffLoader.loadRaster(ADMSConstants.EPSG.getEpsgCode(), terrainHeightFilePath));
    }
  }

  private static void loadDepositionVelocitiesForPlumeDepletion(final ADMSVersion admsVersion, final ADMSData data, final File plumeDepletionFile,
      final Substance substance) throws FactoryException, IOException {
    if (!data.containsPlumeDepletionWriters(admsVersion, substance)) {
      LOG.info("Loading velocity deposition data for plume depletion from {}.", plumeDepletionFile);
      data.addPlumeDepletionWriter(admsVersion, substance, GeoTiffLoader.loadRaster(ADMSConstants.EPSG.getEpsgCode(), plumeDepletionFile));
    }
  }

  private static void loadMetFiles(final ADMSVersion admsVersion, final ADMSData data, final ADMSConfiguration config) throws IOException {
    if (!data.containsMetFileStore(admsVersion)) {
      LOG.info("Constructing met file store.");
      data.addMetFileStore(admsVersion, new MetFileStore(new HttpClientProxy(new HttpClientManager()), config, admsVersion));
    }
  }

  private static void loadNOxToNO2CalculatorData(final ADMSVersion admsVersion, final ADMSData data, final File dataFolder) throws IOException {
    if (!data.containsNOxToNO2CalculatorData(admsVersion)) {
      LOG.info("Loading NOx to NO2 calculator data files from {}.", dataFolder);
      data.addNOxToNO2CalculatorData(admsVersion, NOxToNO2CalculatorDataReader.loadData(admsVersion.getNOxToNO2Version(), dataFolder));
    }
  }
}
