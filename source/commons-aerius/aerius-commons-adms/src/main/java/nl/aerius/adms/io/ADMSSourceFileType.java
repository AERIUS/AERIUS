/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;
import java.util.Locale;

/**
 * Type of ADMS input source file.
 */
public enum ADMSSourceFileType {
  APL,
  UPL;

  /**
   * Returns the ADMS source file type given a ADSM file.
   *
   * @param filename
   * @return
   */
  public static ADMSSourceFileType fileTypeFromExtension(final String filename) {
    final int indexOf = filename.lastIndexOf('.');

    if (indexOf < 0) {
      throw new IllegalArgumentException("Not a ADMS .apl or .upl file: " + filename);
    }
    final String extension = filename.substring(indexOf + 1);

    for (final ADMSSourceFileType type : values()) {
      if (type.name().equalsIgnoreCase(extension)) {
        return type;
      }
    }
    throw new IllegalArgumentException("Not a ADMS .apl or .upl file: " + filename);
  }

  public File fileWithExtension(final File file) {
    return new File(file.getAbsoluteFile() + "." + name().toLowerCase(Locale.ROOT));
  }
}
