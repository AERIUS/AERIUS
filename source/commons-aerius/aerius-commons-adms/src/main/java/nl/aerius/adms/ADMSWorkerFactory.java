/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.IOException;
import java.io.Serializable;
import java.util.EnumMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.opengis.referencing.FactoryException;

import nl.aerius.adms.backgrounddata.ADMSData;
import nl.aerius.adms.backgrounddata.ADMSDataBuilder;
import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.domain.ADMSConfigurationBuilder;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.version.ADMSVersion;
import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.processmonitor.ProcessMonitorService;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.WorkerFactory;
import nl.overheid.aerius.worker.WorkerTimeLogger;

/**
 * Worker factory to create new instances of ADMS Workers.
 */
public class ADMSWorkerFactory implements WorkerFactory<ADMSConfiguration> {

  private ExecutorService executor;
  private final ADMSModelDataUtil admsModelDataUtil;

  public ADMSWorkerFactory() {
    this(new HttpClientProxy(new HttpClientManager()));
  }

  public ADMSWorkerFactory(final HttpClientProxy httpClientProxy) {
    admsModelDataUtil = new ADMSModelDataUtil(httpClientProxy);
  }

  @Override
  public ConfigurationBuilder<ADMSConfiguration> configurationBuilder(final Properties properties) {
    return new ADMSConfigurationBuilder(properties, admsModelDataUtil);
  }

  @Override
  public Worker<ADMSInputData, Serializable> createWorkerHandler(final ADMSConfiguration configuration,
      final WorkerConnectionHelper workerConnectionHelper) throws Exception {
    executor = Executors.newFixedThreadPool(configuration.getProcesses());
    return new ADMSWorker(configuration, workerConnectionHelper);
  }

  @Override
  public WorkerType getWorkerType() {
    return WorkerType.ADMS;
  }

  @Override
  public void shutdown() {
    if (executor != null) {
      executor.shutdown();
    }
  }

  private final class ADMSWorker implements Worker<ADMSInputData, Serializable> {
    private final ADMSConfiguration configuration;
    private final ADMSData admsData;
    private final Map<ADMSVersion, ADMSProcessRunner> admsProcessRunners = new EnumMap<>(ADMSVersion.class);
    private final ADMSExportRunner exportADMS;
    private final ProcessMonitorService processMonitorService;

    private ADMSWorker(final ADMSConfiguration configuration, final WorkerConnectionHelper workerConnectionHelper)
        throws IOException, FactoryException {
      this.configuration = configuration;
      admsData = ADMSDataBuilder.build(configuration);
      exportADMS = new ADMSExportRunner(executor, configuration, admsData, workerConnectionHelper.createFileServerProxy(configuration));
      processMonitorService = workerConnectionHelper.getProcessMonitorService();
    }

    @Override
    public Serializable run(final ADMSInputData input, final JobIdentifier jobIdentifier,
        final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
      final WorkerTimeLogger timeLogger = WorkerTimeLogger.start(WorkerType.ADMS.name());

      try {
        final ADMSVersion admsVersion = input.getAdmsVersion();
        admsModelDataUtil.ensureModelDataPresence(configuration, admsVersion);
        ADMSDataBuilder.loadAdditionalData(configuration, admsVersion, admsData);

        if (input.getCommandType() == CommandType.EXPORT) {
          return exportADMS.run(input, jobIdentifier.getJobKey());
        } else {
          final ADMSProcessRunner runADMS;

          if (admsProcessRunners.containsKey(admsVersion)) {
            runADMS = admsProcessRunners.get(admsVersion);
          } else {
            runADMS = new ADMSProcessRunner(executor, configuration, admsData, admsVersion);
            admsProcessRunners.put(admsVersion, runADMS);
          }
          return runADMS.run(input, workerIntermediateResultSender,
              processMonitorService == null ? null : processMonitorService.registerProcessListener(jobIdentifier.getJobKey()));
        }
      } finally {
        timeLogger.logDuration(jobIdentifier, input);
      }
    }
  }
}
