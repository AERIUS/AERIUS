/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.IOException;

import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Class to transform any coordinate system to BNG coordinates, primarily used to convert Irish Grid coordinates.
 */
class EPSGTransformer {

  private final MathTransform transform;

  public EPSGTransformer(final EPSG sourceEPSG) throws FactoryException {
    MathTransform trsfrm;

    if (sourceEPSG == null) {
      trsfrm = null;
    } else {
      try {
        final CoordinateReferenceSystem sourceCRS = CRS.decode(sourceEPSG.getEpsgCode());

        trsfrm = sourceCRS == null ? null : CRS.findMathTransform(sourceCRS, CRS.decode(EPSG.BNG.getEpsgCode()), true);
      } catch (final NoSuchAuthorityCodeException e) {
        trsfrm = null;
      }
    }
    this.transform = trsfrm;
  }

  public void transformCoordinates(final AplFileContent content) {
    if (transform == null) {
      return;
    }
    content.getBuildings().forEach(b -> transform(b.getBuildingCentre()));
    content.getObjects().forEach(this::transformObject);
  }

  private void transformObject(final ADMSSource<?> source) {
    final Geometry geometry = source.getGeometry();
    if (geometry instanceof final Point point) {
      transform(point);
    } else if (geometry instanceof final LineString line) {
      transform(line);
    } else if (geometry instanceof final Polygon polygon) {
      transform(polygon);
    }
  }

  /**
   * Transform x-y coordinates and return a {@link Point}.
   *
   * @param x x-coordinate
   * @param y y-coordinate
   * @return transformed Point
   * @throws IOException
   */
  private void transform(final Point point) {
    point.setCoordinates(transform(point.getX(), point.getY()));
  }

  private void transform(final LineString line) {
    final double[][] coordinates = line.getCoordinates();

    for (int i = 0; i < coordinates.length; i++) {
      coordinates[i] = transform(coordinates[i][0], coordinates[i][1]);
    }
  }

  private void transform(final Polygon polygon) {
    final double[][][] coordinates = polygon.getCoordinates();

    for (int i = 0; i < coordinates.length; i++) {
      for (int j = 0; j < coordinates[0].length; j++) {
        coordinates[i][j] = transform(coordinates[i][j][0], coordinates[i][j][1]);
      }
    }
  }

  /**
   * Transform x-y coordinates and return a coordinate array.
   *
   * @param x x-coordinate
   * @param y y-coordinate
   * @return x-y array, [0] = x, [1] = y
   * @throws IOException
   */
  public double[] transform(final double x, final double y) {
    final double[] sourceCoords = new double[] {x, y};

    if (transform != null) {
      try {
        transform.transform(sourceCoords, 0, sourceCoords, 0, 1);
      } catch (final TransformException e) {
        throw new IllegalArgumentException(e.getMessage());
      }
    }
    return sourceCoords;
  }
}
