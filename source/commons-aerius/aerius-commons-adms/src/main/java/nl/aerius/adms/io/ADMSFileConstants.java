/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;

/**
 * Class to generic ADMS file related constants.
 */
class ADMSFileConstants {
  /**
   * Extension of ADMS surface roughness files.
   */
  public static final String RUF_EXTENSION = ".ruf";

  /**
   * Extension of ADMS terrain files.
   */
  public static final String TER_EXTENSION = ".ter";


  /**
   * Extension of ADMS plume depletion files.
   */
  public static final String DIN_EXTENSION = ".din";

  /**
   * Constructs the name and path of a additional data file to be used in an ADMS calculation.
   *
   * @param outputFile the generic name and path of the ADMS data file
   * @param extension extension to add to the file name
   * @return new file name with given extension
   */
  public static File constructDataFilename(final File outputFile, final String extension) {
    return new File(outputFile.getParentFile(), outputFile.getName() + extension);
  }
}
