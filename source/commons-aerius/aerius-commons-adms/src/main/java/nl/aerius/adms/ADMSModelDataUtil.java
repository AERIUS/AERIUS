/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.util.ModelDataUtil;
import nl.overheid.aerius.worker.ModelDataUtilPreloader;

/**
 * Util to ensure the ADMS model data is present.
 */
public class ADMSModelDataUtil implements ModelDataUtilPreloader<ADMSVersion, ADMSConfiguration> {

  private static final Base64.Decoder BASE64_DECODER = Base64.getDecoder();
  static final String MODEL = "adms";

  private final ModelDataUtil modelDataUtil;

  public ADMSModelDataUtil(final HttpClientProxy httpClientProxy) {
    this.modelDataUtil = new ModelDataUtil(httpClientProxy);
  }

  ADMSModelDataUtil(final ModelDataUtil modelDataUtil) {
    this.modelDataUtil = modelDataUtil;
  }

  @Override
  public synchronized void ensureModelDataPresence(final ADMSConfiguration config) throws Exception {
    for (final ADMSVersion admsVersion : config.getModelPreloadVersions()) {
      ensureModelDataPresence(config, admsVersion);
    }
  }

  public synchronized void ensureModelDataPresence(final ADMSConfiguration config, final ADMSVersion admsVersion) throws Exception {
    modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL, admsVersion.getVersion(),
        config.getVersionsRoot().getAbsolutePath(), path -> true, () -> {});
    modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL, admsVersion.getDepositionVelocityVersion(),
        config.getVersionsRoot().getAbsolutePath(), path -> true, () -> {});
    modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL, admsVersion.getSurfaceRoughnessVersion(),
        config.getVersionsRoot().getAbsolutePath(), path -> true, () -> {});
    modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL, admsVersion.getPlumeDepletionVersionFormat(),
        config.getVersionsRoot().getAbsolutePath(), path -> true, () -> {});
    writeLicenseFile(config, admsVersion);

    modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL, admsVersion.getTerrainHeightVersionFormat(),
        config.getVersionsRoot().getAbsolutePath(), path -> true, () -> {});
    modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL, admsVersion.getNOxToNO2Version().getDataDirectory(),
        config.getVersionsRoot().getAbsolutePath(), path -> true, () -> {});
  }

  private static void writeLicenseFile(final ADMSConfiguration config, final ADMSVersion admsVersion) throws IOException {
    final File admsRoot = new File(config.getVersionsRoot(), admsVersion.getVersion());
    final File licenseFile = new File(admsRoot, ADMSExecutor.ADMS_URBAN_LIC_NAME);

    if (!licenseFile.exists() && config.getAdmsLicenseBase64().isPresent()) {
      try (final OutputStream stream = new FileOutputStream(licenseFile)) {
        stream.write(BASE64_DECODER.decode(config.getAdmsLicenseBase64().get()));
      }
    }
  }
}
