/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.util;

import java.util.Collection;

import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;

/**
 * Util class for calculating the (maximum) dimension of source(s)
 */
final class DimensionUtil {

  private DimensionUtil() {
    // Util class.
  }

  /**
   * Determine the maximum dimension for a map of a collection of sources.
   * Filters out only the {@link ADMSSource} objects to determine the max dimension.
   *
   * @param sources engine sources
   * @return Maximum dimension for the supplied sources.
   */
  public static double determineMaxDimension(final Collection<EngineSource> engineSources) {
    if (engineSources == null) {
      return 0.0;
    }
    return engineSources.stream()
        .filter(ADMSSource.class::isInstance)
        .map(ADMSSource.class::cast)
        .mapToDouble(DimensionUtil::toMaxDimension)
        .max().orElse(0.0);
  }

  /**
   * Determine the maximum dimension for a source
   * This is the maximum of the following for all sources:
   * - Height
   * - Diameter (point/yet sources)
   * - Width (line/road sources)
   * - Segment length, or length between 2 vertices in linestring or area
   * - Diagonal of an area or volume (?)
   *
   * @param engineSource engine source
   * @return Maximum dimension for the supplied source.
   */
  public static double toMaxDimension(final ADMSSource<?> admsSource) {
    if (admsSource == null) {
      return 0.0;
    }
    final double sourceHeight = admsSource.getHeight();
    final double sourceMaxDimension = switch (admsSource.getSourceType()) {
        case POINT, JET -> Math.max(sourceHeight, admsSource.getDiameter());
        // L1 = width for line and road
        // L1 = vertical dimension for volume
        case LINE, ROAD, VOLUME -> Math.max(admsSource.getHeight(), admsSource.getL1());
        default -> admsSource.getHeight();
    };
    final double maxDimension;

    if (admsSource.getGeometry() instanceof final LineString lineString) {
      maxDimension = Math.max(sourceMaxDimension, toMaxLineStringDimension(lineString));
    } else if (admsSource.getGeometry() instanceof final Polygon polygon) {
      maxDimension = Math.max(sourceMaxDimension, toMaxPolygonDimension(polygon));
    } else {
      // Nothing for other geometry types
      maxDimension = sourceMaxDimension;
    }
    return maxDimension;
  }

  /**
   * From a mail from CERC:
   * For road sources, this is the maximum distance between consecutive vertices of the road source.
   */
  private static double toMaxLineStringDimension(final LineString lineString) {
    double maxVerticesDistance = 0.0;
    final double[][] coords = lineString.getCoordinates();
    // For good measure, ensure coordinates are valid (this should already be checked before).
    if (coords.length > 1) {
      for (int i = 0; i < coords.length - 1; i++) {
        maxVerticesDistance = Math.max(maxVerticesDistance, toLength(coords[i], coords[i + 1]));
      }
    }
    return maxVerticesDistance;
  }

  /**
   * From a mail from CERC:
   * For area or volume sources the maximum dimension is the maximum distance for any two pairs of vertices.
   * The checking is over all pairs of vertices, not just consecutive ones,
   * so the diagonal length would be included in the checking for a rectangular source.
   */
  private static double toMaxPolygonDimension(final Polygon polygon) {
    double maxVerticesDistance = 0.0;
    final double[][][] polyCoords = polygon.getCoordinates();
    for (final double[][] coords : polyCoords) {
      maxVerticesDistance = Math.max(maxVerticesDistance, toMaxDistanceBetweenAllCoordinates(coords));
    }
    return maxVerticesDistance;
  }

  private static double toMaxDistanceBetweenAllCoordinates(final double[][] coords) {
    double maxVerticesDistance = 0.0;
    // The goal is to determine distance from every coordinate to every other coordinate, so can do 2 loops,
    // where the inner loop only covers all later coords.
    // Don't have to compare coordinate to itself, and distance from 1 to 2 should be the same as from 2 to 1.
    // Assuming in this case that only have to look at vertices within the ring (usually have 1 ring anyway).
    // For good measure, ensure coordinates are valid (this should already be checked before).
    if (coords.length > 1) {
      for (int i = 0; i < coords.length - 1; i++) {
        for (int j = i + 1; j < coords.length; j++) {
          maxVerticesDistance = Math.max(maxVerticesDistance, toLength(coords[i], coords[j]));
        }
      }
    }
    return maxVerticesDistance;
  }

  private static double toLength(final double[] startCoord, final double[] endCoord) {
    final double xDiff = Math.abs(startCoord[0] - endCoord[0]);
    final double yDiff = Math.abs(startCoord[1] - endCoord[1]);
    return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
  }
}
