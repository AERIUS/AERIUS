/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion.nox2;

/**
 * Photochemical reaction calculation for very small concentrations NOx to NO2.
 * NOTE: The algorithm implemtend here is taken from the spreadsheet NOx_to_NO2_Calculator_v8.1_noprotection.xlsm
 * It has been converted from Excell Visual Basic to Java. However the algorithm has been kept as much as possible to be the same as
 * the original Visual Basic code to keep it as transparent as possible. This means it's a long method, not compilent with the coding standards.
 */
class Photochemical {

  private static final double NUMBER = 2.59378E+16;

  /**
   * Photochemical reaction calculation for very small concentrations NOx to NO2.
   * Converts NOx to NO2 assuming photostationary state only used if background concentrations are less than the regional background.
   *
   * @param afac
   * @param noxSite
   * @param no2Back
   * @param noxBack
   * @param o3Back
   * @param k1
   * @param j
   * @return
   */
  public static PhotoResult photoNOxtoNO2(final double afac, final double noxSite, final double no2Back, final double noxBack, final double o3Back,
      final double k1, final double j) {

    final double NOxb = NUMBER * noxBack;
    final double O3b = NUMBER * o3Back;
    final double NO2b = NUMBER * no2Back;

    final double NOx = NUMBER * noxSite;

    // Solution of quadratic equation

    final double a = k1;
    final double b = -k1 * NOx - k1 * afac * NOx + k1 * afac * NOxb - k1 * O3b - k1 * NO2b - j;
    final double c = k1 * afac * NOx * NOx - k1 * afac * NOxb * NOx + k1 * NOx * O3b + k1 * NOx * NO2b;

    final double NO2 = (-b - Math.sqrt(b * b - 4D * a * c)) / 2D / a;
    final double O3 = j * NO2 / k1 / (NOx - NO2);

    return new PhotoResult(NO2 / NUMBER, O3 / NUMBER);
  }

  public static class PhotoResult {
    private final double no;
    private final double o3;

    public PhotoResult(final double no, final double o3) {
      this.no = no;
      this.o3 = o3;
    }

    public double getNO() {
      return no;
    }

    public double getO3() {
      return o3;
    }
  }
}
