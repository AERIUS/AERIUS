/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import nl.aerius.adms.io.ADMSSourceFileType;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;

/**
 * Input file for ADMS calculation.
 */
public class ADMSInputData extends EngineInputData<EngineSource, ADMSCalculationPoint> {

  private static final long serialVersionUID = 7L;

  private ADMSSourceFileType fileType;
  private String timeZone;
  private boolean quickRun;

  private ADMSOptions admsOptions;

  private ADMSVersion admsVersion;

  /**
   * Optional provide an ADMS license file.
   */
  private byte[] license;

  private String meteoYear;
  private BBox extent;

  public ADMSInputData(final ADMSEngineDataKey engineDataKey, final CommandType commandType) {
    super(Theme.NCA, engineDataKey, ADMSVersion.NCA_LATEST, commandType);
  }

  public ADMSSourceFileType getFileType() {
    return fileType;
  }

  public void setFileType(final ADMSSourceFileType fileType) {
    this.fileType = fileType;
  }

  public String getTimeZone() {
    return timeZone;
  }

  public void setTimeZone(final String timeZone) {
    this.timeZone = timeZone;
  }

  public boolean isQuickRun() {
    return quickRun;
  }

  public void setQuickRun(final boolean quickRun) {
    this.quickRun = quickRun;
  }

  public ADMSOptions getAdmsOptions() {
    return admsOptions;
  }

  public void setAdmsOptions(final ADMSOptions admsOptions) {
    this.admsOptions = admsOptions;
  }

  public byte[] getLicense() {
    return license;
  }

  public void setLicense(final byte[] license) {
    this.license = license;
  }

  public ADMSVersion getAdmsVersion() {
    return admsVersion;
  }

  public void setAdmsVersion(final ADMSVersion admsVersion) {
    this.admsVersion = admsVersion;
  }

  public String getMeteoYear() {
    return meteoYear;
  }

  public void setMeteoYear(final String meteoYear) {
    this.meteoYear = meteoYear;
  }

  public BBox getExtent() {
    return extent;
  }

  public void setExtent(final BBox extent) {
    this.extent = extent;
  }
}
