/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.HasName;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;

public class ADMSTimeVaryingProfile implements EngineSource, HasName {

  private static final long serialVersionUID = 3L;

  private String name;
  private CustomTimeVaryingProfileType type;
  private List<Double> values = new ArrayList<>();

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  public CustomTimeVaryingProfileType getType() {
    return type;
  }

  public void setType(final CustomTimeVaryingProfileType type) {
    this.type = type;
  }

  public List<Double> getValues() {
    return values;
  }

  public void setValues(final List<Double> values) {
    this.values = values;
  }

  @Override
  public String toString() {
    return "ADMSTimeVaryingProfile [name=" + name + ", type=" + type + "]";
  }
}
