/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import nl.aerius.adms.domain.ADMSSource;

/**
 * Class to make name of the source unique.
 */
class ADMSUniqueSourceNames {

  private static final int MAX_NAME_LENGTH = 30;
  private final Map<String, AtomicInteger> duplicationCounterMap = new HashMap<>();
  private final Set<String> sourceSet = new HashSet<>();
  private final Map<String, ADMSSource<?>> renamedMap = new HashMap<>();
  private final Map<String, String> renamedOrgNameMap = new HashMap<>();

  /**
   * Sets the name on the source. If the name is a duplicated name it will append the name with .&lt;number>.
   * The number starts with 1, and is increased with each duplicate found.
   * The first source that turned out to be duplicated will keep it's original name.
   * If the renamed name of a source already exists it will increment the number and try again if that name is unique.
   * If an original name turns out to be already given to a renamed source the renamed source name is renamed again, and the source with
   * the original name keeps its name.
   *
   * @param source source to set the name
   * @param name name to set on the source
   */
  public void setUniqueName(final ADMSSource<?> source, final String name) {
    final String cutOffName = cutOfName(name, MAX_NAME_LENGTH);

    if (sourceSet.contains(cutOffName)) {
      final int incementId = duplicationCounterMap.computeIfAbsent(name, k -> new AtomicInteger()).incrementAndGet();
      final String renamedName = cutOfName(name, MAX_NAME_LENGTH - (int) (2 + Math.log10(incementId))) + "." + incementId;

      if (sourceSet.contains(renamedName)) {
        setUniqueName(source, name);
      } else {
        source.setName(renamedName);
        renamedMap.put(renamedName, source);
        renamedOrgNameMap.put(renamedName, name);
      }
    } else {

      sourceSet.add(cutOffName);
      // check if in rename
      if (renamedMap.containsKey(cutOffName)) {
        setUniqueName(renamedMap.remove(cutOffName), renamedOrgNameMap.remove(cutOffName));
      }
      source.setName(cutOffName);
    }
  }

  private String cutOfName(final String name, final int maxNameLength) {
    return name.substring(0, Math.min(name.length(), maxNameLength));
  }
}
