/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSSource;
import nl.aerius.adms.domain.ADMSTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.EngineSource;

/**
 * Data record with a builder to split the generic list of sources received from the Chunker into the specific source objects.
 */
public record ADMSEngineSources(Collection<ADMSSource<?>> sources, List<ADMSBuilding> buildings,
    Collection<ADMSTimeVaryingProfile> timeVaryingProfiles) {

  public static final Builder builder() {
    return new Builder();
  }

  /**
   * Builder to create the {@link ADMSEngineSources} class that splits the generic sources.
   */
  public static class Builder {

    private Collection<ADMSSource<?>> sources;
    private List<ADMSBuilding> buildings;
    private Collection<ADMSTimeVaryingProfile> timeVaryingProfiles;

    /**
     * Set sources, buildings and time-varying profiles from the given list of generic {@link EngineSource} objects.
     *
     * @param engineSources collection of Engine sources.
     */
    public Builder set(final Collection<EngineSource> engineSources) {
      failOnNonNullSources();
      sources = new ArrayList<>();
      buildings = new ArrayList<>();
      timeVaryingProfiles = new ArrayList<>();
      for (final EngineSource engineSource : engineSources) {
        if (engineSource instanceof final ADMSSource<?> amdsSource) {
          sources.add(amdsSource);
        } else if (engineSource instanceof final ADMSBuilding building) {
          buildings.add(building);
        } else if (engineSource instanceof final ADMSTimeVaryingProfile timeProfile) {
          timeVaryingProfiles.add(timeProfile);
        }
      }
      return this;
    }

    /**
     * Set sources, buildings and time-varying profiles from the given adms engine sources.
     *
     * @param admsEngineSources object to get data from.
     */
    public Builder set(final ADMSEngineSources admsEngineSources) {
      failOnNonNullSources();
      sources = admsEngineSources.sources();
      buildings = admsEngineSources.buildings();
      timeVaryingProfiles = admsEngineSources.timeVaryingProfiles();
      return this;
    }

    private void failOnNonNullSources() {
      assert sources == null : "Sources already set. If setSources used, call set method first before calling this method";
    }

    /**
     * Set {@link ADMSSource} objects.
     *
     * @param sources sources to set
     */
    public Builder setSources(final Collection<ADMSSource<?>> sources) {
      this.sources = sources;
      return this;
    }

    /**
     * @return Creates the {@link ADMSEngineSources} object.
     */
    public ADMSEngineSources build() {
      assert sources != null && buildings != null && timeVaryingProfiles != null : "Not all engines sources initialised";
      return new ADMSEngineSources(sources, buildings, timeVaryingProfiles);
    }
  }
}
