/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.util.GeometryCalculatorImpl;

/**
 * <p>Util class to calculate building length to porosity and the other way around.
 *
 * <p>Note that "porosity" will be entered in the UI as a percentage, but will need to be converted to a fraction (divide by 100) to be used with
 * this equation.
 */
public class ADMSPorosityCalculator {

  private static final int ROAD_LENGTH_SCALE = 2;
  private static final int POROSITY_SCALE = 5;
  private static final int BUILDING_LENGTH_SCALE = 2;
  private static final BigDecimal HUNDERD = BigDecimal.valueOf(100);
  private static final BigDecimal HUNDERD_INV = BigDecimal.valueOf(0.01);

  private static final GeometryCalculator GEOMETRY_CALCULATOR = new GeometryCalculatorImpl();

  /**
   * Sets the "pository" on the Road Barrier. The pository will be in percentage.
   *
   * <p><code>α = 100 * (1 -  LB / LR)</code>
   *
   * <p>Where:
   * <ul>
   * <li><code>α</code> is the porosity,
   * <li>code>LB</code> is the length of road with adjacent buildings
   * <li>code>LR</code> is the total length of the road the street canyon is attached to.
   *
   * @param road road from the total length of the road the street canyon is attached to.
   * @Param roadLengthWithBuildings length of road with adjacent buildings
   * @return the porosity in percentage
   * @throws AeriusException
   */
  public static double toPorosity(final LineString road, final double roadLengthWithBuildings) throws AeriusException {
    final BigDecimal length = BigDecimal.valueOf(GEOMETRY_CALCULATOR.determineMeasure(road)).setScale(ROAD_LENGTH_SCALE, RoundingMode.HALF_UP);

    return Math.max(0.0, HUNDERD.multiply(BigDecimal.ONE.subtract(BigDecimal.valueOf(roadLengthWithBuildings)
        .divide(length, MathContext.DECIMAL64))).setScale(POROSITY_SCALE, RoundingMode.HALF_UP).doubleValue());
  }

  /**
   * The building length (referred to as LB) in the formula below) need to be calculated from the "porosity" value (α, input by the user) and the
   * length of the road segment (LR, from the line geometry), using the following equation:
   *
   * <p><code>LB = LR * (1 - (α / 100))</code>
   *
   * <p>Where:
   * <ul>
   * <li><code>α</code> is the porosity,
   * <li>code>LB</code> is the length of road with adjacent buildings
   * <li>code>LR</code> is the total length of the road the street canyon is attached to.
   *
   * @param road road from the total length of the road the street canyon is attached to.
   * @param porosity the porosity in percentage
   * @return Length of road with adjacent buildings
   * @throws AeriusException
   */
  public static double toRoadLengthWithBuildings(final LineString road, final double porosity) throws AeriusException {
    return BigDecimal.valueOf(GEOMETRY_CALCULATOR.determineMeasure(road)).setScale(ROAD_LENGTH_SCALE, RoundingMode.HALF_UP)
        .multiply(BigDecimal.ONE.subtract(HUNDERD_INV.multiply(BigDecimal.valueOf(porosity))))
        .setScale(BUILDING_LENGTH_SCALE, RoundingMode.HALF_UP).doubleValue();
  }
}
