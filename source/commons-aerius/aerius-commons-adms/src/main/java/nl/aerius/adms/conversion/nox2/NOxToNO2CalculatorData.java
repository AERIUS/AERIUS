/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion.nox2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.index.strtree.ItemBoundable;
import org.locationtech.jts.index.strtree.STRtree;
import org.opengis.feature.simple.SimpleFeature;

import nl.aerius.adms.version.NOxToNO2Version;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Data object containing the local authority background data for NO2, NOx and O3 background concentrations,
 * a geometric map of local authority areas and fraction NO2 emitted.
 */
public class NOxToNO2CalculatorData {

  private static final int GEOMETRY_ATTRIBUTE = 0;
  private static final int DATA_ATTRIBUTE = 1;

  // Map<uwe code, Map<Year, LACodeBackgroundConcentration>>
  private final Map<Long, Map<Integer, LACodeBackgroundConcentration>> backgroundMap = new HashMap<>();

  // Map<Area from csv, Map<Year, fNO2>>
  private final Map<String, Map<Integer, FractionNO2>> fNO2Map = new HashMap<>();

  private final STRtree uweTree;
  private final STRtree fNO2AreaTree;
  private final NOxToNO2Version version;

  public NOxToNO2CalculatorData(final NOxToNO2Version version, final SimpleFeatureSource uweFeatureSource,
      final SimpleFeatureSource fNO2AreaFeatureSource) throws IOException {
    this.version = version;
    uweTree = buildTree(uweFeatureSource);
    fNO2AreaTree = buildTree(fNO2AreaFeatureSource);
  }

  private static <T> STRtree buildTree(final SimpleFeatureSource featureSource) throws IOException {
    final STRtree tree = new STRtree();
    final SimpleFeatureCollection features = featureSource.getFeatures();

    try (SimpleFeatureIterator iterator = features.features()) {
      while (iterator.hasNext()) {
        final SimpleFeature feature = iterator.next();
        final Geometry geometry = (Geometry) feature.getAttribute(GEOMETRY_ATTRIBUTE);

        tree.insert(geometry.getEnvelopeInternal(), new TreeObject<T>(geometry, (T) feature.getAttribute(DATA_ATTRIBUTE)));
      }
    }
    tree.build();
    return tree;
  }

  public NOxToNO2Version getVersion() {
    return version;
  }

  /**
   * Returns the year specific background concentration NO2, NOx and O3 for the local authority the x and y coordinate is located in.
   * If the year is earlier than the first year the data for the first year is returned.
   * If the year is later than the last year the data for the last year is returned.
   * If no data could be found null is returned.
   *
   * @param year year to get the background data for
   * @param point point to determine local authority code for
   * @return the background concentration of null if it could not be determined
   */
  public LACodeBackgroundConcentration getBackgroundConcentration(final int year, final Point point) {
    try {
      final Long uweCode = getUweCode(point);

      if (uweCode == null) {
        return null;
      }
      return getBackgroundConcentration(year, uweCode);
    } catch (final AeriusException e) {
      return null;
    }
  }

  /**
   * Returns the local authority code for the given x and y coordinate.
   *
   * @param point point to determine local authority code for
   * @return local authority code
   * @throws AeriusException
   */
  Long getUweCode(final Point point) throws AeriusException {
    return getDataFromTree(point, uweTree, false);
  }

  /**
   * Returns the background concentrations for the given year and local authority code.
   * If the year is earlier than the first year the data for the first year is returned.
   * If the year is later than the last year the data for the last year is returned.
   *
   * @param year year to get concentrations for
   * @param uweCode local authority code to get concentrations for
   * @return background concentration or null if local authority code could not be found
   */
  LACodeBackgroundConcentration getBackgroundConcentration(final int year, final Long uweCode) {
    final Map<Integer, LACodeBackgroundConcentration> uweMap = backgroundMap.get(uweCode);

    if (uweMap == null) {
      return null;
    } else {
      return uweMap.get(Math.min(version.getLACodeLastYear(), Math.max(version.getLACodeFirstYear(), year)));
    }
  }

  /**
   * Store background concentration for a local authority on a year for the given substance
   *
   * @param uweCode local authoritiy code
   * @param year year year the bacground conceration value
   * @param substance substance the substance of the bacground concentration
   * @param value background concentration value
   */
  public void putBackground(final Long uweCode, final Integer year, final Substance substance, final double value) {
    backgroundMap.computeIfAbsent(uweCode, uc -> new HashMap<>()).computeIfAbsent(year,
        y -> new LACodeBackgroundConcentration()).put(substance, value);
  }

  /**
   * Returns the fNO2 factors for the given year at the given location.
   *
   * @param year year to get the fNO2 factor for
   * @param point point to determine fNO2 factor for
   * @return fNO2 factors or null in case it could not be determined what the factor is.
   * @throws AeriusException
   */
  public FractionNO2 getFractionNO2(final int year, final Point point) {
    try {
      return getFractionNO2(year, getFractionNO2Area(point));
    } catch (final AeriusException e) {
      return null;
    }
  }

  /**
   * Returns the area that is to be used to get the fNO2 factors from.
   * This method returns the area name as would be expected to be in the fNO2 csv file.
   *
   * @param point point to determine fNO2 factor for
   * @return csv fNO2 area name
   * @throws AeriusException
   */
  String getFractionNO2Area(final Point point) throws AeriusException {
    return version.getFNO2Area(getDataFromTree(point, fNO2AreaTree, true));
  }

  /**
   * Returns the fNO2 factors for the given year and csv fNO2 area name.
   * If the year is earlier than the first year the data for the first year is returned.
   * If the year is later than the last year the data for the last year is returned.
   * If no data available it returns null.
   *
   * @param year year to get the fNO2 factor for
   * @param area csv fNO2 area name
   * @return fNO2 factors or null if no data available
   */
  FractionNO2 getFractionNO2(final int year, final String area) {
    final Map<Integer, FractionNO2> fNO2 = fNO2Map.get(area);

    if (fNO2 == null) {
      return null;
    } else {
      return fNO2.get(Math.min(version.getfNO2LastYear(), Math.max(version.getfNO2FirstYear(), year)));
    }
  }

  /**
   * Stores the fNO2 fraction for the given csv area name and year.
   *
   * @param area fNO2 csv area name
   * @param year year the data is for
   * @param value fNO2 to store
   */
  public void putFractionNO2(final String area, final Integer year, final double value) {
    fNO2Map.computeIfAbsent(area, a -> new HashMap<>()).put(year, new FractionNO2(value, value));
  }

  /**
   * Finds the nearest Neighbour in the tree for the point.
   * If data is found it will additional, if withinCheck is true, only return the data if the point is within the found geometry,
   * and if withinCheck is false it will return the found data.
   *
   * @param point point to find
   * @param tree tree to traverse
   * @param withinCheck if true performs an additional within check.
   * @return data found in the tree or null if not found
   */
  private static <T> T getDataFromTree(final Point point, final STRtree tree, final boolean withinCheck) throws AeriusException {
    final Geometry pointGeometry = GeometryUtil.getGeometry(point);
    final Object nearestNeighbour = tree.nearestNeighbour(pointGeometry.getEnvelopeInternal(), pointGeometry, NOxToNO2CalculatorData::distance);

    if (nearestNeighbour instanceof TreeObject) {
      final TreeObject<T> item = (TreeObject<T>) nearestNeighbour;

      return withinCheck && !pointGeometry.within(item.geometry) ? null : item.getData();
    } else {
      return null;
    }
  }

  private static double distance(final ItemBoundable item1, final ItemBoundable item2) {
    if (item1 == item2) {
      return Double.MAX_VALUE;
    }
    final Geometry g1 = ((TreeObject<?>) item1.getItem()).getGeometry();
    final Geometry g2 = (Geometry) item2.getItem();
    return g1.distance(g2);
  }


  /**
   * Data class to store local authority specific NOx, NO2 and O3 background concentrations.
   */
  public static class LACodeBackgroundConcentration {
    private double o3;
    private double nox;
    private double no2;

    public LACodeBackgroundConcentration put(final Substance substance, final double value) {
      switch (substance) {
      case NOX:
        nox = value;
        break;
      case NO2:
        no2 = value;
        break;
      case O3:
        o3 = value;
        break;
      default:
      }
      return this;
    }

    public double getO3() {
      return o3;
    }

    public double getNOx() {
      return nox;
    }

    public double getNO2() {
      return no2;
    }
  }

  /**
   * Data class to store local and regional Fraction NO2.
   */
  static record FractionNO2(double local, double regional) {
  }

  private static class TreeObject<T> {
    private final Geometry geometry;
    private final T data;

    TreeObject(final Geometry geometry, final T data) {
      this.geometry = geometry;
      this.data = data;
    }

    public Geometry getGeometry() {
      return geometry;
    }

    public T getData() {
      return data;
    }
  }
}
