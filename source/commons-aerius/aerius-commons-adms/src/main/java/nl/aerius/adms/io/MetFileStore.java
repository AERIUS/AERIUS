/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;

import nl.aerius.adms.backgrounddata.MetDataFileKey;
import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.util.ModelDataUtil;

/**
 * Store for Met files.
 * Can download files from remote location if not present on file system yet.
 */
public class MetFileStore {

  private static final String MODEL = "adms";

  private final ModelDataUtil modelDataUtil;
  private final ADMSConfiguration config;
  private final ADMSVersion version;

  public MetFileStore(final HttpClientProxy httpClientProxy, final ADMSConfiguration config, final ADMSVersion version) {
    this.modelDataUtil = new ModelDataUtil(httpClientProxy);
    this.config = config;
    this.version = version;
  }

  public File getFile(final MetDataFileKey key) {
    return modelDataUtil.ensureModelDataFileAvailable(config.getModelDataUrl(), MODEL, version.getMetFilesVersion(), key.getFileName(),
        config.getVersionsRoot().getAbsolutePath());
  }

}
