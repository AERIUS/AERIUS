/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.HasName;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;

/**
 * Base class containing all ADMS Source parameters.
 *
 * Descriptions of field variables taken form ADMS User guide.
 *
 * @param <P> geometry type
 */
public class ADMSSource<P extends Geometry> extends EngineEmissionSource implements HasName {

  private static final long serialVersionUID = 4L;

  private P geometry;
  private String name;
  private String mainBuilding;
  private double height;
  private double diameter;
  private double volumetricFlowRate = ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_DEFAULT;
  private double verticalVelocity = ADMSLimits.SOURCE_VERTICAL_VELOCITY_DEFAULT;
  private double temperature = ADMSLimits.SOURCE_TEMPERATURE_DEFAULT;
  private double molWeight = ADMSLimits.SOURCE_MOL_WEIGHT_DEFAULT;
  private double density  = ADMSLimits.SOURCE_DENSITY_DEFAULT;
  private double specificHeatCapacity = ADMSLimits.SOURCE_SPECIFIC_HEAT_CAPACITY_DEFAULT;
  private SourceType sourceType;
  private int releaseAtNTP;
  private int effluxType;
  private int buoyancyType;
  private double l1;
  private double l2;

  private int numGroups;
  private int numVertices;
  // Unknown how the following values should be interpreted therefor left in comment for now.
  // SrcPolEmissionRate = 1.0e+0 2.0e+0
  // SrcPolTotalemission = 1.0e+0 1.0e+0
  // SrcPolStartTime = 0.0e+0 0.0e+0
  // SrcPolDuration = 0.0e+0 0.0e+0
  private int numIsotopes;

  // APL parameters:
  private double percentNOxAsNO2;
  private double momentumFlux = ADMSLimits.SOURCE_MOMENTUM_FLUX_DEFAULT;
  private double buoyancyFlux = ADMSLimits.SOURCE_BUOYANCY_FLUX_DEFAULT;
  private double massFlux = ADMSLimits.SOURCE_MASS_FLUX_DEFAULT;
  private double angle1;
  private double angle2;
  private double massH2O;
  private int useVARFile;

  /**
   * {3,7}-day time-varying profile.
   */
  private String hourlyTimeVaryingProfileName;
  /**
   * Monthly time-varying profile.
   */
  private String monthlyTimeVaryingProfileName;


  public P getGeometry() {
    return geometry;
  }

  public void setGeometry(final P geometry) {
    this.geometry = geometry;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  public String getMainBuilding() {
    return mainBuilding;
  }

  public void setMainBuilding(final String mainBuilding) {
    this.mainBuilding = mainBuilding;
  }

  /**
   * Height (m): height of source above the ground. For a volume source, it is the mid-height of the volume above the ground.
   * Minimum = 0.0005 m (0 is allowed by ADMS)
   * Maximum = 15 000 m
   * Default = 0.5 m
   */
  @Min(value = 0, message = "adms height > " + ADMSLimits.SOURCE_HEIGHT_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_HEIGHT_MAXIMUM, message = "adms height < " + ADMSLimits.SOURCE_HEIGHT_MAXIMUM)
  public double getHeight() {
    return height;
  }

  public void setHeight(final double height) {
    this.height = height;
  }

  /**
   * Diameter (m): internal diameter of a point/jet source. 0 if other source.
   *
   * Minimum = 0.001 m (0 for non point/jet sources)
   * Maximum = 100 m
   */
  @Min(value = 0, message = "adms diameter > " + ADMSLimits.SOURCE_DIAMETER_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_DIAMETER_MAXIMUM, message = "adms diameter < " + ADMSLimits.SOURCE_DIAMETER_MAXIMUM)
  public double getDiameter() {
    return diameter;
  }

  public void setDiameter(final double diameter) {
    this.diameter = diameter;
  }

  /**
   * Volume flux (m3/s): volume flow rate of the release (in m3/s if {@link #releaseAtNTP} is 0 (Actual) or in Nm3/s if is 1 (NTP)).
   * Minimum = 0 m3/s
   * Maximum = 10^9 m3/s
   * Default = 11.781 m3/s
   */
  @Min(value = ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_MINIMUM, message = "adms volumetric flow rate > " + ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_MAXIMUM, message = "adms volumetric flow rate < " + ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_MAXIMUM)
  public double getVolumetricFlowRate() {
    return volumetricFlowRate;
  }

  public void setVolumetricFlowRate(final double volumetricFlowRate) {
    this.volumetricFlowRate = volumetricFlowRate;
  }

  /**
   * Velocity (m/s): vertical velocity of release at source exit (in m/s if {@link #releaseAtNTP} is 0 (Actual) or in Nm/s if is 1 (NTP)).
   * Minimum = 0 m/s
   * Maximum = 1 000 m/s
   * Default = 15 m/s
   */
  @Min(value = ADMSLimits.SOURCE_VERTICAL_VELOCITY_MINIMUM, message = "adms vertical velocity > " + ADMSLimits.SOURCE_VERTICAL_VELOCITY_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_VERTICAL_VELOCITY_MAXIMUM, message = "adms vertical velocity < " + ADMSLimits.SOURCE_VERTICAL_VELOCITY_MAXIMUM)
  public double getVerticalVelocity() {
    return verticalVelocity;
  }

  public void setVerticalVelocity(final double verticalVelocity) {
    this.verticalVelocity = verticalVelocity;
  }

  /**
   * Temperature (°C): temperature of the release.
   * Minimum = -100°C
   * Maximum = 5 000°C
   * Default = 15°C
   */
  @Min(value = ADMSLimits.SOURCE_TEMPERATURE_MINIMUM, message = "adms temperature > " + ADMSLimits.SOURCE_TEMPERATURE_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_TEMPERATURE_MAXIMUM, message = "adms temperature < " + ADMSLimits.SOURCE_TEMPERATURE_MAXIMUM)
  public double getTemperature() {
    return temperature;
  }

  public void setTemperature(final double temperature) {
    this.temperature = temperature;
  }

  /**
   * Mol. mass (g): molecular mass of the release material (mass of one mole of the material).
   * Minimum = 1 g
   * Maximum = 300 g
   * Default = 28.966 g (typical value for air)
   */
  @Min(value = ADMSLimits.SOURCE_MOL_WEIGHT_MINIMUM, message = "adms mol weight > " + ADMSLimits.SOURCE_MOL_WEIGHT_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_MOL_WEIGHT_MAXIMUM, message = "adms mol weight < " + ADMSLimits.SOURCE_MOL_WEIGHT_MAXIMUM)
  public double getMolWeight() {
    return molWeight;
  }

  public void setMolWeight(final double molWeight) {
    this.molWeight = molWeight;
  }

  /**
   * Density (kg/m3): density of the whole release.
   * Minimum = 0.01 kg/m3  (0 for those cases where it isn't required)
   * Maximum = 2 kg/m3
   * Default = 1.225 kg/m3
   */
  @Min(value = 0, message = "adms density > " + ADMSLimits.SOURCE_DENSITY_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_DENSITY_MAXIMUM, message = "adms density < " + ADMSLimits.SOURCE_DENSITY_MAXIMUM)
  public double getDensity() {
    return density;
  }

  public void setDensity(final double density) {
    this.density = density;
  }

  /**
   * Cp (J/°C/kg): specific heat capacity of the source material.
   * Minimum = 1 J/kg/°C
   * Maximum = 10^5 J/kg/°C
   * Default = 1012 J/kg/°C (typical value for air)
   */
  @Min(value = ADMSLimits.SOURCE_SPECIFIC_HEAT_CAPACITY_MINIMUM, message = "adms specific heat capacity > " + ADMSLimits.SOURCE_SPECIFIC_HEAT_CAPACITY_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_SPECIFIC_HEAT_CAPACITY_MAXIMUM, message = "adms specific heat capacity < " + ADMSLimits.SOURCE_SPECIFIC_HEAT_CAPACITY_MAXIMUM)
  public double getSpecificHeatCapacity() {
    return specificHeatCapacity;
  }

  public void setSpecificHeatCapacity(final double specificHeatCapacity) {
    this.specificHeatCapacity = specificHeatCapacity;
  }

  public SourceType getSourceType() {
    return sourceType;
  }

  public void setSourceType(final SourceType sourceType) {
    this.sourceType = sourceType;
  }

  public int getReleaseAtNTP() {
    return releaseAtNTP;
  }

  public void setReleaseAtNTP(final int releaseAtNTP) {
    this.releaseAtNTP = releaseAtNTP;
  }

  public int getEffluxType() {
    return effluxType;
  }

  public void setEffluxType(final int effluxType) {
    this.effluxType = effluxType;
  }

  public int getBuoyancyType() {
    return buoyancyType;
  }

  public void setBuoyancyType(final int buoyancyType) {
    this.buoyancyType = buoyancyType;
  }

  /**
   * L1 (m): width of a line source or vertical dimension of a volume source.
   * Minimum = 0.001 m (0 for those cases where it isn't required)
   * Maximum = 1 000 m
   * Default = 1 m
   */
  @Min(value = 0, message = "adms L1 > " + ADMSLimits.SOURCE_L1_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_L1_MAXIMUM, message = "adms L1 < " + ADMSLimits.SOURCE_L1_MAXIMUM)
  public double getL1() {
    return l1;
  }

  public void setL1(final double l1) {
    this.l1 = l1;
  }

  public double getL2() {
    return l2;
  }

  public void setL2(final double l2) {
    this.l2 = l2;
  }

  public int getNumGroups() {
    return numGroups;
  }

  public void setNumGroups(final int numGroups) {
    this.numGroups = numGroups;
  }

  public int getNumVertices() {
    return numVertices;
  }

  public void setNumVertices(final int numVertices) {
    this.numVertices = numVertices;
  }

  public int getNumIsotopes() {
    return numIsotopes;
  }

  public void setNumIsotopes(final int numIsotopes) {
    this.numIsotopes = numIsotopes;
  }

  public double getPercentNOxAsNO2() {
    return percentNOxAsNO2;
  }

  public void setPercentNOxAsNO2(final double percentNOxAsNO2) {
    this.percentNOxAsNO2 = percentNOxAsNO2;
  }

  /**
   * Fm (m4/s2): momentum flux of emission as defined in the HMIP D1 document (HMIP, 1993). Fm should always be specified at the release conditions,
   * i.e. Actual temperature and pressure. Refer to Section 9.22 for more information.
   * Minimum = 1 m4/s2
   * Maximum = 10^6 m4/s2
   * Default = 1 m4/s
   */
  @Min(value = ADMSLimits.SOURCE_MOMENTUM_FLUX_MINIMUM, message = "adms momentum flux (fm) > " + ADMSLimits.SOURCE_MOMENTUM_FLUX_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_MOMENTUM_FLUX_MAXIMUM, message = "adms momentum flux (fm) < " + ADMSLimits.SOURCE_MOMENTUM_FLUX_MAXIMUM)
  public double getMomentumFlux() {
    return momentumFlux;
  }

  public void setMomentumFlux(final double momentumFlux) {
    this.momentumFlux = momentumFlux;
  }

  /**
   * Fb (MW): heat release rate of emission as defined in the HMIP D1 document (HMIP, 1993). Fb should always be specified at the release conditions,
   * i.e. Actual temperature and pressure.
   * Minimum = 0.0001 MW
   * Maximum = 10^4 MW
   * Default = 1 MW
   */
  @Min(value = ADMSLimits.SOURCE_BUOYANCY_FLUX_MINIMUM, message = "adms buoyancy flux (fb) > " + ADMSLimits.SOURCE_BUOYANCY_FLUX_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_BUOYANCY_FLUX_MAXIMUM, message = "adms buoyancy flux (fb) < " + ADMSLimits.SOURCE_BUOYANCY_FLUX_MAXIMUM)
  public double getBuoyancyFlux() {
    return buoyancyFlux;
  }

  public void setBuoyancyFlux(final double buoyancyFlux) {
    this.buoyancyFlux = buoyancyFlux;
  }

  /**
   * Mass flux (kg/s): mass flux of the emission.
   * Minimum = 0.1 kg/s
   * Maximum = 10^5 kg/s
   * Default = 1 kg/s
   */
  @Min(value = ADMSLimits.SOURCE_MASS_FLUX_MINIMUM, message = "adms mass flux > " + ADMSLimits.SOURCE_MASS_FLUX_MINIMUM)
  @Max(value = ADMSLimits.SOURCE_MASS_FLUX_MAXIMUM, message = "adms mass flux < " + ADMSLimits.SOURCE_MASS_FLUX_MAXIMUM)
  public double getMassFlux() {
    return massFlux;
  }

  public void setMassFlux(final double massFlux) {
    this.massFlux = massFlux;
  }

  public double getAngle1() {
    return angle1;
  }

  public void setAngle1(final double angle1) {
    this.angle1 = angle1;
  }

  public double getAngle2() {
    return angle2;
  }

  public void setAngle2(final double angle2) {
    this.angle2 = angle2;
  }

  public double getMassH2O() {
    return massH2O;
  }

  public void setMassH2O(final double massH2O) {
    this.massH2O = massH2O;
  }

  public int getUseVARFile() {
    return useVARFile;
  }

  public void setUseVARFile(final int useVARFile) {
    this.useVARFile = useVARFile;
  }

  public String getHourlyTimeVaryingProfileName() {
    return hourlyTimeVaryingProfileName;
  }

  public void setHourlyDayTimeVaryingProfileName(final String profileName) {
    this.hourlyTimeVaryingProfileName = profileName;
  }

  public String getMonthlyTimeVaryingProfileName() {
    return monthlyTimeVaryingProfileName;
  }

  public void setMonthlyTimeVaryingProfileName(final String monthlyTimeVaryingProfileName) {
    this.monthlyTimeVaryingProfileName = monthlyTimeVaryingProfileName;
  }
}
