/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.backgrounddata;

import java.util.List;
import java.util.stream.Stream;

import nl.overheid.aerius.geo.shared.BBox;

/**
 * Grid resolution values used by ADMS. These are the resolution for the terrain data and the HilGridSize variable to be used in the UPL/APL file.
 */
public enum GridResolution {
  GR_1_50(9.000, 1, 50),
  GR_2_50(49.000, 2, 50),
  GR_2_100(107.1225, 2, 100),
  GR_2_150(160.0225, 2, 150),
  GR_2_200(259.2100, 2, 200),
  GR_3_100(382.2025, 3, 100),
  GR_3_150(699.6025, 3, 150),
  GR_3_200(1270.9225, 3, 200),
  GR_4_100(1528.8100, 4, 100),
  GR_4_150(2678.0625, 4, 150),
  GR_4_200(4792.7929, 4, 200);

  private final double upperLimit;
  private final int hilGridSize;
  private final int resolution;

  GridResolution(final double upperLimitArea, final int hilGridSize, final int resolution) {
    this.upperLimit = upperLimitArea * 1_000_000;
    this.hilGridSize = hilGridSize;
    this.resolution = resolution;
  }

  public static List<Integer> resolutions() {
    return Stream.of(values()).mapToInt(GridResolution::getResolution).distinct().boxed().toList();
  }

  /**
   * Returns the {@link GridResolution} enum based on the extent. This will be the one with the smallest area the extent area still fits in.
   * Or the last available {@link GridResolution} if it doesn't fit.
   *
   * @param extent extent to determine grid resolution based on area of the extent
   * @return {@link GridResolution} matching the extent area
   */
  public static GridResolution getGridResolutionFromExtent(final BBox extent) {
    final double area = extent == null ? 0 : extent.getHeight() * extent.getWidth();
    final GridResolution[] values = GridResolution.values();

    for (int i = 0; i < values.length; i++) {
      if (values[i].isSmaller(area)) {
        return values[i];
      }
    }
    return GridResolution.GR_4_200;
  }

  public boolean isSmaller(final double area) {
    return area < upperLimit;
  }

  public int getHilGridSize() {
    return hilGridSize;
  }

  public int getResolution() {
    return resolution;
  }
}
