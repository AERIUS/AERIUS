/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.function.AeriusConsumer;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Util class to check if an ADMS run resulted in an error.
 */
public final class ErrorFileChecker {

  private static final String ERROR_PREFIX = "ERROR";
  private static final String ERROR_EXTENSION = ".err";
  private static final String WARNING_PREFIX = "WARNING";
  private static final String WARNING_EXTENSION = ".wng";

  private static final Logger LOG = LoggerFactory.getLogger(ErrorFileChecker.class);

  private ErrorFileChecker() {
    // Util class
  }

  public static void checkErrors(final File outputDirectory) throws IOException, AeriusException {
    check(outputDirectory, ERROR_EXTENSION, ERROR_PREFIX, line -> ErrorFileChecker.handleError(outputDirectory, line));
  }

  private static void handleError(final File outputDirectory, final String line) throws AeriusException {
    LOG.error("ADMS run for '{}' resulted in an error: {}", outputDirectory.getParent(), line);
    throw new AeriusException(AeriusExceptionReason.ADMS_INTERNAL_EXCEPTION, line);
  }

  public static void checkWarnings(final File outputDirectory) throws IOException, AeriusException {
    check(outputDirectory, WARNING_EXTENSION, WARNING_PREFIX, line -> ErrorFileChecker.handleWarn(outputDirectory, line));
  }

  private static void handleWarn(final File outputDirectory, final String line) {
    LOG.warn("ADMS run for '{}' resulted in a warning: {}", outputDirectory.getParent(), line);
  }

  public static void check(final File outputDirectory, final String ext, final String prefix, final AeriusConsumer<String> handleMessage)
      throws IOException, AeriusException {
    final File file = new File(outputDirectory.getAbsoluteFile() + ext);

    if (file.exists()) {
      for (final String line : Files.readAllLines(file.toPath())) {
        if (line.startsWith(prefix)) {
          handleMessage.apply(line);
          return;
        }
      }
    }
  }
}
