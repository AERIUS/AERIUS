/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.exec.ExecuteParameters;
import nl.overheid.aerius.exec.ExecuteWrapper;
import nl.overheid.aerius.exec.ProcessListener;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.OSUtils;

/**
 * Runs the ADMS executable.
 */
class ADMSExecutor {

  private static final Logger LOG = LoggerFactory.getLogger(ADMSExecutor.class);

  public static final String ADMS_URBAN_LIC_NAME = "ADMS-Urban.lic";
  private static final String EXECUTABLE_NAME = OSUtils.isWindows() ? "ADMSUrbanModel.exe" : "ADMSUrbanModel.out";

  private final File admsRoot;
  private final ADMSVersion admsVersion;

  public ADMSExecutor(final ADMSVersion admsVersion, final File admsRoot) {
    this.admsVersion = admsVersion;
    this.admsRoot = admsRoot;
  }

  /**
   * Runs the ADMS executable in the target directory with the given parameters.
   * @param targetDirectory directory to run from
   * @param parameters parameters to pass
   * @param license optional pass the license
   * @param progressCollector Collects progress during ADMS run
   * @param processListener listener to monitor the ADMS executable process
   * @return returns the directory where the results are stored
   * @throws Exception exception thrown in case of error during run
   */
  public File run(final File targetDirectory, final List<String> parameters, final byte[] license, final Consumer<String> progressCollector,
      final ProcessListener processListener)  throws Exception {
    final String executablePath;
    if (license == null) {
      executablePath = admsRoot.getAbsolutePath();
    } else {
      copyADMS(targetDirectory, license);
      executablePath = targetDirectory.getAbsolutePath();
    }
    try {
      final ExecuteParameters executeParameters = new ExecuteParameters(EXECUTABLE_NAME, parameters.toArray(new String[parameters.size()]));
      final ADMSStreamGobblers streamGobblers = new ADMSStreamGobblers(admsVersion.getVersion(), progressCollector);
      final ExecuteWrapper wrapper = new ExecuteWrapper(executeParameters, executablePath, streamGobblers, processListener);

      final int exitCode = wrapper.run(UUID.randomUUID().toString(), targetDirectory);

      if (!streamGobblers.getExceptions().isEmpty()) {
        // Throw first found error.
        throw streamGobblers.getExceptions().get(0);
      }
      if (exitCode != 0) {
        // When a process is killed because the job was cancelled it will return exit code 137 (on Linux)
        throw new AeriusException(AeriusExceptionReason.ADMS_INTERNAL_EXCEPTION, "ADMS stopped with exitcode " + exitCode);
      }
    } finally {
      if (license != null) {
        clearADMS(targetDirectory);
      }
    }
    return targetDirectory;
  }

  /**
   * Copies the ADMS executable to the target directory and stores the passed license in a file next to the executable.
   * The copying and storing of the license file is only done when the target directory is different from the configured ADMS directory.
   *
   * @param targetDirectory target directory
   * @param license ADMS license
   * @throws IOException
   */
  private void copyADMS(final File targetDirectory, final byte[] license) throws IOException {
    // Safety check to only delete files if in a temporary directory and not in the same directory as the configured root
    if (!admsRoot.getAbsolutePath().equals(targetDirectory.getAbsolutePath())) {
      Files.copy(Path.of(admsRoot.getAbsolutePath(), EXECUTABLE_NAME),
          Path.of(targetDirectory.getAbsolutePath(), EXECUTABLE_NAME));
      Files.write(new File(targetDirectory, ADMS_URBAN_LIC_NAME).toPath(), license);
    }
  }

  private void clearADMS(final File targetDirectory) {
    // Safety check to only delete files if in a temporary directory and not in the same directory as the configured root
    if (!admsRoot.getAbsolutePath().equals(targetDirectory.getAbsolutePath())) {
      try {
        Files.deleteIfExists(new File(targetDirectory, ADMS_URBAN_LIC_NAME).toPath());
      } catch (final IOException e) {
        LOG.error("Could not delete ADMS license file from {}", targetDirectory, e);
      }
      try {
        Files.deleteIfExists(new File(targetDirectory, EXECUTABLE_NAME).toPath());
      } catch (final IOException e) {
        LOG.error("Could not delete ADMS executable from {}", targetDirectory, e);
      }
    }
  }
}
