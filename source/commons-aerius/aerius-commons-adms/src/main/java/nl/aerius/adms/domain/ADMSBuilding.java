/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import nl.overheid.aerius.shared.domain.EngineBuilding;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.HasName;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;

/**
 * Data object of an ADMS Building representation.
 */
public class ADMSBuilding implements EngineSource, EngineBuilding, HasName {

  private static final long serialVersionUID = 2L;

  /**
   * Shape of the buildings.
   */
  public enum BuildingShape {
    /**
     * Rectangular building.
     */
    RECTANGULAR(0),
    /**
     * Circular building.
     */
    CIRCULAR(1);

    private final int code;

    BuildingShape(final int code) {
      this.code = code;
    }

    public int getCode() {
      return code;
    }
  }

  private String name;
  private BuildingShape shape;
  private Point buildingCentre;
  private double height;
  private double length;
  private double width;
  private double angle;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  public BuildingShape getShape() {
    return shape;
  }

  public void setShape(final BuildingShape shape) {
    this.shape = shape;
  }

  public Point getBuildingCentre() {
    return buildingCentre;
  }

  public void setBuildingCentre(final Point buildingCentre) {
    this.buildingCentre = buildingCentre;
  }

  @Override
  public double getBuildingHeight() {
    return height;
  }

  @Override
  public void setBuildingHeight(final double height) {
    this.height = height;
  }

  @Override
  public double getBuildingLength() {
    return length;
  }

  /**
   * Length of a rectangular building or diameter of a circular building, in metres.
   *
   * @param length Length (m)/ Diameter (m)
   */
  @Override
  public void setBuildingLength(final double length) {
    this.length = length;
  }

  @Override
  public double getBuildingWidth() {
    return shape == BuildingShape.CIRCULAR ? length : width;
  }

  /**
   * Width of a rectangular building, in metres.
   *
   * @param width width (m)
   */
  @Override
  public void setBuildingWidth(final double width) {
    this.width = width;
  }

  @Override
  public double getBuildingOrientation() {
    return shape == BuildingShape.CIRCULAR ? 0 : angle;
  }

  /**
   * Angle the length of a rectangular building makes with  north, measured clockwise in degrees.
   *
   * @param angle Angle (Degrees)
   */
  @Override
  public void setBuildingOrientation(final double angle) {
    this.angle = angle;
  }

}
