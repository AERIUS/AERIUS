/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSBuilding.BuildingShape;
import nl.aerius.adms.domain.ADMSRoadSource;
import nl.aerius.adms.domain.ADMSSource;
import nl.aerius.adms.domain.ADMSTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.characteristics.ADMSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.BuoyancyType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.EffluxType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ReleaseTemperatureAndPressure;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.source.ADMSRoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Converts an ADMS Source to an {@link EmissionSource}.
 */
public class ADMS2EmissionSourceConverter {

  private static final Logger LOGGER = LoggerFactory.getLogger(ADMS2EmissionSourceConverter.class);

  private static final BigDecimal TO_PERCENTAGE = BigDecimal.valueOf(100);
  private static final int ROAD_SECTOR_ID = 3100;

  /**
   * Converts an ADMS Source to an {@link EmissionSource}.
   *
   * @param source adms source to convert
   * @param id numeric unique id
   * @return Emission Source object.
   * @throws AeriusException
   */
  public static EmissionSourceFeature convert(final ADMSSource<?> source, final String id) throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setId(id);
    feature.setGeometry(source.getGeometry());
    if (source instanceof ADMSRoadSource) {
      feature.setProperties(convertToRoadSource((ADMSRoadSource) source, id));
    } else {
      feature.setProperties(convertToOther(source, id));
    }
    return feature;
  }

  private static EmissionSource convertToRoadSource(final ADMSRoadSource source, final String id) throws AeriusException {
    final ADMSRoadEmissionSource roadSource = new ADMSRoadEmissionSource();

    roadSource.setElevation(source.getHeight());
    roadSource.setWidth(source.getL1());
    roadSource.setGradient(source.getTraGradient());
    roadSource.setBarrierLeft(source.getLeftRoadSideBarrier());
    roadSource.setBarrierRight(source.getRightRoadSideBarrier());
    roadSource.setCoverage(BigDecimal.valueOf(source.getFractionCovered()).multiply(TO_PERCENTAGE).doubleValue());
    roadSource.setSectorId(ROAD_SECTOR_ID);
    return convertToGeneric(source, id, roadSource);
  }

  private static EmissionSource convertToOther(final ADMSSource<?> source, final String id) throws AeriusException {
    final EmissionSource genericEmissionSource = convertToGeneric(source, id, new GenericEmissionSource());
    genericEmissionSource.setSectorId(Sector.DEFAULT_SECTOR_ID);

    final ADMSSourceCharacteristics chars = (ADMSSourceCharacteristics) genericEmissionSource.getCharacteristics();
    chars.setHeight(source.getHeight());
    l1ToCharacteristic(source, chars);
    return genericEmissionSource;
  }

  private static EmissionSource convertToGeneric(final ADMSSource<?> source, final String id, final EmissionSource emissionSource)
      throws AeriusException {
    emissionSource.setLabel(source.getName());
    emissionSource.setDescription(source.getName());
    emissionSource.setGmlId(GMLIdUtil.toValidGmlId(id, GMLIdUtil.SOURCE_PREFIX));
    EmissionRateConverter.adms2EmissionSourceEmissions(source, emissionSource.getEmissions());
    emissionSource.setCharacteristics(toCharacteristics(source));
    return emissionSource;
  }

  private static ADMSSourceCharacteristics toCharacteristics(final ADMSSource<?> source) {
    final ADMSSourceCharacteristics chars = new ADMSSourceCharacteristics();
    chars.setBuildingId(source.getMainBuilding());
    chars.setBuoyancyType(BuoyancyType.valueOf(source.getBuoyancyType()));
    chars.setEffluxType(EffluxType.valueOf(source.getEffluxType()));
    chars.setReleaseTemperatureAndPressure(ReleaseTemperatureAndPressure.valueOf(source.getReleaseAtNTP()));
    chars.setSourceType(source.getSourceType());
    chars.setDiameter(source.getDiameter());
    chars.setTemperature(source.getTemperature());
    chars.setDensity(source.getDensity());
    chars.setVerticalVelocity(source.getVerticalVelocity());
    chars.setVolumetricFlowRate(source.getVolumetricFlowRate());
    chars.setSpecificHeatCapacity(source.getSpecificHeatCapacity());
    chars.setPercentNOxAsNO2(source.getPercentNOxAsNO2());
    chars.setMomentumFlux(source.getMomentumFlux());
    chars.setBuoyancyFlux(source.getBuoyancyFlux());
    chars.setMassFlux(source.getMassFlux());
    if (source.getSourceType() == SourceType.JET) {
      chars.setElevationAngle(source.getAngle1());
      chars.setHorizontalAngle(source.getAngle2());
    }
    chars.setCustomHourlyTimeVaryingProfileId(source.getHourlyTimeVaryingProfileName());
    chars.setCustomMonthlyTimeVaryingProfileId(source.getMonthlyTimeVaryingProfileName());
    return chars;
  }

  private static void l1ToCharacteristic(final ADMSSource<?> source, final ADMSSourceCharacteristics chars) {
    final SourceType sourceType = source.getSourceType();

    if (sourceType == SourceType.LINE || sourceType == SourceType.ROAD) {
      chars.setWidth(source.getL1());
    } else if (sourceType == SourceType.VOLUME) {
      chars.setVerticalDimension(source.getL1());
    }
  }

  /**
   * Converts {@link ADMSBuilding} to BuidingFeature.
   *
   * @param admsBuilding ADMS building
   * @param id index in list of buildings, used for id
   * @return
   * @throws AeriusException
   */
  public static BuildingFeature convert(final ADMSBuilding admsBuilding, final String id) throws AeriusException {
    final BuildingFeature feature = new BuildingFeature();
    feature.setId(id);

    final Building building = new Building();
    building.setLabel(admsBuilding.getName());
    building.setGmlId(GMLIdUtil.toValidGmlId(admsBuilding.getName(), GMLIdUtil.BUILDING_PREFIX));
    building.setHeight(admsBuilding.getBuildingHeight());

    final Point buildingCentre = admsBuilding.getBuildingCentre();
    if (admsBuilding.getShape() == BuildingShape.RECTANGULAR) {
      feature.setGeometry(GeometryUtil.constructPolygonFromDimensions(buildingCentre, admsBuilding.getBuildingLength(),
          admsBuilding.getBuildingWidth(), admsBuilding.getBuildingOrientation()));
    } else if (admsBuilding.getShape() == BuildingShape.CIRCULAR) {
      feature.setGeometry(buildingCentre);
      building.setDiameter(admsBuilding.getBuildingLength());
    } else {
      LOGGER.error("Unexpected building shape type: {}", admsBuilding.getShape());
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    feature.setProperties(building);
    return feature;
  }

  /**
   * Converts {@link ADMSTimeVaryingProfile} to {@link CustomTimeVaryingProfile} objects.
   *
   * @param timeVaryingProfiles List of {@link ADMSTimeVaryingProfile}
   * @return list of {@link CustomTimeVaryingProfile}
   */
  public static List<CustomTimeVaryingProfile> convert(final List<ADMSTimeVaryingProfile> timeVaryingProfiles) {
    return timeVaryingProfiles.stream().map(ADMS2EmissionSourceConverter::convert).collect(Collectors.toList());
  }

  private static CustomTimeVaryingProfile convert(final ADMSTimeVaryingProfile adv) {
    final CustomTimeVaryingProfile cdv = new CustomTimeVaryingProfile();

    cdv.setGmlId(adv.getName());
    cdv.setType(adv.getType());
    cdv.setValues(adv.getValues());
    return cdv;
  }

}
