/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileConstants.NO_BUILDING;
import static nl.aerius.adms.io.AplFileKeys.SRC_ANGLE_1;
import static nl.aerius.adms.io.AplFileKeys.SRC_ANGLE_2;
import static nl.aerius.adms.io.AplFileKeys.SRC_BUOYANCY_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_DENSITY;
import static nl.aerius.adms.io.AplFileKeys.SRC_DIAMETER;
import static nl.aerius.adms.io.AplFileKeys.SRC_EFFLUX_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_FB;
import static nl.aerius.adms.io.AplFileKeys.SRC_FM;
import static nl.aerius.adms.io.AplFileKeys.SRC_HEIGHT;
import static nl.aerius.adms.io.AplFileKeys.SRC_L1;
import static nl.aerius.adms.io.AplFileKeys.SRC_L2;
import static nl.aerius.adms.io.AplFileKeys.SRC_MAIN_BUILDING;
import static nl.aerius.adms.io.AplFileKeys.SRC_MASS_FLUX;
import static nl.aerius.adms.io.AplFileKeys.SRC_MOL_WEIGHT;
import static nl.aerius.adms.io.AplFileKeys.SRC_NAME;
import static nl.aerius.adms.io.AplFileKeys.SRC_NUM_VERTICES;
import static nl.aerius.adms.io.AplFileKeys.SRC_PERCENT_NOX_AS_NO2;
import static nl.aerius.adms.io.AplFileKeys.SRC_POLLUTANTS;
import static nl.aerius.adms.io.AplFileKeys.SRC_POL_EMISSION_RATE;
import static nl.aerius.adms.io.AplFileKeys.SRC_RELEASE_AT_NTP;
import static nl.aerius.adms.io.AplFileKeys.SRC_SOURCE_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_SPECIFIC_HEAT_CAPACITY;
import static nl.aerius.adms.io.AplFileKeys.SRC_TEMPERATURE;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_GRADIENT;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_ROAD_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_YEAR;
import static nl.aerius.adms.io.AplFileKeys.SRC_VERTICAL_VELOCITY;
import static nl.aerius.adms.io.AplFileKeys.SRC_VOLUMETRIC_FLOW_RATE;
import static nl.aerius.adms.io.AplFileKeys.SRC_X1;
import static nl.aerius.adms.io.AplFileKeys.SRC_Y1;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import nl.aerius.adms.domain.ADMSRoadSource;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Reads the emission source data from a single source block.
 */
class AplSourceReader {

  private final ADMSSourceFileType fileType;

  public AplSourceReader(final ADMSSourceFileType fileType) {
    this.fileType = fileType;
  }

  /**
   * Adds the source in the read block to the content.
   *
   * @param br block read with source data
   * @param content object to add the source block too.
   * @return
   * @throws IOException
   * @throws AeriusException
   */
  public ADMSSource readSource(final AplBlockReader br) throws IOException, AeriusException {
    final SourceType type = SourceType.byType(br.parseInt(SRC_SOURCE_TYPE));

    if (type == null) {
      // TODO add error handling
    }
    final ADMSSource<?> source = createSourceObject(type);

    source.setSourceType(type);
    setGeometry(br, source, type);
    source.setName(br.parseString(SRC_NAME));
    setBuildingName(source, br.parseString(SRC_MAIN_BUILDING));
    source.setHeight(br.parseDouble(SRC_HEIGHT));
    source.setDiameter(br.parseDouble(SRC_DIAMETER));
    source.setVolumetricFlowRate(br.parseDouble(SRC_VOLUMETRIC_FLOW_RATE));
    source.setVerticalVelocity(br.parseDouble(SRC_VERTICAL_VELOCITY));
    source.setTemperature(br.parseDouble(SRC_TEMPERATURE));
    source.setMolWeight(br.parseDouble(SRC_MOL_WEIGHT));
    source.setDensity(br.parseDouble(SRC_DENSITY));
    source.setSpecificHeatCapacity(br.parseDouble(SRC_SPECIFIC_HEAT_CAPACITY));
    source.setReleaseAtNTP(br.parseInt(SRC_RELEASE_AT_NTP));
    source.setEffluxType(br.parseInt(SRC_EFFLUX_TYPE));
    source.setBuoyancyType(br.parseInt(SRC_BUOYANCY_TYPE));
    if (ADMSSourceFileType.APL == fileType) {
      source.setPercentNOxAsNO2(br.parseDouble(SRC_PERCENT_NOX_AS_NO2));
      source.setMomentumFlux(br.parseDouble(SRC_FM));
      source.setBuoyancyFlux(br.parseDouble(SRC_FB));
      source.setMassFlux(br.parseDouble(SRC_MASS_FLUX));
    }
    source.setL1(br.parseDouble(SRC_L1));
    source.setL2(br.parseDouble(SRC_L2));
    if (type == SourceType.JET) {
      source.setAngle1(br.parseDouble(SRC_ANGLE_1));
      source.setAngle2(br.parseDouble(SRC_ANGLE_2));
    } else if (source instanceof ADMSRoadSource) {
      setTrafficSource(br, (ADMSRoadSource) source);
    }
    setEmissions(br, source);
    return source;
  }

  private static ADMSSource<?> createSourceObject(final SourceType type) {
    final ADMSSource<?> source;
    if (type == SourceType.ROAD) {
      source = new ADMSRoadSource();
    } else {
      source = new ADMSSource<>();
    }
    return source;
  }

  private static void setBuildingName(final ADMSSource source, final String buildingName) {
    source.setMainBuilding(NO_BUILDING.equals(buildingName) ? "" : buildingName);
  }

  private static void setGeometry(final AplBlockReader br, final ADMSSource source, final SourceType type) throws IOException {
    final Geometry geometry;

    if (type == SourceType.POINT || type == SourceType.JET) {
      geometry = new Point(br.parseDouble(SRC_X1), br.parseDouble(SRC_Y1));
    } else {
      geometry = parseGeometryFromVertices(br, source, type == SourceType.AREA || type == SourceType.VOLUME);
    }
    source.setGeometry(geometry);
  }

  private static Geometry parseGeometryFromVertices(final AplBlockReader br, final ADMSSource source, final boolean ispolygon) throws IOException {
    final int nrOfVertices = br.parseInt(SRC_NUM_VERTICES);

    if (nrOfVertices > 0) {
      final int coordinatesSize = ispolygon ? (nrOfVertices + 1) : nrOfVertices;
      final double[][] coordinates = new double[coordinatesSize][2];

      br.readVertices(nrOfVertices, coordinates);
      completePolygon(ispolygon, coordinates);

      if (ispolygon) {
        final Polygon polygon = new Polygon();
        polygon.setCoordinates(new double[][][] {coordinates});
        return polygon;
      } else {
        final LineString line = new LineString();
        line.setCoordinates(coordinates);
        return line;
      }
    } else {
      return null; // handle other than point source with no vertices.
    }
  }

  /**
   * If polygon copy the first coordinate to the last coordinate to complete the polygon geometry.
   *
   * @param polygon true if the coordinates should be for a polygon
   * @param coordinates coordinates to complete
   */
  private static void completePolygon(final boolean polygon, final double[][] coordinates) {
    if (polygon) {
      coordinates[coordinates.length - 1][0] = coordinates[0][0];
      coordinates[coordinates.length - 1][1] = coordinates[0][1];
    }
  }

  private static void setTrafficSource(final AplBlockReader br, final ADMSRoadSource source) {
    source.setTraGradient(br.parseDouble(SRC_TRA_GRADIENT));
    source.setHeight(br.parseDouble(SRC_HEIGHT));
    source.setTraYear(br.parseInt(SRC_TRA_YEAR));
    source.setTraRoadType(br.parseString(SRC_TRA_ROAD_TYPE));
    // Unknown how these values should be handled, left in comment for now
    //    source.setTraEmissionsMode", 0),//0
    //    source.setTRA_NUM_TRAFFIC_FLOWS("SrcTraNumTrafficFlows", 0),//0
  }

  private static void setEmissions(final AplBlockReader br, final ADMSSource source) throws AeriusException {
    final List<Substance> substances = br.parseStringList(SRC_POLLUTANTS).stream().map(Substance::safeValueOf).collect(Collectors.toList());
    final List<Double> emissions = br.parseDoubleList(SRC_POL_EMISSION_RATE);
    // Unknown how these values should be handled, left in comment for now
    //    source.setPolTotalemission", 0.0),//1.0e+0 1.0e+0
    //    source.setPolStartTime", 0.0),//0.0e+0 0.0e+0
    //    source.setPolDuration", 0.0),//0.0e+0 0.0e+0

    for (int i = 0; i < substances.size(); i++) {
      final Substance substance = substances.get(i);

      if (substance != null) {
        source.setEmission(substance, emissions.get(i));
      }
    }
  }
}
