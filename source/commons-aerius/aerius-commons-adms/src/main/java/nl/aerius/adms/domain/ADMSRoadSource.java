/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrier;

/**
 * ADMS road source
 */
public class ADMSRoadSource extends ADMSSource<LineString> {

  private static final long serialVersionUID = 3L;

  private int traEmissionsMode;
  private int traYear;
  private String traRoadType;
  private double traGradient;
  private int numTrafficFlows;
  private ADMSRoadSideBarrier left;
  private ADMSRoadSideBarrier right;
  // fraction covered ranges: 0.0 - 1.0
  private double fractionCovered;

  public int getTraEmissionsMode() {
    return traEmissionsMode;
  }

  public void setTraEmissionsMode(final int traEmissionsMode) {
    this.traEmissionsMode = traEmissionsMode;
  }

  public int getTraYear() {
    return traYear;
  }

  public void setTraYear(final int traYear) {
    this.traYear = traYear;
  }

  public String getTraRoadType() {
    return traRoadType;
  }

  public void setTraRoadType(final String traRoadType) {
    this.traRoadType = traRoadType;
  }

  public double getTraGradient() {
    return traGradient;
  }

  public void setTraGradient(final double traGradient) {
    this.traGradient = traGradient;
  }

  public int getNumTrafficFlows() {
    return numTrafficFlows;
  }

  public void setNumTrafficFlows(final int numTrafficFlows) {
    this.numTrafficFlows = numTrafficFlows;
  }

  public ADMSRoadSideBarrier getLeftRoadSideBarrier() {
    return left;
  }

  public void setLeftRoadSideBarrier(final ADMSRoadSideBarrier left) {
    this.left = left;
  }

  public ADMSRoadSideBarrier getRightRoadSideBarrier() {
    return right;
  }

  public void setRightRoadSideBarrier(final ADMSRoadSideBarrier right) {
    this.right = right;
  }

  public double getFractionCovered() {
    return fractionCovered;
  }

  public void setFractionCovered(final double fractionCovered) {
    this.fractionCovered = fractionCovered;
  }
}
