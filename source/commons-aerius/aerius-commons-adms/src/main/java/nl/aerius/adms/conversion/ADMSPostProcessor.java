/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.NCACalculationResultPoint;
import nl.aerius.adms.util.ADMSUtil;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * Utility class to post process calculation results. It will compute the NO2 concentration and the Nxx depositions.
 */
public final class ADMSPostProcessor {

  private ADMSPostProcessor() {
    // Util class
  }

  /**
   * Computes NO2 from NOx concentration and compute Deposition from the Concentration results.
   * When deposition is calculated results for deposition are added.
   *
   * @param groupKey The type
   * @param erks keys results are to be expected and checked if to be computed
   * @param results calculation results to process
   * @return returns the processed results
   */
  public static List<NCACalculationResultPoint> postprocess(final ADMSGroupKey groupKey, final Set<EmissionResultKey> erks,
      final List<NCACalculationResultPoint> results) {
    // Compute NO2 from NOx
    ADMSNO2ConcentrationCalculator.computeNO2Concentrations(groupKey, erks, results);

    return aggregateDepositionResults(results, erks);
  }

  /**
   * Aggregate results into the receptor points from subreceptor results, and computes deposition from the concentration results.
   *
   * @param concentrationKeys results keys with only concentration pollutant result keys
   * @param receptors original set of receptors to calculate
   */
  private static List<NCACalculationResultPoint> aggregateDepositionResults(final List<NCACalculationResultPoint> results,
      final Set<EmissionResultKey> erks) {
    final Set<EmissionResultKey> concentrationKeys = ADMSUtil.filterKeys(erks, EmissionResultType.CONCENTRATION, Collectors.toSet());

    if (concentrationKeys.isEmpty()) {
      return results;
    } else {
      final ADMSSubReceptorAggregator conAggregator = new ADMSSubReceptorAggregator(results, concentrationKeys);
      final List<NCACalculationResultPoint> aggregatedResults = conAggregator.aggregate(results);
      final Map<EmissionResultKey, EmissionResultKey> depositionKeys = ADMSUtil.filterKeys(erks, EmissionResultType.DEPOSITION,
          Collectors.toMap(Function.identity(),
              k -> EmissionResultKey.valueOf(ADMSUtil.toConcentrationSubstance(k), EmissionResultType.CONCENTRATION)));

      if (!depositionKeys.isEmpty()) {
        computeDeposition(aggregatedResults, depositionKeys);
      }
      return aggregatedResults;
    }
  }

  private static void computeDeposition(final List<NCACalculationResultPoint> results,
      final Map<EmissionResultKey, EmissionResultKey> depositionKeys) {
    // Compute deposition from concentration results.
    results.stream().filter(p -> !p.getPointType().isSubReceptor()).forEach(r -> setDeposition(depositionKeys, r));
  }

  private static void setDeposition(final Map<EmissionResultKey, EmissionResultKey> depositionKeys, final NCACalculationResultPoint resultPoint) {
    depositionKeys.forEach((d, c) -> {
      final double depositionVelocity = resultPoint.getDepositionVelocity(c.getSubstance());
      final double deposition = ADMSDepositionCalculator.calculateNutrientNitrogenDeposition(c.getSubstance(), depositionVelocity,
          resultPoint.getEmissionResult(c));

      resultPoint.setEmissionResult(d, deposition);
    });
  }
}
