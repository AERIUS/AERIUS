/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion.nox2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import nl.aerius.adms.backgrounddata.ADMSData;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData.FractionNO2;
import nl.aerius.adms.conversion.nox2.NOxToNO2CalculatorData.LACodeBackgroundConcentration;
import nl.aerius.adms.domain.ADMSInputData;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Writes the NOx to NO2 background data for the receptors used in the calculation to a file.
 */
public final class NOxToNO2BackgroundDataWriter {

  private static final Long UNKNOWN_UWE = Long.valueOf(-1);
  private static final double UNKNOWN_DATA = -1.0;
  private static final String UNKNOWN_AREA = "";
  private static final String HEADER = "X,Y,UWE_CODE,YEAR_NOX_BACK,NO2_BACK,NOX_BACK,O3_BACK,fNO2_AREA,fNO2_REGIONAL,fNO2_LOCAL,fNO2_CUSTOM";
  private static final String ROW = "%.3f,%.3f,%d,%.5f,%.5f,%.5f,%.5f,%s,%.5f,%.5f,%.5f";
  private static final String NOX_TO_NO2_BACKGROUND_DATA = "NOxToNO2BackgroundData.csv";

  private NOxToNO2BackgroundDataWriter() {
    // Util class
  }

  /**
   * Writes the background data for the given receptors to a file in the output directory.
   *
   * @param admsData data to retreive background data from
   * @param input input to get receptors from
   * @param outputDirectory directory to write data to.
   * @throws IOException
   */
  public static void writeNO2Conversion(final ADMSData admsData, final ADMSInputData input, final File outputDirectory) throws IOException {
    final NOxToNO2CalculatorData data = admsData.getNOxToNO2CalculatorData(input.getAdmsVersion());
    final List<String> lines = new ArrayList<>();

    lines.add(HEADER);
    lines.addAll(input.getReceptors().stream().map(r -> toRow(data, input.getYear(), r)).toList());
    Files.write(new File(outputDirectory, NOX_TO_NO2_BACKGROUND_DATA).toPath(), lines);
  }

  private static String toRow(final NOxToNO2CalculatorData data, final int year, final ADMSCalculationPoint point) {
    final LACodeBackgroundConcentration bc = data.getBackgroundConcentration(year, point);
    final double no2Back = bc == null ? UNKNOWN_DATA : bc.getNO2();
    final double noxBack = bc == null ? UNKNOWN_DATA : bc.getNOx();
    final double o3Back = bc == null ? UNKNOWN_DATA : bc.getO3();
    final FractionNO2 fractionNO = data.getFractionNO2(year, point);
    final double fNO2Regional = fractionNO == null ? UNKNOWN_DATA : fractionNO.regional();
    final double fNO2Local = fractionNO == null ? UNKNOWN_DATA : fractionNO.local();

    return String.format(ROW, point.getX(), point.getY(), getUweCode(data, point), point.getBackgroundNOx(year), no2Back, noxBack, o3Back,
        getfNO2Area(data, point), fNO2Regional, fNO2Local, point.getRoadLocalFNO2());
  }

  private static Long getUweCode(final NOxToNO2CalculatorData data, final Point point) {
    Long uweCode;

    try {
      uweCode = data.getUweCode(point);
    } catch (final AeriusException e) {
      uweCode = UNKNOWN_UWE;
    }
    return uweCode;
  }

  private static String getfNO2Area(final NOxToNO2CalculatorData data, final Point point) {
    try {
      return data.getFractionNO2Area(point);
    } catch (final AeriusException e) {
      return UNKNOWN_AREA;
    }
  }
}
