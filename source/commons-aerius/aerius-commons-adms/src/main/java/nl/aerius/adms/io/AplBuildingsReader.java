/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileKeys.BLD_ANGLE;
import static nl.aerius.adms.io.AplFileKeys.BLD_HEIGHT;
import static nl.aerius.adms.io.AplFileKeys.BLD_LENGTH;
import static nl.aerius.adms.io.AplFileKeys.BLD_NAME;
import static nl.aerius.adms.io.AplFileKeys.BLD_NUM_BUILDINGS;
import static nl.aerius.adms.io.AplFileKeys.BLD_TYPE;
import static nl.aerius.adms.io.AplFileKeys.BLD_WIDTH;
import static nl.aerius.adms.io.AplFileKeys.BLD_X;
import static nl.aerius.adms.io.AplFileKeys.BLD_Y;

import java.util.List;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSBuilding.BuildingShape;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;

/**
 * Reader for reading building data from APL/UPL read content.
 */
class AplBuildingsReader {

  /**
   * Adds the buildings from the block to the content object.
   *
   * @param br block reader containing the building information
   * @param content content to store the building information in
   */
  public void addBuildings(final AplBlockReader br, final AplFileContent content) {
    final int nrOfBuildings = br.parseInt(BLD_NUM_BUILDINGS);

    if (nrOfBuildings == 0) {
      return;
    }
    final List<String> names = br.parseStringList(BLD_NAME);
    final List<Integer> types = br.parseIntegerList(BLD_TYPE);
    final List<Double> xs = br.parseDoubleList(BLD_X);
    final List<Double> ys = br.parseDoubleList(BLD_Y);
    final List<Double> heights = br.parseDoubleList(BLD_HEIGHT);
    final List<Double> lengths = br.parseDoubleList(BLD_LENGTH);
    final List<Double> widths = br.parseDoubleList(BLD_WIDTH);
    final List<Double> angles = br.parseDoubleList(BLD_ANGLE);

    for (int i = 0; i < nrOfBuildings; i++) {
      final ADMSBuilding b = new ADMSBuilding();
      b.setName(names.get(i));
      b.setShape(BuildingShape.CIRCULAR.getCode() == types.get(i) ? BuildingShape.CIRCULAR : BuildingShape.RECTANGULAR);
      b.setBuildingCentre(new Point(xs.get(i), ys.get(i)));
      b.setBuildingHeight(heights.get(i));
      b.setBuildingLength(lengths.get(i));
      b.setBuildingWidth(widths.get(i));
      b.setBuildingOrientation(angles.get(i));
      content.addBuilding(b);
    }
  }
}
