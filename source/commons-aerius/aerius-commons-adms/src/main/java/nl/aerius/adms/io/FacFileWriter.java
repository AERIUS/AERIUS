/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import nl.aerius.adms.domain.ADMSSource;
import nl.aerius.adms.domain.ADMSTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * File writer for time-varying profile (.fac) files.
 */
class FacFileWriter {
  private static final int NUMBER_OF_HOURS_IN_PROFILE = 24;
  private static final String VERSION = "FacVersion4";
  private static final String FAC_EXTENSION = ".fac";
  private static final String SOURCE_PROFLE_BITS = "%s,%d,%d,0";
  private final File facFile;
  private final String newline;

  /**
   * Constructor
   *
   * @param outputFile Output file with path, but without the file extension
   * @param newline
   */
  public FacFileWriter(final File outputFile, final String newline) {
    this.newline = newline;
    facFile = ADMSFileConstants.constructDataFilename(outputFile, FAC_EXTENSION);
  }

  public File getOutputFile() {
    return facFile;
  }

  /**
   * Writes the time-varying profile for the given sources.
   *
   * @param sources a sources to link to time-varying profile data
   * @param timeVaryingProfiles time-hprofiles
   * @return returns true if at least 1 entry was written to file
   * @throws IOException
   * @throws AeriusException
   */
  public final boolean write(final Collection<ADMSSource<?>> sources, final Collection<ADMSTimeVaryingProfile> timeVaryingProfiles)
      throws IOException {
    if (timeVaryingProfiles.isEmpty()) {
      return false;
    }
    final Map<CustomTimeVaryingProfileType, List<ADMSTimeVaryingProfile>> profiles = timeVaryingProfiles.stream()
        .collect(Collectors.groupingBy(ADMSTimeVaryingProfile::getType));
    final StringBuilder sourcesBuilder = new StringBuilder();
    // Add source lines to sourcesBuilder (if any present).
    final boolean hasSources = buildSources(sourcesBuilder, timeVaryingProfiles, sources);

    if (hasSources) {
      try (Writer writer = Files.newBufferedWriter(facFile.toPath(), StandardCharsets.UTF_8)) {
        writeHeader(writer);
        writeSpecificProfiles(CustomTimeVaryingProfileType.THREE_DAY, writer, profiles);
        writeEmptyProfile(writer); // 7-day profile not supported
        writeSpecificProfiles(CustomTimeVaryingProfileType.MONTHLY, writer, profiles);
        writer.append(sourcesBuilder.toString());
      }
    }
    return hasSources;
  }

  private void writeHeader(final Writer writer) throws IOException {
    writer.append(VERSION);
    writer.append(newline);
  }

  private void writeSpecificProfiles(final CustomTimeVaryingProfileType profile, final Writer writer,
      final Map<CustomTimeVaryingProfileType, List<ADMSTimeVaryingProfile>> profiles) throws IOException {
    final List<ADMSTimeVaryingProfile> timeVaryingProfiles = profiles.get(profile);
    if (timeVaryingProfiles == null || timeVaryingProfiles.isEmpty()) {
      writeEmptyProfile(writer);
    } else {
      writeProfiles(writer, profile, timeVaryingProfiles);
    }
  }

  private void writeProfiles(final Writer writer, final CustomTimeVaryingProfileType type,
      final Collection<ADMSTimeVaryingProfile> timeVaryingProfiles) throws IOException {
    writer.append(String.valueOf(timeVaryingProfiles.size()));
    writer.append(newline);
    for (final ADMSTimeVaryingProfile tvp : timeVaryingProfiles) {
      writer.append(tvp.getName());
      writer.append(newline);
      if (type == CustomTimeVaryingProfileType.MONTHLY) {
        writeProfile(writer, tvp.getValues());
      } else {
        for (int i = 0; i < type.getExpectedNumberOfValues(); i += NUMBER_OF_HOURS_IN_PROFILE) {
          writeProfile(writer, tvp.getValues().subList(i, i + NUMBER_OF_HOURS_IN_PROFILE));
        }
      }
    }
    writer.append(newline);
  }

  private void writeProfile(final Writer writer, final List<Double> profile) throws IOException {
    writer.append(profile.stream().map(String::valueOf).collect(Collectors.joining(",")));
    writer.append(newline);
  }

  private void writeEmptyProfile(final Writer writer) throws IOException {
    writer.append("0");
    writer.append(newline);
    writer.append(newline);
  }

  private boolean buildSources(final StringBuilder builder, final Collection<ADMSTimeVaryingProfile> timeVaryingProfiles,
      final Collection<ADMSSource<?>> sources) throws IOException {
    boolean hasSourcesWithProfile = false;
    for (final ADMSSource<?> source : sources) {
      final String hourlyProfileName = source.getHourlyTimeVaryingProfileName();
      final String monthyProfileName = source.getMonthlyTimeVaryingProfileName();
      final int hourlyProfile = flag(hourlyProfileName);
      final int monthlyProfile = flag(monthyProfileName);

      if (hourlyProfile + monthlyProfile > 0) {
        hasSourcesWithProfile = true;
        builder.append(String.format(SOURCE_PROFLE_BITS, source.getName(), hourlyProfile, monthlyProfile));
        builder.append(newline);
        writeSourceProfileName(builder, hourlyProfile, hourlyProfileName);
        writeSourceProfileName(builder, monthlyProfile, monthyProfileName);
      }
    }
    return hasSourcesWithProfile;
  }

  private static int flag(final String profileName) {
    return profileName != null && !profileName.isBlank() ? 1 : 0;
  }

  private void writeSourceProfileName(final StringBuilder builder, final int profileEnabled, final String profileName) throws IOException {
    if (profileEnabled > 0) {
      builder.append(profileName);
      builder.append(newline);
    }
  }
}
