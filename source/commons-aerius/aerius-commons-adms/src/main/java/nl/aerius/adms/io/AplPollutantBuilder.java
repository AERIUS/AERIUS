/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Builds the Pollution blocks in the APL/UPL file.
 */
class AplPollutantBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(AplPollutantBuilder.class);

  private static final String POLLUTANT_FILE_TEMPLATE = "pollutant_details_%s.txt";
  private static final String POLLUTANT_APL_DEFAULT = "pollutant_apl_default.txt";
  private static final Set<Substance> POLLUTANT_APL_DEFAULT_SUBSTANCES =
      EnumSet.of(Substance.NO2, Substance.NOX, Substance.NH3, Substance.PM10, Substance.PM25, Substance.O3);

  private final String aplDefault;
  private final Map<Substance, String> pollutants;

  public AplPollutantBuilder() {
    try {
      aplDefault = readUplDefault();
      pollutants = readPollutantTemplates();
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  public String getAplDefault() {
    return aplDefault;
  }

  public String getPollutantBlock(final Substance substance) {
    return pollutants.get(substance);
  }

  private static Map<Substance, String> readPollutantTemplates() throws IOException {
    final Map<Substance, String> map = new EnumMap<>(Substance.class);

    for (final Substance substance : Substance.values()) {
      if (POLLUTANT_APL_DEFAULT_SUBSTANCES.contains(substance)) {
        // ignore default substances.
        continue;
      }
      final String filename = String.format(POLLUTANT_FILE_TEMPLATE, substance.getName().toLowerCase(Locale.ROOT));

      try (final InputStream is = AplPollutantBuilder.class.getResourceAsStream(filename)) {
        if (is == null) {
          LOG.debug("No pollutant template for {} found. Substance can't be calculated with ADMS.", substance);
        } else {
          map.put(substance, new String(is.readAllBytes(), StandardCharsets.UTF_8));
        }
      }
    }
    return map;
  }

  private static String readUplDefault() throws IOException {
    try (final InputStream is = AplPollutantBuilder.class.getResourceAsStream(POLLUTANT_APL_DEFAULT)) {
      if (is == null) {
        throw new IOException("Couldn't read default upl pollutant file.");
      } else {
        return new String(is.readAllBytes(), StandardCharsets.UTF_8);
      }
    }
  }
}
