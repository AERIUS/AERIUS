/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Locale;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.util.AeriusPointUtil;

/**
 * File write to write calculation points to an ADMS ASP file.
 */
public class AspFileWriter {

  private static final String RECEPTOR_FORMAT = "%s,%.1f,%.1f,%.1f";
  private static final String FILE_EXTENSION = ".asp";
  private static final double DEFAULT_HEIGHT = 1.0;

  /**
   * Write the calculation points ADMS ASP file content.
   *
   * @param file to write points in
   * @param receptors points to store in the file
   * @param newline new line string to be used at the end of lines in the files
   * @throws IOException
   */
  public static <R extends AeriusPoint> void writeFile(final File file, final Collection<R> receptors, final String newline) throws IOException {
    final String receptorFormat = RECEPTOR_FORMAT + newline;
    try (final Writer writer = Files.newBufferedWriter(file.toPath(), StandardCharsets.UTF_8)) {
      for (final AeriusPoint point : receptors) {
        final double height = point.getHeight() == null ? DEFAULT_HEIGHT : point.getHeight();

        writer.write(String.format(Locale.ROOT, receptorFormat, AeriusPointUtil.encodeName(point),
            (double) point.getRoundedX(), (double) point.getRoundedY(), height));
      }
    }
  }

  public static File fileWithExtension(final File file) {
    return new File(file.getAbsoluteFile() + FILE_EXTENSION);
  }
}
