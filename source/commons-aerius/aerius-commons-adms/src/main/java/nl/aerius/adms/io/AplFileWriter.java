/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileConstants.ACTIVE;
import static nl.aerius.adms.io.AplFileConstants.ADMS_PARAMETERS_BLD_KEY;
import static nl.aerius.adms.io.AplFileConstants.ADMS_PARAMETERS_OPT_KEY;
import static nl.aerius.adms.io.AplFileConstants.ADMS_POLLUTANT_DETAILS_KEY;
import static nl.aerius.adms.io.AplFileConstants.GRD_PTS_POINTS_FILE_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.HIL_GRID_SIZE;
import static nl.aerius.adms.io.AplFileConstants.HIL_ROUGH_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.HIL_TERRAIN_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.HIL_USE_ROUGH_FILE;
import static nl.aerius.adms.io.AplFileConstants.HIL_USE_TER_FILE;
import static nl.aerius.adms.io.AplFileConstants.MET_DATA_FILE_WELL_FORMED_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.MET_DATA_IS_SEQUENTIAL;
import static nl.aerius.adms.io.AplFileConstants.MET_DS_MS_PARAMETERS;
import static nl.aerius.adms.io.AplFileConstants.MET_LATITUDE;
import static nl.aerius.adms.io.AplFileConstants.MET_SUBSET_YEAR_END;
import static nl.aerius.adms.io.AplFileConstants.MET_SUBSET_YEAR_START;
import static nl.aerius.adms.io.AplFileConstants.MET_WIND_IN_SECTORS;
import static nl.aerius.adms.io.AplFileConstants.NOT_ACTIVE;
import static nl.aerius.adms.io.AplFileConstants.SUP_ADD_INPUT_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.SUP_MODEL_BUILDINGS;
import static nl.aerius.adms.io.AplFileConstants.SUP_MODEL_COMPLEX_TERRAIN;
import static nl.aerius.adms.io.AplFileConstants.SUP_PROJECT_NAME_KEY;
import static nl.aerius.adms.io.AplFileConstants.SUP_SITE_NAME_KEY;
import static nl.aerius.adms.io.AplFileConstants.SUP_TIME_VARYING_EMISSIONS_TYPE;
import static nl.aerius.adms.io.AplFileConstants.SUP_TIME_VARYING_FAC_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.SUP_USE_ADD_INPUT_KEY;
import static nl.aerius.adms.io.AplFileConstants.SUP_USE_TIME_VARYING_EMISSIONS;
import static nl.aerius.adms.io.AplFileConstants.SUP_USE_TIME_VARYING_FAC;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.calculation.MetSurfaceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Writes an ADMS APL or UPL file to disk.
 */
public class AplFileWriter {

  private static final String EMPTY_VALUE = " ";
  private static final String TEMPLATE_NAME_UPL = "template.upl";
  private static final String TEMPLATE_NAME_APL = "template.apl";
  private static final AplPollutantBuilder POLLUTANT_BUILDER = new AplPollutantBuilder();

  private static final String SCIENTIFIC_NOTATION = "%.8e";
  private static final boolean DEFAULT_MET_WIND_IN_SECTORS = false;
  private static final double DEFAULT_MET_LATITUDE = 52.0;

  private final ADMSSourceFileType fileType;
  private final String newline;

  public AplFileWriter(final ADMSSourceFileType fileType, final String newline) {
    this.fileType = fileType;
    this.newline = newline;
  }

  /**
   * Writes the content to the output file.
   *
   * @param outputFile file to write content in. File will be appended with file type extension
   * @param content content to store into the file
   * @param admsGroupKey
   * @return location of the project file
   * @throws IOException
   * @throws AeriusException
   */
  public File writeFile(final File outputFile, final AplFileContent content, final ADMSGroupKey admsGroupKey)
      throws IOException {
    String template = getTemplateContent(fileType == ADMSSourceFileType.APL ? TEMPLATE_NAME_APL : TEMPLATE_NAME_UPL);
    // write general data
    template = insertGeneral(template, content);
    // write UAI input file if present
    template = insertUaiFile(template, content.getUaiDataFile());
    // write FAC input file if present
    template = insertFacFile(template, content.getFacDataFile());
    // write environment data, like meteo, terrain
    template = insertEnvironmentData(template, content);
    // write location of points file
    template = insertPointsFileLocation(template, content.getAspFile());
    // write buildings
    template = insertBuildings(template, content.getBuildings(), content.getObjects());
    // write pollutants
    template = insertPollutants(template, admsGroupKey.getOutputSubstances());
    // write dry deposition options
    template = setDryDepositionCalculationOption(template, content);
    // Number of sources
    template = template.replace(AplFileConstants.SRC_NUM_SOURCES, String.valueOf(content.getObjects().size()));
    final File projectFile = fileType.fileWithExtension(outputFile);

    template = replaceNewline(template);
    try (final FileWriter writer = new FileWriter(projectFile, StandardCharsets.UTF_8)) {
      // First write the template
      writer.append(template);
      // Write sources
      writeSources(writer, content.getObjects(), admsGroupKey);
    }
    return projectFile;
  }

  private String getTemplateContent(final String templateFile) throws IOException {
    try (final InputStream is = getClass().getResourceAsStream(templateFile)) {
      return new String(is.readAllBytes(), StandardCharsets.UTF_8);
    }
  }

  private static String insertGeneral(final String template, final AplFileContent content) {
    final ScenarioMetaData metaData = content.getMetaData();
    final boolean noMetaData = metaData == null;
    return replaceMetSiteCharacteristics(template, content)
        .replace(SUP_SITE_NAME_KEY, noMetaData ? EMPTY_VALUE : metaData.getCorporation())
        .replace(SUP_PROJECT_NAME_KEY, noMetaData ? EMPTY_VALUE : metaData.getProjectName())
        .replace(MET_DATA_IS_SEQUENTIAL, content.isMetDataIsSequential() ? ACTIVE : NOT_ACTIVE)
        .replace(HIL_GRID_SIZE, String.valueOf(content.getHilGridSize()));
  }

  private static String replaceMetSiteCharacteristics(final String template, final AplFileContent content) {
    final Optional<ADMSOptions> admsOptions = Optional.ofNullable(content.getOptions());
    final Optional<MetSurfaceCharacteristics> characteristics = admsOptions
        .map(ao -> ao.getMetSiteCharacteristics(content.getMeteoYear()));

    return template
        .replace(MET_WIND_IN_SECTORS,
            characteristics.map(MetSurfaceCharacteristics::isWindInSectors).orElse(DEFAULT_MET_WIND_IN_SECTORS) ? ACTIVE : NOT_ACTIVE)
        .replace(MET_LATITUDE, String.format(Locale.ROOT, SCIENTIFIC_NOTATION,
            admsOptions.map(ADMSOptions::getMetSiteLatitude).orElse(DEFAULT_MET_LATITUDE)))
        .replace(MET_SUBSET_YEAR_START, content.getMeteoYear()).replace(MET_SUBSET_YEAR_END, content.getMeteoYear());
  }

  private static String insertUaiFile(final String template, final String uaiFile) {
    final boolean noUAI = uaiFile == null;

    return template
        .replace(SUP_USE_ADD_INPUT_KEY, noUAI ? NOT_ACTIVE : ACTIVE)
        .replace(SUP_ADD_INPUT_PATH_KEY, noUAI ? EMPTY_VALUE : uaiFile);
  }

  private String insertFacFile(final String template, final String facFile) {
    final boolean noFAC = facFile == null;
    final String activeState = noFAC ? NOT_ACTIVE : ACTIVE;

    return template
        .replace(SUP_USE_TIME_VARYING_EMISSIONS, activeState)
        .replace(SUP_TIME_VARYING_EMISSIONS_TYPE, activeState)
        .replace(SUP_USE_TIME_VARYING_FAC, activeState)
        .replace(SUP_TIME_VARYING_FAC_PATH_KEY, noFAC ? EMPTY_VALUE : facFile);
  }

  private static String insertEnvironmentData(final String template, final AplFileContent content) {
    String template2 = template
        .replace(MET_DS_MS_PARAMETERS,
            AplMetBuilder.build(content.getOptions(), content.getOptions().getMetSiteCharacteristics(content.getMeteoYear())))
        .replace(MET_DATA_FILE_WELL_FORMED_PATH_KEY, content.getMetDataFile());

    template2 = replaceActive(template2, SUP_MODEL_COMPLEX_TERRAIN, content.getRufDataFile() != null || content.getTerDataFile() != null);
    template2 = insertActiveFile(template2, HIL_USE_TER_FILE, HIL_TERRAIN_PATH_KEY, content.getTerDataFile());
    return insertActiveFile(template2, HIL_USE_ROUGH_FILE, HIL_ROUGH_PATH_KEY, content.getRufDataFile());
  }

  private static String insertActiveFile(final String template, final String activeKey, final String pathKey, final String path) {
    final boolean active = path != null;

    return replaceActive(template, activeKey, active).replace(pathKey, active ? path : EMPTY_VALUE);
  }

  private static String insertPointsFileLocation(final String template, final File pointsFile) {
    return template.replace(GRD_PTS_POINTS_FILE_PATH_KEY, pointsFile.getAbsolutePath());
  }

  private static String insertBuildings(final String template, final List<ADMSBuilding> buildings, final List<ADMSSource<?>> sources) {
    return replaceActive(template, SUP_MODEL_BUILDINGS,
        !buildings.isEmpty() && sources.stream().anyMatch(s -> s.getMainBuilding() != null && !s.getMainBuilding().isBlank()))
            .replace(ADMS_PARAMETERS_BLD_KEY, AplBuildingsBuilder.build(buildings));
  }

  private String insertPollutants(final String template, final List<Substance> substances) {
    // Build &ADMS_PARAMETERS_OPT
    final String templateWithOpt = template.replace(ADMS_PARAMETERS_OPT_KEY, AplOutputPollutantsBuilder.build(substances));

    // Build &ADMS_POLLUTANT_DETAILS
    // TODO if other than default need to be added enable next line to read specific pollutant files.
    //    final String pollutants = substances.stream().map(pollutantBuilder::getPollutantBlock).collect(Collectors.joining(OSUtils.NL));
    final String pollutants = POLLUTANT_BUILDER.getAplDefault();
    return templateWithOpt
        .replace(ADMS_POLLUTANT_DETAILS_KEY, pollutants)
        .replace(AplFileConstants.POL_NUM_POLLUTANTS, String.valueOf(AplFileConstants.POL_NUM_POLLUTANTS_DEFAULT));
  }

  private String setDryDepositionCalculationOption(final String template, final AplFileContent content) {
    return replaceActive(template, AplFileConstants.SUP_CALC_DRY_DEP, content.isSupCalcDryDep());
  }

  private void writeSources(final FileWriter writer, final List<ADMSSource<?>> sources, final ADMSGroupKey admsGroupKey) throws IOException {
    for (final ADMSSource<?> source : sources) {
      writer.append(replaceNewline(AplSourceBuilder.build(fileType, source, admsGroupKey)));
    }
  }

  private static String replaceActive(final String template, final String activeKey, final boolean active) {
    return template.replace(activeKey, active ? ACTIVE : NOT_ACTIVE);
  }

  private String replaceNewline(final String string) {
    return OSUtils.NL.equals(newline) ? string : string.replace(OSUtils.NL, newline);
  }
}
