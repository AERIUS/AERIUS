/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import org.opengis.referencing.FactoryException;

import nl.aerius.adms.conversion.ADMS2EmissionSourceConverter;
import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.characteristics.ADMSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.SourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Imports data from a ADMS UPL file.
 */
public class AplImportReader implements ImportReader {
  private static final String APL_FILE_EXTENSION = ".apl";
  private static final String UPL_FILE_EXTENSION = ".upl";
  private static final Object MAIN_BUILDING = "(Main)";

  @Override
  public boolean detect(final String filename) {
    final String lowerCase = filename.toLowerCase(Locale.ROOT);

    return lowerCase.endsWith(APL_FILE_EXTENSION) || lowerCase.endsWith(UPL_FILE_EXTENSION);
  }

  @Override
  public void read(final String filename, final InputStream inputStream, final SectorCategories categories, final Substance substance,
      final ImportParcel importParcel) throws IOException, AeriusException {
    try {
      final ScenarioSituation situation = importParcel.getSituation();
      final AplFileContent results = new AplFileReader(ADMSSourceFileType.fileTypeFromExtension(filename)).readObjects(inputStream);

      importParcel.getExceptions().addAll(results.getExceptions());

      convertSources(categories, situation, results.getObjects(), importParcel.getExceptions());
      convertBuildings(situation.getBuildingsList(), results.getBuildings(), importParcel.getExceptions());
      linkBuildings(situation.getEmissionSourcesList(), results.getObjects(), situation.getBuildingsList());
      readCalculationSetOptions(importParcel, results);
    } catch (final IllegalArgumentException | FactoryException e) {
      throw new AeriusException(AeriusExceptionReason.ADMS_INPUT_VALIDATION, e.getMessage());
    }
  }

  private static void readCalculationSetOptions(final ImportParcel importParcel, final AplFileContent results) {
    final CalculationSetOptions calculationSetOptions = new CalculationSetOptions();
    calculationSetOptions.getNcaCalculationOptions().setAdmsOptions(results.getOptions());
    calculationSetOptions.setCalculationMethod(CalculationMethod.NATURE_AREA);
    importParcel.setCalculationSetOptions(calculationSetOptions);
  }

  private static void convertSources(final SectorCategories categories, final ScenarioSituation situation,
      final List<ADMSSource<?>> sources, final List<AeriusException> exceptions) throws AeriusException {
    final List<EmissionSourceFeature> emissionSources = situation.getEmissionSourcesList();

    for (int i = 0; i < sources.size(); i++) {
      emissionSources.add(ADMS2EmissionSourceConverter.convert(sources.get(i), String.valueOf(i + 1)));
    }
  }

  private static void convertBuildings(final List<BuildingFeature> list, final List<ADMSBuilding> buildings, final List<AeriusException> exceptions) {
    for (int i = 0; i < buildings.size(); i++) {
      final ADMSBuilding building = buildings.get(i);

      try {
        list.add(ADMS2EmissionSourceConverter.convert(building, String.valueOf(i + 1)));
      } catch (final AeriusException e) {
        exceptions.add(e);
      }
    }
  }

  private static void linkBuildings(final List<EmissionSourceFeature> emissionSourcesList, final List<ADMSSource<?>> admsSources,
      final List<BuildingFeature> buildingsList) {
    if (buildingsList.isEmpty()) {
      return;
    }
    // First building in list corresponds with {Main}
    final String mainBuildingId = buildingsList.get(0).getProperties().getGmlId();

    for (int i = 0; i < admsSources.size(); i++) {
      final String sourceMainBuilding = admsSources.get(i).getMainBuilding();

      if (sourceMainBuilding.isBlank()) {
        continue;
      } else {
        final SourceCharacteristics sc = emissionSourcesList.get(i).getProperties().getCharacteristics();

        if (sc instanceof ADMSSourceCharacteristics) {
          final String buildingId = MAIN_BUILDING.equals(sourceMainBuilding) ? mainBuildingId
              : GMLIdUtil.toValidGmlId(sourceMainBuilding, GMLIdUtil.BUILDING_PREFIX);

          ((ADMSSourceCharacteristics) sc).setBuildingId(buildingId);
        }
      }
    }
  }
}
