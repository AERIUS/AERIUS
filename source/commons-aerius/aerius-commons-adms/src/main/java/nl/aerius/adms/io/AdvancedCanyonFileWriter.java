/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import nl.aerius.adms.conversion.ADMSPorosityCalculator;
import nl.aerius.adms.domain.ADMSRoadSource;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrier;
import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrierType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

/**
 * File writer for Advanced Canyon files.
 */
class AdvancedCanyonFileWriter {

  private static final String ADVANCED_CANYON_VERSION = "AdvancedCanyonVersion2" + OSUtils.NL;
  private static final String HEADER = "Name,X1,Y1,X2,Y2,"
      + "width_L,avgHeight_L,minHeight_L,maxHeight_L,buildLength_L,"
      + "width_R,avgHeight_R,minHeight_R,maxHeight_R,buildLength_R,fracCovered" + OSUtils.NL;
  private static final String ACF_EXTENSION = ".csv";
  private static final ADMSRoadSideBarrier NO_ROAD_SIDE_BARRIER = new ADMSRoadSideBarrier();

  private final File acFile;
  private Writer writer;

  /**
   * Constructor
   *
   * @param outputFile Output file with path, but without the file extension
   */
  public AdvancedCanyonFileWriter(final File outputFile) {
    acFile = ADMSFileConstants.constructDataFilename(outputFile, ACF_EXTENSION);
  }

  /**
   * Writes the Advanced Canyon rows for the given sources.
   *
   * @param sources a sources to get Advanced Canyon data from
   * @return returns true if at least 1 entry was written to file
   * @throws IOException
   * @throws AeriusException
   */
  public final boolean write(final List<ADMSSource<?>> sources) throws IOException, AeriusException {
    boolean hasData = false;
    try {
      for (final ADMSSource<?> admsSource : sources) {
        if (hasBarrier(admsSource)) {
          if (!hasData) {
            openAndWriteHeader();
          }
          writeRoadData((ADMSRoadSource) admsSource);
          hasData = true;
        }
      }
    } finally {
      if (writer != null) {
        writer.close();
      }
    }
    return hasData;
  }

  private static boolean hasBarrier(final ADMSSource<?> admsSource) {
    if (admsSource instanceof ADMSRoadSource) {
      final ADMSRoadSource road = ((ADMSRoadSource) admsSource);

      return hasRoadSideBarrier(road.getLeftRoadSideBarrier()) || hasRoadSideBarrier(road.getRightRoadSideBarrier());
    } else {
      return false;
    }
  }

  private static boolean hasRoadSideBarrier(final ADMSRoadSideBarrier roadSideBarrier) {
    return roadSideBarrier != null && roadSideBarrier.getBarrierType() != ADMSRoadSideBarrierType.NONE;
  }

  /**
   * @return Returns the full name of the advanced canyon output file
   */
  public File getOutputFile() {
    return acFile;
  }

  private void openAndWriteHeader() throws IOException {
    writer = Files.newBufferedWriter(acFile.toPath(), StandardCharsets.UTF_8);
    writer.append(ADVANCED_CANYON_VERSION);
    writer.append(HEADER);
  }

  private void writeRoadData(final ADMSRoadSource admsSource) throws IOException, AeriusException {
    final List<String> row = new ArrayList<>();
    // write name
    row.add(admsSource.getName());
    // write x1,y1
    addCoordinate(row, admsSource, 0, 0);
    addCoordinate(row, admsSource, 0, 1);
    // write y1, y2
    addCoordinate(row, admsSource, 1, 0);
    addCoordinate(row, admsSource, 1, 1);
    // write left road barrier
    addRoadBarrier(row, safeBarrier(admsSource.getLeftRoadSideBarrier()), admsSource.getGeometry());
    // add right road barrier
    addRoadBarrier(row, safeBarrier(admsSource.getRightRoadSideBarrier()), admsSource.getGeometry());
    // add frac coverage
    row.add(String.valueOf(admsSource.getFractionCovered()));
    writer.append(String.join(",", row));
    writer.append(OSUtils.NL);
  }

  private ADMSRoadSideBarrier safeBarrier(final ADMSRoadSideBarrier roadSideBarrier) {
    return roadSideBarrier == null ? NO_ROAD_SIDE_BARRIER : roadSideBarrier;
  }

  private void addCoordinate(final List<String> row, final ADMSRoadSource admsSource, final int point, final int xy) {
    row.add(String.valueOf(admsSource.getGeometry().getCoordinates()[point][xy]));
  }

  private void addRoadBarrier(final List<String> row, final ADMSRoadSideBarrier barrier, final LineString road) throws AeriusException {
    row.add(String.valueOf(barrier.getWidth()));
    row.add(String.valueOf(barrier.getAverageHeight()));
    row.add(String.valueOf(barrier.getMinimumHeight()));
    row.add(String.valueOf(barrier.getMaximumHeight()));
    row.add(String.valueOf(ADMSPorosityCalculator.toRoadLengthWithBuildings(road, barrier.getPorosity())));
  }
}
