/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.domain.NCACalculationResultPoint;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.AeriusPointUtil;

/**
 * Reads ADMS Point Long Term results (.plt) file.
 */
public class PltFileReader extends AbstractPointReader<NCACalculationResultPoint> {

  private static final String EXTENSION = ".plt";
  private static final String COLUMN_CONCENTRATION = "conc";

  private static final Logger LOG = LoggerFactory.getLogger(PltFileReader.class);

  private final Collection<EmissionResultKey> resultKeys;
  // Map<EmissionResultKey, index in column>
  private final Map<EmissionResultKey, Integer> columnIndex = new HashMap<>();
  // Map<EmissionResultKey, name of the column>
  private final Map<EmissionResultKey, String> columnName = new HashMap<>();

  public PltFileReader(final Collection<EmissionResultKey> resultKeys) {
    super(1);
    this.resultKeys = resultKeys;
  }

  public LineReaderResult<NCACalculationResultPoint> read(final File outputFile) throws IOException, AeriusException {
    final String file = outputFile.getAbsolutePath() + EXTENSION;
    if (!new File(file).exists()) {
      LOG.error("ADMS run didn't generate output file: {}", outputFile);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    try (final InputStream is = new FileInputStream(file)) {
      return readObjects(is);
    }
  }

  @Override
  protected String skipHeaderRow(final BufferedReader reader, final int headerRowCount) throws IOException, AeriusException {
    final String headerRow = super.skipHeaderRow(reader, headerRowCount);
    preprocessLine(headerRow);

    for (int i = 4; i < nrOfColums(); i++) {
      final String column = processColumn(i);

      for (final EmissionResultKey erk : resultKeys) {
        if (findColumn(column, erk)) {
          columnIndex.put(erk, i);
          columnName.put(erk, column);
          break;
        }
      }
    }
    return headerRow;
  }

  private boolean findColumn(final String column, final EmissionResultKey erk) {
    final String columnLowercase = column.toLowerCase(Locale.ROOT);

    return erk.getEmissionResultType() == EmissionResultType.CONCENTRATION &&
        columnLowercase.contains(erk.getSubstance().getName().toLowerCase(Locale.ROOT)) && columnLowercase.contains(COLUMN_CONCENTRATION);
  }

  @Override
  protected NCACalculationResultPoint parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final AeriusPoint decodedPoint = AeriusPointUtil.decodeName(getId());
    final NCACalculationResultPoint point = new NCACalculationResultPoint(decodedPoint);

    setFields(point);
    for (final EmissionResultKey erk : resultKeys) {
      if (columnIndex.containsKey(erk)) {
        point.setEmissionResult(erk, getDouble(columnName.get(erk), columnIndex.get(erk)));
      }
    }
    return point;
  }

}
