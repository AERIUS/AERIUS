/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.worker.EngineWorkerConfiguration;

/**
 * Configuration file for ADMS.
 */
public class ADMSConfiguration extends EngineWorkerConfiguration<ADMSVersion> {

  private Optional<String> admsLicenseBase64;
  private int progressReportIntervalSeconds;

  public ADMSConfiguration(final int processes) {
    super(processes);
  }

  public File getADMSRoot(final ADMSVersion admsVersion) {
    return new File(getVersionsRoot(), admsVersion.getVersion());
  }

  public File getDepositionVelocityFile(final ADMSVersion admsVersion) {
    return getDataFilePath(admsVersion.getDepositionVelocityVersion(), ".csv");
  }

  public File getSurfaceRoughnessFile(final ADMSVersion admsVersion) {
    return getDataFilePath(admsVersion.getSurfaceRoughnessVersion(), ".tif");
  }

  public File getTerrainHeightFile(final ADMSVersion admsVersion, final int resolution) {
    return getDataFilePath(admsVersion.getTerrainHeightVersionFormat(), admsVersion.getTerrainHeightFileName(resolution), ".tif");
  }

  public File getPlumeDepletionFile(final ADMSVersion admsVersion, final Substance substance) {
    return getDataFilePath(admsVersion.getPlumeDepletionVersionFormat(), admsVersion.getPlumeDepletionFileName(substance), ".tif");
  }

  public File getMetFilesFolder(final ADMSVersion admsVersion) {
    return getVersionsRoot() == null ? null : new File(getVersionsRoot(), String.join(File.separator, admsVersion.getMetFilesVersion()));
  }

  public File getNOxToNO2CalculatorFolder(final ADMSVersion admsVersion) {
    return getVersionsRoot() == null ? null : new File(getVersionsRoot(), admsVersion.getNOxToNO2Version().getDataDirectory());
  }

  private File getDataFilePath(final String name, final String extension) {
    return getDataFilePath(name, name, extension);
  }

  private File getDataFilePath(final String directory, final String name, final String extension) {
    return getVersionsRoot() == null ? null : new File(new File(getVersionsRoot(), directory), name + extension);
  }

  public File createOutputDirectory() throws IOException {
    return FileUtil.createDirectory(getRunFilesDirectory(), "adms");
  }

  public Optional<String> getAdmsLicenseBase64() {
    return admsLicenseBase64;
  }

  void setAdmsLicenseBase64(final Optional<String> admsLicenseBase64) {
    this.admsLicenseBase64 = admsLicenseBase64;
  }

  public int getProgressReportIntervalSeconds() {
    return progressReportIntervalSeconds;
  }

  public void setProgressReportIntervalSeconds(final int progressReportIntervalSeconds) {
    this.progressReportIntervalSeconds = progressReportIntervalSeconds;
  }
}
