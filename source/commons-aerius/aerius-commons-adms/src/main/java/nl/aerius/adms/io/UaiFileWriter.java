/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.zone.ZoneOffsetTransition;
import java.time.zone.ZoneRules;
import java.util.List;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Writer to create a ADMS Advanced input file (UAI)
 */
class UaiFileWriter {

  private static final String EXTENSION = ".uai";
  private static final String UAI_VERSION = "uaifileversion6";

  private static final String YES = "Y";
  private static final String NO = "N";

  // Adjust Daylight saving time
  private static final String DST_FORMAT = "uuuu,MM,dd,HH";
  private static final DateTimeFormatter DST_SDF = DateTimeFormatter.ofPattern(DST_FORMAT);
  private static final String ADJUSTFACTORS_DST = "ADJUSTFACTORS_DST";
  private static final int FIRST_DST_YEAR = 2014;
  private static final int LAST_DST_YEAR = 2050;
  private static final String NUMBER_OF_DST = String.valueOf(2 * (LAST_DST_YEAR - FIRST_DST_YEAR + 1)); // Number of years * 2
  private static final String FORMAT = "%s";
  private static final String START = "START";
  private static final String END = "END";
  private static final String ONE = "1";
  private static final List<String> ADJUSTFACTORS_DST_OPTIONS_BEFORE = List.of(
      ADJUSTFACTORS_DST,
      YES,
      NUMBER_OF_DST // Number of entries
  );
  private static final List<String> ADJUSTFACTORS_DST_OPTIONS_FOR_EACH_YEAR = List.of(
      FORMAT, // Start Date
      START, // keyword START
      FORMAT, // End data
      END // keyword END
  );
  // Advanced Street Canyon
  private static final String ADVANCEDSTREETCANYONS = "ADVANCEDSTREETCANYONS";
  private static final String ADV_CANYON_CSV = "{ADV_CANYON_CSV}";
  private static final String ADV_CANYON_PATH = "PATH = \"" + ADV_CANYON_CSV + '"';
  private static final List<String> ADVANCED_STREET_CANYONS_OPTIONS = List.of(
      ADVANCEDSTREETCANYONS, // keyword
      YES, // use path
      ADV_CANYON_PATH, // Path to advanced street canyon file
      NO, // Suppress warnings from the afvanced canyon module
      NO, // Use network mode
      NO); // Specify parameters for advanced canyon modeling

  // Variable Dry Deposition
  private static final String VARIABLE_DRY_DEPOSITION = "VARDEPOSITION";
  private static final String VAR_DRY_DEP_DIN = "{DIN_FILE}";
  private static final String VAR_DRY_DEP_SUBSTANCE = "{DRY_DEP_SUBSTANCE}";
  private static final String VAR_DRY_DEP_DIN_PATH = "PATH = \"" + VAR_DRY_DEP_DIN + '"';
  private static final List<String> VAR_DRY_DEP_OPTIONS = List.of(
      VARIABLE_DRY_DEPOSITION,
      "1",
      VAR_DRY_DEP_SUBSTANCE,
      "VD",
      VAR_DRY_DEP_DIN_PATH,
      NO);

  // Quick run option
  private static final List<String> QUICK_RUN = List.of("QUICKRUN", "Y");

  private final File uaiFile;
  private final String uaiVersion;
  private final String advancedStreetCanyons;
  private final String newline;
  private final String variableDryDeposition;
  private final ZoneId zoneId;
  private final DateTimeFormatter dtf;
  private File advCanyonCsvFile;
  private String dinFileLocation;
  private Substance substance;
  private boolean hasQuickRun;
  private final String quickRun;

  public UaiFileWriter(final File outputFile, final String timeZone, final String newline) {
    this.newline = newline;
    uaiFile = new File(outputFile.getParentFile(), outputFile.getName() + EXTENSION);
    uaiVersion = UAI_VERSION + newline + newline;
    advancedStreetCanyons = String.join(newline, ADVANCED_STREET_CANYONS_OPTIONS) + newline;
    variableDryDeposition = String.join(newline, VAR_DRY_DEP_OPTIONS) + newline;
    quickRun = String.join(newline, QUICK_RUN) + newline;
    zoneId = ZoneId.of(timeZone);
    dtf = DST_SDF.withZone(zoneId);
  }

  /**
   * Set the Advanced Canyon Street filename. If not null it means the Advanced Canyon Street was created.
   */
  public void setASC(final File advCanyonCsvFile) {
    this.advCanyonCsvFile = advCanyonCsvFile;
  }

  /**
   * Set the variable dry deposition filename. If not null it means dry deposition (i.e. plume depletion) should be calculated.
   */
  public void setDinFile(final String dinFileLocation) {
    this.dinFileLocation = dinFileLocation;
  }

  /**
   * Sets the quick run mode.
   *
   * @param activate if true quick run mode is activated.
   */
  public void setQuickRun(final boolean activate) {
    this.hasQuickRun = activate;
  }

  /**
   * Set the substance to calculate dy deposition for.
   */
  public void setSubstance(final Substance substance) {
    this.substance = substance;
  }

  /**
   * Writes the UAI file to disk.
   *
   * @return Returns the UAI file that was written to disk
   * @throws IOException
   */
  public File writeUaiFile() throws IOException {
    final boolean hasACF = advCanyonCsvFile != null;
    final boolean hasDinFile = dinFileLocation != null;

    try (final FileWriter writer = new FileWriter(uaiFile, StandardCharsets.UTF_8)) {
      writer.append(uaiVersion);
      writeDST(writer);

      if (hasACF) {
        writer.append(advancedStreetCanyons.replace(ADV_CANYON_CSV, advCanyonCsvFile.getAbsolutePath()));
      }

      if (hasDinFile) {
        writer.append(variableDryDeposition.replace(VAR_DRY_DEP_DIN, dinFileLocation).replace(VAR_DRY_DEP_SUBSTANCE, substance.getName()));
      }
      if (hasQuickRun) {
        writer.append(quickRun);
      }
    }
    return uaiFile;
  }

  private void writeDST(final FileWriter writer) throws IOException {
    final ZoneRules rules = zoneId.getRules();

    writer.append(String.format(String.join(newline, ADJUSTFACTORS_DST_OPTIONS_BEFORE)) + newline);

    // Write DST data for each supported year
    for (int year = FIRST_DST_YEAR; year <= LAST_DST_YEAR; year++) {
      final ZonedDateTime firstDayOfYear = ZonedDateTime.of(LocalDateTime.of(year, 1, 1, 0, 0, 0), zoneId);
      final ZoneOffsetTransition start = rules.nextTransition(firstDayOfYear.toInstant());
      final ZoneOffsetTransition end = rules.nextTransition(start.getInstant());

      writer.append(
          String.format(
              String.join(newline, ADJUSTFACTORS_DST_OPTIONS_FOR_EACH_YEAR),
              dtf.format(start.getDateTimeBefore()),
              dtf.format(end.getDateTimeBefore()))
              + newline);
    }

    writer.append(ONE + newline);

  }
}
