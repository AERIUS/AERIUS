/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.util.List;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.AeriusPointUtil;

/**
 * File reader to read calculation points from an ADMS ASP file.
 */
public class AspFileReader extends AbstractPointReader<AeriusPoint> {

  public AspFileReader() {
    super(0);
  }

  @Override
  protected AeriusPoint parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final AeriusPoint decodedPoint = AeriusPointUtil.decodeName(getId());
    final AeriusPoint point = new AeriusPoint(decodedPoint.getId(), decodedPoint.getParentId(), decodedPoint.getPointType(), getX(), getY());

    setFields(point);
    return point;
  }
}
