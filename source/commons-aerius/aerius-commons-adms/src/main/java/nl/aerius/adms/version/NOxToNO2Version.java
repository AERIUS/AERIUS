/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.version;

import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * NOx to NO2 calculator data versions.
 */
public enum NOxToNO2Version {
  V8_1(
      "nox_to_no2_calculator_v8.1",
      "la2018a.shp",
      "UWE_2016",
      2018,
      2030,
      "lacodes_no220182030.csv",
      "lacodes_nox20182030.csv",
      "lacodes_o320182030.csv",
      "fNO2_lookup.shp",
      "fNO2_factors_NAEI17_2015-2030_v2.csv",
      2015,
      2030,
      Map.of("London", "All London traffic", "Urban not London", "All other urban UK traffic", "", "All non-urban UK traffic")
      );

  private final String dataDirectory;
  private final String uweShapeFilename;
  private final String uweShapeAttribute;
  private final int laCodeFirstYear;
  private final int laCodeLastYear;
  private final String laCodesNO2Filename;
  private final String laCodesNOXFilename;
  private final String laCodesO3Filename;
  private final String fNO2ShapeFilename;
  private final String fNO2FactorsFilename;
  private final int fNO2FirstYear;
  private final int fNO2LastYear;
  private final Map<String, String> fNO2AreaMap;

  NOxToNO2Version(
      final String dataDirectory,
      final String uweShapeFilename,
      final String uweShapeAttribute,
      final int laCodeFirstYear,
      final int laCodeLastYear,
      final String laCodesNO2Filename,
      final String laCodesNOXFilename,
      final String laCodesO3Filename,
      final String fNO2ShapeFilename,
      final String fNO2FactorsFilename,
      final int fNO2FirstYear,
      final int fNO2LastYear,
      final Map<String, String> fNO2AreaMap) {
    this.dataDirectory = dataDirectory;
    this.uweShapeFilename = uweShapeFilename;
    this.uweShapeAttribute = uweShapeAttribute;
    this.laCodeFirstYear = laCodeFirstYear;
    this.laCodeLastYear = laCodeLastYear;
    this.laCodesNO2Filename = laCodesNO2Filename;
    this.laCodesNOXFilename = laCodesNOXFilename;
    this.laCodesO3Filename = laCodesO3Filename;
    this.fNO2ShapeFilename = fNO2ShapeFilename;
    this.fNO2FactorsFilename = fNO2FactorsFilename;
    this.fNO2FirstYear = fNO2FirstYear;
    this.fNO2LastYear = fNO2LastYear;
    this.fNO2AreaMap = fNO2AreaMap;
  }

  public String getDataDirectory() {
    return dataDirectory;
  }

  public String getUweShapeFilename() {
    return uweShapeFilename;
  }

  public String getUweShapeAttribute() {
    return uweShapeAttribute;
  }

  public int getLACodeFirstYear() {
    return laCodeFirstYear;
  }

  public int getLACodeLastYear() {
    return laCodeLastYear;
  }

  public String getLaCodesFilename(final Substance substance) {
    switch (substance) {
    case NO2:
      return laCodesNO2Filename;
    case NOX:
      return laCodesNOXFilename;
    case O3:
      return laCodesO3Filename;
    default:
      return null;
    }
  }

  public String getfNO2ShapeFilename() {
    return fNO2ShapeFilename;
  }

  public String getfNO2FactorsFilename() {
    return fNO2FactorsFilename;
  }

  public int getfNO2FirstYear() {
    return fNO2FirstYear;
  }

  public int getfNO2LastYear() {
    return fNO2LastYear;
  }

  /**
   * Returns the area name as used in the fNO2 csv file given the name in the shape file.
   * If the shape area name could not be found or is null it returns the default value as configured in the empty string key.
   *
   * @param shapeAreaName shape area name
   * @return fNO2 csv area name
   */
  public String getFNO2Area(final String shapeAreaName) {
    return shapeAreaName == null ? fNO2AreaMap.get("") : fNO2AreaMap.getOrDefault(shapeAreaName, fNO2AreaMap.get(""));
  }
}
