/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import java.io.Serializable;

/**
 * Data record to store all additional data needed to calculate NO2 concentrations from NOx for road sources.
 *
 * @param backgroundNOx system provided background NOx
 * @param laNOxBackground local Authority NOx background
 * @param laNO2Background local Authority NO2 background
 * @param laO3Background local Authority O3 background
 * @param localFNO2 Road local NO2 factor
 * @param regionalFNO2 Road regional NO2 factor
 */
public record ADMSPointNO2Data(double backgroundNOx, double laNOxBackground, double laNO2Background, double laO3Background,
    double localFNO2, double regionalFNO2) implements Serializable {
}
