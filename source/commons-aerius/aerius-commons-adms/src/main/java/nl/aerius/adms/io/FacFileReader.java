/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.aerius.adms.domain.ADMSSource;
import nl.aerius.adms.domain.ADMSTimeVaryingProfile;
import nl.overheid.aerius.io.AbstractLineReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Class to read ADMS time-varying profiles (.fac) files.
 *
 * This reader supports reading 3 day and monthly profiles.
 */
class FacFileReader extends AbstractLineReader<Void> {

  /**
   * Enum to keep track the specific data to be read.
   *
   * The order of the enum is relevant and should not be changed.
   * It represents the order in the fac file.
   */
  private enum ReadingState {
    /**
     * 3-day profile
     */
    THREE_DAYS,
    /**
     * 7 day profiles
     */
    ZEVEN_DAYS,
    /**
     * Monthly profiles
     */
    MONTHLY,
    /**
     * Linking profiles to sources
     */
    SOURCES,
  }

  /**
   * Enum to keep track of the state of a specific profile that is being read.
   */
  private enum ProfileReadingState {
    /**
     * Name of the profile
     */
    NAME,
    /**
     * Values for the week days.
     */
    WEEKDAY,
    /**
     * Values for the week days.
     */
    SATURDAY,
    /**
     * Values for the week days.
     */
    SUNDAY,
    /**
     * Values for the monthly profile.
     */
    MONTHLY;
  }

  private enum SourceReadingState {
    /**
     * Name of the source and profile configuration.
     */
    NAME(0),
    /**
     * Line with hourly profile if enabled.
     */
    HOURLY(1),
    /**
     * Line with monthly profile if enabled.
     */
    MONTHLY(2),
    /**
     * Line with Phi values if enabled.
     */
    PHI(4);

    private final int bit;

    SourceReadingState(final int bit) {
      this.bit = bit;
    }

    public int flag(final String flag) {
      return "1".equals(flag) ? bit : 0;
    }

    public boolean isSet(final int bits) {
      return (bit & bits) == bit;
    }
  }

  private static final String SPLIT_PATTERN = ",";
  private static final int HEADER_COUNT = 1;
  private static final int MAX_LINE_LENGTH = 1000;

  // Fields to keep track of number of profiles to read.
  private ReadingState activeReadingState = ReadingState.THREE_DAYS;
  private int numberOfProfiles = -1;
  private int profilesRead = 0;

  // Fields to keep track of profile read progress.
  private ProfileReadingState profileReadState = ProfileReadingState.NAME;
  private ADMSTimeVaryingProfile currentADMSTimeVaryingProfile;

  // Fields to keep track of source profile information read.
  private String sourceName = null;

  // Fields to collect read profiles/sources
  private final List<ADMSTimeVaryingProfile> timeVaryingProfiles = new ArrayList<>();
  private final Map<String, String> sourceToHourlyProfile = new HashMap<>();
  private final Map<String, String> sourceToMonthlyProfile = new HashMap<>();
  private int sourceProfileFlags;
  private SourceReadingState sourceReadState = SourceReadingState.NAME;

  public FacFileReader() {
    super(true);
  }

  /**
   * Reads the time-varying profiles from the fac file and links the profiles to the sources.
   * The sources are linked by their label.
   *
   * @param is data stream
   * @param sources source to link profiles to
   */
  public LineReaderResult<Void> readObjects(final InputStream is, final List<ADMSSource<?>> sources) throws IOException {
    final LineReaderResult<Void> result = readObjects(is);
    linkSources(sources);
    return result;
  }

  @Override
  public LineReaderResult<Void> readObjects(final InputStream inputStream) throws IOException {
    try (final InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
      return readObjects(reader, HEADER_COUNT);
    }
  }

  public List<ADMSTimeVaryingProfile> getProfiles() {
    return timeVaryingProfiles;
  }

  public void linkSources(final List<ADMSSource<?>> sources) {
    for (final ADMSSource<?> source : sources) {
      source.setHourlyDayTimeVaryingProfileName(sourceToHourlyProfile.getOrDefault(source.getName(), null));
      source.setMonthlyTimeVaryingProfileName(sourceToMonthlyProfile.getOrDefault(source.getName(), null));
    }
  }

  @Override
  protected Void parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    if (activeReadingState == ReadingState.SOURCES) {
      parseSources(line);
    } else if (numberOfProfiles < 0) {
      numberOfProfiles = getInt("", 0);
    } else if (profilesRead < numberOfProfiles) {
      parseProfile(line);
    } else if (line.isBlank()) {
      profilesRead = 0;
      numberOfProfiles = -1;
      activeReadingState = ReadingState.values()[activeReadingState.ordinal() + 1];
    }
    return null;
  }

  private void parseProfile(final String line) throws AeriusException {
    switch (profileReadState) {
    case NAME:
      currentADMSTimeVaryingProfile = new ADMSTimeVaryingProfile();
      currentADMSTimeVaryingProfile.setName(line.trim());
      parseName();
      break;
    case WEEKDAY:
      currentADMSTimeVaryingProfile.getValues().addAll(parseProfileNumbers(line, ProfileReadingState.WEEKDAY));
      profileReadState = ProfileReadingState.SATURDAY;
      break;
    case SATURDAY:
      currentADMSTimeVaryingProfile.getValues().addAll(parseProfileNumbers(line, ProfileReadingState.SATURDAY));
      profileReadState = ProfileReadingState.SUNDAY;
      break;
    case SUNDAY:
      currentADMSTimeVaryingProfile.getValues().addAll(parseProfileNumbers(line, ProfileReadingState.SUNDAY));
      timeVaryingProfiles.add(currentADMSTimeVaryingProfile);
      profilesRead++;
      currentADMSTimeVaryingProfile = null;
      profileReadState = ProfileReadingState.NAME;
      break;
    case MONTHLY:
      currentADMSTimeVaryingProfile.getValues().addAll(parseProfileNumbers(line, ProfileReadingState.MONTHLY));
      timeVaryingProfiles.add(currentADMSTimeVaryingProfile);
      profilesRead++;
      currentADMSTimeVaryingProfile = null;
      profileReadState = ProfileReadingState.NAME;
      break;
    default:
      assert false : "Unknown profile read state: " + profileReadState;
    }
  }

  private void parseName() {
    switch (activeReadingState) {
    case THREE_DAYS:
      currentADMSTimeVaryingProfile.setType(CustomTimeVaryingProfileType.THREE_DAY);
      profileReadState = ProfileReadingState.WEEKDAY;
      break;
    case ZEVEN_DAYS:
      assert false : "7 days profile not supported";
      break;
    case MONTHLY:
      currentADMSTimeVaryingProfile.setType(CustomTimeVaryingProfileType.MONTHLY);
      profileReadState = ProfileReadingState.MONTHLY;
      break;
    case SOURCES:
    default:
      assert false : "Unknown reading state: " + activeReadingState;
    }
  }

  private List<Double> parseProfileNumbers(final String line, final ProfileReadingState state) throws AeriusException {
    try {
      return Stream.of(line.trim().split(SPLIT_PATTERN)).map(Double::valueOf).collect(Collectors.toList());
    } catch (final NumberFormatException e) {
      throw new AeriusException(AeriusExceptionReason.IO_EXCEPTION_NUMBER_FORMAT, String.valueOf(getCurrentLineNumber()), state.name(),
          line.substring(0, MAX_LINE_LENGTH));
    }
  }

  private void parseSources(final String line) throws AeriusException {
    switch(sourceReadState) {
    case NAME:
      parseSourceName(line);
      break;
    case HOURLY:
      sourceToHourlyProfile.put(sourceName, line.trim());
      sourceReadState = SourceReadingState.MONTHLY.isSet(sourceProfileFlags) ? SourceReadingState.MONTHLY : SourceReadingState.NAME;
      break;
    case MONTHLY:
      sourceToMonthlyProfile.put(sourceName, line.trim());
      sourceReadState = SourceReadingState.NAME;
      break;
    case PHI:
      // For now we don't add support for reading Phi line as it's not used.
      break;
    }
  }

  private void parseSourceName(final String line) throws AeriusException {
    final String[] split = line.trim().split(SPLIT_PATTERN);
    if (split.length == 4) {
      sourceName = split[0];
    } else {
      throw new AeriusException(AeriusExceptionReason.IO_EXCEPTION_UNKNOWN, String.valueOf(getCurrentLineNumber()));
    }
    sourceProfileFlags = SourceReadingState.HOURLY.flag(split[1]) + SourceReadingState.MONTHLY.flag(split[2]) + SourceReadingState.PHI.flag(split[3]);
    sourceReadState = SourceReadingState.HOURLY.isSet(sourceProfileFlags) ? SourceReadingState.HOURLY :  SourceReadingState.MONTHLY;
  }
}
