/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.util.GeometryCalculatorImpl;

/**
 * Converts emissions from ADMS to AERIUS emission Source objects and back.
 * Emissions in AERIUS emission source objects are the total emission in kg/y.
 * The emissions in ADMS objects are in units:
 * <table>
 * <tr><td>Point<td>g/s
 * <tr><td>Jet<td>g/s
 * <tr><td>Line<td>g/m/s
 * <tr><td>Area<td>g/m2/s
 * <tr><td>Volume<td>g/m3/s
 * <tr><td>Road<td>g/km/s
 * </table>
 */
final class EmissionRateConverter {

  private static final BigDecimal M_TO_KM = BigDecimal.valueOf(1000);

  private static final GeometryCalculator GEOMETRY_CALCULATOR = new GeometryCalculatorImpl();

  private EmissionRateConverter() {
    // Util class
  }

  /**
   * Sets the converted emissions rates on the ADMS source given the emission source emissions (kg/y).
   * ADMS source should have source type, geometry and source height set.
   *
   * @param substances substances to set the emissions for
   * @param emissions emissions in kg/y
   * @param admsSource adms source to set the emssions on.
   * @throws AeriusException
   */
  public static void emissionSource2ADMSEmission(final List<Substance> substances, final Map<Substance, Double> emissions,
      final ADMSSource<?> admsSource) throws AeriusException {
    for (final Substance substance : substances) {
      admsSource.setEmission(substance, calculateEmissionSource2ADMS(admsSource, BigDecimal.valueOf(emissions.getOrDefault(substance, 0.0))));
    }
  }

  private static double calculateEmissionSource2ADMS(final ADMSSource<?> admsSource, final BigDecimal emissionKgY) throws AeriusException {
    final BigDecimal factor = emissionFactor(admsSource);

    return emissionKgY.divide(factor, MathContext.DECIMAL64).divide(Substance.EMISSION_IN_G_PER_S_FACTOR_BD, MathContext.DECIMAL64).doubleValue();
  }

  /**
   * Sets the emissions in kg/y from the ADMS sources.
   *
   * @param admsSource adms source to get the emissions from
   * @param esEmssions object to set the emissions in kg/y
   * @throws AeriusException
   */
  public static void adms2EmissionSourceEmissions(final ADMSSource<?> admsSource, final Map<Substance, Double> esEmssions) throws AeriusException {
    for (final Entry<Substance, Double> entry : admsSource.getEmissions().entrySet()) {
      esEmssions.put(entry.getKey(), calculateADMS2EmissionSource(admsSource, entry.getKey()));
    }
  }

  private static double calculateADMS2EmissionSource(final ADMSSource<?> admsSource, final Substance substance) throws AeriusException {
    final BigDecimal emission = BigDecimal.valueOf(admsSource.getEmission(substance));
    final BigDecimal factor = emissionFactor(admsSource);

    return emission.multiply(factor).multiply(Substance.EMISSION_IN_G_PER_S_FACTOR_BD).doubleValue();
  }

  private static BigDecimal emissionFactor(final ADMSSource<?> source) throws AeriusException {
    final Geometry geometry = source.getGeometry();
    final BigDecimal factor;

    if (geometry instanceof Polygon) {
      final BigDecimal area = BigDecimal.valueOf(GEOMETRY_CALCULATOR.determineMeasure(geometry));
      if (source.getSourceType() == SourceType.VOLUME) {
        final double verticalDimension = source.getL1();
        // Safety check on vertical source dimension, it should be larger than 0, otherwise use 1.0 as vertical source dimension value.
        factor = area.multiply(verticalDimension > 0.0 ? BigDecimal.valueOf(verticalDimension) : BigDecimal.ONE);
      } else {
        factor = area;
      }
    } else if (geometry instanceof LineString) {
      final BigDecimal length = BigDecimal.valueOf(GEOMETRY_CALCULATOR.determineMeasure(geometry));

      if (source.getSourceType() == SourceType.ROAD) {
        factor = length.divide(M_TO_KM, MathContext.DECIMAL64);
      } else {
        factor = length;
      }
    } else {
      factor = BigDecimal.ONE;
    }
    return factor;
  }
}
