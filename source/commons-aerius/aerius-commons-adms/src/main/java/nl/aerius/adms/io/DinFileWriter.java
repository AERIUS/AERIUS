/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.ADMSConstants.NO2_TO_NOX_DEPOSITION_VELOCITY_CONVERSION_FACTOR;

import java.io.File;
import java.io.IOException;
import java.util.List;

import nl.aerius.adms.backgrounddata.ADMSData;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Writer for .din files
 */
public class DinFileWriter {

  private final ADMSVersion admsVersion;
  private final String newline;
  private final ADMSData admsData;

  public DinFileWriter(final ADMSVersion admsVersion, final String newline, final ADMSData admsData) {
    this.admsVersion = admsVersion;
    this.newline = newline;
    this.admsData = admsData;
  }

  /**
   * Write dinfile and return its absolute path, sets the parameter in APLFileContent for calculating dry deposition if a din file is written.
   * @param outputFile the filename to output to.
   * @param content the APL fileContent
   * @param extent the extent of deposition velocities to include.
   * @param substance if not null should write din file
   * @return path to din file, or {@code null} if no plume depletion is enabled in inputdata.
   */
  public String writeDinFile(final File outputFile, final AplFileContent content, final BBox extent, final Substance substance) throws IOException {
    final File dinFile = ADMSFileConstants.constructDataFilename(outputFile, ADMSFileConstants.DIN_EXTENSION);

    // Use NO2 deposition velocity for NOx calculations.
    final double valueScalar = substance == Substance.NOX
        ? NO2_TO_NOX_DEPOSITION_VELOCITY_CONVERSION_FACTOR : GeoTiffGridDataFileWriter.DEFAULT_VALUE_SCALAR;
    final Substance subtanceForDepositionVelocityLookup = substance == Substance.NOX ? Substance.NO2 : substance;

    // Write file contents
    final GeoTiffGridDataFileWriter writer = admsData.getPlumeDepletionWriter(admsVersion, subtanceForDepositionVelocityLookup);
    if (writer != null && writer.write(dinFile, extent, GeoTiffGridDataFileWriter.DataFileType.DIN, newline, valueScalar,
        admsVersion.getDinFileVersion())) {

      // Enable dry deposition calculation
      content.setSupCalcDryDep(true);
      return dinFile.getAbsolutePath();
    }
    return null;
  }

  /**
   * Returns the substance to use for plume depletion. When no plume depletion return null.
   *
   * @param inputData input data.
   * @param admsGroupKey source group key
   * @return substance of related to plume depletion or null
   * @throws AeriusException
   */
  public Substance substanceForPlumeDepletion(final ADMSInputData inputData, final ADMSGroupKey admsGroupKey) throws AeriusException {
    final List<Substance> substances = admsGroupKey.getEmissionSubstances(false);
    final Substance substance;

    if (ADMSGroupKey.NOX_ROAD == admsGroupKey) {
      substance = null; // Don't do plume depletion for road sources.
    } else if (inputData.getAdmsOptions().isPlumeDepletionNH3() && substances.contains(Substance.NH3)) {
      substance = Substance.NH3;
    } else if (inputData.getAdmsOptions().isPlumeDepletionNOX() && substances.contains(Substance.NOX)) {
      substance = Substance.NOX;
    } else {
      substance = null;
    }
    return substance;
  }
}
