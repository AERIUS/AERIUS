/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.File;
import java.util.concurrent.ExecutorService;

import nl.aerius.adms.backgrounddata.ADMSData;
import nl.aerius.adms.conversion.nox2.NOxToNO2BackgroundDataWriter;
import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.util.ADMSProgressCollector;
import nl.overheid.aerius.exec.ProcessListener;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.shared.domain.result.ExportResult;
import nl.overheid.aerius.util.FilenameUtil;
import nl.overheid.aerius.util.OSUtils;
import nl.overheid.aerius.worker.ZipAndProcessChainer;

/**
 * Generates all ADMS input files and zips them, posts the zip to the internal file server and returns the fileCode to the chunker worker.
 */
class ADMSExportRunner implements ADMSRunner<File> {

  private static final String PREFIX = "apas_adms";

  /**
   * Generated files should be kept by the processor as they are deleted by this class after adding them to the zip.
   */
  private static final boolean KEEP_GENERATED_FILES_PROCESSOR = true;

  private final ExecutorService executor;
  private final FileServerProxy fileServerProxy;
  private final boolean keepGeneratedFiles;
  private final ADMSConfiguration configuration;
  private final ADMSData admsData;

  public ADMSExportRunner(final ExecutorService executor, final ADMSConfiguration configuration, final ADMSData admsData,
      final FileServerProxy fileServerProxy) {
    this.executor = executor;
    this.configuration = configuration;
    this.admsData = admsData;
    this.fileServerProxy = fileServerProxy;
    keepGeneratedFiles = configuration.isKeepGeneratedFiles();
  }

  /**
   * Generates the ADMS input files, sends them to the file server and returns the fileCode.
   *
   * @param input input to generate input files from
   * @param jobKey
   * @return result with fileCode of zipped ADMS input files sent to the file server
   * @throws Exception
   */
  public ExportResult run(final ADMSInputData input, final String jobKey) throws Exception {
    final ProcessADMS<File> processor = new ProcessADMS<>(executor, this, admsData, configuration, true, KEEP_GENERATED_FILES_PROCESSOR);
    final File zipOutputDirectory = configuration.createOutputDirectory();

    try (final ZipAndProcessChainer chainer = new ZipAndProcessChainer(fileServerProxy, FilenameUtil.modelZipFilename(zipOutputDirectory, PREFIX),
        keepGeneratedFiles)) {
      processor.run(input, (key, outputDirectory) -> chainer.addDirectoryContent(outputDirectory, true), null, null, OSUtils.WNL);

      return new ExportResult(chainer.put(jobKey, input.getExpire()));
    }
  }

  @Override
  public File run(final ADMSProgressCollector progressCollector, final ProcessListener processListener, final ADMSInputData input,
      final ADMSGroupKey groupKey, final File outputDirectory, final File outputFile, final File projectFile) throws Exception {
    if (ADMSGroupKey.NOX_ROAD == groupKey) {
      NOxToNO2BackgroundDataWriter.writeNO2Conversion(admsData, input, outputDirectory);
    }
    return outputDirectory;
  }
}
