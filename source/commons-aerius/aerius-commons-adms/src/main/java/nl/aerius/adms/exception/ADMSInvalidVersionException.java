/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.exception;

/**
 * Exception thrown when the ADMS version in the input data doesn't match the version of the worker.
 */
public class ADMSInvalidVersionException extends Exception {

  private static final long serialVersionUID = 1L;

  public ADMSInvalidVersionException(final String expectedVersion, final String invalidVersion) {
    super("Expected ADMS " + expectedVersion + ", but found version " + invalidVersion);
  }
}
