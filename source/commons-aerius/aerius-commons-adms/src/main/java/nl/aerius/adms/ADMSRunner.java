/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.File;

import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.util.ADMSProgressCollector;
import nl.overheid.aerius.exec.ProcessListener;

/**
 * Interface for ADMS processes
 *
 * @param <T> type of the data returned by the process
 */
interface ADMSRunner<T> {

  /**
   * Method to perform the actual task.
   *
   * @param progressCollector
   * @Param processListener
   * @param input ADMS input
   * @param groupKey group key identifing type of input
   * @param outputDirectory output directory the input files are stored in
   * @param outputFile outputFile is a random filename in a random directory. Each input file type creates a file by appending it's extension.
   * @param projectFile ADMS project file
   * @return result of the operation
   * @throws Exception throws exception in case something fails
   */
  T run(ADMSProgressCollector progressCollector, ProcessListener processListener, ADMSInputData input, ADMSGroupKey groupKey, File outputDirectory,
      File outputFile, File projectFile) throws Exception;
}
