/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileConstants.ADMS_COORDINATESYSTEM;
import static nl.aerius.adms.io.AplFileConstants.ADMS_PARAMETERS_BLD;
import static nl.aerius.adms.io.AplFileConstants.ADMS_PARAMETERS_MET;
import static nl.aerius.adms.io.AplFileConstants.ADMS_PARAMETERS_SUP;
import static nl.aerius.adms.io.AplFileConstants.ADMS_SOURCE_DETAILS;
import static nl.aerius.adms.io.AplFileConstants.MET_MS_PARAMETER_MODE_ENTER_VALUE;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_MIN_LMO;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_MIN_LMO_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_PRIESTLY_TAYLOR;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_PRIESTLY_TAYLOR_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_SURFACE_ALBEDO;
import static nl.aerius.adms.io.AplFileKeys.MET_DS_SURFACE_ALBEDO_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_MIN_LMO;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_MIN_LMO_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_PRIESTLY_TAYLOR;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_PRIESTLY_TAYLOR_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_ROUGHNESS;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_ROUGHNESS_MODE;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_SURFACE_ALBEDO;
import static nl.aerius.adms.io.AplFileKeys.MET_MS_SURFACE_ALBEDO_MODE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.opengis.referencing.FactoryException;

import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ADMSOptions;
import nl.overheid.aerius.shared.domain.calculation.MetSurfaceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Reader to read ADMS project file with extension .apl or .upl.
 */
class AplFileReader {

  private static final int ACTIVE = 1;
  private final AplSourceReader sourceReader;
  private final AplBuildingsReader buildingsReader = new AplBuildingsReader();
  private EPSGTransformer epsgTransformer;

  public AplFileReader(final ADMSSourceFileType fileType) {
    sourceReader = new AplSourceReader(fileType);
  }

  public AplFileContent readObjects(final InputStream inputStream) throws IOException, AeriusException, FactoryException {
    final AplFileContent content = new AplFileContent();

    try (final InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        final BufferedReader br = new BufferedReader(isr)) {
      while (readBlock(br, content)) {
      }
    }
    setSubstances(content);
    epsgTransformer.transformCoordinates(content);
    return content;
  }

  private boolean readBlock(final BufferedReader br, final AplFileContent content) throws IOException, AeriusException, FactoryException {
    final AplBlockReader blockReader = AplBlockReader.readBlock(br);
    final String blockName = blockReader.getBlockName();

    if (blockName == null) {
      return false;
    }
    switch (blockName) {
    case ADMS_COORDINATESYSTEM:
      readEPSG(blockReader, content);
      break;
    case ADMS_PARAMETERS_SUP:
      readSupParameters(blockReader, content);
      break;
    case ADMS_PARAMETERS_MET:
      readMetParameters(blockReader, content);
      break;
    case ADMS_PARAMETERS_BLD:
      buildingsReader.addBuildings(blockReader, content);
      break;
    case ADMS_SOURCE_DETAILS:
      final ADMSSource<?> source = sourceReader.readSource(blockReader);
      if (source != null) {
        content.addObject(source);
      }
      break;
    }
    return true;
  }

  private void readEPSG(final AplBlockReader blockReader, final AplFileContent content) throws FactoryException {
    final int srid = blockReader.parseInt(AplFileKeys.PROJECTED_EPSG);
    final EPSG epsg = EPSG.getEnumBySrid(srid);

    if (epsg == null) {
      content.getWarnings().add(new AeriusException(AeriusExceptionReason.ADMS_DEFAULT_EPSG, srid));
    }
    epsgTransformer = new EPSGTransformer(epsg);
  }

  private static void readSupParameters(final AplBlockReader blockReader, final AplFileContent content) {
    final ScenarioMetaData metaData = new ScenarioMetaData();
    metaData.setCorporation(blockReader.parseString(AplFileKeys.SUP_SITE_NAME));
    metaData.setProjectName(blockReader.parseString(AplFileKeys.SUP_PROJECT_NAME));
    content.setSupCalcDryDep(blockReader.parseInt(AplFileKeys.SUP_CALC_DRY_DEP) == ACTIVE);
    content.setMetaData(metaData);
  }

  private void readMetParameters(final AplBlockReader blockReader, final AplFileContent content) {
    final ADMSOptions options = new ADMSOptions();

    options.setSurfaceAlbedo(blockReader.parseInt(MET_DS_SURFACE_ALBEDO_MODE) == ACTIVE
        ? blockReader.parseDouble(MET_DS_SURFACE_ALBEDO) : ADMSLimits.SURFACE_ALBEDO_DEFAULT);
    options.setPriestleyTaylorParameter(blockReader.parseInt(MET_DS_PRIESTLY_TAYLOR_MODE) == ACTIVE
        ? blockReader.parseDouble(MET_DS_PRIESTLY_TAYLOR) : ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_DEFAULT);
    options.setMinMoninObukhovLength(blockReader.parseInt(MET_DS_MIN_LMO_MODE) == ACTIVE
        ? blockReader.parseDouble(MET_DS_MIN_LMO) : ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT);

    final String year = String.valueOf(blockReader.parseInt(AplFileKeys.MET_SUBSET_YEAR_START));
    final MetSurfaceCharacteristics msc =
        MetSurfaceCharacteristics
            .builder()
            .roughness(blockReader.parseInt(MET_MS_ROUGHNESS_MODE) == MET_MS_PARAMETER_MODE_ENTER_VALUE
                ? blockReader.parseDouble(MET_MS_ROUGHNESS) : (double) MET_MS_ROUGHNESS.getDefaultValue())
            .surfaceAlbedo(blockReader.parseInt(MET_MS_SURFACE_ALBEDO_MODE) == MET_MS_PARAMETER_MODE_ENTER_VALUE
                ? blockReader.parseDouble(MET_MS_SURFACE_ALBEDO) : ADMSLimits.SURFACE_ALBEDO_DEFAULT)
            .priestleyTaylorParameter(blockReader.parseInt(MET_MS_PRIESTLY_TAYLOR_MODE) == MET_MS_PARAMETER_MODE_ENTER_VALUE
                ? blockReader.parseDouble(MET_MS_PRIESTLY_TAYLOR) : ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_DEFAULT)
            .minMoninObukhovLength(blockReader.parseInt(MET_MS_MIN_LMO_MODE) == MET_MS_PARAMETER_MODE_ENTER_VALUE
                ? blockReader.parseDouble(MET_MS_MIN_LMO) : ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT)
            .build();
    options.setMetYears(List.of(year));
    content.setMeteoYear(year);
    options.putMetSiteCharacteristics(year, msc);
    content.setOptions(options);
  }

  private static void setSubstances(final AplFileContent content) {
    final Set<Substance> substances = new HashSet<>();
    for (final ADMSSource<?> admsSource : content.getObjects()) {
      substances.addAll(admsSource.getSubstances());
    }
    substances.forEach(content::addSubstance);
  }
}
