/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;

/**
 * ADMSReceptor is a receptor point that is either a sub receptor point or a receptor point that should be calculated as sub receptor.
 */
public class ADMSSubReceptor extends ADMSCalculationPoint implements IsSubPoint {

  private static final long serialVersionUID = 5L;

  private AeriusPointType parentPointType;
  private int level;
  private Polygon area;

  public ADMSSubReceptor(final AeriusPoint point, final AeriusPointType pointType, final int level) {
    super(point, pointType);
    this.level = level;
  }

  @Override
  public AeriusPointType getParentPointType() {
    return parentPointType;
  }

  @Override
  public void setParent(final AeriusPoint parentPoint) {
    setParentId(parentPoint.getId());
    this.parentPointType = parentPoint.getPointType();
  }

  @Override
  public int getLevel() {
    return level;
  }

  public void setLevel(final int level) {
    this.level = level;
  }

  @Override
  public Polygon getArea() {
    return area;
  }

  @Override
  public void setArea(final Polygon Area) {
    area = Area;
  }
}
