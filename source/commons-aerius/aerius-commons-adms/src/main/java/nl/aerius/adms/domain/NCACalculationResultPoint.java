/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import java.util.EnumMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

public class NCACalculationResultPoint extends AeriusResultPoint {

  private ADMSPointNO2Data no2BackgroundData;
  private final Map<Substance, Double> depositionVelocities = new EnumMap<>(Substance.class);

  private int validMetLines;

  public NCACalculationResultPoint(final AeriusPoint point) {
    super(point);
    if (point instanceof final NCACalculationResultPoint ncaPoint) {
      this.no2BackgroundData = ncaPoint.getNO2BackgroundData();
      ncaPoint.depositionVelocities.forEach((k, v) -> this.depositionVelocities.put(k, v));
      this.validMetLines = ncaPoint.validMetLines;
      ncaPoint.getEmissionResults().entrySet().forEach(e -> setEmissionResult(e.getKey(), e.getValue()));
    }
  }

  public NCACalculationResultPoint(final int id, final Integer parentId, final AeriusPointType pointType, final double x, final double y) {
    super(id, parentId, pointType, x, y);
  }

  private static final long serialVersionUID = 1L;

  public ADMSPointNO2Data getNO2BackgroundData() {
    return no2BackgroundData;
  }

  public void setNO2BackgroundData(final ADMSPointNO2Data no2BackgroundData) {
    this.no2BackgroundData = no2BackgroundData;
  }

  public double getDepositionVelocity(final Substance substance) {
    return depositionVelocities.getOrDefault(substance, 0.0);
  }

  public void putDepositionVelocity(final Substance substance, final double depositionVelocity) {
    depositionVelocities.put(substance, depositionVelocity);
  }

  public int getValidMetLines() {
    return validMetLines;
  }
}
