/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSBuilding.BuildingShape;
import nl.aerius.adms.domain.ADMSRoadSource;
import nl.aerius.adms.domain.ADMSSource;
import nl.aerius.adms.domain.ADMSTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.characteristics.StandardTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.characteristics.TimeVaryingProfiles;
import nl.overheid.aerius.shared.domain.geo.OrientedEnvelope;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.characteristics.ADMSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.v2.characteristics.SourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.BuoyancyType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.EffluxType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.source.ADMSRoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Converter class to convert EmissionSource objects to ADMSSource objects.
 */
public class EmissionSource2ADMSConverter {

  private static final BigDecimal TO_FRACTION = BigDecimal.valueOf(0.01D);

  private final ADMSUniqueSourceNames uniqueSourceNames = new ADMSUniqueSourceNames();
  private final int srid;
  private final int year;

  private final RoadEmissionCategories roadEmissionCategories;

  public EmissionSource2ADMSConverter(final int srid, final int year, final RoadEmissionCategories roadEmissionCategories) {
    this.srid = srid;
    this.year = year;
    this.roadEmissionCategories = roadEmissionCategories;
  }

  public <G extends Geometry> List<EngineSource> convert(final EmissionSource source, final G geometry, final List<Substance> substances)
      throws AeriusException {
    final List<Geometry> geometries = ADMSGeometrySplitter.split(geometry);
    final List<EngineSource> sources = new ArrayList<>();

    for (final Geometry newGeometry : geometries) {
      sources.add(convertSingle(source, newGeometry, substances));
    }
    return sources;
  }

  /**
   * Converts an EmissionSource object to an ADMSSource object.
   *
   * @param source source to convert
   * @param geometry geometry of the source
   * @param substances substances of emissions to copy
   * @return
   * @throws AeriusException
   */
  public <G extends Geometry> ADMSSource<G> convertSingle(final EmissionSource source, final G geometry, final List<Substance> substances)
      throws AeriusException {
    final boolean roadSource = source instanceof ADMSRoadEmissionSource;
    final ADMSSourceCharacteristics chars = getADMSSourceCharacteristics(source.getCharacteristics());
    final SourceType sourceType = getSourceType(geometry, roadSource, chars.getSourceType());
    final ADMSSource<G> admsSource;

    if (roadSource) {
      admsSource = (ADMSSource<G>) createADMSRoadSource((ADMSRoadEmissionSource) source, sourceType);
    } else {
      admsSource = createADMSOther(chars, sourceType);
    }
    admsSource.setGeometry(safeGeometry(geometry));
    uniqueSourceNames.setUniqueName(admsSource, source.getLabel());
    admsSource.setHourlyDayTimeVaryingProfileName(determineHourlyTimeVaryingProfileCode(chars));
    admsSource.setMonthlyTimeVaryingProfileName(determineMonthlyTimeVaryingProfileCode(chars));
    // ADMS source sourcetype, geometry, and height must be set before calculating emissions.
    EmissionRateConverter.emissionSource2ADMSEmission(substances, source.getEmissions(), admsSource);
    return admsSource;
  }

  private static String determineHourlyTimeVaryingProfileCode(final ADMSSourceCharacteristics chars) {
    return determineTimeVaryingProfileCode(chars::getStandardHourlyTimeVaryingProfileCode, chars::getCustomHourlyTimeVaryingProfileId);
  }

  private static String determineMonthlyTimeVaryingProfileCode(final ADMSSourceCharacteristics chars) {
    return determineTimeVaryingProfileCode(chars::getStandardMonthlyTimeVaryingProfileCode, chars::getCustomMonthlyTimeVaryingProfileId);
  }

  private static String determineTimeVaryingProfileCode(final Supplier<String> codeSupplier, final Supplier<String> idSupplier) {
    final String timeVaryingProfileName;
    final String standardTimeVaryingProfileCode = codeSupplier.get();

    if (standardTimeVaryingProfileCode == null) {
      timeVaryingProfileName = idSupplier.get();
    } else if (TimeVaryingProfiles.CONTINUOUS.equals(standardTimeVaryingProfileCode)) {
      timeVaryingProfileName = null;
    } else {
      timeVaryingProfileName = standardTimeVaryingProfileCode;
    }
    return timeVaryingProfileName;
  }

  private static ADMSSourceCharacteristics getADMSSourceCharacteristics(final SourceCharacteristics characteristics) {
    return characteristics instanceof final ADMSSourceCharacteristics admssc ? admssc : new ADMSSourceCharacteristics();
  }

  private static <G extends Geometry> SourceType getSourceType(final G geometry, final boolean roadSource, final SourceType sourceType) {
    final SourceType type;
    if (geometry instanceof Point) {
      type = sourceType == null || sourceType != SourceType.JET ? SourceType.POINT : sourceType;
    } else if (geometry instanceof LineString) {
      type = roadSource ? SourceType.ROAD : SourceType.LINE;
    } else if (geometry instanceof Polygon) {
      type = sourceType == null || sourceType != SourceType.VOLUME ? SourceType.AREA : SourceType.VOLUME;
    } else {
      throw new IllegalArgumentException("Unexpected geometry: " + geometry);
    }
    return type;
  }

  private ADMSRoadSource createADMSRoadSource(final ADMSRoadEmissionSource source, final SourceType sourceType) {
    final ADMSRoadSource roadSource = new ADMSRoadSource();

    roadSource.setSourceType(sourceType);
    roadSource.setL1(source.getWidth());
    roadSource.setHeight(source.getElevation());
    roadSource.setTraYear(year);
    setTraRoadType(roadSource, source.getRoadAreaCode());
    roadSource.setTraGradient(source.getGradient());
    roadSource.setLeftRoadSideBarrier(source.getBarrierLeft());
    roadSource.setRightRoadSideBarrier(source.getBarrierRight());
    roadSource.setFractionCovered(BigDecimal.valueOf(source.getCoverage()).multiply(TO_FRACTION).doubleValue());
    return roadSource;
  }

  private void setTraRoadType(final ADMSRoadSource roadSource, final String roadAreaCode) {
    final SimpleCategory area = roadEmissionCategories.getArea(roadAreaCode);

    if (area != null) {
      roadSource.setTraRoadType(area.getDescription());
    }
  }

  private static <G extends Geometry> ADMSSource<G> createADMSOther(final ADMSSourceCharacteristics chars, final SourceType sourceType) {
    final ADMSSource<G> admsSource = new ADMSSource<>();
    admsSource.setSourceType(sourceType);

    setCharacteristics(admsSource, chars);
    admsSource.setHeight(chars.getHeight());
    admsSource.setL1(getL1(chars));
    return admsSource;
  }

  private static <G extends Geometry> void setCharacteristics(final ADMSSource<G> es, final ADMSSourceCharacteristics sc) {
    es.setMainBuilding(sc.getBuildingId());
    // Set default to get sane default values as long as ADMS characteristics are not fully supported
    // throughout the rest of the application.
    setDiameter(es, sc);
    es.setBuoyancyType((sc.getBuoyancyType() == null ? BuoyancyType.TEMPERATURE : sc.getBuoyancyType()).type());
    es.setTemperature(sc.getTemperature());
    es.setDensity(sc.getDensity());
    es.setEffluxType((sc.getEffluxType() == null ? EffluxType.VELOCITY : sc.getEffluxType()).type());
    es.setVerticalVelocity(sc.getVerticalVelocity());
    es.setVolumetricFlowRate(sc.getVolumetricFlowRate());
    es.setSpecificHeatCapacity(sc.getSpecificHeatCapacity());
    es.setPercentNOxAsNO2(sc.getPercentNOxAsNO2());
    es.setMomentumFlux(sc.getMomentumFlux());
    es.setBuoyancyFlux(sc.getBuoyancyFlux());
    es.setMassFlux(sc.getMassFlux());
    es.setAngle1(sc.getElevationAngle());
    es.setAngle2(sc.getHorizontalAngle());
  }

  private static <G extends Geometry> void setDiameter(final ADMSSource<G> es, final ADMSSourceCharacteristics sc) {
    double diameter;
    // if other than point/jet this value is not used so we just pass what we got.
    final boolean pointJet = sc.getSourceType() == SourceType.POINT || sc.getSourceType() == SourceType.JET;

    if (pointJet && sc.getDiameter() < ADMSLimits.SOURCE_DIAMETER_MINIMUM) {
      // For Point/Jet sources the diameter is relevant, and should not be 0.0. Otherwise ADMS will crash.
      // Therefore if given diameter is smaller than the minimum set to to minimum value.
      diameter = ADMSLimits.SOURCE_DIAMETER_MINIMUM;
    } else {
      diameter = sc.getDiameter();
    }
    es.setDiameter(diameter);
  }

  private static double getL1(final ADMSSourceCharacteristics sc) {
    final SourceType ct = sc.getSourceType();
    final double l1;

    if (ct == SourceType.LINE || ct == SourceType.ROAD) {
      l1 = sc.getWidth();
    } else if (ct == SourceType.VOLUME) {
      l1 = sc.getVerticalDimension();
    } else {
      l1 = 0.0;
    }
    return l1;
  }

  private <G extends Geometry> G safeGeometry(final G geometry) throws AeriusException {
    if (geometry instanceof final Polygon polygon) {
      return (G) GeometryUtil.toConvexHull(polygon, srid);
    } else {
      return geometry;
    }
  }

  /**
   * Converts BuidingFeature to {@link ADMSBuilding}.
   *
   * @param feature Building feature
   * @return ADMSBuilding
   * @throws AeriusException
   */
  public static ADMSBuilding convert(final BuildingFeature feature) throws AeriusException {
    final ADMSBuilding building = new ADMSBuilding();
    final Building properties = feature.getProperties();

    building.setName(properties.getGmlId());
    final Geometry geometry = feature.getGeometry();
    if (properties.isCircle()) {
      building.setBuildingCentre((Point) geometry);
      building.setShape(BuildingShape.CIRCULAR);
      building.setBuildingLength(properties.getDiameter());
      building.setBuildingWidth(properties.getDiameter());
      building.setBuildingOrientation(0);
    } else {
      final OrientedEnvelope envelope = GeometryUtil.determineOrientedEnvelope((Polygon) geometry);

      building.setBuildingCentre(GeometryUtil.determineCenter(geometry));
      building.setShape(BuildingShape.RECTANGULAR);
      building.setBuildingLength(envelope.getLength());
      building.setBuildingWidth(envelope.getWidth());
      building.setBuildingOrientation(envelope.getOrientation());
    }
    building.setBuildingHeight(properties.getHeight());
    return building;
  }

  /**
   * Converts {@link CustomTimeVaryingProfile} to {@link ADMSTimeVaryingProfile} objects.
   *
   * @param timeVaryingProfiles list of {@link CustomTimeVaryingProfile}
   * @return Collection of {@link ADMSTimeVaryingProfile}
   */
  public static Collection<ADMSTimeVaryingProfile> convertCustomTimeVaryingProfiles(
      final Collection<CustomTimeVaryingProfile> timeVaryingProfiles) {
    return timeVaryingProfiles.stream().map(EmissionSource2ADMSConverter::convert).toList();
  }

  private static ADMSTimeVaryingProfile convert(final CustomTimeVaryingProfile dv) {
    final ADMSTimeVaryingProfile tvp = new ADMSTimeVaryingProfile();

    tvp.setName(dv.getGmlId());
    tvp.setType(dv.getType());
    tvp.setValues(dv.getValues());
    return tvp;
  }

  /**
   * Converts {@link StandardTimeVaryingProfile} to {@link ADMSTimeVaryingProfile} objects.
   *
   * @param timeVaryingProfiles list of {@link StandardTimeVaryingProfile}
   * @return Collection of {@link ADMSTimeVaryingProfile}
   */
  public static Collection<ADMSTimeVaryingProfile> convertStandardTimeVaryingProfiles(
      final Collection<StandardTimeVaryingProfile> timeVaryingProfiles) {
    return timeVaryingProfiles.stream().map(EmissionSource2ADMSConverter::convert).toList();
  }

  private static ADMSTimeVaryingProfile convert(final StandardTimeVaryingProfile dv) {
    final ADMSTimeVaryingProfile tvp = new ADMSTimeVaryingProfile();

    tvp.setName(dv.getCode());
    tvp.setType(dv.getType());
    tvp.setValues(dv.getValues());
    return tvp;
  }
}
