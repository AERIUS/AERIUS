/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

class AplFileConstants {

  public interface KeyWithDefault {
    String getKey();

    Object getValueOrDefault(Object value);

    Object getDefaultValue();
  }
  public static final String NOT_ACTIVE = "0";
  public static final String ACTIVE = "1";
  public static final String SUP_SITE_NAME_KEY = "{SupSiteName}";
  public static final String SUP_PROJECT_NAME_KEY = "{SupProjectName}";
  public static final String SUP_USE_ADD_INPUT_KEY = "{SupUseAddInput}";
  public static final String SUP_ADD_INPUT_PATH_KEY = "{SupAddInputPath}";
  public static final String SUP_MODEL_BUILDINGS = "{SupModelBuildings}";
  public static final String SUP_MODEL_COMPLEX_TERRAIN = "{SupModelComplexTerrain}";
  public static final String SUP_USE_TIME_VARYING_EMISSIONS = "{SupUseTimeVaryingEmissions}";
  public static final String SUP_TIME_VARYING_EMISSIONS_TYPE = "{SupTimeVaryingEmissionsType}";
  public static final String SUP_USE_TIME_VARYING_FAC = "{SupUseTimeVaryingFAC}";
  public static final String SUP_TIME_VARYING_FAC_PATH_KEY = "{SupTimeVaryingFACPath}";
  public static final String SUP_CALC_DRY_DEP = "{SupCalcDryDep}";
  public static final String MET_DATA_FILE_WELL_FORMED_PATH_KEY = "{MetDataFileWellFormedPath}";
  public static final String MET_DS_MS_PARAMETERS = "{Met_DS_MS_Parameters}";
  public static final String MET_DATA_IS_SEQUENTIAL = "{MetDataIsSequential}";
  public static final String MET_SUBSET_YEAR_START = "{MetSubsetYearStart}";
  public static final String MET_SUBSET_YEAR_END = "{MetSubsetYearEnd}";
  public static final String MET_LATITUDE = "{MetLatitude}";
  public static final String MET_WIND_IN_SECTORS = "{MetWindInSectors}";
  public static final String HIL_GRID_SIZE = "{HilGridSize}";
  public static final int DEFAULT_HIL_GRID_SIZE = 2;
  public static final String HIL_USE_TER_FILE = "{HilUseTerFile}";
  public static final String HIL_TERRAIN_PATH_KEY = "{HilTerrainPath}";
  public static final String HIL_USE_ROUGH_FILE = "{HilUseRoughFile}";
  public static final String HIL_ROUGH_PATH_KEY = "{HilRoughPath}";
  public static final String GRD_PTS_POINTS_FILE_PATH_KEY = "{GrdPtsPointsFilePath}";
  public static final String NO_BUILDING = "(None)";

  // PARAMETERS_ETC
  public static final String SRC_NUM_SOURCES = "{SrcNumSources}";
  public static final String POL_NUM_POLLUTANTS = "{PolNumPollutants}";
  public static final int POL_NUM_POLLUTANTS_DEFAULT = 9;
  public static final int MET_MS_PARAMETER_MODE_ENTER_VALUE = 1;

  // APL Header constants
  public static final String ADMS_COORDINATESYSTEM = "ADMS_COORDINATESYSTEM";
  public static final String ADMS_PARAMETERS_MET = "ADMS_PARAMETERS_MET";
  public static final String ADMS_PARAMETERS_SUP = "ADMS_PARAMETERS_SUP";
  public static final String ADMS_PARAMETERS_BLD = "ADMS_PARAMETERS_BLD";
  public static final String ADMS_SOURCE_DETAILS = "ADMS_SOURCE_DETAILS";
  public static final String ADMS_SOURCE_VERTEX = "ADMS_SOURCE_VERTEX";
  public static final String ADMS_POLLUTANT_DETAILS = "ADMS_POLLUTANT_DETAILS";
  public static final String ADMS_PARAMETERS_BLD_KEY = "{ADMS_PARAMETERS_BLD}";
  public static final String ADMS_PARAMETERS_OPT_KEY = "{ADMS_PARAMETERS_OPT}";
  public static final String ADMS_POLLUTANT_DETAILS_KEY = "{ADMS_POLLUTANT_DETAILS}";

}
