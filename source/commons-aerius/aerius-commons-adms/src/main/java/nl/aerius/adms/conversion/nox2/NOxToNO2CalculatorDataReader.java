/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion.nox2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureSource;

import nl.aerius.adms.version.NOxToNO2Version;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Utll class to read all NOx to NO2 calculator data files into a data object.
 */
public class NOxToNO2CalculatorDataReader {

  /**
   * Read the data for the given version.
   *
   * @param version version to read
   * @param dataRootDirectory directory to read the data from
   * @return data object
   * @throws IOException
   */
  public static NOxToNO2CalculatorData loadData(final NOxToNO2Version version, final File dataRootDirectory) throws IOException {
    final NOxToNO2CalculatorData backgroundData = new NOxToNO2CalculatorData(version, readLACodesShapefile(version, dataRootDirectory),
        readfNO2LookupShapefile(version, dataRootDirectory));

    readLABackgroundFile(version, Substance.NOX, dataRootDirectory, backgroundData);
    readLABackgroundFile(version, Substance.NO2, dataRootDirectory, backgroundData);
    readLABackgroundFile(version, Substance.O3, dataRootDirectory, backgroundData);
    readFNO2File(version, dataRootDirectory, backgroundData);
    return backgroundData;
  }

  /**
   * Reads the local authoritiy geometry to codes shape file.
   *
   * @param version version to read
   * @param dataRootDirectory directory to read the data from
   * @return FeatureSource with the shape data
   * @throws IOException
   */
  static SimpleFeatureSource readLACodesShapefile(final NOxToNO2Version version, final File dataRootDirectory) throws IOException {
    return readShapeFile(version.getUweShapeFilename(), dataRootDirectory, "LA Codes shape file");
  }

  /**
   * Reads the local authoritiy background data for a specific version.
   *
   * @param version version to read
   * @param substance substance to data is for
   * @param dataRootDirectory directory to read the data from
   * @param backgroundData background data objec to store the data in
   * @throws IOException
   */
  static void readLABackgroundFile(final NOxToNO2Version version, final Substance substance, final File dataRootDirectory,
      final NOxToNO2CalculatorData backgroundData) throws IOException {
    final LABackgroundReader reader = new LABackgroundReader(substance, backgroundData);
    final File laCodesBackgroundFile = new File(dataRootDirectory, version.getLaCodesFilename(substance));

    try (InputStream inputStream = new FileInputStream(laCodesBackgroundFile)) {
      reader.readObjects(inputStream);
    }
  }

  /**
   * Reads the fNO2 factor lookup area geometry shape file.
   *
   * @param version version to read
   * @param dataRootDirectory directory to read the data from
   * @return FeatureSource with the shape data
   * @throws IOException
   */
  static SimpleFeatureSource readfNO2LookupShapefile(final NOxToNO2Version version, final File dataRootDirectory) throws IOException {
    return readShapeFile(version.getfNO2ShapeFilename(), dataRootDirectory, "fNO2 area shape file");
  }

  /**
   * Reads the fNO2 csv file
   *
   * @param version version to read
   * @param dataRootDirectory directory to read the data from
   * @param backgroundData background data objec to store the data in
   * @throws IOException
   */
  static void readFNO2File(final NOxToNO2Version version, final File dataRootDirectory, final NOxToNO2CalculatorData backgroundData)
      throws IOException {
    final FractionNO2Reader reader = new FractionNO2Reader(backgroundData);
    final File fNO2FractionFile = new File(dataRootDirectory, version.getfNO2FactorsFilename());

    try (InputStream inputStream = new FileInputStream(fNO2FractionFile)) {
      reader.readObjects(inputStream);
    }
  }

  private static SimpleFeatureSource readShapeFile(final String filename, final File dataRootDirectory, final String type) throws IOException {
    final File shapeFile = new File(dataRootDirectory, filename);
    final FileDataStore store = FileDataStoreFinder.getDataStore(shapeFile);
    final SimpleFeatureSource source = store.getFeatureSource();

    if (source == null) {
      throw new IllegalArgumentException("Could not process " + type + " file:" + shapeFile);
    }
    return source;
  }

}
