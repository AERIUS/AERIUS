# ADMS

## Source Characteristics

This table describes the supported ADMS source characteristics.
Per source type the table describes if the parameter is supported or not.
Below the table each parameter is described in detail.
An empty colum indicates the parameter is supported for the specific source type.

| Parameter            | Point | Area    | Volume  | Line | Jet   | Road |
|----------------------|-------|---------|---------|------|-------|------|
| geometry             | Point | Polygon | Polygon | Line | Point | Line |
| height               |       |         |         |      |       |      |
| diameter             |       | n/a     | n/a     | n/a  |       | n/a  |
| mainBuilding         | tbd   | n/a     | n/a     | n/a  | n/a   | n/a  |
| buoyancyType         |       |         |         |      |       |      |
| temperature          |       |         |         |      |       |      |
| density              |       |         |         |      |       |      |
| specificHeatCapacity |       |         |         |      |       |      |
| effluxType           |       |         |         |      |       |      |
| verticalVelocity     |       |         |         |      |       |      |
| volumetricFlowRate   |       |         |         |      | n/a   |      |
| molWeight            |       |         |         |      |       |      |
| releaseAtNTP         | tbd   | tbd     | n/a     | tbd  | tbd   | tbd  |
| verticalDimension    | tbd   | tbd     |         | tbd  | tbd   | tbd  |

### height

Height of source (m) above ground. For a volume source, it is the mid-height of the volume above the ground.

###  diameter

Internal diameter of point source (m)

### sourceType

Source type specified as: 0=point, 1=area, 2=volume, 3=line, 4=road, 5=?, 6=jet

### buoyancyType

Buoyancy specified as: 0=temperature (user can enter temperature), 1=density (user can enter density), 2=ambient (temperature and density set to default)

### temperature

Exit temperature of the source (oC), used if SrcBuoyancyType=0 otherwise leave as default

### density

Density of source, used if SrcBuoyancyType=1, otherwise leave as default (1.225 kg/m3 for air)

### specificHeatCapacity 

Default = 1012 J/kg/oC (typical value for air)

### effluxType

Efflux specified as:
| Code | Name                                | Available                                                      |
|------|-------------------------------------|----------------------------------------------------------------|
| 0    | velocity                            | Available for all source types                                 |
| 1    | volume flow rate                    | Available for point, line, area sources                        |
| 2    | momentum flux and heat release rate | for example for fires; available for point, line, area sources |
| 3    | mass flow rate                      | Available for point, line, area sources                        |

### verticalVelocity

Exit vertical velocity of source, used if SrcEffluxType=0, otherwise leave as default.
Units are m/s if SrcReleaseAtNTP=0 and Nm/s if SrcReleaseAtNTP=1 (where N means normal pressure and temperature).
Default to 0 but can be edited for area and line sources. Default to 0 and cannot be edited for volume sources.

### volumetricFlowRate

Volume flow rate for source, used if SrcEffluxType=1, otherwise leave as default.
Units are m3/s if SrcReleaseAtNTP=0 and Nm3/s if SrcReleaseAtNTP=1 (where N means normal temperature and pressure).
Default to 0 but can be edited for area and line sources. 


### molWeight

Overall molecular weight of material leaving source, should be fine to leave as default (28.966 g, i.e. air)

### releaseAtNTP

### verticalDimension

Vertical dimension of a volume source (m). Stored in the L1 parameter in an ADMS APL/UPL file.

## Binaries and Data files

To do an ADMS calculation for some settings the data is provided by the worker.
Some data needs to be written to additional ADMS input files.
This is geometric data for which the worker contains data covering the whole of the UK.
The following data files are required:

### Surface Roughness

This is surface roughness data covering the whole of the UK.
This data is stored in a GeoTiff file.
