/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.chrome;

import java.io.File;
import java.util.Locale;

import nl.overheid.aerius.worker.DatabaseConfiguration;

public class ChromeConfiguration extends DatabaseConfiguration {

  public enum Mode {
    /**
     * Use Karate library to generate PDF via chrome service.
     */
    KARATE,
    /**
     * Generate PDF using the chrome executable with headless option.
     */
    HEADLESS;

    public static Mode safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ROOT));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  /**
   * Mode to use to generate PDF.
   */
  private Mode mode;
  /**
   * Directory where Chrome executable is present.
   */
  private File chromeRootDir;
  /**
   * Use new headless version.
   */
  private boolean newHeadless;
  /**
   * Timeout in milliseconds.
   */
  private int timeout;

  public ChromeConfiguration(final int processes) {
    super(processes);
  }

  public Mode getMode() {
    return mode;
  }

  void setMode(final Mode mode) {
    this.mode = mode;
  }

  public File getRootDir() {
    return chromeRootDir;
  }

  void setRootDir(final File chromeRootDir) {
    this.chromeRootDir = chromeRootDir;
  }

  public int getTimeout() {
    return timeout;
  }

  void setTimeout(final int timeout) {
    this.timeout = timeout;
  }

  public boolean isNewHeadless() {
    return newHeadless;
  }

  void setNewHeadless(final boolean newHeadless) {
    this.newHeadless = newHeadless;
  }
}
