/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.chrome;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.exec.ExecuteParameters;
import nl.overheid.aerius.exec.ExecuteWrapper;
import nl.overheid.aerius.exec.ExecuteWrapper.StreamGobblers;
import nl.overheid.aerius.export.util.PdfContentGenerator;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Runner to call the Chrome executable headless.
 */
public class ChromeRunner implements PdfContentGenerator {

  private static final String EXECUTABLE_NAME = OSUtils.isWindows() ? "chrome.exe" : "google-chrome";

  // https://bugs.chromium.org/p/chromium/issues/detail?id=737678
  private static final String DISABLE_GPU = "--disable-gpu";
  private static final String EXPORT_TAGGED_PDF = "--export-tagged-pdf";
  private static final String HEADLESS = "--headless=old";
  private static final String HEADLESS_NEW = "--headless=new";
  private static final String NO_HEADER = "--no-pdf-header-footer";
  private static final String NO_HEADER_OLD = "--print-to-pdf-no-header";
  private static final String NO_MARGINS = "--no-margins";
  private static final String PRINT_TO_PDF = "--print-to-pdf=";
  private static final String RUN_ALL_COMPOSITOR_STAGES_BEFORE_DRAW = "--run-all-compositor-stages-before-draw";
  private static final String VIRTUAL_TIME_BUDGET = "--virtual-time-budget=";

  private final ChromeConfiguration configuration;
  /**
   * Set runtime for testing purposes.
   */
  private Runtime runtime;

  public ChromeRunner(final ChromeConfiguration configuration) {
    this.configuration = configuration;
  }

  void setRuntime(final Runtime runtime) {
    this.runtime = runtime;
  }

  /**
   * Returns true if the Chrome executable binary is executable.
   *
   * @param chromeDir Chrome executable directory
   * @return true if executable
   */
  public boolean isExecutable(final Path chromeDir) {
    return Files.isExecutable(chromeDir.resolve(EXECUTABLE_NAME));
  }

  @Override
  public void generate(final String runId, final File pdfFileName, final String url) throws IOException, AeriusException {
    // construct the execute worker.
    final StreamGobblers gobblers = new ChromeStreamGobblers();
    final ExecuteWrapper executeWrapper = createWrapper(createParameters(pdfFileName, url), gobblers);

    if (runtime != null) { // set test runtime if present
      executeWrapper.setRuntime(runtime);
    }
    // perform the actual chrome run.
    try {
      executeWrapper.run(runId, pdfFileName.getParentFile());
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

  private ExecuteWrapper createWrapper(final List<String> parameters, final StreamGobblers streamGobblers) {
    final ExecuteParameters executeParameters = new ExecuteParameters(EXECUTABLE_NAME, parameters.toArray(new String[parameters.size()]));

    return new ExecuteWrapper(executeParameters, configuration.getRootDir().getAbsolutePath(), streamGobblers);
  }

  private List<String> createParameters(final File outputFilename, final String url) {
    final List<String> parameters = new ArrayList<>();

    parameters.add(configuration.isNewHeadless() ? HEADLESS_NEW : HEADLESS);
    parameters.add(DISABLE_GPU);
    parameters.add(EXPORT_TAGGED_PDF);
    parameters.add(NO_MARGINS);
    parameters.add(NO_HEADER);
    parameters.add(NO_HEADER_OLD);
    parameters.add(RUN_ALL_COMPOSITOR_STAGES_BEFORE_DRAW);
    parameters.add(VIRTUAL_TIME_BUDGET + configuration.getTimeout());
    parameters.add(PRINT_TO_PDF + outputFilename.getAbsolutePath());
    parameters.add(url);

    return parameters;
  }
}
