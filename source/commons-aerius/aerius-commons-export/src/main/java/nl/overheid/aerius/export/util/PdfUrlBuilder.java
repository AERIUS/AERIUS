/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.util;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Locale;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Builder to construct the url to the web server to render the PDF HTML page.
 */
public class PdfUrlBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(PdfUrlBuilder.class);

  private final URIBuilder uriBuilder;

  public PdfUrlBuilder(final String webserverUrl) throws AeriusException {
    try {
      uriBuilder = new URIBuilder(webserverUrl);
      uriBuilder.setPathSegments(List.of("print"));
      uriBuilder.addParameter("internal", "true");
    } catch (final URISyntaxException ignored) {
      LOG.error("Unable to build a URI from preview url '{}'", webserverUrl);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  public PdfUrlBuilder setLocale(final Locale locale) {
    uriBuilder.addParameter("locale", locale.getLanguage());
    return this;
  }

  public PdfUrlBuilder setJobKey(final String jobKey) {
    uriBuilder.addParameter("jobKey", jobKey);
    return this;
  }

  public PdfUrlBuilder setPdfProductType(final PdfProductType pdfProductType) {
    uriBuilder.addParameter("pdfProductType", pdfProductType.name());
    return this;
  }

  public PdfUrlBuilder setProposedSituationId(final String proposedSituationId) {
    uriBuilder.addParameter("proposedSituationId", proposedSituationId);
    return this;
  }

  public PdfUrlBuilder setReference(final String reference) {
    uriBuilder.addParameter("reference", reference);
    return this;
  }

  public String build() throws AeriusException {
    try {
      return uriBuilder.build().toString();
    } catch (final URISyntaxException e) {
      LOG.error("Unable to build a URI from preview url '{}'", uriBuilder);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }
}
