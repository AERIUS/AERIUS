/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export;

import nl.overheid.aerius.export.util.PdfContentProcessor;
import nl.overheid.aerius.util.FileProcessor;

/**
 * Interface to attach zip file to a PDF file. The implementation of this class is required by {@link PdfContentProcessor}.
 * It is called when the zip file needs to be attached to the PDF. The implementation should generate the zip file,
 * and pass the information about the zip file to the given {@link FileProcessor}.
 */
public interface PdfAttachZipFileProcessor {

  /**
   * Generate the zip file and pass the zip file information to the given file processor.
   *
   * @param fileProcessor file processor to handle the zip files
   */
  void attachFile(FileProcessor fileProcessor) throws Exception;
}
