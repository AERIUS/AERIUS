/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.util;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;

import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;

/**
 * Interface for Theme specific implementations.
 */
public interface PdfThemeContentDelegate {

  default void drawLogoOnPage(final Document document, final Rectangle pageSize, final Image image, final int pageNo) {
    // No-op
  }

  default void processFrontPage(final Document document, final Rectangle pageSize, final Image logo, final PdfFont pdfFont,
      final PdfTitles pdfTitles, final String reference) {
    // No-op
  }

  DateTimeFormatter getDateTimeFormatter();

  PageSize getPageSize(PdfDocument pdfDocument);

  String getFont();

  BigDecimal getLogoTop();

  BigDecimal getLogoDimension();

  BigDecimal getLogoLeft();

  BigDecimal getMarginLeft();

  BigDecimal getMarginRight();

  BigDecimal getMarginVer();

  BigDecimal getTitleTop();

  BigDecimal getTitleTopSecondRow();

  BigDecimal getPageTitleTop();

  BigDecimal getPageTitleTopSecondRow();

  BigDecimal getFontSizeFrontPageTitle();

  BigDecimal getFontSizeFrontPageSubTitle();

  BigDecimal getFontSizePageTitle();

  BigDecimal getFontSizePageSubTitle();

  BigDecimal getFontSizeReleaseType();

  BigDecimal getFontSizeFooter();
}
