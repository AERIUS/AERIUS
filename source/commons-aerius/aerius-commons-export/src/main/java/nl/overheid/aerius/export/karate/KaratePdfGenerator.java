/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.karate;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intuit.karate.KarateException;

import nl.aerius.pdf.TimeoutException;
import nl.aerius.print.ExportJob;
import nl.overheid.aerius.export.util.PdfContentGenerator;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.worker.RequeueTaskException;

/**
 * Generate a PDF using the Karate library to connect to Chrome.
 *
 * @deprecated When generating PDF via local Chrome executable is sufficient proved generating using Karate library will be removed.
 */
@Deprecated(forRemoval = true)
public class KaratePdfGenerator implements PdfContentGenerator {

  private static final Logger LOG = LoggerFactory.getLogger(KaratePdfGenerator.class);

  protected static final int RETRY_COUNT = 21;

  private static final double A4_WIDTH_INCHES = 8.3;
  private static final double A4_HEIGHT_INCHES = 11.7;

  // large payload size, since pdf contents are transferred in a single websocket frame as string.
  // The current size is about 2 * (length of string containing base64 encoded pdf with ~1000 sources)
  private static final int MAX_PAYLOAD_SIZE = 10_000_000;
  // Arbitrarily large webdriver command timeout, since pdf content rendering and retrieval can take much longer than the default 15 seconds.
  private static final int WEBDRIVER_COMMAND_TIMEOUT = 5 * 60 * 1000;

  @Override
  public void generate(final String runId, final File pdfFileName, final String url) throws IOException, AeriusException, RequeueTaskException {
    try {
      ExportJob
          .create(url)
          .handle(FileUtil.getFileWithoutExtension(pdfFileName))
          .completeOrFailViaIndicator()
          .retry(RETRY_COUNT)
          .destination(pdfFileName.getParentFile().getAbsolutePath() + File.separator)
          .driverOptions(getDriverOptions())
          .print(getPrintParams())
          .save()
          .outputDocument();
    } catch (final TimeoutException e) {
      LOG.trace("Timeout exception while generating PDF", e);
      throw new RequeueTaskException("Timeout exception while generating PDF");
    } catch (final KarateException e) {
      throw unpackKarateException(e);
    } catch (final RuntimeException e) {
      if (e.getClass() == RuntimeException.class && e.getCause() instanceof RuntimeException) {
        // Rethrow karate runtime exception that indicates a problem as RequeueTaskException.
        // RequeueTaskException will put the task back on the queue, and cause the task to be retried (until chrome is restarted again).
        final String causeMessage = e.getCause().getMessage();
        if (e.getCause() instanceof TimeoutException || (causeMessage != null && causeMessage.contains("waitUntil (js)"))) {
          // Message indicates problem with chrome instance, probably recoverable (later, if chrome is restarted).
          throw new RequeueTaskException(causeMessage);
        }
      }
      throw e;
    }
  }

  private static Map<String, Object> getPrintParams() {
    final Map<String, Object> printParams = new HashMap<>();
    printParams.put("paperWidth", A4_WIDTH_INCHES);
    printParams.put("paperHeight", A4_HEIGHT_INCHES);
    printParams.put("preferCSSPageSize", true);
    printParams.put("printBackground", true);
    printParams.put("margins", "default");

    return printParams;
  }

  /*
   * Unpack a karate exception:
   * If cause was a IOException, throw that one
   * If cause was a runtime exception, return that one
   * If cause was a karate exception, recursively try to get the correct exception
   * Any other case (no cause or not a runtime/io-exception), return the supplied exception.
   */
  private static RuntimeException unpackKarateException(final KarateException e) throws IOException {
    if (e.getCause() instanceof IOException) {
      throw (IOException) e.getCause();
    } else if (e.getCause() instanceof KarateException) {
      return unpackKarateException((KarateException) e.getCause());
    } else if (e.getCause() instanceof RuntimeException) {
      return (RuntimeException) e.getCause();
    } else {
      return e;
    }
  }

  private static Map<String, Object> getDriverOptions() {
    final Map<String, Object> opt = new HashMap<>();
    opt.put("timeout", WEBDRIVER_COMMAND_TIMEOUT);
    opt.put("maxPayloadSize", MAX_PAYLOAD_SIZE);
    return opt;
  }
}
