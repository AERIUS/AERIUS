/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.util;

import nl.overheid.aerius.export.util.impl.NcaPdfContentDelegate;
import nl.overheid.aerius.export.util.impl.Own2000PdfContentDelegate;

public class PdfContentProcessorFactory {

  public PdfThemeContentDelegate create(final PdfTask pdfTask) {
    return switch (pdfTask.pdfProductType()) {
      case NCA_CALCULATOR ->
          new NcaPdfContentDelegate();
      case OWN2000_CALCULATOR, OWN2000_EDGE_EFFECT_APPENDIX, OWN2000_EXTRA_ASSESSMENT_APPENDIX, OWN2000_LBV_POLICY, OWN2000_PERMIT, OWN2000_SINGLE_SCENARIO ->
          new Own2000PdfContentDelegate();
    };
  }
}
