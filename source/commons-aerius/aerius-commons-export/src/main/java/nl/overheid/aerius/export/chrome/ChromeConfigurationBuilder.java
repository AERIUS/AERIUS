/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.chrome;

import java.io.File;
import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.export.chrome.ChromeConfiguration.Mode;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.ConfigurationValidator;

/**
 * Builder to read configuration settings to run Chome executable.
 */
public class ChromeConfigurationBuilder<T extends ChromeConfiguration> extends ConfigurationBuilder<T> {

  private static final String ROOT = "root";
  private static final String CHROME_MODE = "mode";
  private static final String CHROME_PREFIX = "chrome";
  private static final String CHROME_TIMEOUT = "timeout";
  private static final String CHROME_NEW_HEADLESS = "newheadless";

  // Arbitrarily large webdriver command timeout, since pdf content rendering and retrieval can take much longer than the default 15 seconds.
  private static final int DEFAULT_TIMEOUT_MILLISECONDS = 5 * 60 * 1000;

  public ChromeConfigurationBuilder(final Properties properties) {
    super(properties, CHROME_PREFIX);
  }

  @Override
  protected T create() {
    throw new IllegalStateException("ChromeConfigurationBuilder should not be used as separated configuration.");
  }

  @Override
  protected void build(final T configuration) {
    configuration.setMode(Mode.safeValueOf(workerProperties.getPropertyOrDefault(CHROME_MODE, Mode.KARATE.name())));
    final String root = workerProperties.getProperty(ROOT);

    if (root != null) {
      configuration.setRootDir(new File(root));
    }
    configuration.setTimeout(workerProperties.getPropertyOrDefault(CHROME_TIMEOUT, DEFAULT_TIMEOUT_MILLISECONDS));
    configuration.setNewHeadless(workerProperties.getPropertyBooleanSafe(CHROME_NEW_HEADLESS, true));
  }

  @Override
  protected List<String> validate(final T configuration) {
    final ConfigurationValidator validator = new ConfigurationValidator();

    if (configuration.getMode() == Mode.HEADLESS) {
      validator.validateDirectory(workerProperties.getFullPropertyName(ROOT), configuration.getRootDir(), false);
    }
    return validator.getValidationErrors();
  }
}
