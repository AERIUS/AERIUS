/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.util;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Locale;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.EncryptionConstants;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfDocumentInfo;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfVersion;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.WriterProperties;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.itextpdf.svg.converter.SvgConverter;

import nl.overheid.aerius.export.PdfAttachZipFileProcessor;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.util.FileUtil;

public class PdfContentProcessor {

  private static final float DEGREES_30_IN_RADIANS = 0.5235F;

  private final PdfThemeContentDelegate pdfContentDelegate;
  private final PdfAttachZipFileProcessor pdfAttachZipFileProcessor;
  private final PdfFont pdfFont;
  private final Paragraph pageTitle;
  private final Paragraph pageSubTitle;
  private final Paragraph referenceAndDate;
  private final PdfProductType pdfProductType;
  private final ReleaseType releaseType;
  private final String reference;
  private final PdfTitles pdfTitles;
  private final String customer;
  private final Locale locale;

  public PdfContentProcessor(final PdfThemeContentDelegate pdfContentDelegate, final PdfAttachZipFileProcessor pdfAttachZipFileProcessor,
      final String reference, final PdfTask pdfTask, final Locale locale, final PdfTitles pdfTitles, final Clock clock) throws IOException {
    this.pdfContentDelegate = pdfContentDelegate;
    this.pdfAttachZipFileProcessor = pdfAttachZipFileProcessor;
    this.locale = locale;
    this.pdfProductType = pdfTask.pdfProductType();
    this.releaseType = pdfTask.releaseType();
    this.customer = pdfTask.customer().name();
    this.reference = reference;
    this.pdfTitles = pdfTitles;

    try (final InputStream fontInputStream = PdfContentProcessor.class.getResourceAsStream(pdfContentDelegate.getFont())) {
      final byte[] fontBytes = fontInputStream.readAllBytes();
      pdfFont = PdfFontFactory.createFont(fontBytes, PdfEncodings.IDENTITY_H, PdfFontFactory.EmbeddingStrategy.PREFER_EMBEDDED);
    }

    pageTitle = new Paragraph(pdfTitles.getPageTitle())
        .setFont(pdfFont)
        .setFontColor(pdfTitles.getColorTitle())
        .setFontSize(pdfContentDelegate.getFontSizePageTitle().floatValue());

    if (!pdfTitles.getPageSubTitle().isBlank()) {
      pageSubTitle = new Paragraph(pdfTitles.getPageSubTitle())
          .setFont(pdfFont)
          .setFontSize(pdfContentDelegate.getFontSizePageSubTitle().floatValue());
    } else {
      pageSubTitle = null;
    }

    final LocalDateTime now = LocalDateTime.now(clock);
    referenceAndDate = new Paragraph(String.format("%s (%s)", reference, pdfContentDelegate.getDateTimeFormatter().format(now)))
        .setFont(pdfFont)
        .setFontSize(pdfContentDelegate.getFontSizeFooter().floatValue());
  }

  public PdfThemeContentDelegate getPdfContentAdaptor() {
    return pdfContentDelegate;
  }

  public void process(final File file) throws Exception {
    final PdfReader reader;
    // PdfReader reads the file from input stream in constructor. Do this before creating the writer because the write
    // overwrites the file.
    try (final FileInputStream is = new FileInputStream(file)) {
      reader = new PdfReader(is);
    }
    final WriterProperties writerProperties = new WriterProperties();
    final int permissions = EncryptionConstants.ALLOW_PRINTING | EncryptionConstants.ALLOW_SCREENREADERS
        | EncryptionConstants.DO_NOT_ENCRYPT_METADATA;

    writerProperties.setStandardEncryption(null, null, permissions, EncryptionConstants.ENCRYPTION_AES_256);
    writerProperties.setPdfVersion(PdfVersion.PDF_1_7);

    try (final FileOutputStream os = new FileOutputStream(file);
        final PdfDocument pdfDocument = new PdfDocument(reader, new PdfWriter(os, writerProperties));
        final Document document = new Document(pdfDocument)) {
      pdfDocument.setTagged();
      final PageSize pageSize = pdfContentDelegate.getPageSize(pdfDocument);

      final Image logo = getHeaderImage(pdfDocument, pdfProductType);

      pdfContentDelegate.processFrontPage(document, pageSize, logo, pdfFont, pdfTitles, reference);

      // Add header and footer to all other pages.
      final int numberOfPages = pdfDocument.getNumberOfPages();
      for (int i = 2; i <= numberOfPages; i++) {
        processPage(document, pageSize, numberOfPages, logo, i);
      }

      drawReleaseTypeIndication(document, pageSize, numberOfPages);
      attachGmlToDocument(pdfDocument, document);
      addMetaInfo(document, file);
    }
  }

  private void attachGmlToDocument(final PdfDocument pdfDocument, final Document document) throws Exception {
    if (!pdfProductType.containsGmls()) {
      // If no GML data should be included don't continue here.
      return;
    }
    final PdfZipFileAttacher pdfZipFileAttachter = new PdfZipFileAttacher(pdfAttachZipFileProcessor, pdfDocument, document);

    pdfZipFileAttachter.attachGmls();
  }

  private void addMetaInfo(final Document document, final File file) {
    final PdfDocumentInfo documentInfo = document.getPdfDocument().getDocumentInfo();

    documentInfo.setTitle(FileUtil.getFileWithoutExtension(file).replace('_', ' '));
  }

  private void processPage(final Document document, final Rectangle pageSize, final int numberOfPages, final Image logo, final int i) {
    final Paragraph pageNumber = new Paragraph(String.format("%d/%d", i, numberOfPages))
        .setFont(pdfFont)
        .setFontSize(pdfContentDelegate.getFontSizeFooter().floatValue());

    pdfContentDelegate.drawLogoOnPage(document, pageSize, logo, i);
    document.showTextAligned(pageTitle,
        BigDecimal.valueOf(pageSize.getWidth()).subtract(pdfContentDelegate.getMarginRight()).floatValue(),
        BigDecimal.valueOf(pageSize.getHeight()).subtract(pdfContentDelegate.getPageTitleTop()).floatValue(),
        i, TextAlignment.RIGHT, VerticalAlignment.TOP, 0);
    if (pageSubTitle != null) {
      document.showTextAligned(pageSubTitle,
          BigDecimal.valueOf(pageSize.getWidth()).subtract(pdfContentDelegate.getMarginRight()).floatValue(),
          BigDecimal.valueOf(pageSize.getHeight()).subtract(pdfContentDelegate.getPageTitleTopSecondRow()).floatValue(),
          i, TextAlignment.RIGHT, VerticalAlignment.TOP, 0);
    }
    document.showTextAligned(pageNumber,
        BigDecimal.valueOf(pageSize.getWidth()).subtract(pdfContentDelegate.getMarginRight()).floatValue(),
        pdfContentDelegate.getMarginVer().floatValue(),
        i, TextAlignment.RIGHT, VerticalAlignment.TOP, 0);
    document.showTextAligned(referenceAndDate,
        pdfContentDelegate.getMarginLeft().floatValue(), pdfContentDelegate.getMarginVer().floatValue(), i, TextAlignment.LEFT,
        VerticalAlignment.TOP, 0);
  }

  private void drawReleaseTypeIndication(final Document document, final Rectangle pageSize, final int numberOfPages) {
    if (releaseType != ReleaseType.PRODUCTION) {
      for (int page = 1; page <= numberOfPages; page++) {
        final Paragraph releaseText = new Paragraph(releaseType.name().toUpperCase(Locale.ROOT))
            .setFont(pdfFont)
            .setFontColor(new DeviceRgb(Color.BLACK), page == 1 ? 0.85F : 0.35F)
            .setFontSize(pdfContentDelegate.getFontSizeReleaseType().floatValue());
        document.showTextAligned(releaseText,
            BigDecimal.valueOf(pageSize.getWidth()).divide(BigDecimal.valueOf(2D)).floatValue(),
            BigDecimal.valueOf(pageSize.getHeight()).divide(BigDecimal.valueOf(2D)).floatValue(),
            page,
            TextAlignment.CENTER, VerticalAlignment.MIDDLE, DEGREES_30_IN_RADIANS);
      }
    }
  }

  private static Image getHeaderImage(final PdfDocument pdfDocument, final PdfProductType pdfProductType) throws IOException {
    try (final InputStream resourceAsStream = PdfContentProcessor.class.getResourceAsStream(pdfProductType.getLogoResource())) {
      return SvgConverter.convertToImage(resourceAsStream, pdfDocument);
    }
  }

}
