/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.util;

import java.io.File;
import java.io.IOException;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.RequeueTaskException;

/**
 * Interface for the generator to generate the PDF from the HTML served page.
 */
public interface PdfContentGenerator {

  /**
   * Generate the PDF from the given webpage url and store it in the file name passed.
   *
   * @param runId unique id for this run
   * @param pdfFileName name of the pdf file to use
   * @param url url to the webpage to generate the pdf from
   */
  void generate(String runId, File pdfFileName, String url) throws IOException, AeriusException, RequeueTaskException;
}
