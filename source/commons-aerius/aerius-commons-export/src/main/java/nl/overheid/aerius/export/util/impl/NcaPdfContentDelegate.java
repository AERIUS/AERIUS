/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.util.impl;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;

import nl.overheid.aerius.export.util.PdfThemeContentDelegate;
import nl.overheid.aerius.util.LocaleUtils;

public class NcaPdfContentDelegate implements PdfThemeContentDelegate {
  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd MMMM yyyy", LocaleUtils.getDefaultLocale());

  private static final BigDecimal LOGO_DIMENSION = BigDecimal.valueOf(PdfContentUtil.millimeterToUserUnit(0));
  private static final BigDecimal LOGO_LEFT = BigDecimal.valueOf(PdfContentUtil.millimeterToUserUnit(0));

  private static final BigDecimal MARGIN_LEFT = BigDecimal.valueOf(PdfContentUtil.millimeterToUserUnit(16));
  private static final BigDecimal MARGIN_RIGHT = BigDecimal.valueOf(PdfContentUtil.millimeterToUserUnit(16));
  private static final BigDecimal MARGIN_VER = BigDecimal.valueOf(PdfContentUtil.millimeterToUserUnit(16));
  private static final BigDecimal TITLE_TOP = BigDecimal.valueOf(PdfContentUtil.millimeterToUserUnit(10.6))
      .add(LOGO_DIMENSION.divide(BigDecimal.valueOf(2)));
  private static final BigDecimal TITLE_TOP_SECOND_ROW = BigDecimal.valueOf(PdfContentUtil.millimeterToUserUnit(10.6))
      .add(BigDecimal.valueOf(PdfContentUtil.millimeterToUserUnit(12.6)))
      .add(LOGO_DIMENSION.divide(BigDecimal.valueOf(2)));
  private static final BigDecimal PAGE_TITLE_TOP = BigDecimal.valueOf(PdfContentUtil.millimeterToUserUnit(10.6));
  private static final BigDecimal PAGE_TITLE_TOP_SECOND_ROW = PAGE_TITLE_TOP.add(BigDecimal.valueOf(18));
  private static final BigDecimal FONT_SIZE_FRONT_PAGE_TITLE = BigDecimal.valueOf(22); // in points
  private static final BigDecimal FONT_SIZE_FRONT_PAGE_SUB_TITLE = BigDecimal.valueOf(14); // in points
  private static final BigDecimal FONT_SIZE_PAGE_TITLE = BigDecimal.valueOf(12); // in points
  private static final BigDecimal FONT_SIZE_PAGE_SUB_TITLE = BigDecimal.valueOf(10); // in points
  private static final BigDecimal FONT_SIZE_RELEASE_TYPE = BigDecimal.valueOf(48); // in points
  private static final BigDecimal FONT_SIZE_FOOTER = BigDecimal.valueOf(8);

  private static final BigDecimal LOGO_TOP = null;

  private static final String FONT = "NotoSansTC-Regular.otf";

  @Override
  public PageSize getPageSize(final PdfDocument pdfDocument) {
    final PageSize pageSize = pdfDocument.getDefaultPageSize();

    return pageSize.rotate(); // Make it landscape
  }


  @Override
  public String getFont() {
    return FONT;
  }

  @Override
  public BigDecimal getLogoTop() {
    return LOGO_TOP;
  }

  // Getters for the private static final constants
  @Override
  public BigDecimal getLogoDimension() {
    return LOGO_DIMENSION;
  }

  @Override
  public BigDecimal getLogoLeft() {
    return LOGO_LEFT;
  }

  @Override
  public BigDecimal getMarginLeft() {
    return MARGIN_LEFT;
  }

  @Override
  public BigDecimal getMarginRight() {
    return MARGIN_RIGHT;
  }

  @Override
  public BigDecimal getMarginVer() {
    return MARGIN_VER;
  }

  @Override
  public BigDecimal getTitleTop() {
    return TITLE_TOP;
  }

  @Override
  public BigDecimal getTitleTopSecondRow() {
    return TITLE_TOP_SECOND_ROW;
  }

  @Override
  public BigDecimal getPageTitleTop() {
    return PAGE_TITLE_TOP;
  }

  @Override
  public BigDecimal getPageTitleTopSecondRow() {
    return PAGE_TITLE_TOP_SECOND_ROW;
  }

  @Override
  public BigDecimal getFontSizeFrontPageTitle() {
    return FONT_SIZE_FRONT_PAGE_TITLE;
  }

  @Override
  public BigDecimal getFontSizeFrontPageSubTitle() {
    return FONT_SIZE_FRONT_PAGE_SUB_TITLE;
  }

  @Override
  public BigDecimal getFontSizePageTitle() {
    return FONT_SIZE_PAGE_TITLE;
  }

  @Override
  public BigDecimal getFontSizePageSubTitle() {
    return FONT_SIZE_PAGE_SUB_TITLE;
  }

  @Override
  public BigDecimal getFontSizeReleaseType() {
    return FONT_SIZE_RELEASE_TYPE;
  }

  @Override
  public BigDecimal getFontSizeFooter() {
    return FONT_SIZE_FOOTER;
  }

  @Override
  public DateTimeFormatter getDateTimeFormatter() {
    return DATE_TIME_FORMATTER;
  }
}
