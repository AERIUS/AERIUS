/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export;

import java.io.File;
import java.time.Clock;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.SituationResultRepositoryBean;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.export.util.PdfContentGenerator;
import nl.overheid.aerius.export.util.PdfContentProcessor;
import nl.overheid.aerius.export.util.PdfContentProcessorFactory;
import nl.overheid.aerius.export.util.PdfTask;
import nl.overheid.aerius.export.util.PdfThemeContentDelegate;
import nl.overheid.aerius.export.util.PdfTitles;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Generates a PDF via the {@link PdfContentGenerator} and performs a post processing step to add additional information
 * to the PDF.
 */
public class PdfGenerator {

  private static final Logger LOG = LoggerFactory.getLogger(PdfGenerator.class);

  private final PMF pmf;
  private final PdfContentGenerator pdfContentGenerator;
  private final PdfContentProcessorFactory pdfContentProcessorFactory;

  public PdfGenerator(final PMF pmf, final PdfContentGenerator pdfContentGenerator) {
    this.pmf = pmf;
    this.pdfContentGenerator = pdfContentGenerator;
    this.pdfContentProcessorFactory = new PdfContentProcessorFactory();
  }

  public File generate(final File pdfFile, final String reference, final PdfTask pdfTask, final String url,
      final PdfAttachZipFileProcessor pdfAttachFileProcessor, final Locale locale) throws Exception {
    LOG.info("Start generating pdf for job '{}' with url {}", pdfTask.jobIdentifier(), url);
    pdfContentGenerator.generate(reference, pdfFile, url);

    LOG.info("Start post processing pdf for job '{}'", pdfTask.jobIdentifier());
    final PdfThemeContentDelegate pdfContentDelegate = pdfContentProcessorFactory.create(pdfTask);
    final PdfContentProcessor pdfContentProcessor = new PdfContentProcessor(pdfContentDelegate, pdfAttachFileProcessor, reference,
        pdfTask, locale, getPdfTitles(pdfTask, locale), Clock.systemDefaultZone());

    pdfContentProcessor.process(pdfFile);

    return pdfFile;
  }

  private PdfTitles getPdfTitles(final PdfTask pdfTask, final Locale locale) throws AeriusException {
    final PdfProductType pdfProductType = pdfTask.pdfProductType();
    final JobIdentifier jobIdentifier = pdfTask.jobIdentifier();
    return new PdfTitles(getFrontPageTitle(pdfProductType, locale), getFrontPageSubTitle(pdfProductType, locale),
        getPageTitle(pdfProductType, jobIdentifier, locale), getPageSubTitle(pdfProductType, locale), getReferenceTitle(pdfProductType, locale),
        getTitleColor(pdfProductType));
  }

  private String getFrontPageTitle(final PdfProductType pdfProductType, final Locale locale) {
    final MessagesEnum key = switch (pdfProductType) {
    case OWN2000_CALCULATOR -> MessagesEnum.PDF_OWN2000_FRONT_PAGE_TITLE;
    case OWN2000_SINGLE_SCENARIO -> MessagesEnum.PDF_OWN2000_SINGLE_SCENARIO_FRONT_PAGE_TITLE;
    case OWN2000_EDGE_EFFECT_APPENDIX -> MessagesEnum.PDF_OWN2000_EDGE_EFFECT_REPORT_FRONT_PAGE_TITLE;
    case OWN2000_EXTRA_ASSESSMENT_APPENDIX -> MessagesEnum.PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_FRONT_PAGE_TITLE;
    case OWN2000_LBV_POLICY -> MessagesEnum.PDF_OWN2000_LBV_POLICY_FRONT_PAGE_TITLE;
    case OWN2000_PERMIT -> MessagesEnum.PDF_PERMIT_DEMAND_FRONT_PAGE_TITLE;
    case NCA_CALCULATOR -> MessagesEnum.PDF_NCA_FRONT_PAGE_TITLE;
    };
    return messageRepositoryGetString(key, locale);
  }

  private String getFrontPageSubTitle(final PdfProductType pdfProductType, final Locale locale) {
    return switch (pdfProductType) {
    case NCA_CALCULATOR -> messageRepositoryGetString(MessagesEnum.PDF_NCA_FRONT_PAGE_SUB_TITLE, locale);
    case OWN2000_EDGE_EFFECT_APPENDIX -> messageRepositoryGetString(MessagesEnum.PDF_OWN2000_EDGE_EFFECT_REPORT_FRONT_PAGE_SUB_TITLE, locale);
    case OWN2000_EXTRA_ASSESSMENT_APPENDIX -> messageRepositoryGetString(MessagesEnum.PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_FRONT_PAGE_SUB_TITLE,
        locale);
    default -> "";
    };
  }

  public String getPageTitle(final PdfProductType pdfProductType, final JobIdentifier jobIdentifier, final Locale locale) throws AeriusException {
    final MessagesEnum key = switch (pdfProductType) {
    case OWN2000_CALCULATOR -> MessagesEnum.PDF_OWN2000_PAGE_TITLE;
    case OWN2000_SINGLE_SCENARIO -> MessagesEnum.PDF_OWN2000_SINGLE_SCENARIO_PAGE_TITLE;
    case OWN2000_EDGE_EFFECT_APPENDIX -> MessagesEnum.PDF_OWN2000_EDGE_EFFECT_REPORT_PAGE_TITLE;
    case OWN2000_EXTRA_ASSESSMENT_APPENDIX -> MessagesEnum.PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_PAGE_TITLE;
    case OWN2000_PERMIT -> pdfPermitDemandTitle(jobIdentifier);
    case OWN2000_LBV_POLICY -> MessagesEnum.PDF_OWN2000_LBV_POLICY_PAGE_TITLE;
    case NCA_CALCULATOR -> MessagesEnum.PDF_NCA_PAGE_TITLE;
    };
    return messageRepositoryGetString(key, locale);
  }

  public String getPageSubTitle(final PdfProductType pdfProductType, final Locale locale) {
    return switch (pdfProductType) {
    case OWN2000_EDGE_EFFECT_APPENDIX -> messageRepositoryGetString(MessagesEnum.PDF_OWN2000_EDGE_EFFECT_REPORT_PAGE_SUB_TITLE, locale);
    case OWN2000_EXTRA_ASSESSMENT_APPENDIX -> messageRepositoryGetString(MessagesEnum.PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_PAGE_SUB_TITLE, locale);
    default -> "";
    };
  }

  public String getReferenceTitle(final PdfProductType pdfProductType, final Locale locale) {
    return switch (pdfProductType) {
    case OWN2000_EDGE_EFFECT_APPENDIX -> messageRepositoryGetString(MessagesEnum.PDF_OWN2000_EDGE_EFFECT_REPORT_REFERENCE_TITLE, locale);
    case OWN2000_EXTRA_ASSESSMENT_APPENDIX -> messageRepositoryGetString(MessagesEnum.PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_REFERENCE_TITLE, locale);
    default -> "";
    };
  }

  private static Color getTitleColor(final PdfProductType pdfProductType) {
    return switch (pdfProductType) {
    case OWN2000_LBV_POLICY -> new DeviceRgb(209, 99, 38);
    case OWN2000_SINGLE_SCENARIO -> new DeviceRgb(102, 102, 102);
    default -> new DeviceRgb(25, 68, 116);
    };
  }

  private MessagesEnum pdfPermitDemandTitle(final JobIdentifier jobIdentifier) throws AeriusException {
    final SituationResultRepositoryBean situationResultRepository = new SituationResultRepositoryBean(pmf);
    final boolean permitDemandStatus = situationResultRepository.determinePermitDemandCheck(jobIdentifier.getJobKey());
    return permitDemandStatus
        ? MessagesEnum.PDF_PERMIT_DEMAND_AVAILABLE_TITLE
        : MessagesEnum.PDF_PERMIT_DEMAND_NOT_AVAILABLE_TITLE;
  }

  private String messageRepositoryGetString(final MessagesEnum key, final Locale locale) {
    return key == null ? "" : MessageRepository.getString(pmf, key, locale);
  }
}
