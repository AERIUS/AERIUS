/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.export.util.PdfContentGenerator;
import nl.overheid.aerius.export.util.PdfTask;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link PdfGenerator}.
 */
@ExtendWith(MockitoExtension.class)
class PdfGeneratorTest {

  private static final String WEBSITE_URL = "http://localhost";
  private static final String REFERENCE = "reference";
  private static final String DUMMY = "dummy";
  private static final JobIdentifier JOB_IDENTIFIER = new JobIdentifier("jobKey");
  private static final Map<PdfProductType, MessagesEnum> FRONT_PAGE_TITLES = Map.of(PdfProductType.OWN2000_CALCULATOR,
      MessagesEnum.PDF_OWN2000_FRONT_PAGE_TITLE,
      PdfProductType.OWN2000_EDGE_EFFECT_APPENDIX, MessagesEnum.PDF_OWN2000_EDGE_EFFECT_REPORT_FRONT_PAGE_TITLE,
      PdfProductType.OWN2000_EXTRA_ASSESSMENT_APPENDIX, MessagesEnum.PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_FRONT_PAGE_TITLE,
      PdfProductType.OWN2000_LBV_POLICY, MessagesEnum.PDF_OWN2000_LBV_POLICY_FRONT_PAGE_TITLE,
      PdfProductType.OWN2000_PERMIT, MessagesEnum.PDF_PERMIT_DEMAND_FRONT_PAGE_TITLE,
      PdfProductType.NCA_CALCULATOR, MessagesEnum.PDF_NCA_FRONT_PAGE_TITLE,
      PdfProductType.OWN2000_SINGLE_SCENARIO, MessagesEnum.PDF_OWN2000_SINGLE_SCENARIO_FRONT_PAGE_TITLE);
  private static final Map<PdfProductType, MessagesEnum> PAGE_TITLES = Map.of(PdfProductType.OWN2000_CALCULATOR,
      MessagesEnum.PDF_OWN2000_FRONT_PAGE_TITLE,
      PdfProductType.OWN2000_EDGE_EFFECT_APPENDIX, MessagesEnum.PDF_OWN2000_EDGE_EFFECT_REPORT_PAGE_TITLE,
      PdfProductType.OWN2000_EXTRA_ASSESSMENT_APPENDIX, MessagesEnum.PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_PAGE_TITLE,
      PdfProductType.OWN2000_LBV_POLICY, MessagesEnum.PDF_OWN2000_LBV_POLICY_PAGE_TITLE,
      PdfProductType.OWN2000_PERMIT, MessagesEnum.PDF_PERMIT_DEMAND_NOT_AVAILABLE_TITLE,
      PdfProductType.NCA_CALCULATOR, MessagesEnum.PDF_NCA_PAGE_TITLE,
      PdfProductType.OWN2000_SINGLE_SCENARIO, MessagesEnum.PDF_OWN2000_SINGLE_SCENARIO_PAGE_TITLE);

  private @TempDir File tempDir;
  private @Mock PMF pmf;
  private @Mock Connection connection;
  private @Mock PreparedStatement stmt;
  private @Mock ResultSet rs;
  private @Mock PdfContentGenerator pdfContentGenerator;
  private @Mock PdfAttachZipFileProcessor pdfAttachFileProcessor;

  private @Captor ArgumentCaptor<MessagesEnum> messagesEnumArgumentCaptor;

  @ParameterizedTest
  @EnumSource(value = PdfProductType.class)
  void testGeneratePdfOwN2000Calculator(final PdfProductType pdfProductType) throws Exception {
    final PdfGenerator pdfGenerator = new PdfGenerator(pmf, pdfContentGenerator);
    final File file = new File(getClass().getResource("pre-processing.pdf").getFile());
    final File pdfFile = new File(tempDir, "aerius.pdf");
    Files.copy(file.toPath(), pdfFile.toPath());

    mockConnection();
    try (final MockedStatic<MessageRepository> messageRepositoryMockedStatic = mockStatic(MessageRepository.class)) {
      messageRepositoryMockedStatic.when(
          () -> MessageRepository.getString(eq(pmf), messagesEnumArgumentCaptor.capture(), eq(LocaleUtils.getDefaultLocale()))).thenReturn(DUMMY);
      final PdfTask pdfTask = new PdfTask(AeriusCustomer.RIVM, WEBSITE_URL, JOB_IDENTIFIER, pdfProductType, ReleaseType.CONCEPT);

      pdfGenerator.generate(pdfFile, REFERENCE, pdfTask, WEBSITE_URL, pdfAttachFileProcessor, LocaleUtils.getDefaultLocale());

      verify(pdfContentGenerator).generate(REFERENCE, pdfFile, WEBSITE_URL);
      assertTrue(messagesEnumArgumentCaptor.getAllValues().contains(FRONT_PAGE_TITLES.get(pdfProductType)),
          "Should get " + pdfProductType + " frontpage title from database");
      assertTrue(messagesEnumArgumentCaptor.getAllValues().contains(PAGE_TITLES.get(pdfProductType)),
          "Should get " + pdfProductType + " page title from database");
    } finally {
      pdfFile.delete();
    }
  }

  private void mockConnection() throws SQLException {
    lenient().doReturn(connection).when(pmf).getConnection();
    lenient().doReturn(connection).when(pmf).getConnection();
    lenient().doReturn(stmt).when(connection).prepareStatement(any());
    lenient().doReturn(rs).when(stmt).executeQuery();
    lenient().doReturn(false).when(rs).next();
  }
}
