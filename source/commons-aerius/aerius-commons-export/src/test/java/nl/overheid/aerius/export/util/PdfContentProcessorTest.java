/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.util;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.canvas.parser.PdfTextExtractor;

import nl.overheid.aerius.PAAConstants;
import nl.overheid.aerius.export.PdfAttachZipFileProcessor;
import nl.overheid.aerius.export.PdfGenerator;
import nl.overheid.aerius.export.util.impl.Own2000PdfContentDelegate;
import nl.overheid.aerius.paa.PAAImport;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.util.FileProcessor;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link PdfContentProcessor} and {@link Own2000PdfContentDelegate}.
 */
class PdfContentProcessorTest {

  private static final Logger LOG = LoggerFactory.getLogger(PdfGenerator.class);

  private static final boolean DEBUGGING = false;

  private static final String ZIP_FILENAME = "filename.zip";
  private static final String WEBSITE_URL = "http://localhost";
  private static final byte[] ATTACHMENT_CONTENT = "ATTACHEMENT_CONTENT".getBytes();
  private static final LocalDate TEST_DATE = LocalDate.of(2022, 1, 1);
  private static final String REFERENCE = "reference";
  private static final String FRONT_PAGE_HEADER = "frontPageHeader";
  private static final String FRONT_PAGE_SUB_HEADER = "frontPageSubHeader";
  private static final String PAGE_HEADER = "pageHeader";
  private static final PdfTitles PDF_TITLES = new PdfTitles(FRONT_PAGE_HEADER, FRONT_PAGE_SUB_HEADER, PAGE_HEADER, "", "",
      new DeviceRgb(25, 68, 116));
  private static final Set<PdfProductType> SHOULD_CONTAIN_GML = Set.of(PdfProductType.OWN2000_CALCULATOR, PdfProductType.OWN2000_LBV_POLICY,
      PdfProductType.NCA_CALCULATOR);


  private static final ReleaseType RELEASE_TYPE = ReleaseType.CONCEPT;
  private PdfContentProcessorFactory pdfContentProcessorFactory;

  private @TempDir Path tempDir;

  @BeforeEach
  void setup() {
    pdfContentProcessorFactory = new PdfContentProcessorFactory();
  }

  @ParameterizedTest
  @EnumSource(value = PdfProductType.class)
  void testProcess(final PdfProductType pdfProductType) throws Exception {
    final Clock fixedClock = Clock.fixed(TEST_DATE.atStartOfDay().toInstant(ZoneOffset.UTC), ZoneOffset.UTC);
    final PdfTask pdfTask = new PdfTask(AeriusCustomer.RIVM, WEBSITE_URL, new JobIdentifier("123"), pdfProductType, RELEASE_TYPE);
    final File zipFile = new File(tempDir.toFile(), ZIP_FILENAME);
    Files.write(zipFile.toPath(), ATTACHMENT_CONTENT);

    final PdfAttachZipFileProcessor pdfAttachZipFileProcessor = new PdfAttachZipFileProcessor() {
      @Override
      public void attachFile(final FileProcessor fileProcessor) throws Exception {
        fileProcessor.processFile("", ZIP_FILENAME, zipFile, null);
      }
    };
    final PdfThemeContentDelegate delegate = pdfContentProcessorFactory.create(pdfTask);
    final PdfContentProcessor pdfContentProcessor = new PdfContentProcessor(delegate, pdfAttachZipFileProcessor, REFERENCE, pdfTask,
        LocaleUtils.getDefaultLocale(), PDF_TITLES, fixedClock);

    // Copy the test file, because it is manipulated in-place.
    final Path original = Path.of(getClass().getResource("pre-processing.pdf").toURI());
    final Path tempCopy = tempDir.resolve("pre-processing-copy.pdf");
    Files.copy(original, tempCopy);
    pdfContentProcessor.process(tempCopy.toFile());

    try (final PdfDocument actualPDF = new PdfDocument(new PdfReader(tempCopy.toFile()))) {
      final boolean shouldContainGML = SHOULD_CONTAIN_GML.contains(pdfProductType);
      final byte[] actualAttachmentContent = shouldContainGML ? ATTACHMENT_CONTENT : null;
      assertArrayEquals(actualAttachmentContent, PAAImport.getAttachment(actualPDF), "Attachment should be added");
      final boolean hash = !Optional.ofNullable(actualPDF.getDocumentInfo().getMoreInfo(PAAConstants.CALC_PDF_METADATA_HASH_KEY)).orElse("").isBlank();
      assertEquals(shouldContainGML, hash, "A hash of the zip should be set if the pdf contains a GML.");

      assertEquals(6, actualPDF.getNumberOfPages(), "No pages should be added or removed by post-processing");

      for (int i = 2; i <= actualPDF.getNumberOfPages(); i++) {
        final String textFromPage = PdfTextExtractor.getTextFromPage(actualPDF.getPage(i));

        assertTrue(textFromPage.contains(PAGE_HEADER), "All pages should mention document title");
        assertTrue(textFromPage.contains(i + "/" + actualPDF.getNumberOfPages()), "All pages should be numbered");
        assertTrue(textFromPage.contains(pdfContentProcessor.getPdfContentAdaptor().getDateTimeFormatter().format(TEST_DATE)),
            "All pages should contain date");
        assertTrue(textFromPage.contains(REFERENCE), "All pages should contain reference.");
      }
    }

    // Debugging purposes, left in for convenience sake
    if (DEBUGGING) {
      if (pdfProductType == PdfProductType.NCA_CALCULATOR) {
        final Path homeDir = Paths.get(System.getProperty("user.home"));
        final Path dump = homeDir.resolve("uk-pdf-output");

        Files.createDirectories(dump);
        final Path dest = dump.resolve("out.pdf");
        Files.copy(tempCopy, dest, StandardCopyOption.REPLACE_EXISTING);
        LOG.info("{} > {}", pdfProductType, dest.toAbsolutePath());
      }
    }
  }
}
