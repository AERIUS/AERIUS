/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.chrome;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ChromeRunner}.
 */
@ExtendWith(MockitoExtension.class)
class ChromeRunnerTest {

  private static final int TIMEOUT = 100;

  private @Mock Runtime runtime;
  private @Mock Process process;
  private @Mock InputStream outputStream;
  private @TempDir File tempDir;

  private @Captor ArgumentCaptor<String[]> parameterCaptor;

  @BeforeEach
  void before() throws IOException {
    doReturn(process).when(runtime).exec(parameterCaptor.capture(), any(), any());
    doReturn(new ByteArrayInputStream(new byte[0])).when(process).getErrorStream();
    doReturn(new ByteArrayInputStream(new byte[0])).when(process).getInputStream();
  }

  @Test
  void testRun() throws IOException, AeriusException {
    final ChromeConfiguration configuration = new ChromeConfiguration(1);
    configuration.setNewHeadless(true);
    configuration.setTimeout(TIMEOUT);
    configuration.setRootDir(tempDir);
    final ChromeRunner runner = new ChromeRunner(configuration);
    runner.setRuntime(runtime);
    final File pdfFilename = new File("dummy");
    final String url = "http://localhost";
    runner.generate("123", pdfFilename, url);
    final List<String> parameters = List.of(parameterCaptor.getValue());

    assertEquals(tempDir,
        new File(parameters.get(0)).getParentFile().getAbsoluteFile(),
        "First parameter should contain path to chrome executable");
    assertTrue(parameters.stream().anyMatch("--headless=new"::equals), "Should contain parameter to use new headless");
    final String vtb = "--virtual-time-budget=" + TIMEOUT;
    assertTrue(parameters.stream().anyMatch(vtb::equals), "Should contain parameter to virtual time budget");
  }
}
