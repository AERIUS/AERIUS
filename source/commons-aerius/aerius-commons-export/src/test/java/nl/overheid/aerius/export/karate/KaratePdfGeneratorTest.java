/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export.karate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.intuit.karate.KarateException;

import nl.aerius.print.ExportJob;
import nl.aerius.print.PrintJob;
import nl.overheid.aerius.export.util.PdfContentGenerator;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.RequeueTaskException;

/**
 * Test class for {@link KaratePdfGenerator}.
 */
@ExtendWith(MockitoExtension.class)
class KaratePdfGeneratorTest {
  private static final String WEBSERVER_URL = "webserverUrl";
  private static final String REFERENCE = "reference";
  private static final File DUMMY_PDF_FILE = new File("aerius.pdf");

  private @Spy ExportJob exportJob;

  private @Mock PrintJob printJob;

  private PdfContentGenerator pdfContentGenerator;

  private @Captor ArgumentCaptor<String> urlArgumentCaptor;

  @BeforeEach
  void setup() throws IOException {
    pdfContentGenerator = new KaratePdfGenerator();
  }

  @ParameterizedTest
  @EnumSource(value = PdfProductType.class, names = {"OWN2000_CALCULATOR", "OWN2000_LBV_POLICY"})
  void testGenerate(final PdfProductType pdfProductType) throws AeriusException, URISyntaxException, IOException, RequeueTaskException {
    doReturn(printJob).when(exportJob).print(any());
    when(printJob.save()).thenReturn(exportJob);
    final String testFileLocation = getClass().getResource("../pre-processing.pdf").toURI().toString();
    final File testFile = new File(testFileLocation);
    when(exportJob.outputDocument()).thenReturn(testFileLocation);

    try (final MockedStatic<ExportJob> exportJobMockedStatic = mockStatic(ExportJob.class)) {
      exportJobMockedStatic.when(() -> ExportJob.create(urlArgumentCaptor.capture())).thenReturn(exportJob);

      pdfContentGenerator.generate(REFERENCE, testFile, WEBSERVER_URL);

      assertEquals(WEBSERVER_URL, urlArgumentCaptor.getValue(), "url should be same");
    }
  }

  @Test
  void testGenerateKarateException() throws AeriusException, URISyntaxException, IOException {
    final Exception karateException = new KarateException("My karate exception");

    try (final MockedStatic<ExportJob> exportJobMockedStatic = mockStatic(ExportJob.class)) {
      exportJobMockedStatic.when(() -> ExportJob.create(urlArgumentCaptor.capture())).thenThrow(karateException);

      final KarateException thrownException = assertThrows(KarateException.class,
          () -> pdfContentGenerator.generate(REFERENCE, DUMMY_PDF_FILE, WEBSERVER_URL), "Should throw an KarateException");

      assertEquals(karateException, thrownException, "Exception should be thrown as-is");
    }
  }

  @Test
  void testGenerateKarateIOException() throws AeriusException, URISyntaxException, IOException {
    final IOException ioException = new IOException("Our little io exception");
    final Exception karateException = new KarateException("My karate exception", ioException);

    try (final MockedStatic<ExportJob> exportJobMockedStatic = mockStatic(ExportJob.class)) {
      exportJobMockedStatic.when(() -> ExportJob.create(urlArgumentCaptor.capture())).thenThrow(karateException);

      final IOException thrownException = assertThrows(IOException.class,
          () -> pdfContentGenerator.generate(REFERENCE, DUMMY_PDF_FILE, WEBSERVER_URL), "Should throw an IOException");

      assertEquals(ioException, thrownException, "Exception thrown should be the cause of the KarateException in case of IOException");
    }
  }

  @Test
  void testGenerateChromeFailureRuntimeException() throws AeriusException, URISyntaxException, IOException {
    final String exceptionMessage = "waitUntil (js): failed after 21 retries and 126049 milliseconds";
    final Exception runtimeException = new RuntimeException(new RuntimeException(exceptionMessage));

    try (final MockedStatic<ExportJob> exportJobMockedStatic = mockStatic(ExportJob.class)) {
      exportJobMockedStatic.when(() -> ExportJob.create(urlArgumentCaptor.capture())).thenThrow(runtimeException);

      final RequeueTaskException thrownException = assertThrows(RequeueTaskException.class,
          () -> pdfContentGenerator.generate(REFERENCE, DUMMY_PDF_FILE, WEBSERVER_URL),
          "Should throw a RequeueTaskException");

      assertEquals(exceptionMessage, thrownException.getMessage(), "Exception should contain message of RuntimeException");
    }
  }
}
