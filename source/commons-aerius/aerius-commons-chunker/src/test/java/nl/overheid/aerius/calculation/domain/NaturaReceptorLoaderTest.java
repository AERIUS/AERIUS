/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.sql.SQLException;
import java.util.Collection;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.natura2k.Natura2KReceptorLoader;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link Natura2KReceptorLoader}.
 */
class NaturaReceptorLoaderTest extends BaseDBTest {

  @Test
  void testFillNatura2kReceptorStore() throws SQLException, AeriusException {
    final Natura2KReceptorLoader loader = new Natura2KReceptorLoader(RECEPTOR_UTIL, new GridSettings(RECEPTOR_GRID_SETTINGS));
    final Natura2kReceptorStore store = loader.fillReceptorStore(getConnection());
    final int n2kId = 65;
    final Collection<AeriusPoint> gridIds = store.getReceptors(n2kId);
    assertNotEquals(0, gridIds.size(), "Grid id's for natura area should not be empty");
  }
}
