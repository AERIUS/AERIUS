/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link RBLOuputHandler).
 */
class RBLCsvOutputHandlerTest {

  private static final String EXPECTED_FILE = "rbl_sorted_results.csv";
  private static final int DEFAULT_CALCULATION_ID = 231;
  private static final String DEFAULT_SCENARIO_NAME = "Mijn berekening";

  @TempDir
  File tempDir;

  @Test
  void testNormal() throws IOException, AeriusException {
    final Date date = Date.from(ZonedDateTime.of(2020, 11, 28, 19, 51, 43, 0, ZoneId.systemDefault()).toInstant());
    final RBLOuputHandler handler = new RBLOuputHandler(tempDir, Map.of(DEFAULT_CALCULATION_ID, 2030), "DEV", "Test-database", DEFAULT_SCENARIO_NAME,
        date);

    handler.onTotalResults(firstResult(), DEFAULT_CALCULATION_ID);
    handler.onTotalResults(secondResult(), DEFAULT_CALCULATION_ID);
    handler.onTotalResults(thirdResult(), DEFAULT_CALCULATION_ID);

    handler.onCompleted(null);

    final File[] generatedFiles = tempDir.listFiles();
    assertEquals(1, generatedFiles.length, "Number of generated files");
    assertTrue(generatedFiles[0].getName().startsWith("231_results_"), "File name prefix");
    assertTrue(generatedFiles[0].getName().contains(DEFAULT_SCENARIO_NAME.replace(" ", "")), "File name prefix");
    assertTrue(generatedFiles[0].getName().contains("20201128195143"), "File name prefix");

    assertEquals(getFileContent(expectedFile()), getFileContent(generatedFiles[0]), "Result file should be sorted");
  }

  @Test
  void testWithReversedOrder() throws IOException, AeriusException {
    final RBLOuputHandler handler = new RBLOuputHandler(tempDir, Map.of(DEFAULT_CALCULATION_ID, 2030), "DEV", "Test-database", DEFAULT_SCENARIO_NAME,
        new Date());

    handler.onTotalResults(thirdResult(), DEFAULT_CALCULATION_ID);
    handler.onTotalResults(secondResult(), DEFAULT_CALCULATION_ID);
    handler.onTotalResults(firstResult(), DEFAULT_CALCULATION_ID);

    handler.onCompleted(null);

    final File[] generatedFiles = tempDir.listFiles();
    assertEquals(1, generatedFiles.length, "Number of generated files");

    assertEquals(getFileContent(expectedFile()), getFileContent(generatedFiles[0]), "Result file should be sorted");
  }

  private List<AeriusResultPoint> firstResult() {
    return Arrays.asList(mockResult("aaa", 23), mockResult("AAA", 11), mockResult("aAa", 44));
  }

  private List<AeriusResultPoint> secondResult() {
    return Arrays.asList(mockResult("4", 3), mockResult("2", 7), mockResult("3", 2));
  }

  private List<AeriusResultPoint> thirdResult() {
    return Arrays.asList(mockResult("c1", 3), mockResult("D4", 7), mockResult("b3", 2));
  }

  private CIMLKResultPoint mockResult(final String id, final int randomResult) {
    final CIMLKResultPoint resultPoint = new CIMLKResultPoint(new AeriusPoint());
    resultPoint.setId(randomResult);
    resultPoint.setGmlId(id);
    resultPoint.setLabel("SomeLabel");
    for (final EmissionResultKey resultKey : EmissionResultKey.values()) {
      resultPoint.setEmissionResult(resultKey, 1.01 * randomResult * resultKey.ordinal());
    }

    return resultPoint;
  }

  private String getFileContent(final File file) throws IOException {
    // Remove windows style line ending because csv output writes files with windows line endings
    // while our test files are unix file format.
    return FileUtils.readFileToString(file, StandardCharsets.UTF_8).replace("\r", "");
  }

  private File expectedFile() {
    final URL url = RBLOuputHandler.class.getResource(EXPECTED_FILE);
    return url == null ? null : new File(url.getFile());
  }

}
