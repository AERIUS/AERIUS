/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.mock.TestInputBuilder;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.result.ExportResult;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.test.TestDomain;

/**
 * Integration test to run test the Export command in chunker.
 */
class ExportIT extends CalculationITBase {

  private static final String HTTP_NO_FILE_SERVER = "http://localhost/test/test.zip";
  private static final String FILECODE = "1234";
  private final ExportResult mockExportResult = new ExportResult(FILECODE);
  private EngineInputData<?, ?> constructedInput;
  private final Consumer<EngineInputData<?, ?>> inputAsserter = input -> {
    constructedInput = input;
    passedFileService = HTTP_NO_FILE_SERVER;
  };
  private String passedFileService;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp((dsv, rdf) -> new ExportMockConnection(mockExportResult, inputAsserter), AeriusCustomer.RIVM, WorkerType.OPS);
  }

  @Test
  @Timeout(value = 20, unit = TimeUnit.SECONDS)
  void testRunExport() throws Exception {
    final CalculationInputData data = new ExportTestInputBuilder().createInputData();
    data.setExportType(ExportType.OPS);
    run(data);

    assertTrue(constructedInput instanceof OPSInputData, "Data generated should be of OPSInputData.");
    assertEquals(FILECODE, calculationJob.getResultUrl(), "Database should contain result url");
    assertEquals(HTTP_NO_FILE_SERVER, passedFileService, "FileServiceUrl should be set");
  }

  private static class ExportTestInputBuilder extends TestInputBuilder {

    public ExportTestInputBuilder() {
      super(Theme.OWN2000);
    }

    @Override
    protected void setSources(final List<EmissionSourceFeature> sources, final double emissionFactor) {
      sources.addAll(TestDomain.getExampleSourceList());
    }

    @Override
    protected CalculationSetOptions setCalculationSetOptions(final CalculationSetOptions options) {
      options.setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
      return options;
    }
  }
}
