/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.results;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link ResultsSummariesHandler}.
 */
class ResultsSummariesHandlerTest extends CalculationRepositoryTestBase {

  @Test()
  void testOnCompleted() throws SQLException, AeriusException {
    insertCalculationResults();
    final Scenario scenario = new Scenario(Theme.OWN2000);
    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);
    scenarioCalculations.getOptions().setCalculationMethod(CalculationMethod.NATURE_AREA);
    scenarioCalculations.getOptions().getSubstances().add(Substance.NH3);
    final JobIdentifier jobIdentifier = new JobIdentifier(JobRepository.createJob(getConnection(), JobType.CALCULATION, false));
    final CalculationJob calculationJob = new CalculationJob(jobIdentifier, null, scenarioCalculations, ExportType.GML_WITH_RESULTS, null);
    final ResultsSummariesHandler handler = new ResultsSummariesHandler(getPMF());

    // Check if not throwing an exception to have at least 1 assert.
    // Test might need some better validations, like if actually inserted.
    assertDoesNotThrow(() -> handler.onCompleted(calculationJob), "The onCompleted should finish without exception.");
  }
}
