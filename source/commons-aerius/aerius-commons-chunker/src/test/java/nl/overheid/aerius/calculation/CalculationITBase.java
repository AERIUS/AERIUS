/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.function.BiFunction;

import org.junit.jupiter.api.AfterEach;

import com.rabbitmq.client.Connection;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.configuration.BrokerConfiguration;
import nl.aerius.taskmanager.client.configuration.ConnectionConfiguration;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor.RabbitMQWorkerObserver;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.PropertiesUtil;


/**
 * Base class for Calculation Integration tests.
 */
abstract class CalculationITBase extends BaseDBTest {

  protected TestBrokerConnectionFactory factory;
  protected Properties properties;
  protected CalculatorBuildDirector builder;
  protected CalculationJob calculationJob;

  public void setUp(final BiFunction<Double, Double, Connection> mockConnectionFactory, final AeriusCustomer customer,
      final WorkerType... workerTypes) throws Exception {
    //worker uses connections on different threads, which doesn't really work with a cached connection which is used for all those threads...
    getPMF().setAutoCommit(true);
    super.setUp();
    properties = PropertiesUtil.getFromTestPropertiesFile("chunker");
    factory = new TestBrokerConnectionFactory(EXECUTOR, new BrokerConfiguration(properties).buildConnectionConfiguration(), mockConnectionFactory);
    final RabbitMQWorkerMonitor workerMonitor = mock(RabbitMQWorkerMonitor.class);
    lenient().doAnswer(invocation -> {
      ((RabbitMQWorkerObserver) invocation.getArgument(0)).updateWorkers(WorkerType.CONNECT.type().getWorkerQueueName(), 1, 0);
      for (final WorkerType workerType : workerTypes) {
        ((RabbitMQWorkerObserver) invocation.getArgument(0)).updateWorkers(workerType.type().getWorkerQueueName(), 5, 0);
      }
      return null;
    }).when(workerMonitor).addObserver(any());
    builder = new CalculatorBuildDirector(getPMF(), factory, customer, 1, null, workerMonitor);
  }

  @Override
  @AfterEach
  public void tearDown() throws Exception {
    if (calculationJob != null) {
      //clean up any calculations
      for (final Calculation calculation : calculationJob.getCalculations()) {
        CalculationRepository.removeCalculation(getConnection(), calculation.getCalculationId());
      }
    }
    super.tearDown();
    factory.shutdown();
    builder.shutdown();
  }

  /**
   * Runs a test with the given input data, and asserts the results with a preset number of results expected.
   *
   * @param inputData input data for test
   */
  protected void assertTestRun(final CalculationInputData inputData) throws Exception {
    assertTestRun(inputData, 1);
  }

  protected void assertTestRun(final CalculationInputData inputData, final int numberOfResults) throws Exception {
    run(inputData);
    assertResults(numberOfResults);
  }

  protected void run(final CalculationInputData inputData) throws Exception {
    inputData.setQueueName("matters_not");
    final String jobKey = JobRepository.createJob(getConnection(), JobType.CALCULATION, false);

    calculationJob = builder.construct(inputData, new JobIdentifier(jobKey)).calculate();
  }

  protected void assertResults(final int nrOfCalculationsExpected) throws Exception {
    assertEquals(JobState.POST_PROCESSING, JobRepository.getProgress(getConnection(), calculationJob.getJobIdentifier().getJobKey()).getState(),
        "Calculation should end as POST_PROCESSING");
    assertEquals(nrOfCalculationsExpected, calculationJob.getCalculations().size(), "Number of calculations not as expected");
  }

  protected static class TestBrokerConnectionFactory extends BrokerConnectionFactory {
    //does both nox and nh3..
    private double depositionStartValue = 100.0;
    private double resultDecrementFactor = 0.1;
    private final BiFunction<Double, Double, Connection> mockConnectionFactory;

    public TestBrokerConnectionFactory(final ExecutorService executorService, final ConnectionConfiguration connectionConfiguration,
        final BiFunction<Double, Double, Connection> mockConnectionFactory) {
      super(executorService, connectionConfiguration);
      this.mockConnectionFactory = mockConnectionFactory;
    }

    @Override
    protected Connection createNewConnection() throws IOException {
      return mockConnectionFactory.apply(depositionStartValue, resultDecrementFactor);
    }

    void setDepostionStartValue(final double depositionStartValue) {
      this.depositionStartValue = depositionStartValue;
    }

    void setResultDecrementFactor(final double resultDecrementFactor) {
      this.resultDecrementFactor = resultDecrementFactor;
    }
  }
}
