/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.calculator.CalculationInfoRepository;
import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Test class for {@link TotalResultDBUnsafeHandler}.
 */
class TotalResultDBUnsafeHandlerTest extends CalculationRepositoryTestBase {

  @Test
  void testOnSuccess() throws Exception {
    final List<AeriusResultPoint> results = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
    final TotalResultDBUnsafeHandler handler = new TotalResultDBUnsafeHandler(getPMF(), null);

    init(results, handler);
    assertEquals(results.size(), CalculationInfoRepository.getCalculationResults(getConnection(), calculation.getCalculationId()).size(),
        "Should have inserted results");
  }

  public TotalResultDBUnsafeHandler init(final List<AeriusResultPoint> results, final TotalResultDBUnsafeHandler handler) throws Exception {
    handler.onTotalResults(results, calculation.getCalculationId());
    return handler;
  }
}
