/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Random;

import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.calculation.NCACalculationOptionsUtil;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.mock.TestInputBuilder;
import nl.overheid.aerius.db.adms.MetSiteRepository;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.characteristics.ADMSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.source.ADMSRoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.road.CustomVehicles;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Class to create NCA theme test data object.
 */
public class NcaTestInputBuilder extends TestInputBuilder {

  private final MetSiteRepository msscRepository;

  public NcaTestInputBuilder(final MetSiteRepository msscRepository) {
    super(Theme.NCA);
    this.msscRepository = msscRepository;
  }

  public static void mockQuery(final Connection connection, final String queryPart, final String result) throws SQLException {
    final ResultSet rs = mock(ResultSet.class);
    final PreparedStatement stmt = mock(PreparedStatement.class);

    lenient().doReturn(stmt).when(connection).prepareStatement(contains(queryPart));
    lenient().doReturn(rs).when(stmt).executeQuery();
    lenient().doReturn(true).when(rs).next();
    lenient().doReturn(result).when(rs).getString(1);
  }

  @Override
  public CalculationInputData createInputData() {
    final CalculationInputData inputData = super.createInputData();
    inputData.setExportType(ExportType.CALCULATION_UI);
    return inputData;
  }

  @Override
  protected void setScenario(final Scenario scenario) {
    super.setScenario(scenario);
  }

  public void addPoints(final Scenario scenario) {
    final Random random = new Random();

    for (int i = 0; i < 100; i++) {
      addPoint(scenario, i, TestDomain.XCOORD_2 + random.nextInt(2000), TestDomain.YCOORD_2 + random.nextInt(2000));
    }
  }

  public static void addPoint(final Scenario scenario, final int i, final double x, final double y) {
    scenario.getCustomPoints().getFeatures().add(createCalculationPointFeature(i, x, y));
  }

  public static CalculationPointFeature createCalculationPointFeature(final int i, final double x, final double y) {
    final CalculationPointFeature point = new CalculationPointFeature();
    point.setGeometry(new Point(x, y));
    final CustomCalculationPoint calculationPoint = new CustomCalculationPoint();

    calculationPoint.setCustomPointId(i);
    point.setProperties(calculationPoint);
    point.setId(String.valueOf(i));
    calculationPoint.setGmlId(point.getId());
    return point;
  }

  @Override
  protected void setSources(final List<EmissionSourceFeature> sources, final double emissionFactor) {
    sources.add(createRoadEmissionSource(emissionFactor));
  }

  @Override
  protected CalculationSetOptions setCalculationSetOptions(final CalculationSetOptions options) {
    final NCACalculationOptionsUtil util = new NCACalculationOptionsUtil(msscRepository, ADMSVersion.NCA_LATEST);
    try {
      return util.createCalculationSetOptions(options, options.getCalculationMethod() == CalculationMethod.CUSTOM_POINTS, false);
    } catch (final AeriusException e) {
      throw new RuntimeException(e);
    }
  }

  public static EmissionSourceFeature createRoadEmissionSource(final double emissionFactor) {
    final LineString geometryRoad = new LineString();
    geometryRoad.setCoordinates(new double[][] {{TestDomain.XCOORD_2, TestDomain.YCOORD_2}, {TestDomain.XCOORD_2 + 100, TestDomain.YCOORD_2 + 100}});
    final ADMSRoadEmissionSource emissionSource = new ADMSRoadEmissionSource();

    emissionSource.setSectorId(TestDomain.ROAD_SECTOR);
    final CustomVehicles vehicle = new CustomVehicles();
    vehicle.setVehiclesPerTimeUnit(10);
    vehicle.setTimeUnit(TimeUnit.DAY);
    vehicle.getEmissionFactors().put(Substance.NOX, emissionFactor);
    emissionSource.setEmissions(Map.of(Substance.NOX, 10.0));
    emissionSource.setSubSources(List.of(vehicle));
    final EmissionSourceFeature source = TestDomain.getSource("1", geometryRoad, "SomeRoad", emissionSource);

    final ADMSSourceCharacteristics characteristics = new ADMSSourceCharacteristics();
    characteristics.setSourceType(SourceType.ROAD);
    characteristics.setSpecificHeatCapacity(10);
    emissionSource.setCharacteristics(characteristics);
    return source;
  }
}
