/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.mock;

import java.util.List;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.SubReceptorsMode;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.test.TestDomain;

/**
 * Abstract class to build the input data for a test run.
 */
public abstract class TestInputBuilder {

  private final Theme theme;

  protected TestInputBuilder(final Theme theme) {
    this.theme = theme;
  }

  public CalculationInputData createInputData() {
    final CalculationInputData inputData = new CalculationInputData();
    final Scenario scenario = new Scenario(theme);
    setScenario(scenario);
    inputData.setScenario(scenario);
    inputData.setExportType(ExportType.GML_WITH_RESULTS);
    final CalculationSetOptions options = new CalculationSetOptions();
    options.getNcaCalculationOptions().getAdmsOptions().setMetSiteId(0);
    options.getOwN2000CalculationOptions().setSubReceptorsMode(SubReceptorsMode.ENABLED_RECEPTORS_ONLY);
    scenario.setOptions(setCalculationSetOptions(options));
    return inputData;
  }

  protected void setScenario(final Scenario scenario) {
    final ScenarioSituation situation = new ScenarioSituation();
    setSources(situation.getEmissionSourcesList(), 1.0);
    situation.setType(SituationType.PROPOSED);
    situation.setYear(TestDomain.YEAR);
    scenario.getSituations().add(situation);
  }

  protected abstract void setSources(List<EmissionSourceFeature> sources, double emissionFactor);

  protected abstract CalculationSetOptions setCalculationSetOptions(CalculationSetOptions options);
}
