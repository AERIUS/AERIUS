/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.CalculationTestBase;
import nl.overheid.aerius.calculation.base.TwoStepWorkChunker;
import nl.overheid.aerius.calculation.base.WorkChunker;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.shared.domain.calculation.SubReceptorsMode;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FeaturePointToEnginePointUtil;

/**
 * Test class for {@link CustomPointsWorkDistributor}.
 */
@ExtendWith(MockitoExtension.class)
class CustomPointsWorkDistributorTest extends CalculationTestBase {

  @Test
  void testCustomPointsDistributor() throws Exception {
    final AtomicInteger counter = new AtomicInteger();
    final List<AeriusPoint> points = run(counter, SubReceptorsMode.DISABLED);

    assertEquals(points.size(), counter.get(), "Number of points in should be same as passed to handler");
  }

  @Test
  void testCustomPointsDistributorWithSubReceptors() throws Exception {
    final AtomicInteger counter = new AtomicInteger();
    final List<AeriusPoint> points = run(counter, SubReceptorsMode.ENABLED);

    // Number of points at the handler should be the same number of points since no points are close by sources.
    assertEquals(points.size(), counter.get(), "Number of points at the handler should be the same number of points");
  }

  @Test
  void testSourcesExport() throws Exception {
    final CalculationJob calculationJob = createCalculationJob(ExportType.OPS, SubReceptorsMode.DISABLED);
    final List<AeriusPoint> points = FeaturePointToEnginePointUtil.convert(calculationJob.getScenarioCalculations().getCalculationPoints());
    final CustomPointsWorkDistributor distributor = new CustomPointsWorkDistributor(getPMF(), getCategories(getPMF()),
        new GridUtil(new GridSettings(RECEPTOR_GRID_SETTINGS)), points, RECEPTOR_GRID_SETTINGS.getZoomLevel1());
    // Test the CustomPointsWorkDistributor directly because we want to check if it returns all points in a single work chunk.
    distributor.init(calculationJob);
    distributor.prepareWork();
    final WorkPacket<AeriusPoint> workPacket = distributor.work();

    assertEquals(points.size(), workPacket.getPoints().size(), "For an OPS export all points should be returned in a single packet");
    assertFalse(distributor.hasMorePoints(), "Should not have more points");
  }

  private List<AeriusPoint> run(final AtomicInteger counter, final SubReceptorsMode mode) throws Exception {
    final CalculationJob calculationJob = createCalculationJob(ExportType.GML_WITH_RESULTS, mode);
    final List<AeriusPoint> points = FeaturePointToEnginePointUtil.convert(calculationJob.getScenarioCalculations().getCalculationPoints());
    final CustomPointsWorkDistributor distributor = new CustomPointsWorkDistributor(getPMF(), getCategories(getPMF()),
        new GridUtil(new GridSettings(RECEPTOR_GRID_SETTINGS)), points, RECEPTOR_GRID_SETTINGS.getZoomLevel1());

    final WorkChunker handler = new TwoStepWorkChunker<AeriusPoint>(getPMF(), calculationJob, distributor,
        (calculation, jobPacket, calculationPointStore) -> counter.addAndGet(calculationPointStore.size()));

    handler.run((cj, p) -> {});
    return points;
  }

  private CalculationJob createCalculationJob(final ExportType exportType, final SubReceptorsMode mode) throws SQLException, AeriusException {
    final CalculationJob calculationJob = getExampleCalculationJob(exportType);
    calculationJob.setProvider(defaultCalculationEngineProvider);
    calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions().setSubReceptorsMode(mode);
    return calculationJob;
  }
}
