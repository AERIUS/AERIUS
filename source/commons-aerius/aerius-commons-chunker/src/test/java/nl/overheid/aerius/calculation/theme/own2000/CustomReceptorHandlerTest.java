/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.CalculationTestBase;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

class CustomReceptorHandlerTest extends CalculationTestBase {

  @Test
  void testMappingToReceptors() throws SQLException, AeriusException {
    final CalculationJob calculationJob = getExampleCalculationJob();
    final CustomReceptorHandler receptorHandler = new CustomReceptorHandler(RECEPTOR_GRID_SETTINGS, calculationJob);

    for (int receptorId = 1; receptorId <= 200; receptorId++) {
      final boolean includePoint = receptorHandler.includePoint(new AeriusPoint(receptorId, AeriusPoint.AeriusPointType.RECEPTOR));
      if (receptorId <= NUMBER_OF_RECEPTORS) {
        assertTrue(includePoint, "Receptor " + receptorId + " should be included in the calculation");
      } else {
        assertFalse(includePoint, "Receptor " + receptorId + " should not be included in the calculation");
      }
    }
  }

  @Test
  void testRemappingResults() throws SQLException, AeriusException {
    final CalculationJob calculationJob = getExampleCalculationJob();
    final CustomReceptorHandler receptorHandler = new CustomReceptorHandler(RECEPTOR_GRID_SETTINGS, calculationJob);

    final List<AeriusResultPoint> testReceptorResults = new ArrayList<>();
    for (int receptorId = 1; receptorId <= 200; receptorId++) {
      final AeriusResultPoint receptorResult = new AeriusResultPoint(new AeriusPoint(receptorId, AeriusPoint.AeriusPointType.RECEPTOR));
      receptorResult.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, 100.0 * receptorId);
      testReceptorResults.add(receptorResult);
    }

    receptorHandler.postProcessSectorResults(0, testReceptorResults);

    assertEquals(NUMBER_OF_RECEPTORS, testReceptorResults.size(), "Number of result should be equal to the number of calculation points");
    final AeriusResultPoint testResultPoint = testReceptorResults.get(23);
    assertEquals(testResultPoint.getId() * 100.0, testResultPoint.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), "Result should be copied");
    assertEquals(AeriusPoint.AeriusPointType.POINT, testResultPoint.getPointType(), "Result should have point type");
  }

}
