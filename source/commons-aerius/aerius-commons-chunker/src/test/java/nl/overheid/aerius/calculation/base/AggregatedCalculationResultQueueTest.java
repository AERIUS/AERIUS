/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.domain.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * The class <code>CalculationResultQueueTest</code> contains tests for the
 * class {@link AggregatedCalculationResultQueue}.
 */
class AggregatedCalculationResultQueueTest {

  private static final int POINT_ID_1 = 4361388;
  private static final int POINT_X_1 = 130000;
  private static final int POINT_Y_1 = 450000;
  private static final int POINT_ID_2 = 90998;
  private static final int POINT_X_2 = 150000;
  private static final int POINT_Y_2 = 300000;
  private static final int CALCULATION_ID_1 = 1;
  private static final int CALCULATION_ID_2 = 2;
  private static final int SECTOR_ID_1 = 30;
  private static final int SECTOR_ID_2 = 40;

  private AggregatedCalculationResultQueue<Integer> queue;

  @BeforeEach
  void before() {
    queue = new AggregatedCalculationResultQueue<>(AeriusResultPoint::getId);
  }

  /**
   * Run the boolean hasResults(int) method test
   */
  @Test
  void testHasResultsEmpty() {
    assertFalse(queue.hasResults(CALCULATION_ID_1), "No results on empty");
    init(CalculationEngine.OPS, CalculationEngine.OPS);
    assertFalse(queue.hasResults(CALCULATION_ID_1), "Still no results on empty");
  }

  @Test
  void testPutNotInitialized() {
    assertThrows(NullPointerException.class, () -> {
      queue.put(CalculationEngine.OPS, new HashMap<>(), CALCULATION_ID_1);
      fail("Should throw a NullPointerException.");
    });
  }

  /**
   * Run the void put(CalculationResult, int, int) method test
   */
  @Test
  void testPut() throws AeriusException {
    init(CalculationEngine.OPS, CalculationEngine.OPS);
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_1), CALCULATION_ID_1);
    assertFalse(queue.hasResults(CALCULATION_ID_1), "Should not have results with 1 sector for calculation 1");
    assertFalse(queue.hasResults(CALCULATION_ID_2), "Should not have results with 1 sector for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_1);
    assertTrue(queue.hasResults(CALCULATION_ID_1), "Should have results for calculation 1");
    assertFalse(queue.hasResults(CALCULATION_ID_2), "Should not have results for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_1), CALCULATION_ID_2);
    assertTrue(queue.hasResults(CALCULATION_ID_1), "Should have results for calculation 1");
    assertFalse(queue.hasResults(CALCULATION_ID_2), "Should not have results for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_2);
    assertTrue(queue.hasResults(CALCULATION_ID_1), "Should have results for calculation 1");
    assertTrue(queue.hasResults(CALCULATION_ID_2), "Should have results for calculation 2");
  }

  /**
   * Run the void put(CalculationResult, int, int) method test
   */
  @Test
  void testPollCompleteResults() throws AeriusException {
    init(CalculationEngine.ASRM2, CalculationEngine.OPS);
    queue.put(CalculationEngine.ASRM2, createResults(SECTOR_ID_1), CALCULATION_ID_1);
    assertTrue(queue.pollTotalResults(CALCULATION_ID_1).isEmpty(), "Should return empty with 1 sector for calculation 1");
    assertTrue(queue.pollTotalResults(CALCULATION_ID_2).isEmpty(), "Should return empty with 1 sector for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_1);
    assertEquals(2, queue.pollTotalResults(CALCULATION_ID_1).size(), "Should return results for calculation 1");
    assertTrue(queue.pollTotalResults(CALCULATION_ID_2).isEmpty(), "Should return empty with no results for calculation 2");
    queue.put(CalculationEngine.ASRM2, createResults(SECTOR_ID_1), CALCULATION_ID_2);
    assertTrue(queue.pollTotalResults(CALCULATION_ID_1).isEmpty(), "Should return empty for calculation 1");
    assertTrue(queue.pollTotalResults(CALCULATION_ID_2).isEmpty(), "Should return empty with not all results for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_2);
    assertTrue(queue.pollTotalResults(CALCULATION_ID_1).isEmpty(), "Should return empty for calculation 1");
    final List<AeriusResultPoint> results = queue.pollTotalResults(CALCULATION_ID_2);
    assertEquals(2, results.size(), "Should return results for calculation 2");
    assertEquals(6.6, results.get(1).getEmissionResult(EmissionResultKey.NH3_DEPOSITION), 1E-5, "SHould have exptected total deposition");
    assertFalse(queue.isExpectingMoreResults(CALCULATION_ID_1), "All results are in, so no longer expecting more for calculation 1");
    assertFalse(queue.isExpectingMoreResults(CALCULATION_ID_2), "All results are in, so no longer expecting more for calculation 2");
  }

  /**
   * Run the void put(CalculationResult, int, int) method test
   */
  @Test
  void testTooManyResults() throws AeriusException {
    init(CalculationEngine.OPS, CalculationEngine.OPS);
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_1), CALCULATION_ID_1);
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_1);
    assertThrows(AeriusException.class,
        () -> queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_1),
        "Should throw an exception if to many results are added");
  }

  /**
   * Test the behaviour of isExpectingMoreResults()
   */
  @Test
  void testExpectingMoreResults() throws AeriusException {
    init(CalculationEngine.OPS, CalculationEngine.OPS);
    assertFalse(queue.isExpectingMoreResults(CALCULATION_ID_1), "If no results have come in, we don't expect any for calculation 1");
    assertFalse(queue.isExpectingMoreResults(CALCULATION_ID_2), "If no results have come in, we don't expect any for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_1), CALCULATION_ID_1);
    assertTrue(queue.isExpectingMoreResults(CALCULATION_ID_1), "Should expect more results if only one sector results are in for calculation 1");
    assertFalse(queue.isExpectingMoreResults(CALCULATION_ID_2), "If no results have come in, we don't expect any for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_1);
    assertFalse(queue.isExpectingMoreResults(CALCULATION_ID_1), "All results are in, so no longer expecting more for calculation 1");
    assertFalse(queue.isExpectingMoreResults(CALCULATION_ID_2), "If no results have come in, we don't expect any for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_1), CALCULATION_ID_2);
    assertFalse(queue.isExpectingMoreResults(CALCULATION_ID_1), "All results are in, so no longer expecting more for calculation 1");
    assertTrue(queue.isExpectingMoreResults(CALCULATION_ID_2), "Should expect more results if only one sector results are in for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_2);
    assertFalse(queue.isExpectingMoreResults(CALCULATION_ID_1), "All results are in, so no longer expecting more for calculation 1");
    assertFalse(queue.isExpectingMoreResults(CALCULATION_ID_2), "All results are in, so no longer expecting more for calculation 2");
  }

  private AggregatedCalculationResultQueue<Integer> init(final CalculationEngine ceSector1, final CalculationEngine ceSector2) {
    final List<JobPacket> jobPackets = List.of(
        createJobPacket(CALCULATION_ID_1, 1, ceSector1, 2, ceSector2),
        createJobPacket(CALCULATION_ID_2, 1, ceSector1, 2, ceSector2));
    final WorkPacket<AeriusPoint> wp = new WorkPacket<AeriusPoint>(null, jobPackets);
    queue.init(null, List.of(wp));
    return queue;
  }

  private static JobPacket createJobPacket(final int calculationId, final int sectorId11, final CalculationEngine calculationEngine1,
      final int sectorId12, final CalculationEngine calculationEngine2) {
    final GroupedSourcesPacket gsp1 = new GroupedSourcesPacket(sectorId11, calculationEngine1, List.of());
    final GroupedSourcesPacket gsp2 = new GroupedSourcesPacket(sectorId12, calculationEngine1, List.of());
    final List<GroupedSourcesPacket> sourcePackets1 = List.of(gsp1, gsp2);
    return new JobPacket(calculationId, 2020, sourcePackets1);
  }

  private Map<Integer, List<AeriusResultPoint>> createResults(final Integer sectorId) {
    final Map<Integer, List<AeriusResultPoint>> map = new HashMap<>();
    final List<AeriusResultPoint> results = new ArrayList<>();
    results.add(new AeriusResultPoint(POINT_ID_1, 0, AeriusPointType.RECEPTOR, POINT_X_1, POINT_Y_1));
    results.add(new AeriusResultPoint(POINT_ID_2, 0, AeriusPointType.RECEPTOR, POINT_X_2, POINT_Y_2));
    for (final AeriusResultPoint rp : results) {
      rp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 3.3);
    }
    map.put(sectorId, results);
    return map;
  }
}
