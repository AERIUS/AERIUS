/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.conversion.JobPackets.SourceConverterSupplier;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.conversion.OPSSourceConverter;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Crs;
import nl.overheid.aerius.shared.domain.v2.geojson.Crs.CrsContent;
import nl.overheid.aerius.shared.domain.v2.geojson.FeatureCollection;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link AeriusPointsWorkDistributor}.
 */
class AeriusPointsWorkDistributorIT extends BaseDBTest {

  @Test
  void testCollectPoints() throws AeriusException, SQLException {
    // Initialisation of input, 1 emission source
    final Scenario scenario = new Scenario();
    final ScenarioSituation situation = new ScenarioSituation();
    final EmissionSourceFeature source = new EmissionSourceFeature();
    final EmissionSource emissionSource = new GenericEmissionSource();
    emissionSource.setEmissions(Map.of(Substance.NOX, 10.0));
    source.setGeometry(new Point(139785, 455018));
    source.setProperties(emissionSource);
    final Crs mockCrs = mock(Crs.class);
    when(mockCrs.getProperties()).thenReturn(mock(CrsContent.class));
    final FeatureCollection<EmissionSourceFeature> sources = situation.getSources();
    sources.setCrs(mockCrs);
    sources.getFeatures().add(source);

    scenario.getSituations().add(situation);
    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);
    scenarioCalculations.getOptions().getSubstances().add(Substance.NOX);

    final CalculationJob job = new CalculationJob(null, null, scenarioCalculations, null, null);
    final CalculatorOptions calculatorOptions = new CalculatorOptions();
    calculatorOptions.setMaxEngineSources(10);
    calculatorOptions.setMaxCalculationEngineUnits(10);
    calculatorOptions.setMaxReceptors(10);
    job.setCalculatorOptions(calculatorOptions);

    // Distributor
    // 10 chunks (= nr of times new points are added)
    // 10 points per chunk
    // 2 points added for each point to be added.
    // This gives total of 10 * 10 * 2 points to calculate
    final AeriusPointsWorkDistributor distributor = new TestAeriusPointsWorkDistributor(10, 10, 2);
    final List<WorkPacket<AeriusPoint>> workPackets = new ArrayList<>();

    distributor.init(job);
    while (distributor.prepareWork()) {
      workPackets.add(distributor.work());
    }
    assertEquals(20, workPackets.size(), "Not the expected number of chunks: max 10, per chunk, and 200 points");
  }

  private static class TestAeriusPointsWorkDistributor extends AeriusPointsWorkDistributor {

    private final int chunkSize;
    private int nrOfChunks;
    private final int nrOfPointsToAdd;

    public TestAeriusPointsWorkDistributor(final int nrOfChunks, final int chunkSize, final int nrOfPointsToAdd) throws SQLException {
      super(getPMF(), null);
      this.nrOfChunks = nrOfChunks;
      this.chunkSize = chunkSize;
      this.nrOfPointsToAdd = nrOfPointsToAdd;
    }

    @Override
    protected SourceConverterSupplier createSourceConverterSupplier(final CalculationJob calculationJob) {
      return (con, situation) -> new OPSSourceConverter(con, situation);
    }

    @Override
    protected boolean hasMorePoints() {
      return nrOfChunks > 0;
    }

    @Override
    protected List<AeriusPoint> collectPoints() throws AeriusException {
      nrOfChunks--;
      return IntStream.range(0, chunkSize).mapToObj(n -> new AeriusPoint(nrOfChunks*100 + n)).toList();
    }

    @Override
    protected int addPoints(final List<AeriusPoint> pointsToCalculate, final AeriusPoint pointToAdd) throws AeriusException {
      IntStream.range(0, nrOfPointsToAdd).forEach(n -> pointsToCalculate.add(pointToAdd));
      return nrOfPointsToAdd;
    }
  }
}
