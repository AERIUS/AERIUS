/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.GroupedSourcesKey;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.calculation.theme.own2000.OwN2000SourceConverter;
import nl.overheid.aerius.conversion.EngineSourceExpander;
import nl.overheid.aerius.conversion.SourceConversionHelper;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geo.EPSG;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link GridEmissionSourceDigestor}.
 */
class GridEmissionSourceDigestorTest extends BaseDBTest {

  private static final int XCOORD = 114051;
  private static final int YCOORD = 395806;

  private final GridUtil gridUtil = new GridUtil(new GridSettings(RECEPTOR_GRID_SETTINGS));
  private final AggregationDistanceProfilePicker aggregationProfilePicker = new AggregationDistanceProfilePicker();
  private final EngineSourceAggregator<EngineSource, Substance> aggregator = new EngineSourceAggregator<>(
      new GridEngineSourceAggregatable(EPSG.RDNEW.getSrid()));

  private GridEmissionSourceDigestor sourceDigestor;
  private DefaultCalculationEngineProvider defaultCalculationEngineProvider;

  @BeforeEach
  void before() throws SQLException {
    sourceDigestor = new GridEmissionSourceDigestor(gridUtil, aggregator, aggregationProfilePicker);

    try (Connection con = getConnection()) {
      defaultCalculationEngineProvider = new DefaultCalculationEngineProvider(SectorRepository.getSectorProperties(con));
    }
  }

  @Test
  void testDigest() throws SQLException, AeriusException {
    final ScenarioSituation situation = createSituation();
    final CalculationJob job = createCalculationJob(situation);
    final List<Substance> substances = createEmissionValueKeyList();
    final SectorCategories sectorCategories = getCategories(getPMF());
    final SourceConversionHelper conversionHelper = new SourceConversionHelper(getPMF(), sectorCategories, 2021);
    final List<GroupedSourcesPacket> expanded;

    try (Connection con = getConnection()) {
      expanded = EngineSourceExpander.toEngineSourcesByGroup(con,
          new OwN2000SourceConverter(getConnection(), defaultCalculationEngineProvider, situation), conversionHelper, situation, substances, true);
    }
    final SourcesStore sourcesStore = sourceDigestor.digest(job.getCalculationSetOptions(), substances, expanded);
    final int cell0 = getCellIdFromSource(job);
    final Map<GroupedSourcesKey, Collection<EngineSource>> sourceGridIds = sourcesStore.getSourcesFor(cell0);
    assertEquals(3, sourceGridIds.size(), "Should contain 3 sectors");
    int count = 0;
    for (final Entry<GroupedSourcesKey, Collection<EngineSource>> entry : sourceGridIds.entrySet()) {
      count += entry.getValue().size();
    }
    assertEquals(9, count, "Should contain 9 engine sources");
  }

  private int getCellIdFromSource(final CalculationJob job) {
    return gridUtil.getCellFromPosition(new SridPoint(XCOORD, YCOORD), gridUtil.getGridSettings().getZoomLevelMax());
  }

  private List<Substance> createEmissionValueKeyList() {
    return List.of(Substance.NH3, Substance.NOX, Substance.NO2);
  }

  private ScenarioSituation createSituation() throws SQLException, AeriusException {
    final TestDomain testDomain = new TestDomain(getCategories(getPMF()));
    final ScenarioSituation situation = new ScenarioSituation();
    situation.getEmissionSourcesList().add(getSource("1", somePoint(), "SomeFarmSource", testDomain.getFarmEmissionSource()));
    situation.getEmissionSourcesList().add(getSource("2", someLineString(), "SomeRoadSource", testDomain.getSRM2EmissionSource()));
    return situation;
  }

  private CalculationJob createCalculationJob(final ScenarioSituation situation) {
    final Scenario scenario = new Scenario(Theme.OWN2000);
    scenario.getSituations().add(situation);
    final CalculationSetOptions options = new CalculationSetOptions();
    options.getSubstances().add(Substance.NH3);
    options.getSubstances().add(Substance.NOX);
    options.getSubstances().add(Substance.NO2);
    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);
    final int calculationId = 1;
    scenarioCalculations.getCalculations().forEach(calculation -> calculation.setCalculationId(calculationId));
    scenario.setOptions(options);
    final CalculationJob cj = new CalculationJob(new JobIdentifier(UUID.randomUUID().toString()), null, scenarioCalculations, null, "queue");
    cj.setProvider(defaultCalculationEngineProvider);
    return cj;
  }

  private Geometry somePoint() {
    return new nl.overheid.aerius.shared.domain.v2.geojson.Point(XCOORD, YCOORD);
  }

  private Geometry someLineString() {
    final int yCoord1 = YCOORD + 100;
    final LineString lineString = new LineString();
    lineString.setCoordinates(new double[][] {
      {XCOORD, YCOORD},
      {(XCOORD + 100), yCoord1},
      {XCOORD, YCOORD}
    });
    return lineString;
  }

  private <E extends EmissionSource> EmissionSourceFeature getSource(final String id, final Geometry geometry, final String label, final E source)
      throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setId(id);
    feature.setGeometry(geometry);
    feature.setProperties(source);
    source.setLabel(label);
    return feature;
  }
}
