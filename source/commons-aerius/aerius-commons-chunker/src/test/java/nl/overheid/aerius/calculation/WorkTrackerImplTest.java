/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.Thread.State;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;

/**
 * Test class for {@link WorkTrackerImpl}.
 */
class WorkTrackerImplTest {

  private WorkTracker workTracker;
  private Semaphore startSync;
  private Semaphore released;
  private CalculationState finishState;

  @BeforeEach
  void before() {
    startSync = new Semaphore(0);
    released = new Semaphore(0);
    workTracker = new WorkTrackerImpl();
  }

  /**
   * Test normal flow.
   */
  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testWorkFinished() throws InterruptedException {
    final Thread thread = startNewThread();
    while (thread.getState() != State.WAITING) {}
    assertEquals(0, released.availablePermits(), "Still waiting to finish");
    workTracker.decrement();
    released.acquire();
    assertEquals(0, released.availablePermits(), "Should be finished");
    assertSame(CalculationState.COMPLETED, finishState, "Should have COMPLETED state");
  }

  /**
   * Tests if correctly releases waitForFinished() when all tasks finish before waitForFinished is called.
   */
  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testWorkFinishedBeforeWait() throws InterruptedException {
    createNewThread().start();
    released.acquire();
    assertEquals(0, released.availablePermits(), "Still waiting to finish");
    assertSame(CalculationState.COMPLETED, finishState, "Should have COMPLETED state");
  }

  /**
   * Tests if correctly releases waitForFinished() when all tasks finish before waitForFinished is called.
   */
  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testWorkFinishedBeforeWait2() throws InterruptedException {
    workTracker.increment();
    final Thread thread = startNewThread();
    while(thread.getState() != State.WAITING) {}
    workTracker.decrement();
    assertTrue(((WorkTrackerImpl) workTracker).isWaiting(), "Should still be waiting");
    assertEquals(0, released.availablePermits(), "Still waiting to finish");
    workTracker.decrement();
    released.acquire();
    assertEquals(0, released.availablePermits(), "Should be finished");
    assertSame(CalculationState.COMPLETED, finishState, "Should have COMPLETED state");
  }

  /**
   * Test when exception is given before waitForFinished() is called.
   */
  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testExceptionBeforeWait() throws InterruptedException {
    final IllegalArgumentException theException = new IllegalArgumentException("");
    workTracker.cancel(theException);
    createNewThread().start();
    assertExceptionOnRelease(theException);
  }

  /**
   * Test when exception is given after waitForFinished() is called.
   */
  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testException() throws InterruptedException {
    startNewThread();
    assertEquals(0, released.availablePermits(), "Still waiting to finish");
    final IllegalArgumentException theException = new IllegalArgumentException("1");
    workTracker.cancel(theException);
    workTracker.cancel(new TaskCancelledException(theException)); // this exception should not be reported.
    assertExceptionOnRelease(theException);
  }

  /**
   * Tests if waitForFinish is correctly handled when after an exception an onCompleted is called.
   */
  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testUpdateAfterException() throws InterruptedException {
    startNewThread();
    final IllegalArgumentException theException = new IllegalArgumentException("");
    workTracker.cancel(theException);
    workTracker.decrement();
    assertExceptionOnRelease(theException);
  }

  private Thread startNewThread() throws InterruptedException {
    workTracker.increment();
    final Thread thread = createNewThread();
    thread.start();
    startSync.acquire();
    assertEquals(0, released.availablePermits(), "Nothing done should not finished");
    return thread;
  }

  private Thread createNewThread() {
    return new Thread(() -> {
      try {
        startSync.release();
        finishState = workTracker.waitForCompletion();
        released.release();
      } catch (final InterruptedException e) {
      }
    });
  }

  private void assertExceptionOnRelease(final IllegalArgumentException theException) throws InterruptedException {
    released.acquire();
    assertEquals(0, released.availablePermits(), "Should finish on Exception ");
    assertSame(CalculationState.CANCELLED, finishState, "Should have CANCELLED state");
    assertSame(theException, workTracker.getException(), "Should have exception");
  }
}
