/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link EmissionSourceListSTRTree}.
 */
class EmissionSourceListSTRTreeTest {

  private static final Random RANDOM = new Random();

  @Test
  void testFindClosestDistancePoint() throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final GenericEmissionSource source = new GenericEmissionSource();
    feature.setProperties(source);
    feature.setGeometry(new Point(10000, 10000));
    final EmissionSourceListSTRTree tree = creatTreeWithOneSource(feature);
    final AeriusPoint point = new AeriusPoint(10000, 20000);
    assertEquals(10000, tree.findShortestDistance(point), 0, "Distance should be 10000");
  }

  @Test
  void testFindClosestDistanceLine() throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final GenericEmissionSource source = new GenericEmissionSource();
    feature.setProperties(source);
    final LineString geometry = new LineString();
    geometry.setCoordinates(new double[][] {{1000, 1000}, {1000, 2000}});
    feature.setGeometry(geometry);
    final EmissionSourceListSTRTree tree = creatTreeWithOneSource(feature);
    final AeriusPoint point = new AeriusPoint(1500, 1500);
    assertEquals(500, tree.findShortestDistance(point), 0, "Distance should be 500");
  }

  @Test
  void testFindClosestDistancePolygon() throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final GenericEmissionSource source = new GenericEmissionSource();
    feature.setProperties(source);
    final Polygon polygon = new Polygon();
    polygon.setCoordinates(new double[][][] {{
      {100, 100},
      {100, 200},
      {200, 200},
      {200, 100},
      {100, 100}
    }});
    feature.setGeometry(polygon);
    final EmissionSourceListSTRTree tree = creatTreeWithOneSource(feature);
    final AeriusPoint point1 = new AeriusPoint(50, 150);
    assertEquals(50, tree.findShortestDistance(point1), 0, "Distance outside polygon should be 50");
    final AeriusPoint point2 = new AeriusPoint(150, 150);
    assertEquals(0, tree.findShortestDistance(point2), 0, "Distance inside polygon should be 0");
  }

  @Test
  void testFindClosestDistanceRandomPoints() throws AeriusException {
    final int numberOfReceptors = 100;
    final int range = 1000;
    final int x1 = 10000;
    final int y1 = 10000;
    final List<EmissionSourceFeature> list = createPoints(numberOfReceptors, range, x1, y1);
    final EmissionSourceListSTRTree tree = createTree(list);
    final AeriusPoint point = new AeriusPoint(x1, y1);
    assertNotEquals(0, tree.findShortestDistance(point), 0.1, "Distance should be 0");
  }

  private EmissionSourceListSTRTree creatTreeWithOneSource(final EmissionSourceFeature source) throws AeriusException {
    final List<EmissionSourceFeature> list = new ArrayList<>();
    list.add(source);
    return createTree(list);
  }

  private EmissionSourceListSTRTree createTree(final List<EmissionSourceFeature> list) throws AeriusException {
    final EmissionSourceListSTRTree tree = new EmissionSourceListSTRTree();
    tree.add(list);
    tree.build();
    return tree;
  }

  private List<EmissionSourceFeature> createPoints(final int count, final int maxRange, final int offsetX, final int offsetY) {
    final List<EmissionSourceFeature> esl = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      final EmissionSourceFeature feature = new EmissionSourceFeature();
      feature.setGeometry(new Point(
          offsetX - (offsetX / 2) + RANDOM.nextInt(maxRange),
          offsetY - (offsetY / 2) + RANDOM.nextInt(maxRange)
          ));
      final GenericEmissionSource source = new GenericEmissionSource();
      feature.setProperties(source);
      esl.add(feature);
    }
    return esl;
  }
}
