/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.archive.ArchiveProxy;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.adms.MetSiteRepository;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.characteristics.TimeVaryingProfiles;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link NCANatura2kWorkDistributor} and .
 *
 * Note this test runs on the NL database. This should not be an issue as it's just data for the processes that uses the same data structure.
 */
@ExtendWith(MockitoExtension.class)
class NCAWorkDistributorTest {

  private static final int XCOORD_PL = 192889;
  private static final int YCOORD_PL = 471525;
  private static final String POLYGON = "POLYGON((190889 470525,194889 470525,194889 472525,190889 472525,190889 470525))";

  private @Mock PMF pmf;
  private @Mock Connection connection;
  private @Mock Natura2kReceptorStore natura2kReceptorStore;
  private @Mock TimeVaryingProfiles timeVaryingProfiles;
  private @Mock RoadEmissionCategories roadEmissionCategories;
  private @Mock SectorCategories sectorCategories;
  private @Mock ReceptorGridSettings receptorGridSettings;
  private @Mock JobIdentifier jobIdentifier;
  private @Mock CalculationSetOptions calculationSetOptions;
  private @Mock MetSiteRepository msscRepository;
  private @Mock NCABackgroundSupplier ncaBackgroundSupplier;
  private @Mock ArchiveProxy archiveProxy;

  @BeforeEach
  void before() throws SQLException {
    final HexagonZoomLevel hexagonZoomLevel = new HexagonZoomLevel(1, 1000);
    lenient().doReturn(hexagonZoomLevel).when(receptorGridSettings).getZoomLevel1();
    doReturn(connection).when(pmf).getConnection();
    NcaTestInputBuilder.mockQuery(connection, "system.constants_view", CharacteristicsType.ADMS.getName());
    NcaTestInputBuilder.mockQuery(connection, "relevant_habitat_areas", POLYGON);
    doReturn(timeVaryingProfiles).when(sectorCategories).getTimeVaryingProfiles();
    lenient().doReturn(roadEmissionCategories).when(sectorCategories).getRoadEmissionCategories();
  }

  @Test
  void testSubReceptor() throws SQLException, AeriusException {
    assertRun(cj -> {}, w -> {}, 1);
  }

  @Test
  void testMeteoYears() throws SQLException, AeriusException {
    assertRun(
        cj -> cj.getCalculationSetOptions().getNcaCalculationOptions().getAdmsOptions().setMetYears(List.of("2017", "2024")),
        w -> {
          assertMeteoYear(w, 0, 2017);
          assertMeteoYear(w, 1, 2024);
        },
        2);
  }

  private void assertMeteoYear(final WorkPacket<AeriusPoint> work, final int index, final int year) {
    assertEquals("ADMS-" + year, work.getJobPackets().get(index).getSourcesPackets().get(0).getKey().engineDataKey().key(),
        "Not the expected engine data key");
  }

  void assertRun(final Consumer<CalculationJob> prepareOptions, final Consumer<WorkPacket<AeriusPoint>> asserter, final int numberOfPackets)
      throws SQLException, AeriusException {
    // mock n2k points
    final List<Entry<Integer, Integer>> orderN2KLists = List.of(Map.of(1, 1).entrySet().iterator().next());
    doReturn(orderN2KLists).when(natura2kReceptorStore).order(any());
    final List<AeriusPoint> receptors = List.of(new AeriusPoint(XCOORD_PL + 40, YCOORD_PL + 40));
    doReturn(receptors).when(natura2kReceptorStore).getReceptors(any());

    final CalculationJob calculationJob = mockCalculationJob();
    prepareOptions.accept(calculationJob);

    // Initialize and test the distributor
    final NCANatura2kWorkDistributor distributor = new NCANatura2kWorkDistributor(pmf, natura2kReceptorStore, sectorCategories, receptorGridSettings,
        ncaBackgroundSupplier, archiveProxy);
    distributor.init(calculationJob);
    distributor.prepareWork();

    final WorkPacket<AeriusPoint> work = distributor.work();
    assertEquals(385, work.getPoints().size(), "Should have 1 point with subreceptor points at level 4, minus points within 2 meter");
    assertEquals(numberOfPackets, work.getJobPackets().size(), "Not the expected number of work packets");
    verify(ncaBackgroundSupplier).setBackgroundNOx(any(ADMSCalculationPoint.class), any());
    asserter.accept(work);
  }

  @Test
  void testNCAPointsWorkDistributor() throws SQLException, AeriusException {
    final CalculationPointFeature point1 = NcaTestInputBuilder.createCalculationPointFeature(1, 1E4, 1E5);
    final CalculationPointFeature point2 = NcaTestInputBuilder.createCalculationPointFeature(2, 1E5, 1E4);
    final List<CalculationPointFeature> points = List.of(point1, point2);
    final CalculationJob calculationJob = mockCalculationJob();

    final NCAPointsWorkDistributor distributor = new NCAPointsWorkDistributor(pmf, points, sectorCategories, ncaBackgroundSupplier);
    distributor.init(calculationJob);
    distributor.prepareWork();

    final WorkPacket<AeriusPoint> work = distributor.work();
    assertNotNull(work, "Should have something to do");
    assertEquals(2, work.getPoints().size(), "Should have 2 points");
    verify(ncaBackgroundSupplier, times(2)).setBackgroundNOx(any(List.class), any());
  }

  private CalculationJob mockCalculationJob() {
    // mock source data
    final NcaTestInputBuilder builder = new NcaTestInputBuilder(msscRepository);
    final CalculationInputData inputData = builder.createInputData();
    final EmissionSourceFeature source = TestDomain.getSource("10", new Point(XCOORD_PL, YCOORD_PL), "SomeSource", new GenericEmissionSource());
    source.getProperties().setEmissions(Map.of(Substance.NH3, 100D));
    inputData.getScenario().getSituations().get(0).getEmissionSourcesList().add(source);
    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(inputData.getScenario());

    return new CalculationJob(jobIdentifier, "test", scenarioCalculations, ExportType.ADMS, "test");
  }
}
