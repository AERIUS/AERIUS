/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor.RabbitMQWorkerObserver;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Test class for {@link DynamicWorkSemaphore}.
 */
class DynamicWorkSemaphoreTest {

  private static final String WORKER_QUEUE_NAME = WorkerType.OPS.type().getWorkerQueueName();
  private static final String CHUNKER_QUEUE_NAME = WorkerType.CONNECT.type().getWorkerQueueName();

  private RabbitMQWorkerMonitor monitor;

  private DynamicWorkSemaphore semaphore;
  private RabbitMQWorkerObserver observer;

  @BeforeEach
  void before() {
    monitor = Mockito.mock(RabbitMQWorkerMonitor.class);
    semaphore = new DynamicWorkSemaphore(monitor, WorkerType.CONNECT);
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        observer = invocation.getArgument(0, RabbitMQWorkerObserver.class);
        return null;
      }
    }).when(monitor).addObserver(any());
    // Init the mock observer
    semaphore.availablePermits(WORKER_QUEUE_NAME);
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testAcquireAndRelease() throws InterruptedException {
    assertEquals(0, semaphore.availablePermits(WORKER_QUEUE_NAME), "The semaphore should be exhausted");
    semaphore.release(WORKER_QUEUE_NAME);
    assertEquals(1, semaphore.availablePermits(WORKER_QUEUE_NAME), "There should be 1 slot available");
    semaphore.acquire(WORKER_QUEUE_NAME);
    assertEquals(0, semaphore.availablePermits(WORKER_QUEUE_NAME), "The semaphore should be exhausted again");
    assertNotNull(observer, "After release we should get new aquire, and have a observer.");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testAddingNewSlot() throws InterruptedException {
    updateChunkerQueueUtilisation(1);
    assertEquals(0, semaphore.availablePermits(WORKER_QUEUE_NAME), "The semaphore should be exhausted");
    // This should release a new slot
    updateWorkerQueueSize(1, 0);
    assertEquals(2, semaphore.availablePermits(WORKER_QUEUE_NAME), "There should be 2 slots available");
    semaphore.acquire(WORKER_QUEUE_NAME);
    semaphore.acquire(WORKER_QUEUE_NAME);
    assertEquals(0, semaphore.availablePermits(WORKER_QUEUE_NAME), "The semaphore should be exhausted again");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testChangesInWorkersUtilisationSlots() throws InterruptedException {
    updateChunkerQueueUtilisation(1);
    updateWorkerQueueSize(2, 0);
    assertEquals(2, semaphore.availablePermits(WORKER_QUEUE_NAME), "There should be a 2 slots available");
    updateWorkerQueueSize(0, 0);
    assertEquals(0, semaphore.availablePermits(WORKER_QUEUE_NAME), "The semaphore should be exhausted");
    updateWorkerQueueSize(1, 0);
    assertEquals(2, semaphore.availablePermits(WORKER_QUEUE_NAME), "There should only be 2 slots available");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testChangesInChunkerWorkers() throws InterruptedException {
    // Set 1 chunker busy, with 10 free model workers.
    updateChunkerQueueUtilisation(1);
    updateWorkerQueueSize(10, 0);
    updateWorkerQueueSize(10, 0);
    assertEquals(10, semaphore.availablePermits(WORKER_QUEUE_NAME), "There should be a 10 slots available");
    // Set 7 workers are busy
    updateWorkerQueueSize(10, 7);
    assertEquals(3, semaphore.availablePermits(WORKER_QUEUE_NAME), "When 7 workers busy, only 3 slots should be availble");
    // Set all workers to be free,
    updateWorkerQueueSize(10, 0);
    // Set 2 chunkers to be workers.
    updateChunkerQueueUtilisation(2);
    assertEquals(5, semaphore.availablePermits(WORKER_QUEUE_NAME), "With 2 chunkers only halve (5) of slots should be available");
    // Set to 1 chunker
    updateChunkerQueueUtilisation(1);
    assertEquals(10, semaphore.availablePermits(WORKER_QUEUE_NAME), "With 1 chunker busy, all 10 slots should be available");
  }

  private void updateWorkerQueueSize(final int size, final int utilisation) {
    observer.updateWorkers(WORKER_QUEUE_NAME, size, utilisation);
  }

  private void updateChunkerQueueUtilisation(final int utilisation) {
    observer.updateWorkers(CHUNKER_QUEUE_NAME, 0, utilisation);
  }
}
