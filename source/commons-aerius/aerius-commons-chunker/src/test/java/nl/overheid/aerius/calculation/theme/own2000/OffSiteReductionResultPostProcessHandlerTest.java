/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;

class OffSiteReductionResultPostProcessHandlerTest {

  private static final int OFF_SITE_REDUCTION_CALCULATION_ID = 21;
  private static final double EXAMPLE_NETTING_FACTOR = 0.4;

  @Test
  void testPostProcessSectorResults() {
    final ScenarioCalculations scenarioCalculations = mockCorrectScenario();
    final OffSiteReductionResultPostProcessHandler handler = new OffSiteReductionResultPostProcessHandler(scenarioCalculations);

    final AeriusResultPoint resultPoint = mock(AeriusResultPoint.class);
    final EmissionResults emissionResults = mock(EmissionResults.class);
    when(resultPoint.getEmissionResults()).thenReturn(emissionResults);

    final Map<EmissionResultKey, Double> resultMap = new HashMap<>();
    resultMap.put(EmissionResultKey.NOX_DEPOSITION, 10.0);
    resultMap.put(EmissionResultKey.NOX_CONCENTRATION, 12.5);
    when(emissionResults.entrySet()).thenReturn(resultMap.entrySet());

    handler.postProcessSectorResults(OFF_SITE_REDUCTION_CALCULATION_ID, List.of(resultPoint));

    assertEquals(6.0, resultMap.get(EmissionResultKey.NOX_DEPOSITION).doubleValue(), 1E-4, "Result should have been adjusted accordingly");
    assertEquals(12.5 * (1 - EXAMPLE_NETTING_FACTOR), resultMap.get(EmissionResultKey.NOX_CONCENTRATION).doubleValue(),
        1E-4, "Result should have been adjusted accordingly");
  }

  @Test
  void testPostProcessSectorResultsOtherCalculation() {
    final ScenarioCalculations scenarioCalculations = mockCorrectScenario();
    final OffSiteReductionResultPostProcessHandler handler = new OffSiteReductionResultPostProcessHandler(scenarioCalculations);

    final AeriusResultPoint resultPoint = mock(AeriusResultPoint.class);
    final EmissionResults emissionResults = mock(EmissionResults.class);
    when(resultPoint.getEmissionResults()).thenReturn(emissionResults);

    final Map<EmissionResultKey, Double> resultMap = new HashMap<>();
    resultMap.put(EmissionResultKey.NOX_DEPOSITION, 10.0);
    resultMap.put(EmissionResultKey.NOX_CONCENTRATION, 12.5);
    when(emissionResults.entrySet()).thenReturn(resultMap.entrySet());

    handler.postProcessSectorResults(999, List.of(resultPoint));

    assertEquals(10.0, resultMap.get(EmissionResultKey.NOX_DEPOSITION).doubleValue(), 1E-4, "Result should have been adjusted accordingly");
    assertEquals(12.5, resultMap.get(EmissionResultKey.NOX_CONCENTRATION).doubleValue(), 1E-4, "Result should have been adjusted accordingly");
  }

  @Test
  void testCanApplyCorrect() {
    final ScenarioCalculations scenarioCalculations = mockCorrectScenario();
    assertTrue(OffSiteReductionResultPostProcessHandler.canApply(scenarioCalculations), "Our test case should be applicable");
  }

  @Test
  void testCanApplyEmptyScenario() {
    final ScenarioCalculations scenarioCalculations = mock(ScenarioCalculations.class);

    final Scenario scenario = mock(Scenario.class);
    when(scenarioCalculations.getScenario()).thenReturn(scenario);

    when(scenario.getSituations()).thenReturn(Collections.emptyList());

    assertFalse(OffSiteReductionResultPostProcessHandler.canApply(scenarioCalculations), "Empty scenario should not be applicable");
  }

  @Test
  void testCanApplyOthertypeSituations() {
    final ScenarioCalculations scenarioCalculations = mock(ScenarioCalculations.class);

    final Scenario scenario = mock(Scenario.class);
    when(scenarioCalculations.getScenario()).thenReturn(scenario);

    final List<ScenarioSituation> situations = new ArrayList<>();
    when(scenario.getSituations()).thenReturn(situations);
    for (final SituationType situationType : SituationType.values()) {
      if (situationType != SituationType.OFF_SITE_REDUCTION) {
        final ScenarioSituation situation = mock(ScenarioSituation.class);
        when(situation.getType()).thenReturn(situationType);
        situations.add(situation);
      }
    }

    assertFalse(OffSiteReductionResultPostProcessHandler.canApply(scenarioCalculations), "Other situation types should not be applicable");
  }

  @Test
  void testCanApplyWithoutNettingFactor() {
    final ScenarioCalculations scenarioCalculations = mock(ScenarioCalculations.class);

    final Scenario scenario = mock(Scenario.class);
    when(scenarioCalculations.getScenario()).thenReturn(scenario);

    final List<ScenarioSituation> situations = new ArrayList<>();
    when(scenario.getSituations()).thenReturn(situations);

    final ScenarioSituation offSiteReductionSituation = mock(ScenarioSituation.class);
    when(offSiteReductionSituation.getType()).thenReturn(SituationType.OFF_SITE_REDUCTION);
    when(offSiteReductionSituation.getNettingFactor()).thenReturn(null);

    situations.add(offSiteReductionSituation);

    assertFalse(OffSiteReductionResultPostProcessHandler.canApply(scenarioCalculations),
        "null netting factor shouldn't be applicable (should be handled earlier)");
  }

  @Test
  void testCanApplyZeroNettingFactor() {
    final ScenarioCalculations scenarioCalculations = mock(ScenarioCalculations.class);

    final Scenario scenario = mock(Scenario.class);
    when(scenarioCalculations.getScenario()).thenReturn(scenario);

    final List<ScenarioSituation> situations = new ArrayList<>();
    when(scenario.getSituations()).thenReturn(situations);

    final ScenarioSituation offSiteReductionSituation = mock(ScenarioSituation.class);
    when(offSiteReductionSituation.getType()).thenReturn(SituationType.OFF_SITE_REDUCTION);
    when(offSiteReductionSituation.getNettingFactor()).thenReturn(0.0);

    situations.add(offSiteReductionSituation);

    assertFalse(OffSiteReductionResultPostProcessHandler.canApply(scenarioCalculations), "0.0 shouldn't be applicable (no effect)");
  }

  private ScenarioCalculations mockCorrectScenario() {
    final ScenarioCalculations scenarioCalculations = mock(ScenarioCalculations.class);

    final Scenario scenario = mock(Scenario.class);
    when(scenarioCalculations.getScenario()).thenReturn(scenario);

    final List<ScenarioSituation> situations = new ArrayList<>();
    when(scenario.getSituations()).thenReturn(situations);

    final ScenarioSituation proposedSituation = mock(ScenarioSituation.class);
    when(proposedSituation.getType()).thenReturn(SituationType.PROPOSED);

    final ScenarioSituation offSiteReductionSituation = mock(ScenarioSituation.class);
    when(offSiteReductionSituation.getType()).thenReturn(SituationType.OFF_SITE_REDUCTION);
    when(offSiteReductionSituation.getNettingFactor()).thenReturn(EXAMPLE_NETTING_FACTOR);

    situations.add(proposedSituation);
    situations.add(offSiteReductionSituation);

    final Calculation calculation = mock(Calculation.class);
    when(calculation.getCalculationId()).thenReturn(OFF_SITE_REDUCTION_CALCULATION_ID);
    when(scenarioCalculations.getCalculationForSituation(offSiteReductionSituation)).thenReturn(Optional.of(calculation));

    return scenarioCalculations;
  }

}
