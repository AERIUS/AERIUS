/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepositoryBean;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.OPSSubReceptor;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link WorkStatisticsUtil}.
 */
@ExtendWith(MockitoExtension.class)
class WorkStatisticsUtilTest {

  private @Mock JobRepositoryBean jobRepositoryBean;
  private @Mock CalculationRepositoryBean calculationRepositoryBean;
  private @Mock PMF pmf;
  private @Mock Connection connection;
  private @Captor ArgumentCaptor<Integer> pointsCaptor;
  private @Captor ArgumentCaptor<Integer> sourcesCaptor;
  private @Captor ArgumentCaptor<Integer> substancesCaptor;

  private WorkStatisticsUtil<AeriusPoint> util;
  private CalculationJob calculationJob;

  @BeforeEach
  void beforeEach() throws SQLException {
    final Scenario scenario = new Scenario();
    final ScenarioCalculations sc = new ScenarioCalculationsImpl(scenario);
    calculationJob = new CalculationJob(new JobIdentifier("jobKey"), null, sc, null, null);
    util = new WorkStatisticsUtil<>(pmf, jobRepositoryBean, calculationRepositoryBean);
    doReturn(connection).when(pmf).getConnection();
  }

  @Test
  void testInitNrOfPoints() throws AeriusException, SQLException {
    init();
    verify(jobRepositoryBean).setNumberOfPoints(eq(connection), any(), pointsCaptor.capture());

    final int expectedNumberOfPoints = (10 + 20) * 2/* calculations */ * 2/* calculation engines */ * (1 + 1) /* normal receptors + sub receptors*/;
    assertEquals(expectedNumberOfPoints, pointsCaptor.getValue(), "Not the expected number of points counted");
  }

  @Test
  void testInitNrOfSources() throws AeriusException, SQLException {
    init();
    verify(calculationRepositoryBean).setNumberOfSources(eq(connection), anyInt(), sourcesCaptor.capture(), anyInt());

    final int expectedNumberOfSources = 3 /* per group */ * 4 /* number of groups */;
    assertEquals(expectedNumberOfSources , sourcesCaptor.getValue(), "Not the expected number of sources counted");
  }

  @Test
  void testInitNrOfSourcesAndSubstances() throws AeriusException, SQLException {
    final List<Substance> substances = calculationJob.getCalculationSetOptions().getSubstances();
    substances.add(Substance.NOX);
    substances.add(Substance.NH3);
    init();
    verify(calculationRepositoryBean).setNumberOfSources(eq(connection), anyInt(), anyInt(), substancesCaptor.capture());

    assertEquals(2, substancesCaptor.getValue(), "Not the expected number of substances counted");
  }

  @Test
  void testInitNoPackets() throws AeriusException, SQLException {
    final List<WorkPacket<AeriusPoint>> packets = List.of();
    util.init(calculationJob, packets);
    verify(jobRepositoryBean).setNumberOfPoints(eq(connection), any(), pointsCaptor.capture());

    assertEquals(0, pointsCaptor.getValue(), "Should be no points counted");
  }

  private void init() throws AeriusException {
    final List<WorkPacket<AeriusPoint>> packets = List.of(createWorkPacket(10, 1), createWorkPacket(10, 2), createWorkPacket(20, 1),
        createWorkPacket(20, 2));
    util.init(calculationJob, packets);
  }

  private WorkPacket<AeriusPoint> createWorkPacket(final int nrOfPoints, final int calculationId) {
    final List<AeriusPoint> points = Stream.of(
        IntStream.range(0, nrOfPoints).mapToObj(i -> new AeriusPoint(i)),
        IntStream.range(0, nrOfPoints).mapToObj(i -> (AeriusPoint) new OPSSubReceptor(new AeriusPoint(i), AeriusPointType.SUB_RECEPTOR, i)),
        IntStream.range(0, nrOfPoints).mapToObj(i -> (AeriusPoint) new OPSSubReceptor(new AeriusPoint(i), AeriusPointType.RECEPTOR, i)),
        IntStream.range(0, nrOfPoints).mapToObj(i -> (AeriusPoint) new OPSSubReceptor(new AeriusPoint(i), AeriusPointType.POINT, i)))
        .flatMap(Function.identity()).toList();
    final Collection<EngineSource> sources = IntStream.range(0, 3).mapToObj(i -> (EngineSource) new EngineEmissionSource()).toList();
    final List<GroupedSourcesPacket> sourcesPackets = List.of(
        new GroupedSourcesPacket(1, CalculationEngine.OPS, sources),
        new GroupedSourcesPacket(2, CalculationEngine.OPS, sources),
        new GroupedSourcesPacket(3, CalculationEngine.ASRM2, sources),
        new GroupedSourcesPacket(4, CalculationEngine.ASRM2, sources));
    final JobPacket jobPacket = new JobPacket(1, 2020, sourcesPackets);
    final List<JobPacket> jobPackets = List.of(jobPacket);
    return new WorkPacket<AeriusPoint>(points, jobPackets);
  }
}
