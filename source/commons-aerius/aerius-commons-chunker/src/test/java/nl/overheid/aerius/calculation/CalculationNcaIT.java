/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.theme.nca.NCAMockConnection;
import nl.overheid.aerius.calculation.theme.nca.NcaTestInputBuilder;
import nl.overheid.aerius.db.adms.MetSiteRepository;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Integration test to do a complete calculation NCA.
 */
@ExtendWith(MockitoExtension.class)
class CalculationNcaIT extends CalculationITBase {

  private static final double ROAD_LOCAL_FNO2 = 0.5;

  private @Mock MetSiteRepository msscRepository;

  private NCAMockConnection mockConnection;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp((dsv, rdf) -> mockConnection = new NCAMockConnection(dsv, rdf), AeriusCustomer.JNCC, WorkerType.ADMS);
  }

  @Test
  @Timeout(value = 120, unit = TimeUnit.SECONDS)
  void testRunSingle() throws Exception {
    assertTestRun(new NcaTestInputBuilder(msscRepository).createInputData());
  }

  @Test
  @Timeout(value = 120, unit = TimeUnit.SECONDS)
  void testRunSingleExportTypeADMS() throws Exception {
    final CalculationInputData inputData = new NcaTestInputBuilder(msscRepository).createInputData();
    inputData.setExportType(ExportType.ADMS);
    assertTestRun(inputData);
  }

  @Test
  @Timeout(value = 120, unit = TimeUnit.SECONDS)
  void testRunSingleCustomPoints() throws Exception {
    final NcaTestInputBuilder builder = new NcaTestInputBuilder(msscRepository);
    final CalculationInputData inputData = builder.createInputData();

    builder.addPoints(inputData.getScenario());
    final CalculationSetOptions options = inputData.getScenario().getOptions();
    options.setCalculationMethod(CalculationMethod.CUSTOM_POINTS);
    options.getNcaCalculationOptions().setRoadLocalFractionNO2PointsOption(RoadLocalFractionNO2Option.ONE_CUSTOM_VALUE);
    options.getNcaCalculationOptions().setRoadLocalFractionNO2(ROAD_LOCAL_FNO2);
    assertTestRun(inputData);
    assertfNO2();

  }

  private void assertfNO2() {
    final double fNO2 = ((List<ADMSCalculationPoint>) mockConnection.getADMSInputDataList().get(0).getCalculateReceptors()).get(0).getRoadLocalFNO2();

    assertEquals(ROAD_LOCAL_FNO2, fNO2, 1E-8, "Expects load road fNO2 to set");
  }

}
