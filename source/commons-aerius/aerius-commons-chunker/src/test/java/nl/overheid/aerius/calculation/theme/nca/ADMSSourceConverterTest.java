/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import static nl.aerius.adms.domain.ADMSGroupKey.NH3;
import static nl.aerius.adms.domain.ADMSGroupKey.NOX_OTHER;
import static nl.aerius.adms.domain.ADMSGroupKey.NOX_ROAD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.aerius.adms.domain.ADMSGroupKey;
import nl.aerius.adms.domain.ADMSSource;
import nl.aerius.adms.domain.ADMSTimeVaryingProfile;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.characteristics.StandardTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.characteristics.TimeVaryingProfiles;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.ADMSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.scenario.Definitions;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geo.EPSG;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link ADMSSourceConverter}.
 */
class ADMSSourceConverterTest {

  private static final String CUSTOM_ROAD_DV_3D = "CUSTOM_ROAD_DV_3D";
  private static final String CUSTOM_ROAD_DV_M = "CUSTOM_ROAD_DV_M";
  private static final String STANDARD_ROAD_DV = "STANDARD_ROAD_DV";
  private static final String DEFAULT_SECTOR_DV = "DEFAULT_SECTOR_DV";
  private static final Integer YEAR = 2030;
  private static final List<Double> THREE_DAY_VALUES = IntStream.range(0, 72).mapToDouble(Double::valueOf).boxed().collect(Collectors.toList());
  private static final CustomTimeVaryingProfileType THREE_DAY_PROFILE = CustomTimeVaryingProfileType.THREE_DAY;
  private static final CustomTimeVaryingProfileType MONTHLY_PROFILE = CustomTimeVaryingProfileType.MONTHLY;

  private ScenarioSituation situation;
  private ADMSSourceConverter converter;

  @BeforeEach
  void beforeEach() throws AeriusException {
    situation = new ScenarioSituation();
    situation.setYear(YEAR);
    // Set custom time-varying profiles
    final Definitions defintions = new Definitions();
    defintions.setCustomTimeVaryingProfiles(
        List.of(createCustomTimeVaryingProfile(CUSTOM_ROAD_DV_3D), createCustomTimeVaryingProfile(CUSTOM_ROAD_DV_M)));
    situation.setDefinitions(defintions);
    final SectorCategories sectorCategories = new SectorCategories();
    sectorCategories.setRoadEmissionCategories(new RoadEmissionCategories());

    // Set standard time-varying profiles
    final TimeVaryingProfiles timeVaryingProfiles = new TimeVaryingProfiles();
    timeVaryingProfiles.setSectorYearDefaultStandardProfileCodes(THREE_DAY_PROFILE, Map.of(TestDomain.ROAD_SECTOR, Map.of(YEAR, STANDARD_ROAD_DV),
        TestDomain.DEFAULT_SECTOR_ID, Map.of(YEAR, DEFAULT_SECTOR_DV)));
    timeVaryingProfiles.setStandardTimeVaryingProfiles(THREE_DAY_PROFILE,
        List.of(
            new StandardTimeVaryingProfile(1, STANDARD_ROAD_DV, STANDARD_ROAD_DV, "", CustomTimeVaryingProfileType.THREE_DAY, THREE_DAY_VALUES),
            new StandardTimeVaryingProfile(2, DEFAULT_SECTOR_DV, DEFAULT_SECTOR_DV, "", CustomTimeVaryingProfileType.THREE_DAY, THREE_DAY_VALUES)));

    sectorCategories.setTimeVaryingProfiles(timeVaryingProfiles);

    converter = new ADMSSourceConverter(EPSG.BNG.getSrid(), situation, sectorCategories);
  }

  private CustomTimeVaryingProfile createCustomTimeVaryingProfile(final String name) {
    final CustomTimeVaryingProfile customTimeVaryingProfile = new CustomTimeVaryingProfile();
    customTimeVaryingProfile.setGmlId(name);
    customTimeVaryingProfile.setLabel(name);
    return customTimeVaryingProfile;
  }

  @Test
  void testSourcesSplit() throws AeriusException {
    final GroupedSourcesPacketMap map = new GroupedSourcesPacketMap();
    final EmissionSourceFeature source = createRoadESource();
    final LineString line = new LineString();
    final double coordinates[][] = new double[103][2];
    IntStream.range(0, 103).forEach(i -> coordinates[i] = new double[] {i, i});
    line.setCoordinates(coordinates);
    source.setGeometry(line);
    converter.convertBuildings();
    converter.convert(null, map, source.getProperties(), source.getGeometry(), List.of(Substance.NOX));

    final List<GroupedSourcesPacket> groups = map.toGroupedSourcesPacket();

    assertEquals(3, groups.get(0).getSources().size(), "Should have split line source");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testTimeVaryingProfileConversion(final EmissionSourceFeature source, final Substance substance, final Integer groupKey,
      final String expectedProfileName, final CustomTimeVaryingProfileType expectedProfile) throws AeriusException {
    final GroupedSourcesPacketMap map = new GroupedSourcesPacketMap();

    converter.convertBuildings();
    converter.convert(null, map, source.getProperties(), source.getGeometry(), List.of(substance));
    // Add a second source with no time-varying profile.
    final EmissionSourceFeature noDVSSource = createOtherSource();
    noDVSSource.getProperties().setEmissions(Map.of(substance, 10.0));
    noDVSSource.getProperties().setSectorId(1000);
    noDVSSource.setId("100");
    converter.convert(null, map, noDVSSource.getProperties(), noDVSSource.getGeometry(), List.of(substance));

    final Collection<GroupedSourcesPacket> groups = map.toGroupedSourcesPacket();
    final boolean roadNOx = groupKey == NOX_ROAD.getIndex();
    assertEquals(roadNOx ? 3 : 2, groups.stream().mapToInt(s -> s.getSources().size()).sum(),
        "Should contain the expected number of sources");

    converter.postProcess(map);
    final int expectedNumberOfProfiles = (expectedProfile == MONTHLY_PROFILE ? 4 : 3) + (roadNOx ? 2 : 0);
    assertEquals(expectedNumberOfProfiles, groups.stream().mapToInt(s -> s.getSources().size()).sum(),
        "Should contain 2 (or 3 for NOX_ROAD) sources, and 1 (or 2 for NOX_ROAD) profile for three day,"
            + "and a three day and monthly when profile monthly profile is expected");

    final List<EngineSource> sources = groups.stream().flatMap(s -> s.getSources().stream()).toList();
    // Test if the time-varying profile is correctly set on the source
    final ADMSSource<?> admsSource = (ADMSSource<?>) sources.stream()
        .filter(s -> s instanceof ADMSSource)
        .filter(s -> source.getProperties().getLabel().equals(((ADMSSource<?>) s).getName())).findAny().get();

    if (expectedProfile == THREE_DAY_PROFILE) {
      assertEquals(expectedProfileName, admsSource.getHourlyTimeVaryingProfileName(), "Invalid or no three day profile name found on the source");
      assertNull(admsSource.getMonthlyTimeVaryingProfileName(), "No monthly profile expected");
    } else {
      assertEquals(expectedProfileName, admsSource.getMonthlyTimeVaryingProfileName(), "Invalid or no monthly profile name found on the source");
      assertEquals(DEFAULT_SECTOR_DV, admsSource.getHourlyTimeVaryingProfileName(), "Invalid or no three day profile name found on the source");
    }

    // Test if the time-varying profile is correctly added to the list of profiles.
    final String tvpLabel = sources.stream()
        .filter(s -> s instanceof final ADMSTimeVaryingProfile tvp && expectedProfileName.equals(tvp.getName())).findAny()
        .map(atvp -> ((ADMSTimeVaryingProfile) atvp).getName()).orElse(null);
    assertEquals(expectedProfileName, tvpLabel, "Invalid or no profile added");
  }

  private static List<Arguments> data() throws AeriusException {
    return List.of(
        // @formatter:off
        //         source               id,  substance      custom final dv id standard dv id    group key, expected profile,  profile type
        createData(createRoadESource(), "1", Substance.NOX, null,              null,             NOX_ROAD,  STANDARD_ROAD_DV,  THREE_DAY_PROFILE),
        createData(createRoadESource(), "2", Substance.NOX, CUSTOM_ROAD_DV_3D, null,             NOX_ROAD,  CUSTOM_ROAD_DV_3D, THREE_DAY_PROFILE),
        createData(createRoadESource(), "3", Substance.NOX, null,              STANDARD_ROAD_DV, NOX_ROAD,  STANDARD_ROAD_DV,  THREE_DAY_PROFILE),
        createData(createRoadESource(), "4", Substance.NH3, null,              null,             NH3,       STANDARD_ROAD_DV,  THREE_DAY_PROFILE),
        createData(createRoadESource(), "5", Substance.NH3, CUSTOM_ROAD_DV_3D, null,             NH3,       CUSTOM_ROAD_DV_3D, THREE_DAY_PROFILE),
        createData(createRoadESource(), "6", Substance.NH3, null,              STANDARD_ROAD_DV, NH3,       STANDARD_ROAD_DV,  THREE_DAY_PROFILE),
        createData(createOtherSource(), "7", Substance.NOX, null,              null,             NOX_OTHER, DEFAULT_SECTOR_DV, THREE_DAY_PROFILE),
        createData(createOtherSource(), "8", Substance.NOX, CUSTOM_ROAD_DV_3D, null,             NOX_OTHER, CUSTOM_ROAD_DV_3D, THREE_DAY_PROFILE),
        createData(createOtherSource(), "9", Substance.NOX, null,              STANDARD_ROAD_DV, NOX_OTHER, STANDARD_ROAD_DV,  THREE_DAY_PROFILE),
        createData(createOtherSource(), "A", Substance.NOX, CUSTOM_ROAD_DV_M,  null,             NOX_OTHER, CUSTOM_ROAD_DV_M,  MONTHLY_PROFILE)
        // @formatter:on
        );
  }

  private static EmissionSourceFeature createRoadESource() {
    return NcaTestInputBuilder.createRoadEmissionSource(1.0);
  }

  private static EmissionSourceFeature createOtherSource() {
    final GenericEmissionSource emissionSource = new GenericEmissionSource();

    emissionSource.setSectorId(TestDomain.DEFAULT_SECTOR_ID);
    final EmissionSourceFeature source = TestDomain.getSource("1", new Point(10.0, 20.0), "SomeSource", emissionSource);

    emissionSource.setCharacteristics(new ADMSSourceCharacteristics());
    return source;
  }

  private static Arguments createData(final EmissionSourceFeature source, final String id, final Substance substance,
      final String customDVId, final String standardDVId, final ADMSGroupKey groupKey, final String expectedDVid,
      final CustomTimeVaryingProfileType expectedProfile) throws AeriusException {
    final ADMSSourceCharacteristics admsSourceCharacteristics = (ADMSSourceCharacteristics) source.getProperties().getCharacteristics();

    admsSourceCharacteristics.setStandardHourlyTimeVaryingProfileCode(standardDVId);
    if (expectedProfile == THREE_DAY_PROFILE) {
      admsSourceCharacteristics.setCustomHourlyTimeVaryingProfileId(customDVId);
    } else {
      admsSourceCharacteristics.setCustomMonthlyTimeVaryingProfileId(customDVId);
    }
    source.getProperties().setEmissions(Map.of(substance, 10.0));
    source.setId(id);
    source.getProperties().setLabel(id);
    return Arguments.of(source, substance, groupKey.getIndex(), expectedDVid, expectedProfile);
  }
}
