/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.geo.SridPoint;

/**
 * Test class for {@link GridUtil}.
 */
class GridUtilTest extends BaseDBTest {

  private static final GridSettings ZOOM_LEVELS = new GridSettings(RECEPTOR_GRID_SETTINGS);
  private static final GridZoomLevel ZOOM_LEVEL_1 = ZOOM_LEVELS.levelFromLevel(1);
  private static final GridZoomLevel ZOOM_LEVEL_2 = ZOOM_LEVELS.levelFromLevel(2);
  private static final GridZoomLevel ZOOM_LEVEL_3 = ZOOM_LEVELS.levelFromLevel(3);
  private static final GridZoomLevel ZOOM_LEVEL_4 = ZOOM_LEVELS.levelFromLevel(4);
  private static final GridZoomLevel ZOOM_LEVEL_5 = ZOOM_LEVELS.levelFromLevel(5);
  private static final GridZoomLevel ZOOM_LEVEL_MAX = ZOOM_LEVELS.getZoomLevelMax();
  private static final GridUtil GRID_UTIL = new GridUtil(ZOOM_LEVELS);

  @Test
  void test2AndFromCell() {
    final SridPoint[] points = { new SridPoint(0, 96000), new SridPoint(96000, 0), new SridPoint(96000, 96000) };
    for (final SridPoint p : points) {
      assertEquals(p, GRID_UTIL.getPositionFromCell(GRID_UTIL.getCellFromPosition(p, ZOOM_LEVEL_MAX)), "Should be same point");
    }
  }

  @Test
  void testSurrounding1() {
    final Collection<Integer> surrounding1 = GRID_UTIL.getSurrounding(1, 0, ZOOM_LEVEL_2);
    assertSize(surrounding1, 1);
    assertContains(surrounding1, 1);
  }

  @Test
  void testSurrounding2() {
    final Collection<Integer> surrounding2 = GRID_UTIL.getSurrounding(1, 500, ZOOM_LEVEL_2);
    assertSize(surrounding2, 4);
    assertContains(surrounding2, 1, 3, 1281, 1283);
  }

  @Test
  void testSurrounding3() {
    final Collection<Integer> surrounding3 = GRID_UTIL.getSurrounding(1281, 250, ZOOM_LEVEL_2);
    assertSize(surrounding3, 6);
    assertContains(surrounding3, 1, 3, 1283, 1281, 2561, 2563);
  }

  @Test
  void testSurrounding4() {
    final Collection<Integer> surrounding4 = GRID_UTIL.getSurrounding(640, 500, ZOOM_LEVEL_1);
    assertSize(surrounding4, 4);
    assertContains(surrounding4, 639, 640, 1279, 1280);
  }

  @Test
  void testSurrounding5() {
    final Collection<Integer> surrounding5 = GRID_UTIL.getSurrounding(1, 1750, ZOOM_LEVEL_1);
    assertSize(surrounding5, 16);
    assertContains(surrounding5, 1, 2, 3, 4);
    assertContains(surrounding5, 641, 642, 643, 644);
    assertContains(surrounding5, 1281, 1282, 1283, 1284);
    assertContains(surrounding5, 1921, 1922, 1923, 1924);
  }

  @Test
  void testSurrounding6() {
    final Collection<Integer> surrounding6 = GRID_UTIL.getSurrounding(1, 10000, ZOOM_LEVEL_3);
    assertSize(surrounding6, 25);
    assertContains(surrounding6, 1, 5, 9, 13, 17);
    assertContains(surrounding6, 2561, 2565, 2569, 2573, 2577);
    assertContains(surrounding6, 5121, 5125, 5129, 5133, 5137);
    assertContains(surrounding6, 7681, 7685, 7689, 7693, 7697);
    assertContains(surrounding6, 10241, 10245, 10249, 10253, 10257);
  }

  @Test
  void testSurrounding7() {
    final Collection<Integer> surrounding1 = GRID_UTIL.getSurrounding(1, 250, ZOOM_LEVEL_1);
    assertSize(surrounding1, 4);
    assertContains(surrounding1, 1, 2, 641, 642);
  }

  @Test
  void testSurrounding8() {
    final Collection<Integer> surrounding4 = GRID_UTIL.getSurrounding(639, 500, ZOOM_LEVEL_2);
    assertSize(surrounding4, 4);
    assertContains(surrounding4, 637, 639, 1917, 1919);
  }

  @Test
  void testSurrounding9() {
    assertThrows(IllegalArgumentException.class, () -> GRID_UTIL.getSurrounding(2, 0, ZOOM_LEVEL_2));
  }

  @Test
  void testInnerLevel1Var1() {
    final Collection<Integer> inner = GRID_UTIL.getInnerCells(1, ZOOM_LEVEL_1);

    assertSize(inner, 4);
    assertContains(inner, 1, 2, 641, 642);
  }

  @Test
  void testInnerIsValidCellHorizontal() {
    assertTrue(GRID_UTIL.checkIsValidCell(1, ZOOM_LEVEL_1), "Cell 1 is expected to exist on zoom level 1."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(1, ZOOM_LEVEL_2), "Cell 1 is expected to exist on zoom level 2."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(1, ZOOM_LEVEL_3), "Cell 1 is expected to exist on zoom level 3."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(1, ZOOM_LEVEL_4), "Cell 1 is expected to exist on zoom level 4."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(1, ZOOM_LEVEL_5), "Cell 1 is expected to exist on zoom level 5."); // 1

    assertTrue(GRID_UTIL.checkIsValidCell(2, ZOOM_LEVEL_1), "Cell 2 is expected to exist on zoom level 1."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(2, ZOOM_LEVEL_2), "Cell 2 is NOT expected to exist on zoom level 2."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(2, ZOOM_LEVEL_3), "Cell 2 is NOT expected to exist on zoom level 3."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(2, ZOOM_LEVEL_4), "Cell 2 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(2, ZOOM_LEVEL_5), "Cell 2 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(3, ZOOM_LEVEL_1), "Cell 3 is expected to exist on zoom level 1."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(3, ZOOM_LEVEL_2), "Cell 3 is expected to exist on zoom level 2."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(3, ZOOM_LEVEL_3), "Cell 3 is NOT expected to exist on zoom level 3."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(3, ZOOM_LEVEL_4), "Cell 3 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(3, ZOOM_LEVEL_5), "Cell 3 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(4, ZOOM_LEVEL_1), "Cell 4 is expected to exist on zoom level 1."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(4, ZOOM_LEVEL_2), "Cell 4 is NOT expected to exist on zoom level 2."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(4, ZOOM_LEVEL_3), "Cell 4 is NOT expected to exist on zoom level 3."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(4, ZOOM_LEVEL_4), "Cell 4 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(4, ZOOM_LEVEL_5), "Cell 4 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(5, ZOOM_LEVEL_1), "Cell 5 is expected to exist on zoom level 1."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(5, ZOOM_LEVEL_2), "Cell 5 is expected to exist on zoom level 2."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(5, ZOOM_LEVEL_3), "Cell 5 is expected to exist on zoom level 3."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(5, ZOOM_LEVEL_4), "Cell 5 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(5, ZOOM_LEVEL_5), "Cell 5 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(6, ZOOM_LEVEL_1), "Cell 6 is expected to exist on zoom level 1."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(6, ZOOM_LEVEL_2), "Cell 6 is NOT expected to exist on zoom level 2."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(6, ZOOM_LEVEL_3), "Cell 6 is NOT expected to exist on zoom level 3."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(6, ZOOM_LEVEL_4), "Cell 6 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(6, ZOOM_LEVEL_5), "Cell 6 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(7, ZOOM_LEVEL_1), "Cell 7 is expected to exist on zoom level 1."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(7, ZOOM_LEVEL_2), "Cell 7 is expected to exist on zoom level 2."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(7, ZOOM_LEVEL_3), "Cell 7 is NOT expected to exist on zoom level 3."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(7, ZOOM_LEVEL_4), "Cell 7 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(7, ZOOM_LEVEL_5), "Cell 7 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(8, ZOOM_LEVEL_1), "Cell 8 is expected to exist on zoom level 1."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(8, ZOOM_LEVEL_2), "Cell 8 is NOT expected to exist on zoom level 2."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(8, ZOOM_LEVEL_3), "Cell 8 is NOT expected to exist on zoom level 3."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(8, ZOOM_LEVEL_4), "Cell 8 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(8, ZOOM_LEVEL_5), "Cell 8 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(9, ZOOM_LEVEL_1), "Cell 9 is expected to exist on zoom level 1."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(9, ZOOM_LEVEL_2), "Cell 9 is expected to exist on zoom level 2."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(9, ZOOM_LEVEL_3), "Cell 9 is expected to exist on zoom level 3."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(9, ZOOM_LEVEL_4), "Cell 9 is expected to exist on zoom level 4."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(9, ZOOM_LEVEL_5), "Cell 9 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(10, ZOOM_LEVEL_1), "Cell 10 is expected to exist on zoom level 1."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(10, ZOOM_LEVEL_2), "Cell 10 is NOT expected to exist on zoom level 2."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(10, ZOOM_LEVEL_3), "Cell 10 is NOT expected to exist on zoom level 3."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(10, ZOOM_LEVEL_4), "Cell 10 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(10, ZOOM_LEVEL_5), "Cell 10 is NOT expected to exist on zoom level 5."); // 0
  }

  @Test
  void testInnerIsValidCellVertical() {
    assertTrue(GRID_UTIL.checkIsValidCell(641, ZOOM_LEVEL_1), "Cell 641 is expected to exist on zoom level 1."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(641, ZOOM_LEVEL_2), "Cell 641 is NOT expected to exist on zoom level 2."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(641, ZOOM_LEVEL_3), "Cell 641 is NOT expected to exist on zoom level 3."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(641, ZOOM_LEVEL_4), "Cell 641 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(641, ZOOM_LEVEL_5), "Cell 641 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(1281, ZOOM_LEVEL_1), "Cell 1281 is expected to exist on zoom level 1."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(1281, ZOOM_LEVEL_2), "Cell 1281 is expected to exist on zoom level 2."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(1281, ZOOM_LEVEL_3), "Cell 1281 is NOT expected to exist on zoom level 3."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(1281, ZOOM_LEVEL_4), "Cell 1281 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(1281, ZOOM_LEVEL_5), "Cell 1281 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(1921, ZOOM_LEVEL_1), "Cell 1921 is expected to exist on zoom level 1."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(1921, ZOOM_LEVEL_2), "Cell 1921 is NOT expected to exist on zoom level 2."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(1921, ZOOM_LEVEL_3), "Cell 1921 is NOT expected to exist on zoom level 3."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(1921, ZOOM_LEVEL_4), "Cell 1921 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(1921, ZOOM_LEVEL_5), "Cell 1921 is NOT expected to exist on zoom level 5."); // 0

    assertTrue(GRID_UTIL.checkIsValidCell(2561, ZOOM_LEVEL_1), "Cell 2561 is expected to exist on zoom level 1."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(2561, ZOOM_LEVEL_2), "Cell 2561 is expected to exist on zoom level 2."); // 1
    assertTrue(GRID_UTIL.checkIsValidCell(2561, ZOOM_LEVEL_3), "Cell 2561 is expected to exist on zoom level 3."); // 1
    assertFalse(GRID_UTIL.checkIsValidCell(2561, ZOOM_LEVEL_4), "Cell 2561 is NOT expected to exist on zoom level 4."); // 0
    assertFalse(GRID_UTIL.checkIsValidCell(2561, ZOOM_LEVEL_5), "Cell 2561 is NOT expected to exist on zoom level 5."); // 0
  }

  @Test
  void testInnerLevel2() {
    final Collection<Integer> inner = GRID_UTIL.getInnerCells(1, ZOOM_LEVEL_2);

    assertSize(inner, 4);
    assertContains(inner, 1, 3, 1281, 1283);
  }

  private void assertSize(final Collection<Integer> surrounding, final int expectedSize) {
    assertEquals(expectedSize, surrounding.size(), "Surrounding size different");
  }

  private void assertContains(final Collection<Integer> surrounding, final int... contains) {
    for (final int value : contains) {
      assertTrue(surrounding.contains(value),
          "Surrounding cell should contain cell " + value + " from(" + Arrays.toString(surrounding.toArray()) + ")");
    }
  }

  @Test
  void testIdDerivation() {
    assertEquals(1, GRID_UTIL.getCellFromPosition(new SridPoint(0, 640000), ZOOM_LEVEL_1), "Wrong ID");
    assertEquals(1, GRID_UTIL.getCellFromPosition(new SridPoint(0, 640000), ZOOM_LEVEL_2), "Wrong ID");
    assertEquals(1, GRID_UTIL.getCellFromPosition(new SridPoint(0, 640000), ZOOM_LEVEL_3), "Wrong ID");

    assertEquals(2, GRID_UTIL.getCellFromPosition(new SridPoint(500, 640000), ZOOM_LEVEL_1), "Wrong ID");
    assertEquals(11, GRID_UTIL.getCellFromPosition(new SridPoint(5000, 640000), ZOOM_LEVEL_2), "Wrong ID");
    assertEquals(49, GRID_UTIL.getCellFromPosition(new SridPoint(25000, 640000), ZOOM_LEVEL_3), "Wrong ID");
    assertEquals(33, GRID_UTIL.getCellFromPosition(new SridPoint(25000, 640000), ZOOM_LEVEL_MAX), "Wrong ID");
    assertEquals(33, GRID_UTIL.getCellFromPosition(new SridPoint(20000, 640000), ZOOM_LEVEL_MAX), "Wrong ID");

    assertEquals(1, GRID_UTIL.getCellFromPosition(new SridPoint(0, 640000), ZOOM_LEVEL_1), "Wrong ID");
    assertEquals(5121, GRID_UTIL.getCellFromPosition(new SridPoint(0, 636000), ZOOM_LEVEL_2), "Wrong ID");
    assertEquals(51201, GRID_UTIL.getCellFromPosition(new SridPoint(0, 599000), ZOOM_LEVEL_3), "Wrong ID");
  }

  @Test
  void testMinDistance() {
    assertEquals(500D, GRID_UTIL.getMinDistance(1, 3, ZOOM_LEVEL_1, ZOOM_LEVEL_1), 0D, "Min distance invalid");
    assertEquals(149000D, GRID_UTIL.getMinDistance(1, 300, ZOOM_LEVEL_1, ZOOM_LEVEL_1), 0D, "Min distance invalid");

    assertEquals(1500D, GRID_UTIL.getMinDistance(6, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D, "Min distance invalid");
    assertEquals(4000D, GRID_UTIL.getMinDistance(11, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D, "Min distance invalid");
    assertEquals(154000D, GRID_UTIL.getMinDistance(311, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D, "Min distance invalid");
    assertEquals(114000D, GRID_UTIL.getMinDistance(1511, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D, "Min distance invalid");
  }

  @Test
  void testMaxDistance() {
    assertEquals(500D, GRID_UTIL.getMaxDistance(1, 2, ZOOM_LEVEL_1, ZOOM_LEVEL_1), 0D, "Max distance invalid");
    assertEquals(149500D, GRID_UTIL.getMaxDistance(1, 300, ZOOM_LEVEL_1, ZOOM_LEVEL_1), 0.1D, "Max distance invalid");

    assertEquals(2500D, GRID_UTIL.getMaxDistance(6, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D, "Max distance invalid");
    assertEquals(2500D, GRID_UTIL.getMaxDistance(6, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D, "Max distance invalid");
    assertEquals(12500D, GRID_UTIL.getMaxDistance(26, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_3), 0D, "Max distance invalid");
  }

  @Test
  void testIsOutside() {
    assertTrue(GRID_UTIL.isFullyOutside(1, 6, 0, ZOOM_LEVEL_2, ZOOM_LEVEL_1), "Cell 1 should be outside 6 on zl 1");
    assertTrue(GRID_UTIL.isFullyOutside(1, 6, 0, ZOOM_LEVEL_2, ZOOM_LEVEL_2), "Cell 1 should be outside 6 on zl 2");
    assertTrue(GRID_UTIL.isFullyOutside(1, 6, 0, ZOOM_LEVEL_2, ZOOM_LEVEL_3), "Cell 1 should be outside 6 on zl 3");

    assertFalse(GRID_UTIL.isFullyOutside(7, 6, 1000, ZOOM_LEVEL_1, ZOOM_LEVEL_1), "Cell 7 should NOT be fully outside 7 on zl 1");
    assertFalse(GRID_UTIL.isFullyOutside(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_1), "Cell 7 should NOT be fully outside 7 on zl 1");
    assertFalse(GRID_UTIL.isFullyOutside(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_2), "Cell 7 should NOT be fully outside 7 on zl 2");
    assertFalse(GRID_UTIL.isFullyOutside(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_3), "Cell 7 should NOT be fully outside 7 on zl 3");
  }

  @Test
  void testIsFullyEncapsulated() {
    assertTrue(GRID_UTIL.isFullyEncapsulated(7, 6, 1000, ZOOM_LEVEL_1, ZOOM_LEVEL_1), "Cell 7 should NOT be fully outside 7 on zl 1");
    assertTrue(GRID_UTIL.isFullyEncapsulated(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_1), "Cell 7 should NOT be fully outside 7 on zl 1");
    assertTrue(GRID_UTIL.isFullyEncapsulated(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_2), "Cell 7 should NOT be fully outside 7 on zl 2");
    assertTrue(GRID_UTIL.isFullyEncapsulated(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_3), "Cell 7 should NOT be fully outside 7 on zl 3");
  }
}
