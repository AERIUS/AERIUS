/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.subreceptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.receptor.SubReceptorCreator;
import nl.overheid.aerius.shared.domain.calculation.SubReceptorsMode;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.ops.OPSSubReceptor;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link SubReceptorPointSupplier}.
 */
@ExtendWith(MockitoExtension.class)
class SubReceptorPointSupplierTest extends BaseDBTest {

  // Neighbour of 4776053, which is partially covered by habitat in test data
  private static final Point REFERENCE_POINT = RECEPTOR_UTIL.getPointFromReceptorId(4772995);
  private static final Point FAR_AWAY_POINT = RECEPTOR_UTIL.getPointFromReceptorId(1);

  private final SubReceptorCreator<OPSSubReceptor> subReceptorCreator = new TestSubReceptorCreator();

  private @Mock EmissionSourceListSTRTree sourcesTree;

  @ParameterizedTest
  @MethodSource("data")
  void testSubreceptorFiltering(final Point point, final double minDistance, final SubReceptorsMode subReceptorMode, final int expectedCount)
      throws AeriusException {
    doReturn(1.0).when(sourcesTree).findShortestDistance(any(SridPoint.class));
    final AeriusPoint aeriusPoint = new AeriusPoint(point.getX(), point.getY());
    final SubReceptorPointSupplier<OPSSubReceptor> supplier = create(minDistance, subReceptorMode);
    final List<AeriusPoint> pointsToCalculate = new ArrayList<>();

    assertEquals(expectedCount, supplier.addPoint(pointsToCalculate, aeriusPoint),
        "Expected number of points included doesn't match.");
  }

  private static List<Arguments> data() {
    return List.of(
        // No habitat check, distance 0, this should always return the sub point.
        // Point overlaps reference, but doesn't matter
        Arguments.of(REFERENCE_POINT, 0, SubReceptorsMode.DISABLED, 6),
        // Point doesn't overlap, but this doesn't matter for this case, it should return the sub point.
        Arguments.of(FAR_AWAY_POINT, 0, SubReceptorsMode.DISABLED, 6),

        // No habitat check, distance 2, result should not differ as the point should not be included as it's closer than the min distance.
        Arguments.of(REFERENCE_POINT, 2, SubReceptorsMode.DISABLED, 0),
        Arguments.of(FAR_AWAY_POINT, 2, SubReceptorsMode.DISABLED, 0),

        // Do habitat check, distance 0, result depends on point
        // Point overlaps reference, this should be returned, but with filtered number of points.
        Arguments.of(REFERENCE_POINT, 0, SubReceptorsMode.ENABLED, 3),
        // Point doesn't overlap, therefore point should not be returned.
        Arguments.of(FAR_AWAY_POINT, 0, SubReceptorsMode.ENABLED, 0),

        // Do habitat check, distance 2, but result should not differ as the point should not be included as it's closer than the min distance.
        Arguments.of(REFERENCE_POINT, 2, SubReceptorsMode.ENABLED, 0),
        Arguments.of(FAR_AWAY_POINT, 2, SubReceptorsMode.ENABLED, 0));
  }

  private SubReceptorPointSupplier<OPSSubReceptor> create(final double minDistance, final SubReceptorsMode subReceptorMode) {
    return new SubReceptorPointSupplier<>(getPMF(), subReceptorCreator, RECEPTOR_GRID_SETTINGS.getZoomLevel1(), minDistance, sourcesTree,
        subReceptorMode);
  }

  private static class TestSubReceptorCreator extends SubReceptorCreator<OPSSubReceptor> {

    protected TestSubReceptorCreator() {
      super(AeriusPointType.SUB_RECEPTOR, RECEPTOR_GRID_SETTINGS.getZoomLevel1().getHexagonRadius(), List.of(0.0, 1000.0), 0);
    }

    @Override
    public OPSSubReceptor copyToSubReceptorPoint(final AeriusPoint aeriusPoint, final AeriusPointType pointType, final int subLevel) {
      return new OPSSubReceptor(aeriusPoint, pointType, subLevel);
    }
  }
}
