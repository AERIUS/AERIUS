/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import nl.overheid.aerius.calculation.base.CalculatorThemeFactory;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.theme.own2000.OwN2000CalculatorFactory;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Base class for testing classes in the calculation package.
 */
public class CalculationTestBase extends BaseDBTest {

  protected static final int NUMBER_OF_RECEPTORS = 100;

  protected static final int TEST_YEAR = 2030;

  protected static CalculationEngineProvider defaultCalculationEngineProvider;

  protected CalculatorThemeFactory themeFactory;

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    try (Connection con = getPMF().getConnection()) {
      defaultCalculationEngineProvider = new DefaultCalculationEngineProvider(SectorRepository.getSectorProperties(con));

    }
  }

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    themeFactory = new OwN2000CalculatorFactory(getPMF());
  }

  protected CalculationJob getExampleCalculationJob() throws SQLException, AeriusException {
    return getExampleCalculationJob(ExportType.GML_WITH_RESULTS);
  }

  protected CalculationJob getExampleCalculationJob(final ExportType exportType) throws SQLException, AeriusException {
    final String jobKey = JobRepository.createJob(getConnection(), JobType.CALCULATION, false);

    return getExampleCalculationJob(new JobIdentifier(jobKey), exportType);
  }

  protected CalculationJob getExampleCalculationJob(final JobIdentifier jobIdentifier) throws SQLException, AeriusException {
    return getExampleCalculationJob(jobIdentifier, ExportType.GML_WITH_RESULTS);
  }

  protected CalculationJob getExampleCalculationJob(final JobIdentifier jobIdentifier, final ExportType exportType)
      throws SQLException, AeriusException {
    //add sources
    final List<EmissionSourceFeature> emissionSources = createSources();

    final List<CalculationPointFeature> calculationPoints = new ArrayList<>();
    //add receptors
    for (int i = 1; i <= NUMBER_OF_RECEPTORS; i++) {
      final CalculationPointFeature feature = new CalculationPointFeature();
      final AeriusPoint calculationPoint = new AeriusPoint();
      calculationPoint.setId(i);
      setCoordinatesFromId(calculationPoint);
      feature.setGeometry(new Point(calculationPoint.getX(), calculationPoint.getY()));
      final ReceptorPoint receptorPoint = new ReceptorPoint();
      receptorPoint.setReceptorId(i);
      feature.setProperties(receptorPoint);
      calculationPoints.add(feature);
    }
    return createCalculationJob(jobIdentifier, emissionSources, calculationPoints, exportType);
  }

  protected List<EmissionSourceFeature> createSources() {
    final List<EmissionSourceFeature> emissionSources = new ArrayList<>();

    addSources(emissionSources, 1800, 50);
    addSources(emissionSources, 4120, 25);
    addSources(emissionSources, 4600, 25);
    return emissionSources;
  }

  private void addSources(final List<EmissionSourceFeature> features, final int sectorId, final int nrOfSources) {
    for (int i = 1; i <= nrOfSources; i++) {
      final EmissionSourceFeature feature = new EmissionSourceFeature();
      feature.setId("1");
      feature.setGeometry(new Point(100_0000 + 1000 * i, 200_000 + 1000 * i));
      final GenericEmissionSource emissionSource = new GenericEmissionSource();
      emissionSource.getEmissions().put(Substance.NH3, 1000.0);
      emissionSource.getEmissions().put(Substance.NOX, 1000.0);
      emissionSource.getEmissions().put(Substance.PM10, 1000.0);
      emissionSource.setSectorId(sectorId);
      feature.setProperties(emissionSource);
      features.add(feature);
    }
  }

  protected CalculationJob createCalculationJob(final JobIdentifier jobIdentifier, final List<EmissionSourceFeature> emissionSources,
      final List<CalculationPointFeature> calculationPoints, final ExportType exportType) throws SQLException, AeriusException {
    final Scenario scenario = new Scenario(Theme.OWN2000);
    final ScenarioSituation situation = new ScenarioSituation();
    situation.getEmissionSourcesList().addAll(emissionSources);
    situation.setYear(TEST_YEAR);
    scenario.getSituations().add(situation);
    scenario.getCustomPointsList().addAll(calculationPoints);
    final CalculationSetOptions options = new CalculationSetOptions();
    options.setCalculationMethod(CalculationMethod.CUSTOM_POINTS);
    options.setCalculateMaximumRange(1000);
    scenario.setOptions(options);

    scenario.getOptions().getSubstances().add(Substance.NOXNH3);
    scenario.getOptions().getSubstances().add(Substance.PM10);
    scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
    scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.PM10_CONCENTRATION);
    final CalculatorOptions co = CalculatorOptionsFactory.initWorkerCalculationOptions(getConnection());
    final CalculationInputData data = new CalculationInputData();
    data.setName("test");
    data.setQueueName("calculator");
    data.setExportType(exportType);
    return InitCalculation.initCalculation(getConnection(), data, new ScenarioCalculationsImpl(scenario), co, jobIdentifier);
  }
}
