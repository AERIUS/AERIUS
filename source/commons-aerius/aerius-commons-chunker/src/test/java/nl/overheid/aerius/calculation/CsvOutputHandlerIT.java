/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.mock.TestInputBuilder;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * Test class for {@link CsvOutputHandler}.
 */
class CsvOutputHandlerIT extends CalculationITBase {

  private static final int SRM2_SECTOR = 3100;
  private static final String REFERENCE_CSV = "CsvOutputHandlerTest.csv";
  private static final String REFERENCE_SUMMARY = "CsvOutputHandlerTest.summary";
  private static final String REFERENCE_METADATA = "CsvOutputHandlerTest.json";

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp((dsv, rdf) -> new OwN2000MockConnection(dsv, rdf, input -> {}), AeriusCustomer.RIVM, WorkerType.OPS, WorkerType.ASRM);
  }

  @Test
  @Timeout(value = 60, unit = TimeUnit.SECONDS)
  void testRunSingle() throws Exception {
    assertTestRun(new CsvTestInputBuilder().createInputData());
  }

  @Override
  protected void assertResults(final int nrOfCalculationsExpected) throws Exception {
    super.assertResults(nrOfCalculationsExpected);
    final File directory = WorkerUtil.getFolderForJob(calculationJob.getJobIdentifier());
    // Test result output
    final byte[] resultCsv = Files.readAllBytes(new File(directory, calculationJob.getName() + "_resulttype-deposition.csv").toPath());
    final byte[] referenceCsv = getClass().getResourceAsStream(REFERENCE_CSV).readAllBytes();
    assertEquals(sort(referenceCsv), sort(resultCsv), "Csv file should match");
    // Test summary
    final byte[] resultSummary = Files.readAllBytes(new File(directory, calculationJob.getName() + ".summary").toPath());
    final byte[] referenceSummary = getClass().getResourceAsStream(REFERENCE_SUMMARY).readAllBytes();
    assertEquals(sort(referenceSummary), sort(resultSummary), "Summary file should match");
    // Test metadata
    final String resultMetadata = toString(Files.readAllBytes(new File(directory, calculationJob.getName() + "_resulttype-metadata.json").toPath()))
        // Adjust for time measurement value (which can be different each run) by replacing time with a static value of 0.
        .replaceAll("\"duration_seconds\":\"\\d+\"", "\"duration_seconds\":\"0\"")
        // Adjust for version changes by replacing version values with an empty string.
        .replaceAll("\"aerius_version\":\"[^\"]*\"", "\"aerius_version\":\"\"")
        .replaceAll("\"database_version\":\"[^\"]*\"", "\"database_version\":\"\"")
        .replaceAll("\"ops_version\":\"[^\"]*\"", "\"ops_version\":\"\"")
        .replaceAll("\"srm_version\":\"[^\"]*\"", "\"srm_version\":\"\"");
    final String referenceMetadata = toString(getClass().getResourceAsStream(REFERENCE_METADATA).readAllBytes());
    assertEquals(referenceMetadata, resultMetadata, "Metadata file should match");
  }

  private static String sort(final byte[] input) {
    final String string = toString(input);
    final List<String> list = Arrays.asList(string.split(System.lineSeparator())); // List.of is immutable and can't be sorted.

    Collections.sort(list);
    return String.join(System.lineSeparator(), list);
  }

  private static String toString(final byte[] input) {
    return new String(input, StandardCharsets.UTF_8).trim();
  }

  private static class CsvTestInputBuilder extends TestInputBuilder {

    public CsvTestInputBuilder() {
      super(Theme.OWN2000);
    }

    @Override
    public CalculationInputData createInputData() {
      final CalculationInputData inputData = super.createInputData();
      inputData.setName("test");
      inputData.setExportType(ExportType.CSV);

      inputData.getScenario().getCustomPoints().getFeatures().addAll(
          List.of(createPoint(1), createPoint(2), createPoint(3)));
      return inputData;
    }

    private static CalculationPointFeature createPoint(final int i) {
      final CalculationPointFeature feature = new CalculationPointFeature();
      feature.setGeometry(new Point(i * 100_000, i * 100_000));
      final CustomCalculationPoint point = new CustomCalculationPoint();
      point.setGmlId(i + "");
      point.setCustomPointId(i - 1);
      feature.setProperties(point);
      return feature;
    }

    @Override
    protected void setSources(final List<EmissionSourceFeature> sources, final double emissionFactor) {
      final LineString geometrySource1 = new LineString();

      geometrySource1.setCoordinates(
          new double[][] {{TestDomain.XCOORD_1, TestDomain.YCOORD_1}, {TestDomain.XCOORD_1 + 10, TestDomain.YCOORD_1 + 10}});
      final GenericEmissionSource emissionSource = new GenericEmissionSource();
      emissionSource.setSectorId(SRM2_SECTOR);
      emissionSource.getEmissions().put(Substance.NOX, 20.0 * emissionFactor);
      sources.add(TestDomain.getSource("1", geometrySource1, "SomeSource", emissionSource));
    }

    @Override
    protected CalculationSetOptions setCalculationSetOptions(final CalculationSetOptions options) {
      options.setCalculationMethod(CalculationMethod.CUSTOM_POINTS);
      options.getSubstances().add(Substance.NOX);
      options.getEmissionResultKeys().add(EmissionResultKey.NOX_DEPOSITION);
      return options;
    }
  }
}
