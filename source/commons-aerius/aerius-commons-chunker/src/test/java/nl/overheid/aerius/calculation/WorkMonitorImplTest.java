/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.Thread.State;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.overheid.aerius.calculation.base.WorkMonitor;
import nl.overheid.aerius.calculation.base.WorkSemaphore;
import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link WorkMonitorImpl}.
 */
class WorkMonitorImplTest {

  private static final WorkKey WORK_KEY_1 = new WorkKey("", new JobIdentifier("1"), 1, CalculationEngine.OPS);
  private static final WorkKey WORK_KEY_2 = new WorkKey("", new JobIdentifier("2"), 2, CalculationEngine.OPS);
  private static final WorkKey WORK_KEY_3 = new WorkKey("", new JobIdentifier("3"), 3, CalculationEngine.OPS);
  private AtomicBoolean cancelled;
  private WorkTracker tracker;
  private WorkMonitor monitor;

  @BeforeEach
  void before() {
    tracker = Mockito.mock(WorkTracker.class);
    monitor = new WorkMonitorImpl(tracker, new WorkSemaphore() {
      private final Semaphore semaphore = new Semaphore(2);

      @Override
      public void release(final String queueName) {
        semaphore.release();
      }

      @Override
      public void drain() {
        while (semaphore.hasQueuedThreads()) {
          semaphore.release();
        }
      }

      @Override
      public void acquire(final String queueName) throws InterruptedException {
        semaphore.acquire();
      }
    });
    cancelled = new AtomicBoolean();
    Mockito.doAnswer(new Answer<Boolean>() {
      @Override
      public Boolean answer(final InvocationOnMock invocation) throws Throwable {
        return cancelled.get();
      }

    }).when(tracker).isCanceled();
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        cancelled.set(true);
        return null;
      }}).when(tracker).cancel(ArgumentMatchers.any());
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testDrain() throws InterruptedException, TaskCancelledException {
    final String taskId1 = monitor.registerTask(WORK_KEY_1);
    monitor.registerTask(WORK_KEY_2);
    final Semaphore stop = new Semaphore(0);
    final Thread thread = new Thread(() -> {
      try {
        monitor.registerTask(WORK_KEY_3);
        stop.release();
      } catch (final TaskCancelledException | InterruptedException e) {
        // ignore this exception
      }
    });
    thread.start();
    while (thread.getState() != State.WAITING) {}
    monitor.drain();
    stop.acquire();
    assertNull(monitor.getWorkKey(taskId1), "Key should be gone after drain");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testIsCancelled() {
    monitor.taskFailed("", new IllegalArgumentException(""));
    assertTrue(monitor.isCancelled(), "Should report cancelled");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testGetWorkKey() throws InterruptedException, TaskCancelledException {
    assertSame(WORK_KEY_1, monitor.getWorkKey(monitor.registerTask(WORK_KEY_1)), "Key should be same");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testRegisterTask() throws InterruptedException, TaskCancelledException {
    assertNotNull(monitor.registerTask(WORK_KEY_1), "Should return a uuid");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testRegisterTaskOnCancelled() {
    assertThrows(TaskCancelledException.class, () -> {
      monitor.taskFailed("", new IllegalArgumentException(""));
      monitor.registerTask(WORK_KEY_1);
    });
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testUnRegisterTask() throws InterruptedException, TaskCancelledException {
    final AtomicInteger counter = new AtomicInteger();
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        counter.getAndIncrement();
        return null;
      }
    }).when(tracker).decrement();
    final String taskId1 = monitor.registerTask(WORK_KEY_1);
    final String taskId2 = monitor.registerTask(WORK_KEY_2);
    final Semaphore stop = new Semaphore(0);
    final AtomicReference<String> taskId3 = new AtomicReference<>();
    final Thread thread = new Thread(() -> {
      try {
        taskId3.set(monitor.registerTask(WORK_KEY_3));
        stop.release();
      } catch (final TaskCancelledException | InterruptedException e) {
        // ignore this exception
      }
    });
    thread.start();
    while (thread.getState() != State.WAITING) {}
    monitor.unRegisterTask(taskId1);
    stop.acquire();
    assertEquals(1, counter.get(), "Decrement should be called once");
    monitor.unRegisterTask(taskId2);
    monitor.unRegisterTask(taskId3.get());
    assertEquals(3, counter.get(), "Decrement should be called 3 times");
  }
}
