/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.aerius.adms.domain.ADMSEngineDataKey;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link NCACalculationResultQueue}.
 */
class NCACalculationResultQueueTest {

  private static final int NH3_GROUP_KEY = ADMSGroupKey.NH3.getIndex();
  private static final int NOX_OTHER_GROUP_KEY = ADMSGroupKey.NOX_OTHER.getIndex();

  @Test
  void testAggregateCompleteResults() throws AeriusException {
    final NCACalculationResultQueue queue = new NCACalculationResultQueue();
    final ADMSEngineDataKey edk1 = new ADMSEngineDataKey("2018");
    final ADMSEngineDataKey edk2 = new ADMSEngineDataKey("2019");

    final List<JobPacket> jobPackets = List.of(createJobPacket(edk1), createJobPacket(edk2));
    final WorkPacket<AeriusPoint> wp = new WorkPacket<AeriusPoint>(null, jobPackets);
    queue.init(null, List.of(wp));
    addResultToQueue(queue, edk1, 10.0, 10.0);
    addResultToQueue(queue, edk2, 5.0, 20.0);
    final AeriusResultPoint finalResult = queue.pollTotalResults(1).get(0);
    assertEquals(20, finalResult.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), 1E-5, "Should have highest NH3 Deposition");
    assertEquals(40, finalResult.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), 1E-5, "Should have highest NOX Deposition");
  }

  private static void addResultToQueue(final NCACalculationResultQueue queue, final ADMSEngineDataKey edk, final double nh3Deposition,
      final double noxDeposition) throws AeriusException {
    final AeriusResultPoint resultPoint = new AeriusResultPoint(1, 1, AeriusPointType.RECEPTOR, 1, 1);

    resultPoint.getEmissionResults().add(EmissionResultKey.NH3_DEPOSITION, nh3Deposition);
    resultPoint.getEmissionResults().add(EmissionResultKey.NOX_DEPOSITION, noxDeposition);
    queue.put(edk, Map.of(NH3_GROUP_KEY, List.of(resultPoint)), 1);
    queue.put(edk, Map.of(NOX_OTHER_GROUP_KEY, List.of(resultPoint)), 1);
  }

  private static JobPacket createJobPacket(final ADMSEngineDataKey edk) {
    final GroupedSourcesPacket gsp1 = new GroupedSourcesPacket(NH3_GROUP_KEY, edk, List.of());
    final GroupedSourcesPacket gsp2 = new GroupedSourcesPacket(NOX_OTHER_GROUP_KEY, edk, List.of());
    final List<GroupedSourcesPacket> sourcePackets1 = List.of(gsp1, gsp2);
    return new JobPacket(1, 2020, sourcePackets1);
  }
}
