/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.ModelProgressReport;
import nl.overheid.aerius.db.Transaction;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.connect.ConnectUserRepository;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link TrackJobProgressHandler}.
 */
class TrackJobProgressHandlerTest extends CalculationTestBase {

  private static final String TEST_EMAIL = "aerius@example.com";

  private String jobKey;
  private TrackJobProgressHandler handler;
  private int calculationId;
  private Connection connection;
  private Transaction transaction;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    connection = getConnection();
    transaction = new Transaction(connection);
    final ConnectUser user = createUser(connection);
    jobKey = JobRepository.createJob(connection, user, JobType.CALCULATION, false);
    handler = initHandler(new JobIdentifier(jobKey), false);
  }

  @AfterEach
  void after() throws Exception {
    transaction.rollback();
    super.tearDown();
  }

  @Test
  void testInit() throws SQLException {
    assertTrue(handler.isJobFound(), "Job should be found");
    assertFalse(StringUtils.isEmpty(jobKey), "jobKey should be set:" + jobKey);
    final JobProgress progress = JobRepository.getProgress(connection, jobKey);
    assertEquals(0, progress.getNumberOfPointsCalculated(), "Number of points calculated should be 0");
  }

  @Test
  void testOnModelProgressReport() throws AeriusException, SQLException {
    handler = initHandler(new JobIdentifier(jobKey), true);
    handler.onModelProgressReport(new ModelProgressReport("1", 12));
    assertJobProgress(12);

    // Make call to onTotalResults, but that should not have an effect.
    handler.onTotalResults(createResults(), calculationId);
    assertJobProgress(12);

  }

  @Test
  void testOnTotalResults() throws AeriusException, SQLException {
    handler.onTotalResults(createResults(), calculationId);
    assertJobProgress(3);
    // Make call to onModelProgressReport, but that should not have an effect.
    handler.onModelProgressReport(new ModelProgressReport("1", 12));
    assertJobProgress(3);
  }

  private void assertJobProgress(final int expectedProgress) throws SQLException {
    final JobProgress progress2 = JobRepository.getProgress(connection, jobKey);
    assertEquals(expectedProgress, progress2.getNumberOfPointsCalculated(), "Number of points calculated should be matching created results");
  }

  private TrackJobProgressHandler initHandler(final JobIdentifier jobIdentifier, final boolean nca) throws AeriusException, SQLException {
    final CalculationJob cj = getExampleCalculationJob(jobIdentifier);

    if (nca) {
      cj.getCalculatorOptions().setCalculateIntermediateProgressReports();
    }
    return new TrackJobProgressHandler(getPMF(), cj);
  }

  private ConnectUser createUser(final Connection connection) throws SQLException, AeriusException {
    final ConnectUser createUser = new ConnectUser();
    createUser.setApiKey(TEST_EMAIL);
    createUser.setEmailAddress(TEST_EMAIL);
    createUser.setEnabled(true);

    try {
      ConnectUserRepository.createUser(connection, createUser);
    } catch (final AeriusException e) {
      if (e.getReason() != AeriusExceptionReason.USER_EMAIL_ADDRESS_ALREADY_EXISTS) {
        throw e;
      }
    }
    return ConnectUserRepository.getUserByEmailAddress(connection, TEST_EMAIL);
  }


  private static List<AeriusResultPoint> createResults() {
    final List<AeriusResultPoint> result = new ArrayList<>();
    addResult(result, 11500, 10000, 2.0);
    addResult(result, 12000, 10000, 3.0);
    addResult(result, 13000, 10000, 0.5);
    return result;
  }

  private static void addResult(final List<AeriusResultPoint> result, final int x, final int y, final double emission) {
    final AeriusResultPoint point = new AeriusResultPoint(0, 0, AeriusPointType.RECEPTOR, x, y);
    point.getEmissionResults().put(EmissionResultKey.NOX_DEPOSITION, emission);
    result.add(point);
  }
}
