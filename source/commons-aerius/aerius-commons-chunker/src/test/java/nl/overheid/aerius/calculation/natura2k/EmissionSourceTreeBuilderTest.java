/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.natura2k;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link EmissionSourceTreeBuilder}.
 */
class EmissionSourceTreeBuilderTest {

  private static final Point RELEVANT_POINT = new Point(10, 20);
  private static final Point OTHER_POINT = new Point(20, 1000);
  private static final SridPoint CHECK_POINT = new AeriusPoint(10, 1000);
  private static final EnumSet<SituationType> NONE_RELEVANT_SITUTATIONS = EnumSet.of(SituationType.OFF_SITE_REDUCTION, SituationType.COMBINATION_REFERENCE,
      SituationType.COMBINATION_PROPOSED);

  @Test
  void testExcludeRelevantSituations() throws AeriusException {
    final List<ScenarioSituation> situations = Stream.of(SituationType.values()).map(EmissionSourceTreeBuilderTest::create)
        .collect(Collectors.toList());

    final EmissionSourceTreeBuilder builder = EmissionSourceTreeBuilder.build(situations);
    final double distance = builder.getRelevantSituationSourcesTree().findShortestDistance(CHECK_POINT);

    assertEquals(980, distance, 0.001, "None relevant situations should not be included, therefor distance should be to relevenant point");
  }

  @Test
  void testOnlyNoneRelevantSituations() throws AeriusException {
    final List<ScenarioSituation> situations = NONE_RELEVANT_SITUTATIONS.stream().map(EmissionSourceTreeBuilderTest::create)
        .collect(Collectors.toList());

    final EmissionSourceTreeBuilder builder = EmissionSourceTreeBuilder.build(situations);
    final double distance = builder.getRelevantSituationSourcesTree().findShortestDistance(CHECK_POINT);

    assertEquals(10, distance, 0.001, "Only none relevant situations, therefor distances should be to other point distance");
  }

  private static ScenarioSituation create(final SituationType situationType) {
    final ScenarioSituation situation = new ScenarioSituation();
    final GenericEmissionSource source = new GenericEmissionSource();
    source.getEmissions().put(Substance.NOX, 10D);
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setProperties(source);
    feature.setGeometry(NONE_RELEVANT_SITUTATIONS.contains(situationType) ? OTHER_POINT : RELEVANT_POINT);
    situation.getEmissionSourcesList().add(feature);
    situation.setType(situationType);
    return situation;
  }
}
