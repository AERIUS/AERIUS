/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import nl.overheid.aerius.calculation.CalculationTestBase;
import nl.overheid.aerius.calculation.base.IncrementalWorkChunker;
import nl.overheid.aerius.calculation.base.WorkChunker;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.Vehicles;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FeaturePointToEnginePointUtil;

/**
 * Test class for {@link RBLWorkDistributor}.
 */
class RBLWorkDistributorTest extends CalculationTestBase {

  private static final int NUMBER_OF_SOURCES = 100;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    themeFactory = new RBLCalculatorFactory(getPMF());
  }

  @Test
  void testNSLWorkDistributor() throws Exception {
    assertPointsAndSources(List.of());
  }

  @Test
  void testNSLWorkDistributorWithMonitorSrm2Sources() throws Exception {
    final List<EmissionSourceFeature> monitorSrm2Sources = new ArrayList<>();
    monitorSrm2Sources.add(Mockito.mock(EmissionSourceFeature.class));
    assertPointsAndSources(monitorSrm2Sources);
  }

  private void assertPointsAndSources(final List<EmissionSourceFeature> monitorSrm2Sources) throws Exception {
    final CalculationJob calculationJob = getExampleCalculationJob();
    final List<AeriusPoint> points = FeaturePointToEnginePointUtil.convert(calculationJob.getScenarioCalculations().getCalculationPoints());

    final RBLWorkDistributor distributor = new RBLWorkDistributor(getPMF(), getCategories(getPMF()));
    final AtomicInteger pointsCounter = new AtomicInteger();
    final AtomicInteger sourcesCounter = new AtomicInteger();
    final WorkHandler<AeriusPoint> workHandler = new WorkHandler<>() {
      @Override
      public void work(final CalculationJob calculation, final JobPacket jobPacket, final Collection<AeriusPoint> calculationPointStore)
          throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
        pointsCounter.addAndGet(calculationPointStore.size());
        sourcesCounter.addAndGet(jobPacket.getSourcesPackets().stream().mapToInt(p -> p.getSources().size()).sum());
      }
    };
    final WorkChunker handler = new IncrementalWorkChunker<>(calculationJob, distributor, workHandler);

    handler.run((cj, p) -> {});
    assertEquals(points.size(), pointsCounter.get(), "Number of points in should be same as passed to handler");
    assertEquals(NUMBER_OF_SOURCES, sourcesCounter.get(), "Number of sources in should be same as passed to handler");
  }

  @Override
  protected List<EmissionSourceFeature> createSources() {
    final List<EmissionSourceFeature> emissionSources = new ArrayList<>();

    for (int i = 1; i <= 100; i++) {
      final EmissionSourceFeature feature = new EmissionSourceFeature();
      feature.setId(String.valueOf(i));

      final LineString ls = new LineString();
      ls.setCoordinates(new double[][] {{1 + i, 2 + i,}, {3 + i, 4 + i}});
      feature.setGeometry(ls);

      final RoadEmissionSource es = new SRM2RoadEmissionSource();
      es.setGmlId(feature.getId());
      es.setEmissions(Map.of(Substance.NO2, 100.0));
      es.setRoadAreaCode("NL");
      es.setRoadTypeCode("FREEWAY");
      final StandardVehicles vehicle = new StandardVehicles();
      final ValuesPerVehicleType valuePerVehicleType = new ValuesPerVehicleType();
      valuePerVehicleType.setVehiclesPerTimeUnit(1000);
      vehicle.getValuesPerVehicleTypes().put("HEAVY_FREIGHT", valuePerVehicleType);
      vehicle.setTimeUnit(TimeUnit.DAY);
      final List<Vehicles> vehicles = List.of(vehicle);
      es.setSubSources(vehicles);

      feature.setProperties(es);
      emissionSources.add(feature);
    }

    return emissionSources;
  }
}
