/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.mock.TestInputBuilder;
import nl.overheid.aerius.calculation.theme.own2000.ProjectCalculationExportHandler;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * Test class for {@link ProjectCalculationExportHandler}.
 */
class ProjectCalculationExportHandlerIT extends CalculationITBase {

  private static final int GENERIC_SECTOR = 1800;
  private static final int SRM2_SECTOR = 3100;
  private static final String REFERENCE_FILE_NH3_JSON = "JsonOutput%sNH3HandlerTest.json";
  private static final String REFERENCE_FILE_NOX_JSON = "JsonOutput%sNOXHandlerTest.json";
  private static final String SUMMARY = "Summary";
  private static final String SINGLE = "Single" + SUMMARY;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp((dsv, rdf) -> new OwN2000MockConnection(dsv, 0, input -> {}), AeriusCustomer.RIVM, WorkerType.OPS, WorkerType.ASRM);
  }

  @ParameterizedTest
  @Timeout(value = 100, unit = TimeUnit.SECONDS)
  @MethodSource("data")
  void testRunSingleReference(final List<SituationType> situationTypes, final String summaryName) throws Exception {
    assertTestRun(new OwN2000ProjectCalculationTestInputBuilder(situationTypes).createInputData(), situationTypes.size());
    assertSituationResultFiles(situationTypes, summaryName);
  }

  private static List<Arguments> data() {
    return List.of(
        Arguments.of(List.of(SituationType.REFERENCE, SituationType.PROPOSED), SUMMARY),
        Arguments.of(List.of(SituationType.REFERENCE), SINGLE),
        Arguments.of(List.of(SituationType.PROPOSED), SINGLE),
        Arguments.of(List.of(SituationType.OFF_SITE_REDUCTION), SINGLE)
        );
  }

  @Override
  protected void assertResults(final int nrOfCalculationsExpected) throws Exception {
    super.assertResults(nrOfCalculationsExpected);
  }

  private void assertSituationResultFiles(final List<SituationType> situationTypes, final String name) throws IOException {
    final File directory = WorkerUtil.getFolderForJob(calculationJob.getJobIdentifier());

    for (final SituationType situationType : situationTypes) {
      assertSituationResults(directory, String.format(REFERENCE_FILE_NH3_JSON, "Single"), situationType.name() + "_substance-NH3");
      assertSituationResults(directory, String.format(REFERENCE_FILE_NOX_JSON, "Single"), situationType.name() + "_substance-NOX");
    }
    // Test Summary results
    assertSituationResults(directory, String.format(REFERENCE_FILE_NH3_JSON, name), "SUMMARY_substance-NH3");
    assertSituationResults(directory, String.format(REFERENCE_FILE_NOX_JSON, name), "SUMMARY_substance-NOX");
  }

  private void assertSituationResults(final File directory, final String filename, final String name) throws IOException {
    validateAssertEquals(getClass().getResourceAsStream(filename).readAllBytes(), Files
        .readAllBytes(new File(directory, calculationJob.getName() + "_situation-" + name + "_resulttype-DEPOSITION.json").toPath()),
        "Json situation " + name + " file should match");
  }

  private void validateAssertEquals(final byte[] refrence, final byte[] result, final String msg) {
    assertEquals(sort(refrence), sort(result), msg);
  }

  private String sort(final byte[] input) {
    final String string = new String(input, StandardCharsets.UTF_8);
    final List<String> list = Arrays.asList(string.split("\n")); // List.of is immutable and can't be sorted.

    Collections.sort(list);
    return list.stream().collect(Collectors.joining("\n"));
  }

  private static class OwN2000TestInputBuilder extends TestInputBuilder {

    public OwN2000TestInputBuilder() {
      super(Theme.OWN2000);
    }

    @Override
    public CalculationInputData createInputData() {
      final CalculationInputData inputData = super.createInputData();
      inputData.setName("test");
      inputData.setExportType(ExportType.REGISTER_JSON_PROJECT_CALCULATION);
      return inputData;
    }

    @Override
    protected void setSources(final List<EmissionSourceFeature> sources, final double emissionFactor) {
      final LineString geometry = new LineString();
      geometry.setCoordinates(new double[][] {{TestDomain.XCOORD_1, TestDomain.YCOORD_1}, {TestDomain.XCOORD_1 + 100, TestDomain.YCOORD_1}});

      sources.addAll(List.of(createSource(emissionFactor, geometry, SRM2_SECTOR),
          createSource(emissionFactor, geometry, GENERIC_SECTOR)));
    }

    @Override
    protected CalculationSetOptions setCalculationSetOptions(final CalculationSetOptions options) {
      options.setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
      return options;
    }

    protected static List<EmissionSourceFeature> createPointSourceList(final double emissionFactor) {
      final Point geometry = new Point(TestDomain.XCOORD_1, TestDomain.YCOORD_1);

      return List.of(createSource(emissionFactor, geometry, GENERIC_SECTOR));
    }

    private static EmissionSourceFeature createSource(final double emissionFactor, final Geometry geometry, final int sectorId) {
      final GenericEmissionSource emissionSource = new GenericEmissionSource();
      emissionSource.setSectorId(sectorId);
      emissionSource.getEmissions().put(Substance.NH3, 10.0 * emissionFactor);
      emissionSource.getEmissions().put(Substance.NOX, 20.0 * emissionFactor);
      return TestDomain.getSource("1", geometry, "SomeSource", emissionSource);
    }
  }

  private static class OwN2000ProjectCalculationTestInputBuilder extends OwN2000TestInputBuilder {

    private final List<SituationType> situationTypes;

    public OwN2000ProjectCalculationTestInputBuilder(final List<SituationType> situationTypes) {
      this.situationTypes = situationTypes;
    }

    @Override
    protected void setScenario(final Scenario scenario) {
      final AtomicInteger emissionFactor = new AtomicInteger();

      situationTypes.forEach(st -> addSituation(scenario, st, emissionFactor.addAndGet(1)));
    }

    private void addSituation(final Scenario scenario, final SituationType situationType, final double emissionFactor) {
      final ScenarioSituation situation = new ScenarioSituation();
      situation.getEmissionSourcesList().clear();
      situation.getEmissionSourcesList().addAll(createPointSourceList(emissionFactor));
      situation.setType(situationType);
      situation.setYear(TestDomain.YEAR);
      scenario.getSituations().add(situation);
    }
  }
}
