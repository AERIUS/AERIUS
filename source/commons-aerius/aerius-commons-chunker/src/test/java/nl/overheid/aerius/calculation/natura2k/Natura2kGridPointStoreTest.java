/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.natura2k;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.grid.GridPointStore;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.theme.own2000.Natura2kGridPointStore;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Test class for {@link Natura2kGridPointStore}.
 */
class Natura2kGridPointStoreTest extends BaseDBTest {

  @Test
  void testFillPointStore() throws SQLException {
    final Natura2KReceptorLoader loader = new Natura2KReceptorLoader(RECEPTOR_UTIL, new GridSettings(RECEPTOR_GRID_SETTINGS));
    final Natura2kGridPointStore store = loader.fillPointStore(getConnection());
    final int n2kId = 65;
    final Collection<Integer> gridIds = store.getGridIds(n2kId);
    assertNotEquals(0, gridIds.size(), "Grid id's for natura area should not be empty");
    assertFalse(store.getGridCell(gridIds.iterator().next()).isEmpty(),
        "Grid id for first grid store for grid in area should not be empty");
    assertNotEquals(0, store.getSize(n2kId), "Number of receptors in nature area should not be 0");
  }

  @Test
  void testSort() {
    final Natura2kGridPointStore store = new Natura2kGridPointStore(RECEPTOR_UTIL, new GridSettings(RECEPTOR_GRID_SETTINGS));
    addPoint(store, 4760704, 1);
    final AeriusPoint level5a = addPoint(store, 4893745, 5);
    assertTrue(RECEPTOR_UTIL.isReceptorAtZoomLevel(level5a, RECEPTOR_GRID_SETTINGS.getHexagonZoomLevels().get(4)),
        "Should return receptor at zoom level.");
    store.add(level5a);
    final AeriusPoint level5b = addPoint(store, 4893761, 5);
    store.sortPointStore();
    final Iterator<Entry<Integer, Collection<AeriusPoint>>> iterator = store.entrySet().iterator();
    final List<AeriusPoint> list = (List<AeriusPoint>) iterator.next().getValue();
    assertEquals(level5a, list.get(0), "5a should be first");
    assertEquals(level5b, list.get(1), "5b should be second");
  }

  private AeriusPoint addPoint(final GridPointStore store, final int id, final int level) {
    final AeriusPoint point = setCoordinatesFromId(new AeriusPoint(id));
    store.add(point);
    assertEquals(level, RECEPTOR_UTIL.getZoomLevelForReceptor(point.getId()), "Receptor on zoom level");
    return point;
  }
}
