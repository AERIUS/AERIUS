/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;

import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.taskmanager.client.util.QueueHelper;
import nl.aerius.taskmanager.test.MockConnection;
import nl.aerius.taskmanager.test.MockedChannelFactory;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.ExportResult;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;

/**
 * Mocks a ADMS calculation for theme NCA.
 */
public class NCAMockConnection extends MockConnection {

  private static final double MINIMAL_DEPOSITION = 0.0001;
  private double resultFactor;
  private final double resultDecrementFactor;
  private final List<ADMSInputData> inputDataList = new ArrayList<>();

  public NCAMockConnection(final double resultFactor, final double resultDecrementFactor) {
    this.resultFactor = resultFactor;
    this.resultDecrementFactor = resultDecrementFactor;
  }

  @Override
  public Channel createChannel() throws IOException {
    return MockedChannelFactory.create(this::mockResults);
  }

  /**
   * Mocks a Channel to return results that with each run decrease in value. This can be used to mock deposition results in unit tests. Given the
   * points are with each run at a large distance to the sources.
   */
  private byte[] mockResults(final BasicProperties properties, final byte[] body) {
    synchronized (this) {
      try {
        final ADMSInputData input = (ADMSInputData) QueueHelper.bytesToObject(body);
        inputDataList.add(input);
        final Serializable result;

        if (input.getCommandType() == CommandType.CALCULATE) {
          result = mockCalculate(input);
        } else if (input.getCommandType() == CommandType.EXPORT) {
          result = mockExport(input);
        } else {
          return null;
        }
        return QueueHelper.objectToBytes(result);
      } catch (final IOException | ClassNotFoundException e) {
        return null;
      }
    }
  }

  public List<ADMSInputData> getADMSInputDataList() {
    return inputDataList;
  }

  private CalculationResult mockCalculate(final ADMSInputData input) {
    final CalculationResult cr = new CalculationResult(input.getChunkStats(), CalculationEngine.ADMS);
    final Set<Entry<Integer, Collection<EngineSource>>> entrySet = input.getEmissionSources().entrySet();

    for (final Entry<Integer, Collection<EngineSource>> entry : entrySet) {
      final List<AeriusResultPoint> results = new ArrayList<>();
      final Collection<ADMSCalculationPoint> receptors = input.getReceptors();

      for (final AeriusPoint receptor : receptors) {
        results.add(toResultPoint(receptor, resultFactor, input.getEmissionResultKeys()));
      }
      cr.put(entry.getKey(), results);
    }
    resultFactor = Math.max(MINIMAL_DEPOSITION, resultFactor * resultDecrementFactor);
    return cr;
  }

  private ExportResult mockExport(final ADMSInputData input) {
    return new ExportResult("1234");
  }

  private final AeriusResultPoint toResultPoint(final AeriusPoint receptor, final double resultFactor, final Set<EmissionResultKey> keys) {
    final AeriusResultPoint resultPoint = new AeriusResultPoint(receptor);

    for (final EmissionResultKey resultKey : keys) {
      resultPoint.setEmissionResult(resultKey, resultFactor);
    }
    return resultPoint;
  }
}
