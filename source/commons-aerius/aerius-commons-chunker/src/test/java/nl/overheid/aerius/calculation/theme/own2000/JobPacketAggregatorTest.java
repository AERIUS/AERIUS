/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link JobPacketAggregator}.
 */
class JobPacketAggregatorTest extends BaseDBTest {

  private static final int NR_OF_SOURCES = 500;
  private static final double PRECISION = 000.1;

  private GridUtil gridUtil;
  private DefaultCalculationEngineProvider defaultCalculationEngineProvider;
  private CalculationJob calculationJob;
  private AtomicReference<Double> sum;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    try (Connection con = getConnection()) {
      defaultCalculationEngineProvider = new DefaultCalculationEngineProvider(SectorRepository.getSectorProperties(con));
    }
    gridUtil = new GridUtil(new GridSettings(RECEPTOR_GRID_SETTINGS));
    sum = new AtomicReference<Double>(0.0);
    calculationJob = createCalculationJob(sum);
  }

  @Test
  void testCompareAggragrationStacked() throws SQLException, AeriusException {
    assertCompareAggragration(true);
  }

  @Test
  void testCompareAggragrationNonStacked() throws SQLException, AeriusException {
    assertCompareAggragration(false);
  }

  void assertCompareAggragration(final boolean stacking) throws SQLException, AeriusException {
    calculationJob.getCalculationSetOptions().setStacking(stacking);
    final JobPacketAggregator aggregated = initAggregator(calculationJob, true);
    final JobPacketAggregator nonAggregated = initAggregator(calculationJob, false);
    final int cells[] = {1, 97, 129, 161, 193, 225, 257, 385, 417, 449, 481, 513, 577,};

    for (int i = 0; i < cells.length; i++) {
      final int cellId = cells[i];
      final double aggregatedSum = sum(aggregated, cellId);
      final double nonAggregatedSum = sum(nonAggregated, cellId);
      assertEquals(sum.get(), nonAggregatedSum, PRECISION, "Emission sum should be same or sum vs non aggregated.");
      assertEquals(aggregatedSum, nonAggregatedSum, PRECISION, "Emission sum should be same or aggregated vs non aggregated.");
    }
  }

  private double sum(final JobPacketAggregator aggregator, final int cellId) {
    return aggregator.getSourcesFor(cellId).stream()
        .flatMap(p -> p.getSourcesPackets().stream())
        .flatMap(p -> p.getSources().stream()).mapToDouble(s -> ((EngineEmissionSource) s).getEmission(Substance.NH3)).sum();
  }

  private JobPacketAggregator initAggregator(final CalculationJob calculationJob, final boolean aggregate) throws SQLException, AeriusException {
    final JobPacketAggregator aggregator = new JobPacketAggregator(getPMF(), getCategories(getPMF()), gridUtil);
    calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions().setForceAggregation(aggregate);
    aggregator.init(calculationJob);
    return aggregator;
  }

  protected List<EmissionSourceFeature> createSources(final AtomicReference<Double> sum) {
    final List<EmissionSourceFeature> emissionSources = new ArrayList<>();

    sum.set(addSources(emissionSources, 1800, NR_OF_SOURCES)
        + addSources(emissionSources, 4120, NR_OF_SOURCES)
        + addSources(emissionSources, 4600, NR_OF_SOURCES)
        );
    return emissionSources;
  }

  private double addSources(final List<EmissionSourceFeature> features, final int sectorId, final int nrOfSources) {
    final Random random = new Random();
    double sum = 0.0;

    for (int i = 1; i <= nrOfSources; i++) {
      final EmissionSourceFeature feature = new EmissionSourceFeature();
      feature.setId(String.valueOf(i));
      feature.setGeometry(new Point(100_000 + random.nextInt(100_000), 400_000 + random.nextInt(150_000)));
      final GenericEmissionSource emissionSource = new GenericEmissionSource();
      final double emission = random.nextDouble();
      sum += emission;
      emissionSource.getEmissions().put(Substance.NH3, emission);
      emissionSource.setSectorId(sectorId);
      feature.setProperties(emissionSource);
      features.add(feature);
    }
    return sum;
  }

  private CalculationJob createCalculationJob(final AtomicReference<Double> sum) {
    final Scenario scenario = new Scenario(Theme.OWN2000);
    final ScenarioSituation situation = new ScenarioSituation();
    situation.getEmissionSourcesList().addAll(createSources(sum));
    scenario.getSituations().add(situation);
    final CalculationSetOptions options = new CalculationSetOptions();
    options.setCalculationMethod(CalculationMethod.CUSTOM_POINTS);
    options.getSubstances().add(Substance.NH3);
    final Collection<Integer> sectors = new HashSet<>();
    for (final EmissionSourceFeature emissionSource : situation.getEmissionSourcesList()) {
      sectors.add(emissionSource.getProperties().getSectorId());
    }
    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);
    final int calculationId = 1;
    scenarioCalculations.getCalculations().forEach(calculation -> calculation.setCalculationId(calculationId));
    scenario.setOptions(options);
    final CalculationJob cj = new CalculationJob(new JobIdentifier(UUID.randomUUID().toString()), null, scenarioCalculations, null, "queue");
    cj.setProvider(defaultCalculationEngineProvider);
    return cj;
  }
}
