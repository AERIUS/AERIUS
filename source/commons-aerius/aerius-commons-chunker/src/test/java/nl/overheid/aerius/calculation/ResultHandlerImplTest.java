/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.WorkMonitor;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link ResultHandlerImpl}.
 */
class ResultHandlerImplTest extends BaseDBTest {

  private WorkMonitor monitor;
  private CalculationResultQueue queue;
  private CalculationResultHandler handler;
  private AtomicBoolean handlerCalled;
  private ResultHandler resultHandler;
  private Exception exception;

  @BeforeEach
  void before() throws AeriusException {
    exception = null;
    handlerCalled = new AtomicBoolean();
    monitor = Mockito.mock(WorkMonitor.class);
    final WorkKey workKey = new WorkKey("", new JobIdentifier("1"), 1, CalculationEngine.OPS);
    Mockito.when(monitor.getWorkKey(anyString())).then(invocation -> workKey);
    queue = Mockito.mock(CalculationResultQueue.class);
    final List<JobPacket> jobPackets = new ArrayList<>();
    final JobPacket job = new JobPacket(1, 2030, List.of());
    jobPackets.add(job);
    queue.init(null, List.of(new WorkPacket(null, jobPackets)));
    handler = Mockito.mock(CalculationResultHandler.class);
    Mockito.doAnswer(invocation -> {
      handlerCalled.set(true);
      return null;
    }).when(handler).onSectorResults(anyMap(), anyInt(), any());
    Mockito.doAnswer(invocation -> {
      handlerCalled.set(true);
      return null;
    }).when(handler).onSectorResults(anyMap(), anyInt(), any());
    resultHandler = new ResultHandlerImpl(getPMF(), monitor, queue, handler) {
      @Override
      protected void failIfJobCancelledByUser(final String correlationId) throws AeriusException {
        // Don't check if job failed because there is no job in the database, therefore it will fail.
      };
    };
    Mockito.doAnswer(invocation -> {
      exception = invocation.getArgument(1, Exception.class);
      return null;
    }).when(monitor).taskFailed(anyString(), any());
  }

  @Test
  void testSetResultUpdateHandler() {
    final AtomicBoolean resultHandlerCalled = new AtomicBoolean();
    resultHandler.addResultUpdateHandler((calculationid, results) -> resultHandlerCalled.set(true));
    final Map<Integer, List<AeriusResultPoint>> mapResults = new HashMap<>();
    mapResults.put(1, createResultPointList());
    resultHandler.onResult("1", CalculationEngine.OPS, mapResults);
    assertTrue(resultHandlerCalled.get(), "UpdateHandler should have been called");
    assertNull(exception, "No exception should be triggered");
  }

  @Test
  void testOnResult() throws AeriusException {
    final Map<Integer, List<AeriusResultPoint>> mapResults = new HashMap<>();
    mapResults.put(1, createResultPointList());
    resultHandler.onResult("1", CalculationEngine.OPS, mapResults);
    assertTrue(handlerCalled.get(), "CalculationResultHandler should have been called");
    assertNull(exception, "No exception should be triggered");
  }

  @Test
  void testException() throws AeriusException {
    final Map<Integer, List<AeriusResultPoint>> mapResults = new HashMap<>();
    mapResults.put(1, createResultPointList());
    Mockito.doThrow(new RuntimeException("Test Exception")).when(handler).onSectorResults(anyMap(), anyInt(), any());
    resultHandler.onResult("1", CalculationEngine.OPS, mapResults);
    assertNotNull(exception, "Exception should be triggered");
  }

  private static List<AeriusResultPoint> createResultPointList () {
    return List.of(new AeriusResultPoint(new AeriusPoint()));
  }
}
