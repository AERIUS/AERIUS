/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.adms.domain.ADMSSubReceptor;
import nl.overheid.aerius.archive.ArchiveProxy;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveProject;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ArchiveCollectorHandler}.
 */
@ExtendWith(MockitoExtension.class)
class ArchiveCollectorHandlerTest {

  private @Mock HttpClientProxy httpClientProxy;
  private @Captor ArgumentCaptor<List<AeriusResultPoint>> resultsCaptor;
  private @Captor ArgumentCaptor<ArchiveMetaData> metadataCaptor;
  private @Captor ArgumentCaptor<HttpPost> httpPostCaptor;
  private ArchiveCollectorHandler handler;

  @BeforeEach
  void setup() throws SQLException, IOException {
    final ArchiveProxy proxy = new ArchiveProxy(httpClientProxy, "", "apiKey");
    handler = spy(new ArchiveCollectorHandler(null, proxy, 0, 2030));
    doAnswer(a -> null).when(handler).insertArchiveContribution(resultsCaptor.capture());
    doAnswer(a -> null).when(handler).insertArchiveMetadata(metadataCaptor.capture());
  }

  @Test
  void testStorePoints() throws SQLException, IOException, HttpException, AeriusException {
    final String result = readJson("archive_result_1.json");
    doReturn(result).when(httpClientProxy).execute(httpPostCaptor.capture());

    // Don't expect receptor 1, subpoint 6 to check that this is properly ignored
    // Do expect 4 and 5, to see if averages work OK.
    final Collection<AeriusPoint> points = List.of(point(1), subPoint(1, 0, 0), subPoint(1, 4, 2), subPoint(1, 5, 2), point(2));
    handler.addPoints(points);
    handler.requestArchiveData();
    verify(httpClientProxy).execute(any());
    verify(handler).insertArchiveContribution(any());

    final StringEntity entity = (StringEntity) httpPostCaptor.getValue().getEntity();
    try (final ByteArrayOutputStream os = new ByteArrayOutputStream()) {
      entity.writeTo(os);
      final String pointsSendToApi = os.toString();
      final String expectedInput = readJson("archive_input_1.json");
      assertEquals(expectedInput, pointsSendToApi, "");
    }
    final List<AeriusResultPoint> resultPointsFromApi = resultsCaptor.getValue();
    assertEquals(5, resultPointsFromApi.size(), "");
    assertResult(resultPointsFromApi, 1, 0, AeriusPointType.RECEPTOR, 10, EmissionResultKey.NH3_DEPOSITION);
    assertResult(resultPointsFromApi, 4, 1, AeriusPointType.SUB_RECEPTOR, 15, EmissionResultKey.NH3_CONCENTRATION);
    assertResult(resultPointsFromApi, 5, 1, AeriusPointType.SUB_RECEPTOR, 20, EmissionResultKey.NH3_CONCENTRATION);
    // Average of concentration should also be present at receptor level, and should ignore points that were not expected in any case.
    assertResult(resultPointsFromApi, 1, 0, AeriusPointType.RECEPTOR, 17.5, EmissionResultKey.NH3_CONCENTRATION);
    assertResult(resultPointsFromApi, 2, 0, AeriusPointType.RECEPTOR, 30, EmissionResultKey.NH3_DEPOSITION);
    assertResult(resultPointsFromApi, 2, 0, AeriusPointType.RECEPTOR, 33, EmissionResultKey.NH3_CONCENTRATION);
    // If there are no subreceptors for a receptor, a subreceptor with level 0 should be returned as well, with the concentrations.
    assertResult(resultPointsFromApi, 0, 2, AeriusPointType.SUB_RECEPTOR, 33, EmissionResultKey.NH3_CONCENTRATION);

    final ArchiveMetaData metaDataFromApi = metadataCaptor.getValue();
    assertNotNull(metaDataFromApi, "Metadata object shouldn't be null");
    assertNotNull(metaDataFromApi.getRetrievalDateTime(), "Metadata retrieval date time shouldn't be null");
    assertTrue(metaDataFromApi.getArchiveProjects().isEmpty(), "No projects in metadata in test file, so empty list expected");
  }

  @Test
  void testStoreMetaData() throws SQLException, IOException, HttpException, AeriusException {
    final String result = readJson("archive_result_2.json");
    doReturn(result).when(httpClientProxy).execute(httpPostCaptor.capture());

    final Collection<AeriusPoint> points = List.of(point(1), subPoint(1, 0, 0), subPoint(1, 5, 2), point(2));
    handler.addPoints(points);
    handler.requestArchiveData();
    verify(httpClientProxy).execute(any());
    verify(handler).insertArchiveContribution(any());

    final StringEntity entity = (StringEntity) httpPostCaptor.getValue().getEntity();
    try (final ByteArrayOutputStream os = new ByteArrayOutputStream()) {
      entity.writeTo(os);
      final String pointsSendToApi = os.toString();
      final String expectedInput = readJson("archive_input_1.json");
      assertEquals(expectedInput, pointsSendToApi, "");
    }
    final List<AeriusResultPoint> resultPointsFromApi = resultsCaptor.getValue();
    assertEquals(2, resultPointsFromApi.size(), "");
    assertResult(resultPointsFromApi, 0, 1, AeriusPointType.SUB_RECEPTOR, 10, EmissionResultKey.NH3_CONCENTRATION);
    assertResult(resultPointsFromApi, 1, 0, AeriusPointType.RECEPTOR, 10, EmissionResultKey.NH3_CONCENTRATION);

    final ArchiveMetaData metaDataFromApi = metadataCaptor.getValue();
    assertNotNull(metaDataFromApi, "Metadata object shouldn't be null");
    assertNotNull(metaDataFromApi.getRetrievalDateTime(), "Metadata retrieval date time shouldn't be null");
    assertEquals(2, metaDataFromApi.getArchiveProjects().size(), "number of projects in metadata");
    assertArchiveProject(metaDataFromApi.getArchiveProjects(), "some-ref-1", "firstProject", "DEV", 9.2, 38.1);
    assertArchiveProject(metaDataFromApi.getArchiveProjects(), "some-ref-2", "sec0ndProJect", "NON_DEV", 0.0, 0.0);
  }

  private void assertResult(final List<AeriusResultPoint> points, final int id, final Integer parentId, final AeriusPointType pointType,
      final double value, final EmissionResultKey emissionResultKey) {
    final Optional<AeriusResultPoint> optionalPoint = points.stream()
        .filter(p -> p.getId() == id && p.getParentId() == parentId && p.getPointType() == pointType)
        .findFirst();

    assertTrue(optionalPoint.isPresent(), "Could not find point for id " + id + " with parentId " + parentId);
    assertEquals(value, optionalPoint.get().getEmissionResult(emissionResultKey), "Expected returned " + emissionResultKey);
  }

  private void assertArchiveProject(final List<ArchiveProject> projects, final String id, final String name, final String version,
      final double expectedNH3, final double expectedNOx) {
    final Optional<ArchiveProject> optionalProject = projects.stream().filter(p -> p.getId().equals(id)).findFirst();

    assertTrue(optionalProject.isPresent(), "Could not find project for id " + id);
    final ArchiveProject project = optionalProject.get();
    assertEquals(name, project.getName(), "Name of project " + id);
    assertEquals(version, project.getAeriusVersion(), "Version of project " + id);
    assertEquals(2, project.getNetEmissions().size(), "Number of emissions");
    assertEquals(expectedNH3, project.getNetEmissions().get(Substance.NH3), "NH3 emission");
    assertEquals(expectedNOx, project.getNetEmissions().get(Substance.NOX), "NOX emission");
  }

  private String readJson(final String filename) throws IOException {
    return new String(getClass().getResourceAsStream(filename).readAllBytes(), StandardCharsets.UTF_8).replaceAll("[ \\n\\r]", "");
  }

  private AeriusPoint subPoint(final int parentId, final int subId, final int level) {
    final ADMSSubReceptor sp = new ADMSSubReceptor(new AeriusPoint(subId), AeriusPointType.SUB_RECEPTOR, level);

    sp.setParentId(parentId);
    return sp;
  }

  private AeriusPoint point(final int id) {
    return new AeriusPoint(id);
  }
}
