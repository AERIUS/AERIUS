/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.overlapping;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.shared.geo.EPSG;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * Test class for {@link OverlappingHexagonDetector}.
 */
class OverlappingHexagonDetectorTest {

  private static final int NUMBER_OF_RECEPTORS = 10_000_000;
  // 3000000 - 7000000 is a band of receptors in middle of NL
  // Changing to 1 - NUMBER_OF_RECEPTORS should work, but takes more memory.
  private static final int START_RECEPTOR_ID = 3000000;
  private static final int END_RECEPTOR_ID = 7000000;
  private static final int NUMBER_OF_RECEPTORS_SUBSET = END_RECEPTOR_ID - START_RECEPTOR_ID + 1;

  private static final HexagonZoomLevel ZOOM_LEVEL_1 = new HexagonZoomLevel(1, 10_000);
  private static final BBox RECEPTOR_BBOX = new BBox(3604, 296800, 287959, 629300);
  private static final ReceptorGridSettings RGS = new ReceptorGridSettings(RECEPTOR_BBOX, EPSG.RDNEW, 1529,
      new ArrayList<>(List.of(ZOOM_LEVEL_1)));

  private static List<AeriusPoint> receptors;

  static OverlappingHexagonDetector detector;

  @BeforeAll
  static void beforeAll() {
    // For these tests just add all receptors that cover the Netherlands
    final ReceptorUtil receptorUtil = new ReceptorUtil(RGS);
    receptors = new ArrayList<>(NUMBER_OF_RECEPTORS);
    for (int i = START_RECEPTOR_ID; i <= END_RECEPTOR_ID; i++) {
      final Point point = receptorUtil.getPointFromReceptorId(i);
      receptors.add(new AeriusPoint(i, 0, AeriusPointType.RECEPTOR, point.getX(), point.getY()));
    }
    detector = new OverlappingHexagonDetector(receptors, 100);
  }

  @Test
  void testCurrentDistancePoints() throws AeriusException {
    final int distance = 25_000;

    final EmissionSourceFeature source = new EmissionSourceFeature();
    source.setGeometry(new Point(141492, 458987));
    final List<AeriusPoint> overlappingReceptors = detector.detect(List.of(source), distance);
    assertEquals(196350, overlappingReceptors.size(), "Number of receptors within range of 1 source");

    final EmissionSourceFeature sourceAt1km = new EmissionSourceFeature();
    sourceAt1km.setGeometry(new Point(140492, 458987));
    final List<AeriusPoint> overlappingReceptors2 = detector.detect(List.of(source, sourceAt1km), distance);
    assertEquals(191356, overlappingReceptors2.size(), "Number of receptors within range of 2 sources that are 1km apart");

    final EmissionSourceFeature sourceAt100km = new EmissionSourceFeature();
    sourceAt100km.setGeometry(new Point(241492, 458987));
    final List<AeriusPoint> overlappingReceptors3 = detector.detect(List.of(source, sourceAt1km, sourceAt100km), distance);
    assertEquals(0, overlappingReceptors3.size(), "Number of receptors within range of 3 sources where 2 are 100km apart");
  }

  @ParameterizedTest
  @MethodSource("casesOverlapPoints")
  void testOverlapPoints(final List<Point> sourcePoints, final int distance, final int expectedNrOfResults) throws AeriusException {
    final List<EmissionSourceFeature> sources = sourcePoints.stream()
        .map(OverlappingHexagonDetectorTest::source)
        .collect(Collectors.toList());
    final List<AeriusPoint> overlappingReceptors = detector.detect(sources, distance);
    assertEquals(expectedNrOfResults, overlappingReceptors.size(), "Number of receptors within range of supplied sources");
  }

  private static Stream<Arguments> casesOverlapPoints() {
    // Validation possible through postgres if receptors table is fully loaded (which isn't by default):
    // SELECT count(*) FROM receptors
    // WHERE st_distance(geometry, 'SRID=28992;POINT(141492 458987)') < 1000
    // AND st_distance(geometry, 'SRID=28992;POINT(143392 458987)') < 1000;
    final Point mainSource = new Point(141492, 458987);
    final Point sourceAt500mWest = new Point(140992, 458987);
    final Point sourceAt500mNorth = new Point(141492, 459487);
    final Point sourceAt1km = new Point(142492, 458987);
    final Point sourceAtJustUnder2km = new Point(143392, 458987);
    final Point sourceAt2km = new Point(143492, 458987);
    final int distance = 1_000;
    return Stream.of(
        Arguments.of(List.of(), distance, NUMBER_OF_RECEPTORS_SUBSET),
        Arguments.of(List.of(mainSource), distance, 316),
        Arguments.of(List.of(mainSource, sourceAt500mWest), distance, 218),
        Arguments.of(List.of(mainSource, sourceAt500mNorth), distance, 214),
        Arguments.of(List.of(mainSource, sourceAt500mWest, sourceAt500mNorth), distance, 160),
        Arguments.of(List.of(mainSource, sourceAt1km), distance, 124),
        Arguments.of(List.of(mainSource, sourceAtJustUnder2km), distance, 5),
        Arguments.of(List.of(mainSource, sourceAt2km), distance, 0));
  }

  private static EmissionSourceFeature source(final Geometry geometry) {
    final EmissionSourceFeature source = new EmissionSourceFeature();
    source.setGeometry(geometry);
    return source;
  }

  @Test
  void testSmallWellKnownPoints() throws AeriusException {
    final int distance = 200;

    final EmissionSourceFeature source = new EmissionSourceFeature();
    source.setGeometry(new Point(141492, 458987));
    final List<AeriusPoint> overlappingReceptors = detector.detect(List.of(source), distance);
    assertEquals(12, overlappingReceptors.size(), "Number of receptors within range of 1 source");

    final Set<Integer> expectedPointsWithin1Source = Set.of(
        4613734,
        4613735,
        4615263,
        4615264,
        4616792,
        4616793,
        4619850,
        4619851,
        4621380,
        4612206,
        4618321,
        4618322);

    validateExpected(expectedPointsWithin1Source, overlappingReceptors);

    final EmissionSourceFeature sourceAt100m = new EmissionSourceFeature();
    sourceAt100m.setGeometry(new Point(141392, 458987));
    final List<AeriusPoint> overlappingReceptorsAt100m = detector.detect(List.of(source, sourceAt100m), distance);
    assertEquals(8, overlappingReceptorsAt100m.size(), "Number of receptors within range of 2 sources that are 100m apart");

    final Set<Integer> expectedPointsAt100m = Set.of(
        4615263,
        4615264,
        4616792,
        4618321,
        4618322,
        4619850,
        4613734,
        4612206);

    validateExpected(expectedPointsAt100m, overlappingReceptorsAt100m);

    final EmissionSourceFeature sourceAt141m = new EmissionSourceFeature();
    sourceAt141m.setGeometry(new Point(141392, 458887));
    final List<AeriusPoint> overlappingReceptorsAt141m = detector.detect(List.of(source, sourceAt141m), distance);
    assertEquals(6, overlappingReceptorsAt141m.size(), "Number of receptors within range of 2 sources that are 141m apart");

    final Set<Integer> expectedPointsAt141m = Set.of(
        4613734,
        4616792,
        4618321,
        4615263,
        4615264,
        4612206);

    validateExpected(expectedPointsAt141m, overlappingReceptorsAt141m);

    final EmissionSourceFeature sourceAt200m = new EmissionSourceFeature();
    sourceAt200m.setGeometry(new Point(141292, 458987));
    final List<AeriusPoint> overlappingReceptorsAt200m = detector.detect(List.of(source, sourceAt200m), distance);
    assertEquals(5, overlappingReceptorsAt200m.size(), "Number of receptors within range of 2 sources that are 200m apart");

    final Set<Integer> expectedPointsAt200m = Set.of(
        4613734,
        4615263,
        4618321,
        4619850,
        4616792);

    validateExpected(expectedPointsAt200m, overlappingReceptorsAt200m);
  }

  @ParameterizedTest
  @MethodSource("casesOverlapMixing")
  void testOverlapMixing(final List<Geometry> sourceGeometries, final int distance, final int expectedNrOfResults) throws AeriusException {
    final List<EmissionSourceFeature> sources = sourceGeometries.stream()
        .map(OverlappingHexagonDetectorTest::source)
        .collect(Collectors.toList());
    final List<AeriusPoint> overlappingReceptors = detector.detect(sources, distance);
    assertEquals(expectedNrOfResults, overlappingReceptors.size(), "Number of receptors within range of supplied sources");
  }

  private static Stream<Arguments> casesOverlapMixing() {
    final Point pointSource = new Point(141492, 458987);
    final LineString lineSource = new LineString();
    lineSource.setCoordinates(new double[][] {
        {pointSource.getX() - 100, pointSource.getY()}, {pointSource.getX(), pointSource.getY() - 100}});
    final Polygon polygonSource = new Polygon();
    polygonSource.setCoordinates(new double[][][] {{
        {pointSource.getX() + 200, pointSource.getY() + 200},
        {pointSource.getX() + 200, pointSource.getY() + 400},
        {pointSource.getX() + 400, pointSource.getY() + 400},
        {pointSource.getX() + 400, pointSource.getY() + 200},
        {pointSource.getX() + 200, pointSource.getY() + 200}}});
    final int distance = 1_000;
    return Stream.of(
        Arguments.of(List.of(pointSource), distance, 316),
        // line sources are ignored in detecting overlapping receptors
        // if a situation only contains line sources, all receptors are overlapping (no edge effect)
        Arguments.of(List.of(lineSource), distance, NUMBER_OF_RECEPTORS_SUBSET),
        // polygon sources are converted into points, where all points have to be inside the range
        // this will result in fewer receptors than a situation where only part of the geometry is in range
        Arguments.of(List.of(polygonSource), distance, 255),
        // these two sets should have the same number of results as a point source only since line sources are ignored
        Arguments.of(List.of(pointSource, lineSource), distance, 316),
        Arguments.of(List.of(lineSource, pointSource), distance, 316),
        // as line sources are ignored and order does not matter, these three should result in the same set of receptors
        Arguments.of(List.of(pointSource, polygonSource), distance, 200),
        Arguments.of(List.of(polygonSource, pointSource), distance, 200),
        Arguments.of(List.of(polygonSource, lineSource, pointSource), distance, 200));
  }

  @Test
  void testIncorrectGeometries() throws AeriusException {
    final int distance = 1_000;

    final Polygon unfinishedPolygon = new Polygon();
    unfinishedPolygon.setCoordinates(new double[][][] {{
        {142000, 460000},
        {142100, 460000},
        {142000, 460200},
    }});
    final List<EmissionSourceFeature> sourceUnfinishedPolygon = List.of(source(unfinishedPolygon));
    final AeriusException exception = assertThrows(AeriusException.class, () -> detector.detect(sourceUnfinishedPolygon, distance),
        "Should throw an exception when polygon is incorrect");
    assertEquals(ImaerExceptionReason.GEOMETRY_INVALID, exception.getReason(), "Should throw geometry invalid reason");
  }

  private void validateExpected(final Set<Integer> expectedIds, final List<AeriusPoint> overlappingReceptors) {
    final Set<Integer> overlappingReceptorIds = overlappingReceptors.stream().map(AeriusPoint::getId).collect(Collectors.toSet());
    assertEquals(expectedIds.size(), overlappingReceptorIds.size(), "Should have same amount of unique IDs as expected");

    for (final int overlappingReceptorId : overlappingReceptorIds) {
      assertTrue(expectedIds.contains(overlappingReceptorId), "Receptor ID not found in expected");
    }
  }

}
