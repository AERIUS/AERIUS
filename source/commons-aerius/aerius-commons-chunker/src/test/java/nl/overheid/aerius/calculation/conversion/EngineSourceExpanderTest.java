/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.theme.own2000.OwN2000SourceConverter;
import nl.overheid.aerius.conversion.EngineSourceExpander;
import nl.overheid.aerius.conversion.SourceConversionHelper;
import nl.overheid.aerius.conversion.SourceConverter;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringMaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Unit test for {@link EngineSourceExpander}
 */
class EngineSourceExpanderTest extends BaseDBTest {

  private static final Point POINT = new Point(4000, 50000);
  private static final List<Substance> KEY_NOX = List.of(Substance.NOX);
  private static final List<Substance> KEY_NOX_NH3 = List.of(Substance.NOX, Substance.NH3);

  private static TestDomain testDomain;

  private final SourceConversionHelper conversionHelper;
  private ScenarioSituation situation;
  private SourceConverter geometryExpander;

  EngineSourceExpanderTest() {
    conversionHelper = new SourceConversionHelper(getPMF(), testDomain.getCategories(), TestDomain.YEAR);
  }

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    testDomain = new TestDomain(getCategories(getPMF()));
  }

  @BeforeEach
  public void setUpBeforeEach() throws IOException, SQLException {
    situation = new ScenarioSituation();
    try (Connection con = getPMF().getConnection()) {
      geometryExpander = new OwN2000SourceConverter(getPMF().getConnection(),
          new DefaultCalculationEngineProvider(SectorRepository.getSectorProperties(con)), situation);
    }
  }

  @Test
  void testStackingOfOffRoadMobileSource() throws SQLException, AeriusException {
    assertEquals(2, createAndExpandOffRoadMobileSources(true).size(), "Number of point sources");
  }

  @Test
  void testNonStackingOffRoadMobileSource() throws SQLException, AeriusException {
    assertEquals(4, createAndExpandOffRoadMobileSources(false).size(), "Number of point sources");
  }

  @Test
  void testGuardAgainstInvalidSector() {
    final AeriusException exception = assertThrows(AeriusException.class, () -> {
      final EmissionSourceFeature feature = new EmissionSourceFeature();
      feature.setId("1");
      feature.setGeometry(getExampleLineWKTGeometry());
      final MooringMaritimeShippingEmissionSource emissionSource = new MooringMaritimeShippingEmissionSource();
      final MooringMaritimeShippingEmissionSource source = testDomain.getMooringMaritimeShipEmissionSource(emissionSource);
      feature.setProperties(source);

      // Set the sector to have a none ops calculation engine
      source.setSectorId(TestDomain.ROAD_SECTOR);
      toEngineSources(List.of(feature), KEY_NOX_NH3, true);
    }, "Would have expected Exception because of invalid sector.");
    assertEquals(1016, exception.getReason().getErrorCode(), "Should throw with expected reason code");
  }

  @Test
  void testSourceWithBuilding() throws SQLException, AeriusException {
    final EmissionSourceFeature source = createGenericSource();
    final String buildingId = "some-building-id";
    final BuildingFeature building = createBuilding(buildingId);
    source.getProperties().getCharacteristics().setBuildingId(buildingId);
    situation.getEmissionSourcesList().add(source);
    situation.getBuildingsList().add(building);

    final List<EngineSource> converted = toEngineSources(KEY_NOX, false);
    assertEquals(1, converted.size(), "Number of point sources");
    final EngineSource engineSource = converted.get(0);
    assertInstanceOf(OPSSource.class, engineSource, "Should be an OPSSource");
    final OPSSource opsSource = (OPSSource) engineSource;
    assertEquals(50, opsSource.getBuildingWidth(), "building width");
    assertEquals(100, opsSource.getBuildingLength(), "building length");
    assertEquals(0, opsSource.getBuildingOrientation(), "building orientation");
    assertEquals(2.3, opsSource.getBuildingHeight(), "building height");
  }

  private List<EngineSource> createAndExpandOffRoadMobileSources(final boolean stacking) throws SQLException, AeriusException {
    final Sector sector = testDomain.getCategories().getSectorById(3220);
    final List<EmissionSourceFeature> sourceList = createStackableOffRoadSourceList(sector);
    return toEngineSources(sourceList, KEY_NOX, stacking);
  }

  private List<EngineSource> toEngineSources(final List<EmissionSourceFeature> sourceList, final List<Substance> substances, final boolean stacking)
      throws SQLException, AeriusException {
    situation.getEmissionSourcesList().addAll(sourceList);
    return toEngineSources(substances, stacking);
  }

  private List<EngineSource> toEngineSources(final List<Substance> substances, final boolean stacking)
      throws SQLException, AeriusException {
    return EngineSourceExpander.toEngineSources(getConnection(), geometryExpander, conversionHelper, situation, substances, stacking);
  }

  private static List<EmissionSourceFeature> createStackableOffRoadSourceList(final Sector sector) {
    final EmissionSourceFeature emissionSource1 = createOffRoadEmissionSource(sector, 1);
    final EmissionSourceFeature emissionSource2 = createOffRoadEmissionSource(sector, 2);
    final List<EmissionSourceFeature> sourceList = new ArrayList<>();
    sourceList.add(emissionSource1);
    sourceList.add(emissionSource2);
    return sourceList;
  }

  private static EmissionSourceFeature createOffRoadEmissionSource(final Sector sector, final int id) {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setGeometry(POINT);
    final OffRoadMobileEmissionSource emissionSource = new OffRoadMobileEmissionSource();
    feature.setProperties(emissionSource);
    emissionSource.setSectorId(sector.getSectorId());
    testDomain.getOffRoadMobileEmissionSource(emissionSource);
    return feature;
  }

  private static EmissionSourceFeature createGenericSource() {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setGeometry(POINT);
    final GenericEmissionSource emissionSource = TestDomain.getGenericEmissionSource();
    emissionSource.getEmissions().put(Substance.NOX, 231.8);
    feature.setProperties(emissionSource);
    return feature;
  }

  private static BuildingFeature createBuilding(final String id) {
    final BuildingFeature feature = new BuildingFeature();
    feature.setId(id);
    final Polygon polygon = new Polygon();
    polygon.setCoordinates(new double[][][] {{{0, 0}, {100, 0}, {100, 50}, {0, 50}, {0, 0}}});
    feature.setGeometry(polygon);
    final Building building = new Building();
    building.setHeight(2.3);
    building.setGmlId(id);
    feature.setProperties(building);
    return feature;
  }

  public static Geometry getExampleLineWKTGeometry() {
    final LineString lineString = new LineString();
    lineString.setCoordinates(new double[][] {
        {166430, 471689},
        {167214, 464592},
    });

    return lineString;
  }
}
