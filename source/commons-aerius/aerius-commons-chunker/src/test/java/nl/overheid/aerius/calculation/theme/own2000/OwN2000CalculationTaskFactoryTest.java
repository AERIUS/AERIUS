/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.OwN2000CalculationOptions;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link OwN2000CalculationTaskFactory}.
 */
@ExtendWith(MockitoExtension.class)
class OwN2000CalculationTaskFactoryTest extends BaseDBTest {

  private static final int SPLIT_DISTANCE = 1000;

  private @Mock JobIdentifier jobIdentifier;

  private OwN2000CalculationTaskFactory factory;


  @BeforeEach
  void beforeEach() throws SQLException {
    factory = new OwN2000CalculationTaskFactory(RECEPTOR_GRID_SETTINGS, getPMF());
  }


  @Test
  void testCreateOPSTaskDefault() throws AeriusException {
    final Consumer<CalculationSetOptions> preparer = calculationSetOptions -> {
      calculationSetOptions.setCalculationMethod(CalculationMethod.CUSTOM_POINTS);
    };
    final Consumer<OPSInputData> verifier = opsInput -> {
      assertFalse(opsInput.isForceTerrainProperties(), "Should not have forced terrain options");
      assertNull(opsInput.getMeteo(), "Should not have meteo");
      assertFalse(opsInput.isMaxDistance(), "Should not have use max distance set");
      assertSame(SubReceptorCalculation.PROVIDED, opsInput.getSubReceptorCalculation(), "Should have Provided calculation");
      assertEquals(0, opsInput.getSplitDistance(), "Should npt have split distance set");
    };
    runTest(preparer, verifier);
  }

  @Test
  void testCreateOPSTask() throws AeriusException {
    final Meteo meteo = new Meteo(2020, 2030);
    final Consumer<CalculationSetOptions> preparer = calculationSetOptions -> {
      calculationSetOptions.setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
      final OwN2000CalculationOptions owN2000CalculationOptions = calculationSetOptions.getOwN2000CalculationOptions();
      owN2000CalculationOptions.setUseMaxDistance(true);
      owN2000CalculationOptions.setMeteo(meteo);
      owN2000CalculationOptions.setSplitSubReceptorWork(true);
      owN2000CalculationOptions.setSplitSubReceptorWorkDistance(SPLIT_DISTANCE);
    };
    final Consumer<OPSInputData> verifier = opsInput -> {
      assertTrue(opsInput.isForceTerrainProperties(), "Should have forced terrain options");
      assertEquals(meteo, opsInput.getMeteo(), "Should have meteo");
      assertTrue(opsInput.isMaxDistance(), "Should have use max distance set");
      assertSame(SubReceptorCalculation.SPLIT_PROVIDED, opsInput.getSubReceptorCalculation(), "Should have Split Provided calculation");
      assertEquals(SPLIT_DISTANCE, opsInput.getSplitDistance(), "Should have split distance set");
    };
    runTest(preparer, verifier);
  }

  void runTest(final Consumer<CalculationSetOptions> preparer, final Consumer<OPSInputData> verifier) throws AeriusException {
    final Scenario scenario = new Scenario();
    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);
    final CalculationJob calculationJob = new CalculationJob(jobIdentifier, "ops test", scenarioCalculations, ExportType.OPS, "queue name");
    calculationJob.setThemeCalculationJobOptions(new OwN2000CalculationJobOptions());
    preparer.accept(calculationJob.getCalculationSetOptions());
    final CalculationTask<?, ?, ?> task = factory.createTask(CalculationEngine.OPS, calculationJob);

    assertSame(OPSInputData.class, task.getTaskInput().getClass(), "Should return an OPSInputData class");
    verifier.accept((OPSInputData) task .getTaskInput());
  }
}
