/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import static nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option.INDIVIDUAL_CUSTOM_VALUES;
import static nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option.LOCATION_BASED;
import static nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option.ONE_CUSTOM_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;

/**
 * Test class for {@link NCAFractionNO2Supplier}.
 */
class NCAFractionNO2SupplierTest {

  private static final double ROAD_LOCAL_FNO2 = 0.3;
  private static final double ROAD_ONE_FNO2 = 0.4;
  private static final double OTHER = 0.5;

  @ParameterizedTest
  @MethodSource("data")
  void testSetFractionNO2(final RoadLocalFractionNO2Option option, final Double oneCustomValue, final boolean adsmPoint, final double expected) {
    final ADMSCalculationPoint admsCalculationPoint = new ADMSCalculationPoint();
    // If not setting an admspoint the admsCalculationPoint should not be used in the test or modified.
    // This is check by setting it to OTHER, which is not used in any of the other test values.
    admsCalculationPoint.setRoadLocalFNO2(adsmPoint ? ROAD_LOCAL_FNO2 : OTHER);
    final AeriusPoint point = adsmPoint ? admsCalculationPoint : new AeriusPoint();
    final NCAFractionNO2Supplier supplier = new NCAFractionNO2Supplier(option, oneCustomValue);

    supplier.setFractionNO2(point);
    assertEquals(expected, admsCalculationPoint.getRoadLocalFNO2(), 1E-8, "Expect fNO2 to be correctly set.");
  }

  static Object[][] data() {
    return new Object[][] {
        {LOCATION_BASED, ROAD_ONE_FNO2, true, 0.0},
        {ONE_CUSTOM_VALUE, ROAD_ONE_FNO2, true, ROAD_ONE_FNO2},
        {INDIVIDUAL_CUSTOM_VALUES, ROAD_ONE_FNO2, true, ROAD_LOCAL_FNO2},
        // value set by the user is null
        {LOCATION_BASED, null, true, 0.0},
        {ONE_CUSTOM_VALUE, null, true, 0.0},
        {INDIVIDUAL_CUSTOM_VALUES, null, true, ROAD_LOCAL_FNO2},
        // Point is not an ADMSCalculationPoint
        {LOCATION_BASED, ROAD_ONE_FNO2, false, OTHER},
        {ONE_CUSTOM_VALUE, ROAD_ONE_FNO2, false, OTHER},
        {INDIVIDUAL_CUSTOM_VALUES, ROAD_ONE_FNO2, false, OTHER},
    };
  }
}
