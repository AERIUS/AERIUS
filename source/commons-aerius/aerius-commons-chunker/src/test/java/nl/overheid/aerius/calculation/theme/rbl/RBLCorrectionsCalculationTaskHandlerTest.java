/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link RBLCorrectionsCalculationTaskHandler}.
 */
class RBLCorrectionsCalculationTaskHandlerTest {

  @Test
  void testCorrectPointsMapped() throws AeriusException, InterruptedException, TaskCancelledException {
    final SRMInputData<EngineSource> inputData = new SRMInputData<>(Theme.RBL, AeriusSRMVersion.STABLE);
    final List<Calculation> calculations = createDummyCalculations();
    final AtomicBoolean success = new AtomicBoolean();
    final CalculationTaskHandler wrapper = new CalculationTaskHandler() {
      @Override
      public <E extends EngineSource, T extends CalculationTask<E, ?, ?>> void work(final WorkKey workKey, final T task,
          final Collection<AeriusPoint> points) throws AeriusException, InterruptedException, TaskCancelledException {
        assertEquals(3, inputData.getCorrections().size(), "Should have 3 corrections");
        assertCorrectPoint(inputData, "3", 2);
        assertCorrectPoint(inputData, "5", 1);
        success.set(true);
      }

      private void assertCorrectPoint(final SRMInputData<EngineSource> inputData, final String gmlId, final int numberOfCorrections) {
        assertTrue(inputData.getCorrections().stream().anyMatch(p -> gmlId.equals(p.getCalculationPointGmlId())), "Should have correct id " + gmlId);
        assertEquals(numberOfCorrections, inputData.getCorrections().stream().filter(p -> gmlId.equals(p.getCalculationPointGmlId())).count(),
            "Should have expected number of corrections");
      }
    };
    final RBLCorrectionsCalculationTaskHandler handler = new RBLCorrectionsCalculationTaskHandler(wrapper, calculations);
    final Collection<AeriusPoint> points = createDummyPoints();
    final CalculationTask<?, ?, ?> task = new CalculationTask<>(WorkerType.ASRM, inputData);
    handler.work(new WorkKey("", new JobIdentifier(""), 2, CalculationEngine.ASRM2), task, points);
    assertTrue(success.get(), "Test did not reach asserts check.");
  }

  /**
   * @return
   */
  private List<Calculation> createDummyCalculations() {
    final List<Calculation> calculations = new ArrayList<>();
    add(calculations, 1);
    final Calculation calculation = add(calculations, 2);
    calculation.getCIMLKCorrections().add(createCorrectionPoint("3", Substance.NH3));
    calculation.getCIMLKCorrections().add(createCorrectionPoint("3", Substance.NO2));
    calculation.getCIMLKCorrections().add(createCorrectionPoint("5", Substance.NH3));
    calculation.getCIMLKCorrections().add(createCorrectionPoint("7", Substance.NH3));
    calculation.getCIMLKCorrections().add(createCorrectionPoint("7", Substance.NO2));
    return calculations;
  }

  private CIMLKCorrection createCorrectionPoint(final String gmlId, final Substance substance) {
    final CIMLKCorrection p = new CIMLKCorrection();
    p.setCalculationPointGmlId(gmlId);
    p.setSubstance(substance);
    return p;
  }

  private Calculation add(final List<Calculation> calculations, final int index) {
    final Calculation calculation = new Calculation();
    calculation.setCalculationId(index);
    calculations.add(calculation);
    return calculation;
  }
  private List<AeriusPoint> createDummyPoints() {
    final List<AeriusPoint> points = new ArrayList<>();
    points.add(createPoint("1"));
    points.add(createPoint("3"));
    points.add(createPoint("5"));
    return points;
  }

  private AeriusPoint createPoint(final String gmlId) {
    final AeriusPoint p = new AeriusPoint();
    p.setGmlId(gmlId);
    return p;
  }

}
