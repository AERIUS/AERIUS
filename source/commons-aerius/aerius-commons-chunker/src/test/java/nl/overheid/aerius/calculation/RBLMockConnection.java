/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;

import nl.aerius.taskmanager.client.util.QueueHelper;
import nl.aerius.taskmanager.test.MockConnection;
import nl.aerius.taskmanager.test.MockedChannelFactory;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CIMLKResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.srm2.domain.SRMInputData;

/**
 * Mocks a SRM calculation for theme RBL.
 */
class RBLMockConnection extends MockConnection {

  private static final double MINIMAL_DEPOSITION = 0.0001;
  private double resultFactor;
  private final double resultDecrementFactor;

  public RBLMockConnection(final double resultFactor, final double resultDecrementFactor) {
    this.resultFactor = resultFactor;
    this.resultDecrementFactor = resultDecrementFactor;
  }

  @Override
  public Channel createChannel() throws IOException {
    return MockedChannelFactory.create(this::mockResults);
  }

  /**
   * Mocks a Channel to return results that with each run decrease in value. This can be used to mock deposition results in unit tests. Given the
   * points are with each run at a large distance to the sources.
   */
  private byte[] mockResults(final BasicProperties properties, final byte[] body) {
    synchronized (this) {
      try {
        final SRMInputData input = (SRMInputData) QueueHelper.bytesToObject(body);
        final CalculationResult cr = new CalculationResult(input.getChunkStats(), CalculationEngine.ASRM2);
        final List<AeriusResultPoint> results = new ArrayList<>();
        final Set<Entry<Integer, Collection<?>>> entrySet = input.getEmissionSources().entrySet();

        for (final Entry<Integer, Collection<?>> entry : entrySet) {
          final Collection<AeriusPoint> receptors = input.getReceptors();

          for (final AeriusPoint receptor : receptors) {
            results.add(toResultPoint(receptor, resultFactor, input.getEmissionResultKeys()));
          }
          cr.put(entry.getKey(), results);
        }
        resultFactor = Math.max(MINIMAL_DEPOSITION, resultFactor * resultDecrementFactor);

        return QueueHelper.objectToBytes(cr);
      } catch (final IOException | ClassNotFoundException e) {
        return null;
      }
    }
  }

  private final CIMLKResultPoint toResultPoint(final AeriusPoint receptor, final double resultFactor, final Set<EmissionResultKey> keys) {
    final CIMLKResultPoint resultPoint = new CIMLKResultPoint(receptor);
    for (final EmissionResultKey resultKey : keys) {
      resultPoint.setEmissionResult(resultKey, resultFactor);
    }
    return resultPoint;
  }
}
