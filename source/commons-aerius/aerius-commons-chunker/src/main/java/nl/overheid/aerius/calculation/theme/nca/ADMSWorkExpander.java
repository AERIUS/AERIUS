/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.util.ArrayList;
import java.util.List;

import nl.aerius.adms.domain.ADMSEngineDataKey;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Expands the JobPackets to create a JobPacket for each meteo year that needs to be calculated.
 * By default the JobPacket will get {@link CalculationEngine#ADMS} as the engine key. This CalculationEngine key will be substituted for a
 * key containing the meteo year.
 */
final class ADMSWorkExpander {

  private ADMSWorkExpander() {
    // Util class
  }

  /**
   * Expand JobPackets by duplicating the job packets if there are multiple meteo years. Also the used {@link CalculationEngine#ADMS} key will
   * be replaced with the key containing the meteo year.
   *
   * @param meteoYears list of meteo years.
   * @param originalWork original created work packet containing all source/point job packets to calculate.
   * @return enhances work packet
   */
  public static WorkPacket<AeriusPoint> expand(final List<String> meteoYears, final WorkPacket<AeriusPoint> originalWork) {
    final List<JobPacket> newJobPackets = new ArrayList<>();

    for (final JobPacket jobPacket : originalWork.getJobPackets()) {
      for (final String meteoYear : meteoYears) {
        final List<GroupedSourcesPacket> sourcesPackets = jobPacket.getSourcesPackets().stream()
            .map(sp -> new GroupedSourcesPacket(sp.getKey().id(), new ADMSEngineDataKey(meteoYear), sp.getSources())).toList();

        newJobPackets.add(new JobPacket(jobPacket.getCalculationId(), jobPacket.getYear(), sourcesPackets));
      }
    }
    return new WorkPacket<>(originalWork.getPoints(), newJobPackets);
  }
}
