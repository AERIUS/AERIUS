/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

import java.util.List;

/**
 * Interface to determine if given object can be aggregated with other similar objects.
 * @param <T> object to check
 */
interface Aggregatable<T, S> {
  /**
   * Returns true if object is aggregatable.
   *
   * @param obj object to check
   * @return true if object is aggregatable
   */
  boolean isAggregatable(T obj);

  /**
   * Aggregate given objects.
   *
   * @param objects objects to aggregate
   * @param emissionKeys keys for the emissions to be part of the aggregation.
   * @return aggregated objects.
   */
  List<T> aggregateSources(final List<T> objects, final List<S> emissionKeys);

  /**
   * Return the {@link AggregationKey} for the given object.
   * @param obj object to return key for
   * @return key identifying object
   */
  AggregationKey aggregationKey(T obj);

  /**
   * Interface for the key that is the same for sources that can be aggregated together.
   */
  interface AggregationKey {

    @Override
    int hashCode();
  }
}
