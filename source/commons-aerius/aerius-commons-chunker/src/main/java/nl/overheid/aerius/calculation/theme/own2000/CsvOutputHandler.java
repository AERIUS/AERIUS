/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.SummaryCollector;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * {@link CalculationResultHandler} to write results to csv files.
 */
class CsvOutputHandler implements CalculationResultHandler, PostCalculationHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(CsvOutputHandler.class);

  // <user specified name>_resulttype-<resulttype>.csv
  private static final String FILENAME_FORMAT = "%s_resulttype-%s%s";
  private static final String CSV_EXT = ".csv";
  private static final String FILE_HEADER = "year;model;sector_id;result_type;substance_id;receptor_id;result";
  private static final String DELIMITER = ";";

  private final String name;
  private final String directory;
  private final HashMap<Integer, ReentrantLock> locks = new HashMap<>();
  private final SummaryCollector collector;
  private final PMF pmf;
  private final CalculationJob calculationJob;

  /**
   * Constructor.
   *
   * @param pmf Database connection factory, only needed to get the database version for metadata
   * @param calculationJob The calculation job.
   * @throws SQLException
   * @throws IOException
   */
  public CsvOutputHandler(final PMF pmf, final CalculationJob calculationJob) {
    this.pmf = pmf;
    this.calculationJob = calculationJob;
    name = calculationJob.getName() == null ? calculationJob.getJobIdentifier().getJobKey() : calculationJob.getName();
    this.directory = WorkerUtil.getFolderForJob(calculationJob.getJobIdentifier()).getAbsolutePath();
    calculationJob.setUnitsAccumulator();
    collector = new SummaryCollector(calculationJob);
    ensureReferencePerSituation(calculationJob);
  }

  private static void ensureReferencePerSituation(final CalculationJob calculationJob) {
    Optional.ofNullable(calculationJob.getScenarioCalculations())
        .map(ScenarioCalculations::getScenario).stream()
        .flatMap(scenario -> scenario.getSituations().stream())
        .filter(situation -> situation.getReference() == null)
        .forEach(situation -> situation.setReference(ReferenceUtil.generatePermitReference()));
  }

  @Override
  public void onSectorResults(final List<AeriusResultPoint> results, final int calculationId, final EngineDataKey engineDataKey,
      final int sectorId) throws AeriusException {
    // files are opened per onSectorResults as we don't want to have files open unnecessarily
    final EnumMap<EmissionResultType, PrintWriter> openedFiles = new EnumMap<>(EmissionResultType.class);

    final int calculationYear = calculationJob.getScenarioCalculations().getCalculationYears().get(calculationId);

    final ReentrantLock lock = getLock(calculationId);
    try {
      lock.lock();
      for (final AeriusResultPoint result : results) {
        for (final Entry<EmissionResultKey, Double> entry : result.getEmissionResults().entrySet()) {
          final EmissionResultKey key = entry.getKey();
          final EmissionResultType ert = key.getEmissionResultType();

          try {
            final PrintWriter writer = getWriter(openedFiles, ert, calculationId);
            writeRecord(writer, calculationYear, engineDataKey, sectorId, ert, key.getSubstance(), result.getId(), entry.getValue());
          } catch (final IOException e) {
            LOGGER.error("Error writing sector results", e);
            throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
          }
          collector.addResult(calculationId, sectorId, key, entry.getValue());
        }
      }
    } finally {
      for (final PrintWriter writer : openedFiles.values()) {
        writer.close();
      }
      lock.unlock();
    }
  }

  private synchronized ReentrantLock getLock(final int calculationId) {
    return locks.computeIfAbsent(calculationId, v -> new ReentrantLock());
  }

  private PrintWriter getWriter(final EnumMap<EmissionResultType, PrintWriter> openedFiles, final EmissionResultType ert, final int calculationId)
      throws IOException {
    if (!openedFiles.containsKey(ert)) {
      final String ertString = ert.toDatabaseString();
      final String filename = String.format(FILENAME_FORMAT, name, ertString, CSV_EXT);
      final Path openFile = Paths.get(directory, filename);
      final boolean newFile = !openFile.toFile().exists();
      final PrintWriter writer = new PrintWriter(
          Files.newBufferedWriter(openFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND));
      openedFiles.put(ert, writer);

      if (newFile) {
        collector.addSummary(calculationId, ert, filename);
        writer.println(FILE_HEADER);
      }
    }

    return openedFiles.get(ert);
  }

  @Override
  public void onCompleted(final CalculationJob calculationJob) throws AeriusException {
    try {
      collector.writeSummary(this.directory, name);
      MetaDataUtil.writeMetadata(pmf, calculationJob, directory, name, collector.getDuration());
    } catch (final IOException | SQLException e) {
      LOGGER.error("Error writing summary file for job: {}", name, e);
    }
  }

  private static void writeRecord(final PrintWriter writer, final int year, final EngineDataKey ce, final int sectorId,
      final EmissionResultType ert, final Substance substance, final int receptorId, final double value) {
    writer.print(year);
    writer.print(DELIMITER);
    writer.print(ce.key());
    writer.print(DELIMITER);
    writer.print(sectorId);
    writer.print(DELIMITER);
    writer.print(ert.toDatabaseString());
    writer.print(DELIMITER);
    writer.print(substance.getId());
    writer.print(DELIMITER);
    writer.print(receptorId);
    writer.print(DELIMITER);
    writer.print(value);
    writer.println();
  }
}
