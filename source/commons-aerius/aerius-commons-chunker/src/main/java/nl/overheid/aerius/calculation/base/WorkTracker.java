/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import nl.overheid.aerius.shared.domain.calculation.CalculationState;

/**
 * Tracker class to manage waiting for all task to finish or cancelled.
 */
public interface WorkTracker {

  /**
   * Called when the tracking should be started.
   */
  CalculationState start();

  /**
   * Call when a task has been canceled.
   * @param exception The reason the task was cancelled.
   */
  void cancel(Exception exception);

  /**
   * Returns true if the {@link #cancel(Exception)} was called.
   * @return
   */
  boolean isCanceled();

  /**
   * Returns the exception pass by {@link #cancel(Exception)}.
   * @return exception
   */
  Exception getException();

  /**
   * @return Returns the current state.
   */
  CalculationState getState();

  /**
   * Call when a new task should be tracked.
   */
  void increment();

  /**
   * Call when a task should be untracked.
   */
  void decrement();

  /**
   * Waits until total number of tasks added via {@link #increment()} and {@link #decrement()} are 0 or {@link #cancel(Exception)} have been called.
   * This method should be called in 1 thread together with the {@link #increment()} calls. Otherwise it might miss increments and finish to early.
   * @return the final state
   */
  CalculationState waitForCompletion() throws InterruptedException;
}
