/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.subreceptor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.locationtech.jts.geom.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.AssessmentAreaRepository;
import nl.overheid.aerius.receptor.SubReceptorCreator;
import nl.overheid.aerius.shared.domain.calculation.SubReceptorsMode;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.HexagonUtil;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Supplies the additional sub receptors points to be calculated.
 *
 * The aggregation works as follows. The chunker computes for each hexagon the number of subreceptors needed.
 * The number of subreceptors depend on the specific theme implementation. But all have a number of levels.
 * Where each level means a number of subreceptors, and a higher the level gives more subreceptors.
 * Level 0 is for receptors with no subreceptors. In that case the subreceptor is on the same location as the receptor.
 *
 * To calculate with subreceptors a specific instance of a receptor point is used. This receptor point implements {@link IsSubPoint}.
 * For each receptor a point with point type {@link AeriusPointType#RECEPTOR} is added which is the center of the hexagon,
 * and the sub receptor points with point type {@link AeriusPointType#SUB_RECEPTOR} which have the sub receptor locations.
 * For level 0 this means 2 points are added on the same location, but with different point types.
 *
 * In the model workers. The points with point type {@link AeriusPointType#SUB_RECEPTOR} should be calculated.
 * And to get the aggregated results the results will be aggregated in the point results point with point type {@link AeriusPointType#RECEPTOR}.
 *
 * In determining the subreceptors it's also checked if the subreceptors do intersect with habitat data.
 * Sub receptors not intersecting with habitats are not calculated.
 */
public class SubReceptorPointSupplier<R extends AeriusPoint & IsSubPoint> implements AdditionalPointSupplier {

  private static final Logger LOG = LoggerFactory.getLogger(SubReceptorPointSupplier.class);

  private final PMF pmf;
  private final SubReceptorCreator<R> subReceptorCreator;
  private final EmissionSourceListSTRTree sourcesTree;
  private final HexagonZoomLevel hexagonZoomLevel;
  private final double minDistance;
  private final SubReceptorsMode subReceptorMode;

  public SubReceptorPointSupplier(final PMF pmf, final SubReceptorCreator<R> subReceptorCreator, final HexagonZoomLevel hexagonZoomLevel,
      final double minDistance, final EmissionSourceListSTRTree sourcesTree, final SubReceptorsMode subReceptorMode) {
    this.pmf = pmf;
    this.subReceptorCreator = subReceptorCreator;
    this.hexagonZoomLevel = hexagonZoomLevel;
    this.minDistance = minDistance;
    this.sourcesTree = sourcesTree;
    this.subReceptorMode = subReceptorMode;
  }

  @Override
  public int addPoint(final List<AeriusPoint> pointsToCalculate, final AeriusPoint aeriusPoint) throws AeriusException {
    final int subLevel = computeDistanceLevel(aeriusPoint);

    if (subLevel == 0) {
      // If there are no sub-receptors/points, just pass the original point.
      pointsToCalculate.add(aeriusPoint);
      return 1;
    } else {
      final R receptor = subReceptorCreator.copyToSubReceptorPoint(aeriusPoint, aeriusPoint.getPointType(), 0);
      final Collection<R> subReceptors = collectSubReceptors(receptor, subLevel, getRelevantGeometry(aeriusPoint));

      if (subReceptors.isEmpty()) {
        // In some cases we end up with no subreceptors (for instance when the source has a polygon geometry and covers the whole hexagon).
        // In that case, don't add anything.
        return 0;
      }
      pointsToCalculate.add(receptor);
      pointsToCalculate.addAll(subReceptors);
      return subReceptors.size();
    }
  }

  private int computeDistanceLevel(final AeriusPoint aeriusPoint) throws AeriusException {
    return subReceptorCreator.findLevel(sourcesTree.findShortestDistance(aeriusPoint));
  }

  private Geometry getRelevantGeometry(final AeriusPoint aeriusPoint) throws AeriusException {
    try (Connection con = pmf.getConnection()) {
      if (subReceptorMode.isOnlyInsideHabitats()) {
        return AssessmentAreaRepository.relevantHabitatsInsideGeometry(con, HexagonUtil.createHexagon(aeriusPoint, hexagonZoomLevel));
      } else if (subReceptorMode.isOnlyInsideNatureAreas()) {
        return AssessmentAreaRepository.relevantAssessmentAreaInsideGeometry(con, HexagonUtil.createHexagon(aeriusPoint, hexagonZoomLevel));
      } else {
        return null;
      }
    } catch (final SQLException e) {
      LOG.error("SQLException in collectSubReceptors", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private Collection<R> collectSubReceptors(final R aeriusPoint, final int subLevel, final Geometry habitatBoundery) throws AeriusException {
    return subReceptorCreator.computePoints(aeriusPoint, subLevel, p -> filterCloseSubReceptors(p, habitatBoundery));
  }

  /**
   * Returns true if receptor point distance to closest source beyond minimum distance and intersects with a habitat/nature area.
   *
   * @param point receptor point to check
   * @param geometry geometry to check for intersection
   * @return true if distance beyond minimum distance and intersects
   * @throws AeriusException in case of errors
   */
  private Boolean filterCloseSubReceptors(final R point, final Geometry geometry) throws AeriusException {
    final boolean mightBeAdded = !(subReceptorMode.isOnlyInsideHabitats() || subReceptorMode.isOnlyInsideNatureAreas())
        || (geometry != null && geometry.intersects(GeometryUtil.getGeometry(point.getArea())));

    return mightBeAdded && sourcesTree.findShortestDistance(point) > minDistance;
  }
}
