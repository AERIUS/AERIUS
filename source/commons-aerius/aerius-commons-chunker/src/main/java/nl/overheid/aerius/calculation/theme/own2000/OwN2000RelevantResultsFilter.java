/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Only when results are > 0.0 it should be included. Results 0.0 are when outside range.
 */
class OwN2000RelevantResultsFilter implements IncludeResultsFilter {

  private static final double NO_RESULT = 0.0;

  @Override
  public boolean includeResult(final AeriusResultPoint point) {
    return point.getEmissionResult(EmissionResultKey.NOXNH3_DEPOSITION) > NO_RESULT || point.getPointType() == AeriusPoint.AeriusPointType.POINT;
  }
}
