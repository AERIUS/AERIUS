/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepositoryBean;
import nl.overheid.aerius.db.common.results.ProjectCalculationRepository;
import nl.overheid.aerius.db.common.results.ProjectCalculationRepository.CalculationResultsType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.ReceptorResult;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * Export ExportResultKey.NOXNH3_DEPOSITION situation results and summary results to JSON export type file.
 *
 */
public class ProjectCalculationExportHandler implements PostCalculationHandler {

  private static final String SUMMARY = "SUMMARY";

  private static final Logger LOGGER = LoggerFactory.getLogger(ProjectCalculationExportHandler.class);

  private static final String FILENAME_FORMAT = "%s_situation-%s_substance-%s_resulttype-%s%s";
  private static final String JSON_EXT = ".json";

  private final ObjectMapper objectMapper = new ObjectMapper();
  private final PMF pmf;
  private final String name;
  private final File directory;

  private final SummaryHexagonType hexagonType;
  private final OverlappingHexagonType overlappingHexagonType;
  private final long startTime = System.currentTimeMillis();

  public ProjectCalculationExportHandler(final PMF pmf, final CalculationJob calculationJob, final SummaryHexagonType hexagonType,
      final OverlappingHexagonType overlappingHexagonType) {
    this.pmf = pmf;
    name = calculationJob.getName() == null ? calculationJob.getJobIdentifier().getJobKey() : calculationJob.getName();
    this.directory = WorkerUtil.getFolderForJob(calculationJob.getJobIdentifier());
    this.hexagonType = hexagonType;
    this.overlappingHexagonType = overlappingHexagonType;
  }

  @Override
  public void onCompleted(final CalculationJob calculationJob) throws AeriusException {
    final CalculationRepositoryBean calculationRepository = new CalculationRepositoryBean(pmf);
    final ProjectCalculationRepository projectCalculationRepository = new ProjectCalculationRepository(pmf);
    final List<Calculation> calculations = calculationJob.getScenarioCalculations().getCalculations();
    final int[] calculationIds = calculations.stream().mapToInt(Calculation::getCalculationId).toArray();
    final SituationType summmarySituation = determineSummarySituationType(calculations);

    for (final Calculation calculation : calculations) {
      final Map<Integer, EmissionResultKey> resultSets = calculationRepository
          .getCalculationResultIdSetsEmissionResultKey(calculation.getCalculationId());

      for (final Entry<Integer, EmissionResultKey> resultSet : resultSets.entrySet()) {
        if (Substance.NOXNH3 != resultSet.getValue().getSubstance()) {
          final SituationType situation = calculation.getSituation().getType();
          final List<ReceptorResult> situationResult = calculationRepository.getCalculationResults(calculationIds, resultSet.getKey(), hexagonType);
          Collections.sort(situationResult, ProjectCalculationExportHandler::compare);
          writeJsonResult(resultSet.getValue(), situation.name(), situationResult);
          writeSummary(projectCalculationRepository, summmarySituation, calculation, resultSet, situation);
        }
      }
    }
    writeMetadata(calculationJob);
  }

  private static SituationType determineSummarySituationType(final List<Calculation> calculations) {
    final Set<SituationType> situationTypes = calculations.stream()
        .map(Calculation::getSituation)
        .map(ScenarioSituation::getType)
        .collect(Collectors.toSet());
    final SituationType situationType;

    if (situationTypes.contains(SituationType.PROPOSED)) {
      situationType = SituationType.PROPOSED;
    } else if (situationTypes.contains(SituationType.REFERENCE)) {
      situationType = SituationType.REFERENCE;
    } else if (situationTypes.contains(SituationType.OFF_SITE_REDUCTION)) {
      situationType = SituationType.OFF_SITE_REDUCTION;
    } else {
      throw new IllegalArgumentException("Unable to determine project calculation summary type. Types in calculation are: "
          + situationTypes.stream().map(SituationType::name).collect(Collectors.joining(",")));
    }
    return situationType;
  }

  /**
   * Write the summary file if the current situation matches the summary situation
   *  If the situation is proposed the project summary is used.
   * If another situation it means only a single situation was present and in that case the situation results should be obtained.
   * Technically this could also be done when only a single proposed situation is present,
   * but since a proposed situation is always handled as a project situation the project results are obtained.
   *
   * @param projectCalculationRepository
   * @param summmarySituation the situation to get the summary results for
   * @param calculation calculation data
   * @param resultSet result set keys
   * @param situation situation to possibly write summary for
   * @throws AeriusException
   */
  private void writeSummary(final ProjectCalculationRepository projectCalculationRepository, final SituationType summmarySituation,
      final Calculation calculation, final Entry<Integer, EmissionResultKey> resultSet, final SituationType situation) throws AeriusException {
    if (situation == summmarySituation) {
      final CalculationResultsType calculationResultsType = situation == SituationType.PROPOSED ? CalculationResultsType.PROJECT
          : CalculationResultsType.SITUATION;

      writeSummary(projectCalculationRepository, calculationResultsType, calculation, resultSet);
    }
  }

  private void writeSummary(final ProjectCalculationRepository projectCalculationRepository, final CalculationResultsType calculationResultsType,
      final Calculation calculation, final Entry<Integer, EmissionResultKey> resultSet) throws AeriusException {
    final List<ReceptorResult> results = projectCalculationRepository.getCalculationResults(calculationResultsType, calculation.getCalculationId(),
        resultSet.getValue().getSubstance(), hexagonType, overlappingHexagonType);

    Collections.sort(results, ProjectCalculationExportHandler::compare);
    writeJsonResult(resultSet.getValue(), SUMMARY, results);
  }

  private static int compare(final ReceptorResult result1, final ReceptorResult result2) {
    return Integer.compare(result1.getReceptorId(), result2.getReceptorId());
  }

  private void writeJsonResult(final EmissionResultKey emissionResultKey, final String situation, final List<ReceptorResult> situationResult)
      throws AeriusException {
    try {
      final String filename = String.format(FILENAME_FORMAT, name, situation, emissionResultKey.getSubstance().name(),
          emissionResultKey.getEmissionResultType().name(), JSON_EXT);
      final File openFile = new File(directory, filename);

      objectMapper.writeValue(openFile, situationResult);
      LOGGER.debug("Writing json file: {}", filename);
    } catch (final IOException e) {
      LOGGER.warn("Could not save json result output file.", e);
      throw new AeriusException(AeriusExceptionReason.JSON_IO_EXCEPTION_ERROR);
    }
  }

  private void writeMetadata(final CalculationJob calculationJob) {
    try {
      MetaDataUtil.writeMetadata(pmf, calculationJob, directory.getAbsolutePath(), name, getDuration());
    } catch (final IOException | SQLException e) {
      LOGGER.error("Error writing metadata file for job: {}", name, e);
    }
  }

  private String getDuration() {
    return String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime));
  }

}
