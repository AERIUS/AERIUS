/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.locationtech.jts.algorithm.ConvexHull;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.archive.ArchiveProxy;
import nl.overheid.aerius.archive.DevelopmentPressureSearchResponse;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.decision.DecisionFrameworkRepository;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Class to handle the development pressure search.
 */
public class DevelopmentPressureSearchHandler {

  private static final Logger LOG = LoggerFactory.getLogger(DevelopmentPressureSearchHandler.class);

  private static final int DEFAULT_BUFFER = 5000;

  private final DecisionFrameworkRepository repository;
  private final JobRepositoryBean jobRepository;
  private final ArchiveProxy proxy;

  public DevelopmentPressureSearchHandler(final DecisionFrameworkRepository repository, final JobRepositoryBean jobRepository,
      final ArchiveProxy proxy) {
    this.repository = repository;
    this.jobRepository = jobRepository;
    this.proxy = proxy;
  }

  public void handleSearch(final CalculationJob calculationJob) throws AeriusException {
    final CalculationSetOptions options = calculationJob.getCalculationSetOptions();
    if (isCorrectJob(calculationJob)
        && isCorrectJobType(options.getCalculationJobType())
        && hasDevelopmentPressureSourceIds(options)) {
      final Optional<ScenarioSituation> proposedSituation = getProposedSituation(calculationJob);
      if (proposedSituation.isEmpty()) {
        LOG.error("Somehow ended up without a proposed situation when doing development pressure search for {}", calculationJob.getJobIdentifier());
        throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
      }
      final int jobId = jobRepository.getJobId(calculationJob.getJobIdentifier().getJobKey());
      searchAndInsert(options, jobId, proposedSituation.get());
    }
  }

  public void handleSearch(final CalculationSetOptions options, final int jobId, final ScenarioSituation proposedSituation) throws AeriusException {
    if (isCorrectJobType(options.getCalculationJobType())
        && hasDevelopmentPressureSourceIds(options)) {
      searchAndInsert(options, jobId, proposedSituation);
    }
  }

  private void searchAndInsert(final CalculationSetOptions options, final int jobId, final ScenarioSituation proposedSituation)
      throws AeriusException {
    final String wkt = getDevelopmentSearchWkt(options, proposedSituation);
    try {
      final DevelopmentPressureSearchResponse response = proxy.developmentPressureSearch(wkt);
      repository.insertDevelopmentPressureSearchResult(jobId, response.getNumberOfProjects());
    } catch (final IOException e) {
      LOG.trace("Error doing development pressure search", e);
      LOG.error("Error doing development pressure search: {}", e.getMessage());
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private static boolean isCorrectJob(final CalculationJob calculationJob) {
    final ExportType exportType = calculationJob.getExportType();
    return exportType.isExportWithSummaries() || exportType.isResultsExport();
  }

  private static boolean isCorrectJobType(final CalculationJobType type) {
    return type == CalculationJobType.PROCESS_CONTRIBUTION || type == CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION;
  }

  private static boolean hasDevelopmentPressureSourceIds(final CalculationSetOptions options) {
    return options.getNcaCalculationOptions() != null
        && options.getNcaCalculationOptions().getDevelopmentPressureSourceIds() != null
        && !options.getNcaCalculationOptions().getDevelopmentPressureSourceIds().isEmpty();
  }

  private static Optional<ScenarioSituation> getProposedSituation(final CalculationJob calculationJob) {
    return calculationJob.getScenarioCalculations().getScenario().getSituations().stream()
        .filter(situation -> situation.getType() == SituationType.PROPOSED)
        .findFirst();
  }

  private static String getDevelopmentSearchWkt(final CalculationSetOptions options, final ScenarioSituation proposedSituation)
      throws AeriusException {
    final List<Geometry> geometries = new ArrayList<>();
    final Set<String> developmentPressureSourceIds =
        new HashSet<>(options.getNcaCalculationOptions().getDevelopmentPressureSourceIds());

    for (final EmissionSourceFeature source : proposedSituation.getSources().getFeatures()) {
      if (developmentPressureSourceIds.contains(source.getProperties().getGmlId())) {
        geometries.add(GeometryUtil.getGeometry(source.getGeometry()));
      }
    }
    final GeometryCollection collection = new GeometryCollection(geometries.toArray(new Geometry[0]),
        new GeometryFactory(new PrecisionModel()));
    final ConvexHull convexHull = new ConvexHull(collection);
    return convexHull.getConvexHull().buffer(DEFAULT_BUFFER).toText();
  }

}
