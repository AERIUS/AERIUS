/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.domain.ADMSEngineDataKey;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.io.ADMSSourceFileType;
import nl.aerius.adms.version.ADMSVersion;
import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.calculation.base.CalculationTaskFactory;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.theme.nca.NcaSubReceptorConstants;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * NCA {@link CalculationTaskFactory}. Creates the ADMS input data object.
 */
class NCACalculationTaskFactory implements CalculationTaskFactory {
  private static final Logger LOGGER = LoggerFactory.getLogger(NCACalculationTaskFactory.class);

  private static final String TIME_ZONE = "Europe/London";

  @Override
  public <E extends EngineSource, R extends AeriusPoint, T extends EngineInputData<E, R>> CalculationTask<E, R, T> createTask(
      final EngineDataKey engineDataKey, final CalculationJob calculationJob) throws AeriusException {
    if (engineDataKey instanceof final ADMSEngineDataKey admsEdk) {
      final ADMSInputData input = new ADMSInputData(admsEdk, calculationJob.getCommandType());
      final NCACalculationJobOptions ncaCalculationJobOptions = (NCACalculationJobOptions) calculationJob.getThemeCalculationJobOptions();

      input.setTimeZone(TIME_ZONE);
      setQuickRun(input, calculationJob.getCalculationSetOptions().getCalculationMethod(), ncaCalculationJobOptions.isDemoMode());
      final NCACalculationOptions ncaCalculationOptions = calculationJob.getCalculationSetOptions().getNcaCalculationOptions();
      input.setAdmsOptions(ncaCalculationOptions.getAdmsOptions());
      input.setMeteoYear(admsEdk.meteoYear());
      input.setLicense(ncaCalculationJobOptions.getAdmsLicense());
      input.setFileType(ADMSSourceFileType.UPL);
      setSubreceptorCalculation(input, input.isQuickRun());
      input.setExtent(calculationJob.getAdmsExtent());
      input.setAdmsVersion(ADMSVersion.getADMSVersionByLabel(ncaCalculationOptions.getAdmsVersion()));
      return new CalculationTask<>(WorkerType.ADMS, (T) input);
    } else {
      LOGGER.error("Calculation engine {} not supported for NCA theme.", engineDataKey);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private static void setSubreceptorCalculation(final ADMSInputData input, final boolean quickRun) {
    if (quickRun) {
      input.setSubReceptorCalculation(SubReceptorCalculation.SPLIT_PROVIDED);
      input.setSplitDistance(NcaSubReceptorConstants.SUB_RECEPTOR_FARTHEST_DISTANCE);
    }
  }

  private static void setQuickRun(final ADMSInputData input, final CalculationMethod calculationMethod, final boolean demoMode) {
    input.setQuickRun(calculationMethod == CalculationMethod.QUICK_RUN || demoMode);
  }

}
