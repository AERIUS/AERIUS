/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.domain.SRMInputData;

/**
 * Wraps a {@link CalculationTaskHandler} and adds the user corrections points to the input.
 */
public class RBLCorrectionsCalculationTaskHandler implements CalculationTaskHandler {

  private final Map<Integer, Map<String, List<CIMLKCorrection>>> calculationCorrectionsMap = new HashMap<>();
  private final CalculationTaskHandler wrappedCalculationTaskHandler;

  public RBLCorrectionsCalculationTaskHandler(CalculationTaskHandler wrappedCalculationTaskHandler, final List<Calculation> calculations) {
    this.wrappedCalculationTaskHandler = wrappedCalculationTaskHandler;
    for (Calculation calculation : calculations) {
      final Map<String, List<CIMLKCorrection>> gmlIdMap = new HashMap<>();

      for (CIMLKCorrection correction : calculation.getCIMLKCorrections()) {
        gmlIdMap.computeIfAbsent(correction.getCalculationPointGmlId(), e -> new ArrayList<>(4)).add(correction);
      }
      calculationCorrectionsMap.put(calculation.getCalculationId(), gmlIdMap);
    }
  }

  @Override
  public <E extends EngineSource, T extends CalculationTask<E, ?, ?>> void work(WorkKey workKey, T task, Collection<AeriusPoint> points)
      throws AeriusException, InterruptedException, TaskCancelledException {
    if (task.getTaskInput() instanceof SRMInputData<?>) {
      final Map<String, List<CIMLKCorrection>> map = calculationCorrectionsMap.get(workKey.getCalculationId());
      final List<CIMLKCorrection> corrections =
          points.stream().map(p -> map.get(p.getGmlId())).filter(Objects::nonNull).flatMap(List::stream).collect(Collectors.toList());

      ((SRMInputData<?>) task.getTaskInput()).setCorrections(corrections);
    }
    wrappedCalculationTaskHandler.work(workKey, task, points);
  }

}
