/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.natura2k;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore.N2kReceptorStoreBuilder;
import nl.overheid.aerius.calculation.theme.own2000.Natura2kGridPointStore;
import nl.overheid.aerius.db.common.GeometryRepository;
import nl.overheid.aerius.db.own2000.ReceptorRepository;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AreaType;
import nl.overheid.aerius.shared.domain.geo.WKTGeometryWithBox;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Class to pre load all receptors for all assessment areas receptors relevant for the PAS.
 */
public class Natura2KReceptorLoader {
  private static final Logger LOGGER = LoggerFactory.getLogger(Natura2KReceptorLoader.class);

  private static final Float TOLERANCE = 1F;
  private static final Pattern GEOMETRY_STRIP_PRECISION_PATTERN = Pattern.compile("\\.[\\d]+");

  private final ReceptorUtil receptorUtil;
  private final GridSettings gridSettings;

  /**
   * Constructor
   * @param receptorUtil receptorUtil
   * @param gridSettings grid settings
   */
  public Natura2KReceptorLoader(final ReceptorUtil receptorUtil, final GridSettings gridSettings) {
    this.receptorUtil = receptorUtil;
    this.gridSettings = gridSettings;
  }

  /**
   * Fills a {@link Natura2kReceptorStore} from the database.
   *
   * @param con
   * @return
   * @throws SQLException
   * @throws AeriusException
   */
  public Natura2kReceptorStore fillReceptorStore(final Connection con) throws SQLException, AeriusException {
    LOGGER.info("Loading Nature2000 Receptors...");
    final Map<Integer, List<AeriusPoint>> pointsMap = ReceptorRepository.getPermitRequiredPoints(con, receptorUtil);
    final N2kReceptorStoreBuilder builder = new N2kReceptorStoreBuilder(receptorUtil, gridSettings);

    int numberOfAreas = 0;
    int numberOfReceptors = 0;
    for (final Entry<Integer, List<AeriusPoint>> entry : pointsMap.entrySet()) {
      final Integer id = entry.getKey();
      if (builder.addPoints(id, entry.getValue())) {
        numberOfAreas++;
        numberOfReceptors += entry.getValue().size();
        final WKTGeometryWithBox geometry =
            GeometryRepository.getSimplifiedGeometry(con, AreaType.NATURA2000_AREA, id, null, TOLERANCE, null);
        // The coordinates in the geometry are rounded by stripping the precisions using a regular expression.
        builder.addGeometry(id, GeometryUtil.getGeometry(GEOMETRY_STRIP_PRECISION_PATTERN.matcher(geometry.getWKT()).replaceAll("")));
        if (LOGGER.isTraceEnabled()) {
          LOGGER.trace("Total number receptors added for {} in Natura2kReceptorStore:{}", id, entry.getValue().size());
        }
      }
    }
    final Natura2kReceptorStore store = builder.build();
    LOGGER.info("Loading Nature2000 Receptor Store for {} areas with a total of {} receptors completed.",
        numberOfAreas, numberOfReceptors);
    return store;
  }

  /**
   * Fills a {@link Natura2kGridPointStore} from the database.
   *
   * @param pointsMap database connection
   * @return storage with all points.
   * @throws SQLException
   * @deprecated This store is for the deprecated aggregation only.
   */
  @Deprecated
  public Natura2kGridPointStore fillPointStore(final Connection con) throws SQLException {
    final Map<Integer, List<AeriusPoint>> pointsMap = ReceptorRepository.getPermitRequiredPoints(con, receptorUtil);
    final Natura2kGridPointStore pointStore = new Natura2kGridPointStore(receptorUtil, gridSettings);
    fillPointMap(pointsMap, pointStore);
    pointStore.sortPointStore();
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Total number gridcells in Natura2kGridPointStore:{}", pointStore.size());
    }
    return pointStore;
  }

  static void fillPointMap(final Map<Integer, List<AeriusPoint>> pointsMap, final Natura2kGridPointStore pointStore) {
    for (final Entry<Integer, List<AeriusPoint>> entry : pointsMap.entrySet()) {
      final Integer n2kId = entry.getKey();
      final List<AeriusPoint> aeriusPoints = entry.getValue();

      if (aeriusPoints.isEmpty()) {
        LOGGER.trace("Ignoring n2k id:{} because size is 0.", n2kId);
      } else {
        pointStore.addAll(n2kId, aeriusPoints);
        if (LOGGER.isTraceEnabled()) {
          LOGGER.trace("Initializing n2kid:{} #grid cells:{}, #receptors:{}, gridids:{}", n2kId, pointStore.getGridIds(n2kId).size(),
              aeriusPoints.size(), ArrayUtils.toString(pointStore.getGridIds(n2kId)));
        }
      }
    }
  }
}
