/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.calculation.base.CalculationTaskFactory;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Implementation of {@link CalculationTaskFactory} specific for RBL.
 */
class RBLCalculationTaskFactory implements CalculationTaskFactory {
  private static final Logger LOGGER = LoggerFactory.getLogger(RBLCalculationTaskFactory.class);

  @Override
  public <E extends EngineSource, R extends AeriusPoint, T extends EngineInputData<E, R>> CalculationTask<E, R, T> createTask(
      final EngineDataKey engineDataKey, final CalculationJob calculationJob) throws AeriusException {
    if (engineDataKey == CalculationEngine.ASRM2) {
      return new CalculationTask<E, R, T>(WorkerType.ASRM, (T) prepareSRMInputData(calculationJob));
    } else {
      LOGGER.error("Calculation engine {} not supported for RBL theme.", engineDataKey);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private <E extends EngineSource> SRMInputData<E> prepareSRMInputData(final CalculationJob calculationJob) {
    return new SRMInputData<>(Theme.RBL, ((RBLCalculationJobOptions) calculationJob.getThemeCalculationJobOptions()).getSRMVersion());
  }
}
