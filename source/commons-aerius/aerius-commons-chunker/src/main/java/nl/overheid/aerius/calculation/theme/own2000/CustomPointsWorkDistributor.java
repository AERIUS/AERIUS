/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.calculation.base.AeriusPointsWorkDistributor;
import nl.overheid.aerius.calculation.conversion.JobPackets.SourceConverterSupplier;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.grid.GridPointStore;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.calculation.subreceptor.AdditionalPointSupplier;
import nl.overheid.aerius.calculation.subreceptor.SubReceptorPointSupplier;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.ops.domain.OPSSubReceptorCreator;
import nl.overheid.aerius.receptor.SubReceptorCreator;
import nl.overheid.aerius.shared.domain.calculation.SubReceptorsMode;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.ops.OPSSubReceptor;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.theme.own2000.OwN2000SubReceptorConstants;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Work distributor to calculate a custom list of points.
 */
class CustomPointsWorkDistributor extends AeriusPointsWorkDistributor {

  private final GridUtil gridUtil;
  private final List<AeriusPoint> calculationPoints;

  private final JobPacketAggregator aggregator;

  private final SubReceptorCreator<OPSSubReceptor> subReceptorCreator;
  private final HexagonZoomLevel hexagonZoomLevel;
  private Iterator<Map.Entry<Integer, Collection<AeriusPoint>>> pointStoreIterator;
  private AdditionalPointSupplier additionalPointSupplier;

  private List<WorkPacket.JobPacket> jobPackets;
  private boolean aggregation;

  public CustomPointsWorkDistributor(final PMF pmf, final SectorCategories sectorCategories, final GridUtil gridUtil,
      final List<AeriusPoint> calculationPoints, final HexagonZoomLevel hexagonZoomLevel) {
    super(pmf, sectorCategories);
    this.gridUtil = gridUtil;
    this.calculationPoints = calculationPoints;
    this.hexagonZoomLevel = hexagonZoomLevel;
    this.subReceptorCreator = new OPSSubReceptorCreator(AeriusPointType.SUB_POINT, hexagonZoomLevel.getHexagonRadius());
    aggregator = new JobPacketAggregator(pmf, sectorCategories, gridUtil);
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    super.init(calculationJob);
    aggregation = calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions().isForceAggregation();
    final GridPointStore pointStore = new GridPointStore(gridUtil.getGridSettings());

    fillPoints(pointStore, calculationPoints);
    pointStoreIterator = pointStore.entrySet().iterator();
    aggregator.init(calculationJob);

    final SubReceptorsMode subReceptorsMode = calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions().getSubReceptorsMode();
    if (subReceptorsMode.isEnabledForPoints()) {
      final EmissionSourceListSTRTree emissionSourceTree = calculationJob.getSourcesTreeBuilder().getAllSourcesTree();
      additionalPointSupplier = new SubReceptorPointSupplier<OPSSubReceptor>(pmf, subReceptorCreator,
          hexagonZoomLevel, OwN2000SubReceptorConstants.MIN_SOURCE_SUBRECEPTOR_DISTANCE, emissionSourceTree, subReceptorsMode);
    } else {
      additionalPointSupplier = new AdditionalPointSupplier() {
      };
    }
  }

  @Override
  protected SourceConverterSupplier createSourceConverterSupplier(final CalculationJob calculationJob) {
    return (con, situation) -> new OwN2000SourceConverter(con, calculationJob.getProvider(), situation);
  }

  @Override
  protected boolean hasMorePoints() {
    return pointStoreIterator.hasNext();
  }

  @Override
  protected List<AeriusPoint> collectPoints() {
    final Map.Entry<Integer, Collection<AeriusPoint>> next = nextPoints();

    if (next == null) {
      return List.of(); // No more points to calculate
    } else {
      // Return points, and prepare sources
      jobPackets = aggregator.getSourcesFor(next.getKey());
      return new ArrayList<>(next.getValue());
    }
  }

  @Override
  protected List<AeriusPoint> collectMorePoints() throws AeriusException {
    if (aggregation) {
      // We don't want points of different grid cells ending up in the same chunk when aggregating.
      return List.of();
    } else {
      final Entry<Integer, Collection<AeriusPoint>> next = nextPoints();

      return next == null ? List.of() : new ArrayList<>(next.getValue());
    }
  }

  private Map.Entry<Integer, Collection<AeriusPoint>> nextPoints() {
    return pointStoreIterator.hasNext() ? pointStoreIterator.next() : null;
  }

  @Override
  protected List<WorkPacket.JobPacket> collectJobPackets() {
    return jobPackets;
  }

  @Override
  public int nrOfReceptorsByCommandType(final EngineInputData.CommandType commandType, final CalculatorOptions calculationOptions,
      final int sourcesSize) {

    // We don't use the sourcesSize as passed since it does not take aggregation into account
    final int correctSourcesSize = jobPackets.stream().mapToInt(e -> e.getSourcesPackets().stream().mapToInt(s -> s.getSources().size()).sum()).sum();
    return nrOfPoints(calculationOptions, correctSourcesSize);
  }

  @Override
  protected int addPoints(final List<AeriusPoint> pointsToCalculate, final AeriusPoint pointToAdd) throws AeriusException {
    return additionalPointSupplier.addPoint(pointsToCalculate, pointToAdd);
  }

  /**
   * Put calculationPoints in store receptors.
   * @param pointStore
   * @param calculationPoints
   */
  private void fillPoints(final GridPointStore pointStore, final List<AeriusPoint> calculationPoints) {
    calculationPoints.forEach(p -> pointStore.add(toPointSourcePoint(p)));
  }

  /**
   * Returns the point to actually store in the internal points grid store.
   *
   * @param point point to calculate
   * @return point to store in the grid point store
   */
  private static AeriusPoint toPointSourcePoint(final AeriusPoint point) {
    return point instanceof OPSReceptor ? point : new OPSReceptor(point);
  }
}
