/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.archive.ArchiveProxy;
import nl.overheid.aerius.archive.InCombinationProcessResponse;
import nl.overheid.aerius.archive.InCombinationProcessResponse.ArchiveResult;
import nl.overheid.aerius.archive.InCombinationProcessResponse.ArchiveResultValue;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.AeriusSubPoint;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveProject;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Class to collect result data from the Archive server.
 */
class ArchiveCollectorHandler {

  private static final Logger LOG = LoggerFactory.getLogger(ArchiveCollectorHandler.class);

  private static final List<Substance> ARCHIVE_SUBSTANCES = List.of(Substance.NH3, Substance.NOX);

  private final PMF pmf;
  private final ArchiveProxy proxy;
  private final int proposedCalculationId;
  private final int calculationYear;
  // Map<receptorId, Level> Map to keep track of what level data for each receptor should be requested.
  private final Map<Integer, Integer> receptorIdToLevel = new HashMap<>();
  // Map<receptor/subreceptor key, point> Map to keep track on all points to use to make sure all calculated points have a
  //                                      archive result stored, even when no archive results present.
  private final Map<String, AeriusPoint> allPoints = new HashMap<>();

  public ArchiveCollectorHandler(final PMF pmf, final ArchiveProxy proxy, final int proposedCalculationId,
      final int calculationYear) {
    this.pmf = pmf;
    this.proxy = proxy;
    this.proposedCalculationId = proposedCalculationId;
    this.calculationYear = calculationYear;
  }

  /**
   * Add points to be collected from the Archive server in the {@link #requestArchiveData()} call.
   *
   * @param points
   */
  public void addPoints(final Collection<AeriusPoint> points) {
    points.stream().forEach(this::putReceptor);
    final Set<Integer> receptorsWithSubReceptor = determineReceptorsWithSubPoints(points);
    points.stream()
        .filter(p -> p.getPointType() == AeriusPointType.RECEPTOR && !receptorsWithSubReceptor.contains(p.getId()))
        .map(p -> new AeriusSubPoint(p, p.getId()))
        .forEach(this::putReceptor);
  }

  /**
   * Store points that are calculated. It keeps track of the original point and a map of a receptor and the highest level the point is calculated for.
   * These per-level are used to collect the data from Archive.
   *
   * @param aeriuspoint
   */
  private void putReceptor(final AeriusPoint aeriuspoint) {
    if (aeriuspoint.getPointType() == AeriusPointType.POINT || aeriuspoint.getPointType() == AeriusPointType.SUB_POINT) {
      // Ignore none receptor points as Archive data is receptor based.
      return;
    }
    final int id;
    final int subId;
    final int level;

    if (aeriuspoint.getPointType() == AeriusPointType.SUB_RECEPTOR) {
      final IsSubPoint sp = (IsSubPoint) aeriuspoint;

      id = sp.getParentId();
      subId = sp.getId();
      level = sp.getLevel();
    } else {
      id = aeriuspoint.getId();
      subId = 0;
      level = 0;
    }
    allPoints.put(key(id, subId, aeriuspoint.getPointType()), aeriuspoint);
    receptorIdToLevel.merge(id, level, Integer::max);
  }

  /**
   * Retrieves the data from the Archive server and stores the result data in the database.
   */
  public void requestArchiveData() throws AeriusException {
    try {
      if (!receptorIdToLevel.isEmpty()) {
        final InCombinationProcessResponse response = proxy.getInCombinationProcess(calculationYear, receptorIdToLevel);

        insertArchiveContribution(convertAndFilterResults(response.getResults()));
        insertArchiveMetadata(convertMetaData(response));
      }
    } catch (final IOException e) {
      LOG.trace("Error getting InCombinationProcess data", e);
      LOG.error("Error getting InCombinationProcess data: {}", e.getMessage());
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    } catch (final SQLException e) {
      LOG.error("SQL error getting InCombinationProcess data", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  protected void insertArchiveContribution(final List<AeriusResultPoint> results) throws SQLException {
    try (final Connection con = pmf.getConnection()) {
      CalculationRepository.insertArchiveContribution(con, proposedCalculationId, results);
    }
  }

  private static ArchiveMetaData convertMetaData(final InCombinationProcessResponse response) {
    final ArchiveMetaData metaData = new ArchiveMetaData();
    metaData.setRetrievalDateTime(OffsetDateTime.now());
    metaData.setArchiveProjects(response.getProjects() == null
        ? List.of()
        : response.getProjects().stream().map(ArchiveCollectorHandler::map).toList());
    return metaData;
  }

  protected void insertArchiveMetadata(final ArchiveMetaData metaData) throws SQLException, IOException {
    try (final Connection con = pmf.getConnection()) {
      CalculationRepository.insertArchiveMetadata(con, proposedCalculationId, metaData);
    }
  }

  private static ArchiveProject map(final InCombinationProcessResponse.ArchiveProject origin) {
    final ArchiveProject target = new ArchiveProject();
    target.setId(origin.getId());
    target.setName(origin.getName());
    target.setType(origin.getType());
    target.setPermitReference(origin.getPermitReference());
    target.setPlanningReference(origin.getPlanningReference());
    target.setNetEmissions(ARCHIVE_SUBSTANCES.stream()
        .collect(Collectors.toMap(Function.identity(), substance -> getEmission(origin, substance))));
    target.setAeriusVersion(origin.getAeriusVersion());
    if (Double.compare(origin.getCentroidX(), 0.0) != 0 && Double.compare(origin.getCentroidY(), 0.0) != 0) {
      target.setCentroid(new Point(origin.getCentroidX(), origin.getCentroidY()));
    }
    return target;
  }

  private static double getEmission(final InCombinationProcessResponse.ArchiveProject origin, final Substance substance) {
    return Optional.ofNullable(origin.getNetEmissions())
        .map(x -> x.get(substance.getName()))
        .orElse(0.0);
  }

  /**
   * Maps the results from Archive back on earlier collected points that are calculated in the chunker run.
   *
   * @param iCPResults results from Archive
   * @return List of result points
   */
  private List<AeriusResultPoint> convertAndFilterResults(final List<ArchiveResult> iCPResults) {
    final Map<String, EmissionResults> map = new HashMap<>();
    final Map<Integer, List<ArchiveResultValue>> receptorSubPointResultsMap = new HashMap<>();

    // Put all Archive results in a map to easily map results to actual points.
    iCPResults.forEach(ar -> putPoint(map, receptorSubPointResultsMap, ar));
    // Add the (aggregated) results for subpoints to the receptors
    receptorSubPointResultsMap.entrySet().stream()
        .forEach(e -> addAggregatedSubPointResults(map, e.getKey(), e.getValue()));
    // map points to new results points with Archive results as data
    return allPoints.entrySet().stream()
        .filter(e -> map.containsKey(e.getKey()))
        .map(e -> mapICPResult(map.get(e.getKey()), e.getValue()))
        .toList();
  }

  /**
   * Stores the results of the {@link ArchiveResult} in a map with receptorId/subPointId/pointType as key.
   * Also keep track of the sub point results per receptor, for later aggregation.
   *
   * @param pointResultMap map to store Archive result in
   * @param subPointResultsMap map to store sub receptor results per receptor in.
   * @param aResult Result from Archive
   */
  private void putPoint(final Map<String, EmissionResults> pointResultMap,
      final Map<Integer, List<ArchiveResultValue>> subPointResultsMap, final ArchiveResult aResult) {
    aResult.getResults().forEach(value -> {
      if (value.getType() == EmissionResultType.DEPOSITION) {
        putResult(pointResultMap, aResult, value, AeriusPointType.RECEPTOR);
      } else {
        putResult(pointResultMap, aResult, value, AeriusPointType.SUB_RECEPTOR);
        if (allPoints.containsKey(key(aResult.getReceptorId(), aResult.getSubPointId(), AeriusPointType.SUB_RECEPTOR))) {
          subPointResultsMap.computeIfAbsent(aResult.getReceptorId(), k -> new ArrayList<>()).add(value);
        }
      }
    });
  }

  /**
   * Stores a single result in a map with receptorId/subPointId/pointType as key.
   *
   * @param map map to store Archive result in
   * @param aResult Result from Archive
   * @param value value to store
   * @param pointType point type to store it for
   */
  private static void putResult(final Map<String, EmissionResults> map, final ArchiveResult aResult,
      final InCombinationProcessResponse.ArchiveResultValue value, final AeriusPointType pointType) {
    map.computeIfAbsent(key(aResult.getReceptorId(), aResult.getSubPointId(), pointType), k -> new EmissionResults())
        .add(EmissionResultKey.safeValueOf(value.getSubstance(), value.getType()), value.getValue());
  }

  /**
   * Aggregate the sub point results to 1 value per emission result key for the receptor, if not yet present.
   *
   * @param map map to store result in
   * @param receptorId The receptor ID to add results for
   * @param subPointResults The values that were retrieved for the sub points associated with the receptor
   */
  private static void addAggregatedSubPointResults(final Map<String, EmissionResults> map,
      final int receptorId, final List<ArchiveResultValue> subPointResults) {
    final Map<EmissionResultKey, List<Double>> resultsPerERK = new EnumMap<>(EmissionResultKey.class);
    subPointResults.forEach(v -> resultsPerERK
        .computeIfAbsent(EmissionResultKey.safeValueOf(v.getSubstance(), v.getType()), k -> new ArrayList<>())
        .add(v.getValue()));
    resultsPerERK.forEach((erk, resultPerERK) -> {
      final EmissionResults emissionResults = map.computeIfAbsent(key(receptorId, 0, AeriusPointType.RECEPTOR), k -> new EmissionResults());
      // While this shouldn't occur with current implementation, don't overwrite any results that were directly retrieved from Archive.
      if (!emissionResults.hasResult(erk)) {
        emissionResults.add(erk, resultPerERK.stream().mapToDouble(x -> x).average().orElse(0.0));
      }
    });

  }

  private static AeriusResultPoint mapICPResult(final EmissionResults iCPResult, final AeriusPoint aeriusPoint) {
    final AeriusResultPoint archiveResultPoint = new AeriusResultPoint(aeriusPoint);

    if (iCPResult != null) {
      iCPResult.copyTo(archiveResultPoint.getEmissionResults());
    }
    return archiveResultPoint;
  }

  private static String key(final int receptorId, final int subPointId, final AeriusPointType pointType) {
    return receptorId + "-" + subPointId + "-" + pointType.name();
  }

  private static Set<Integer> determineReceptorsWithSubPoints(final Collection<AeriusPoint> resultPoints) {
    return resultPoints.stream()
        .filter(x -> x.getPointType() == AeriusPointType.SUB_RECEPTOR)
        .map(AeriusPoint::getParentId)
        .collect(Collectors.toSet());
  }
}
