/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.own2000.OwN2000ChunkerRepository;
import nl.overheid.aerius.db.own2000.OwN2000ChunkerRepository.AResult;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Keeps the state of a Wet NatuurBeschreming calculation.
 * The calculation continues until the distance to the last complete calculated area and the first area that was completely below the threshold value
 * is at least {@link #MINIMUM_CALCULATION_DISTANCE}. If a nature area is found to be above threshold, and an earlier area was below the threshold
 * the distance counting is reset until a new area is found to be below the threshold value.
 */
class ChunkState {
  private static final Logger LOG = LoggerFactory.getLogger(ChunkState.class);

  private static final int MINIMUM_CALCULATION_DISTANCE = 10_000;
  private static final int MINIMUM_AREAS_CALCULATED = 5;

  private final Queue<AreaData> areasSizes = new ArrayDeque<>();
  private final PMF pmf;
  private final OwN2000ChunkerRepository rr;
  private final CalculationJob calculationJob;
  private final double relevantThresholdValue;
  /**
   * Distance to the area that was first found to have results below the threshold.
   */
  private int firstBelowThresholdDistance = -1;
  private int areasCalculatedBelowThreshold;

  public ChunkState(final PMF pmf, final CalculationJob calculationJob, final List<Entry<Integer, Integer>> listAreas,
      final OwN2000ChunkerRepository rr) throws SQLException {
    this.pmf = pmf;
    this.calculationJob = calculationJob;
    this.rr = rr;
    this.relevantThresholdValue = ConstantRepository.getDouble(pmf, ConstantsEnum.PRONOUNCEMENT_THRESHOLD_VALUE);
  }

  /**
   * Add the given natura 2000 to the list of areas to calculate.
   * @param n2kId id of the natura 2000 area
   * @param size number of point to calculate for the given area
   * @param distance minimal distance to the sources.
   */
  public void addN2KArea(final Integer n2kId, final int size, final int distance) {
    areasSizes.add(new AreaData(n2kId, size, distance));
  }

  public boolean resultsPending() throws AeriusException {
    try (Connection con = pmf.getConnection()) {
      return resultsPending(con);
    } catch (final SQLException e) {
      LOG.error("Calculating pending results failed:", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Returns true as long as there are natura 2000 areas to calculate.
   * @param con database connection
   * @return
   * @throws SQLException
   */
  boolean resultsPending(final Connection con) throws SQLException {
    final boolean pending;
    final AreaData as = areasSizes.peek();
    if (as == null) {
      pending = false;
      LOG.trace("Everything calculated.");
    } else {
      final int assessmentId = as.getAssessmentId();
      final ScenarioCalculations scenarioCalculations = calculationJob.getScenarioCalculations();
      final int receptorCount = as.getReceptorCount();
      final List<AResult> results = queryResultStatus(con, assessmentId, scenarioCalculations, receptorCount);

      if (!results.isEmpty()) {
        LOG.trace("Yes all receptors are calculated for assessment area:{}", assessmentId);
        // only call resultsPending if current result is above threshold. If that is true the next area must be checked.
        final boolean aboveThreshold = calculateAboveThreshold(results);
        final int distance = getDistance(as);
        final int deltaDistance = updateThresholdCounters(aboveThreshold, distance);
        final boolean belowMinimalDistance = deltaDistance < MINIMUM_CALCULATION_DISTANCE;
        final boolean notMinimalAreasCalculated = areasCalculatedBelowThreshold < MINIMUM_AREAS_CALCULATED;
        LOG.trace("Below minimal distance:{}({}m), Distance to first low value area:{}m, Areas with minimal:{}(#{}), "
            + "aboveThreshold:{}, receptors:{}", belowMinimalDistance, distance, deltaDistance, notMinimalAreasCalculated,
            areasCalculatedBelowThreshold, aboveThreshold, receptorCount);
        pending = (belowMinimalDistance || notMinimalAreasCalculated) && resultsPending(con);
      } else {
        LOG.trace("Not all receptors calculated for n2k id:{}", assessmentId);
        // Not all result are stored in the database yet, so still pending for results.
        pending = true;
      }
    }
    return pending;
  }

  private List<AResult> queryResultStatus(final Connection con, final int assessmentId, final ScenarioCalculations scenarioCalculations,
      final int receptorCount) throws SQLException {
    final List<AResult> results = new ArrayList<>();
    for (final Calculation calculation : scenarioCalculations.getCalculations()) {
      final AResult result = rr.getCalculationResultsForAssessmentArea(con, assessmentId, calculation.getCalculationId());

      if (result.getCount() != receptorCount) {
        return List.of();
      }
      results.add(result);
    }
    return results;
  }

  private int getDistance(final AreaData currentArea) {
    final AreaData next = areasSizes.peek();
    return (next == null ? currentArea : next).getDistance();
  }

  private boolean calculateAboveThreshold(final List<AResult> results) {
    return results.stream().anyMatch(r -> r.getMax() > relevantThresholdValue);
  }

  private int updateThresholdCounters(final boolean aboveThreshold, final int distance) {
    if (aboveThreshold) {
      firstBelowThresholdDistance = -1;
      areasCalculatedBelowThreshold = 0;
    } else {
      areasCalculatedBelowThreshold++;
      if (firstBelowThresholdDistance < 0) {
        firstBelowThresholdDistance = distance;
      }
    } // else keep the current value
    return firstBelowThresholdDistance < 0 ? -1 : (distance - firstBelowThresholdDistance);
  }
}
