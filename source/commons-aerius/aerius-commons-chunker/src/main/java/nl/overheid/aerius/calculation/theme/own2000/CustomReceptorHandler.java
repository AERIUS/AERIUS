/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.overheid.aerius.calculation.base.IncludePointFilter;
import nl.overheid.aerius.calculation.base.ResultPostProcessHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.util.FeaturePointToEnginePointUtil;

/**
 * Handler for {@link nl.overheid.aerius.shared.domain.calculation.CalculationMethod.CUSTOM_RECEPTORS} calculations.
 * Translates calculation points to receptors in the normal grid.
 * Only mapped receptors should be calculated in this grid.
 * Includes a result post-processing step that converts the receptors back to the calculation points.
 */
public class CustomReceptorHandler implements IncludePointFilter, ResultPostProcessHandler {

  private final List<AeriusPoint> calculationPoints;
  private final Map<AeriusPoint, Integer> pointsToReceptors = new HashMap<>();
  private final Set<Integer> receptors = new HashSet<>();

  public CustomReceptorHandler(final ReceptorGridSettings rgs, final CalculationJob calculationJob) {
    this.calculationPoints = FeaturePointToEnginePointUtil.convert(calculationJob.getScenarioCalculations().getCalculationPoints());

    final ReceptorUtil receptorUtil = new ReceptorUtil(rgs);
    for (final AeriusPoint calculationPoint : calculationPoints) {
      if (rgs.getBoundingBox().isPointWithinBoundingBox(calculationPoint)) {
        final int receptorId = receptorUtil.getReceptorIdFromCoordinate(calculationPoint.getX(), calculationPoint.getY());
        receptors.add(receptorId);
        pointsToReceptors.put(calculationPoint, receptorId);
      }
    }
  }

  @Override
  public void postProcessSectorResults(final int calculationId, final List<AeriusResultPoint> results) {
    final Map<Integer, EmissionResults> receptorResults = new HashMap<>();
    for (final AeriusResultPoint result : results) {
      receptorResults.put(result.getId(), result.getEmissionResults());
    }
    // clear the original receptor-based results
    results.clear();

    for (final AeriusPoint calculationPoint : calculationPoints) {
      final int receptorId = pointsToReceptors.get(calculationPoint);

      final EmissionResults emissionResults = receptorResults.get(receptorId);
      if (emissionResults != null) {
        final AeriusResultPoint resultPoint = new AeriusResultPoint(calculationPoint);
        for (final Map.Entry<EmissionResultKey, Double> emissionValue : emissionResults.entrySet()) {
          resultPoint.setEmissionResult(emissionValue.getKey(), emissionValue.getValue());
        }
        results.add(resultPoint);
      }
    }
  }

  @Override
  public boolean includePoint(final AeriusPoint point) {
    return point.getPointType() != AeriusPoint.AeriusPointType.RECEPTOR || receptors.contains(point.getId());
  }
}
