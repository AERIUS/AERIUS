/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;

/**
 * Implementation of a {@link WorkTracker}.
 */
class WorkTrackerImpl implements WorkTracker {

  private static final Logger LOGGER = LoggerFactory.getLogger(WorkTrackerImpl.class);

  private final Semaphore taskMapLock = new Semaphore(0);
  private final AtomicInteger counter = new AtomicInteger();

  private CalculationState state = CalculationState.INITIALIZED;
  private Exception exception;
  private boolean waitForCompletion;

  @Override
  public synchronized CalculationState start() {
    if (state == CalculationState.INITIALIZED) {
      state = CalculationState.RUNNING;
    }
    return state;
  }

  @Override
  public boolean isCanceled() {
    return state == CalculationState.CANCELLED;
  }

  @Override
  public void cancel(final Exception exception) {
    synchronized (taskMapLock) {
      if (!isCanceled()) { // only cancel once.
        this.exception = exception;
        state = CalculationState.CANCELLED;
        taskMapLock.release();
      }
    }
  }

  @Override
  public Exception getException() {
    return exception;
  }

  @Override
  public CalculationState getState() {
    return state;
  }

  @Override
  public void increment() {
    counter.incrementAndGet();
  }

  @Override
  public void decrement() {
    synchronized (taskMapLock) {
      counter.decrementAndGet();
      if (state != CalculationState.CANCELLED && waitForCompletion && counter.get() == 0) {
        taskMapLock.release();
      }
    }
  }

  boolean isWaiting() {
    return taskMapLock.getQueueLength() > 0;
  }

  @Override
  public CalculationState waitForCompletion() throws InterruptedException {
    LOGGER.trace("waitForCompletion");
    synchronized (taskMapLock) {
      waitForCompletion = state != CalculationState.CANCELLED && counter.get() != 0;
    }
    if (waitForCompletion) {
      taskMapLock.acquire();
    }
    synchronized (taskMapLock) {
      if (state != CalculationState.CANCELLED) {
        state = CalculationState.COMPLETED;
      }
    }
    LOGGER.trace("waitForCompletion finished:{}", state);
    return state;
  }
}
