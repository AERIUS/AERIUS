/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import nl.overheid.aerius.calculation.base.ResultPostProcessHandler;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;

/**
 * Result post-process handler that will update results based on the netting factor for situations of the OFF_SITE_REDUCTION type.
 *
 * Results will be adjusted using the following formula:
 * result = result * (1 - netting factor)
 */
public class OffSiteReductionResultPostProcessHandler implements ResultPostProcessHandler {

  private final Map<Integer, BigDecimal> offSiteReductionCalculationsToNettingFactor;

  public OffSiteReductionResultPostProcessHandler(final ScenarioCalculations scenarioCalculations) {
    offSiteReductionCalculationsToNettingFactor = scenarioCalculations.getScenario().getSituations().stream()
        .filter(OffSiteReductionResultPostProcessHandler::isOffSiteReductionSituation)
        .collect(Collectors.toMap(
            situation -> scenarioCalculations.getCalculationForSituation(situation).get().getCalculationId(),
            situation -> BigDecimal.valueOf(situation.getNettingFactor())));
  }

  @Override
  public void postProcessSectorResults(final int calculationId, final List<AeriusResultPoint> results) {
    if (offSiteReductionCalculationsToNettingFactor.containsKey(calculationId)) {
      results.forEach(resultPoint -> adjustResults(resultPoint, offSiteReductionCalculationsToNettingFactor.get(calculationId)));
    }
  }

  private static void adjustResults(final AeriusResultPoint resultPoint, final BigDecimal nettingFactor) {
    resultPoint.getEmissionResults().entrySet().forEach(entry -> {
      final BigDecimal correctedResult = BigDecimal.ONE.subtract(nettingFactor).multiply(BigDecimal.valueOf(entry.getValue()));
      entry.setValue(correctedResult.doubleValue());
    });
  }

  /**
   * Convenience method to check if an instance of this handler will do anything for a scenario calculation.
   */
  public static boolean canApply(final ScenarioCalculations scenarioCalculations) {
    return scenarioCalculations.getScenario().getSituations().stream()
        .anyMatch(OffSiteReductionResultPostProcessHandler::isOffSiteReductionSituation);
  }

  private static boolean isOffSiteReductionSituation(final ScenarioSituation situation) {
    return situation.getType() == SituationType.OFF_SITE_REDUCTION && situation.getNettingFactor() != null && situation.getNettingFactor() > 0;
  }

}
