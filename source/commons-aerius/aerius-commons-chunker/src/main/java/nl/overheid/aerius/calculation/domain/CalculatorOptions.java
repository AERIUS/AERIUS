/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.io.Serializable;

/**
 * Data class for Calculator options that guide the calculation chunking process.
 */
public class CalculatorOptions implements Serializable {

  private static final long serialVersionUID = 1L;

  private int maxCalculationEngineUnits;
  private int minReceptor;
  private int maxReceptors;
  private int maxEngineSources;
  /**
   * Flag to indicate if the progress is based on the total results returned by the worker, or based on intermediate progress results.
   */
  private boolean countTotalProgressOnly = true;

  public int getMaxCalculationEngineUnits() {
    return maxCalculationEngineUnits;
  }

  public void setMaxCalculationEngineUnits(final int maxCalculationEngineUnits) {
    this.maxCalculationEngineUnits = maxCalculationEngineUnits;
  }

  public int getMinReceptor() {
    return minReceptor;
  }

  public void setMinReceptors(final int minReceptor) {
    this.minReceptor = minReceptor;
  }

  public int getMaxReceptors() {
    return maxReceptors;
  }

  public void setMaxReceptors(final int maxReceptors) {
    this.maxReceptors = maxReceptors;
  }

  public int getMaxEngineSources() {
    return maxEngineSources;
  }

  public void setMaxEngineSources(final int maxEngineSources) {
    this.maxEngineSources = maxEngineSources;
  }

  public boolean isCountTotalProgressOnly() {
    return countTotalProgressOnly;
  }

  /**
   * If called progress reports are done with intermediate progress reports sent by the model worker.
   * Otherwise progress will be computed based on results received from the model worker.
   */
  public void setCalculateIntermediateProgressReports() {
    this.countTotalProgressOnly = false;
  }
}
