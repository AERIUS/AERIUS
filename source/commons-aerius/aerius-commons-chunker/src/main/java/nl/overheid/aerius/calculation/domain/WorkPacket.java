/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.Collection;
import java.util.List;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;

/**
 * A complete package of work for the same set of points. The work packet contains multiple JobPackets.
 * A JobPacket contains per calculation id/year a set of grouped sources packets. Each grouped source packet
 * will be a individual calculation. The result of a single point is the sum of all grouped source packet results.
 * @param <P>
 */
public class WorkPacket<P extends AeriusPoint> {

  /**
   * A packet of sources. Multiple situations are put into this packet.
   */
  public static class JobPacket {
    private final Integer calculationId;
    private final int year;
    private final List<GroupedSourcesPacket> sourcesPackets;

    public JobPacket(final Integer calculationId, final int year, final List<GroupedSourcesPacket> sourcesPackets) {
      this.calculationId = calculationId;
      this.year = year;
      this.sourcesPackets = sourcesPackets;
    }

    public Integer getCalculationId() {
      return calculationId;
    }

    public int getYear() {
      return year;
    }

    public List<GroupedSourcesPacket> getSourcesPackets() {
      return sourcesPackets;
    }
  }

  private final Collection<P> points;
  private final List<JobPacket> jobPackets;

  /**
   * Constructor.
   *
   * @param points calculation points (receptors) to calculate.
   * @param jobPackets packet of sources to calculate.
   */
  public WorkPacket(final Collection<P> points, final List<JobPacket> jobPackets) {
    this.points = points;
    this.jobPackets = jobPackets;
  }

  public Collection<P> getPoints() {
    return points;
  }

  /**
   * Count only points that will actually be calculated. {@link IsSubPoint} points that are not of a sub type are used for aggregation of
   * deposition and not actually using in the calculation and therefore should not be counted as point to be calculated.
   *
   * @param packet
   * @return
   */
  public static <P extends AeriusPoint> int nrOfPointsToCalculate(final Collection<P> points) {
    return (int) points.stream().filter(p -> !(p instanceof IsSubPoint &&
        (p.getPointType() == AeriusPointType.RECEPTOR || p.getPointType() == AeriusPointType.POINT))).count();
  }

  public List<JobPacket> getJobPackets() {
    return jobPackets;
  }
}
