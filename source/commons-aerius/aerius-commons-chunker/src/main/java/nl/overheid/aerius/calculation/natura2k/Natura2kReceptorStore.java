/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.natura2k;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.locationtech.jts.geom.Geometry;

import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * Storage for Natura 2000 areas calculation points and to return the n2k areas in order of distances to the sources.
 */
public class Natura2kReceptorStore {

  private final Map<Integer, List<AeriusPoint>> n2kPoints;
  private final Map<Integer, Geometry> n2kGeometries;

  private Natura2kReceptorStore(final Map<Integer, List<AeriusPoint>> n2kPoints, final Map<Integer, Geometry> n2kGeometries) {
    this.n2kPoints = n2kPoints;
    this.n2kGeometries = n2kGeometries;
  }

  public static N2kReceptorStoreBuilder builder(final ReceptorUtil receptorUtil, final GridSettings gridSettings) {
    return new N2kReceptorStoreBuilder(receptorUtil, gridSettings);
  }

  /**
   * Returns the receptors for the given n2k area.
   *
   * @param n2kId id of the n2k area
   * @return list of receptors in the n2k area
   */
  public List<AeriusPoint> getReceptors(final Integer n2kId) {
    return new ArrayList<>(n2kPoints.get(n2kId));
  }

  /**
   * Returns all receptors for n2k areas.
   * @return Set of receptors for n2k areas.
   */
  public Set<AeriusPoint> getAllReceptors() {
    return n2kPoints.values().stream().flatMap(List::stream).collect(Collectors.toSet());
  }

  public List<Entry<Integer, Integer>> order(final EmissionSourceListSTRTree tree) {
    final List<Entry<Integer, Integer>> distances = n2kGeometries.entrySet().stream()
        .map(e -> new SimpleEntry<Integer, Integer>(e.getKey(), Integer.valueOf((int) tree.findShortestDistance(e.getValue()))))
        .collect(Collectors.toList());

    Collections.sort(distances, (o1, o2) -> Integer.compare(o1.getValue(), o2.getValue()));
    return distances;
  }

  /**
   * Builder class for creating a {@link Natura2kReceptorStore}.
   */
  public static class N2kReceptorStoreBuilder {
    private final Comparator<AeriusPoint> comparator;
    private final Map<Integer, List<AeriusPoint>> n2kPoints = new HashMap<>();
    private final Map<Integer, Geometry> n2kGeometries = new HashMap<>();

    N2kReceptorStoreBuilder(final ReceptorUtil receptorUtil, final GridSettings gridSettings) {
      comparator = (o1, o2) -> {
        final int comp = Integer.compare(receptorUtil.getZoomLevelForReceptor(o2.getId()), receptorUtil.getZoomLevelForReceptor(o1.getId()));

        return comp == 0 ? Integer.compare(o1.getId(), o2.getId()) : comp;
      };
    }

    public void addGeometry(final Integer n2kId, final Geometry geometry) {
      n2kGeometries.put(n2kId, geometry);
    }

    /**
     * Adds the points for a specific nature2000 area. Returns true if the points were added, and false if the points list was empty.
     *
     * @param n2kId id of natura2000 area
     * @param points calculation points in the natura2000 area
     * @return true if added, false if points list was empty
     */
    public boolean addPoints(final Integer n2kId, final List<AeriusPoint> points) {
      if (points.isEmpty()) {
        // don't add empty areas there is noting to calculate.
        return false;
      }
      final List<AeriusPoint> sorted = new ArrayList<>(points);

      Collections.sort(sorted, comparator);
      n2kPoints.put(n2kId, sorted);
      return true;
    }

    public Natura2kReceptorStore build() {
      // filter all geometries of n2k areas that don't have any points.
      n2kGeometries.keySet().stream().filter(n2kId -> !n2kPoints.containsKey(n2kId)).forEach(n2kGeometries::remove);
      return new Natura2kReceptorStore(n2kPoints, n2kGeometries);
    }
  }
}
