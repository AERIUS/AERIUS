/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;

import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.otel.TracerUtil;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Report as Metric and log when calculation is completed.
 */
class MetricReporter implements PostCalculationHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(MetricReporter.class);
  private static final String CALCULATION_COMPLETED_METRIC = "aer.worker.calculation.completed";

  private final ExportType exportType;

  public MetricReporter(final ExportType exportType) {
    this.exportType = exportType;
  }

  @Override
  public void onCompleted(final CalculationJob calculationJob) throws AeriusException {
    final String calculationMethod =  calculationJob.getThemeCalculationJobOptions().getCalculationMethodMetricName(calculationJob);
    final String exportTypeName = exportType.name().toLowerCase(Locale.ROOT);
    final Attributes attributes = Attributes.builder()
        .put(AttributeKey.stringKey("export_type"), exportTypeName)
        .put(AttributeKey.stringKey("calculation_method"), calculationMethod)
        .build();

    TracerUtil.getLongCounter(CALCULATION_COMPLETED_METRIC).add(1, attributes);
    LOGGER.info("[{}] Calculation Job Completed Metric: {},{}", calculationJob.getJobIdentifier(), calculationMethod, exportTypeName);
  }
}
