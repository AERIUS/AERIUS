/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

/**
 * Semaphore that manages the number of work tasks that are run concurrently.
 */
public interface WorkSemaphore {

  /**
   * Release all callers waiting in acquire and cleans up event handlers it's holding. This method should be called when the work is
   * or cancelled.
   */
  void drain();

  /**
   * Call when a new tasks should be executed. Waits until a slot is free.
   *
   * @param queueName name of the queue the tasks will be sent to. Each queue will have it's own semaphore.
   */
  void acquire(String queueName) throws InterruptedException;

  /**
   * Call when a tasks is finished.
   *
   * @param queueName name of the queue the tasks will be sent to. Each queue will have it's own semaphore.
   */
  void release(String queueName);

}
