/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.index.strtree.GeometryItemDistance;
import org.locationtech.jts.index.strtree.STRtree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * {@link STRtree} for {@link EmissionSourceFeature}s.
 */
public class EmissionSourceListSTRTree implements Serializable {

  private static final long serialVersionUID = 3392130242144636420L;

  private static final Logger LOG = LoggerFactory.getLogger(EmissionSourceListSTRTree.class);
  private static final GeometryItemDistance ITEM_DIST = new GeometryItemDistance();

  private final STRtree tree = new STRtree();
  private final Set<Geometry> set = new HashSet<>();

  private final EmissionSourceSTRTReeVisitor visitor;

  /**
   * Constructs a tree.
   */
  public EmissionSourceListSTRTree() {
    visitor = new EmissionSourceSTRTReeVisitor(set);
  }

  /**
   * Builds a {@link EmissionSourceListSTRTree} given a list of source lists. No sources can be added to this tree as the #build() method is called.
   * @param sourceLists list of source lists.
   * @return tree build
   * @throws AeriusException
   */
  public static EmissionSourceListSTRTree buildTree(final List<List<EmissionSourceFeature>> sourceLists) throws AeriusException {
    final EmissionSourceListSTRTree sourcesTree = new EmissionSourceListSTRTree();
    for (final List<EmissionSourceFeature> esl : sourceLists) {
      sourcesTree.add(esl);
    }
    sourcesTree.build();
    return sourcesTree;
  }

  /**
   * Add the geometries of an {@link EmissionSourceList}.
   * @param esl the list
   * @throws AeriusException exception in case of problems with geometries.
   */
  public void add(final List<EmissionSourceFeature> esl) throws AeriusException {
    for (final EmissionSourceFeature es : esl) {
      es.accept(visitor);
    }
  }

  /**
   * Builds the tree. No objects can be added after this command.
   */
  public void build() {
    for (final Geometry geo : set) {
      tree.insert(geo.getEnvelopeInternal(), geo);
    }
    tree.build();
    set.clear();
  }

  /**
   * Finds the shortest distance to the given point.
   * @param point point to get the shortest distance
   * @return shortest distance to point
   * @throws AeriusException throws exception in case point didn't contain valid point WKT.
   */
  public double findShortestDistance(final SridPoint point) throws AeriusException {
    return findShortestDistance(GeometryUtil.getGeometry(point.toWKT()));
  }

  /**
   * Finds the shortest distance to the given geometry.
   * @param geometry geometry to get the shortest distance
   * @return shortest distance
   */
  public double findShortestDistance(final Geometry geometry) {
    final Object nn = tree.nearestNeighbour(geometry.getEnvelopeInternal(), geometry, ITEM_DIST);
    if (!(nn instanceof Geometry)) {
      LOG.error("#findShortestDistance didn't return a geometry:{}", nn);
    }
    return nn instanceof final Geometry ng ? geometry.distance(ng) : Integer.MAX_VALUE;
  }
}
