/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.ThemeCalculationJobOptions;

/**
 * NCA calculation job specific options.
 */
class NCACalculationJobOptions implements ThemeCalculationJobOptions {
  // Optional ADMS license
  private byte[] admsLicense;

  /**
   * To enable ADMS demo mode
   */
  private boolean demoMode;

  /**
   * @return Optional License for CERC ADMS tool. Will be passed to ADMS worker.
   */
  public byte[] getAdmsLicense() {
    return admsLicense;
  }

  public void setAdmsLicense(final byte[] admsLicense) {
    this.admsLicense = admsLicense;
  }

  public boolean isDemoMode() {
    return demoMode;
  }

  public void setDemoMode(final boolean demoMode) {
    this.demoMode = demoMode;
  }

  @Override
  public String getCalculationMethodMetricName(final CalculationJob calculationJob) {
    return ThemeCalculationJobOptions.super.getCalculationMethodMetricName(calculationJob) + (demoMode ? "_demo" : "");
  }
}
