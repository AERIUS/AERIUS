/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.natura2k;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import nl.overheid.aerius.calculation.base.AeriusPointsWorkDistributor;
import nl.overheid.aerius.calculation.base.IncludePointFilter;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.theme.own2000.CustomReceptorHandler;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Abstract base class to determine the receptors to calculate. Receptors are from nature areas.
 * It sorts the nature areas based on the distance to the sources.
 */
public abstract class AbstractNatura2KWorkDistributor extends AeriusPointsWorkDistributor {

  protected final Natura2kReceptorStore n2kReceptorStore;
  protected final ReceptorGridSettings receptorGridSettings;

  private Iterator<Entry<Integer, Integer>> listN2KIdIterator;
  private final Set<Integer> receptorsDone = new HashSet<>();
  private IncludePointFilter includePointFilter = point -> true;

  protected AbstractNatura2KWorkDistributor(final PMF pmf, final Natura2kReceptorStore n2kReceptorStore, final SectorCategories sectorCategories,
      final ReceptorGridSettings receptorGridSettings) {
    super(pmf, sectorCategories);
    this.n2kReceptorStore = n2kReceptorStore;
    this.receptorGridSettings = receptorGridSettings;
  }

  protected void init(final CalculationJob calculationJob, final List<Entry<Integer, Integer>> listn2k) throws SQLException, AeriusException {
    listN2KIdIterator = listn2k.iterator();
    if (calculationJob.getCalculationSetOptions().getCalculationMethod() == CalculationMethod.CUSTOM_RECEPTORS) {
      this.includePointFilter = new CustomReceptorHandler(receptorGridSettings, calculationJob);
    }
    super.init(calculationJob);
  }

  @Override
  protected boolean hasMorePoints() {
    return listN2KIdIterator.hasNext();
  }

  @Override
  protected List<AeriusPoint> collectPoints() throws AeriusException {
    if (listN2KIdIterator.hasNext()) {
      // Get all receptors of a single nature area.
      return n2kReceptorStore.getReceptors(listN2KIdIterator.next().getKey());
    } else {
      // No more points to calculate, break out of the while loop
      return List.of();
    }
  }

  @Override
  protected int addPoints(final List<AeriusPoint> pointsToCalculate, final AeriusPoint pointToAdd)
      throws AeriusException {
    int added = 0;

    if (checkPoint(pointToAdd) && receptorsDone.add(pointToAdd.getId())) {
      added += addPoint(pointsToCalculate, pointToAdd);
    }
    return added;
  }

  /**
   * Add the point to the pointToCalculate list.
   * Can be used to add additional points that need to be calculated, like sub receptors.
   *
   * @param pointsToCalculate list of points to calculate
   * @param aeriusPoint point to add.
   * @return results the actual number of points added to the list.
   * @throws AeriusException
   */
  protected abstract int addPoint(final List<AeriusPoint> pointsToCalculate, final AeriusPoint aeriusPoint) throws AeriusException;

  /**
   * Additional method to determine if a point needs to be added to be calculated.
   * For example if the receptor is to far away it should not be added
   * @param aeriusPoint point to check
   * @return true if point should be calculated
   * @throws AeriusException
   */
  protected boolean checkPoint(final AeriusPoint aeriusPoint) throws AeriusException {
    return includePointFilter.includePoint(aeriusPoint);
  }
}
