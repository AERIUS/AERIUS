/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.ModelProgressReport;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * Keeps track of the progress of the job.
 * There are 2 ways the progress is calculated by this handler.
 *
 * <h3>Per ModelProgressReport</h3>
 * When using model progress report the progress is reported in intervals by the worker. This is used by the ADMS worker.
 * Than when the total results arrive these are ignored, because the total is already passed via the model progress.
 * Intermediate results mean the worker is not yet finished with the work, but has already some indication on the progress.
 * This is useful if individual chunks take a long time to calculate.
 *
 * The progress is calculated per chunk. For each chunk it's tracked what the progress was and the next time the progress for the chunk is
 * reported the delta relative to the previous report is calculated. This delta is added to the total progress as stored in the database.
 * This way it can handle restarting of chunks. When a worker crashes and the chunk would be picked up by another worker, the new worker
 * will report a new progress. But because we kept the previous progress as an absolute number we can get an correct delta. This can even be a
 * negative number initially. But that is correct.
 *
 *
 * <h3>On Total results</h3>
 * When using total results the progress is kept track of by the results returned by the worker.
 * This means the points in the chunk calculated are received from the worker and will be processed by the chunker.
 */
class TrackJobProgressHandler implements CalculationResultHandler {

  private static final Logger LOG = LoggerFactory.getLogger(TrackJobProgressHandler.class);

  private final PMF pmf;
  private final String jobKey;
  private final boolean countTotalProgressOnly;
  private final Map<String, Double> chunkModelProgressReports = new HashMap<>();
  private boolean jobFound;

  /**
   * Default constructor.
   * @param pmf Persistence manager factory for database operations.
   * @param calculationJob calculatorJob
   * @param jobKey The correlation id of the job.
   */
  public TrackJobProgressHandler(final PMF pmf, final CalculationJob calculationJob) {
    this.pmf = pmf;
    this.jobKey = calculationJob.getJobIdentifier().getJobKey();
    countTotalProgressOnly = calculationJob.getCalculatorOptions().isCountTotalProgressOnly();

    try (final Connection con = pmf.getConnection()) {
      jobFound = JobRepository.getJobId(con, jobKey) > 0;

      if (!jobFound) {
        LOG.warn("I am created to track job '{}' that doesn't exist. I won't track anything. "
            + "If this keeps happening it's likely parts of the application are configured to use different databases.", jobKey);
        // This could also mean that a new database deploy is done (or the job manually removed from the database)
        // while a calculation is still present on the queue, which results in the job being lost.
        // In such a case as they say in one of the most annoying song from a movie from 2013.. "Let it go!"
      }
    } catch (final SQLException e) {
      jobFound = false;
      // Keep executing the job so the user might get his job result, instead of crashing here. Do log it though.
      LOG.error("Error occurred while trying to check if job {} is present. "
          + "If there is a job present no progress will be recorded.", jobKey, e);
    }
  }

  private void failIfJobCancelledByUser(final Connection con) throws AeriusException {
    WorkerUtil.failIfJobCancelledByUser(con, jobKey);
  }

  boolean isJobFound() {
    return jobFound;
  }

  @Override
  public synchronized void onModelProgressReport(final ModelProgressReport progressReport) throws AeriusException {
    if (countTotalProgressOnly) {
      // If counting progress in only done on total do nothing here.
      return;
    }
    final Double previousValue = Optional.ofNullable(chunkModelProgressReports.put(progressReport.chunkId(), progressReport.progress())).orElse(0.0);
    final double delta = BigDecimal.valueOf(progressReport.progress()).subtract(BigDecimal.valueOf(previousValue)).setScale(16, RoundingMode.HALF_UP)
        .doubleValue();

    try (final Connection con = pmf.getConnection()) {
      failIfJobCancelledByUser(con);
      JobRepository.increaseNumberOfPointsCalculatedCounter(con, jobKey, delta);
    } catch (final SQLException e) {
      LOG.error("Error occurred while trying to increase hexagon counter for job {}", jobKey, e);
    }
  }

  @Override
  public void onTotalResults(final List<AeriusResultPoint> results, final int calculationId) throws AeriusException {
    if (jobFound && countTotalProgressOnly) {
      try (final Connection con = pmf.getConnection()) {
        failIfJobCancelledByUser(con);
        JobRepository.increaseNumberOfPointsCalculatedCounter(con, jobKey,
            results.stream().filter(rp -> !rp.getPointType().isSubReceptor()).mapToInt(AeriusResultPoint::getNumberOfSubPointsUsed).sum());
      } catch (final SQLException e) {
        LOG.error("Error occurred while trying to increase hexagon counter for job {}", jobKey, e);
      }
    }
  }
}
