/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import nl.overheid.aerius.calculation.domain.ZoomLevel;

/**
 * Grid zoom level object.
 */
public class GridZoomLevel implements ZoomLevel {

  private final int level;
  private final double diameter;
  private final int ribLength;
  private final int surface;
  private final int scale;
  private final int horizontal;

  /**
   * Constructor for level.
   *
   * @param level the level code
   * @param scaleMultiplier The surface scale by which a zoom level differs from a sibling
   * @param zoomLevelRibLengthMin Minimal zoom level in square meters
   * @param squareHor Number of horizontal minimal zoomlevel squares. This implies the zoom level has knowledge about the grid it lives in, but saves
   *          a division operation.
   */
  GridZoomLevel(final int level, final int scaleMultiplier, final int zoomLevelRibLengthMin, final int squareHor) {
    this.level = level;
    scale = (int) Math.pow(scaleMultiplier, level - 1D);
    ribLength = zoomLevelRibLengthMin * scale;
    horizontal = squareHor / scale;
    surface = ribLength * ribLength;
    // The following might appear.... strange... but the diameter is still sqrt(a^2 + b^2), and considering that, for squares, a == b == 'ribLength',
    // what must be rooted the first multiple of 'surface'
    diameter = Math.sqrt(surface * 2D);
  }

  public double getDiameter() {
    return diameter;
  }

  public int getHorizontal() {
    return horizontal;
  }

  public int getLevel() {
    return level;
  }

  public int getRibLength() {
    return ribLength;
  }

  public int getScale() {
    return scale;
  }

  public int getSurface() {
    return surface;
  }

  @Override
  public int hashCode() {
    return level;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final GridZoomLevel other = (GridZoomLevel) obj;
    return level == other.level;
  }

  @Override
  public String toString() {
    return "GridZoomLevel [level=" + level + "]";
  }
}
