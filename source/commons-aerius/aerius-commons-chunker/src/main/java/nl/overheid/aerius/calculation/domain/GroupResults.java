/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Store emission calculation results organized by point and with each point the result per group.
 */
public class GroupResults<T> {

  private static final Logger LOGGER = LoggerFactory.getLogger(GroupResults.class);

  /**
   * Keep track of all keys that had a result.
   */
  private final Set<T> keys = new HashSet<>();

  /**
   * Map<Receptor Id, GroupResultPoint>
   */
  private final Map<T, GroupResultPoint> results = new ConcurrentHashMap<>();
  /**
   * Expected number of results.
   */
  private final int expectedCount;
  private final Function<AeriusResultPoint, T> result2KeyFunction;

  /**
   * Creates a results storage expecting the given groups to be stored results for
   * @param expectedCount Expected number of results
   * @param result2KeyFunction function to compute the key from a point to use in the map.
   */
  public GroupResults(final int expectedCount, final Function<AeriusResultPoint, T> result2KeyFunction) {
    this.expectedCount = expectedCount;
    this.result2KeyFunction = result2KeyFunction;
  }

  /**
   * Puts the result for the given result point.
   *
   * @param engineDataKey engine data key identifier
   * @param groupId group id to relate the results with
   * @param resultPoint point and results to get emission results from
   * @return returns the results if all data present else null
   * @throws AeriusException
   */
  public GroupResultPoint put(final EngineDataKey engineDataKey, final int groupId, final AeriusResultPoint resultPoint)
      throws AeriusException {
    synchronized (this) {
      final T key = result2KeyFunction.apply(resultPoint);
      final GroupResultPoint srp = getGroupResultPoint(resultPoint, key);

      if (srp == null) {
        LOGGER.error("Too many results received from workers. Last for {} with id {}.", engineDataKey, groupId);
        throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
      }
      srp.put(engineDataKey, groupId, resultPoint.getEmissionResults());
      final int size = srp.size();

      if (size == expectedCount) {
        results.remove(key);
        return srp;
      }
      return null;
    }
  }

  /**
   * Returns the GroupResultPoint, if this was the first time the key appeared if will add the GroupResultPoint to the results map.
   * If all results were already retrieved and a GroupResultPoint was still requested it will return null.
   *
   * @param resultPoint
   * @param key
   * @return
   */
  private GroupResultPoint getGroupResultPoint(final AeriusResultPoint resultPoint, final T key) {
    final GroupResultPoint srp;

    if (keys.contains(key)) {
      srp = results.get(key);
    } else {
      keys.add(key);
      srp = results.computeIfAbsent(key, id -> new GroupResultPoint(resultPoint));
    }
    return srp;
  }

  public boolean isExpectingMoreResults() {
    return results.values().stream().anyMatch(point -> point.size() != expectedCount);
  }

  @Override
  public String toString() {
    return "GroupResults [expectedCount=" + expectedCount + ", results=" + results.size() + "]";
  }
}
