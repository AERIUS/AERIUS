/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;

/**
 * Sets the road local fNO2 in the calculation point depending on the settings made by the user.
 *
 * <li>LOCATION_BASED : set the value to 0.0, it will use the internal fNO2 value.
 * <li>ONE_CUSTOM_VALUE : set the value to the globally user defined value.
 * <li>INDIVIDUAL_CUSTOM_VALUES: leave the value as set by the user on the point.
 */
class NCAFractionNO2Supplier {

  private final boolean overrideFNO2;
  private final double globalRoadLocalFractionNO2;

  public NCAFractionNO2Supplier(final RoadLocalFractionNO2Option roadLocalFractionNO2Option, final Double roadLocalFractionNO2) {
    overrideFNO2 = roadLocalFractionNO2Option != RoadLocalFractionNO2Option.INDIVIDUAL_CUSTOM_VALUES;
    globalRoadLocalFractionNO2 = roadLocalFractionNO2Option == RoadLocalFractionNO2Option.ONE_CUSTOM_VALUE && roadLocalFractionNO2 != null
        ? roadLocalFractionNO2 : 0.0;
  }

  /**
   * Set the local road fNO2 to the point (if {@link ADMSCalculationPoint}) if this is required by the roadLocalFractionNO2 settings.
   * @param point to optional set the local road fNO2
   */
  public void setFractionNO2(final AeriusPoint point) {
    if (point instanceof final ADMSCalculationPoint admsPoint && overrideFNO2) {
      admsPoint.setRoadLocalFNO2(globalRoadLocalFractionNO2);
    } // else keep the INDIVIDUAL_CUSTOM_VALUE
  }
}
