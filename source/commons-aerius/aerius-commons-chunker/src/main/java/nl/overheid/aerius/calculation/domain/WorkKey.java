/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Key class for identifying a single task sent to a worker.
 */
public class WorkKey {

  private final String queueName;
  private final JobIdentifier jobIdentifier;
  private final int calculationId;
  private final EngineDataKey engineDataKey;

  public WorkKey(final String queueName, final JobIdentifier jobIdentifier, final int calculationId, final EngineDataKey engineDataKey) {
    this.queueName = queueName;
    this.jobIdentifier = jobIdentifier;
    this.calculationId = calculationId;
    this.engineDataKey = engineDataKey;
  }

  public String getQueueName() {
    return queueName;
  }

  /**
   * Returns the unique id identifying the whole work job.
   * @return
   */
  public JobIdentifier getJobIdentifier() {
    return jobIdentifier;
  }

  public int getCalculationId() {
    return calculationId;
  }

  public EngineDataKey getEngineDataKey() {
    return engineDataKey;
  }

  @Override
  public String toString() {
    return "WorkKey [queueName=" + queueName + ", jobIdentifier=" + jobIdentifier + ", calculationId=" + calculationId + ", engineDataKey="
        + engineDataKey + "]";
  }
}
