/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.domain.ModelProgressReport;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Handler to process results. Implementations can store results in the database.
 */
public interface CalculationResultHandler {

  /**
   * Called with the results for all sectors.
   *
   * @param results all calculation results per sector
   * @param calculationId calculation id
   * @param engineDataKey engine data key identifier
   * @throws AeriusException
   */
  default void onSectorResults(final Map<Integer, List<AeriusResultPoint>> results, final int calculationId,
      final EngineDataKey engineDataKey) throws AeriusException {
    for (final Entry<Integer, List<AeriusResultPoint>> entry : results.entrySet()) {
      onSectorResults(entry.getValue(), calculationId, engineDataKey, entry.getKey());
    }
  }

  /**
   * Called with the results for a specific sector.
   *
   * @param results calculation results for a specific sector
   * @param calculationId calculation id
   * @param engineDataKey engine data key identifier
   * @param sectorId id of the sector the results are for
   * @throws AeriusException
   */
  default void onSectorResults(final List<AeriusResultPoint> results, final int calculationId, final EngineDataKey engineDataKey,
      final int sectorId) throws AeriusException {
  }

  /**
   * Called with results where results for all sectors processes are available and summarized.
   * @param result
   * @param calculationId calculation id
   * @throws AeriusException
   */
  default void onTotalResults(final List<AeriusResultPoint> result, final int calculationId) throws AeriusException {
  }

  /**
   * Called when model export results are returned.
   * @param fileCode file code of the file on the file server
   */
  default void onExportResult(final String fileCode) throws AeriusException {
  }

  /**
   * Called when the model reports intermediate progress.
   *
   * @param progressReport progress report
   */
  default void onModelProgressReport(final ModelProgressReport progressReport) throws AeriusException {
  }
}
