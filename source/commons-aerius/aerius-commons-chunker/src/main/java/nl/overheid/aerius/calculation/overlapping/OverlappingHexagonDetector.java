/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.overlapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.index.strtree.STRtree;
import org.locationtech.jts.operation.distance.IndexedFacetDistance;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.PolygonToPointsConverter;

/**
 * A detector to find so called Overlapping Hexagons.
 *
 * An Overlapping Hexagon is a hexagon where all sources supplied are within a given distance.
 * This can be used to indicate which hexagons should have a result for each source,
 * if results are only calculated for each individual source up to a maximum distance.
 *
 * Definition for when results are expected to be calculated is based on what is used in the OPS model.
 */
class OverlappingHexagonDetector {

  private static final GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();

  private final List<AeriusPoint> allReceptors;
  private final Map<Geometry, AeriusPoint> receptorGeometryMap;
  private final STRtree receptorSTRtree = new STRtree();
  private final PolygonToPointsConverter polygonConverter;

  public OverlappingHexagonDetector(final Collection<AeriusPoint> receptors, final double polygonToPointsGridSize) {
    allReceptors = new ArrayList<>(receptors);
    receptorGeometryMap = toReceptorGeometryMap(receptors);
    for (final Geometry geo : receptorGeometryMap.keySet()) {
      receptorSTRtree.insert(geo.getEnvelopeInternal(), geo);
    }
    receptorSTRtree.build();
    polygonConverter = new PolygonToPointsConverter(polygonToPointsGridSize);
  }

  public List<AeriusPoint> detect(final Collection<EmissionSourceFeature> sources, final int distance) throws AeriusException {
    final List<Point> emissionPoints = new ArrayList<>();
    for (final EmissionSourceFeature source : sources) {
      emissionPoints.addAll(convertToEmissionPoints(source.getId(), source.getGeometry()).toList());
    }
    if (emissionPoints.isEmpty()) {
      // if there are no emission points, all receptors are overlapping
      return new ArrayList<>(allReceptors);
    }
    Set<Geometry> pointsWithinRange = getInitialReceptorsWithinRange(emissionPoints, distance);
    for (final Point point : emissionPoints) {
      pointsWithinRange = detectReceptorsWithinRange(point, pointsWithinRange, distance);
    }
    return pointsWithinRange.stream().map(receptorGeometryMap::get).toList();
  }

  private Set<Geometry> getInitialReceptorsWithinRange(final Collection<Point> emissionPoints, final int distance) {
    final Set<Geometry> pointsWithinRange = new HashSet<>();
    final Geometry sourceGeometry = emissionPoints.iterator().next();
    final Envelope sourceEnvelope = sourceGeometry.getEnvelopeInternal();
    // Returned envelope should be a copy already (based on javadoc), so safe to expand.
    sourceEnvelope.expandBy(distance);
    receptorSTRtree.query(sourceEnvelope, item -> pointsWithinRange.add((Geometry) item));
    return pointsWithinRange;
  }

  private static Map<Geometry, AeriusPoint> toReceptorGeometryMap(final Collection<AeriusPoint> receptors) {
    final Map<Geometry, AeriusPoint> receptorGeometryMap = new HashMap<>();
    for (final AeriusPoint receptor : receptors) {
      receptorGeometryMap.put(createRoundedPoint(receptor), receptor);
    }
    return receptorGeometryMap;
  }

  private static Set<Geometry> detectReceptorsWithinRange(final Point emissionPoint, final Set<Geometry> points, final int distance) {
    if (points.isEmpty()) {
      return Set.of();
    }
    final Set<Geometry> pointsWithinRange = new HashSet<>();
    final IndexedFacetDistance indexedFacetDistance = new IndexedFacetDistance(emissionPoint);
    for (final Geometry point : points) {
      // isWithinDistance works inclusively: if the point is exactly at the distance, it's considered within.
      // Same definition as OPS (which skips if 'disx .GT. DISTMAX'), so good on that department.
      if (indexedFacetDistance.isWithinDistance(point, distance)) {
        pointsWithinRange.add(point);
      }
    }
    return pointsWithinRange;
  }

  private Stream<Point> convertToEmissionPoints(final String id, final nl.overheid.aerius.shared.domain.v2.geojson.Geometry geometry)
      throws AeriusException {
    if (geometry instanceof final nl.overheid.aerius.shared.domain.v2.geojson.Point point) {
      return convertPoint(point);
    }

    if (geometry instanceof nl.overheid.aerius.shared.domain.v2.geojson.LineString) {
      return Stream.of(); // ignore line sources
    }

    if (geometry instanceof final Polygon polygon) {
      return polygonConverter.convert(id, polygon).stream().map(OverlappingHexagonDetector::createRoundedPoint);
    }
    return Stream.of();
  }

  private static Stream<Point> convertPoint(final nl.overheid.aerius.shared.domain.v2.geojson.Point geometry) {
    return Stream.of(createRoundedPoint(geometry));
  }

  private static Point createRoundedPoint(final nl.overheid.aerius.shared.domain.v2.geojson.Point aeriusPoint) {
    // Use rounded coordinates:
    // This detector is used to determine points that overlap according to OPS
    // OPS is supplied with integer-based coordinates for both receptors and sources
    // (for receptors this is done using the methods on our Point object,
    // for sources, this is done by a format of "%8.0f %8.0f", which is effectively the same).

    // As long as this detector is only used in combination with OPS and RDnew, this works OK.
    return GEOMETRY_FACTORY.createPoint(new Coordinate(aeriusPoint.getRoundedX(), aeriusPoint.getRoundedY()));
  }

}
