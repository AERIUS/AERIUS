/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.sql.SQLException;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Stream;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.conversion.JobPackets.SourceConverterSupplier;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.calculation.natura2k.AbstractNatura2KWorkDistributor;
import nl.overheid.aerius.calculation.natura2k.EmissionSourceTreeBuilder;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.calculation.subreceptor.AdditionalPointSupplier;
import nl.overheid.aerius.calculation.subreceptor.SubReceptorPointSupplier;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.function.AeriusFunction;
import nl.overheid.aerius.ops.domain.OPSSubReceptorCreator;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.OwN2000CalculationOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.ops.OPSSubReceptor;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.theme.own2000.OwN2000SubReceptorConstants;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link WorkDistributor} implementation for calculating nature area receptors.
 * It can be used to either calculate receptors for the Wet Natuur Bescherming (OwN2000) calculation or
 * to calculate all receptors in natura areas within a specific distance or all available receptors.
 */
class OwN2000DistanceWorkDistributor extends AbstractNatura2KWorkDistributor implements CalculationResultHandler {

  /**
   * Max distance + some safety margin. The margin is not relevant for the end results as the actual distance is calculated in OPS.
   */
  private static final int OWN2000_MAX_DISTANCE_M = 25_500;

  private final OPSSubReceptorCreator subReceptorCreator;
  private final HexagonZoomLevel hexagonZoomLevel;
  private AeriusFunction<AeriusPoint, Boolean> checkPointFunction;
  private AdditionalPointSupplier additionalPointSupplier;

  public OwN2000DistanceWorkDistributor(final PMF pmf, final Natura2kReceptorStore n2kReceptorStore, final SectorCategories sectorCategories,
      final ReceptorGridSettings receptorGridSettings) {
    super(pmf, n2kReceptorStore, sectorCategories, receptorGridSettings);
    hexagonZoomLevel = receptorGridSettings.getZoomLevel1();
    subReceptorCreator = new OPSSubReceptorCreator(AeriusPointType.SUB_RECEPTOR, hexagonZoomLevel.getHexagonRadius());
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    final CalculationSetOptions options = calculationJob.getCalculationSetOptions();
    // Collect all areas that are at least 25km within the sources, other areas are ignored.
    final EmissionSourceTreeBuilder esb = calculationJob.getSourcesTreeBuilder();
    final EmissionSourceListSTRTree sourcesTree = esb.getRelevantSituationSourcesTree();

    final Stream<Entry<Integer, Integer>> stream = n2kReceptorStore.order(sourcesTree).stream();
    final Stream<Entry<Integer, Integer>> to2Collect;
    final OwN2000CalculationOptions own2000CalculationOptions = options.getOwN2000CalculationOptions();

    if (hasMaxDistance(options)) {
      final double maxDistance = own2000CalculationOptions.isUseMaxDistance() ? OWN2000_MAX_DISTANCE_M : options.getCalculateMaximumRange();
      // filter points that are beyond the max distance
      checkPointFunction = p -> sourcesTree.findShortestDistance(p) <= maxDistance;
      // filter areas that are beyond the max distance
      to2Collect = stream.filter(e -> e.getValue() <= maxDistance);
    } else {
      // If no max distance points should always be accepted.
      checkPointFunction = p -> true;
      to2Collect = stream;
    }
    init(calculationJob, to2Collect.toList());

    // For the stable version no sub receptor points are calculated.
    additionalPointSupplier = own2000CalculationOptions.getSubReceptorsMode().isDisabled()
        ? new AdditionalPointSupplier() {}
        : new SubReceptorPointSupplier<OPSSubReceptor>(pmf, subReceptorCreator, hexagonZoomLevel,
            OwN2000SubReceptorConstants.MIN_SOURCE_SUBRECEPTOR_DISTANCE, esb.getAllSourcesTree(), own2000CalculationOptions.getSubReceptorsMode());
  }

  @Override
  protected SourceConverterSupplier createSourceConverterSupplier(final CalculationJob calculationJob) {
    return (con, situation) -> new OwN2000SourceConverter(con, calculationJob.getProvider(), situation);
  }

  private static boolean hasMaxDistance(final CalculationSetOptions options) {
    return options.getOwN2000CalculationOptions().isUseMaxDistance() || (options.isMaximumRangeRelevant() && options.getCalculateMaximumRange() > 0);
  }

  @Override
  protected boolean checkPoint(final AeriusPoint aeriusPoint) throws AeriusException {
    return checkPointFunction.apply(aeriusPoint) && super.checkPoint(aeriusPoint);
  }

  @Override
  protected int addPoint(final List<AeriusPoint> pointsToCalculate, final AeriusPoint aeriusPoint) throws AeriusException {
    return additionalPointSupplier.addPoint(pointsToCalculate, aeriusPoint);
  }
}
