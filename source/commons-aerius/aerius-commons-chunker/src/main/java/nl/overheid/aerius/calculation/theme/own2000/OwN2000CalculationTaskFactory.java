/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.calculation.base.CalculationTaskFactory;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.OwN2000CalculationOptions;
import nl.overheid.aerius.shared.domain.calculation.SubReceptorsMode;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.sector.EmissionCalculationMethod;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Implementation of {@link CalculationTaskFactory} specific for OwN2000.
 */
public class OwN2000CalculationTaskFactory implements CalculationTaskFactory {
  private static final Logger LOGGER = LoggerFactory.getLogger(OwN2000CalculationTaskFactory.class);

  private final int surfaceZoomLevel1;
  private final List<Integer> roadSectors;

  public OwN2000CalculationTaskFactory(final ReceptorGridSettings rgs, final PMF pmf) throws SQLException {
    surfaceZoomLevel1 = rgs.getZoomLevel1().getSurfaceLevel1();

    // Load the sector IDs for roads from the database
    try (final Connection con = pmf.getConnection()) {
      roadSectors = SectorRepository.getEmissionCalculationMethods(con)
          .entrySet().stream().filter(e -> e.getValue() == EmissionCalculationMethod.SRM2_ROAD)
          .map(Entry::getKey)
          .toList();
    }
  }

  @Override
  public <E extends EngineSource, R extends AeriusPoint, T extends EngineInputData<E, R>> CalculationTask<E, R, T> createTask(
      final EngineDataKey engineDataKey, final CalculationJob calculationJob) throws AeriusException {
    if (engineDataKey instanceof final CalculationEngine ownKey) {
      return switch (ownKey) {
        case ASRM2 -> new CalculationTask<E, R, T>(WorkerType.ASRM, (T) createSRMInputData(calculationJob));
        case OPS -> new CalculationTask<E, R, T>(WorkerType.OPS, (T) createOPSInputData(calculationJob));
        default -> {
          LOGGER.error("Calculation engine {} not supported for OwN2000 theme.", engineDataKey);
          throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
        }
      };
    }
    LOGGER.error("Calculation engine {} not supported for OwN2000 theme.", engineDataKey);
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  private static SRMInputData<?> createSRMInputData(final CalculationJob calculationJob) {
    final SRMInputData<?> srmInputData = new SRMInputData<>(Theme.OWN2000,
        ((OwN2000CalculationJobOptions) calculationJob.getThemeCalculationJobOptions()).getSrmVersion());

    srmInputData.setSubReceptorCalculation(getSubReceptorOptions(calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions()));
    return srmInputData;
  }

  @Override
  public <E extends EngineSource, R extends AeriusPoint, T extends EngineInputData<E, R>> void setTaskOptions(final CalculationTask<E, R, T> task,
      final CalculationJob calculationJob) {
    if (task.getTaskInput() instanceof final OPSInputData opsInputTask) {
      opsInputTask.setOpsOptions(calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions().getOpsOptions());
      for (final Entry<Integer, Collection<OPSSource>> entry : opsInputTask.getEmissionSources().entrySet()) {
        if (calculationJob.getProvider().getCalculationEngine(entry.getKey()) == CalculationEngine.ASRM2) {
          opsInputTask.addMinDistanceGroupId(entry.getKey());
        }
      }
    }
  }

  private OPSInputData createOPSInputData(final CalculationJob calculationJob) {
    final OPSInputData opsInputData = new OPSInputData(
        ((OwN2000CalculationJobOptions) calculationJob.getThemeCalculationJobOptions()).getOpsVersion(), calculationJob.getCommandType(),
        surfaceZoomLevel1);

    final OwN2000CalculationOptions own2000Options = calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions();
    opsInputData.setMaxDistance(own2000Options.isUseMaxDistance());
    opsInputData.getRoadsCats().addAll(roadSectors);
    opsInputData.setSubReceptorCalculation(getSubReceptorOptions(own2000Options));
    opsInputData.setMeteo(own2000Options.getMeteo());
    if (opsInputData.getSubReceptorCalculation() == SubReceptorCalculation.SPLIT_PROVIDED) {
      opsInputData.setSplitDistance(own2000Options.getSplitSubReceptorWorkDistance());
    }

    opsInputData.setForceTerrainProperties(calculationJob.getCalculationSetOptions().getCalculationMethod() == CalculationMethod.FORMAL_ASSESSMENT);
    return opsInputData;
  }

  /**
   * Returns the SubReceptorCalculation give the parameters.
   *
   * @param own2000Options used to check is sub receptors are disabled
   * @return the sub receptor calculation
   */
  private static SubReceptorCalculation getSubReceptorOptions(final OwN2000CalculationOptions own2000Options) {
    final SubReceptorCalculation subReceptorCalculation;

    if (Optional.ofNullable(own2000Options.getSubReceptorsMode()).map(SubReceptorsMode::isDisabled).orElse(Boolean.FALSE)) {
      subReceptorCalculation = SubReceptorCalculation.DISABLED;
    } else {
      subReceptorCalculation = own2000Options.isSplitSubReceptorWork() ? SubReceptorCalculation.SPLIT_PROVIDED : SubReceptorCalculation.PROVIDED;
    }
    return subReceptorCalculation;
  }
}
