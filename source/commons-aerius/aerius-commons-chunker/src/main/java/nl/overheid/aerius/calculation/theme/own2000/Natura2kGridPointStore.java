/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import nl.overheid.aerius.calculation.grid.GridPointStore;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * Storage for all receptors points of all Natura 2000 area relevant points as established in the Wet Natuurbescherming.
 * The set is organized in grid cells.
 * @deprecated Used for aggregation of sources, which has been phased out.
 */
@Deprecated
public class Natura2kGridPointStore extends GridPointStore {

  private final Map<Integer, Collection<Integer>> n2kIds = new HashMap<>();
  private final Map<Integer, Integer> n2kSizes = new HashMap<>();
  private final Map<Integer, SridPoint> gridPoints = new HashMap<>();
  private final Comparator<AeriusPoint> comparator;

  public Natura2kGridPointStore(final ReceptorUtil receptorUtil, final GridSettings gridSettings) {
    super(gridSettings);
    comparator = new Comparator<AeriusPoint>() {
      @Override
      public int compare(final AeriusPoint o1, final AeriusPoint o2) {
        final int comp = Integer.compare(receptorUtil.getZoomLevelForReceptor(o2.getId()), receptorUtil.getZoomLevelForReceptor(o1.getId()));
        return comp == 0 ? Integer.compare(o1.getId(), o2.getId()) : comp;
      }
    };
  }

  @Override
  protected Collection<AeriusPoint> createNewEmptyStore() {
    return new HashSet<>();
  }

  /**
   * Add all points for a specific nature area and order them in grids
   * @param n2kId natura area id
   * @param points points in the nature area.
   */
  public void addAll(final Integer n2kId, final List<AeriusPoint> points) {
    final Collection<Integer> gridCells = new HashSet<>();

    n2kIds.put(n2kId, gridCells);
    n2kSizes.put(n2kId, points.size());
    for (final AeriusPoint point : points) {
      final int gridId = getGridId(point);

      if (gridCells.add(gridId) && !gridPoints.containsKey(gridId)) {
        gridPoints.put(gridId, getPositionFromCell(gridId));
      }
      add(gridId, point);
    }
  }

  /**
   * @return Returns a Set of all grid Id's with the grid point
   */
  public Set<Entry<Integer, SridPoint>> getGridPoints() {
    return gridPoints.entrySet();
  }

  /**
   *
   */
  public void sortPointStore() {
    for (final Entry<Integer, Collection<AeriusPoint>> entry : entrySet()) {
      entry.setValue(new ArrayList<>(entry.getValue()));
      Collections.sort((ArrayList<AeriusPoint>) entry.getValue(), comparator);
    }
  }

  /**
   * Get all grid id's that contain points for a given nature area.
   * @param n2kId The id of nature area.
   * @return List of grid id's
   */
  public Collection<Integer> getGridIds(final Integer n2kId) {
    return n2kIds.getOrDefault(n2kId, Collections.emptyList());
  }

  /**
   * Returns the number of points that are stored for a given nature area.
   * @param n2kId The id of nature area.
   * @return the number of points in a nature area
   */
  public int getSize(final Integer n2kId) {
    final Integer size = n2kSizes.get(n2kId);
    return size == null ? 0 : size.intValue();
  }
}
