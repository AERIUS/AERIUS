/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;

public class GridSettings {

  private static final int NUMBER_OF_ZOOMLEVELS = 6;

  /**
   * Minimal zoom level in square meters.
   */
  private static final int ZOOM_LEVEL_RIB_LENGTH_MIN = 500;

  /**
   * The surface scale by which a zoom level differs from a sibling.
   */
  private static final int SCALE_MULTIPLIER = 2;

  /**
   * Number of horizontal minimal zoomlevel squares
   *
   * This implies the zoom level has knowledge about the grid it lives in, but saves a division operation.
   */
  private static final int SQUARE_HOR = 640;

  /**
   * Zoom levels for the grid: 500, 1000, 2000, 4000, 8000, 16000
   */
  private final GridZoomLevel[] zoomLevels = new GridZoomLevel[NUMBER_OF_ZOOMLEVELS];

  private final int xMin;
  private final int yMax;
  private final int srid;

  public GridSettings(final ReceptorGridSettings rgs) {
    this(rgs, SCALE_MULTIPLIER, ZOOM_LEVEL_RIB_LENGTH_MIN, SQUARE_HOR);
  }

  GridSettings(final ReceptorGridSettings rgs, final int scaleMultiplier, final int zoomLevelRibLengthMin, final int squareHor) {
    for (int i = 0; i < zoomLevels.length; i++) {
      zoomLevels[i] = new GridZoomLevel(i + 1, scaleMultiplier, zoomLevelRibLengthMin, squareHor);
    }
    final int ribLength = getZoomLevelMax().getRibLength();
    xMin = calculateXMin(rgs, ribLength);
    yMax = calculateYMax(rgs, ribLength);
    srid = rgs.getEPSG().getSrid();
  }

  private int calculateXMin(final ReceptorGridSettings rgs, final int ribLength) {
    return ribLength * (int) (rgs.getBoundingBox().getMinX() / ribLength);
  }

  private int calculateYMax(final ReceptorGridSettings rgs, final int ribLength) {
    return ribLength * (int) Math.ceil(rgs.getBoundingBox().getMaxY() / ribLength);
  }

  public int getSrid() {
    return srid;
  }

  public int getXMin() {
    return xMin;
  }
  public int getYMax() {
    return yMax;
  }

  public int getScaleMultiplier() {
    return SCALE_MULTIPLIER;
  }

  public int getSquareHor() {
    return SQUARE_HOR;
  }

  public int getZoomLevelRibLengthMin() {
    return ZOOM_LEVEL_RIB_LENGTH_MIN;
  }

  public GridZoomLevel getZoomLevelMin() {
    return zoomLevels[0];
  }

  public GridZoomLevel getZoomLevelMax() {
    return zoomLevels[zoomLevels.length - 1];
  }

  public GridZoomLevel[] getZoomLevels() {
    return zoomLevels;
  }

  /**
   * Returns a valid GridZoomLevel for the given level number.
   *
   * @param level
   *          scale to find GridZoomLevel for
   * @return GridZoomLevel matching the level number
   */
  public GridZoomLevel levelFromLevel(final int level) {
    return zoomLevels[Math.max(1, Math.min(zoomLevels.length, level)) - 1];
  }
}
