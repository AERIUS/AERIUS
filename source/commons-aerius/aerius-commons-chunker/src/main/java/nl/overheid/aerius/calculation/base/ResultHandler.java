/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.util.List;
import java.util.Map;

import nl.overheid.aerius.calculation.domain.ModelProgressReport;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Interface that implements a handler that is called when calculation results are received. The handler should store them.
 */
public interface ResultHandler {

  /**
   * Set a {@link ResultPostProcessHandler} that is called prior to calling {@link #onResult(String, List)}. This can be used to do extra operations
   * on the results. For example this is used to adjust temporary project results.
   * @param resultUpdateHandler
   */
  void addResultUpdateHandler(ResultPostProcessHandler resultUpdateHandler);

  /**
   * Called when instead of results an error was returned or an error occurred during {@link #onResult(String, List)} call.
   * @param taskId id of the task the error occurred.
   * @param exception the exception that occurred.
   */
  void onFailure(String taskId, Exception exception);

  /**
   * Called with the retrieved calculation results.
   * @param taskId id of the task, used to identify the results back to the calling parameters
   * @param engineDataKey engine data key identifier
   * @param results the calculation results.
   */
  void onResult(String taskId, final EngineDataKey engineDataKey, Map<Integer, List<AeriusResultPoint>> results);

  /**
   * Called when model export results are returned.
   * @param taskId id of the task, used to identify the results back to the calling parameters
   * @param fileUrl external url of the file on the file server
   */
  void onExport(String taskId, String fileUrl);

  /**
   * Called when model progress reports are returned.
   *
   * @param taskId id of the task, used to identify the results back to the calling parameters
   * @param modelProgressReport the progress report
   */
  void onModelProgressReport(String taskId, ModelProgressReport modelProgressReport);

  /**
   * Returns a unique task id that is given back to the {@link #onResult(String, List)} and {@link #onFailure(String, Exception)} to match the results
   * back to the work key. This call may block until enough resources are available.
   * @param workKey key with all information to identify calculation results.
   * @return a unique task id.
   * @throws InterruptedException
   * @throws TaskCancelledException called when the task was cancelled.
   */
  String registerTask(WorkKey workKey) throws InterruptedException, TaskCancelledException;

}
