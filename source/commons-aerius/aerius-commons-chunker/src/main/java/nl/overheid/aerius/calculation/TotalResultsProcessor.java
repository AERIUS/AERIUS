/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.ScheduledWorkRunnable;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Processes the total results (i.e. a result of a single point calculated for all sectors/situations/scenarios).
 *
 * Should be called in a rescheduling thread and run until all results are completed.
 */
class TotalResultsProcessor implements ScheduledWorkRunnable {

  private static final Logger LOG = LoggerFactory.getLogger(TotalResultsProcessor.class);

  private final CalculationResultQueue queue;
  private final CalculationResultHandler handler;
  private final CalculationJob calculationJob;

  public TotalResultsProcessor(final CalculationResultQueue queue, final CalculationResultHandler handler, final CalculationJob calculationJob) {
    this.queue = queue;
    this.handler = handler;
    this.calculationJob = calculationJob;
  }

  /**
   * Gets results from the queue and distributes them to the handlers.
   *
   * @throws AeriusException
   */
  @Override
  public void run() throws AeriusException {
    synchronized (this) {
      for (final Calculation calculation : calculationJob.getCalculations()) {
        final int calculationId = calculation.getCalculationId();
        final List<AeriusResultPoint> totalResults = queue.pollTotalResults(calculationId);

        if (!totalResults.isEmpty()) {
          handler.onTotalResults(totalResults, calculationId);
        }
      }
    }
  }

  @Override
  public void onComplete() throws AeriusException {
    run();
    sanityCheckOnComplete();
  }

  @Override
  public void onCancelled() {
    queue.clear();
  }

  private void sanityCheckOnComplete() throws AeriusException {
    synchronized (this) {
      for (final Calculation calculation : calculationJob.getCalculations()) {
        final int calculationId = calculation.getCalculationId();
        if (queue.isExpectingMoreResults(calculationId)) {
          LOG.error("Queue was still expecting more results for calculation {}", calculationId);
          throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
        }
      }
    }
  }
}
