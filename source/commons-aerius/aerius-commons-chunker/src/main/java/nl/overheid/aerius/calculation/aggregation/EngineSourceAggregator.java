/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.aggregation.Aggregatable.AggregationKey;

/**
 * Utility class to aggregate OPSSource.
 */
public final class EngineSourceAggregator<T, S> {

  private final Aggregatable<T, S> aggregatable;

  public EngineSourceAggregator(final Aggregatable<T, S> aggregatable) {
    this.aggregatable = aggregatable;
  }

  /**
   * @param originals The list of sources that has to be aggregated.
   * @param keys The list keys for specifying emissions to aggragete.
   * @return The aggregated list of sources.
   */
  public Collection<T> aggregate(final Collection<T> originals, final List<S> keys) {
    final Collection<T> aggregatedCollection = new ArrayList<>();
    final Collection<T> aggregatableCollection = new ArrayList<>();

    // First, split and filter the T list
    fillSets(originals, aggregatedCollection, aggregatableCollection);

    // Iterate over each substance, and aggregate the OPS sources inside them
    aggregateSources(aggregatedCollection, aggregatableCollection, keys);

    return aggregatedCollection;
  }

  private void fillSets(final Collection<T> originals, final Collection<T> nonAggregatableCollection,
      final Collection<T> aggregatableCollection) {
    for (final T src : originals) {
      if (aggregatable.isAggregatable(src)) {
        aggregatableCollection.add(src);
      } else {
        nonAggregatableCollection.add(src);
      }
    }
  }

  private void aggregateSources(final Collection<T> aggregatedCollection, final Collection<T> aggregatableCollection,
      final List<S> substances) {
    final Map<AggregationKey, List<T>> splitSources = splitSources(aggregatableCollection);
    for (final Entry<AggregationKey, List<T>> entry : splitSources.entrySet()) {
      final List<T> sources = entry.getValue();
      if (sources.size() == 1) {
        aggregatedCollection.add(sources.get(0));
      } else {
        aggregatedCollection.addAll(aggregatable.aggregateSources(sources, substances));
      }
    }
  }

  private Map<AggregationKey, List<T>> splitSources(final Collection<T> aggregatableCollection) {
    final Map<AggregationKey, List<T>> map = new AutoFillingHashMap<AggregationKey, List<T>>() {
      private static final long serialVersionUID = 1655670090936770201L;

      @Override
      protected List<T> getEmptyObject() {
        return new ArrayList<>();
      }
    };
    for (final T src : aggregatableCollection) {
      map.get(aggregatable.aggregationKey(src)).add(src);
    }
    return map;
  }
}
