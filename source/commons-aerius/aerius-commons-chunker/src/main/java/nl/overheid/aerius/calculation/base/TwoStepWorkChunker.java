/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * {@link WorkChunker} that runs 2 steps.
 * In the first step all sources and points are collected, and the metrics about these sources and points are stored in the database.
 * In the second step the work is send to the model workers.
 */
public class TwoStepWorkChunker<P extends AeriusPoint> implements WorkChunker<P> {

  private final CalculationJob calculationJob;
  private final WorkHandler<P> workHandler;
  private final WorkDistributor<P> workDistributor;
  private final PMF pmf;

  public TwoStepWorkChunker(final PMF pmf, final CalculationJob calculationJob, final WorkDistributor<P> workDistributor,
      final WorkHandler<P> workHandler) {
    this.pmf = pmf;
    this.calculationJob = calculationJob;
    this.workDistributor = workDistributor;
    this.workHandler = workHandler;
  }

  @Override
  public void run(final WorkInitHandler<P> initHandler) throws Exception {
    workDistributor.init(calculationJob);
    final List<WorkPacket<P>> packets = collectWork();

    initHandler.init(calculationJob, packets);
    WorkerUtil.failIfJobCancelledByUser(pmf, calculationJob.getJobIdentifier().getJobKey());
    processWork(packets);
  }

  private List<WorkPacket<P>> collectWork() throws AeriusException {
    final List<WorkPacket<P>> packets = new ArrayList<>();

    while (workDistributor.prepareWork()) {
      final WorkPacket<P> work = workDistributor.work();
      if (work == null) {
        continue; // hasWork lied!! it had no work...try again.
      }
      packets.add(work);
    }
    return packets;
  }

  private void processWork(final List<WorkPacket<P>> packets) throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    for (final WorkPacket<P> work : packets) {
      for (final JobPacket packet : work.getJobPackets()) {
        workHandler.work(calculationJob, packet, work.getPoints());
      }
    }
  }

  @Override
  public void onComplete() throws AeriusException {
    workDistributor.onCompleted();
  }

}
