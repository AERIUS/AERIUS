/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public final class AggregationDistanceProfilePicker implements AggregationProfilePicker<AggregationDistanceProfile> {
  // Stallen
  private static final Set<Integer> STABLE_SECTORS = new HashSet<>(Arrays.asList(new Integer[] { 4110, 4120 }));
  // Industrie laag
  private static final Set<Integer> INDUSTRY_SECTORS = new HashSet<>(
      Arrays.asList(new Integer[] { 1050, 1100, 1300, 1400, 1500, 1700, 1800, 2100, 4320, 4600, 7510, 7520, 7530, 8200, 8210, 8640, 9000, 9999 }));
  // Road
  private static final Set<Integer> ROAD_SECTORS = new HashSet<>(
      Arrays.asList(new Integer[] { 3100, 3111, 3112, 3113, 3210, 3220, 3230, 3640, 3710, 3720, 4130, 4140, 7610, 7620 }));

  // Some others (stijgen, landen, taxien)
  private static final Set<Integer> SOME_OTHER_SECTORS = new HashSet<>(Arrays.asList(new Integer[] { 3610, 3620, 3630 }));

  // Sectors that we will not aggregate by default.
  private static final Set<Integer> AGGREGATION_DISABLED_BY_DEFAULT = new HashSet<>(STABLE_SECTORS);

  @Override
  public AggregationDistanceProfile getAggregationProfile(final int sectorId, final boolean forceAggregation) {
    final AggregationDistanceProfile profile;

    if (!forceAggregation && AGGREGATION_DISABLED_BY_DEFAULT.contains(sectorId)) {
      profile = AggregationDistanceProfile.DISABLED;
    } else if (STABLE_SECTORS.contains(sectorId)) {
      profile = AggregationDistanceProfile.STABLES;
    } else if (INDUSTRY_SECTORS.contains(sectorId)) {
      profile = AggregationDistanceProfile.INDUSTRY;
    } else if (ROAD_SECTORS.contains(sectorId)) {
      profile = AggregationDistanceProfile.ROAD;
    } else if (SOME_OTHER_SECTORS.contains(sectorId)) {
      profile = AggregationDistanceProfile.SOME_OTHER;
    } else {
      profile = AggregationDistanceProfile.ALL_OTHER;
    }

    return profile;
  }
}
