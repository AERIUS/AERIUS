/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import nl.overheid.aerius.calculation.domain.GroupedSourcesKey;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.calculation.grid.GridZoomLevel;
import nl.overheid.aerius.shared.domain.EngineSource;

/**
 * Stores sources original and aggregated in a grid structure. The grid is laid out on the geographical map.
 * Sources are aggregated within the grid cells. Per zoom level the grid cells have a different size.
 */
class SourcesStoreImpl implements SourcesStore {
  private final Map<AggregationDistanceProfile, Map<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>>> original;
  private final Map<AggregationDistanceProfile, Map<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>>> aggregated;
  private final GridUtil gridUtil;

  private final Map<GridZoomLevel, Set<Integer>> gridIds;

  public SourcesStoreImpl(final GridUtil gridUtil,
      final Map<AggregationDistanceProfile, Map<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>>> original,
      final Map<AggregationDistanceProfile, Map<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>>> aggregated) {
    this.original = original;
    this.aggregated = aggregated;
    this.gridUtil = gridUtil;

    gridIds = new AutoFillingHashMap<GridZoomLevel, Set<Integer>>() {
      private static final long serialVersionUID = -5980534791201119318L;

      @Override
      protected Set<Integer> getEmptyObject() {
        return new HashSet<>();
      }
    };

    for (final Entry<AggregationDistanceProfile, Map<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>>> entry : original.entrySet()) {
      final Map<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>> mapThing = entry.getValue();
      for (final GridZoomLevel level : gridUtil.getGridSettings().getZoomLevels()) {
        for (final LevelStore<GridZoomLevel, EngineSource> values : mapThing.values()) {
          gridIds.get(level).addAll(values.getLevel(level).keySet());
        }
      }
    }
  }

  @Override
  public Map<GroupedSourcesKey, Collection<EngineSource>> getSourcesFor(final int cell) {
    final Map<GroupedSourcesKey, Collection<EngineSource>> sectorSources = new HashMap<>();

    for (final AggregationDistanceProfile profile : AggregationDistanceProfile.values()) {
      // Iterate over source types / distances
      final LevelBranch<GridZoomLevel, CellSelection> surroundingCells = getSurroundingCells(gridUtil.getGridSettings().getZoomLevelMax(),
          profile.getDistances(), cell);

      // Get the source map for the given aggregation profile
      final Map<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>> originalMap = original.get(profile);
      final Map<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>> aggregatedMap = aggregated.get(profile);

      // For each sector, find sources
      for (final Entry<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>> entry : originalMap.entrySet()) {
        final GroupedSourcesKey groupKey = entry.getKey();

        // Set to store sources in
        final Collection<EngineSource> sources = new ArrayList<>();

        // Recursively add original or aggregated sources
        final LevelStore<GridZoomLevel, EngineSource> aggregatedSectorStore = aggregatedMap.get(groupKey);
        addSources(sources, profile, entry.getValue(), aggregatedSectorStore, surroundingCells);

        // Fetch foreign top-level aggregated sources
        final HashMap<Integer, Collection<EngineSource>> aggregatedSources = new HashMap<>(
            aggregatedSectorStore.getLevel(gridUtil.getGridSettings().getZoomLevelMax()));

        // Remove non-aggregated cells
        for (final LevelBranch<GridZoomLevel, CellSelection> leaf : surroundingCells) {
          aggregatedSources.keySet().remove(leaf.getElement().getCell());
        }

        // Add them to the list to calculate
        for (final Collection<EngineSource> srcs : aggregatedSources.values()) {
          sources.addAll(srcs);
        }

        // Stick sources in the list
        sectorSources.put(groupKey, sources);
      }
    }

    return sectorSources;
  }

  protected LevelBranch<GridZoomLevel, CellSelection> getSurroundingCells(final GridZoomLevel topLevel, final Map<Integer, Integer> distances,
      final int middle) {
    final LevelBranch<GridZoomLevel, CellSelection> root = new LevelTree<>();

    final Collection<Integer> surrounding = gridUtil.getSurrounding(middle, distances.get(topLevel.getLevel()), topLevel);

    for (final Integer cell : surrounding) {
      root.add(fillBranch(cell, middle, distances, topLevel, topLevel));
    }

    return root;
  }

  private void addSources(final Collection<EngineSource> targetSources, final AggregationDistanceProfile profile,
      final LevelStore<GridZoomLevel, EngineSource> originalStore, final LevelStore<GridZoomLevel, EngineSource> aggregatedStore,
      final LevelBranch<GridZoomLevel, CellSelection> branch) {
    for (final LevelBranch<GridZoomLevel, CellSelection> leaf : branch) {
      if (leaf.isLeaf()) {
        final CellSelection element = leaf.getElement();
        if (element.isOriginal()) {
          targetSources.addAll(originalStore.getLevel(leaf.getLevel()).get(leaf.getElement().getCell()));
        } else {
          targetSources.addAll(aggregatedStore.getLevel(leaf.getLevel()).get(leaf.getElement().getCell()));
        }
      } else {
        addSources(targetSources, profile, originalStore, aggregatedStore, leaf);
      }
    }
  }

  private LevelBranch<GridZoomLevel, CellSelection> fillBranch(final int target, final int center, final Map<Integer, Integer> distances,
      final GridZoomLevel targetLevel, final GridZoomLevel centerLevel) {
    final Integer minDistance = distances.get(gridUtil.getGridSettings().getZoomLevelMin().getLevel());
    final Integer distance = distances.get(targetLevel.getLevel());

    final boolean original = gridUtil.getMinDistance(target, center, targetLevel, centerLevel) < minDistance;

    final CellSelection selection = new CellSelection(target, original);

    final LevelBranch<GridZoomLevel, CellSelection> branch = new LevelBranch<>(targetLevel, selection);

    if (gridUtil.isFullyOutside(target, center, distance, targetLevel, centerLevel)
        || targetLevel.equals(gridUtil.getGridSettings().getZoomLevelMin())) {
      return branch;
    }

    final GridZoomLevel lowerLevel = gridUtil.getGridSettings().levelFromLevel(targetLevel.getLevel() - 1);
    final Collection<Integer> cells = gridUtil.getInnerCells(target, lowerLevel);

    for (final int cell : cells) {
      branch.add(fillBranch(cell, center, distances, lowerLevel, centerLevel));
    }

    // Sanity check that should be removed when cell selection is optimised -- consolidate branches that are of one type to top level ones
    boolean allOriginal = true;
    for (final LevelBranch<GridZoomLevel, CellSelection> sub : branch) {
      allOriginal &= sub.getElement().isOriginal();
    }

    if (allOriginal) {
      // branch.clear();
      // branch.getElement().setOriginal(true);
    }

    return branch;
  }

  @Override
  public Iterable<Integer> getSourceGridIds(final GridZoomLevel zoomLevel) {
    return gridIds.get(zoomLevel);
  }
}
