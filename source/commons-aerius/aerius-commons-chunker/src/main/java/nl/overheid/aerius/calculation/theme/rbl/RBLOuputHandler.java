/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.io.CIMLKResultWriter;
import nl.overheid.aerius.util.FilenameUtil;

/**
 * Writes RBL calculation results to a csv file.
 */
public class RBLOuputHandler implements CalculationResultHandler, PostCalculationHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(RBLOuputHandler.class);
  private static final String FILENAME_PREFIX_FORMAT = "%s_results";
  private static final int SCALE = 10;

  private final HashMap<Integer, ReentrantLock> locks = new HashMap<>();
  private final CIMLKResultWriter resultWriter;
  private final String directory;

  private final Map<Integer, Integer> calculationYears;
  private final String aeriusVersion;
  private final String databaseVersion;
  private final String scenarioName;
  private final Set<Integer> calculations = new HashSet<>();
  private final Date exportDate;

  public RBLOuputHandler(final File folderForJob, final Map<Integer, Integer> calculationYears, final String aeriusVersion,
      final String databaseVersion, final String scenarioName, final Date exportDate) {
    this.directory = folderForJob.getAbsolutePath();
    this.calculationYears = calculationYears;
    this.aeriusVersion = aeriusVersion;
    this.databaseVersion = databaseVersion;
    this.scenarioName = scenarioName;
    this.resultWriter = new CIMLKResultWriter(SCALE);
    this.exportDate = exportDate;
  }

  @Override
  public void onTotalResults(final List<AeriusResultPoint> results, final int calculationId) throws AeriusException {
    final ReentrantLock lock = getLock(calculationId);
    lock.lock();

    try (final Writer writer = getWriter(calculationId)) {
      for (final AeriusResultPoint result : results) {
        final int calculationYear = calculationYears.get(calculationId);
        resultWriter.writeRow(writer, result, calculationYear, aeriusVersion, databaseVersion);
      }
      calculations.add(calculationId);
    } catch (final IOException e) {
      LOGGER.error("Error writing rbl results to file", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void onCompleted(final CalculationJob calculationJob) throws AeriusException {
    for (final int calculationId : calculations) {
      final ReentrantLock lock = getLock(calculationId);
      lock.lock();

      try {
        sortFile(calculationId);
      } catch (final IOException e) {
        LOGGER.error("Error sorting nls results to file", e);
      } finally {
        lock.unlock();
      }
    }
  }

  private Writer getWriter(final int calculationId) throws IOException {
    final Path openFile = targetFile(calculationId);
    final boolean newFile = !openFile.toFile().exists();
    final Writer writer = new PrintWriter(
        Files.newBufferedWriter(openFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND));

    if (newFile) {
      resultWriter.writeHeader(writer);
    }
    return writer;
  }

  private synchronized ReentrantLock getLock(final int calculationId) {
    if (!locks.containsKey(calculationId)) {
      locks.put(calculationId, new ReentrantLock());
    }

    return locks.get(calculationId);
  }

  private List<String> sortFile(final int calculationId) throws IOException {
    final Path fileToSort = targetFile(calculationId);
    final List<String> resultLines = Files.readAllLines(fileToSort);
    //Remove the first line, as we don't want to sort the header along with it.
    final String header = resultLines.remove(0);
    Collections.sort(resultLines);
    try (final PrintWriter writer = new PrintWriter(
        Files.newBufferedWriter(fileToSort, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.WRITE,
            StandardOpenOption.TRUNCATE_EXISTING))) {
      writer.println(header);
      for (final String line : resultLines) {
        writer.println(line);
      }
    }
    return resultLines;
  }

  private Path targetFile(final int calculationId) {
    final String prefix = String.format(FILENAME_PREFIX_FORMAT, calculationId);
    final String filename = FilenameUtil.getFilename(prefix, FileFormat.CSV.getExtension(), exportDate, scenarioName);
    return Paths.get(directory, filename);
  }

}
