/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.calculation.CalculationRoadOPS;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.ConnectSuppliedOptions;
import nl.overheid.aerius.shared.domain.calculation.OwN2000CalculationOptions;
import nl.overheid.aerius.util.ScenarioObjectMapperUtil;

/**
 * Collector to store system meta data about versions in a metadata.json file.
 */
final class MetaDataUtil {

  private static final ObjectMapper OBJECT_MAPPER = ScenarioObjectMapperUtil.getScenarioObjectMapper();
  private static final String FILENAME_FORMAT = "%s_resulttype-metadata.json";

  private MetaDataUtil() {
    // Util class
  }

  public static void writeMetadata(final PMF pmf, final CalculationJob calculationJob, final String directory, final String name,
      final String duration) throws IOException, SQLException {
    final Path openFile = Paths.get(directory, String.format(FILENAME_FORMAT, name));
    try (final PrintWriter writer = new PrintWriter(
        Files.newBufferedWriter(openFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE))) {
      OBJECT_MAPPER.writeValue(writer, setMetaData(pmf, calculationJob, duration));
    }
  }

  private static Map<String, String> setMetaData(final PMF pmf, final CalculationJob calculationJob, final String duration) throws SQLException {
    final Map<String, String> metadataMap = new LinkedHashMap<>();
    final CalculationSetOptions cso = calculationJob.getCalculationSetOptions();
    final OwN2000CalculationOptions own2000Options = cso.getOwN2000CalculationOptions();

    metadataMap.put("aerius_version", AeriusVersion.getVersionNumber());
    metadataMap.put("database_version", pmf.getDatabaseVersion());
    Optional.ofNullable(calculationJob.getCalculationSetOptions()).map(CalculationSetOptions::getOwN2000CalculationOptions)
        .map(OwN2000CalculationOptions::getOpsVersion).ifPresent(opsVersion -> metadataMap.put("ops_version", opsVersion));
    Optional.ofNullable(calculationJob.getCalculationSetOptions()).map(CalculationSetOptions::getOwN2000CalculationOptions)
        .map(OwN2000CalculationOptions::getSrmVersion).ifPresent(srmVersion -> metadataMap.put("srm_version", srmVersion));
    metadataMap.put("api_meteo_year", own2000Options.getMeteo() == null ? "" : own2000Options.getMeteo().toString());
    metadataMap.put("source_stacking", onOff(cso.isStacking()));
    metadataMap.put("duration_seconds", duration);
    metadataMap.put("receptor_count", String.valueOf(calculationJob.getScenarioCalculations().getCalculationPoints().size()));
    metadataMap.put("max_distance", onOff(own2000Options.isUseMaxDistance()));
    metadataMap.put("sub_receptors", own2000Options.getSubReceptorsMode().name());
    if (own2000Options.getSubReceptorZoomLevel() != null) {
      metadataMap.put("sub_receptors_zoom_level", String.valueOf(own2000Options.getSubReceptorZoomLevel()));
    }
    if (own2000Options.getRoadOPS() != CalculationRoadOPS.DEFAULT) {
      metadataMap.put("ops_road", own2000Options.getRoadOPS().name());
    }
    metadataMap.put("forced_aggregation", onOff(own2000Options.isForceAggregation()));
    metadataMap.put("use_receptor_height", String.valueOf(own2000Options.isUseReceptorHeights()));
    addConnectSuppliedOptions(calculationJob.getCalculationSetOptions().getConnectSuppliedOptions(), metadataMap);
    metadataMap.put("split_subreceptors", onOff(own2000Options.isSplitSubReceptorWork()));
    if (own2000Options.isSplitSubReceptorWork()) {
      metadataMap.put("split_subreceptors_distance", String.valueOf(own2000Options.getSplitSubReceptorWorkDistance()));
    }

    return metadataMap;
  }

  private static String onOff(final boolean on) {
    return on ? "on" : "off";
  }

  private static void addConnectSuppliedOptions(final ConnectSuppliedOptions connectSuppliedOptions, final Map<String, String> metadataMap) {
    if (connectSuppliedOptions != null) {
      metadataMap.put("calculation_year", String.valueOf(connectSuppliedOptions.getCalculationYear()));
      metadataMap.put("receptor_set", connectSuppliedOptions.getReceptorSetName());
    }
  }

}
