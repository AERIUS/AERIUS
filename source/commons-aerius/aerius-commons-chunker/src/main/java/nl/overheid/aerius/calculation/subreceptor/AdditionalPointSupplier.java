/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.subreceptor;

import java.util.List;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface to that is used to optionally add additional points to be calculated. For example this is used to add sub receptor points to the
 * set of points to calculate.
 */
public interface AdditionalPointSupplier {

  /**
   * Add the point(s) to calculate
   * If sub receptor points need to be calculated it adds the sub receptor points here.
   * If a sub receptor these points are added as {@link IsSubPoint} points so it's easy to track which points need special treatment.
   */
  default int addPoint(final List<AeriusPoint> pointsToCalculate, final AeriusPoint aeriusPoint) throws AeriusException {
    pointsToCalculate.add(aeriusPoint);
    return 1;
  }

}
