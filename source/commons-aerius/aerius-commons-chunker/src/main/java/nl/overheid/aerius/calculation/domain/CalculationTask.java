/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Base class with data for a calculation task. Calculation workers should implement this method to create the input data used by the worker.
 */
public class CalculationTask<E extends EngineSource, R extends AeriusPoint, T extends EngineInputData<E, R>> implements Serializable {

  private static final long serialVersionUID = 3L;

  private final WorkerType workerType;
  private final T taskInput;

  /**
   * Initializes object with type of worker.
   */
  public CalculationTask(final WorkerType workerType, final T taskInput) {
    this.workerType = workerType;
    this.taskInput = taskInput;
  }

  public T getTaskInput() {
    return taskInput;
  }

  public WorkerType getWorkerType() {
    return workerType;
  }

  /**
   * Set options in the taskInput
   *
   * @param substances substances
   * @param emissionResultKeys The result keys for which results should be returned.
   * @param year year to calculate
   * @param expire the tag to indicate how long the import file should be stored
   */
  public void setOptions(final List<Substance> substances, final Set<EmissionResultKey> emissionResultKeys, final int year,
      final FileServerExpireTag expire) {
    taskInput.setSubstances(substances);
    taskInput.setEmissionResultKeys(EnumSet.copyOf(emissionResultKeys));
    taskInput.setYear(year);
    taskInput.setExpire(expire);
  }

  /**
   * Sets the source objects.
   * @param sourcesKey the key to identify these grouped sources
   * @param sources the sources to set
   */
  public void setSources(final Integer sourcesKey, final Collection<E> sources) {
    taskInput.setEmissionSources(sourcesKey, sources);
  }
}
