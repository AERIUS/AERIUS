/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.sql.SQLException;
import java.util.List;

import nl.overheid.aerius.calculation.base.AeriusPointsWorkDistributor;
import nl.overheid.aerius.calculation.conversion.JobPackets.SourceConverterSupplier;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geo.EPSG;
import nl.overheid.aerius.util.FeaturePointToEnginePointUtil;

/**
 * Distributor for NCA custom points calculations.
 */
class NCAPointsWorkDistributor extends AeriusPointsWorkDistributor {

  private final NCABackgroundSupplier backgroundSupplier;
  private List<AeriusPoint> calculationPoints;
  private List<Integer> years = List.of();
  private NCAFractionNO2Supplier fractionNO2Supplier;
  private List<String> meteoYears = List.of();

  public NCAPointsWorkDistributor(final PMF pmf, final List<CalculationPointFeature> points, final SectorCategories sectorCategories,
      final NCABackgroundSupplier ncaBackgroundSupplier) {
    super(pmf, sectorCategories);
    this.calculationPoints = FeaturePointToEnginePointUtil.convert(points, NCAPointsWorkDistributor::toNCAPoint);
    backgroundSupplier = ncaBackgroundSupplier;
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    super.init(calculationJob);
    meteoYears = calculationJob.getCalculationSetOptions().getNcaCalculationOptions().getAdmsOptions().getMetYears();
    final NCACalculationOptions ncaOptions = calculationJob.getCalculationSetOptions().getNcaCalculationOptions();
    fractionNO2Supplier = new NCAFractionNO2Supplier(ncaOptions.getRoadLocalFractionNO2PointsOption(), ncaOptions.getRoadLocalFractionNO2());
  }

  @Override
  public WorkPacket<AeriusPoint> work() {
    return ADMSWorkExpander.expand(meteoYears, super.work());
  }

  @Override
  protected SourceConverterSupplier createSourceConverterSupplier(final CalculationJob calculationJob) {
    years = calculationJob.getScenarioCalculations().getCalculationYears().values().stream().distinct().toList();
    return (con, situation) -> new ADMSSourceConverter(EPSG.BNG.getSrid(), situation, sectorCategories);
  }

  @Override
  protected boolean hasMorePoints() {
    return !calculationPoints.isEmpty();
  }

  @Override
  protected List<AeriusPoint> collectPoints() {
    // Pass all points at once. The super class will take care of splitting the points if necessary
    final List<AeriusPoint> allPoints = calculationPoints;

    calculationPoints = List.of();
    return allPoints;
  }

  @Override
  protected int addPoints(final List<AeriusPoint> pointsToCalculate, final AeriusPoint pointToAdd) throws AeriusException {
    backgroundSupplier.setBackgroundNOx(List.of(pointToAdd), years);
    fractionNO2Supplier.setFractionNO2(pointToAdd);
    pointsToCalculate.add(pointToAdd);
    return 1;
  }

  private static AeriusPoint toNCAPoint(final CalculationPoint point) {
    if (point instanceof final CustomCalculationPoint ccp) {
      return FeaturePointToEnginePointUtil.toNCAPoint(ccp);
    } else {
      return new ADMSCalculationPoint();
    }
  }
}
