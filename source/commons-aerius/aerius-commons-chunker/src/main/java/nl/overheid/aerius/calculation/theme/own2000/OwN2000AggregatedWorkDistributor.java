/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.calculation.grid.GridZoomLevel;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.own2000.OwN2000ChunkerRepository;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link WorkDistributor} implementation for the Omgevingswet N2000 (OwN2000) calculation.
 * Given the sources it collects the receptors in Natura2000 areas needed to be calculated.
 * This process runs until based on the results it is determined all relevant receptors have been calculated.
 *
 * This distributor is for aggregated sources.
 */
class OwN2000AggregatedWorkDistributor implements WorkDistributor<AeriusPoint> {

  private static final int SOURCES_ZOOM_LEVEL = 3;

  private final PMF pmf;
  private final GridUtil gridUtil;
  private final GridZoomLevel zoomLevelSources;
  private final OwN2000ChunkerRepository rr;
  private final Natura2kGridPointStore pointStore;
  private final JobPacketAggregator aggregator;

  private Iterator<Integer> listGridIdsIterator;
  private ChunkState chunkState;

  /**
   * Constructor
   *
   * @param pmf
   * @param sectorCategories
   * @param gridUtil
   * @param pointStore
   */
  public OwN2000AggregatedWorkDistributor(final PMF pmf, final SectorCategories sectorCategories, final GridUtil gridUtil,
      final Natura2kGridPointStore pointStore) {
    this.pmf = pmf;
    this.gridUtil = gridUtil;
    this.zoomLevelSources = gridUtil.getGridSettings().levelFromLevel(SOURCES_ZOOM_LEVEL);
    this.pointStore = pointStore;
    rr = getNBWetRepository();
    aggregator = new JobPacketAggregator(pmf, sectorCategories, gridUtil);
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    aggregator.init(calculationJob);
    final List<Entry<Integer, Integer>> listn2k = sortedAssessmentAreaByDistance(calculationJob);
    chunkState = new ChunkState(pmf, calculationJob, listn2k, rr);
    initializeChunkStateWithDistances(listn2k, chunkState);
    listGridIdsIterator = initGridIds(listn2k);
  }

  private void initializeChunkStateWithDistances(final List<Entry<Integer, Integer>> listn2k, final ChunkState chunkState) {
    for (final Entry<Integer, Integer> entry : listn2k) {
      final Integer n2kId = entry.getKey();
      final int size = pointStore.getSize(n2kId);

      if (size > 0) {
        chunkState.addN2KArea(n2kId, size, entry.getValue());
      }
    }
  }

  /**
   * Creates the list of all receptor grid ids ordered by the distances to the nature areas.
   *
   * @param listn2k list of nature area id/distance key/value pair.
   * @return Iterator to the list of grid idsrelevantThresholdValue
   */
  private Iterator<Integer> initGridIds(final List<Entry<Integer, Integer>> listn2k) {
    final Set<Integer> calculatedGridIds = new HashSet<>();
    final List<Integer> gridIds = new ArrayList<>();

    for (final Entry<Integer, Integer> entry : listn2k) {
      for (final Integer n2kGridId : pointStore.getGridIds(entry.getKey())) {
        if (calculatedGridIds.add(n2kGridId)) {
          gridIds.add(n2kGridId);
        }
      }
    }
    return gridIds.iterator();
  }

  @Override
  public boolean prepareWork() throws AeriusException {
    return listGridIdsIterator.hasNext() && chunkState.resultsPending();
  }

  @Override
  public WorkPacket<AeriusPoint> work() {
    final Integer n2kGridId = listGridIdsIterator.next();

    return new WorkPacket<>(pointStore.getGridCell(n2kGridId), aggregator.getSourcesFor(n2kGridId));
  }

  /**
   * Returns the wrapper around the repository objects. Override this method in tests to stub the database.
   *
   * @return wrapper object
   */
  protected OwN2000ChunkerRepository getNBWetRepository() {
    return new OwN2000ChunkerRepository();
  }

  /**
   *
   * @param calculationJob
   * @return
   * @throws SQLException
   */
  private List<Entry<Integer, Integer>> sortedAssessmentAreaByDistance(final CalculationJob calculationJob) throws SQLException {
    final Collection<SridPoint> points = new HashSet<>();
    for (final Calculation job : calculationJob.getCalculations()) {
      final Iterable<Integer> sourceGridIds = aggregator.getSourceGridIds(job.getCalculationId(), zoomLevelSources);

      for (final Integer gridId : sourceGridIds) {
        points.add(gridUtil.getPositionFromCell(gridId, zoomLevelSources));
      }
    }
    try (Connection con = pmf.getConnection()) {
      return sortAssessmentAreaByDistance(con, points);
    }
  }

  /**
   * Sorts the assessment areas by distance to the sources.
   *
   * @param con database connection
   * @param sourceGridPoints
   * @return Entry tuple of Id's and distances in sorted order by distance of assessment areas. Sorted order by increasing distance.
   * @throws SQLException
   */
  private <P extends SridPoint> List<Entry<Integer, Integer>> sortAssessmentAreaByDistance(final Connection con, final Collection<P> sourceGridPoints)
      throws SQLException {
    final Map<Integer, Integer> map = new HashMap<>();
    for (final SridPoint point : sourceGridPoints) {
      for (final Entry<Integer, Integer> pointEntry : rr.getDistanceSetForPoint(con, point).entrySet()) {
        final Integer currentDistance = map.get(pointEntry.getKey());

        if (currentDistance == null || currentDistance > pointEntry.getValue()) {
          map.put(pointEntry.getKey(), pointEntry.getValue());
        }
      }
    }
    final List<Entry<Integer, Integer>> list = map.entrySet().stream().collect(Collectors.toList());

    Collections.sort(list, (o1, o2) -> Integer.compare(o1.getValue(), o2.getValue()));
    return list;
  }
}
