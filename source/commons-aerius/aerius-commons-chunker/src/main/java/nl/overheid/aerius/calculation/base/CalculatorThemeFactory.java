/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.ThemeCalculationJobOptions;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface to handle separate logic needed for calculation for different themes.
 */
public interface CalculatorThemeFactory {

  /**
   * Returns the Calculator settings based on the input to be calculated.
   *
   * @param con database connection
   * @param inputData data to determine calculator options from
   * @return calculator options
   * @throws SQLException
   */
  CalculatorOptions getCalculatorOptions(final Connection con, final CalculationInputData inputData) throws SQLException;

  /**
   * Called on initialization of a calculation and can be used to force certain options.
   *
   * @param scenarioCalculations scenario to being calculated
   * @throws AeriusException
   */
  void prepareOptions(ScenarioCalculations scenarioCalculations) throws AeriusException;

  /**
   * Returns theme specific calculation job options.
   *
   * @param inputData
   * @param calculationJob
   * @return theme specific calculation job options.
   */
  ThemeCalculationJobOptions getThemeCalculationJobOptions(final CalculationInputData inputData, CalculationJob calculationJob);

  /**
   * Returns the {@link CalculationEngineProvider} related to this theme.
   */
  CalculationEngineProvider getProvider(CalculationJob calculationJob);

  /**
   * Optional wrap the {@link CalculationTaskHandler} to allow additional work before sending task to the worker.
   * The returned method should call the passed {@link CalculationTaskHandler} at some point to send the task to the worker.
   *
   * @param calculationTaskHandler the CalculationTaskHandler to wrap.
   * @return a wrapped CalculationTaskHandler or the CalculationTaskHandler itself
   */
  default CalculationTaskHandler wrapCalculationTaskHandler(final CalculationJob calculationJob,
      final CalculationTaskHandler calculationTaskHandler) {
    return calculationTaskHandler;
  }

  /**
   * @param calculationJob the job to calculate
   * @return List of handlers that run after the results of a calculation are retrieved
   */
  default List<PostCalculationHandler> getPostCalculationHandlers(final CalculationJob calculationJob) throws AeriusException {
    return List.of();
  }

  /**
   * Returns additional handlers for theme specific actions. Should return an empty list if no handlers available to return.
   *
   * @param calculationJob
   * @param exportType
   * @param uiCalculation if this calculation is for the ui or background task
   * @param exportDate the moment the export started
   * @throws AeriusException
   */
  List<CalculationResultHandler> getCalculationResultHandler(CalculationJob calculationJob, ExportType exportType, Date exportDate)
      throws AeriusException;

  /**
   * @return A list of additional {@link WorkInitHandler}s.Should return empty list (Which does the default implementation) if no handlers needed.
   */
  default List<WorkInitHandler<AeriusPoint>> getWorkInitHandlers() {
    return List.of();
  }

  /**
   * @return a new queue that collects the calculated results.
   */
  CalculationResultQueue createCalculationResultQueue();

  /**
   * Option to add additional processing to the result handler. The default does nothing.
   *
   * @param resultHandler Result handler to use
   * @param calculationJob job containing calculation specific details.
   */
  default void prepareResultHandler(final ResultHandler resultHandler, final CalculationJob calculationJob) {
  }

  /**
   * Creates a theme specific work handler that can be used to process the input before it is sent to be calculated.
   *
   * @param calculationJob data object with input data
   * @param workHandler handler to which the processed calculation should be passed on
   * @return a theme specific work handler
   * @throws AeriusException
   */
  WorkHandler<AeriusPoint> createWorkHandler(CalculationJob calculationJob, CalculationTaskHandler workHandler) throws AeriusException;

  /**
   * Creates a theme specific {@link WorkDistributor} object.
   */
  WorkDistributor<AeriusPoint> createWorkDistributor(CalculationJob calculationJob) throws AeriusException, SQLException;
}
