/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;

/**
 * {@link CalculationResultHandler} to store the fileCode returned by the model worker into the database.
 */
public class ExportCalculationResultHandler implements CalculationResultHandler {

  private final CalculationJob calculationJob;

  public ExportCalculationResultHandler(final CalculationJob calculationJob) {
    this.calculationJob = calculationJob;
  }

  @Override
  public void onExportResult(final String fileUrl) {
    calculationJob.setResultUrl(fileUrl);
  }
}
