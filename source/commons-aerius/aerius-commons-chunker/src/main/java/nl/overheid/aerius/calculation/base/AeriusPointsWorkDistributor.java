/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.calculation.conversion.JobPackets;
import nl.overheid.aerius.calculation.conversion.JobPackets.JobPacketBuilder;
import nl.overheid.aerius.calculation.conversion.JobPackets.SourceConverterSupplier;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Generic Work distributor to calculate points.
 */
public abstract class AeriusPointsWorkDistributor implements WorkDistributor<AeriusPoint> {

  private static final Logger LOG = LoggerFactory.getLogger(AeriusPointsWorkDistributor.class);

  protected final PMF pmf;
  protected final SectorCategories sectorCategories;

  private final JobPacketBuilder jobPacketBuilder;

  /***
   * If not all points could be put in a work packet the remaining set of points is kept in this list.
   */
  private List<AeriusPoint> pointsToDo = List.of();
  /**
   * The set of receptors to return in the next work set.
   */
  private List<AeriusPoint> nextPointsToDo = List.of();

  private JobPackets jobPackets;
  private CommandType commandType;
  private int sourcesSize;
  private Integer calculationSubPointSetId;
  private CalculatorOptions calculatorOptions;

  protected AeriusPointsWorkDistributor(final PMF pmf, final SectorCategories sectorCategories) {
    this.pmf = pmf;
    this.sectorCategories = sectorCategories;
    jobPacketBuilder = new JobPacketBuilder(pmf, sectorCategories);
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    jobPackets = jobPacketBuilder.build(calculationJob, createSourceConverterSupplier(calculationJob));
    sourcesSize = jobPackets.getSourcesCount();
    commandType = calculationJob.getCommandType();
    calculationSubPointSetId = calculationJob.getScenarioCalculations().getCalculationPointSetTracker().getCalculationSubPointSetId();
    calculatorOptions = calculationJob.getCalculatorOptions();
  }

  /**
   * Creates a new {@link SourceConverterSupplier} to be used in the {@link JobPacketBuilder}.
   * @param calculationJob
   * @return the new {@link SourceConverterSupplier}
   */
  protected abstract SourceConverterSupplier createSourceConverterSupplier(final CalculationJob calculationJob);

  @Override
  public boolean prepareWork() throws AeriusException {
    synchronized (this) {
      if (!nextPointsToDo.isEmpty()) {
        return true; // if nextReceptorsToDo not yet processed do nothing yet.
      }
      if (hasMorePoints() || !pointsToDo.isEmpty()) {
        nextPointsToDo = collectReceptors(calculatorOptions);
        persistPointsIfNeeded(nextPointsToDo);
        postProcessPointsToDo(nextPointsToDo);
        return !nextPointsToDo.isEmpty();
      } else {
        return false;
      }
    }
  }

  /**
   * @return Returns true if there are still points in the list of points of this subclass.
   */
  protected abstract boolean hasMorePoints();

  @Override
  public WorkPacket<AeriusPoint> work() {
    synchronized (this) {
      if (nextPointsToDo.isEmpty()) {
        return null;
      }
      final WorkPacket<AeriusPoint> packet = new WorkPacket<>(nextPointsToDo, collectJobPackets());

      nextPointsToDo = List.of();
      return packet;
    }
  }

  private List<AeriusPoint> collectReceptors(final CalculatorOptions calculationOptions) throws AeriusException {
    if (pointsToDo.isEmpty()) {
      // put in modifiable list as points added will be removed from this list.
      pointsToDo = new ArrayList<>(collectPoints());
    }

    int nrOfPointsToAdd = nrOfReceptorsByCommandType(commandType, calculationOptions, sourcesSize);
    final List<AeriusPoint> pointsToCalculate = nrOfPointsToAdd == Integer.MAX_VALUE ? new ArrayList<>() : new ArrayList<>(nrOfPointsToAdd);

    while (nrOfPointsToAdd > 0) {
      if (pointsToDo.isEmpty()) {
        pointsToDo = collectMorePoints();
        if (pointsToDo.isEmpty()) {
          // No more points to calculate, break out of the while loop
          break;
        }
      }
      while (!pointsToDo.isEmpty() && nrOfPointsToAdd > 0) {
        final AeriusPoint pointToAdd = pointsToDo.remove(0);

        nrOfPointsToAdd -= addPoints(pointsToCalculate, pointToAdd);
      }
    }
    return pointsToCalculate;
  }

  /**
   * Perform operations on all points collected in a chunk. Default implementation does nothing.
   *
   * @param pointsToCalculate all points to calculated
   */
  protected void postProcessPointsToDo(final List<AeriusPoint> pointsToCalculate) throws AeriusException {
    // Default implementation does nothing.
  }

  private void persistPointsIfNeeded(final List<AeriusPoint> pointsToCalculate) throws AeriusException {
    if (calculationSubPointSetId != null) {
      final List<IsSubPoint> subPoints = pointsToCalculate.stream()
          .filter(IsSubPoint.class::isInstance)
          .map(IsSubPoint.class::cast)
          .filter(p -> p.getPointType() == AeriusPointType.SUB_RECEPTOR)
          .toList();
      try (final Connection con = pmf.getConnection()) {
        CalculationRepository.insertCalculationSubPointsForSet(con, calculationSubPointSetId, subPoints);
      } catch (final SQLException e) {
        LOG.error("SQL error while inserting sub points", e);
        throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
      }
    }
  }

  /**
   * When a chunk is not entirely filled by the collected receptors, by default we retrieve more receptors from the next set
   * When using aggregation we don't want receptors from different sets into one chunk, so we override this method
   * @return Returns a list of even more potential points to calculate.
   * @throws AeriusException
   */
  protected List<AeriusPoint> collectMorePoints() throws AeriusException {
    return collectPoints();
  }

  /**
   * @return Returns the list of job packets associated with a receptor set
   */
  protected List<WorkPacket.JobPacket> collectJobPackets() {
    return jobPackets.getJobPackets();
  }

  /**
   * @return Returns a list of potential points to calculate.
   */
  protected abstract List<AeriusPoint> collectPoints() throws AeriusException;

  /**
   * Add the pointsToDo to the pointsToCalculate list up till the maxAmountToAdd and returns the amount of points added.
   * Allows to filter points that should not be calculated (for example duplicate, outside scope).
   *
   * @param pointsToCalculate list of points to calculate
   * @param pointToAdd possible point to calculate
   * @return number of points added to the pointsToCalculate list
   * @throws AeriusException
   */
  protected abstract int addPoints(List<AeriusPoint> pointsToCalculate, AeriusPoint pointToAdd) throws AeriusException;
}
