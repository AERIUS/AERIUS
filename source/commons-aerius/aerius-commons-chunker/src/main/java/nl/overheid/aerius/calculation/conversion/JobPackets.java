/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.conversion.EngineSourceExpander;
import nl.overheid.aerius.conversion.SourceConversionHelper;
import nl.overheid.aerius.conversion.SourceConverter;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Contains the expanded sources grouped by an id.
 */
public class JobPackets {

  /**
   * Wrapper to lazily create a new context specific source converter.
   */
  public interface SourceConverterSupplier {
    /**
     * Creates a new source converter.
     */
    SourceConverter create(final Connection con, final ScenarioSituation situation) throws SQLException;
  }

  private final List<JobPacket> jobPacketList;
  private final int sourcesCount;

  private JobPackets(final List<JobPacket> jobPacketList, final int sourcesCount) {
    this.jobPacketList = jobPacketList;
    this.sourcesCount = sourcesCount;
  }

  public List<JobPacket> getJobPackets() {
    return jobPacketList;
  }

  public int getSourcesCount() {
    return sourcesCount;
  }

  public static class JobPacketBuilder {
    private final PMF pmf;
    private final SectorCategories sectorCategories;

    public JobPacketBuilder(final PMF pmf, final SectorCategories sectorCategories) {
      this.pmf = pmf;
      this.sectorCategories = sectorCategories;
    }

    /**
     * Digest all sources and potentially store them in the {@link CalculationJob} to be used by later on.
     * @return
     */
    public JobPackets build(final CalculationJob calculationJob, final SourceConverterSupplier sourceConverterSupplier)
        throws SQLException, AeriusException {
      final List<Substance> substances = calculationJob.getSubstances();
      final List<JobPacket> jobPackets = new ArrayList<>();

      for (final Calculation job : calculationJob.getScenarioCalculations().getCalculations()) {
        final CalculationSetOptions cso = calculationJob.getCalculationSetOptions();
        final SourceConversionHelper conversionHelper = new SourceConversionHelper(pmf, sectorCategories, job.getYear());

        try (Connection con = pmf.getConnection()) {
          final SourceConverter sourceConverter = sourceConverterSupplier.create(con, job.getSituation());

          jobPackets.add(new JobPacket(job.getCalculationId(), job.getYear(),
              EngineSourceExpander.toEngineSourcesByGroup(con, sourceConverter, conversionHelper, job.getSituation(),
                  substances, cso.isStacking())));
        }
      }
      return new JobPackets(jobPackets, countAllSources(jobPackets));
    }

    /**
     * Computes the total number of sources.
     *
     * @param jobPackets
     * @return
     */
    private static int countAllSources(final List<JobPacket> jobPackets) {
      return jobPackets.stream().mapToInt(e -> e.getSourcesPackets().stream().mapToInt(s -> s.getSources().size()).sum()).sum();
    }
  }
}
