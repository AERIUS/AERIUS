/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.io.Serializable;

class AreaData implements Serializable {
  private static final long serialVersionUID = -4019232787744033039L;

  private final int assessmentId;
  private final int receptorCount;
  private final int distance;

  public AreaData(final int assessmentId, final int receptorCount, final int distance) {
    this.assessmentId = assessmentId;
    this.receptorCount = receptorCount;
    this.distance = distance;
  }

  public int getAssessmentId() {
    return assessmentId;
  }

  public int getDistance() {
    return distance;
  }

  public int getReceptorCount() {
    return receptorCount;
  }
}
