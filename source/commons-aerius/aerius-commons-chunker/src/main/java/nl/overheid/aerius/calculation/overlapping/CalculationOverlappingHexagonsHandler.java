/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.overlapping;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 *
 */
public class CalculationOverlappingHexagonsHandler implements PostCalculationHandler {

  private static final Logger LOG = LoggerFactory.getLogger(CalculationOverlappingHexagonsHandler.class);

  private static final int OWN2000_MAX_DISTANCE_M = 25_000;

  private final PMF pmf;
  private final OverlappingHexagonDetector detector;

  public CalculationOverlappingHexagonsHandler(final PMF pmf, final Natura2kReceptorStore receptorStore) throws SQLException {
    this.pmf = pmf;
    final double maxPolygonToPointsGridSize = ConstantRepository.getNumber(pmf, ConstantsEnum.CONVERT_POLYGON_TO_POINTS_GRID_SIZE, Double.class);
    detector = new OverlappingHexagonDetector(receptorStore.getAllReceptors(), maxPolygonToPointsGridSize);
  }

  @Override
  public void onCompleted(final CalculationJob calculationJob) throws AeriusException {
    final Optional<Integer> maxDistance = getMaxDistance(calculationJob.getCalculationSetOptions());
    if (maxDistance.isPresent()) {
      for (final ScenarioSituation situation : calculationJob.getScenarioCalculations().getScenario().getSituations()) {
        final Optional<Integer> calculationId = calculationJob.getScenarioCalculations().getCalculationForSituation(situation)
            .map(Calculation::getCalculationId);
        if (calculationId.isPresent()) {
          final List<AeriusPoint> overlappingHexagons = detector.detect(situation.getEmissionSourcesList(), maxDistance.get());

          insertIntoDatabase(calculationId.get(), overlappingHexagons);
        }
      }
    }
  }

  private void insertIntoDatabase(final int calculationId, final List<AeriusPoint> overlappingHexagons) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      CalculationRepository.insertOverlappingHexagons(connection, calculationId, overlappingHexagons);
    } catch (final SQLException e) {
      LOG.error("SQL exception when inserting overlapping hexagons", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private static Optional<Integer> getMaxDistance(final CalculationSetOptions options) {
    final Optional<Integer> maxDistance;
    if (options.getOwN2000CalculationOptions().isUseMaxDistance()) {
      maxDistance = Optional.of(OWN2000_MAX_DISTANCE_M);
    } else if (options.isMaximumRangeRelevant() && options.getCalculateMaximumRange() > 0) {
      maxDistance = Optional.of((int) Math.round(options.getCalculateMaximumRange()));
    } else {
      maxDistance = Optional.empty();
    }
    return maxDistance;
  }

}
