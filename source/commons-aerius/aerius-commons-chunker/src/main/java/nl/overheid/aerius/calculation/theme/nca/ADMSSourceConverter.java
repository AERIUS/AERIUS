/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;

import nl.aerius.adms.conversion.ADMSBuildingTracker;
import nl.aerius.adms.conversion.EmissionSource2ADMSConverter;
import nl.aerius.adms.domain.ADMSGroupKey;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.conversion.SourceConverter;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.characteristics.StandardTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.ADMSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.ADMSRoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * ADMS specific conversion of {@link EmissionSource} objects to ADMS source objects.
 */
class ADMSSourceConverter implements SourceConverter {

  private static final Set<String> EMPTY_DV_CODES = new HashSet<>();

  private final EmissionSource2ADMSConverter converter;
  private final SectorCategories sectorCategories;
  private ADMSBuildingTracker buildingTracker;
  private final ScenarioSituation situation;
  private final Map<ADMSGroupKey, Set<String>> customTimeVaryingProfileIds = new EnumMap<>(ADMSGroupKey.class);
  private final Map<ADMSGroupKey, Set<String>> standardTimeVaryingProfileCodes = new EnumMap<>(ADMSGroupKey.class);

  public ADMSSourceConverter(final int srid, final ScenarioSituation situation, final SectorCategories sectorCategories) {
    this.situation = situation;
    this.sectorCategories = sectorCategories;
    converter = new EmissionSource2ADMSConverter(srid, situation.getYear(), sectorCategories.getRoadEmissionCategories());
  }

  @Override
  public void convertBuildings() throws AeriusException {
    buildingTracker = new ADMSBuildingTracker(situation.getBuildingsList());
  }

  @Override
  public boolean convert(final Connection con, final GroupedSourcesPacketMap map, final EmissionSource originalSource, final Geometry geometry,
      final List<Substance> substances) throws AeriusException {
    // add NH3
    add(map, ADMSGroupKey.NH3, originalSource, geometry);
    // add NOx road
    if (originalSource instanceof ADMSRoadEmissionSource) {
      add(map, ADMSGroupKey.NOX_ROAD, originalSource, geometry);
    } else {
      // add NOx other
      add(map, ADMSGroupKey.NOX_OTHER, originalSource, geometry);
    }
    return true;
  }

  private void add(final GroupedSourcesPacketMap map, final ADMSGroupKey key, final EmissionSource originalSource, final Geometry geometry)
      throws AeriusException {
    final boolean roadSource = originalSource instanceof ADMSRoadEmissionSource;
    if (key.getEmissionSubstances(roadSource).stream()
        .anyMatch(s -> originalSource.getEmissions().containsKey(s) && originalSource.getEmissions().get(s) > 0.0)) {
      // Collect time-varying profiles before converting, because collect modifies the original source and sets time-varying profile.
      // In the converter after the collect it passes the time-varying profile to the converted source.
      collectTimeVaryingProfiles(originalSource, key);
      map.addAll(key.getIndex(), CalculationEngine.ADMS, converter.convert(originalSource, geometry, key.getEmissionSubstances(roadSource)));
    }
  }

  private void collectTimeVaryingProfiles(final EmissionSource source, final ADMSGroupKey key) {
    if (source.getCharacteristics() instanceof final ADMSSourceCharacteristics admsChars) {
      processTimeVaryingProfile(admsChars, source.getSectorId(), key);
    } else {
      // Other objects don't have time-varying aspects
    }
  }

  private void processTimeVaryingProfile(final ADMSSourceCharacteristics chars, final int sectorId, final ADMSGroupKey key) {
    processTimeVaryingProfile(CustomTimeVaryingProfileType.THREE_DAY, chars::getCustomHourlyTimeVaryingProfileId,
        chars::getStandardHourlyTimeVaryingProfileCode, chars::setStandardHourlyTimeVaryingProfileCode, sectorId, key);
    processTimeVaryingProfile(CustomTimeVaryingProfileType.MONTHLY, chars::getCustomMonthlyTimeVaryingProfileId,
        chars::getStandardMonthlyTimeVaryingProfileCode, chars::setStandardMonthlyTimeVaryingProfileCode, sectorId, key);
  }

  private void processTimeVaryingProfile(final CustomTimeVaryingProfileType type, final Supplier<String> customIdGetter,
      final Supplier<String> standardCodeGetter, final Consumer<String> defaultCodeSetter, final int sectorId, final ADMSGroupKey key) {
    final String customId = customIdGetter.get();
    final String standardCode = standardCodeGetter.get();
    if (customId != null) {
      addToTimeVaryingProfilesMap(customTimeVaryingProfileIds, key, customId);
    } else if (standardCode != null) {
      addToTimeVaryingProfilesMap(standardTimeVaryingProfileCodes, key, standardCode);
    } else {
      final StandardTimeVaryingProfile defaultProfile = sectorCategories.getTimeVaryingProfiles().defaultProfileForSectorAndYear(
          type, sectorId, situation.getYear());
      if (defaultProfile != null) {
        // Ensure the time-varying profile is set to the correct standard code, for further processing by converter.
        defaultCodeSetter.accept(defaultProfile.getCode());
        addToTimeVaryingProfilesMap(standardTimeVaryingProfileCodes, key, defaultProfile.getCode());
      }
    }
  }

  private static void addToTimeVaryingProfilesMap(final Map<ADMSGroupKey, Set<String>> map, final ADMSGroupKey key, final String dvId) {
    map.computeIfAbsent(key, k -> new HashSet<>()).add(dvId);
  }

  @Override
  public void postProcess(final GroupedSourcesPacketMap map) {
    final Map<String, EngineSource> buildings = buildingTracker.getAllBuildings();

    if (!buildings.isEmpty()) {
      final Collection<EngineSource> buildingValues = buildings.values();

      for (final ADMSGroupKey key : ADMSGroupKey.values()) {
        map.addAllIfNotEmpty(key.getIndex(), CalculationEngine.ADMS, buildingValues);
      }
    }
    for (final ADMSGroupKey key : ADMSGroupKey.values()) {
      map.addAllIfNotEmpty(key.getIndex(), CalculationEngine.ADMS, convertTimeVaryingProfileDefinitions(key));
    }
  }

  private Collection<EngineSource> convertTimeVaryingProfileDefinitions(final ADMSGroupKey key) {
    final List<EngineSource> converted = new ArrayList<>();

    addStandardProfiles(CustomTimeVaryingProfileType.THREE_DAY, key, converted);
    addStandardProfiles(CustomTimeVaryingProfileType.MONTHLY, key, converted);

    if (situation.getDefinitions() != null && situation.getDefinitions().getCustomTimeVaryingProfiles() != null) {
      final List<CustomTimeVaryingProfile> usedCustomDefinitions = situation.getDefinitions().getCustomTimeVaryingProfiles().stream()
          .filter(x -> customTimeVaryingProfileIds.getOrDefault(key, EMPTY_DV_CODES).contains(x.getGmlId()))
          .toList();
      converted.addAll(EmissionSource2ADMSConverter.convertCustomTimeVaryingProfiles(usedCustomDefinitions));
    }
    return converted;
  }

  private void addStandardProfiles(final CustomTimeVaryingProfileType type, final ADMSGroupKey key, final List<EngineSource> converted) {
    final List<StandardTimeVaryingProfile> usedStandardDefinitions = standardTimeVaryingProfileCodes.getOrDefault(key, EMPTY_DV_CODES).stream()
        .map(x -> sectorCategories.getTimeVaryingProfiles().determineStandardTimeVaryingProfile(type, x))
        .filter(Objects::nonNull)
        .toList();
    converted.addAll(EmissionSource2ADMSConverter.convertStandardTimeVaryingProfiles(usedStandardDefinitions));
  }
}
