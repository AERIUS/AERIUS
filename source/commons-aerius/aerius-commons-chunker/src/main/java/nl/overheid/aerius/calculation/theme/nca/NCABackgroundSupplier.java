/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_CONCENTRATION;

import java.util.List;

import nl.aerius.adms.domain.ADMSSubReceptor;
import nl.overheid.aerius.db.common.ReceptorInfoRepositoryBean;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * Sets the NOx background values from the database on the given ADMSCalculationPoints.
 * Because a single point can be used to calculate multiple years in 1 run background for multiple years can be put into the ADMSCalculationPoint.
 */
class NCABackgroundSupplier {

  private final ReceptorInfoRepositoryBean receptorInfoRepository;
  private final ReceptorUtil receptorUtil;

  public NCABackgroundSupplier(final ReceptorInfoRepositoryBean receptorInfoRepository, final ReceptorUtil receptorUtil) {
    this.receptorInfoRepository = receptorInfoRepository;
    this.receptorUtil = receptorUtil;
  }

  /**
   * Sets background NOx for given years on the given points.
   *
   * @param points points to set the background NOx on
   * @param years years get the NOx background
   */
  public void setBackgroundNOx(final List<AeriusPoint> points, final List<Integer> years) {
    for (final AeriusPoint aeriusPoint : points) {
      if (aeriusPoint instanceof final ADMSSubReceptor admsSubReceptor) {
        setBackgroundSubReceptor(admsSubReceptor, years);
      } else if (aeriusPoint instanceof final ADMSCalculationPoint admsCalculationPoint) {
        setBackgroundNOx(admsCalculationPoint, years);
      }
    }
  }

  /**
   * Sets background NOx for given years on the given point.
   *
   * @param point point to set the background NOx on
   * @param years years get the NOx background
   */
  public void setBackgroundNOx(final ADMSCalculationPoint point, final List<Integer> years) {
    years.forEach(year -> point.setBackgroundNOx(year,
        queryBackgroundNOx(point, receptorUtil.getReceptorIdFromCoordinate(point.getX(), point.getY()), year)));
  }

  private void setBackgroundSubReceptor(final ADMSSubReceptor point, final List<Integer> years) {
    years.forEach(year -> point.setBackgroundNOx(year, queryBackgroundNOx(point, point.getParentId(), year)));
  }

  private double queryBackgroundNOx(final AeriusPoint point, final int receptorId, final int year) {
    try {
      return receptorInfoRepository.getBackgroundEmissionResultERK(point, receptorId, year, NOX_CONCENTRATION).get(NOX_CONCENTRATION);
    } catch (final AeriusException e) {
      return 0;
    }
  }
}
