/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.calculation.base.WorkInitHandler;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface for the queue that receives the calculated results.
 */
public interface CalculationResultQueue extends WorkInitHandler<AeriusPoint> {

  /**
   * Initialize the queue.Used to determine the amount of results to expect based on what is in the job.
   *
   * @param calculationJob calculation job
   * @param packets work packets
   */
  @Override
  void init(final CalculationJob calculationJob, final List<WorkPacket<AeriusPoint>> packets);

  /**
   * Clears the queue in case of an error.
   */
  void clear();

  /**
   * Put a result in the queue for a specific calculation.
   *
   * @param engineDataKey engine data key identifier
   * @param results results
   * @param calculationId calculation these results are for
   * @throws AeriusException
   */
  void put(EngineDataKey engineDataKey, Map<Integer, List<AeriusResultPoint>> results, int calculationId) throws AeriusException;

  /**
   * Returns a list of calculated results that contain results for all models.
   *
   * @param calculationId
   * @return
   */
  ArrayList<AeriusResultPoint> pollTotalResults(int calculationId);

  /**
   * Returns true if not all results that are to be calculated are available in the queue.
   *
   * @param calculationId
   * @return
   */
  boolean isExpectingMoreResults(final int calculationId);

}
