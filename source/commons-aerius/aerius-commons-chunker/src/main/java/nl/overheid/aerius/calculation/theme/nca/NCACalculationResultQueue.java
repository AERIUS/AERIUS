/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.base.AggregatedCalculationResultQueue;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.util.AeriusPointUtil;

/**
 * {@link AggregatedCalculationResultQueue} implementation for NCA theme.
 */
class NCACalculationResultQueue extends AggregatedCalculationResultQueue<String> {

  public NCACalculationResultQueue() {
    super(AeriusPointUtil::encodeName);
  }

  @Override
  protected AeriusResultPoint aggregateCompleteResult(final AeriusResultPoint arp,
      final Map<EngineDataKey, Map<Integer, EmissionResults>> allResults) {
    if (allResults.size() == 1) {
      return super.aggregateCompleteResult(arp, allResults);
    } else {
      return aggregateMultiMeteoYearResults(arp, allResults);
    }
  }

  private static AeriusResultPoint aggregateMultiMeteoYearResults(final AeriusResultPoint arp,
      final Map<EngineDataKey, Map<Integer, EmissionResults>> allResults) {
    for (final Entry<EngineDataKey, Map<Integer, EmissionResults>> engineResult : allResults.entrySet()) {
      calculateEmissionResultsSingleKey(engineResult).entrySet().forEach(e -> {
        if (e.getValue() > arp.getEmissionResult(e.getKey())) {
          arp.setEmissionResult(e.getKey(), e.getValue());
        }
      });
    }
    return arp;
  }

  private static EmissionResults calculateEmissionResultsSingleKey(final Entry<EngineDataKey, Map<Integer, EmissionResults>> engineResult) {
    final EmissionResults edkResults = new EmissionResults();

    for (final Entry<Integer, EmissionResults> groupResult : engineResult.getValue().entrySet()) {
      for (final Entry<EmissionResultKey, Double> emission : groupResult.getValue().entrySet()) {
        final Double value = emission.getValue();

        if (!Double.isNaN(value)) {
          edkResults.put(emission.getKey(), edkResults.get(emission.getKey()) + value);
        }
      }
    }
    return edkResults;
  }
}
