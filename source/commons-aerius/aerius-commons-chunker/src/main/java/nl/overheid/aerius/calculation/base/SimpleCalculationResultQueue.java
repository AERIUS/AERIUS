/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Queue to collect calculated results before storing them in a persistent storage place.
 */
public class SimpleCalculationResultQueue implements CalculationResultQueue {

  private final Map<Integer, List<AeriusResultPoint>> completeResults = new HashMap<>();

  @Override
  public void init(final CalculationJob calculationJob, final List<WorkPacket<AeriusPoint>> packets) {
    if (!packets.isEmpty()) {
      for (final JobPacket packet : packets.get(0).getJobPackets()) {
        completeResults.put(packet.getCalculationId(), new ArrayList<>());
      }
    }
  }

  @Override
  public void clear() {
    completeResults.clear();
  }

  @Override
  public void put(final EngineDataKey engineDataKey, final Map<Integer, List<AeriusResultPoint>> results, final int calculationId) {
    synchronized (this) {
      results.forEach((k, v) -> completeResults.get(calculationId).addAll(v));
    }
  }

  @Override
  public ArrayList<AeriusResultPoint> pollTotalResults(final int calculationId) {
    final ArrayList<AeriusResultPoint> results = new ArrayList<>();
    final List<AeriusResultPoint> queue = completeResults.get(calculationId);

    synchronized (this) {
      if (queue != null) {
        results.addAll(queue);
        queue.clear();
      }
    }
    return results;
  }

  @Override
  public boolean isExpectingMoreResults(final int calculationId) {
    // This queue has no way of knowing that more results are expected.
    return false;
  }

}
