/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.util.ArrayList;
import java.util.List;

import org.locationtech.jts.geom.Geometry;

import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasure;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Adds measures to SRM1 road sources.
 */
public class RBLSRM1Preprocessor {

  private static class MeasureGeometry {
    private final Geometry geometry;
    private final CIMLKMeasure measure;

    MeasureGeometry(final Geometry geometry, final CIMLKMeasure measure) {
      this.geometry = geometry;
      this.measure = measure;
    }
  }

  private static final double MEASURE_APPLY_THRESHOLD = 0.5;

  private final List<MeasureGeometry> measureGeometries = new ArrayList<>();

  RBLSRM1Preprocessor(final Calculation calculation) throws AeriusException {
    this.fillMeasureMap(calculation);
  }

  private void fillMeasureMap(final Calculation calculation) throws AeriusException {
    for (final CIMLKMeasureFeature feature : calculation.getCIMLKMeasures()) {
      final Geometry measureGeometry = GeometryUtil.getGeometry(feature.getGeometry());
      measureGeometries.add(new MeasureGeometry(measureGeometry, feature.getProperties()));
    }
  }

  /**
   * Adds all applicable measures to the emission source.
   * @param geometry the geometry of the source
   * @param source the source
   * @throws AeriusException when geometry can't be processed.
   */
  public void addMeasures(final LineString geometry, final EmissionSource source) throws AeriusException {
    if (!measureGeometries.isEmpty() && source instanceof SRM1RoadEmissionSource) {
      final SRM1RoadEmissionSource srm2Source = (SRM1RoadEmissionSource) source;
      final Geometry jtsSourceGeometry = GeometryUtil.getGeometry(geometry);
      for (final MeasureGeometry measureGeometry : measureGeometries) {
        if (shouldApplyMeasure(measureGeometry.geometry, jtsSourceGeometry)) {
          srm2Source.getSubSources().stream()
              .filter(StandardVehicles.class::isInstance)
              .map(StandardVehicles.class::cast)
              .forEach(subSource -> subSource.getMeasures().addAll(measureGeometry.measure.getVehicleMeasures()));
        }
      }
    }
  }

  private static boolean shouldApplyMeasure(final Geometry measureGeometry, final Geometry sourceGeometry) {
    return measureGeometry.intersection(sourceGeometry).getLength() > MEASURE_APPLY_THRESHOLD * sourceGeometry.getLength();
  }

}
