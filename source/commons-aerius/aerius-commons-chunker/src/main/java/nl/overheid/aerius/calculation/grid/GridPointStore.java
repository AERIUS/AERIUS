/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.SridPoint;

/**
 * Storage for organizing AERIUS points in a grid. Uses for organizing points to calculate in a grid.
 */
public class GridPointStore {
  private final GridUtil gridUtil;
  private final GridZoomLevel zoomLevel;
  private final Map<Integer, Collection<AeriusPoint>> map;

  public GridPointStore(final GridSettings gridSettings) {
    gridUtil = new GridUtil(gridSettings);
    this.zoomLevel = gridSettings.getZoomLevelMax();
    map = new HashMap<>();
  }

  public double getGridDiameter() {
    return zoomLevel.getDiameter();
  }

  protected SridPoint getPositionFromCell(final int gridId) {
    return gridUtil.getPositionFromCell(gridId, zoomLevel);
  }

  protected Collection<AeriusPoint> createNewEmptyStore() {
    return new ArrayList<>();
  }

  public void add(final AeriusPoint point) {
    add(getGridId(point), point);
  }

  protected void add(final int gridId, final AeriusPoint point) {
    getGridCell(gridId).add(point);
  }

  protected int getGridId(final AeriusPoint point) {
    return gridUtil.getCellFromPosition(point, zoomLevel);
  }

  public Collection<AeriusPoint> getGridCell(final int gridId) {
    return map.computeIfAbsent(gridId, gid -> createNewEmptyStore());
  }

  /**
   * @return Returns a Set of grid all points organized by grid id
   */
  public Set<Entry<Integer, Collection<AeriusPoint>>> entrySet() {
    return map.entrySet();
  }

  public int size() {
    return map.size();
  }
}
