/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.domain.ModelProgressReport;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Wraps multiple {@link CalculationResultHandler}s and calls them in order of the handlers being added.
 */
class MultipleCalculationResultHandler implements CalculationResultHandler {

  private final List<CalculationResultHandler> resultHandlers = new ArrayList<>();

  public <T extends CalculationResultHandler> T addHandler(final T handler) {
    resultHandlers.add(handler);
    return handler;
  }

  public List<CalculationResultHandler> getResultHandlers() {
    return resultHandlers;
  }

  @Override
  public void onSectorResults(final Map<Integer, List<AeriusResultPoint>> results, final int calculationId, final EngineDataKey engineDataKey)
      throws AeriusException {
    for (final CalculationResultHandler handler : resultHandlers) {
      handler.onSectorResults(results, calculationId, engineDataKey);
    }
  }

  @Override
  public void onSectorResults(final List<AeriusResultPoint> results, final int calculationId, final EngineDataKey engineDataKey, final int sectorId)
      throws AeriusException {
    throw new IllegalCallerException("onSectorResults with sectorId in MultipleCalculationResultHandler should not be called directly");
  }

  @Override
  public void onTotalResults(final List<AeriusResultPoint> result, final int calculationId) throws AeriusException {
    for (final CalculationResultHandler handler : resultHandlers) {
      handler.onTotalResults(result, calculationId);
    }
  }

  @Override
  public void onExportResult(final String fileUrl) throws AeriusException {
    for (final CalculationResultHandler handler : resultHandlers) {
      handler.onExportResult(fileUrl);
    }
  }

  @Override
  public void onModelProgressReport(final ModelProgressReport progressReport) throws AeriusException {
    for (final CalculationResultHandler handler : resultHandlers) {
      handler.onModelProgressReport(progressReport);
    }
  }
}
