/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.SQLException;

import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * A {@link WorkDistributor} determines the receptors and sources to calculate.
 * It should return 1 set of sources/receptors for a single run on a worker.
 */
public interface WorkDistributor<P extends AeriusPoint> {

  /**
   * Called to initialize the work distributor.
   *
   * @param calculationJob
   * @throws SQLException
   * @throws AeriusException
   */
  void init(final CalculationJob calculationJob) throws SQLException, AeriusException;

  /**
   * @return Returns true if there is still work to do.
   *
   * @throws AeriusException
   */
  boolean prepareWork() throws AeriusException;

  /**
   * Returns a single task of work to do on a worker. The size of the work is based on the input parameters.
   * It should return (if possible) at least the given minimal number of receptors. This to avoid have to small sized tasks.
   * The number of receptors to return in the task is determined by the maxCalculationEngineUnits variable.
   * The number of sources multiplied by the number of receptors should remain below the max value.
   *
   * @return a work packet to be processed to be sent to the workers.
   * @throws AeriusException
   */
  WorkPacket<P> work() throws AeriusException;

  /**
   * Called when work is done.
   *
   * @throws AeriusException Thrown to indicate the completion failed.
   */
  default void onCompleted() throws AeriusException {
  }

  /**
   * Returns the number of receptors to calculate in a work packet. If this is an export it should return all receptors in 1 packet.
   *
   * @param calculationOptions
   * @return
   */
  default int nrOfReceptorsByCommandType(final CommandType commandType, final CalculatorOptions calculationOptions, final int sourcesSize) {
    return commandType == CommandType.EXPORT ? Integer.MAX_VALUE : nrOfPoints(calculationOptions, sourcesSize);
  }

  /**
   * Computes the number of calculations points that can be calculated given the boundaries and number of sources.
   *
   * @param calculationOptions boundaries for max and minimum units to calculate
   * @param sourcesSize number of sources to calculate
   * @return number of calculation points that can be calculated
   */
  default int nrOfPoints(final CalculatorOptions calculationOptions, final int sourcesSize) {
    // Compute the amount of receptors that could be calculated based on the ratio between max engine units and number of sources.
    final double computedReceptors = Math.ceil(calculationOptions.getMaxCalculationEngineUnits() / (double) sourcesSize);
    // Make sure a minimum number of receptors is always calculated
    final double maxWithMinimumReceptors = Math.max(computedReceptors, calculationOptions.getMinReceptor());
    // Make sure the number of receptors to calculate doesn't exceed the set max amount of receptors.
    return (int) Math.min(maxWithMinimumReceptors, calculationOptions.getMaxReceptors());
  }
}
