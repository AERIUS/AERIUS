/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculatorThemeFactory;
import nl.overheid.aerius.calculation.base.IncrementalWorkChunker;
import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.ScheduledWorkRunnable;
import nl.overheid.aerius.calculation.base.TwoStepWorkChunker;
import nl.overheid.aerius.calculation.base.WorkChunker;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.base.WorkStatisticsUtil;
import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Creates a Calculator object on which all handlers can be added. When done with the call to {@link #build(RabbitMQQueueMonitor, TaskManagerClient)}
 * the Calculator object is completed and ready to run.
 */
final class CalculatorBuilder {

  private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorBuilder.class);

  private final PMF pmf;
  private final ScheduledExecutorService executor;
  private final CalculatorThemeFactory themeFactory;
  private final CalculationJob calculationJob;
  private final MultipleCalculationResultHandler calculationResultHandler = new MultipleCalculationResultHandler();
  private final List<PostCalculationHandler> postCalculationHandlers = new ArrayList<>();
  private final WorkStatisticsUtil<AeriusPoint> workStatisticsUtil;

  public CalculatorBuilder(final PMF pmf, final ScheduledExecutorService executor, final CalculatorThemeFactory themeFactory,
      final CalculationJob calculationJob) {
    this.pmf = pmf;
    this.executor = executor;
    this.themeFactory = themeFactory;
    this.calculationJob = calculationJob;
    this.workStatisticsUtil = new WorkStatisticsUtil<>(pmf);
  }

  public CalculatorBuilder init(final ExportType exportType, final Date exportDate) throws AeriusException {
    setCalculationResultHandler(exportType, exportDate);
    setPostCalculationHandlers(exportType);
    return this;
  }

  /**
   * Sets the {@link CalculationResultHandler} on the calculator.
   * @param exportType The export type being processed
   * @param exportDate The moment of the export.
   * @throws AeriusException
   */
  private void setCalculationResultHandler(final ExportType exportType, final Date exportDate)
      throws AeriusException {
    themeFactory.getCalculationResultHandler(calculationJob, exportType, exportDate).forEach(calculationResultHandler::addHandler);
  }

  /**
   * Sets the PostCalculationHandlers.
   * @param exportType The export type being processed
   */
  private void setPostCalculationHandlers(final ExportType exportType) throws AeriusException {
    if (!exportType.isSourceExport()) {
      calculationResultHandler.getResultHandlers().forEach(crh -> {
        if (crh instanceof final PostCalculationHandler pch) {
          postCalculationHandlers.add(pch);
        }
      });
      themeFactory.getPostCalculationHandlers(calculationJob).forEach(handler -> {
        if (handler instanceof final CalculationResultHandler crh) {
          calculationResultHandler.addHandler(crh);
        }
        postCalculationHandlers.add(handler);
      });
      postCalculationHandlers.add(new MetricReporter(exportType));
    }
  }

  /**
   * Set the progress tracking of the calculation in the database. Used in combination with a database Job.
   * @return this object
   */
  public CalculatorBuilder setTrackJobHandler() {
    LOGGER.trace("Add TrackJobProgressHandler");
    calculationResultHandler.addHandler(new TrackJobProgressHandler(pmf, calculationJob));
    return this;
  }

  /**
   * Builds the calculator object and stitches everything together.
   * @param rabbitMQWorkerMonitor
   * @param taskManagerClient
   * @param workerType
   * @return new Calculator object
   */
  public Calculator build(final RabbitMQWorkerMonitor rabbitMQWorkerMonitor, final TaskManagerClientSender taskManagerClient,
      final WorkerType workerType) throws AeriusException, SQLException {
    LOGGER.trace("Build the Calculator object.");

    final DynamicWorkSemaphore workLocker = new DynamicWorkSemaphore(rabbitMQWorkerMonitor, workerType);
    final WorkTracker tracker = new WorkTrackerImpl();
    final WorkMonitorImpl workMonitor = new WorkMonitorImpl(tracker, workLocker);
    calculationResultHandler.addHandler(workMonitor);
    postCalculationHandlers.add(workMonitor);
    final CalculationResultQueue queue = themeFactory.createCalculationResultQueue();

    LOGGER.trace("Create Handlers");
    final ResultHandler resultHandler = new ResultHandlerImpl(pmf, workMonitor, queue, calculationResultHandler);
    final CalculationTaskHandler remoteWorkHandler = new RemoteCalculationTaskHandler(taskManagerClient, resultHandler,
        calculationJob.getQueueName());
    final CalculationTaskHandler calculationTaskHandler = themeFactory.wrapCalculationTaskHandler(calculationJob, remoteWorkHandler);
    themeFactory.prepareResultHandler(resultHandler, calculationJob);

    LOGGER.trace("Create WorkHandler");
    final WorkHandler<AeriusPoint> workHandler = themeFactory.createWorkHandler(calculationJob, calculationTaskHandler);
    LOGGER.trace("Create WorkDistributor");
    final WorkDistributor<AeriusPoint> workDistributor = createWorkDistributor();
    LOGGER.trace("Create Scheduled WorkRunnables");
    final List<ScheduledWorkRunnable> scheduledWorkRunnables = createScheduledWorkRunnables(queue);
    final MultipleWorkInitHandler<AeriusPoint> workInitHandler = new MultipleWorkInitHandler<>();

    workInitHandler.addHandler(queue);
    themeFactory.getWorkInitHandlers().forEach(workInitHandler::addHandler);
    final WorkChunker<AeriusPoint> workChunker;

    if (calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions().isForceAggregation()) {
      workChunker = new IncrementalWorkChunker<>(calculationJob, workDistributor, workHandler);
    } else {
      workInitHandler.addHandler(workStatisticsUtil);
      workChunker = new TwoStepWorkChunker<>(pmf, calculationJob, workDistributor, workHandler);
    }

    return new Calculator(pmf, executor, calculationJob, workChunker, scheduledWorkRunnables, postCalculationHandlers, workInitHandler, tracker);
  }

  private WorkDistributor<AeriusPoint> createWorkDistributor() throws AeriusException, SQLException {
    final WorkDistributor<AeriusPoint> workDistributor = themeFactory.createWorkDistributor(calculationJob);

    if (workDistributor instanceof final CalculationResultHandler crh) {
      calculationResultHandler.addHandler(crh);
    }
    if (workDistributor instanceof final PostCalculationHandler pch) {
      postCalculationHandlers.add(pch);
    }
    return workDistributor;
  }

  private List<ScheduledWorkRunnable> createScheduledWorkRunnables(final CalculationResultQueue queue) {
    if (calculationJob.getCommandType() == CommandType.CALCULATE) {
      return List.of(new TotalResultsProcessor(queue, calculationResultHandler, calculationJob));
    } else {
      return List.of();
    }
  }
}
