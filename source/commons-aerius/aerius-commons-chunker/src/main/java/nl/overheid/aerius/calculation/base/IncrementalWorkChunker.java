/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.SQLException;
import java.util.List;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Performs the main chunking of the work distributor.
 *
 * This chunker send each work package after the packet has been collected.
 * This is done until all points to be calculate are determined.
 * Determine of the point can be done based on outcome results, but that process is outside of this class,
 * it's just this class supports functionallity.
 */
public class IncrementalWorkChunker<P extends AeriusPoint> implements WorkChunker<P> {

  private final WorkHandler<P> workHandler;
  private final CalculationJob calculationJob;
  private final WorkDistributor<P> workDistributor;

  public IncrementalWorkChunker(final CalculationJob calculationJob, final WorkDistributor<P> workDistributor, final WorkHandler<P> workHandler) {
    this.calculationJob = calculationJob;
    this.workDistributor = workDistributor;
    this.workHandler = workHandler;
  }

  /**
   * Perform all calculations until all work is done.
   *
   * @throws AeriusException
   * @throws SQLException
   * @throws InterruptedException
   * @throws TaskCancelledException
   */
  @Override
  public void run(final WorkInitHandler<P> initHandler) throws Exception {
    workDistributor.init(calculationJob);
    boolean first = true;

    while (workDistributor.prepareWork()) {
      final WorkPacket<P> work = workDistributor.work();

      if (work == null) {
        continue; // prepareWork lied!! it had no work...try again.
      }
      if (first) {
        initHandler.init(calculationJob, List.of(work));
        first = false;
      }
      for (final JobPacket packet : work.getJobPackets()) {
        workHandler.work(calculationJob, packet, work.getPoints());
      }
    }
  }

  @Override
  public void onComplete() throws AeriusException {
    workDistributor.onCompleted();
  }
}
