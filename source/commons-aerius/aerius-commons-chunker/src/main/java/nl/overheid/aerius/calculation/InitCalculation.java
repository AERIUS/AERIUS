/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Class to initialize a calculation job and insert the calculation base in the database.
 */
final class InitCalculation {

  private static final Logger LOG = LoggerFactory.getLogger(InitCalculation.class);

  private InitCalculation() {
    // Util class.
  }

  /**
   * Initializes a CalculationJob, inserts the calculation in the database.
   *
   * @param con database connection
   * @param inputData input data
   * @param scenarioCalculations Object keeping track of scenario and it's calculations
   * @param calculatorOptions calculator options (if null the default worker options will be used)
   * @param jobIdentifier
   * @return new CalculationJob object
   * @throws SQLException database errors
   * @throws AeriusException aerius errors
   */
  public static CalculationJob initCalculation(final Connection con, final CalculationInputData inputData,
      final ScenarioCalculations scenarioCalculations, final CalculatorOptions calculatorOptions, final JobIdentifier jobIdentifier)
          throws SQLException, AeriusException {
    final String name = inputData.getName();
    final String queueName = inputData.getQueueName();
    final boolean customPointsInDatabase = inputData.isCustomPointsInDatabase();
    final boolean subPointsinDatabase = inputData.isSubPointsInDatabase();
    final ExportType exportType = inputData.getExportType();

    sanityCheck(scenarioCalculations);
    sanityCheckOptions(scenarioCalculations.getOptions());
    sanityCheck2(scenarioCalculations.getScenario(), calculatorOptions.getMaxEngineSources());

    final CalculationJob calculationJob = new CalculationJob(jobIdentifier, name, scenarioCalculations, exportType, queueName);
    initCustomPoints(calculationJob);
    fixSubstances(calculationJob.getCalculationSetOptions());
    calculationJob.setCalculatorOptions(calculatorOptions);
    calculationJob.setExpire(inputData.getExpire());
    // start inserting in database after sanity checks are not triggered.
    CalculationRepository.insertCalculations(con, scenarioCalculations, customPointsInDatabase, subPointsinDatabase);
    JobRepository.attachCalculations(con, calculationJob.getJobIdentifier().getJobKey(), calculationJob.getScenarioCalculations().getCalculations());
    return calculationJob;
  }

  /**
   * @param options
   */
  private static void fixSubstances(final CalculationSetOptions options) {
    // ensure stuff like NOXNH3 is taken care of.
    final ArrayList<EmissionValueKey> keys = EmissionValueKey.getEmissionValueKeys(0, options.getSubstances());
    options.getSubstances().clear();
    for (final EmissionValueKey key : keys) {
      options.getSubstances().add(key.getSubstance());
    }
    final Set<EmissionResultKey> resultKeys = EnumSet.noneOf(EmissionResultKey.class);
    for (final EmissionResultKey key : options.getEmissionResultKeys()) {
      resultKeys.addAll(key.hatch());
    }
    options.getEmissionResultKeys().clear();
    options.getEmissionResultKeys().addAll(resultKeys);
  }

  private static void sanityCheck(final ScenarioCalculations scenarioCalculations) throws AeriusException {
    for (final Calculation calculation : scenarioCalculations.getCalculations()) {
      if (calculation.getSources().isEmpty()) {
        final String name = scenarioCalculations.getScenario().getMetaData().getProjectName();

        throw new AeriusException(AeriusExceptionReason.CALCULATION_NO_SOURCES, name == null ? "" : name);
      }
    }
  }

  private static void sanityCheckOptions(final CalculationSetOptions calculationSetOptions) throws AeriusException {
    // extra sanity check for substance/emission result keys
    if (calculationSetOptions.getSubstances().isEmpty() || calculationSetOptions.getEmissionResultKeys().isEmpty()) {
      LOG.error("Substances and emissionResultKeys in options should both be filled. Substances {}, ERKS {}.", calculationSetOptions.getSubstances(),
          calculationSetOptions.getEmissionResultKeys());
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  /**
   * After initialization check if everything is ok.
   *
   * @param calculationJob
   * @param maxEngineSources
   * @throws AeriusException
   */
  private static void sanityCheck2(final Scenario scenario, final int maxEngineSources) throws AeriusException {
    final String projectName = scenario.getMetaData().getProjectName();
    final String safeProjectName = projectName == null || projectName.isEmpty() ? "" : projectName;
    for (final ScenarioSituation situation : scenario.getSituations()) {
      if (situation.getEmissionSourcesList().isEmpty()) {
        throw new AeriusException(AeriusExceptionReason.CALCULATION_NO_SOURCES, safeProjectName);
      } else if (maxEngineSources > 0 && situation.getEmissionSourcesList().size() > maxEngineSources) {
        throw new AeriusException(AeriusExceptionReason.CALCULATION_TO_COMPLEX, safeProjectName);
      }
    }
  }

  private static void initCustomPoints(final CalculationJob calculationJob) throws AeriusException {
    final CalculationSetOptions options = calculationJob.getCalculationSetOptions();
    final List<CalculationPointFeature> calculationPoints = calculationJob.getScenarioCalculations().getCalculationPoints();

    if (options.getCalculationMethod() == CalculationMethod.CUSTOM_POINTS || options.getCalculationMethod() == CalculationMethod.CUSTOM_RECEPTORS) {
      if (calculationPoints.isEmpty()) {
        throw new AeriusException(AeriusExceptionReason.IMPORT_NO_CALCULATION_POINTS_PRESENT);
      }
      LOG.debug("Calculating user defined calculation points.");
    }

    final long uniqueCalculationPointIds = calculationPoints.stream()
        .mapToInt(point -> point.getProperties().getId())
        .distinct()
        .count();
    if (uniqueCalculationPointIds != calculationPoints.size()) {
      throw new AeriusException(AeriusExceptionReason.CALCULATION_DUPLICATE_POINT_IDS);
    }
  }
}
