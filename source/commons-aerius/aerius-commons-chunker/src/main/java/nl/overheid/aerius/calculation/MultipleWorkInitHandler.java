/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.calculation.base.WorkInitHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Implementation of {@link WorkInitHandler} that iterates over multiple handlers.
 */
class MultipleWorkInitHandler<P extends AeriusPoint> implements WorkInitHandler<P> {

  private final List<WorkInitHandler<P>> handlers = new ArrayList<>();

  @Override
  public void init(final CalculationJob calculationJob, final List<WorkPacket<P>> packets) throws AeriusException {
    for (final WorkInitHandler<P> handler : handlers) {
      handler.init(calculationJob, packets);
    }
  }

  public void addHandler(final WorkInitHandler<P> handler) {
    handlers.add(handler);
  }
}
