/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * Data class storing results for all groups in a single point
 */
public class GroupResultPoint extends AeriusPoint {

  private static final long serialVersionUID = 2L;

  /**
   * Map<EngineDataKey, Map<Group Id, EmissionResults>>
   */
  private final Map<EngineDataKey, Map<Integer, EmissionResults>> groupResults = new HashMap<>();
  private int size;

  private final int numberOfSubPointsUsed;

  /**
   * Creates a new point for the given point location
   * @param ap point location
   */
  public GroupResultPoint(final AeriusResultPoint ap) {
    super(ap.getId(), ap.getParentId(), ap.getPointType(), ap.getX(), ap.getY(), ap.getHeight());
    numberOfSubPointsUsed = ap.getNumberOfSubPointsUsed();
  }

  public AeriusResultPoint getTotalsResultPoint(
      final BiFunction<AeriusResultPoint, Map<EngineDataKey, Map<Integer, EmissionResults>>, AeriusResultPoint> aggregateResultsFunction) {
    final AeriusResultPoint arp = new AeriusResultPoint(this);

    arp.setNumberOfSubPointsUsed(numberOfSubPointsUsed);
    return aggregateResultsFunction.apply(arp, groupResults);
  }

  /**
   * Adds the results for the given group. The caller should guarantee the results are actually for the location of this point
   * @param calculationEngine
   * @param groupId group Id the results are for
   * @param emissionResults emission results for group
   */
  public void put(final EngineDataKey engineDataKey, final int groupId, final EmissionResults emissionResults) {
    synchronized (this) {
      groupResults.computeIfAbsent(engineDataKey, ce -> new HashMap<>()).put(groupId, emissionResults);
      size++;
    }
  }

  /**
   * Returns the number of groups results are put in this object.
   * @return number of groups results are put in this object.
   */
  public int size() {
    return size;
  }

  @Override
  public String toString() {
    return "GroupResultPoint [groupResults=" + groupResults + "]";
  }
}
