/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.TaskCancelListener;
import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.aerius.taskmanager.client.TaskMultipleResultCallback;
import nl.overheid.aerius.calculation.EngineInputData.ChunkStats;
import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.ModelProgressReport;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.ExportResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * {@link CalculationTaskHandler} to send tasks to the queue.
 */
class RemoteCalculationTaskHandler implements CalculationTaskHandler, TaskMultipleResultCallback {

  private static final Logger LOGGER = LoggerFactory.getLogger(RemoteCalculationTaskHandler.class);

  private final TaskManagerClientSender taskManagerClient;
  private final ResultHandler resultHandler;
  private final String queueName;

  public RemoteCalculationTaskHandler(final TaskManagerClientSender taskManagerClient, final ResultHandler resultHandler, final String queueName) {
    this.taskManagerClient = taskManagerClient;
    this.resultHandler = resultHandler;
    this.queueName = queueName;
  }

  @Override
  public <E extends EngineSource, T extends CalculationTask<E, ?, ?>> void work(final WorkKey workKey, final T task,
      final Collection<AeriusPoint> points) throws InterruptedException, TaskCancelledException {
    final String taskId = resultHandler.registerTask(workKey);
    final String correlationId = workKey.getJobIdentifier().getJobKey();

    try {
      final EngineInputData<?, AeriusPoint> taskInput = (EngineInputData<?, AeriusPoint>) task.getTaskInput();
      taskInput.setReceptors(points);
      taskInput.setChunkStats(new ChunkStats(taskId, WorkPacket.nrOfPointsToCalculate(points)));
      taskManagerClient.sendTask(taskInput, correlationId, taskId, this, task.getWorkerType().type(), queueName);
    } catch (final IOException e) {
      onFailure(e, correlationId, taskId);
    }
  }

  @Override
  public void onSuccess(final Object value, final String correlationId, final String messageId) {
    if (value instanceof final CalculationResult cr) {
      resultHandler.onResult(messageId, cr.getEngineDataKey(), cr.getResults());
      // Also call the ModelProgressReport to make sure we certainly don't miss any progress reports,
      // in case the last intermediate update wasn't sent.
      resultHandler.onModelProgressReport(messageId, new ModelProgressReport(messageId, cr.getChunkStats().nrOfPointsToCalculate()));
    } else if (value instanceof final ExportResult er) {
      resultHandler.onExport(messageId, er.getFileUrl());
    } else if (value instanceof final ModelProgressReport mpr) {
      resultHandler.onModelProgressReport(messageId, mpr);
    } else {
      LOGGER.error("[correlationId:{}] Unknown calculation result retrieved: {}", correlationId, value);
      onFailure(new AeriusException(AeriusExceptionReason.INTERNAL_ERROR), correlationId, messageId);
    }
  }

  @Override
  public void onFailure(final Exception exception, final String correlationId, final String messageId) {
    resultHandler.onFailure(messageId, exception);
  }

  @Override
  public boolean isFinalResult(final Object value) {
    return !(value instanceof ModelProgressReport);
  }

  @Override
  public void setTaskCancelListener(final TaskCancelListener listener) {
    // Not used.
  }
}
