/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import nl.overheid.aerius.calculation.domain.ThemeCalculationJobOptions;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;

/**
 * OwN2000 calculation job specific options.
 */
class OwN2000CalculationJobOptions implements ThemeCalculationJobOptions {

  private OPSVersion opsVersion;
  private AeriusSRMVersion srmVersion;

  public OPSVersion getOpsVersion() {
    return opsVersion;
  }

  public void setOpsVersion(final OPSVersion opsVersion) {
    this.opsVersion = opsVersion;
  }

  public AeriusSRMVersion getSrmVersion() {
    return srmVersion;
  }

  public void setSrmVersion(final AeriusSRMVersion srmVersion) {
    this.srmVersion = srmVersion;
  }
}
