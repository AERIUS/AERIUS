/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.natura2k;

import java.util.EnumSet;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.SourcesUtil;

/**
 * Geometry emissions source tree builder to build different trees, one with all sources and one with off site reduction filtered.
 */
public final class EmissionSourceTreeBuilder {

  private static final Predicate<ScenarioSituation> ACCEPT_ALL_FILTER = s -> true;
  /**
   * Relevant situations that should be included to compute the points to calculate.
   */
  private static final EnumSet<SituationType> RELEVANT_SITUATIONS = EnumSet.of(SituationType.REFERENCE, SituationType.TEMPORARY,
      SituationType.PROPOSED);

  private final long countNoneRelevantSituations;
  private final List<ScenarioSituation> situations;

  private EmissionSourceListSTRTree sourcesRelevantSituationsTree;
  private EmissionSourceListSTRTree allSourcesTree;

  private EmissionSourceTreeBuilder(final List<ScenarioSituation> situations) {
    this.situations = situations;
    countNoneRelevantSituations = situations.stream().filter(s -> !isRevelantSituation(s)).count();
  }

  private static boolean isRevelantSituation(final ScenarioSituation situation) {
    return RELEVANT_SITUATIONS.contains(situation.getType());
  }

  /**
   * Returns an initialized builder object with all sources and with only relevant situations sources tree created.
   *
   * @param situations All situations
   * @return builder
   * @throws AeriusException
   */
  public static EmissionSourceTreeBuilder build(final List<ScenarioSituation> situations) throws AeriusException {
    final EmissionSourceTreeBuilder emissionSourceTreeBuilder = new EmissionSourceTreeBuilder(situations);

    return emissionSourceTreeBuilder.buildTrees();
  }

  private EmissionSourceTreeBuilder buildTrees() throws AeriusException {
    allSourcesTree = buildTree(situations, ACCEPT_ALL_FILTER);
    // If only off site reduction/combination than we need the sources of Off Site Reduction/Combination,
    // otherwise off site reduction/combination will be filtered out in determining the calculation points.
    if (countNoneRelevantSituations == 0 || situations.size() == countNoneRelevantSituations) {
      sourcesRelevantSituationsTree = allSourcesTree;
    } else {
      sourcesRelevantSituationsTree = buildTree(situations, EmissionSourceTreeBuilder::isRevelantSituation);
    }
    return this;
  }

  /**
   * @return Returns a sources tree with all emission sources
   */
  public EmissionSourceListSTRTree getAllSourcesTree() {
    return allSourcesTree;
  }

  /**
   * @return Returns a sources tree without off site reduction/combination scenarios (unless all situations are off site reduction/combination)
   */
  public EmissionSourceListSTRTree getRelevantSituationSourcesTree() {
    return sourcesRelevantSituationsTree;
  }

  private EmissionSourceListSTRTree buildTree(final List<ScenarioSituation> situations,
      final Predicate<ScenarioSituation> filter) throws AeriusException {
    final List<List<EmissionSourceFeature>> emissionSources = situations.stream()
        .filter(filter::test)
        .map(ScenarioSituation::getEmissionSourcesList)
        .map(SourcesUtil::filterSourcesWithEmission)
        .collect(Collectors.toList());

    return EmissionSourceListSTRTree.buildTree(emissionSources);
  }

}
