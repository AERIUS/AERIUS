/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanKind;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor;
import nl.overheid.aerius.archive.ArchiveProxy;
import nl.overheid.aerius.calculation.base.CalculatorThemeFactory;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.theme.nca.NCACalculatorFactory;
import nl.overheid.aerius.calculation.theme.own2000.OwN2000CalculatorFactory;
import nl.overheid.aerius.calculation.theme.rbl.RBLCalculatorFactory;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.otel.TracerUtil;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.ScenarioObjectMapperUtil;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Director class that constructs a complete calculator object. This class should be instantiated only once preferable.
 *
 * Director constructs a calculator object that is completely initialized. Then the calculator can be started which is a blocking process.
 */
public class CalculatorBuildDirector {

  private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorBuildDirector.class);
  private static final ObjectMapper OBJECT_MAPPER = ScenarioObjectMapperUtil.getScenarioObjectMapper();

  private final ScheduledExecutorService executor;
  private final PMF pmf;
  private final RabbitMQWorkerMonitor rabbitMQWorkerMonitor;
  private final TaskManagerClientSender taskManagerClient;
  private final CalculatorThemeFactory rblCalculatorThemeFactory;
  private final CalculatorThemeFactory own2000CalculatorThemeFactory;
  private final NCACalculatorFactory ncaCalculatorThemeFactory;

  /**
   * Constructor.
   *
   * @param pmf
   * @param factory
   * @param nrOfWorkers
   * @param archiveProxy
   * @throws SQLException
   * @throws IOException
   * @throws AeriusException
   */
  public CalculatorBuildDirector(final PMF pmf, final BrokerConnectionFactory factory, final int nrOfWorkers,
      final ArchiveProxy archiveProxy) throws SQLException, IOException, AeriusException {
    this(pmf, factory, ConstantRepository.getEnum(pmf, ConstantsEnum.CUSTOMER, AeriusCustomer.class), nrOfWorkers, archiveProxy,
        new RabbitMQWorkerMonitor(factory));
  }

  /**
   * Constructor to be used for testing only. It requires a custom {@link RabbitMQWorkerMonitor} to be passed.
   *
   * @param pmf
   * @param factory
   * @param customer
   * @param nrOfWorkers
   * @param archiveProxy proxy to Archive service
   * @param rabbitMQWorkerMonitor
   * @throws SQLException
   * @throws IOException
   * @throws AeriusException
   */
  public CalculatorBuildDirector(final PMF pmf, final BrokerConnectionFactory factory, final AeriusCustomer customer, final int nrOfWorkers,
      final ArchiveProxy archiveProxy, final RabbitMQWorkerMonitor rabbitMQWorkerMonitor)
      throws SQLException, IOException, AeriusException {
    this.pmf = pmf;

    // Initialize factories based on customer type. to avoid initializing unavailable theme factories.
    rblCalculatorThemeFactory = customer == AeriusCustomer.RIVM ? new RBLCalculatorFactory(pmf) : null;
    own2000CalculatorThemeFactory = customer == AeriusCustomer.RIVM ? new OwN2000CalculatorFactory(pmf) : null;
    ncaCalculatorThemeFactory = customer == AeriusCustomer.JNCC ? new NCACalculatorFactory(pmf, archiveProxy) : null;
    taskManagerClient = new TaskManagerClientSender(factory);
    this.rabbitMQWorkerMonitor = rabbitMQWorkerMonitor;
    // Each process uses 2 scheduled threads. Therefore start scheduler with 2 times number of processes.
    executor = Executors.newScheduledThreadPool(nrOfWorkers * 2);
    rabbitMQWorkerMonitor.start();
  }

  public void shutdown() {
    try {
      rabbitMQWorkerMonitor.shutdown();
    } finally {
      executor.shutdown();
    }
  }

  /**
   * Constructs a {@link Calculator} object.
   * @param inputData input data to construct the calculator for
   * @param jobIdentifier job identifier
   * @return initialized Calculator object
   * @throws AeriusException
   * @throws SQLException
   */
  public Calculator construct(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws AeriusException, SQLException {
    final Span initSpan = TracerUtil.getSpanBuilder(jobIdentifier.getJobKey(), "aerius.chunker.init", SpanKind.INTERNAL).startSpan();

    try {
      final CalculatorBuilder builder = init(inputData, jobIdentifier).init(inputData.getExportType(), inputData.getCreationDate());

      setOtelAttributes(inputData, initSpan);

      builder.setTrackJobHandler();
      return builder.build(rabbitMQWorkerMonitor, taskManagerClient, WorkerType.CONNECT);
    } finally {
      initSpan.end();
    }
  }

  private static void setOtelAttributes(final CalculationInputData inputData, final Span initSpan) {
    initSpan.setAttribute("chunker.situations", inputData.getScenario().getSituations().size());
    initSpan.setAttribute("chunker.sources",
        inputData.getScenario().getSituations().stream().mapToInt(s -> s.getSources().getFeatures().size()).sum());
    try {
      initSpan.setAttribute("chunker.calculator.options", OBJECT_MAPPER.writeValueAsString(inputData.getScenario().getOptions()));
    } catch (final JsonProcessingException e) {
      LOGGER.error("Error setting otel attribute 'chunker.calculator.options'", e);
    }
  }

  /**
   * Initializes a CalculationJob, inserts the calculation in the database.
   */
  private CalculatorBuilder init(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws AeriusException {
    LOGGER.info("Init calculation of {}", jobIdentifier);
    final CalculatorThemeFactory themeFactory = getCalculatorThemeFactory(inputData.getScenario().getTheme());
    final Scenario scenario = inputData.getScenario();
    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);

    themeFactory.prepareOptions(scenarioCalculations);
    final CalculationJob calculationJob;

    try (final Connection con = pmf.getConnection()) {
      final CalculatorOptions calculatorOptions = themeFactory.getCalculatorOptions(con, inputData);
      calculationJob = InitCalculation.initCalculation(con, inputData, scenarioCalculations, calculatorOptions, jobIdentifier);
      calculationJob.setThemeCalculationJobOptions(themeFactory.getThemeCalculationJobOptions(inputData, calculationJob));
    } catch (final SQLException e) {
      LOGGER.error("SQLException while trying to init calculation for {}", jobIdentifier, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    calculationJob.setProvider(themeFactory.getProvider(calculationJob));

    return new CalculatorBuilder(pmf, executor, themeFactory, calculationJob);
  }

  protected CalculatorThemeFactory getCalculatorThemeFactory(final Theme theme) {
    return switch (theme) {
      case RBL -> rblCalculatorThemeFactory;
      case NCA -> ncaCalculatorThemeFactory;
      case OWN2000 -> own2000CalculatorThemeFactory;
      default -> own2000CalculatorThemeFactory;
    };
  }
}
