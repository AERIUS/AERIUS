/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.GroupResultPoint;
import nl.overheid.aerius.calculation.domain.GroupResults;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Queue to collected results from multiple calculations. When the multiple calculations are done for the same receptor set this queue keeps track if
 * the results for all sectors are available for a specific receptor. If that is the case the result is stored in the completed queue. The calculating
 * process can then pull these results from the queue.
 */
public class AggregatedCalculationResultQueue<T> implements CalculationResultQueue {

  /**
   * Map<Calculation Id, SectorResults>
   */
  private final Map<Integer, GroupResults<T>> situationResults = new HashMap<>();
  /**
   * Map<Calculation Id, Queue<SectorsResultPoint>>
   */
  private final Map<Integer, Queue<AeriusResultPoint>> completeResults = new HashMap<>();

  private final Function<AeriusResultPoint, T> result2KeyFunction;

  /**
   * Constructor
   * @param result2KeyFunction
   */
  public AggregatedCalculationResultQueue(final Function<AeriusResultPoint, T> result2KeyFunction) {
    this.result2KeyFunction = result2KeyFunction;
  }

  /**
   * Initializes the queue with all calculations and sectors per calculation.
   */
  @Override
  public void init(final CalculationJob calculationJob, final List<WorkPacket<AeriusPoint>> packets) {
    if (!packets.isEmpty()) {
      final Map<Integer, List<JobPacket>> map = packets.get(0).getJobPackets().stream().collect(Collectors.groupingBy(JobPacket::getCalculationId));

      for (final Entry<Integer, List<JobPacket>> workPacket : map.entrySet()) {
        addCalculation(workPacket.getKey(), workPacket.getValue().stream().mapToInt(p -> p.getSourcesPackets().size()).sum());
      }
    }
  }

  /**
   * Adds a calculation for the set of sectors that will be calculated.
   *
   * @param calculationId id of the calculation
   * @param sectors set of the sectors that will be calculated
   */
  private void addCalculation(final int calculationId, final int groupCount) {
    situationResults.put(calculationId, new GroupResults<>(groupCount, result2KeyFunction));
    completeResults.put(calculationId, new ArrayDeque<>());
  }

  /**
   * Clears all data. Only use when in case of canceling of job.
   */
  @Override
  public void clear() {
    situationResults.clear();
    completeResults.clear();
  }

  /**
   * Returns true if there results in the complete queue.
   *
   * @param calculationId the calculation id to check
   * @return true if results are present
   */
  public synchronized boolean hasResults(final int calculationId) {
    final Queue<AeriusResultPoint> queue = completeResults.get(calculationId);

    return queue != null && !queue.isEmpty();
  }

  /**
   * Polls the completed queue for total results. If no results are present null is returned.
   *
   * @param calculationId the calculation to get the results for
   * @return null if no results else a result object
   */
  @Override
  public ArrayList<AeriusResultPoint> pollTotalResults(final int calculationId) {
    final ArrayList<AeriusResultPoint> results = new ArrayList<>();

    synchronized (this) {
      final Queue<AeriusResultPoint> queue = completeResults.get(calculationId);

      while (queue != null && !queue.isEmpty()) {
        results.add(queue.poll());
      }
    }
    return results;
  }

  /**
   * Puts the calculation results by calculation id and sector id into the queue and calculates for each point if all results are present, and if so
   * places the result on the completed queue.
   *
   * @param engineDataKey engine data key identifier
   * @param results calculation result
   * @param calculationId id of the calculation the results are from
   * @throws AeriusException
   */
  @Override
  public void put(final EngineDataKey engineDataKey, final Map<Integer, List<AeriusResultPoint>> results, final int calculationId)
      throws AeriusException {
    final GroupResults<T> sr = situationResults.get(calculationId);
    Objects.requireNonNull(sr, "CalculationResultQueue not initialized for calculationId:" + calculationId);

    synchronized (this) {
      for (final Entry<Integer, List<AeriusResultPoint>> entry : results.entrySet()) {
        for (final AeriusResultPoint result : entry.getValue()) {
          putInCompleteIfDone(calculationId, sr.put(engineDataKey, entry.getKey(), result));
        }
      }
    }
  }

  private void putInCompleteIfDone(final int calculationId, final GroupResultPoint grp) {
    if (grp != null) {
      completeResults.get(calculationId).add(grp.getTotalsResultPoint(this::aggregateCompleteResult));
    }
  }

  protected AeriusResultPoint aggregateCompleteResult(final AeriusResultPoint arp,
      final Map<EngineDataKey, Map<Integer, EmissionResults>> allResults) {
    for (final Entry<EngineDataKey, Map<Integer, EmissionResults>> engineResult : allResults.entrySet()) {
      for (final Entry<Integer, EmissionResults> groupResult : engineResult.getValue().entrySet()) {
        for (final Entry<EmissionResultKey, Double> emission : groupResult.getValue().entrySet()) {
          final Double value = emission.getValue();

          if (!Double.isNaN(value)) {
            arp.setEmissionResult(emission.getKey(), arp.getEmissionResult(emission.getKey()) + value);
          }
        }
      }
    }
    return arp;
  }

  @Override
  public synchronized boolean isExpectingMoreResults(final int calculationId) {
    return Optional.ofNullable(situationResults.get(calculationId)).map(GroupResults::isExpectingMoreResults).orElse(false);
  }
}
