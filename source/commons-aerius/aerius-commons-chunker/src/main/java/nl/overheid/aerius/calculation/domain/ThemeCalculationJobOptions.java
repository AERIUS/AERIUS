/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.Locale;

/**
 * Sub classes implement theme specific calculation options.
 */
public interface ThemeCalculationJobOptions {

  /**
   * @return Returns the calculation method name for metric reporting.
   */
  default String getCalculationMethodMetricName(final CalculationJob calculationJob) {
    return calculationJob.getScenarioCalculations().getScenario().getOptions().getCalculationMethod().name().toLowerCase(Locale.ROOT);
  }
}
