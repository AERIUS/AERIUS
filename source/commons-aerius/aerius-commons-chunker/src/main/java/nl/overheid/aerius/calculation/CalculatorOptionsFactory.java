/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;

import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;

/**
 * Factory class to initialize {@link CalculatorOptions} with default values.
 */
public final class CalculatorOptionsFactory {

  private CalculatorOptionsFactory() {
    // util
  }

  /**
   * Returns the Calculator options for UI calculations.
   *
   * @param con The connection to use when retrieving options for UI.
   * @param calculationSetOptions
   * @return Options for UI (webserver) to use when calculating concentrations/depositions.
   * @throws SQLException In case of database errors.
   */
  public static CalculatorOptions initUICalculatorOptions(final Connection con) throws SQLException {
    final CalculatorOptions calculatorOptions = new CalculatorOptions();

    calculatorOptions.setMaxCalculationEngineUnits(ConstantRepository.getInteger(con, ConstantsEnum.CHUNKER_ENGINE_UNITS_UI_MAX));
    calculatorOptions.setMinReceptors(ConstantRepository.getInteger(con, ConstantsEnum.CHUNKER_RECEPTORS_UI_MIN));
    calculatorOptions.setMaxReceptors(ConstantRepository.getInteger(con, ConstantsEnum.CHUNKER_RECEPTORS_UI_MAX));
    calculatorOptions.setMaxEngineSources(ConstantRepository.getInteger(con, ConstantsEnum.MAX_ENGINE_SOURCES_UI));
    return calculatorOptions;
  }

  /**
   * Returns the Calculator options for non UI calculations.
   *
   * @param con The connection to use when querying the database.
   * @return Options for a worker to use when calculating concentrations/depositions.
   * @throws SQLException In case of database errors.
   */
  public static CalculatorOptions initWorkerCalculationOptions(final Connection con) throws SQLException {
    final CalculatorOptions calculatorOptions = new CalculatorOptions();

    calculatorOptions.setMaxCalculationEngineUnits(ConstantRepository.getInteger(con, ConstantsEnum.CHUNKER_ENGINE_UNITS_WORKER_MAX));
    calculatorOptions.setMaxReceptors(ConstantRepository.getInteger(con, ConstantsEnum.CHUNKER_RECEPTORS_WORKER_MAX));
    calculatorOptions.setMinReceptors(ConstantRepository.getInteger(con, ConstantsEnum.CHUNKER_RECEPTORS_WORKER_MIN));
    return calculatorOptions;
  }

  /**
   * Returns the Calculator options for Quick Run calculations.
   *
   * @param con The connection to use when querying the database.
   * @return Options for a worker to use when calculating concentrations/depositions.
   * @throws SQLException In case of database errors.
   */
  public static CalculatorOptions initQuickRunCalculationOptions(final Connection con) throws SQLException {
    final CalculatorOptions calculatorOptions = new CalculatorOptions();

    calculatorOptions.setMaxCalculationEngineUnits(ConstantRepository.getInteger(con, ConstantsEnum.CHUNKER_ENGINE_UNITS_QUICKRUN_MAX));
    calculatorOptions.setMaxReceptors(ConstantRepository.getInteger(con, ConstantsEnum.CHUNKER_RECEPTORS_QUICKRUN_MAX));
    calculatorOptions.setMinReceptors(ConstantRepository.getInteger(con, ConstantsEnum.CHUNKER_RECEPTORS_QUICKRUN_MIN));
    return calculatorOptions;
  }
}
