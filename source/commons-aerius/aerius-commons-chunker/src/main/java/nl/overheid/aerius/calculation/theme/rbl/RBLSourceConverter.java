/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.conversion.SourceConverter;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.conversion.SRM1SRMConverter;
import nl.overheid.aerius.srm2.conversion.SRMSourceConverter;

class RBLSourceConverter implements SourceConverter {

  private final SRMSourceConverter converter;
  private final Integer groupId;
  private final RBLSRM1Preprocessor preProcessor;

  RBLSourceConverter(final Integer sectorId, final Calculation calculation) throws AeriusException {
    this.groupId = sectorId;
    this.preProcessor = new RBLSRM1Preprocessor(calculation);
    this.converter = new SRMSourceConverter(new SRM1SRMConverter());
  }

  @Override
  public void preProcess(final ScenarioSituation situation) throws AeriusException {
    for (final EmissionSourceFeature feature : situation.getSources().getFeatures()) {
      if (feature.getProperties() instanceof SRM1RoadEmissionSource && feature.getGeometry() instanceof LineString) {
        preProcessor.addMeasures((LineString) feature.getGeometry(), feature.getProperties());
      }
    }
  }

  @Override
  public boolean convert(final Connection con, final GroupedSourcesPacketMap map, final EmissionSource originalSource, final Geometry geometry,
      final List<Substance> substances) throws AeriusException {
    final Collection<EngineSource> expanded = new ArrayList<>();
    final boolean converted = converter.convert(expanded, originalSource, geometry, substances);

    if (converted) {
      map.addAll(groupId, CalculationEngine.ASRM2, expanded);
    }
    return converted;
  }
}
