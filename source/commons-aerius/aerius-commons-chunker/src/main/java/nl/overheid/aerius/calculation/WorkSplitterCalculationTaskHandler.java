/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Splits work into multiple tasks if the task size exceeds a max OPS units constant. OPS units are calculated as the number of sources multiplied
 * with the number of result points. The splitting is calculated such that an optimal splitting is obtained. Meaning no tasks should be very small
 * compared to other tasks.
 *
 * @deprecated The {@link WorkDistributor} should take care of splitting the tasks and then this class is not needed anymore.
 */
@Deprecated
public class WorkSplitterCalculationTaskHandler implements CalculationTaskHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(WorkSplitterCalculationTaskHandler.class);

  private final CalculationTaskHandler wrappedHandler;
  private final int maxOpsUnits;
  private final int minPoints;


  public WorkSplitterCalculationTaskHandler(final CalculationTaskHandler wrappedHandler, final int maxOpsUnits, final int minPoints) {
    this.wrappedHandler = wrappedHandler;
    this.maxOpsUnits = maxOpsUnits;
    this.minPoints = minPoints;
  }

  @Override
  public <E extends EngineSource, T extends CalculationTask<E, ?, ?>> void work(final WorkKey workKey, final T task,
      final Collection<AeriusPoint> points) throws AeriusException, InterruptedException, TaskCancelledException {
    final int sourcesSize = task.getTaskInput().getSourcesSize();
    final int pointsSize = points.size();

    LOGGER.trace("[calculationId:{}] sources:{}, receptors:{}, maxOpsUnits:{}", workKey.getCalculationId(), sourcesSize, pointsSize, maxOpsUnits);
    if (doSplitPointSize(sourcesSize, pointsSize)) {
      LOGGER.trace("[calculationId:{}] Split work", workKey.getCalculationId());
      splitWork(workKey, task, points, calcSplitPointSize(sourcesSize, pointsSize));
    } else {
      LOGGER.trace("[calculationId:{}] No splitsing of work", workKey.getCalculationId());
      wrappedHandler.work(workKey, task, points);
    }
  }

  private boolean doSplitPointSize(final int sourcesSize, final int pointsSize) {
    return calculateOpsUnits(sourcesSize, pointsSize) > maxOpsUnits;
  }

  private long calculateOpsUnits(final long sourcesSize, final long pointsSize) {
    return sourcesSize * pointsSize;
  }

  /**
   * Calculates the optimal amount of result points per calculation block. The optimal division means to have as few as possible tasks, while the
   * number of result points per task should be evenly divided. For example with 11 result points and OPS units calculation determines it can be done
   * in 3 tasks the spread of receptor points between the tasks should be 4, 4, 3. Even when based on the max OPS units the max number of result
   * points could be 5. Because when maxing out it would lead to an sub optimal spread of 5, 5, 1.
   *
   * The minimum number of calculated points has a lower limit defined by the variable minPoints.
   *
   * @param sourcesSize number of sources
   * @param pointsSize number of result points
   */
  private int calcSplitPointSize(final int sourcesSize, final int pointsSize) {
    return (int) Math.max(Math.ceil(pointsSize / Math.ceil(pointsSize / Math.ceil(maxOpsUnits / (double) sourcesSize))), minPoints);
  }

  private void splitWork(final WorkKey workKey, final CalculationTask<?, ?, ?> task, final Collection<AeriusPoint> points, final int splitPointsSize)
      throws AeriusException, InterruptedException, TaskCancelledException {
    final Iterator<AeriusPoint> iterator = points.iterator();
    int countSplits = 0;
    while (iterator.hasNext()) {
      final Collection<AeriusPoint> subPoints = new ArrayList<>();
      for (int i = 0; i < splitPointsSize && iterator.hasNext(); i++) {
        subPoints.add(iterator.next());
      }
      if (LOGGER.isTraceEnabled()) {
        countSplits++;
        LOGGER.trace("[calculationId:{}] Splitted receptors (count:{}): #{}", workKey.getCalculationId(), countSplits, subPoints.size());
      }
      wrappedHandler.work(workKey, task, subPoints);
    }
  }
}
