/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

import java.util.Collection;
import java.util.Map;

import nl.overheid.aerius.calculation.domain.GroupedSourcesKey;
import nl.overheid.aerius.calculation.grid.GridZoomLevel;
import nl.overheid.aerius.shared.domain.EngineSource;

/**
 * Interface for storing sources grouped by cells. A cell is determined by the implementation, but can be fixed square on the map.
 */
public interface SourcesStore {

  /**
   * Get all sources by sector that should be calculated given a cellId in a grid.
   * @param cellId id of the cell
   * @return map of sources by sector
   */
  Map<GroupedSourcesKey, Collection<EngineSource>> getSourcesFor(int cellId);

  /**
   * @param zoomLevel zoomlevel of the grid to get cells with sources for
   * @return Returns the cell ids that contain sources. A cell within the grid at the given zoom level.
   */
  Iterable<Integer> getSourceGridIds(GridZoomLevel zoomLevel);

}
