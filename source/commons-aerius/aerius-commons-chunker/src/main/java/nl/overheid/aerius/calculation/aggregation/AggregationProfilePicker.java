/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

/**
 * Maps an {@link AggregationProfile} with sectors.
 * @param <P> aggregation profile implementation
 */
interface AggregationProfilePicker<P extends AggregationProfile> {
  /**
   * Returns the aggregation profile for the given sector id.
   * @param sectorId sector id to get aggregation profile for
   * @param forceAggregation force aggregation for sectors which might not get aggregated by default
   * @return aggregation profile
   */
  P getAggregationProfile(int sectorId, boolean forceAggregation);
}
