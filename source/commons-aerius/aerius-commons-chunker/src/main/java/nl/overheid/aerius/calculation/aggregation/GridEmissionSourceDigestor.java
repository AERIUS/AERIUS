/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.domain.GroupedSourcesKey;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.calculation.grid.GridZoomLevel;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

/**
 * Takes a calculationjob, which contains job calculations, which contains sources.
 *
 * For each job calculation, segregate into sectors.
 *
 * For each segregated set, convert to engine sources.
 *
 * For each sector, stick in grid.
 *
 * For each grid cell, aggregate.
 */
public class GridEmissionSourceDigestor {
  private final class LevelizedGridMap extends AutoFillingHashMap<AggregationDistanceProfile, Map<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>>> {
    private static final long serialVersionUID = 1L;

    @Override
    protected SectorMap getEmptyObject() {
      return new SectorMap();
    }
  }

  private final class SectorMap extends AutoFillingHashMap<GroupedSourcesKey, LevelStore<GridZoomLevel, EngineSource>> {
    private static final long serialVersionUID = 4954101953510638025L;

    @Override
    protected LevelStore<GridZoomLevel, EngineSource> getEmptyObject() {
      return new GridLevelStore<>();
    }
  }

  private final GridUtil gridUtil;
  private final AggregationProfilePicker<AggregationDistanceProfile> profilePicker;
  private final EngineSourceAggregator<EngineSource, Substance> aggregator;

  public GridEmissionSourceDigestor(final GridUtil gridUtil, final EngineSourceAggregator<EngineSource, Substance> aggregator,
      final AggregationProfilePicker<AggregationDistanceProfile> aggregationProfilePicker) throws SQLException {
    this.gridUtil = gridUtil;
    this.aggregator = aggregator;
    this.profilePicker = aggregationProfilePicker;
  }

  /**
   *
   * @param cso
   * @param substances
   * @param expandedSources
   * @return
   * @throws SQLException
   * @throws AeriusException
   */
  public SourcesStore digest(final CalculationSetOptions cso, final List<Substance> substances, final List<GroupedSourcesPacket> expandedSources)
      throws SQLException, AeriusException {
    final LevelizedGridMap originals = new LevelizedGridMap();
    final LevelizedGridMap aggregated = new LevelizedGridMap();

    getEmissionSourceStore(originals, aggregated, expandedSources, cso.getSubstances(), cso.getOwN2000CalculationOptions().isForceAggregation());
    return new SourcesStoreImpl(gridUtil, originals, aggregated);
  }

  private void getEmissionSourceStore(final LevelizedGridMap originals, final LevelizedGridMap aggregated,
      final List<GroupedSourcesPacket> unprocessedOriginals, final List<Substance> substances, final boolean forceAggregation) {
    for (final GroupedSourcesPacket packet : unprocessedOriginals) {
      final GroupedSourcesKey groupKey = packet.getKey();
      final AggregationDistanceProfile profile = profilePicker.getAggregationProfile(groupKey.id(), forceAggregation);

      final LevelStore<GridZoomLevel, EngineSource> aggregatedLevelStore = aggregated.get(profile).get(groupKey);
      final LevelStore<GridZoomLevel, EngineSource> originalLevelStore = originals.get(profile).get(groupKey);
      final Iterable<EngineSource> sectorSources = packet.getSources();

      // Insert sources into original set.
      insertSourcesIntoLevels(originalLevelStore, sectorSources);
      insertSourcesAggregatedLevels(substances, aggregatedLevelStore, originalLevelStore);
    }
  }

  private void insertSourcesIntoLevels(final LevelStore<GridZoomLevel, EngineSource> originalLevelStore, final Iterable<EngineSource> sectorSources) {
    for (final EngineSource src : sectorSources) {
      final SridPoint pointFromSource = getPointFromSource(src);
      for (final GridZoomLevel level : gridUtil.getGridSettings().getZoomLevels()) {
        originalLevelStore.add(level, gridUtil.getCellFromPosition(pointFromSource, level), src);
      }
    }
  }

  private SridPoint getPointFromSource(final EngineSource src) {
    if (src instanceof OPSSource) {
      return ((OPSSource) src).getPoint();
    } else if (src instanceof SRM2RoadSegment) {
      final SRM2RoadSegment seg = (SRM2RoadSegment) src;
      return new SridPoint((seg.getStartX() + seg.getEndX()) / 2, (seg.getStartY() + seg.getEndY()) / 2);
    } else {
      throw new IllegalArgumentException("Unknown EngineSource encountered - cannot handle this. " + src);
    }
  }

  private void insertSourcesAggregatedLevels(final List<Substance> substances, final LevelStore<GridZoomLevel, EngineSource> aggregatedLevelStore,
      final LevelStore<GridZoomLevel, EngineSource> originalLevelStore) {
    for (final GridZoomLevel level : gridUtil.getGridSettings().getZoomLevels()) {
      // Aggregate sources for each cell in the original store, for this level
      for (final Entry<Integer, Collection<EngineSource>> originalEntry : originalLevelStore.getLevel(level).entrySet()) {
        aggregatedLevelStore.addAll(level, originalEntry.getKey(), aggregator.aggregate(originalEntry.getValue(), substances));
      }
    }
  }
}
