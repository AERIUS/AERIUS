/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepositoryBean;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Util class to store calculation statistics in the database.
 *
 * @param <P>
 */
public class WorkStatisticsUtil<P extends AeriusPoint> implements WorkInitHandler<P> {

  private static final Logger LOGGER = LoggerFactory.getLogger(WorkStatisticsUtil.class);

  private final PMF pmf;
  private final JobRepositoryBean jobRepositoryBean;
  private final CalculationRepositoryBean calculationRepositoryBean;

  public WorkStatisticsUtil(final PMF pmf) {
    this(pmf, new JobRepositoryBean(pmf), new CalculationRepositoryBean(pmf));
  }

  public WorkStatisticsUtil(final PMF pmf, final JobRepositoryBean jobRepositoryBean, final CalculationRepositoryBean calculationRepositoryBean) {
    this.pmf = pmf;
    this.jobRepositoryBean = jobRepositoryBean;
    this.calculationRepositoryBean = calculationRepositoryBean;
  }

  /**
   * Compute the statistics of a job. There are:
   * - Number of points to calculate
   * - Number of sources.
   *
   * @param calculationJob
   * @param packets
   * @throws SQLException
   */
  @Override
  public void init(final CalculationJob calculationJob, final List<WorkPacket<P>> packets) throws AeriusException {
    try (Connection con = pmf.getConnection()) {
      setNumberOfPoints(con, calculationJob.getJobIdentifier().getJobKey(), packets);
      setCalculationStats(con, packets, calculationJob.getSubstances().size());
    } catch (final SQLException e) {
      LOGGER.error("SQError setting calculation statistics", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private void setNumberOfPoints(final Connection con, final String jobKey, final List<WorkPacket<P>> packets) throws SQLException {
    final int totalNrOfPoints = countNrOfWorks(packets) * packets.stream().mapToInt(this::countUniquePoints).sum();

    jobRepositoryBean.setNumberOfPoints(con, jobKey, totalNrOfPoints);
  }

  private int countNrOfWorks(final List<WorkPacket<P>> packets) {
    final List<JobPacket> jobPackets = packets.isEmpty() ? List.of() : packets.get(0).getJobPackets();
    final long calculations = jobPackets.stream().mapToInt(JobPacket::getCalculationId).distinct().count();
    final long edkeys = jobPackets.stream().flatMap(sp -> sp.getSourcesPackets().stream().map(g -> g.getKey().engineDataKey())).distinct().count();

    return (int) (calculations * edkeys);
  }

  /**
   * Count only points that will actually be calculated. {@link IsSubPoint} points that are not of a sub type are used for aggregation of
   * deposition and not actually using in the calculation and therefore should not be counted as point to be calculated.
   *
   * @param packet
   * @return
   */
  private int countUniquePoints(final WorkPacket<P> packet) {
    return (int) packet.getPoints().stream().filter(p -> !(p instanceof IsSubPoint &&
        (p.getPointType() == AeriusPointType.RECEPTOR || p.getPointType() == AeriusPointType.POINT))).count();
  }

  private void setCalculationStats(final Connection con, final List<WorkPacket<P>> packets, final int numberOfSubstances) throws SQLException {
    if (!packets.isEmpty()) {
      for (final JobPacket jobPacket : packets.get(0).getJobPackets()) {
        final Integer calculationId = jobPacket.getCalculationId();
        final int numberOfSources = jobPacket.getSourcesPackets().stream().mapToInt(s -> s.getSources().size()).sum();

        calculationRepositoryBean.setNumberOfSources(con, calculationId, numberOfSources, numberOfSubstances);
      }
    }
  }
}
