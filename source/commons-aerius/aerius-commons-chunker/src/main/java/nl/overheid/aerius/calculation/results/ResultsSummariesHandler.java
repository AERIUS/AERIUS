/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.results;

import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.results.ResultsSummaryRepository;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link PostCalculationHandler} to insert result summaries when the calculation is finished.
 */
public class ResultsSummariesHandler implements PostCalculationHandler {

  private final ResultsSummaryRepository summaryRepository;
  private final JobRepositoryBean jobRepository;

  public ResultsSummariesHandler(final PMF pmf) {
    this.summaryRepository = new ResultsSummaryRepository(pmf);
    this.jobRepository = new JobRepositoryBean(pmf);
  }

  @Override
  public void onCompleted(final CalculationJob calculationJob) throws AeriusException {
    final int jobId = jobRepository.getJobId(calculationJob.getJobIdentifier().getJobKey());
    final SituationCalculations situationCalculations = jobRepository.getSituationCalculations(calculationJob.getJobIdentifier().getJobKey());
    summaryRepository.insertResultsSummaries(jobId, situationCalculations,
        calculationJob.getScenarioCalculations().getOptions().getCalculationJobType());
  }

}
