/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.mutable.MutableDouble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.UnitsPerSectorPerEngineAccumulator;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Collect summary results of a calculation and store the results in a file.
 */
public class SummaryCollector {

  private static final Logger LOGGER = LoggerFactory.getLogger(SummaryCollector.class);

  private static final String HEADER =
      "result_type;sector;substance;result_min;result_max;result_sum;result_average;source_count;total_emission;OPS;SRM2;result_filename";
  private static final String FILENAME_FORMAT = "%s.summary";
  private static final String DELIMITER = ";";

  private final Map<String, SectorSummary> sectorSummaries = new TreeMap<>();
  private final CalculationJob calculationJob;
  private final List<Substance> substances;
  private final long startTime = System.currentTimeMillis();

  public SummaryCollector(final CalculationJob calculationJob) {
    this.calculationJob = calculationJob;
    substances = calculationJob.getCalculationSetOptions().getSubstances();
  }

  /**
   * Initialize summary for a calculation id, sector id and {@link EmissionResultType} combination.
   *
   * @param calculationId calculation id
   * @param ert emission result type
   * @param filename name of the file results for calculation are stored in
   */
  public void addSummary(final int calculationId, final EmissionResultType ert, final String filename) {
    final Map<Integer, List<EmissionSourceFeature>> sectorSources = countSourcesBySector(calculationId);

    sectorSources.forEach((sectorId, sources) -> sectorSummaries.put(key(calculationId, sectorId, ert),
        new SectorSummary(sectorId, filename, sources.size(), summarizeEmissions(sources), calculationJob.getUnitsAccumulator())));
  }

  private Map<Integer, List<EmissionSourceFeature>> countSourcesBySector(final int calculationId) {
    final List<EmissionSourceFeature> esl = calculationJob.getScenarioCalculations().getSources(calculationId);
    return esl.stream().collect(Collectors.groupingBy(s -> s.getProperties().getSectorId()));
  }

  private Map<Substance, MutableDouble> summarizeEmissions(final List<EmissionSourceFeature> sectorSources) {
    final Map<Substance, MutableDouble> emissions = new EnumMap<>(Substance.class);

    substances.forEach(substance -> emissions.put(substance, new MutableDouble()));
    sectorSources.forEach(source -> substances
        .forEach(substance -> emissions.get(substance).add(source.getProperties().getEmissions().getOrDefault(substance, 0.0))));
    return emissions;
  }

  /**
   * Processes a single result.
   *
   * @param calculationId id of the calculation
   * @param sectorId sector this result is for
   * @param erk result type of the result
   * @param value result value
   * @throws AeriusException
   */
  public void addResult(final int calculationId, final int sectorId, final EmissionResultKey erk, final Double value) throws AeriusException {
    final SectorSummary sectorSummary = sectorSummaries.get(key(calculationId, sectorId, erk.getEmissionResultType()));
    if (sectorSummary == null) {
      LOGGER.error("Can't add results for ({}, {}, {}). SectorSummary size is {}.",
          calculationId, sectorId, erk.getEmissionResultType(), sectorSummaries.size());
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    } else {
      sectorSummary.addResult(erk, value);
    }
  }

  private String key(final int calculationId, final int sectorId, final EmissionResultType ert) {
    return calculationId + "-" + sectorId + "-" + ert.toDatabaseString();
  }

  /**
   * Writes the summary results to a file.
   *
   * @param directory directory to write the file to
   * @param name name of the file to write
   */
  public void writeSummary(final String directory, final String name) throws IOException {
    final Path openFile = Paths.get(directory, String.format(FILENAME_FORMAT, name));

    try (final PrintWriter writer = new PrintWriter(Files.newBufferedWriter(openFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE))) {
      writer.println(HEADER);
      sectorSummaries.forEach((s, ss) -> ss.write(writer));
    }
  }

  public String getDuration() {
    return String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime));
  }

  /**
   * Data class to collect summary results for a sector.
   */
  private static class SectorSummary {
    private final int sectorIdInt;
    private final String sectorId;
    private final String fileName;
    private final int sourceCount;
    private final Map<Substance, MutableDouble> emissions;
    private final Map<EmissionResultKey, ResultStats> results = new EnumMap<>(EmissionResultKey.class);
    private final UnitsPerSectorPerEngineAccumulator unitsAccumulator;

    public SectorSummary(final Integer sectorId, final String fileName, final int sourceCount, final Map<Substance, MutableDouble> emissions,
        final UnitsPerSectorPerEngineAccumulator unitsAccumulator) {
      this.sourceCount = sourceCount;
      this.emissions = emissions;
      this.sectorIdInt = sectorId;
      this.unitsAccumulator = unitsAccumulator;
      this.sectorId = String.valueOf(sectorId);
      this.fileName = fileName;
    }

    /**
     * Adds a new result for the given result key and increments the number of results with 1.
     * @param key result key
     * @param value value to add
     */
    public void addResult(final EmissionResultKey key, final double value) {
      if (!results.containsKey(key)) {
        results.put(key, new ResultStats());
      }
      results.get(key).add(value);
    }

    /**
     * Writes the summary results as rows to the given writer.
     * @param writer writer to append rows with summary results.
     */
    public void write(final PrintWriter writer) {
      results.forEach((k, v) -> writer.println(collect(k, v, emissions.get(k.getSubstance()).doubleValue())));
    }

    private String collect(final EmissionResultKey erk, final ResultStats results, final double emissionValue) {
      final List<String> values = new ArrayList<>();
      values.add(erk.getEmissionResultType().toDatabaseString());
      values.add(sectorId);
      values.add(erk.getSubstance().toDatabaseString());
      values.add(String.valueOf(results.min));
      values.add(String.valueOf(results.max));
      values.add(String.valueOf(results.sum));
      values.add(String.valueOf(results.getAverage()));
      values.add(String.valueOf(sourceCount));
      values.add(String.valueOf(emissionValue));
      values.add(String.valueOf(unitsAccumulator.getUnits(sectorIdInt, CalculationEngine.OPS)));
      values.add(String.valueOf(unitsAccumulator.getUnits(sectorIdInt, CalculationEngine.ASRM2)));
      values.add(fileName);
      return String.join(DELIMITER, values);
    }

    @Override
    public String toString() {
      return "SectorSummary [sectorId=" + sectorId + ", fileName=" + fileName + ", sourceCount=" + sourceCount + ", emissions=" + emissions
          + ", results=" + results + "]";
    }
  }

  private static class ResultStats {
    double min = Double.MAX_VALUE;
    double max;
    BigDecimal sum = BigDecimal.ZERO;
    int count;

    public void add(final double value) {
      min = Math.min(value, min);
      max = Math.max(value, max);
      sum = sum.add(BigDecimal.valueOf(value));
      count++;
    }

    public double getAverage() {
      return sum.divide(BigDecimal.valueOf(count), RoundingMode.HALF_UP).doubleValue();
    }

    @Override
    public String toString() {
      return "ResultStats [min=" + min + ", max=" + max + ", sum=" + sum + ", count=" + count + "]";
    }
  }
}
