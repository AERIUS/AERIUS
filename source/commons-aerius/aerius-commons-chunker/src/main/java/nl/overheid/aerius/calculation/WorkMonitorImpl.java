/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.base.WorkMonitor;
import nl.overheid.aerius.calculation.base.WorkSemaphore;
import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Implementation of {@link WorkMonitor}.
 */
class WorkMonitorImpl implements WorkMonitor, CalculationResultHandler, PostCalculationHandler {

  private final WorkSemaphore workSemaphore;
  private final Map<String, WorkKey> taskMap = new ConcurrentHashMap<>();
  private final WorkTracker tracker;

  /**
   * @param tracker Work tracker
   * @param workSemaphore Semaphore doing the actual managing the number of concurrent tasks.
   */
  WorkMonitorImpl(final WorkTracker tracker, final WorkSemaphore workSemaphore) {
    this.tracker = tracker;
    this.workSemaphore = workSemaphore;
  }

  @Override
  public void onCompleted(final CalculationJob calculationJob) throws AeriusException {
    drain();
  }

  @Override
  public void onCancelled() {
    drain();
  }

  @Override
  public void drain() {
    workSemaphore.drain();
    taskMap.clear();
  }

  @Override
  public void taskFailed(final String taskId, final Exception exception) {
    try {
      tracker.cancel(exception);
    } finally {
      drain();
    }
  }

  @Override
  public WorkKey getWorkKey(final String taskId) {
    return taskMap.get(taskId);
  }

  @Override
  public String registerTask(final WorkKey workKey) throws InterruptedException, TaskCancelledException {
    checkTaskCancelled();
    final String taskId = UUID.randomUUID().toString();
    taskMap.put(taskId, workKey);
    workSemaphore.acquire(workKey.getQueueName());
    checkTaskCancelled(); // check again, because the world might be changed when the lock has been acquired.
    tracker.increment();
    return taskId;
  }

  @Override
  public void unRegisterTask(final String taskId) {
    try {
      final WorkKey key = taskMap.remove(taskId);
      if (key == null) {
        tracker.cancel(new IllegalArgumentException("Task Id(" + taskId + ") received, but this task was already unregistered."));
      } else {
        workSemaphore.release(key.getQueueName());
      }
    } finally {
      tracker.decrement();
    }
  }

  @Override
  public boolean isCancelled() {
    return tracker.isCanceled();
  }

  private void checkTaskCancelled() throws TaskCancelledException {
    if (isCancelled()) {
      throw new TaskCancelledException(tracker.getException());
    }
  }
}
