/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacketMap;
import nl.overheid.aerius.conversion.OPSSourceConverter;
import nl.overheid.aerius.conversion.SourceConverter;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.InlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringInlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringMaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.srm2.conversion.SRMSourceConverter;

/**
 * Expands {@link EmissionSource} to a list of {@link EngineSource} objects.
 * Converts based on sector to OPS or SRM engine sources.
 */
public class OwN2000SourceConverter implements SourceConverter {

  private static final SRMSourceConverter SRM2_CONVERTER = new SRMSourceConverter();

  private final OPSSourceConverter opsSourceConverter;
  private final CalculationEngineProvider provider;

  public OwN2000SourceConverter(final Connection con, final CalculationEngineProvider provider, final ScenarioSituation situation)
      throws SQLException {
    this.provider = provider;
    opsSourceConverter = new OPSSourceConverter(con, situation);
  }

  @Override
  public boolean convert(final Connection con, final GroupedSourcesPacketMap map, final EmissionSource originalSource, final Geometry geometry,
      final List<Substance> keys) throws AeriusException {
    boolean converted = false;
    final int sectorId = originalSource.getSectorId();

    if (provider.getCalculationEngine(sectorId) == CalculationEngine.ASRM2) {
      final Collection<EngineSource> expanded = new ArrayList<>();

      converted = SRM2_CONVERTER.convert(expanded, originalSource, geometry, keys);
      map.addAll(sectorId, CalculationEngine.ASRM2, expanded);
    }
    return opsSourceConverter.convert(con, map, originalSource, geometry, keys) || converted;
  }

  /**
   * Some emission source objects (the shipping sources) are always converted to OPS sources. If they would be assigned an sector
   * that is to be calculated with another srm2 this would result in sending ops sources to the srm2 worker. Subsequently this will
   * crash the srm2 worker. Therefore this guard will throw an exception when such a combination would be used as input.
   *
   * @param emissionSource source to check
   * @throws AeriusException exception in case the sector for the source doesn't use OPS as calculation engine.
   */
  @Override
  public void validate(final EmissionSourceFeature emissionSource) throws AeriusException {
    final EmissionSource source = emissionSource.getProperties();
    final int sectorId = source.getSectorId();

    if ((source instanceof InlandShippingEmissionSource
        || source instanceof MooringInlandShippingEmissionSource
        || source instanceof MaritimeShippingEmissionSource
        || source instanceof MooringMaritimeShippingEmissionSource)
        && provider.getCalculationEngine(sectorId) != CalculationEngine.OPS) {
      throw new AeriusException(ImaerExceptionReason.SHIPPING_INVALID_SECTOR, source.getLabel(), String.valueOf(source.getSectorId()),
          String.valueOf(source.getSectorId()));
    }
  }

  @Override
  public void convertBuildings() throws AeriusException {
    opsSourceConverter.convertBuildings();
  }

}
