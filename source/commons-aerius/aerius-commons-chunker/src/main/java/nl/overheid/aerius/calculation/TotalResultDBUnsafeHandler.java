/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Stores results in the database unsafe (meaning no check if the results already exists) and keeps results locally to be sent to the web client.
 */
public class TotalResultDBUnsafeHandler extends TotalResultDBHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(TotalResultDBUnsafeHandler.class);

  public TotalResultDBUnsafeHandler(final PMF pmf, final IncludeResultsFilter filter) {
    super(pmf, filter);
  }

  @Override
  public void onTotalResults(final List<AeriusResultPoint> results, final int calculationId) throws AeriusException {
    //Inserting results has to be done before queueing the results.
    //It has to be done before the backgrounds are added to the results via getResults.
    //As that method is called by a different thread...
    try (final Connection con = getPMF().getConnection()) {
      final int inserted = CalculationRepository.insertCalculationResultsUnsafe(con, calculationId, filteredResults(results));
      if (LOGGER.isTraceEnabled()) {
        LOGGER.trace("[calculationId:{}] Received #result:{}, inserted:{}", calculationId, results.size(), inserted);
      }
    } catch (final SQLException e) {
      LOGGER.error("[calculationId:{}] onTotalResults sql error", calculationId, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }
}
