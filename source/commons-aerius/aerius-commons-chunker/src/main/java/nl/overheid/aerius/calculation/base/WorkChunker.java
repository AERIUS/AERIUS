/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Performs the main chunking of the work distributor.
 */
public interface WorkChunker<P extends AeriusPoint> {

  /**
   * Sends all chunks to the workers.
   *
   * @param startTrigger call when actual calculation starts
   */
  void run(WorkInitHandler<P> initHandler) throws Exception;

  /**
   * Called when all work is calculated by the worker and processed.
   *
   * @throws AeriusException
   */
  void onComplete() throws AeriusException;
}
