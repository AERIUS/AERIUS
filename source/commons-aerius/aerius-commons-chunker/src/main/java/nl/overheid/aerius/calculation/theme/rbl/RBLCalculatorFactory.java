/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.calculation.CalculatorOptionsFactory;
import nl.overheid.aerius.calculation.WorkSplitterCalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculatorThemeFactory;
import nl.overheid.aerius.calculation.base.SimpleCalculationResultQueue;
import nl.overheid.aerius.calculation.base.WorkBySectorHandlerImpl;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.ThemeCalculationJobOptions;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.RBLCalculationOptions;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * {@link CalculatorThemeFactory} implementation for RBL calculations.
 */
public class RBLCalculatorFactory implements CalculatorThemeFactory {

  private static final Logger LOGGER = LoggerFactory.getLogger(RBLCalculatorFactory.class);

  private static final List<Substance> NSL_SUBSTANCES = Arrays.asList(Substance.NO2, Substance.NOX, Substance.PM10, Substance.PM25, Substance.EC);
  private static final List<EmissionResultKey> NSL_EMISSIONRESULTKEYS = Arrays.asList(EmissionResultKey.NOX_CONCENTRATION,
      EmissionResultKey.NO2_CONCENTRATION, EmissionResultKey.NO2_EXCEEDANCE_HOURS, EmissionResultKey.PM10_CONCENTRATION,
      EmissionResultKey.PM10_EXCEEDANCE_DAYS, EmissionResultKey.PM25_CONCENTRATION, EmissionResultKey.EC_CONCENTRATION);

  private static final CalculationEngineProvider PROVIDER = sectorId -> CalculationEngine.ASRM2;
  private static final RBLCalculationTaskFactory CALCULATION_TASKFACTORY = new RBLCalculationTaskFactory();

  private final PMF pmf;
  private final SectorCategories sectorCategories;

  private final AeriusSRMVersion srmVersion;

  public RBLCalculatorFactory(final PMF pmf) throws SQLException {
    this.pmf = pmf;
    sectorCategories = SectorRepository.getSectorCategories(pmf, LocaleUtils.getDefaultLocale());
    srmVersion = AeriusSRMVersion.getByVersionLabel(ConstantRepository.getString(pmf, ConstantsEnum.MODEL_VERSION_SRM_RBL));
  }

  @Override
  public CalculatorOptions getCalculatorOptions(final Connection con, final CalculationInputData inputData) throws SQLException {
    // No UI available for RBL therefore always use Worker settings
    return CalculatorOptionsFactory.initWorkerCalculationOptions(con);
  }

  @Override
  public void prepareOptions(final ScenarioCalculations scenarioCalculations) throws AeriusException {
    final CalculationSetOptions options = scenarioCalculations.getOptions();
    final List<Substance> substances = options.getSubstances();

    if (substances.isEmpty()) {
      substances.addAll(NSL_SUBSTANCES);
    }
    final Set<EmissionResultKey> erks = options.getEmissionResultKeys();

    if (erks.isEmpty()) {
      erks.addAll(NSL_EMISSIONRESULTKEYS);
    }
    scenarioCalculations.getScenario().getOptions().getRblCalculationOptions().setSrmVersion(srmVersion.getVersion());
  }

  @Override
  public ThemeCalculationJobOptions getThemeCalculationJobOptions(final CalculationInputData inputData, final CalculationJob calculationJob) {
    final RBLCalculationOptions rblCalculationOptions = inputData.getScenario().getOptions().getRblCalculationOptions();
    final RBLCalculationJobOptions options = new RBLCalculationJobOptions();

    options.setSrmVersion(AeriusSRMVersion.getByVersionLabel(rblCalculationOptions.getSrmVersion()));
    return options;
  }

  @Override
  public CalculationEngineProvider getProvider(final CalculationJob calculationJob) {
    return PROVIDER;
  }

  @Override
  public List<CalculationResultHandler> getCalculationResultHandler(final CalculationJob calculationJob, final ExportType exportType,
      final Date exportDate) throws AeriusException {
    if (exportType == ExportType.CIMLK_CSV) {
      final String databaseVersion;
      try {
        databaseVersion = pmf.getDatabaseVersion();
      } catch (final SQLException e) {
        LOGGER.error("Error while retrieving database version for result file", e);
        throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
      }
      return List.of(new RBLOuputHandler(WorkerUtil.getFolderForJob(calculationJob.getJobIdentifier()),
          calculationJob.getScenarioCalculations().getCalculationYears(), AeriusVersion.getVersionNumber(), databaseVersion, calculationJob.getName(),
          exportDate));
    } else {
      LOGGER.error("Calculations for CIMLK don't support export type '{}'", exportType);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  @Override
  public CalculationTaskHandler wrapCalculationTaskHandler(final CalculationJob calculationJob, final CalculationTaskHandler calculationTaskHandler) {
    final CalculatorOptions calculatorOptions = calculationJob.getCalculatorOptions();

    return new WorkSplitterCalculationTaskHandler(calculationTaskHandler,
        calculatorOptions.getMaxCalculationEngineUnits(), calculatorOptions.getMinReceptor());
  }

  @Override
  public CalculationResultQueue createCalculationResultQueue() {
    return new SimpleCalculationResultQueue();
  }

  @Override
  public WorkDistributor<AeriusPoint> createWorkDistributor(final CalculationJob calculationJob) {
    return new RBLWorkDistributor(pmf, sectorCategories);
  }

  @Override
  public WorkHandler<AeriusPoint> createWorkHandler(final CalculationJob calculationJob, final CalculationTaskHandler remoteWorkHandler)
      throws AeriusException {
    return new WorkBySectorHandlerImpl(CALCULATION_TASKFACTORY,
        new RBLCorrectionsCalculationTaskHandler(remoteWorkHandler, calculationJob.getScenarioCalculations().getCalculations()));
  }
}
