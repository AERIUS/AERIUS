/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Processes that scheduled next to the main chunk work process.
 * When the main process is finished, depending on the finish state {@link #onComplete()} or {@link #onCancelled()} will be called.
 */
public interface ScheduledWorkRunnable {

  /**
   * Run process.
   *
   * @throws Exception
   */
  void run() throws Exception;

  /**
   * Called when main chunker process finished completed.
   *
   * @throws AeriusException
   */
  void onComplete() throws AeriusException;

  /**
   * Called when main chunker process was cancelled.
   */
  default void onCancelled() {
  }
}
