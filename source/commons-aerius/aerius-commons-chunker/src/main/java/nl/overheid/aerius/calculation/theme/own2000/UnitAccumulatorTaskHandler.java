/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.util.Collection;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.UnitsPerSectorPerEngineAccumulator;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Siphons data about the workload a CalculationJob represents for use in the summary accompanying CSV calculation results.
 */
class UnitAccumulatorTaskHandler implements CalculationTaskHandler {

  /**
   * Destination for the workload data.
   */
  private final UnitsPerSectorPerEngineAccumulator accumulator;

  /**
   * Wrapped handler.
   */
  private final CalculationTaskHandler wrappedHandler;

  public UnitAccumulatorTaskHandler(final CalculationJob calculationJob, final CalculationTaskHandler wrappedHandler) {
    this.accumulator = calculationJob.getUnitsAccumulator();
    this.wrappedHandler = wrappedHandler;
  }

  @Override
  public <E extends EngineSource, T extends CalculationTask<E, ?, ?>> void work(final WorkKey workKey, final T task,
      final Collection<AeriusPoint> points) throws AeriusException, InterruptedException, TaskCancelledException {
    for (final Entry<Integer, Collection<E>> entry : task.getTaskInput().getEmissionSources().entrySet()) {
      accumulator.add(entry.getKey(), (Collection<EngineSource>) entry.getValue(), points.size());
    }
    wrappedHandler.work(workKey, task, points);
  }
}
