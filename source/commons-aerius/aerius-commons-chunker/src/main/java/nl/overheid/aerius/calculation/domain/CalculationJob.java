/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.io.File;
import java.time.Instant;
import java.util.List;

import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.natura2k.EmissionSourceTreeBuilder;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Data object with a state and data information of a single calculation.
 */
public class CalculationJob {

  private final JobIdentifier jobIdentifier;
  private final String name;
  private final ScenarioCalculations scenarioCalculations;
  private final String queueName;
  private final ExportType exportType;

  private CalculationEngineProvider provider;
  private CalculatorOptions calculatorOptions;
  private UnitsPerSectorPerEngineAccumulator unitsAccumulator;
  // Location where output results are temporarily stored to be sent to the user
  private File outputDirectory;
  private ThemeCalculationJobOptions themeCalculationJobOptions;
  // Initialize start time when object created.
  private final Instant startTime = Instant.now();
  private FileServerExpireTag expire;
  /**
   * When the results are stored on the fileserver by the model worker this is the url to that result file.
   */
  private String resultUrl;
  private BBox admsExtent;

  public CalculationJob(final JobIdentifier jobIdentifier, final String name, final ScenarioCalculations scenarioCalculations,
      final ExportType exportType, final String queueName) {
    this.jobIdentifier = jobIdentifier;
    this.name = name;
    this.scenarioCalculations = scenarioCalculations;
    this.exportType = exportType;
    this.queueName = queueName;
  }

  public Instant getStartTime() {
    return startTime;
  }

  public String getName() {
    return name;
  }

  public ScenarioCalculations getScenarioCalculations() {
    return scenarioCalculations;
  }

  public List<Calculation> getCalculations() {
    return getScenarioCalculations().getCalculations();
  }

  public EmissionSourceTreeBuilder getSourcesTreeBuilder() throws AeriusException {
    return EmissionSourceTreeBuilder.build(getScenarioCalculations().getScenario().getSituations());
  }

  public String getQueueName() {
    return queueName;
  }

  public JobIdentifier getJobIdentifier() {
    return jobIdentifier;
  }

  public ExportType getExportType() {
    return exportType;
  }

  public CommandType getCommandType() {
    return exportType == null || !exportType.isSourceExport() ? CommandType.CALCULATE : CommandType.EXPORT;
  }

  public ThemeCalculationJobOptions getThemeCalculationJobOptions() {
    return themeCalculationJobOptions;
  }

  public void setThemeCalculationJobOptions(final ThemeCalculationJobOptions themeCalculationJobOptions) {
    this.themeCalculationJobOptions = themeCalculationJobOptions;
  }

  public CalculationEngineProvider getProvider() {
    return provider;
  }

  public void setProvider(final CalculationEngineProvider provider) {
    this.provider = provider;
  }

  public CalculationSetOptions getCalculationSetOptions() {
    return getScenarioCalculations().getOptions();
  }

  public CalculatorOptions getCalculatorOptions() {
    return calculatorOptions;
  }

  public void setCalculatorOptions(final CalculatorOptions calculatorOptions) {
    this.calculatorOptions = calculatorOptions;
  }

  public List<Substance> getSubstances() {
    return getCalculationSetOptions().getSubstances();
  }

  public File getOutputDirectory() {
    return outputDirectory;
  }

  public void setOutputDirectory(final File outputDirectory) {
    this.outputDirectory = outputDirectory;
  }

  public String getResultUrl() {
    return resultUrl;
  }

  public void setResultUrl(final String resultUrl) {
    this.resultUrl = resultUrl;
  }

  /**
   * @return the tag to indicate how long the import file should be stored
   */
  public FileServerExpireTag getExpire() {
    return expire;
  }

  public void setExpire(final FileServerExpireTag expire) {
    this.expire = expire;
  }

  public UnitsPerSectorPerEngineAccumulator getUnitsAccumulator() {
    return unitsAccumulator;
  }

  /**
   * @return true if has an unitsAccumulator
   */
  public boolean isUnitsAccumulator() {
    return unitsAccumulator != null;
  }

  /**
   * Call to enable units accumulator.
   */
  public void setUnitsAccumulator() {
    unitsAccumulator = new UnitsPerSectorPerEngineAccumulator();
  }

  /**
   * Check if summaries should be calculated for this job.
   *
   * @return true if they are needed now (indicated by the exportType), or in the future (indicated by the job protection status).
   */
  public boolean isJobWithResultSummaries() {
    return exportType.isExportWithSummaries() || jobIdentifier.isProtected();
  }

  public BBox getAdmsExtent() {
    return admsExtent;
  }

  public void setAdmsExtent(final BBox admsExtent) {
    this.admsExtent = admsExtent;
  }
}
