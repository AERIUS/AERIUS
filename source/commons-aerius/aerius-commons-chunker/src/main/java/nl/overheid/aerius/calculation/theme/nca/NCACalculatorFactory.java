/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.archive.ArchiveProxy;
import nl.overheid.aerius.calculation.CalculatorOptionsFactory;
import nl.overheid.aerius.calculation.ExportCalculationResultHandler;
import nl.overheid.aerius.calculation.TotalResultSubPointDBHandler;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculationTaskFactory;
import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculatorThemeFactory;
import nl.overheid.aerius.calculation.base.CombinedWorkDistributor;
import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.WorkBySectorHandlerImpl;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.base.WorkInitHandler;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.ThemeCalculationJobOptions;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.natura2k.Natura2KReceptorLoader;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.calculation.results.ResultsSummariesHandler;
import nl.overheid.aerius.calculation.theme.own2000.CustomReceptorHandler;
import nl.overheid.aerius.calculation.theme.own2000.OffSiteReductionResultPostProcessHandler;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.ReceptorInfoRepositoryBean;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * {@link CalculatorThemeFactory} for NCA calculations.
 */
public class NCACalculatorFactory implements CalculatorThemeFactory {

  private static final Logger LOGGER = LoggerFactory.getLogger(NCACalculatorFactory.class);

  private final PMF pmf;

  private final ReceptorGridSettings rgs;
  private final ReceptorUtil receptorUtil;
  private final GridSettings gridSettings;
  private final Natura2kReceptorStore n2kReceptorStore;
  private final SectorCategories sectorCategories;
  private final NCABackgroundSupplier ncaBackgroundSupplier;
  private final ArchiveProxy archiveProxy;
  private final CalculationTaskFactory calculationTaskFactory;

  public NCACalculatorFactory(final PMF pmf, final ArchiveProxy archiveProxy)
      throws SQLException, AeriusException {
    this.pmf = pmf;
    rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(pmf);
    receptorUtil = new ReceptorUtil(rgs);
    gridSettings = new GridSettings(rgs);
    n2kReceptorStore = loadNatura2kReceptorStore();
    sectorCategories = SectorRepository.getSectorCategories(pmf, Locale.ENGLISH);
    ncaBackgroundSupplier = new NCABackgroundSupplier(new ReceptorInfoRepositoryBean(pmf), receptorUtil);
    this.archiveProxy = archiveProxy;
    this.calculationTaskFactory = new NCACalculationTaskFactory();
  }

  @Override
  public CalculatorOptions getCalculatorOptions(final Connection con, final CalculationInputData inputData) throws SQLException {
    if (inputData.isDemoMode() || inputData.getScenario().getOptions().getCalculationMethod() == CalculationMethod.QUICK_RUN) {
      // In demo mode or quickrun the calculation is much faster and therefore the chunk can be larger using different configuration settings.
      return CalculatorOptionsFactory.initQuickRunCalculationOptions(con);
    } else if (inputData.getExportType() == ExportType.CALCULATION_UI) {
      return CalculatorOptionsFactory.initUICalculatorOptions(con);
    } else {
      return CalculatorOptionsFactory.initWorkerCalculationOptions(con);
    }
  }

  /**
   * @return Load Natura2000 area data into Natura2kReceptorStore.
   */
  private Natura2kReceptorStore loadNatura2kReceptorStore() throws SQLException, AeriusException {
    final Natura2KReceptorLoader receptorLoader = new Natura2KReceptorLoader(receptorUtil, gridSettings);

    try (final Connection con = pmf.getConnection()) {
      return receptorLoader.fillReceptorStore(con);
    }
  }

  @Override
  public void prepareOptions(final ScenarioCalculations scenarioCalculations) throws AeriusException {
  }

  @Override
  public ThemeCalculationJobOptions getThemeCalculationJobOptions(final CalculationInputData inputData, final CalculationJob calculationJob) {
    final NCACalculationJobOptions options = new NCACalculationJobOptions();

    options.setAdmsLicense(inputData.getAdmsLicense());
    options.setDemoMode(inputData.isDemoMode());
    calculationJob.getCalculatorOptions().setCalculateIntermediateProgressReports();
    return options;
  }

  @Override
  public CalculationEngineProvider getProvider(final CalculationJob calculationJob) {
    return sectorId -> CalculationEngine.ADMS;
  }

  @Override
  public List<PostCalculationHandler> getPostCalculationHandlers(final CalculationJob calculationJob) {
    final List<PostCalculationHandler> handlers = new ArrayList<>();
    if (calculationJob.isJobWithResultSummaries()) {
      final ResultsSummariesHandler resultsSummariesHandler = new ResultsSummariesHandler(pmf);
      handlers.add(resultsSummariesHandler);
    }
    return handlers;
  }

  @Override
  public List<WorkInitHandler<AeriusPoint>> getWorkInitHandlers() {
    return List.of(new NCAWorkerInitHandler(pmf));
  }

  @Override
  public List<CalculationResultHandler> getCalculationResultHandler(final CalculationJob calculationJob, final ExportType exportType,
      final Date exportDate) throws AeriusException {
    return List.of(exportType == ExportType.ADMS ? new ExportCalculationResultHandler(calculationJob)
        : new TotalResultSubPointDBHandler(pmf, null, receptorUtil));
  }

  @Override
  public CalculationResultQueue createCalculationResultQueue() {
    return new NCACalculationResultQueue();
  }

  @Override
  public WorkHandler<AeriusPoint> createWorkHandler(final CalculationJob calculationJob, final CalculationTaskHandler workHandler)
      throws AeriusException {
    return new WorkBySectorHandlerImpl(calculationTaskFactory, workHandler);
  }

  @Override
  public void prepareResultHandler(final ResultHandler resultHandler, final CalculationJob calculationJob) {
    if (calculationJob.getCalculationSetOptions().getCalculationMethod() == CalculationMethod.CUSTOM_RECEPTORS) {
      resultHandler.addResultUpdateHandler(new CustomReceptorHandler(rgs, calculationJob));
    }
    addOffSiteReductionPostProcessorIfNeeded(resultHandler, calculationJob);
  }

  @Override
  public WorkDistributor<AeriusPoint> createWorkDistributor(final CalculationJob calculationJob) throws AeriusException, SQLException {
    final CalculationMethod ct = calculationJob.getCalculationSetOptions().getCalculationMethod();

    return switch (ct) {
      case FORMAL_ASSESSMENT, NATURE_AREA, QUICK_RUN, CUSTOM_RECEPTORS -> createN2KWorkDistributor(calculationJob);
      case CUSTOM_POINTS -> createCustomPointWorkDistributor(calculationJob);
      default -> {
        LOGGER.debug("Unsupported calculation method for NCA calculation:'{}'", ct);
        throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
      }
    };
  }

  private WorkDistributor<AeriusPoint> createN2KWorkDistributor(final CalculationJob calculationJob) {
    final NCANatura2kWorkDistributor n2KWorkDistributor = new NCANatura2kWorkDistributor(pmf, n2kReceptorStore, sectorCategories, rgs,
        ncaBackgroundSupplier, archiveProxy);

    return calculationJob.getScenarioCalculations().getCalculationPoints().isEmpty() ? n2KWorkDistributor
        : new CombinedWorkDistributor(n2KWorkDistributor, createCustomPointWorkDistributor(calculationJob));
  }

  private NCAPointsWorkDistributor createCustomPointWorkDistributor(final CalculationJob calculationJob) {
    return new NCAPointsWorkDistributor(pmf, calculationJob.getScenarioCalculations().getCalculationPoints(), sectorCategories,
        ncaBackgroundSupplier);
  }

  private static void addOffSiteReductionPostProcessorIfNeeded(final ResultHandler resultHandler, final CalculationJob calculationJob) {
    if (OffSiteReductionResultPostProcessHandler.canApply(calculationJob.getScenarioCalculations())) {
      resultHandler.addResultUpdateHandler(new OffSiteReductionResultPostProcessHandler(calculationJob.getScenarioCalculations()));
    }
  }
}
