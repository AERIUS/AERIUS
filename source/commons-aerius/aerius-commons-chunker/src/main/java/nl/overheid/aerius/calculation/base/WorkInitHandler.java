/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.util.List;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Specific handler that is called before sending packets to the model workers, but after all packets are available.
 */
public interface WorkInitHandler<P extends AeriusPoint> {

  /**
   * Perform additional initialization based on calculationJob and work packets.
   *
   * @param calculationJob The calculation job related to this job
   * @param packets All workpackets contains sources and points of a packets
   */
  void init(CalculationJob calculationJob, List<WorkPacket<P>> packets) throws AeriusException;
}
