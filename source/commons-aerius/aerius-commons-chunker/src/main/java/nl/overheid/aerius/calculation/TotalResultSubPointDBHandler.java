/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.AeriusSubPoint;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * {@link CalculationResultHandler} to store the total results into the database.
 *
 * Takes care of adding any receptor-without-subreceptors to the database as a subreceptor.
 * This in turn ensures aggregation and visualisation works as expected.
 */
public class TotalResultSubPointDBHandler extends TotalResultDBHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(TotalResultSubPointDBHandler.class);

  private final ReceptorUtil receptorUtil;

  /**
   * Constructor.
   *
   * @param pmf
   * @param filter Results filter, or null if no filter should be applied.
   * @param receptorUtil The receptor util to determine proper locations for (sub)receptors.
   */
  public TotalResultSubPointDBHandler(final PMF pmf, final IncludeResultsFilter filter, final ReceptorUtil receptorUtil) {
    super(pmf, filter);
    this.receptorUtil = receptorUtil;
  }

  @Override
  public void onTotalResults(final List<AeriusResultPoint> results, final int calculationId) throws AeriusException {
    List<AeriusResultPoint> pointsToInsert = new ArrayList<>(results);
    try (final Connection con = getPMF().getConnection()) {
      pointsToInsert = addReceptorsWithoutSubreceptorsAsSubReceptor(con, calculationId, pointsToInsert);
    } catch (final SQLException e) {
      LOGGER.error("[calculationId:{}] sql error", calculationId, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    super.onTotalResults(pointsToInsert, calculationId);
  }

  private List<AeriusResultPoint> addReceptorsWithoutSubreceptorsAsSubReceptor(final Connection con, final int calculationId,
      final List<AeriusResultPoint> results) throws SQLException {
    final Set<Integer> receptorsWithSubPoints = determineReceptorsWithSubPoints(results);
    final List<AeriusResultPoint> completeList = new ArrayList<>(results);
    final List<IsSubPoint> subPointsToPersist = new ArrayList<>();
    for (final AeriusResultPoint point : results) {
      if (point.getPointType() == AeriusPointType.RECEPTOR && !receptorsWithSubPoints.contains(point.getId())) {
        final AeriusResultPoint subPointForReceptor = receptorToSubReceptor(point);
        completeList.add(subPointForReceptor);
        subPointsToPersist.add(new AeriusSubPoint(point, point.getId()));
      }
    }
    persistNewSubPoints(con, calculationId, subPointsToPersist);
    return completeList;
  }

  private AeriusResultPoint receptorToSubReceptor(final AeriusResultPoint receptor) {
    final AeriusResultPoint subPointForReceptor = new AeriusResultPoint(receptor);
    subPointForReceptor.setPointType(AeriusPointType.SUB_RECEPTOR);
    // by definition, a subreceptor that represents the receptor gets ID 0.
    subPointForReceptor.setId(0);
    subPointForReceptor.setParentId(receptor.getId());
    final Point location = receptorUtil.getPointFromReceptorId(receptor.getId());
    receptor.setCoordinates(location.getCoordinates());
    receptor.getEmissionResults().copyTo(subPointForReceptor.getEmissionResults());
    return subPointForReceptor;
  }

  private static void persistNewSubPoints(final Connection con, final int calculationId, final List<IsSubPoint> pointsToPersist)
      throws SQLException {
    final Calculation calculation = CalculationRepository.getCalculation(con, calculationId);
    if (calculation != null && calculation.getCustomCalculationSubPointSetId() != null) {
      CalculationRepository.insertCalculationSubPointsForSetSafe(con, calculation.getCustomCalculationSubPointSetId(),
          pointsToPersist);
    }
  }

  private static Set<Integer> determineReceptorsWithSubPoints(final List<AeriusResultPoint> resultPoints) {
    return resultPoints.stream()
        .filter(x -> x.getPointType() == AeriusPointType.SUB_RECEPTOR)
        .map(AeriusResultPoint::getParentId)
        .collect(Collectors.toSet());
  }

}
