/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * {@link CalculationResultHandler} to store the total results into the database.
 */
public class TotalResultDBHandler implements CalculationResultHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(TotalResultDBHandler.class);

  private final PMF pmf;
  private final IncludeResultsFilter filter;

  /**
   * Constructor.
   *
   * @param pmf
   * @param filter Results filter, or null if no filter should be applied.
   */
  public TotalResultDBHandler(final PMF pmf, final IncludeResultsFilter filter) {
    this.pmf = pmf;
    this.filter = filter;
  }

  protected PMF getPMF() {
    return pmf;
  }

  /**
   * Insert results into the database.
   * @param results
   * @param calculationId
   * @throws AeriusException
   * @throws Exception
   */
  @Override
  public void onTotalResults(final List<AeriusResultPoint> results, final int calculationId) throws AeriusException {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("[calculationId:{}] Received #{} results for calculation ", calculationId, results.size());
    }
    try (final Connection con = pmf.getConnection()) {
      CalculationRepository.insertCalculationResults(con, calculationId, filteredResults(results));
    } catch (final SQLException e) {
      LOGGER.error("[calculationId:{}] sql error", calculationId, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Returns filtered results if a filter is supplied.
   *
   * @param results
   * @return
   */
  protected List<AeriusResultPoint> filteredResults(final List<AeriusResultPoint> results) {
    return filter == null ? results : results.stream().filter(filter::includeResult).toList();
  }
}
