/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.ResultPostProcessHandler;
import nl.overheid.aerius.calculation.base.WorkMonitor;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.ModelProgressReport;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.function.AeriusConsumer;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * Implementation of {@link ResultHandler}.
 */
class ResultHandlerImpl implements ResultHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(ResultHandlerImpl.class);

  private final PMF pmf;
  private final WorkMonitor monitor;
  private final CalculationResultQueue queue;
  private final CalculationResultHandler handler;
  private final List<ResultPostProcessHandler> resultUpdateHandlers = new ArrayList<>();

  public ResultHandlerImpl(final PMF pmf, final WorkMonitor monitor, final CalculationResultQueue queue, final CalculationResultHandler handler) {
    this.pmf = pmf;
    this.monitor = monitor;
    this.queue = queue;
    this.handler = handler;
  }

  @Override
  public void addResultUpdateHandler(final ResultPostProcessHandler resultUpdateHandler) {
    this.resultUpdateHandlers.add(resultUpdateHandler);
  }

  @Override
  public String registerTask(final WorkKey workKey) throws InterruptedException, TaskCancelledException {
    return monitor.registerTask(workKey);
  }

  @Override
  public void onFailure(final String taskId, final Exception exception) {
    synchronized (this) {
      monitor.taskFailed(taskId, exception);
      queue.clear();
    }
  }

  @Override
  public void onResult(final String taskId, final EngineDataKey engineDataKey, final Map<Integer, List<AeriusResultPoint>> results) {
    processResults(taskId, workKey -> {
      final Integer calculationId = workKey.getCalculationId();

      for (final Entry<Integer, List<AeriusResultPoint>> entry : results.entrySet()) {
        resultUpdateHandlers.forEach(h -> h.postProcessSectorResults(calculationId, entry.getValue()));
      }
      handleSectorResults(calculationId, engineDataKey, results);
    });
  }

  @Override
  public void onExport(final String taskId, final String fileUrl) {
    processResults(taskId, workKey -> handler.onExportResult(fileUrl));
  }

  @Override
  public void onModelProgressReport(final String taskId, final ModelProgressReport modelProgressReport) {
    try {
      handler.onModelProgressReport(modelProgressReport);
    } catch (AeriusException | RuntimeException e) {
      LOGGER.debug("[taskid: {}] onModelProgressReport failed:", taskId, e);
      onFailure(taskId, e);
    }
  }

  private void processResults(final String taskId, final AeriusConsumer<WorkKey> processor) {
    if (!monitor.isCancelled()) {
      try {
        final WorkKey workKey = monitor.getWorkKey(taskId);

        failIfJobCancelledByUser(workKey.getJobIdentifier().getJobKey());
        processor.apply(workKey);
        // unregister task if all work on the task is done
        monitor.unRegisterTask(taskId);
      } catch (final AeriusException | RuntimeException e) {
        LOGGER.debug("[taskid: {}] onResult failed:", taskId, e);
        onFailure(taskId, e);
      }
    }
  }

  private void handleSectorResults(final int calculationId, final EngineDataKey engineDataKey, final Map<Integer, List<AeriusResultPoint>> results)
      throws AeriusException {
    queue.put(engineDataKey, results, calculationId);
    handler.onSectorResults(results, calculationId, engineDataKey);
  }

  /**
   * Check if the job was not cancelled or deleted before continuing processing the received results.
   *
   * @param correlationId
   * @throws AeriusException
   */
  protected void failIfJobCancelledByUser(final String correlationId) throws AeriusException {
    WorkerUtil.failIfJobCancelledByUser(pmf, correlationId);
  }
}
