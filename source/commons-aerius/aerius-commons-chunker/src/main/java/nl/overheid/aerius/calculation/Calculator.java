/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanKind;

import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.base.ScheduledWorkRunnable;
import nl.overheid.aerius.calculation.base.WorkChunker;
import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.otel.TracerUtil;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * Calculator object to run the calculation. The Calculator is prepared with the {@link CalculatorBuilder} class via the
 * {@link CalculatorBuildDirector}. This class contains the running process that controls the calculation.
 */
public final class Calculator {

  private static final Logger LOGGER = LoggerFactory.getLogger(Calculator.class);

  private static final long RESULT_HANDLER_SLEEP_TIME_SECONDS = 1;

  private final PMF pmf;
  private final ScheduledExecutorService executor;
  private final CalculationJob calculationJob;
  private final WorkTracker tracker;
  private final WorkChunker<AeriusPoint> workChunker;
  private final List<ScheduledWorkRunnable> scheduledWorkRunnables;
  private final List<PostCalculationHandler> postCalculationHandlers;
  private final MultipleWorkInitHandler<AeriusPoint> workInitHandler;

  Calculator(final PMF pmf, final ScheduledExecutorService executor, final CalculationJob calculationJob, final WorkChunker<AeriusPoint> workChunker,
      final List<ScheduledWorkRunnable> scheduledWorkRunnables, final List<PostCalculationHandler> postCalculationHandlers,
      final MultipleWorkInitHandler<AeriusPoint> workInitHandler, final WorkTracker tracker) {
    this.pmf = pmf;
    this.executor = executor;
    this.calculationJob = calculationJob;
    this.workChunker = workChunker;
    this.scheduledWorkRunnables = scheduledWorkRunnables;
    this.postCalculationHandlers = postCalculationHandlers;
    this.workInitHandler = workInitHandler;
    this.tracker = tracker;
  }

  /**
   * Starts a calculation and blocks until it is finished.
   * Flow is as follows:
   * <ol>
   * <li> Schedule handlers running in parallel to chunking step.
   * <li> Sends chunks sequential to the workers.
   * <li> Wait for all results to return from the workers.
   * <li> Wait and finish all scheduled handlers.
   * <li> Call all post calculation handlers to complete/cancel.
   *
   * @throws Exception
   */
  public CalculationJob calculate() throws Exception {
    final JobIdentifier jobIdentifier = calculationJob.getJobIdentifier();
    LOGGER.info("Start the calculation for {}", jobIdentifier);
    final List<ScheduledFuture<?>> futures = scheduleWorkRunnables();

    try {
      WorkerUtil.failIfJobCancelledByUser(pmf, jobIdentifier.getJobKey());
      final CalculationState startState = tracker.start();
      // Send the work to workers.
      final Span spanWork = TracerUtil.getSpanBuilder(jobIdentifier.getJobKey(), "aerius.chunker.work", SpanKind.INTERNAL).startSpan();
      try {
        workInitHandler.addHandler((c, p) -> updateCalculationState(startState));
        workChunker.run(workInitHandler);
        // Wait for all results to be received from the workers.
        waitForWorkerCompletion(jobIdentifier);
        // Make sure all data put in the queues is processed.
        // For example store all results in the database.
        waitForFuturesToFinish(futures, true);
      } finally {
        spanWork.end();
      }

      final Span spanPostProcess = TracerUtil.getSpanBuilder(jobIdentifier.getJobKey(), "chunker.postprocessor", SpanKind.INTERNAL).startSpan();
      try {
        // Wait for all processing threads to complete (i.e. store results, intermediate handlers).
        processPostCalculationHandlers(tracker.getState(), jobIdentifier);
      } finally {
        spanPostProcess.end();
      }
    } catch (final TaskCancelledException e) {
      LOGGER.debug("Calculation TaskCancelledException", e);
      cancel(e.getCause(), futures);
      throw e.getCause();
    } catch (final InterruptedException e) {
      LOGGER.error("Calculation interrupted:", e);
      cancel(e, futures);
      Thread.currentThread().interrupt();
      throw e;
    } catch (final AeriusException e) {
      if (e.isInternalError()) {
        LOGGER.error("Calculation stopped with AeriusException {} for {}", e.getReason(), jobIdentifier, e);
      } else if (e.getReason() != AeriusExceptionReason.CONNECT_JOB_CANCELLED) {
        LOGGER.warn("Calculation stopped with AeriusException  {} and args {} for {}", e.getReason(), String.join(", ", e.getArgs()),
            jobIdentifier, e);
      } else {
        LOGGER.debug("Calculation cancelled", e);
      }
      cancel(e, futures);
      throw e;
    } catch (final Exception e) {
      LOGGER.error("Calculation stopped with exception for job {}: {}", jobIdentifier, e.getMessage(), e);
      cancel(e, futures);
      throw e;
    } finally {
      // This marks the calculation finished in the database.
      updateCalculationState(tracker.getState());
      LOGGER.info("Calculation finished with state '{}' for {}", tracker.getState(), jobIdentifier);
    }
    return calculationJob;
  }

  private List<ScheduledFuture<?>> scheduleWorkRunnables() {
    final List<ScheduledFuture<?>> futures = new ArrayList<>();

    for (final ScheduledWorkRunnable runnable : scheduledWorkRunnables) {
      futures.add(executor.scheduleWithFixedDelay(
          () -> wrapRunnable(runnable, futures), RESULT_HANDLER_SLEEP_TIME_SECONDS, RESULT_HANDLER_SLEEP_TIME_SECONDS, TimeUnit.SECONDS));
    }
    return futures;
  }

  private void wrapRunnable(final ScheduledWorkRunnable runnable, final List<ScheduledFuture<?>> futures) {
    try {
      runnable.run();
    } catch (final Exception e) {
      cancel(e, futures);
    }
  }

  private void cancel(final Exception exception, final List<ScheduledFuture<?>> futures) {
    tracker.cancel(exception);
    try {
      waitForFuturesToFinish(futures, false);
    } catch (final InterruptedException e) {
      LOGGER.error("Cancel interrupted:", e);
      Thread.currentThread().interrupt();
    } catch (final AeriusException e) {
      LOGGER.error("AeriusException during cancel of exception. Original exception will be thrown:", e);
    } finally {
      postCalculationHandlers.forEach(PostCalculationHandler::onCancelled);
    }
  }

  /**
   * Waits for all results to be returned by the workers.
   * @param jobIdentifier
   *
   * @throws Exception exception set in the tracker
   */
  private void waitForWorkerCompletion(final JobIdentifier jobIdentifier) throws Exception {
    LOGGER.debug("All calculation tasks sent to workers, waiting for completion for {}", jobIdentifier);
    final CalculationState state = tracker.waitForCompletion();

    if (state == CalculationState.CANCELLED) {
      throw tracker.getException();
    }
    workChunker.onComplete();
  }

  /**
   * Waits for all scheduled futures to finish and calls the onComplete/onCancelled depending on if the run was succesful or not.
   *
   * @param futures futures of scheduled processes
   * @param success true if calculation was completed successfully.
   * @throws InterruptedException
   * @throws AeriusException
   */
  private void waitForFuturesToFinish(final List<ScheduledFuture<?>> futures, final boolean success) throws InterruptedException, AeriusException {
    for (final ScheduledFuture<?> scheduledFuture : futures) {
      cancelAndWaitForFuture(scheduledFuture);
    }
    for (final ScheduledWorkRunnable wr : scheduledWorkRunnables) {
      if (success) {
        wr.onComplete();
      } else {
        wr.onCancelled();
      }
    }
  }

  /**
   * Stops a future and waits for it to finish.
   *
   * @param future future to stop
   * @throws InterruptedException
   */
  private static void cancelAndWaitForFuture(final Future<?> future) throws InterruptedException {
    if (future == null) {
      return;
    }
    future.cancel(false);
    try {
      future.get();
    } catch (final CancellationException e) {
      LOGGER.trace("Future cancelled, as expected: {}", e.getMessage());
    } catch (final ExecutionException e) {
      LOGGER.error("Future threw an exception, which is unexpected", e);
    }
  }

  /**
   * Calls the onCompleted/onCancelled on the post calculation handlers depending on the state of the calculation.
   *
   * @param state current job state as set in the tracker
   * @param jobIdentifier
   * @throws AeriusException
   * @throws SQLException
   */
  private void processPostCalculationHandlers(final CalculationState state, final JobIdentifier jobIdentifier) throws AeriusException, SQLException {
    LOGGER.info("Calculation done, waiting for completion of postprocessors for {}", jobIdentifier);
    if (state == CalculationState.COMPLETED) {
      try (final Connection con = pmf.getConnection()) {
        JobRepository.updateJobStatus(con, calculationJob.getJobIdentifier().getJobKey(), JobState.POST_PROCESSING);
      }
    }
    for (final PostCalculationHandler postCalculationHandler : postCalculationHandlers) {
      if (state == CalculationState.COMPLETED) {
        postCalculationHandler.onCompleted(calculationJob);
      } else if (state == CalculationState.CANCELLED) {
        postCalculationHandler.onCancelled();
      }
    }
  }

  private void updateCalculationState(final CalculationState state) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      for (final Calculation calculation : calculationJob.getCalculations()) {
        CalculationRepository.updateCalculationState(con, calculation.getCalculationId(), state);
      }
      if (state == CalculationState.RUNNING) {
        JobRepository.updateJobStatus(con, calculationJob.getJobIdentifier().getJobKey(), JobState.CALCULATING);
      }
    } catch (final SQLException e) {
      LOGGER.error("SQL error trying updating calculation/job state.", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }
}
