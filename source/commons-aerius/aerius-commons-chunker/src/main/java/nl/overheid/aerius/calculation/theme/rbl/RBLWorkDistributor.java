/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.conversion.EngineSourceExpander;
import nl.overheid.aerius.conversion.SourceConversionHelper;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.io.PreCalculationUtil;
import nl.overheid.aerius.util.FeaturePointToEnginePointUtil;

/**
 * Work distributor to create source/calculation point sets for CIMLK calculations.
 */
public class RBLWorkDistributor implements WorkDistributor<AeriusPoint> {

  private static final Integer STORAGE_SECTOR_ID = 1;

  private final PMF pmf;
  private final SectorCategories sectorCategories;

  private List<AeriusPoint> points;
  private final List<JobPacket> jobPackets = new ArrayList<>();

  public RBLWorkDistributor(final PMF pmf, final SectorCategories sectorCategories) {
    this.pmf = pmf;
    this.sectorCategories = sectorCategories;
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    points = FeaturePointToEnginePointUtil.convert(calculationJob.getScenarioCalculations().getCalculationPoints());
    // TODO: copy calculation points to calculation so we can assign per situation?
    if (calculationJob.getScenarioCalculations().getCalculations().size() == 1) {
      final Calculation calculation = calculationJob.getScenarioCalculations().getCalculations().get(0);

      PreCalculationUtil.assignDispersionLines(calculation.getSources(), points,
          calculation.getCIMLKDispersionLines());
    }

    final List<Substance> substances = calculationJob.getSubstances();

    initSourcePackets(calculationJob, substances);
  }

  /**
   * Creates the source packages. Since these won't change during calculation it can be done upfront.
   *
   * @param calculationJob
   * @param substances
   * @throws SQLException
   * @throws AeriusException
   */
  private void initSourcePackets(final CalculationJob calculationJob, final List<Substance> substances) throws SQLException, AeriusException {
    for (final Calculation job : calculationJob.getScenarioCalculations().getCalculations()) {
      final List<EngineSource> sources;

      try (Connection con = pmf.getConnection()) {
        final SourceConversionHelper conversionHelper = new SourceConversionHelper(pmf, sectorCategories, job.getYear());

        sources = EngineSourceExpander.toEngineSources(con, new RBLSourceConverter(STORAGE_SECTOR_ID, job), conversionHelper,
            job.getSituation(), substances, true);
      }
      jobPackets.add(new JobPacket(job.getCalculationId(), job.getYear(),
          List.of(new GroupedSourcesPacket(STORAGE_SECTOR_ID, CalculationEngine.ASRM2, sources))));
    }
  }

  @Override
  public boolean prepareWork() throws AeriusException {
    return !points.isEmpty();
  }

  @Override
  public WorkPacket<AeriusPoint> work() {
    final WorkPacket<AeriusPoint> workPacket = new WorkPacket<>(new ArrayList<>(points), jobPackets);

    // Clear the points to indicate there is not other work.
    // This implementation depends on WorkSplitterCalculationTaskHandler to split the tasks.
    // Once WorkSplitterCalculationTaskHandler is being removed this implementation should handle splitting in reasonable sized packets.
    points = List.of();
    return workPacket;
  }

}
