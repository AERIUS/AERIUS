/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.own2000;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.CalculatorOptionsFactory;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.calculation.ExportCalculationResultHandler;
import nl.overheid.aerius.calculation.OwN2000CalculationOptionsUtil;
import nl.overheid.aerius.calculation.SectorAndTotalResultDBHandler;
import nl.overheid.aerius.calculation.TotalResultDBHandler;
import nl.overheid.aerius.calculation.TotalResultDBUnsafeHandler;
import nl.overheid.aerius.calculation.WorkSplitterCalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculatorThemeFactory;
import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.calculation.base.CombinedWorkDistributor;
import nl.overheid.aerius.calculation.base.PostCalculationHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.ResultPostProcessHandler;
import nl.overheid.aerius.calculation.base.WorkBySectorHandlerImpl;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.OPSCalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.ThemeCalculationJobOptions;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.calculation.natura2k.Natura2KReceptorLoader;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.calculation.overlapping.CalculationOverlappingHexagonsHandler;
import nl.overheid.aerius.calculation.results.ResultsSummariesHandler;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationRoadOPS;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.OwN2000CalculationOptions;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.srm.version.AeriusSRMVersion;
import nl.overheid.aerius.util.FeaturePointToEnginePointUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * {@link CalculatorThemeFactory} implementation for PAS Wet-NB calculations.
 */
public class OwN2000CalculatorFactory implements CalculatorThemeFactory {

  private static final Logger LOGGER = LoggerFactory.getLogger(OwN2000CalculatorFactory.class);

  private static final OPSCalculationEngineProvider OPS_CALCULATION_ENGINE_PROVIDER = new OPSCalculationEngineProvider();

  private final PMF pmf;
  private final ReceptorGridSettings rgs;
  private final GridUtil gridUtil;
  private final GridSettings gridSettings;
  private final SectorCategories sectorCategories;
  private final Natura2kGridPointStore n2kPointStore;
  private final Natura2kReceptorStore n2kReceptorStore;
  private final OwN2000CalculationTaskFactory calculationTaskFactory;
  private final OwN2000RelevantResultsFilter own2000IncludeResultsFilter;
  private final DefaultCalculationEngineProvider defaultCalculationEngineProvider;
  private final CalculationOverlappingHexagonsHandler overlappingHexagonsHandler;

  /**
   * Recalculated the receptor id given the x-y coordinates in case a point is a receptor point.
   *
   * @param results list of result points.
   */
  private final ResultPostProcessHandler receptorPointResultsHandler;

  private final OPSVersion opsVersion;
  private final AeriusSRMVersion srmVersion;

  public OwN2000CalculatorFactory(final PMF pmf) throws SQLException, AeriusException {
    this.pmf = pmf;
    rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(pmf);
    final ReceptorUtil receptorUtil = new ReceptorUtil(rgs);

    sectorCategories = SectorRepository.getSectorCategories(pmf, LocaleUtils.getDefaultLocale());
    gridSettings = new GridSettings(rgs);
    gridUtil = new GridUtil(gridSettings);
    n2kPointStore = loadNatura2kGridPointStore(pmf, receptorUtil);
    n2kReceptorStore = loadNatura2kReceptorStore(pmf, receptorUtil);
    calculationTaskFactory = new OwN2000CalculationTaskFactory(rgs, pmf);
    own2000IncludeResultsFilter = new OwN2000RelevantResultsFilter();
    receptorPointResultsHandler = (calculationId, results) -> {
      for (final AeriusResultPoint arp : results) {
        if (arp.getPointType() == AeriusPointType.RECEPTOR) {
          // This looks stupid, but an x-y coordinate for exact center of hexagon has a resolution that is bigger then 1 meter
          // While i.e. OPS has resolution of 1 meter, it means the x-y coordinates back from ops have lost detail information.
          // Therefore first calculate id and then set the exact x-y coordinate for the id.
          arp.setCoordinates(receptorUtil.getPointFromReceptorId(receptorUtil.getReceptorIdFromPoint(arp)).getCoordinates());
        }
      }
    };
    try (final Connection con = pmf.getConnection()) {
      defaultCalculationEngineProvider = new DefaultCalculationEngineProvider(SectorRepository.getSectorProperties(con));
    }
    overlappingHexagonsHandler = new CalculationOverlappingHexagonsHandler(pmf, n2kReceptorStore);
    opsVersion = OPSVersion.getOPSVersionByLabel(ConstantRepository.getString(pmf, ConstantsEnum.MODEL_VERSION_OPS));
    srmVersion = AeriusSRMVersion.getByVersionLabel(ConstantRepository.getString(pmf, ConstantsEnum.MODEL_VERSION_SRM_OWN2000));
  }

  @Override
  public CalculatorOptions getCalculatorOptions(final Connection con, final CalculationInputData inputData) throws SQLException {
    return inputData.getExportType() == ExportType.CALCULATION_UI
        ? CalculatorOptionsFactory.initUICalculatorOptions(con)
        : CalculatorOptionsFactory.initWorkerCalculationOptions(con);
  }

  /**
   * Load Natura2000 area data into Natura2kReceptorStore.
   *
   * @param pmf
   * @param receptorUtil
   * @return
   * @throws SQLException
   * @throws AeriusException
   */
  private Natura2kReceptorStore loadNatura2kReceptorStore(final PMF pmf, final ReceptorUtil receptorUtil) throws SQLException, AeriusException {
    final Natura2KReceptorLoader receptorLoader = new Natura2KReceptorLoader(receptorUtil, gridSettings);
    try (final Connection con = pmf.getConnection()) {
      return receptorLoader.fillReceptorStore(con);
    }
  }

  /**
   * Load Natura2000 area data into Natura2kGridPointStore.
   *
   * @param pmf
   * @param receptorUtil
   * @return
   * @throws SQLException
   * @deprecated gridpointstore is for deprecated aggregation.
   */
  @Deprecated
  private Natura2kGridPointStore loadNatura2kGridPointStore(final PMF pmf, final ReceptorUtil receptorUtil) throws SQLException {
    final Natura2KReceptorLoader receptorLoader = new Natura2KReceptorLoader(receptorUtil, gridSettings);
    try (final Connection con = pmf.getConnection()) {
      return receptorLoader.fillPointStore(con);
    }
  }

  @Override
  public void prepareOptions(final ScenarioCalculations scenarioCalculations) {
    final CalculationSetOptions options = scenarioCalculations.getOptions();

    if (options.getCalculationMethod() == CalculationMethod.FORMAL_ASSESSMENT) {
      scenarioCalculations.getScenario().setOptions(OwN2000CalculationOptionsUtil.createOwN2000CalculationSetOptions());
      scenarioCalculations.getScenario().getOptions().setCalculationJobType(options.getCalculationJobType());
    }
    final OwN2000CalculationOptions own2000CalculationOptions = scenarioCalculations.getScenario().getOptions().getOwN2000CalculationOptions();

    own2000CalculationOptions.setOpsVersion(opsVersion.getVersion());
    own2000CalculationOptions.setSrmVersion(srmVersion.getVersion());
    if (!own2000CalculationOptions.isUseReceptorHeights()) {
      scenarioCalculations.getCalculationPoints().forEach(OwN2000CalculatorFactory::clearPointHeight);
    }
  }

  private static void clearPointHeight(final CalculationPointFeature feature) {
    if (feature.getProperties() instanceof final CustomCalculationPoint ccp) {
      ccp.setHeight(null);
    }
  }

  @Override
  public ThemeCalculationJobOptions getThemeCalculationJobOptions(final CalculationInputData inputData, final CalculationJob calculationJob) {
    final OwN2000CalculationOptions own2000CalculationOptions = inputData.getScenario().getOptions().getOwN2000CalculationOptions();
    final OwN2000CalculationJobOptions options = new OwN2000CalculationJobOptions();

    options.setOpsVersion(OPSVersion.getOPSVersionByLabel(own2000CalculationOptions.getOpsVersion()));
    options.setSrmVersion(AeriusSRMVersion.getByVersionLabel(own2000CalculationOptions.getSrmVersion()));
    return options;
  }

  @Override
  public CalculationEngineProvider getProvider(final CalculationJob calculationJob) {
    return calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions().getRoadOPS() == CalculationRoadOPS.DEFAULT
        ? defaultCalculationEngineProvider
        : OPS_CALCULATION_ENGINE_PROVIDER;
  }

  @Override
  public List<CalculationResultHandler> getCalculationResultHandler(final CalculationJob calculationJob, final ExportType exportType,
      final Date exportDate) {
    return List.of(createResultStorageHandler(calculationJob));
  }

  /**
   * Sets the {@link CalculationResultHandler} on the calculator.
   * @return this object
   */
  private CalculationResultHandler createResultStorageHandler(final CalculationJob calculationJob) {
    final ExportType exportType = calculationJob.getExportType();
    final CalculationResultHandler handler;

    if (exportType == ExportType.CSV) {
      LOGGER.trace("Add CsvOutputHandler");
      handler = new CsvOutputHandler(pmf, calculationJob);
      calculationJob.setUnitsAccumulator();
    } else if (exportType == ExportType.GML_WITH_SECTORS_RESULTS) {
      LOGGER.trace("Add SectorAndTotalResultDBHandler");
      handler = new SectorAndTotalResultDBHandler(pmf, getIncludeResultFilter(calculationJob));
    } else if (exportType == ExportType.CALCULATION_UI) {
      LOGGER.trace("Add TotalResultDBUnsafeHandler");
      handler = new TotalResultDBUnsafeHandler(pmf, getIncludeResultFilter(calculationJob));
    } else if (calculationJob.getCommandType() == CommandType.EXPORT) {
      handler = new ExportCalculationResultHandler(calculationJob);
    } else {
      LOGGER.trace("Add TotalResultDBHandler");
      handler = new TotalResultDBHandler(pmf, getIncludeResultFilter(calculationJob));
    }
    return handler;
  }

  @Override
  public void prepareResultHandler(final ResultHandler resultHandler, final CalculationJob calculationJob) {
    resultHandler.addResultUpdateHandler(receptorPointResultsHandler);
    if (calculationJob.getCalculationSetOptions().getCalculationMethod() == CalculationMethod.CUSTOM_RECEPTORS) {
      resultHandler.addResultUpdateHandler(new CustomReceptorHandler(rgs, calculationJob));
    }
    addOffSiteReductionPostProcessorIfNeeded(resultHandler, calculationJob);
  }

  @Override
  public WorkHandler<AeriusPoint> createWorkHandler(final CalculationJob calculationJob, final CalculationTaskHandler remoteWorkHandler)
      throws AeriusException {
    final CalculationTaskHandler accumulatorHandler = calculationJob.isUnitsAccumulator()
        ? new UnitAccumulatorTaskHandler(calculationJob, remoteWorkHandler)
        : remoteWorkHandler;

    return new WorkBySectorHandlerImpl(calculationTaskFactory, accumulatorHandler);
  }

  private static void addOffSiteReductionPostProcessorIfNeeded(final ResultHandler resultHandler, final CalculationJob calculationJob) {
    if (OffSiteReductionResultPostProcessHandler.canApply(calculationJob.getScenarioCalculations())) {
      resultHandler.addResultUpdateHandler(new OffSiteReductionResultPostProcessHandler(calculationJob.getScenarioCalculations()));
    }
  }

  /**
   * Returns the filter if results below a threshold should be ignored.
   * @return
   */
  private IncludeResultsFilter getIncludeResultFilter(final CalculationJob calculationJob) {
    return CalculationMethod.FORMAL_ASSESSMENT == calculationJob.getCalculationSetOptions().getCalculationMethod()
        ? own2000IncludeResultsFilter
        : null;
  }

  /**
   * Determines if the {@link WorkSplitterCalculationTaskHandler} should be added. This is needed when the {@link WorkDistributor}
   * doesn't split the calculation points into reasonable sized tasks.
   */
  @Override
  public CalculationTaskHandler wrapCalculationTaskHandler(final CalculationJob calculationJob, final CalculationTaskHandler calculationTaskHandler) {
    if (calculationJob.getCalculationSetOptions().getCalculationMethod() == CalculationMethod.NATURE_AREA) {
      final CalculatorOptions calculatorOptions = calculationJob.getCalculatorOptions();

      return new WorkSplitterCalculationTaskHandler(calculationTaskHandler,
          calculatorOptions.getMaxCalculationEngineUnits(), calculatorOptions.getMinReceptor());
    } else {
      return calculationTaskHandler;
    }
  }

  @Override
  public List<PostCalculationHandler> getPostCalculationHandlers(final CalculationJob calculationJob) throws AeriusException {
    final List<PostCalculationHandler> handlers = new ArrayList<>();

    final boolean isCustomPoints = calculationJob.getCalculationSetOptions().getCalculationMethod() == CalculationMethod.CUSTOM_POINTS;
    if (!isCustomPoints) {
      handlers.add(overlappingHexagonsHandler);
    }
    if (calculationJob.isJobWithResultSummaries()) {
      final ResultsSummariesHandler resultsSummariesHandler = new ResultsSummariesHandler(pmf);
      handlers.add(resultsSummariesHandler);
    }
    if (calculationJob.getExportType() == ExportType.REGISTER_JSON_PROJECT_CALCULATION) {
      handlers
          .add(new ProjectCalculationExportHandler(pmf, calculationJob, SummaryHexagonType.EXCEEDING_HEXAGONS, OverlappingHexagonType.ALL_HEXAGONS));
    }
    return handlers;
  }

  @Override
  public CalculationResultQueue createCalculationResultQueue() {
    return new OwN2000CalculationResultQueue();
  }

  @Override
  public WorkDistributor<AeriusPoint> createWorkDistributor(final CalculationJob calculationJob) throws AeriusException, SQLException {
    final WorkDistributor<AeriusPoint> wd;
    final CalculationMethod ct = calculationJob.getCalculationSetOptions().getCalculationMethod();

    switch (ct) {
      case CUSTOM_POINTS:
        wd = createCustomPointWorkDistributor(calculationJob);
        break;
      case CUSTOM_RECEPTORS:
        wd = createOwN2000WorkDistributor();
        break;
      case FORMAL_ASSESSMENT:
        wd = createOwN2000WithCustomPointsWorkDistributor(calculationJob);
        break;
      case NATURE_AREA:
        if (calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions().isForceAggregation()) {
          wd = new OwN2000AggregatedWorkDistributor(pmf, sectorCategories, gridUtil, n2kPointStore);
        } else {
          wd = createOwN2000WorkDistributor();
        }
        break;
      default:
        LOGGER.error("Unsupported calculation method:'{}'", ct);
        throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    return wd;
  }

  private WorkDistributor<AeriusPoint> createCustomPointWorkDistributor(final CalculationJob calculationJob) throws AeriusException {
    final int subReceptorsZoomLevel = Optional
        .ofNullable(calculationJob.getCalculationSetOptions().getOwN2000CalculationOptions().getSubReceptorZoomLevel()).orElse(1);
    final HexagonZoomLevel hexagonZoomLevel =
        rgs.getHexagonZoomLevels().stream().filter(zl -> zl.getLevel() == subReceptorsZoomLevel).findFirst().orElse(null);
    if (hexagonZoomLevel == null) {
      final String availableZoomLevels = rgs.getHexagonZoomLevels().stream().map(zl -> String.valueOf(zl.getLevel()))
          .collect(Collectors.joining(", "));
      throw new AeriusException(AeriusExceptionReason.CONNECT_INVALID_ZOOM_LEVEL, String.valueOf(subReceptorsZoomLevel), availableZoomLevels);
    }
    final List<AeriusPoint> points = FeaturePointToEnginePointUtil.convert(calculationJob.getScenarioCalculations().getCalculationPoints());

    return new CustomPointsWorkDistributor(pmf, sectorCategories, gridUtil, points, hexagonZoomLevel);
  }

  private OwN2000DistanceWorkDistributor createOwN2000WorkDistributor() {
    return new OwN2000DistanceWorkDistributor(pmf, n2kReceptorStore, sectorCategories, rgs);
  }

  private WorkDistributor<AeriusPoint> createOwN2000WithCustomPointsWorkDistributor(final CalculationJob calculationJob) throws AeriusException {
    final OwN2000DistanceWorkDistributor own2000WorkDistributor = createOwN2000WorkDistributor();

    return calculationJob.getScenarioCalculations().getCalculationPoints().isEmpty() ? own2000WorkDistributor
        : new CombinedWorkDistributor(own2000WorkDistributor, createCustomPointWorkDistributor(calculationJob));
  }
}
