/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.SQLException;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Combined WorkDistributor that sequentially works with 2 distributors
 * It first calculates the first distributor, and then the second distributor.
 */
public class CombinedWorkDistributor implements WorkDistributor<AeriusPoint>, CalculationResultHandler {

  private final WorkDistributor<AeriusPoint> firstWorkDistributor;
  private final WorkDistributor<AeriusPoint> secondWorkDistributor;
  private boolean firstWDHasWork;

  public CombinedWorkDistributor(final WorkDistributor<AeriusPoint> firstWorkDistributor, final WorkDistributor<AeriusPoint> secondWorkDistributor) {
    this.firstWorkDistributor = firstWorkDistributor;
    this.secondWorkDistributor = secondWorkDistributor;
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    firstWorkDistributor.init(calculationJob);
    secondWorkDistributor.init(calculationJob);
  }

  @Override
  public boolean prepareWork() throws AeriusException {
    synchronized (this) {
      firstWDHasWork = firstWorkDistributor.prepareWork();
      return firstWDHasWork || secondWorkDistributor.prepareWork();
    }
  }

  @Override
  public WorkPacket<AeriusPoint> work() throws AeriusException {
    synchronized (this) {
      return firstWDHasWork ? firstWorkDistributor.work() : secondWorkDistributor.work();
    }
  }

  @Override
  public void onCompleted() throws AeriusException {
    firstWorkDistributor.onCompleted();
    secondWorkDistributor.onCompleted();
  }
}
