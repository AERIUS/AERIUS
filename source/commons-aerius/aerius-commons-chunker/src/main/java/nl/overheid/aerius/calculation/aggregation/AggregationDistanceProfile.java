/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Stuck in an enum because these values are immutable.
 */
enum AggregationDistanceProfile implements AggregationProfile {
  // When aggregation is disabled the aggregation is simply started after 310 km
  DISABLED(310000, 330000, 360000, 392000, 448000, 540000),

  STABLES(10000, 30000, 60000, 92000, 148000, 240000),

  INDUSTRY(20000, 40000, 80000, 160000, 320000, 496000),

  ROAD(2500, 4000, 6000, 8000, 16000, 32000),

  /**
   * Purportedly this profile contains flight sectors (stijgen, landen, taxien).
   */
  SOME_OTHER(10000, 30000, 60000, 92000, 148000, 240000),

  /**
   * Supposed to be a catch-all profile.
   */
  ALL_OTHER(10000, 30000, 60000, 92000, 148000, 240000);

  private Map<Integer, Integer> distances;

  private AggregationDistanceProfile(final int... distanceValues) {
    this.distances = new LinkedHashMap<>();

    int i = 1;
    for (final int distance : distanceValues) {
      getDistances().put(i, distance);
      i++;
    }
  }

  @Override
  public Map<Integer, Integer> getDistances() {
    return distances;
  }
}
