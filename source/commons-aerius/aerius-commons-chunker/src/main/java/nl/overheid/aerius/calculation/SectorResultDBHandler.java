/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.List;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link CalculationResultHandler} to store only the sector results into the database.
 */
public class SectorResultDBHandler extends SectorAndTotalResultDBHandler {

  public SectorResultDBHandler(final PMF pmf, final IncludeResultsFilter filter) {
    super(pmf, filter);
  }

  @Override
  public void onTotalResults(final List<AeriusResultPoint> result, final int calculationId) throws AeriusException {
    // Override to not call super that stores the totals.
  }
}
