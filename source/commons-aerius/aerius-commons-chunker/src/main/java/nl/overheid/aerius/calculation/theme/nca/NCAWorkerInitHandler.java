/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.util.BoundingBoxUtil;
import nl.overheid.aerius.calculation.base.WorkInitHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link NCAWorkerInitHandler} specific for ADMS calculations.
 * Will determine the met surface characteristics and compute the extent of all sources/points.
 */
class NCAWorkerInitHandler implements WorkInitHandler<AeriusPoint> {
  private static final Logger LOGGER = LoggerFactory.getLogger(NCAWorkerInitHandler.class);
  private final PMF pmf;

  public NCAWorkerInitHandler(final PMF pmf) {
    this.pmf = pmf;
  }

  @Override
  public void init(final CalculationJob calculationJob, final List<WorkPacket<AeriusPoint>> packets) throws AeriusException {
    calculationJob.setAdmsExtent(getExtent(packets));
  }

  private static BBox getExtent(final List<WorkPacket<AeriusPoint>> packets) throws AeriusException {
    if (packets.isEmpty()) {
      return null;
    }
    final List<EngineSource> sources = packets.get(0).getJobPackets().stream().flatMap(NCAWorkerInitHandler::packets2Sources).toList();

    return BoundingBoxUtil.getExtendedBoundingBox(sources, packets.stream().flatMap(p -> p.getPoints().stream()).toList());
  }

  private static Stream<EngineSource> packets2Sources(final JobPacket jp) {
    return jp.getSourcesPackets().stream().flatMap(gsp -> gsp.getSources().stream());
  }

}
