/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;

/**
 * Monitors the amount of tasks concurrently processed.
 */
public interface WorkMonitor {

  /**
   * This will release all blocking processes. Use this when the job is cancelled and all other processes should be cancelled.
   */
  void drain();

  /**
   * Returns the work key given the task id.
   * @param taskId task id
   * @return work key
   */
  WorkKey getWorkKey(String taskId);

  /**
   * @return Returns true if the task was cancelled.
   */
  boolean isCancelled();

  /**
   * Call when a task failed with an exception.
   * @param taskId the id of the task that failed.
   */
  void taskFailed(String taskId, Exception exception);

  /**
   * Each new task should be registered. This method may block the call if the number of tasks currently running exceeds a threshold. The method waits
   * until a new slot becomes available. The method returns a unique id that it binds to the given work key. This unique id should be used
   * unregister the task when completed.
   *
   * @param workKey key representing the task
   * @return unique key that is bound to the given
   * @throws InterruptedException
   * @throws TaskCancelledException thrown when the task is cancelled.
   */
  String registerTask(WorkKey workKey) throws InterruptedException, TaskCancelledException;

  /**
   * Unregisters the task. Tasks should be unregistered after all operations for the task have been processed. Pass the unique id given during
   * registration of the task to unregister the task.
   * @param taskId task id
   */
  void unRegisterTask(String taskId);
}
