/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.sql.SQLException;
import java.util.List;
import java.util.Map.Entry;

import nl.aerius.adms.domain.ADMSSubReceptor;
import nl.aerius.adms.domain.ADMSSubReceptorCreator;
import nl.overheid.aerius.archive.ArchiveProxy;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.conversion.JobPackets.SourceConverterSupplier;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.natura2k.AbstractNatura2KWorkDistributor;
import nl.overheid.aerius.calculation.natura2k.EmissionSourceTreeBuilder;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.calculation.subreceptor.SubReceptorPointSupplier;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.decision.DecisionFrameworkRepository;
import nl.overheid.aerius.function.AeriusFunction;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.shared.domain.calculation.SubReceptorsMode;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Distributor for NCA theme.
 */
class NCANatura2kWorkDistributor extends AbstractNatura2KWorkDistributor implements CalculationResultHandler {

  /**
   * Minimum distance a source points must be away from a subreceptor point. If within this distance the subreceptor point is ignored.
   */
  private static final double SUBRECEPTORS_MIN_DISTANCE = 2D;
  /**
   * Only place subreceptors in habitats
   */
  private static final SubReceptorsMode SUBRECEPTORS_ONLY_INSIDE_HABITATS = SubReceptorsMode.ENABLED;

  private final ADMSSubReceptorCreator subReceptorCreator;
  private final HexagonZoomLevel hexagonZoomLevel;
  private final NCABackgroundSupplier backgroundSupplier;
  private final ArchiveProxy archiveProxy;
  private final DevelopmentPressureSearchHandler developmentPressureSearchHandler;
  private AeriusFunction<AeriusPoint, Boolean> checkPointFunction;
  private List<Integer> years = List.of();

  private SubReceptorPointSupplier<ADMSSubReceptor> additionalPointSupplier;
  private NCAFractionNO2Supplier fractionNO2Supplier;
  private ArchiveCollectorHandler archiveCollectorHandler;
  private List<String> meteoYears = List.of();

  public NCANatura2kWorkDistributor(final PMF pmf, final Natura2kReceptorStore n2kReceptorStore, final SectorCategories sectorCategories,
      final ReceptorGridSettings receptorGridSettings, final NCABackgroundSupplier ncaBackgroundSupplier,
      final ArchiveProxy archiveProxy) {
    super(pmf, n2kReceptorStore, sectorCategories, receptorGridSettings);
    this.archiveProxy = archiveProxy;
    hexagonZoomLevel = receptorGridSettings.getZoomLevel1();
    subReceptorCreator = new ADMSSubReceptorCreator(hexagonZoomLevel.getHexagonRadius());
    backgroundSupplier = ncaBackgroundSupplier;
    developmentPressureSearchHandler = new DevelopmentPressureSearchHandler(
        new DecisionFrameworkRepository(pmf, null), new JobRepositoryBean(pmf), archiveProxy);
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    years = calculationJob.getScenarioCalculations().getCalculationYears().values().stream().distinct().toList();
    meteoYears = calculationJob.getCalculationSetOptions().getNcaCalculationOptions().getAdmsOptions().getMetYears();
    final CalculationSetOptions options = calculationJob.getCalculationSetOptions();
    final double maxDistanceM = options.getCalculateMaximumRange();

    final EmissionSourceTreeBuilder esb = calculationJob.getSourcesTreeBuilder();
    final EmissionSourceListSTRTree sourcesTree = esb.getRelevantSituationSourcesTree();

    additionalPointSupplier = new SubReceptorPointSupplier<>(pmf, subReceptorCreator, hexagonZoomLevel, SUBRECEPTORS_MIN_DISTANCE,
        esb.getAllSourcesTree(), SUBRECEPTORS_ONLY_INSIDE_HABITATS);
    checkPointFunction = p -> sourcesTree.findShortestDistance(p) <= maxDistanceM;
    final NCACalculationOptions ncaOptions = options.getNcaCalculationOptions();
    fractionNO2Supplier = new NCAFractionNO2Supplier(ncaOptions.getRoadLocalFractionNO2ReceptorsOption(), ncaOptions.getRoadLocalFractionNO2());
    final List<Entry<Integer, Integer>> listn2k = n2kReceptorStore.order(sourcesTree).stream()
        .filter(e -> e.getValue() <= maxDistanceM)
        .toList();
    init(calculationJob, listn2k);
    archiveCollectorHandler = getArchiveCollectorHandler(calculationJob);
    developmentPressureSearchHandler.handleSearch(calculationJob);
  }

  private ArchiveCollectorHandler getArchiveCollectorHandler(final CalculationJob calculationJob) throws AeriusException {
    if (calculationJob.getCalculationSetOptions().isUseInCombinationArchive()) {
      final int year = years.stream().max(Integer::compareTo).orElse(0);
      final int proposedCalculationId = calculationJob.getCalculations().stream().filter(s -> s.getSituationType() == SituationType.PROPOSED)
          .findFirst().map(s -> s.getCalculationId()).orElseThrow(() -> new AeriusException(AeriusExceptionReason.CONNECT_SITUATION_NO_PROPOSED));

      return new ArchiveCollectorHandler(pmf, archiveProxy, proposedCalculationId, year);
    } else {
      return null;
    }
  }

  @Override
  public WorkPacket<AeriusPoint> work() {
    return ADMSWorkExpander.expand(meteoYears, super.work());
  }

  @Override
  protected SourceConverterSupplier createSourceConverterSupplier(final CalculationJob calculationJob) {
    return (con, situation) -> new ADMSSourceConverter(EPSG.BNG.getSrid(), situation, sectorCategories);
  }

  @Override
  protected boolean checkPoint(final AeriusPoint aeriusPoint) throws AeriusException {
    return checkPointFunction.apply(aeriusPoint) && super.checkPoint(aeriusPoint);
  }

  /**
   * Add the point(s) to calculate
   * If sub receptor points need to be calculated it adds the sub receptor points here.
   * If a sub receptor these points are added as {@link ADMSSubReceptor} points so it's easy to track which points need special treatment.
   */
  @Override
  protected int addPoint(final List<AeriusPoint> pointsToCalculate, final AeriusPoint aeriusPoint) throws AeriusException {
    final ADMSCalculationPoint copy = new ADMSCalculationPoint(aeriusPoint, aeriusPoint.getPointType());

    backgroundSupplier.setBackgroundNOx(copy, years);
    fractionNO2Supplier.setFractionNO2(copy);
    return additionalPointSupplier.addPoint(pointsToCalculate, copy);
  }

  @Override
  protected void postProcessPointsToDo(final List<AeriusPoint> pointsToCalculate) throws AeriusException {
    if (archiveCollectorHandler != null) {
      archiveCollectorHandler.addPoints(pointsToCalculate);
    }
  }

  @Override
  public void onCompleted() throws AeriusException {
    if (archiveCollectorHandler != null) {
      archiveCollectorHandler.requestArchiveData();
    }
  }
}
