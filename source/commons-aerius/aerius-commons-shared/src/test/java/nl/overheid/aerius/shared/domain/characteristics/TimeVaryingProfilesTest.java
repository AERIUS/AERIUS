/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.characteristics;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;

class TimeVaryingProfilesTest {

  private static final String CODE_1 = "SomeCode";
  private static final String CODE_2 = "SomeOtherCode";
  private static final String CODE_3 = "Another code 2022";

  private static final int SECTOR_WITH_DEFAULTS = 9;
  private static final int YEAR_WITH_DIFFERENT_DEFAULT = 2018;
  private static final CustomTimeVaryingProfileType TYPE = CustomTimeVaryingProfileType.THREE_DAY;

  @Test
  void testDefaultProfileForSectorAndYear() {
    final TimeVaryingProfiles timeVaryingProfiles = new TimeVaryingProfiles();
    final StandardTimeVaryingProfile defaultProfile = mockStandard(CODE_1);
    final StandardTimeVaryingProfile differentDefaultProfile = mockStandard(CODE_3);
    final List<StandardTimeVaryingProfile> standardProfiles = List.of(
        defaultProfile,
        mockStandard(CODE_2),
        differentDefaultProfile);
    timeVaryingProfiles.setStandardTimeVaryingProfiles(TYPE, standardProfiles);
    timeVaryingProfiles.setSectorDefaultStandardProfileCodes(TYPE, Map.of(9, CODE_1));
    timeVaryingProfiles.setSectorYearDefaultStandardProfileCodes(TYPE, Map.of(9, Map.of(YEAR_WITH_DIFFERENT_DEFAULT, CODE_3)));

    StandardTimeVaryingProfile foundProfile =
        timeVaryingProfiles.defaultProfileForSectorAndYear(TYPE, SECTOR_WITH_DEFAULTS, YEAR_WITH_DIFFERENT_DEFAULT);
    assertEquals(differentDefaultProfile, foundProfile, "Correct match when there is a default for sector + year");

    foundProfile = timeVaryingProfiles.defaultProfileForSectorAndYear(TYPE, SECTOR_WITH_DEFAULTS, 2030);
    assertEquals(defaultProfile, foundProfile, "Correct match when there is a default for sector + year, but other year is specified");

    foundProfile = timeVaryingProfiles.defaultProfileForSectorAndYear(TYPE, 929, YEAR_WITH_DIFFERENT_DEFAULT);
    assertNull(foundProfile, "When requesting for a sector without defaults, nothing is found");
  }

  @Test
  void testDetermineStandardTimeVaryingProfile() {
    final TimeVaryingProfiles timeVaryingProfiles = new TimeVaryingProfiles();
    final StandardTimeVaryingProfile profile1 = mockStandard(CODE_1);
    final StandardTimeVaryingProfile profile2 = mockStandard(CODE_2);
    final StandardTimeVaryingProfile profile3 = mockStandard(CODE_3);
    final List<StandardTimeVaryingProfile> standardProfiles = List.of(
        profile1,
        profile2,
        profile3);
    timeVaryingProfiles.setStandardTimeVaryingProfiles(TYPE, standardProfiles);

    assertEquals(profile1, timeVaryingProfiles.determineStandardTimeVaryingProfile(TYPE, CODE_1), "Correct match for code 1");
    assertEquals(profile2, timeVaryingProfiles.determineStandardTimeVaryingProfile(TYPE, CODE_2), "Correct match for code 2");
    assertEquals(profile3, timeVaryingProfiles.determineStandardTimeVaryingProfile(TYPE, CODE_3), "Correct match for code 3");
    assertNull(timeVaryingProfiles.determineStandardTimeVaryingProfile(TYPE, "Something unknown"), "When requesting a unknown code, nothing is found");
  }

  private StandardTimeVaryingProfile mockStandard(final String code) {
    final StandardTimeVaryingProfile mock = mock(StandardTimeVaryingProfile.class);
    when(mock.getCode()).thenReturn(code);
    return mock;
  }
}
