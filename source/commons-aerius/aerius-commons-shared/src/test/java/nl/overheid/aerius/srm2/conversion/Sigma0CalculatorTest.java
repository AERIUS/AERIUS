/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrierType;

/**
 * Test class for {@link Sigma0Calculator}.
 */
public class Sigma0CalculatorTest {

  @Test
  public void testgetElevationHeight() {
    assertEquals(-6, Sigma0Calculator.getElevationHeight(-20), "-20 should be -6");
    assertEquals(-5, Sigma0Calculator.getElevationHeight(-5), "-5 should be -5");
    assertEquals(0, Sigma0Calculator.getElevationHeight(0), "0 should be 0");
    assertEquals(8, Sigma0Calculator.getElevationHeight(8), "8 should be 8");
    assertEquals(12, Sigma0Calculator.getElevationHeight(20), "20 should be 12");
  }

  /**
   * De startwaarde voor de verticale dispersie σz,0 hangt zogezegd af van het type omgeving:
   * <ul>
   * <li>buiten de bebouwde kom, de weg is geen autosnelweg: σz,0 = 2,5 [m];
   * <li>buiten de bebouwde kom, de weg is een autosnelweg: σz,0 = 3 [m].
   * </ul>
   */
  @Test
  public void testSigma0StartValue() {
    final double sigma0NonUrbanRoad = Sigma0Calculator.getSigma0(false, RoadElevation.NORMAL, 0, null, null);
    assertEquals(2.5, sigma0NonUrbanRoad, 0.00001, "Sigma0 non urban road");

    final double sigma0Freeway = Sigma0Calculator.getSigma0(true, RoadElevation.NORMAL, 0, null, null);
    assertEquals(3.0, sigma0Freeway, 0.00001, "Sigma0 freeway");
  }

  /**
   * Test the Sigma z0 for tunnelbak and steep dykes.
   */
  @Test
  public void testSigma0RoadHeight() {
    assertEquals(3.0 + (6 / 4.0), Sigma0Calculator.getSigma0(true, RoadElevation.TUNNEL, -30, null, null),
        0.00001, "Sigma0 tunnel -30");
    assertEquals(3.0 + (6 / 4.0), Sigma0Calculator.getSigma0(true, RoadElevation.TUNNEL, -15, null, null),
        0.00001, "Sigma0 tunnel -15");
    assertEquals(3.0 + (3 / 4.0), Sigma0Calculator.getSigma0(true, RoadElevation.TUNNEL, -3, null, null),
        0.00001, "Sigma0 tunnel -3");
    assertEquals(3.0 + (2 / 2.0), Sigma0Calculator.getSigma0(true, RoadElevation.STEEP_DYKE, 2, null, null),
        0.00001, "Sigma0 steep dyke 2");
    assertEquals(3.0 + (12 / 2.0), Sigma0Calculator.getSigma0(true, RoadElevation.STEEP_DYKE, 15, null, null),
        0.00001, "Sigma0 steep dyke 15");
    assertEquals(3.0 + (12 / 2.0), Sigma0Calculator.getSigma0(true, RoadElevation.STEEP_DYKE, 30, null, null),
        0.00001, "Sigma0 steep dyke 30");
    assertEquals(3.0, Sigma0Calculator.getSigma0(true, RoadElevation.NORMAL, 0, null, null),
        0.00001, "Sigma0 normal");
  }

  /**
   * Op het moment dat het wegvak verhoogd of verdiept ligt ten opzichte van het maaiveld, wordt σz,0 afhankelijk van
   * het type verhoging of verdieping gecorrigeerd:
   * <ul>
   * <li>dijk of wal met zeer vlakke zijkanten (hoek kleiner dan 20°): Er wordt geen correctie bij σz,0 opgeteld;
   * <li>dijk of wal met vlakke zijkanten (hoek groter of gelijk aan 20° maar kleiner dan 45°): Er wordt h/4 bij σz,0
   * opgeteld, waarbij h de hoogte van de dijk is;
   * <li>dijk of wal met scherpe zijkanten (hoek groter dan of gelijk aan 45°): Er wordt h/2 bij σz,0 opgeteld, waarbij
   * h de hoogte van de dijk is;
   * <li>viaduct: Er wordt h bij σz,0 opgeteld, waarbij h de hoogte van het viaduct is;
   * <li>tunnelbak CIMLK: Er wordt d/4 bij σz,0 opgeteld, waarbij d de diepte van de tunnelbak is. (Dit maakt gebruik van nieuwe factor 4)
   * <li>tunnelbak NBWet: Er wordt d/2 bij σz,0 opgeteld, waarbij d de diepte van de tunnelbak is. (Dit maakt gebruik van oude factor 2)
   * </ul>
   */
  @Test
  public void testSigma0RoadElevation() {
    final double sigma0Normal = Sigma0Calculator.getSigma0(false, RoadElevation.NORMAL, 10, null, null);
    assertEquals(2.5, sigma0Normal, 0.00001, "Sigma0 very low sides");

    final double sigma0NormalDyke = Sigma0Calculator.getSigma0(false, RoadElevation.NORMAL_DYKE, 10, null, null);
    assertEquals(2.5 + 10 / 4.0, sigma0NormalDyke, 0.00001, "Sigma0 flat sides");

    final double sigma0SteepDyke = Sigma0Calculator.getSigma0(false, RoadElevation.STEEP_DYKE, 10, null, null);
    assertEquals(2.5 + 10 / 2.0, sigma0SteepDyke, 0.00001, "Sigma0 sharp sides");

    final double sigma0Viaduct = Sigma0Calculator.getSigma0(false, RoadElevation.VIADUCT, 10, null, null);
    assertEquals(2.5 + 10, sigma0Viaduct, 0.00001, "Sigma0 viaduct");

    final double sigma0TunnelNBWet = Sigma0Calculator.getSigma0(false, RoadElevation.TUNNEL, 10, null, null);
    assertEquals(2.5 + 10 / 4.0, sigma0TunnelNBWet, 0.00001, "Sigma0 tunnel");

    final double sigma0TunnelCIMLK = Sigma0Calculator.getSigma0(false, RoadElevation.TUNNEL, 10, null, null);
    assertEquals(2.5 + 10 / 4.0, sigma0TunnelCIMLK, 0.00001, "Sigma0 tunnel");
  }

  /**
   * Op het moment dat er aan één of twee zijden op een afstand kleiner dan 50 meter van de wegrand een scherm of wal
   * met een hoogte van ten minste 1 meter aanwezig is, wordt σz,0 nogmaals gecorrigeerd, afhankelijk van de
   * configuratie:
   * <ul>
   * <li>aan de linker- of rechterzijde een scherm: Er wordt h/2 bij σz,0 opgeteld, waarbij h de hoogte van het scherm
   * is;
   * <li>aan de linker- en rechterzijde een scherm: Er wordt (h1+h2)/2 bij σz,0 opgeteld, waarbij h1 en h2 de hoogten
   * van de schermen zijn;
   * <li>aan de linker- of rechterzijde een wal: Er wordt h/4 bij σz,0 opgeteld, waarbij h de hoogte van de wal is;
   * <li>aan de linker- en rechterzijde een wal: Er wordt (h1+h2)/4 bij σz,0 opgeteld, waarbij h1 en h2 de hoogten van
   * de wallen zijn;
   * <li>aan de ene zijde een wal met hoogte h1, aan de andere zijde een scherm met hoogte h2: Er wordt h1/4+h2/2 bij
   * σz,0 opgeteld.
   * </ul>
   * De maximale hoogte voor een wal of scherm is 6 meter.
   */
  @Test
  public void testSigma0Barrier() {
    final int[] heights = {0, 1, 2, 5, 6, 8};
    final int[] distances = {10, 50, 100};

    for (final int h : heights) {
      for (final int d : distances) {
        assertSigma0Barrier(SRM2RoadSideBarrierType.SCREEN, h, d);
        assertSigma0Barrier(SRM2RoadSideBarrierType.WALL, h, d);
      }
    }
  }

  private void assertSigma0Barrier(final SRM2RoadSideBarrierType barrierType, final int height, final int distance) {
    final boolean inrange = distance < 50 && height >= 1;
    // height used to calculate expected value.
    final int ch = Math.min(height, 6);
    final double factor = getBarrierFactor(barrierType);

    final SRM2RoadSideBarrier rsbBarrier = new SRM2RoadSideBarrier();
    rsbBarrier.setBarrierType(barrierType);
    rsbBarrier.setDistance(distance);
    rsbBarrier.setHeight(height);

    final double sigma0BarrierLeft = Sigma0Calculator.getSigma0(false, RoadElevation.NORMAL, 0, rsbBarrier, null);
    assertEquals(2.5 + (inrange ? ch
        / factor : 0), sigma0BarrierLeft, 0.00001, "Sigma0 " + barrierType.toString() + " barrier left (distance:" + distance + ", height:" + height + ")");

    final double sigma0BarrierRight = Sigma0Calculator.getSigma0(false, RoadElevation.NORMAL, 0, null,
        rsbBarrier);
    assertEquals(2.5 + (inrange ? ch
        / factor : 0), sigma0BarrierRight, 0.00001, "Sigma0 " + barrierType.toString() + " barrier right (distance:" + distance + ", height:" + height + ")");

    final double sigma0BarrierBoth = Sigma0Calculator.getSigma0(false, RoadElevation.NORMAL, 0, rsbBarrier,
        rsbBarrier);
    assertEquals(2.5 + (inrange ? (ch + ch) / factor : 0),
        sigma0BarrierBoth, 0.00001, "Sigma0 " + barrierType.toString() + " barrier left and right (distance:" + distance + ", height:" + height + ")");
  }

  private double getBarrierFactor(final SRM2RoadSideBarrierType barrierType) {
    final double factor;

    switch (barrierType) {
    case SCREEN:
      factor = 2;
      break;
    case WALL:
      factor = 4;
      break;
    default:
      factor = 0;
      break;
    }
    return factor;
  }
}
