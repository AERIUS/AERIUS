/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;

/**
 * Tests for ApplicationSectionsUtil.
 */
class ApplicationSectionsUtilTest {

  @ParameterizedTest
  @MethodSource("casesForCustomPointsCalculationJobType")
  void testIsCustomPointsUsedCalculationJobType(final CalculationJobType calculationJobType, final boolean expectedResult) {
    assertEquals(expectedResult, ApplicationSectionsUtil.isCustomPointsUsed(calculationJobType), "custom points used for calculation job type");
  }

  private static Stream<Arguments> casesForCustomPointsCalculationJobType() {
    return Stream.of(
        Arguments.of(CalculationJobType.SINGLE_SCENARIO, true),
        Arguments.of(CalculationJobType.PROCESS_CONTRIBUTION, true),
        Arguments.of(CalculationJobType.MAX_TEMPORARY_EFFECT, true),
        Arguments.of(CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION, true),
        Arguments.of(CalculationJobType.DEPOSITION_SUM, false));
  }

  @ParameterizedTest
  @MethodSource("casesForCustomPointsProductProfile")
  void testIsCustomPointsUsedProductProfile(final ProductProfile productProfile, final boolean expectedResult) {
    assertEquals(expectedResult, ApplicationSectionsUtil.isCustomPointsUsed(productProfile), "custom points used for product profile");
  }

  private static Stream<Arguments> casesForCustomPointsProductProfile() {
    return Stream.of(
        Arguments.of(ProductProfile.CALCULATOR, true),
        Arguments.of(ProductProfile.LBV_POLICY, false));
  }

}
