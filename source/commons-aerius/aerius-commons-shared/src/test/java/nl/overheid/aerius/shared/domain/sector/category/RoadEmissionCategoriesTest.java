/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory.RoadEmissionCategoryKey;

/**
 * Test class for {@link RoadEmissionCategories}.
 */
class RoadEmissionCategoriesTest {

  private static final String AUTO_BUS = "AUTO_BUS";
  private static final String LIGHT_TRAFFIC = "LIGHT_TRAFFIC";
  private static final String HEAVY_FREIGHT = "HEAVY_FREIGHT";

  private static final String FREEWAY = "FREEWAY";
  private static final String NON_URBAN_ROAD = "NON_URBAN_ROAD";
  private static final String NATIONAL_ROAD = "NATIONAL_ROAD";
  private static final String URBAN_ROAD = "URBAN_ROAD";

  private static final String NL = "NL";

  private static final RoadEmissionCategory AB_80_NOT_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, AUTO_BUS), false, 80, 0);
  private static final RoadEmissionCategory AB_100_NOT_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, AUTO_BUS), false, 100, 0);
  private static final String ONE_CATEGORY_ALWAYS = "With 1 category, this category should always be returned.";

  private static final RoadEmissionCategory URBAN_NOT_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, URBAN_ROAD, LIGHT_TRAFFIC), false, 0, 0);

  private static final RoadEmissionCategory NON_URBAN_ROAD_LT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, NON_URBAN_ROAD, LIGHT_TRAFFIC), false, 0, 0);
  private static final RoadEmissionCategory NATIONAL_ROAD_LT_80 = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, NATIONAL_ROAD, LIGHT_TRAFFIC), false, 80, 0);
  private static final RoadEmissionCategory NATIONAL_ROAD_LT_100 = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, NATIONAL_ROAD, LIGHT_TRAFFIC), false, 100, 0);

  private static final RoadEmissionCategory LT_50_NOT_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, LIGHT_TRAFFIC), false, 50, 0);
  private static final RoadEmissionCategory LT_80_NOT_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, LIGHT_TRAFFIC), false, 80, 0);
  private static final RoadEmissionCategory LT_80_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, LIGHT_TRAFFIC), true, 80, 0);
  private static final RoadEmissionCategory LT_100_NOT_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, LIGHT_TRAFFIC), false, 100, 0);
  private static final RoadEmissionCategory LT_120_NOT_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, LIGHT_TRAFFIC), false, 120, 0);
  private static final RoadEmissionCategory LT_120_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, LIGHT_TRAFFIC), true, 120, 0);
  private static final RoadEmissionCategory LT_130_NOT_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, LIGHT_TRAFFIC), false, 130, 0);
  private static final RoadEmissionCategory LT_130_STRICT = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, LIGHT_TRAFFIC), true, 130, 0);

  private static final RoadEmissionCategory HF_80_GRAD_0 = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, HEAVY_FREIGHT), false, 80, 0);
  private static final RoadEmissionCategory HF_80_GRAD_10 = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, HEAVY_FREIGHT), false, 80, 10);
  private static final RoadEmissionCategory HF_80_GRAD_NEG_10 = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, HEAVY_FREIGHT), false, 80, -10);
  private static final RoadEmissionCategory HF_100_GRAD_0 = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, HEAVY_FREIGHT), false, 100, 0);
  private static final RoadEmissionCategory HF_100_GRAD_10 = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, HEAVY_FREIGHT), false, 100, 10);
  private static final RoadEmissionCategory HF_100_GRAD_NEG_10 = new RoadEmissionCategory(
      new RoadEmissionCategoryKey(NL, FREEWAY, HEAVY_FREIGHT), false, 100, -10);

  private RoadEmissionCategories categories;

  @BeforeEach
  void before() {
    categories = createData();
  }

  @Test
  void testSpeedCategoriesSrm2() {
    final List<Integer> speeds = categories.getMaximumSpeedCategories(NL, FREEWAY);

    assertNotNull(speeds, "No speed categories found.");
    assertEquals(5, speeds.size(), "Unexpected number of speed categories.");

    assertArrayEquals(new int[] {50, 80, 100, 120, 130}, speeds.stream().mapToInt(i -> i).toArray(), "Unexpected speed categories");

    final List<Integer> nationalRoadSpeeds = categories.getMaximumSpeedCategories(NL, NATIONAL_ROAD);

    assertNotNull(nationalRoadSpeeds, "No speed categories found.");
    assertEquals(2, nationalRoadSpeeds.size(), "Unexpected number of speed categories.");

    assertArrayEquals(new int[] {80, 100}, nationalRoadSpeeds.stream().mapToInt(i -> i).toArray(), "Unexpected speed categories");

    final List<Integer> urbanSpeeds = categories.getMaximumSpeedCategories(NL, URBAN_ROAD);

    assertNotNull(urbanSpeeds, "No speed categories found.");
    assertEquals(0, urbanSpeeds.size(), "Unexpected number of speed categories.");
  }

  @Test
  void testSpeedCategoriesSrm1() {
    final List<Integer> speeds = categories.getMaximumSpeedCategories(NL, FREEWAY);

    assertNotNull(speeds, "No speed categories found.");
    assertEquals(5, speeds.size(), "Unexpected number of speed categories.");

    assertArrayEquals(new int[] {50, 80, 100, 120, 130}, speeds.stream().mapToInt(i -> i).toArray(), "Unexpected speed categories");

    final List<Integer> nonUrbanSpeeds = categories.getMaximumSpeedCategories(NL, NON_URBAN_ROAD);

    assertNotNull(nonUrbanSpeeds, "No speed categories found.");
    assertEquals(0, nonUrbanSpeeds.size(), "Unexpected number of speed categories.");

    final List<Integer> urbanSpeeds = categories.getMaximumSpeedCategories(NL, URBAN_ROAD);

    assertNotNull(urbanSpeeds, "No speed categories found.");
    assertEquals(0, urbanSpeeds.size(), "Unexpected number of speed categories.");
  }

  /**
   * Tests input that is below the lowest speed category.
   */
  @Test
  void testBelowCategory() {
    assertEquals(LT_50_NOT_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, true, 30, null),
        "Lower strict should return strict lowest category");
    assertEquals(LT_50_NOT_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, false, 30, null),
        "Lower not strict should return not strict lowest category");
  }

  /**
   * Tests input that matches speed category.
   */
  @Test
  void testMatchCategory() {
    assertEquals(LT_100_NOT_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, false, 100, null),
        "Match exact speed not strict");
    assertEquals(LT_80_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, true, 80, null),
        "Match exact speed strict");
  }

  @Test
  void testNoExactSpeedCategory() {
    assertEquals(LT_100_NOT_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, false, 90, null),
        "No exact match not strict");
    assertEquals(LT_120_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, true, 110, null),
        "No exact match strict");
    assertEquals(LT_130_NOT_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, true, 130, null),
        "No exact match not strict higher");
  }

  @Test
  void testNoExactButLowerNonStictSpeedCategory() {
    assertEquals(LT_100_NOT_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, true, 90, null),
        "No exact match not strict");
  }

  @Test
  void testSpeedAbove() {
    assertEquals(LT_130_NOT_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, true, 160, null),
        "Find not strict but higher while looking for strict");
    assertEquals(LT_130_NOT_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, false, 160, null),
        "Find not strict but higher while looking for strict");
    categories.add(LT_130_STRICT);
    assertEquals(LT_130_STRICT, categories.findClosestCategory(NL, FREEWAY, LIGHT_TRAFFIC, true, 160, null),
        "Find strict when speed above lowest category below");
  }

  @Test
  void testFindClosestCategoryWithOne() {
    assertEquals(URBAN_NOT_STRICT,
        categories.findClosestCategory(NL, URBAN_ROAD, LIGHT_TRAFFIC, true, 30, null),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT,
        categories.findClosestCategory(NL, URBAN_ROAD, LIGHT_TRAFFIC, false, 30, null),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT,
        categories.findClosestCategory(NL, URBAN_ROAD, LIGHT_TRAFFIC, false, 90, null),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT,
        categories.findClosestCategory(NL, URBAN_ROAD, LIGHT_TRAFFIC, true, 120, null),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT,
        categories.findClosestCategory(NL, URBAN_ROAD, LIGHT_TRAFFIC, true, 135, null),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT,
        categories.findClosestCategory(NL, URBAN_ROAD, LIGHT_TRAFFIC, true, 160, null),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT,
        categories.findClosestCategory(NL, URBAN_ROAD, LIGHT_TRAFFIC, false, 160, null),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT,
        categories.findClosestCategory(NL, URBAN_ROAD, LIGHT_TRAFFIC, false, 160, null),
        ONE_CATEGORY_ALWAYS);
  }

  @Test
  void testStrictInOnlyNonStrict() {
    assertEquals(AB_100_NOT_STRICT, categories.findClosestCategory(NL, FREEWAY, AUTO_BUS, true, 90, null),
        "In only non strict return closest if given is strict");
  }

  @Test
  void testGetCategoryNonUrban() {
    assertEquals(NON_URBAN_ROAD_LT, categories.findClosestCategory(NL, NON_URBAN_ROAD, LIGHT_TRAFFIC, false, 0, null),
        "Non urban without maximum speed should be retrievable");
    assertEquals(NATIONAL_ROAD_LT_80, categories.findClosestCategory(NL, NATIONAL_ROAD, LIGHT_TRAFFIC, false, 20, null),
        "Non urban with maximum speed should be retrievable");
    assertEquals(NATIONAL_ROAD_LT_100, categories.findClosestCategory(NL, NATIONAL_ROAD, LIGHT_TRAFFIC, false, 120, null),
        "Non urban with maximum speed should be retrievable");
  }

  @ParameterizedTest(name = "{0}")
  @MethodSource("gradientCases")
  void testFindClosestCategoryWithGradient(final String name, final int speed, final double gradient, final RoadEmissionCategory expectedCategory) {
    assertEquals(expectedCategory,
        categories.findClosestCategory(NL, FREEWAY, HEAVY_FREIGHT, false, speed, gradient),
        name);
  }

  private static Stream<Arguments> gradientCases() {
    // Gradients should work same as speed, even for negatives
    // Negative gradients will (usually) have lower emission factors, and want to be conservative there.
    return Stream.of(
        Arguments.of("Category with negative gradient, lower speed", 50, -12.0, HF_80_GRAD_NEG_10),
        Arguments.of("Category with negative gradient exactly matched, lower speed", 50, -10.0, HF_80_GRAD_NEG_10),
        Arguments.of("Category with slightly negative gradient, lower speed", 50, -2.0, HF_80_GRAD_0),
        Arguments.of("Category without gradient, lower speed", 50, 0.0, HF_80_GRAD_0),
        Arguments.of("Category with slightly positive gradient, lower speed", 50, 2.0, HF_80_GRAD_10),
        Arguments.of("Category with positive gradient exactly matched, lower speed", 50, 10.0, HF_80_GRAD_10),
        Arguments.of("Category with positive gradient, lower speed", 50, 12.0, HF_80_GRAD_10),

        Arguments.of("Category with negative gradient, exact speed", 80, -12.0, HF_80_GRAD_NEG_10),
        Arguments.of("Category with negative gradient exactly matched, exact speed", 80, -10.0, HF_80_GRAD_NEG_10),
        Arguments.of("Category with slightly negative gradient, exact speed", 80, -2.0, HF_80_GRAD_0),
        Arguments.of("Category without gradient, exact speed", 80, 0.0, HF_80_GRAD_0),
        Arguments.of("Category with slightly positive gradient, exact speed", 80, 2.0, HF_80_GRAD_10),
        Arguments.of("Category with positive gradient exactly matched, exact speed", 80, 10.0, HF_80_GRAD_10),
        Arguments.of("Category with positive gradient, exact speed", 80, 12.0, HF_80_GRAD_10),

        Arguments.of("Category with negative gradient, speed between", 90, -12.0, HF_100_GRAD_NEG_10),
        Arguments.of("Category with negative gradient exactly matched, speed between", 90, -10.0, HF_100_GRAD_NEG_10),
        Arguments.of("Category with slightly negative gradient, speed between", 90, -2.0, HF_100_GRAD_0),
        Arguments.of("Category without gradient, speed between", 90, 0.0, HF_100_GRAD_0),
        Arguments.of("Category with slightly positive gradient, speed between", 90, 2.0, HF_100_GRAD_10),
        Arguments.of("Category with positive gradient exactly matched, speed between", 90, 10.0, HF_100_GRAD_10),
        Arguments.of("Category with positive gradient, speed between", 90, 12.0, HF_100_GRAD_10),

        Arguments.of("Category with negative gradient, higher speed", 120, -12.0, HF_100_GRAD_NEG_10),
        Arguments.of("Category with negative gradient exactly matched, higher speed", 120, -10.0, HF_100_GRAD_NEG_10),
        Arguments.of("Category with slightly negative gradient, higher speed", 120, -2.0, HF_100_GRAD_0),
        Arguments.of("Category without gradient, higher speed", 120, 0.0, HF_100_GRAD_0),
        Arguments.of("Category with slightly positive gradient, higher speed", 120, 2.0, HF_100_GRAD_10),
        Arguments.of("Category with positive gradient exactly matched, higher speed", 120, 10.0, HF_100_GRAD_10),
        Arguments.of("Category with positive gradient, higher speed", 120, 12.0, HF_100_GRAD_10));
  }

  private RoadEmissionCategories createData() {
    final RoadEmissionCategories categories = new RoadEmissionCategories();

    categories.add(URBAN_NOT_STRICT);
    categories.add(AB_80_NOT_STRICT);
    categories.add(AB_100_NOT_STRICT);
    categories.add(LT_50_NOT_STRICT);
    categories.add(LT_80_STRICT);
    categories.add(LT_80_NOT_STRICT);
    categories.add(LT_100_NOT_STRICT);
    categories.add(LT_120_NOT_STRICT);
    categories.add(LT_120_STRICT);
    categories.add(LT_130_NOT_STRICT);
    categories.add(NON_URBAN_ROAD_LT);
    categories.add(NATIONAL_ROAD_LT_80);
    categories.add(NATIONAL_ROAD_LT_100);
    categories.add(HF_80_GRAD_0);
    categories.add(HF_80_GRAD_10);
    categories.add(HF_80_GRAD_NEG_10);
    categories.add(HF_100_GRAD_0);
    categories.add(HF_100_GRAD_10);
    categories.add(HF_100_GRAD_NEG_10);
    return categories;
  }
}
