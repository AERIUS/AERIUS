/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link AnimalType}
 */
class AnimalTypeTest {

  @Test
  void testSafeValueOf() {
    assertEquals(AnimalType.COW, AnimalType.safeValueOf("COW"), "by name uppercase");
    assertEquals(AnimalType.COW, AnimalType.safeValueOf("cow"), "by name lowercase");
    assertEquals(AnimalType.GOAT, AnimalType.safeValueOf("C"), "by code");
    assertEquals(AnimalType.GOAT, AnimalType.safeValueOf("Capibara"), "by code, probably unintentional");
    assertEquals(AnimalType.OTHER, AnimalType.safeValueOf("Unknown"), "unknown");
    assertEquals(AnimalType.OTHER, AnimalType.safeValueOf(""), "empty string");
    assertEquals(AnimalType.OTHER, AnimalType.safeValueOf(null), "null");
  }

}
