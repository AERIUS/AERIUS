/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.exception;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

/**
 * Test for {@link AeriusExceptionReason} class.
 */
class AeriusExceptionReasonTest {

  /**
   * Test is a code in the enum is duplicated by accident. Meaning 2 or more enums contain the same number.
   */
  @Test
  void testDuplicateCodes() {
    for (final AeriusExceptionReason reason1: AeriusExceptionReason.values()) {
      for (final AeriusExceptionReason reason2: AeriusExceptionReason.values()) {
        if (reason1 != reason2) {
          assertNotEquals(reason1.getErrorCode(), reason2.getErrorCode(),
              "Found duplicate error code: " + reason1.getErrorCode() + " in " + reason2.name() + ", code already in " + reason1.name());
        }
      }
    }
  }
}
