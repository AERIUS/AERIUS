/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory.RoadEmissionCategoryKey;

/**
 * Test class for {@link RoadEmissionCategory}.
 */
class RoadEmissionCategoryTest {

  private static final String AUTO_BUS = "AUTO_BUS";
  private static final String NORMAL_FREIGHT = "NORMAL_FREIGHT";
  private static final String HEAVY_FREIGHT = "HEAVY_FREIGHT";

  private static final String FREEWAY = "FREEWAY";
  private static final String NON_URBAN_ROAD = "NON_URBAN_ROAD";
  private static final String URBAN_ROAD = "URBAN_ROAD";

  private static final String NL = "NL";
  private static final String UK = "UK";

  @Test
  void testEquals() {
    final RoadEmissionCategory rec = new RoadEmissionCategory();
    assertFalse(rec.equals(null), "Equals on null should be false");
    assertNotEquals(rec, new Object(), "Equals on other object should be false");
    assertEquals(rec, new RoadEmissionCategory(), "Equals on same object should be true");
  }

  @Test
  void testEqualsKey() {
    final RoadEmissionCategoryKey key = new RoadEmissionCategoryKey(NL, FREEWAY, AUTO_BUS);
    assertFalse(key.equals(null), "Equals on null should be false");
    assertTrue(key.equals(key), "Equals on itself should be true");
    assertNotEquals(key, new RoadEmissionCategoryKey(UK, URBAN_ROAD, HEAVY_FREIGHT),
        "EqualsType: false on dfferent area, road and vehicle type");
    assertNotEquals(key, new RoadEmissionCategoryKey(UK, FREEWAY, AUTO_BUS), "EqualsType: false on dfferent area");
    assertNotEquals(key, new RoadEmissionCategoryKey(NL, URBAN_ROAD, HEAVY_FREIGHT), "EqualsType: false on different road type");
    assertNotEquals(key, new RoadEmissionCategoryKey(NL, FREEWAY, HEAVY_FREIGHT), "EqualsType: false on different vehicle type");
    assertEquals(key, new RoadEmissionCategoryKey(NL, FREEWAY, AUTO_BUS), "EqualsType: true on same codes, but different instance");
  }

  @Test
  void testCompareToSame() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    assertEquals(0, base.compareTo(base), "Should compare to itself");
  }

  @Test
  void testCompareToDifferentRoadType() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    final RoadEmissionCategory test = new TestCategoryBuilder().roadType(NON_URBAN_ROAD).build();
    assertNotEquals(0, base.compareTo(test), "Should not be same on different road type");
  }

  @Test
  void testCompareToDifferentVehicleType() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    final RoadEmissionCategory test = new TestCategoryBuilder().vehicleType(AUTO_BUS).build();
    assertNotEquals(0, base.compareTo(test), 0, "Should not be same on different vehicle type");
  }

  @Test
  void testCompareToDifferentStrict() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    final RoadEmissionCategory test = new TestCategoryBuilder().strictEnforcement(false).build();
    assertTrue(base.compareTo(test) < 0, "Strict should be lower then non strict");
  }

  @Test
  void testCompareToStrictButLowerSpeed() {
    final RoadEmissionCategory base = new TestCategoryBuilder().strictEnforcement(false).build();
    final RoadEmissionCategory test = new TestCategoryBuilder().strictEnforcement(true).maximumSpeed(80).build();
    assertTrue(base.compareTo(test) > 0, "Strict should be lower then non strict");
  }

  @Test
  void testCompareToLowerSpeed() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    final RoadEmissionCategory test = new TestCategoryBuilder().maximumSpeed(80).build();
    assertTrue(base.compareTo(test) > 0, "Should return > 0 on lower speed");
  }

  @Test
  void testCompareToDifferentGradient() {
    final RoadEmissionCategory base = new TestCategoryBuilder().gradient(4).build();
    final RoadEmissionCategory test = new TestCategoryBuilder().gradient(2).build();
    assertTrue(base.compareTo(test) > 0, "Should return > 0 for lower gradient");
  }

  @Test
  void testCompareToDifferentAreaCode() {
    final RoadEmissionCategory base = new TestCategoryBuilder()
        .roadType(URBAN_ROAD)
        .build();
    final RoadEmissionCategory test = new TestCategoryBuilder()
        .roadType(URBAN_ROAD)
        .area(UK)
        .build();
    assertNotEquals(0, base.compareTo(test), "Should compare differently to non-standard (ordering doesn't really matter)");
  }

  @Test
  void testCompareToHigerSpeed() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    final RoadEmissionCategory test = new TestCategoryBuilder().maximumSpeed(120).build();
    assertTrue(base.compareTo(test) < 0, "Should return < 0 on higher speed");
  }

  private static class TestCategoryBuilder {
    String areaCode = NL;
    String roadType = FREEWAY;
    String vehicleType = NORMAL_FREIGHT;
    boolean strictEnforcement = true;
    int maximumSpeed = 100;
    int gradient = 0;

    TestCategoryBuilder area(final String areaCode) {
      this.areaCode = areaCode;
      return this;
    }

    TestCategoryBuilder roadType(final String roadType) {
      this.roadType = roadType;
      return this;
    }

    TestCategoryBuilder vehicleType(final String vehicleType) {
      this.vehicleType = vehicleType;
      return this;
    }

    TestCategoryBuilder strictEnforcement(final boolean strictEnforcement) {
      this.strictEnforcement = strictEnforcement;
      return this;
    }

    TestCategoryBuilder maximumSpeed(final int maximumSpeed) {
      this.maximumSpeed = maximumSpeed;
      return this;
    }

    TestCategoryBuilder gradient(final int gradient) {
      this.gradient = gradient;
      return this;
    }

    RoadEmissionCategory build() {
      final RoadEmissionCategoryKey key = new RoadEmissionCategoryKey(areaCode, roadType, vehicleType);
      return new RoadEmissionCategory(key, strictEnforcement, maximumSpeed, gradient);
    }

  }

}
