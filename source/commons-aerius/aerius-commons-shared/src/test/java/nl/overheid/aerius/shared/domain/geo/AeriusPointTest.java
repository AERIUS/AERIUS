/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;

/**
 * Test class for {@link AeriusPoint}.
 */
class AeriusPointTest {

  @Test
  void testEquals() {
    final AeriusPoint ap1 = new AeriusPoint(1, 601231, AeriusPointType.POINT, 400, 500);
    final AeriusPoint ap2 = new AeriusPoint(2, 70322, AeriusPointType.POINT, 400, 500);
    final AeriusPoint ap3 = new AeriusPoint(2, 70322, AeriusPointType.POINT, 500, 400);
    final AeriusPoint ap4 = new AeriusPoint(3, 902342, AeriusPointType.POINT, 500, 400);
    final AeriusPoint ap5 = new AeriusPoint(3, 902342, AeriusPointType.RECEPTOR, 500, 400);

    assertNotEquals(ap1, ap2, "id not same, x-y not");
    assertEquals(ap2, ap3, "id the same, x-y not");
    assertNotEquals(ap3, ap4, "id not same, x-y same");
    assertNotEquals(ap4, ap5, "id same, x-y same, type not");
  }

  @Test
  void testHashCode() {
    final AeriusPoint ap1 = new AeriusPoint(1, 601231, AeriusPointType.POINT, 400, 500);
    final AeriusPoint ap2 = new AeriusPoint(2, 70322, AeriusPointType.POINT, 400, 500);
    final AeriusPoint ap3 = new AeriusPoint(2, 70322, AeriusPointType.POINT, 500, 400);
    final AeriusPoint ap4 = new AeriusPoint(3, 80324, AeriusPointType.POINT, 500, 400);
    final AeriusPoint ap5 = new AeriusPoint(3, 80324, AeriusPointType.RECEPTOR, 500, 400);

    assertNotEquals(ap1.hashCode(), ap2.hashCode(), "id not same, x-y not");
    assertEquals(ap2.hashCode(), ap3.hashCode(), "id the same, x-y not");
    assertNotEquals(ap3.hashCode(), ap4.hashCode(), "id not same, x-y same");
    assertNotEquals(ap4.hashCode(), ap5.hashCode(), "id same, x-y same, type not");
  }
}
