/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;

class ScenarioResultTypeTest {

  @ParameterizedTest
  @MethodSource("resultTypeSource")
  void testResultTypesForSituationType(final SituationType situationType, final CalculationJobType jobType, final Theme theme,
      final boolean isUseInCombinationArchive, final List<ScenarioResultType> resultTypes) {
    final List<ScenarioResultType> types =
        ScenarioResultType.getResultTypesForSituationType(situationType, jobType, isUseInCombinationArchive);
    assertEquals(resultTypes, types, "Should match list with result types from test");
  }

  @ParameterizedTest
  @MethodSource("defaultResultTypes")
  void testDefaultResultType(final CalculationJobType jobType, final SituationType situationType, final ScenarioResultType resultType) {
    final ScenarioResultType defaultType = ScenarioResultType.getDefault(jobType, situationType);
    assertEquals(resultType, defaultType, "Default result type should match with arguments");
  }

  private static Stream<Arguments> resultTypeSource() {
    return Stream.of(
        arguments(SituationType.PROPOSED, CalculationJobType.SINGLE_SCENARIO, Theme.OWN2000, false, List.of(
            ScenarioResultType.SITUATION_RESULT)),
        arguments(SituationType.PROPOSED, CalculationJobType.PROCESS_CONTRIBUTION, Theme.OWN2000, false, List.of(
            ScenarioResultType.SITUATION_RESULT,
            ScenarioResultType.PROJECT_CALCULATION)),
        arguments(SituationType.REFERENCE, CalculationJobType.DEPOSITION_SUM, Theme.OWN2000, false, List.of(
            ScenarioResultType.DEPOSITION_SUM, ScenarioResultType.SITUATION_RESULT)),
        arguments(SituationType.TEMPORARY, CalculationJobType.MAX_TEMPORARY_EFFECT, Theme.OWN2000, false, List.of(
            ScenarioResultType.SITUATION_RESULT,
            ScenarioResultType.MAX_TEMPORARY_EFFECT)),
        arguments(SituationType.PROPOSED, CalculationJobType.PROCESS_CONTRIBUTION, Theme.NCA, false, List.of(
            ScenarioResultType.SITUATION_RESULT,
            ScenarioResultType.PROJECT_CALCULATION)),
        arguments(SituationType.PROPOSED, CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION, Theme.NCA, false, List.of(
            ScenarioResultType.SITUATION_RESULT,
            ScenarioResultType.PROJECT_CALCULATION,
            ScenarioResultType.IN_COMBINATION)),
        arguments(SituationType.PROPOSED, CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION, Theme.NCA, true, List.of(
            ScenarioResultType.SITUATION_RESULT,
            ScenarioResultType.PROJECT_CALCULATION,
            ScenarioResultType.IN_COMBINATION,
            ScenarioResultType.ARCHIVE_CONTRIBUTION)));
  }

  private static Stream<Arguments> defaultResultTypes() {
    return Stream.of(
        arguments(CalculationJobType.SINGLE_SCENARIO, SituationType.PROPOSED, ScenarioResultType.SITUATION_RESULT),
        arguments(CalculationJobType.SINGLE_SCENARIO, SituationType.REFERENCE, ScenarioResultType.SITUATION_RESULT),
        arguments(CalculationJobType.SINGLE_SCENARIO, SituationType.OFF_SITE_REDUCTION, ScenarioResultType.SITUATION_RESULT),
        arguments(CalculationJobType.PROCESS_CONTRIBUTION, SituationType.PROPOSED, ScenarioResultType.PROJECT_CALCULATION),
        arguments(CalculationJobType.PROCESS_CONTRIBUTION, SituationType.REFERENCE, ScenarioResultType.SITUATION_RESULT),
        arguments(CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION, SituationType.PROPOSED, ScenarioResultType.IN_COMBINATION),
        arguments(CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION, SituationType.REFERENCE, ScenarioResultType.SITUATION_RESULT),
        arguments(CalculationJobType.MAX_TEMPORARY_EFFECT, SituationType.TEMPORARY, ScenarioResultType.MAX_TEMPORARY_EFFECT),
        arguments(CalculationJobType.MAX_TEMPORARY_EFFECT, SituationType.REFERENCE, ScenarioResultType.SITUATION_RESULT),
        arguments(CalculationJobType.DEPOSITION_SUM, SituationType.PROPOSED, ScenarioResultType.SITUATION_RESULT),
        arguments(CalculationJobType.DEPOSITION_SUM, SituationType.REFERENCE, ScenarioResultType.DEPOSITION_SUM));
  }

}
