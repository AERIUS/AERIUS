/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.context;

import java.io.Serializable;

/**
 * Class denoting the deposition display type and a relevant conversion factor.
 */
public class EmissionResultValueDisplaySettings implements Serializable {
  private static final long serialVersionUID = 512644462620810857L;

  public enum DepositionValueDisplayType implements Serializable {
    MOLAR_UNITS,

    KILOGRAM_UNITS;
  }

  private DepositionValueDisplayType displayType;
  private double conversionFactor;
  private int depositionValueRoundingLength;
  private int depositionValuePreciseRoundingLength;
  private int depositionValueSumRoundingLength;

  /**
   * GWT Constructor.
   */
  public EmissionResultValueDisplaySettings() {}

  public EmissionResultValueDisplaySettings(final DepositionValueDisplayType displayType, final double conversionFactor,
      final int depositionValueRoundingLength, final int depositionValuePreciseRoundingLength, int depositionValueSumRoundingLength) {
    this.displayType = displayType;
    this.conversionFactor = conversionFactor;
    this.depositionValueRoundingLength = depositionValueRoundingLength;
    this.depositionValuePreciseRoundingLength = depositionValuePreciseRoundingLength;
    this.depositionValueSumRoundingLength = depositionValueSumRoundingLength;
  }

  public DepositionValueDisplayType getDisplayType() {
    return displayType;
  }

  public double getConversionFactor() {
    return conversionFactor;
  }

  public void setDisplayType(final DepositionValueDisplayType displayType) {
    this.displayType = displayType;
  }

  public void setConversionFactor(final double conversionFactor) {
    this.conversionFactor = conversionFactor;
  }

  public int getDepositionValueRoundingLength() {
    return depositionValueRoundingLength;
  }

  public void setDepositionValueRoundingLength(final int depositionValueRoundingLength) {
    this.depositionValueRoundingLength = depositionValueRoundingLength;
  }

  public int getDepositionValuePreciseRoundingLength() {
    return depositionValuePreciseRoundingLength;
  }

  public void setDepositionValuePreciseRoundingLength(final int depositionValuePreciseRoundingLength) {
    this.depositionValuePreciseRoundingLength = depositionValuePreciseRoundingLength;
  }

  public int getDepositionValueSumRoundingLength() {
    return depositionValueSumRoundingLength;
  }

  public void setDepositionValueSumRoundingLength(int depositionValueSumRoundingLength) {
    this.depositionValueSumRoundingLength = depositionValueSumRoundingLength;
  }
}
