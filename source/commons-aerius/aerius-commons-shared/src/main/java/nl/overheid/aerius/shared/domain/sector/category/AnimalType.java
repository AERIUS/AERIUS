/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

/**
 * Enum for the possible RAV animal types.
 */
public enum AnimalType {

  // @formatter:off
  COW('A'),
  SHEEP('B'),
  GOAT('C'),
  PIG('D'),
  CHICKEN('E'),
  TURKEY('F'),
  HORSE('K'),
  DUCK('G'),
  MINK('H'),
  RABBIT('I'),
  GUINEA_FOWL('J'),
  OSTRICH('L'),
  OTHER(' ');
  // @formatter:on

  private final char ravAnimalCode;

  AnimalType(final char ravAnimalCode) {
    this.ravAnimalCode = ravAnimalCode;
  }

  @Deprecated
  public String getAnimalCode() {
    return Character.toString(ravAnimalCode);
  }

  /**
   * @param categoryCode
   * @return
   */
  @Deprecated
  public static AnimalType getByCode(final String categoryCode) {
    AnimalType returnType = AnimalType.OTHER;
    if (categoryCode != null && !categoryCode.isEmpty()) {
      for (final AnimalType animalType : values()) {
        if (animalType.ravAnimalCode == categoryCode.charAt(0)) {
          returnType = animalType;
        }
      }
    }
    return returnType;
  }

  /**
   * Determine animal type for a code.
   * @param value The value to convert to this enum
   * @return The value that was considered appropriate.
   */
  public static AnimalType safeValueOf(final String value) {
    AnimalType returnType = null;
    // First check by full name (case where code is based on enum values)
    for (final AnimalType animalType : values()) {
      if (animalType.name().equalsIgnoreCase(value)) {
        returnType = animalType;
        break;
      }
    }
    // As fallback check by old code (has some overlap with names, hence this approach)
    if (returnType == null) {
      returnType = getByCode(value);
    }
    return returnType;
  }
}
