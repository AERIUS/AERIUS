/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Util class to generate ids for items in a list in consecutive order.
 *
 * This class does not contain the list itself, rather it contains the logic
 * for determining the next id. This only works if the list is consecutive in
 * the sense that the id of each object is in order of the list. This util class
 * then makes sure the list remains consecutive. When an object is added it is
 * added in ascending order at the first position an id is missing from the list.
 *
 * <p>Given a list in consecutive order of ids, a (subsection of a) list is <i>complete</i>
 * if and only if every element has a id corresponding to the index of that element
 * in the list. Whether a (subsection of a) list is complete that quickly be checked
 * by checking whether the last element of (that subsection of) the list has the
 * correct id. Again, this only works if the list is in consecutive order.
 *
 * <p>For example if the list contains 3 objects, where the id's of these objects
 * would be 1, 2, 4. In this case the added object will get id 3 and be placed
 * in between objects with id 2 and 4.
 * <p>If an object is removed the spot remains empty.
 * <p>The implementation uses a binary search algorithm to find missing id's in
 * Log(n) time.
 *
 */
public class ConsecutiveIdUtil {

  private final Function<Integer, String> indexToIdFunction;
  private final Function<Integer, String> idLookupFunction;
  private final Supplier<Integer> currentListSizeFunction;

  /**
   * Create a new ConsecutiveIdUtil.
   * @param indexToIdFunction Specifies how to convert an index in a list to String ID.
   *                          This can be as simple as `String.valueOf()` to use 0-starting
   *                          index as ID, or `(i) -> "ES." + (i + 1)` to use prefixed
   *                          String id starting at 1.
   * @param idLookupFunction  This function should return the current ID of the element at
   *                          the requested index, for example `(i) -> testList.get(i).getId()`
   * @param currentListSizeFunction This function should return the current number of elements
   *                                in the list, for example `testList::size`
   */
  public ConsecutiveIdUtil(
      final Function<Integer, String> indexToIdFunction,
      final Function<Integer, String> idLookupFunction,
      final Supplier<Integer> currentListSizeFunction) {
    this.indexToIdFunction = indexToIdFunction;
    this.idLookupFunction = idLookupFunction;
    this.currentListSizeFunction = currentListSizeFunction;
  }

  /**
   * Identifies the next ID + index it should be inserted at
   * @return The IndexIdPair containing the ID and index.
   */
  public IndexIdPair generate() {
    final int currentListSize = currentListSizeFunction.get();

    // If the list is empty, start at index = 0
    if (currentListSize == 0) {
      return new IndexIdPair(0, indexToIdFunction.apply(0));
    }

    if (isSublistComplete(currentListSize - 1)) {
      // The list is complete, add element to the end
      return new IndexIdPair(currentListSize, indexToIdFunction.apply(currentListSize));
    } else {
      // The list is incomplete, there are gaps. Find the first gap
      return findFirstGap(-1, currentListSize - 1);
    }
  }

  /**
   * Finds the first gap in a sublist. Works recursively.
   * @param fromIndex Starting index of the sublist. Invariant: the list must be complete until
   *                  this index.
   * @param toIndex   Ending index of the sublist. Invariant: the list must be incomplete until
   *                  this index.
   * @return The first gap in the sublist
   */
  private IndexIdPair findFirstGap(final int fromIndex, final int toIndex) {
    if (toIndex - fromIndex == 1) {
      // Complete index until fromIndex, incomplete from toIndex (invariant)
      // The gap must be at index toIndex.
      return new IndexIdPair(toIndex, indexToIdFunction.apply(toIndex));
    }

    final int midpoint = (toIndex - fromIndex) / 2 + fromIndex;

    if (isSublistComplete(midpoint)) {
      // The sublist is complete until midpoint, there must be a gap in the second half
      return findFirstGap(midpoint, toIndex);
    } else {
      // The sublist is incomplete until midpoint, there must be a gap in the first half
      return findFirstGap(fromIndex, midpoint);
    }
  }

  /**
   * A (sub)list is complete if every element has the id associated with the index
   * of that element. Given that the list must be in consecutive order this can
   * be checked by checking if the last element has the correct id.
   * @param toIndex Index of the last element of the sublist
   * @return Whether the sublist is complete
   */
  private boolean isSublistComplete(final int toIndex) {
    final String expectedId = indexToIdFunction.apply(toIndex);
    final String encounteredId = idLookupFunction.apply(toIndex);

    return expectedId.equals(encounteredId);
  }

  /**
   * Describes the result of a generate action on the [ConsecutiveIdUtil].
   */
  public static class IndexIdPair {

    private final int index;
    private final String id;

    public IndexIdPair(final int index, final String id) {
      this.index = index;
      this.id = id;
    }

    public int getIndex() {
      return index;
    }

    public String getId() {
      return id;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      IndexIdPair that = (IndexIdPair) o;
      return index == that.index && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
      return Objects.hash(index, id);
    }
  }

}
