/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Category for a cold start source.
 */
public class ColdStartSourceCategory extends AbstractEmissionCategory {

  private static final long serialVersionUID = 1L;

  /**
   * Emission factor per cold start.
   */
  private @JsProperty Map<EmissionValueKey, Double> emissionFactors = new HashMap<>();

  public double getEmissionFactor(final Substance substance, final int year) {
    return emissionFactors.getOrDefault(new EmissionValueKey(year, substance), 0.0).doubleValue();
  }

  public void setEmissionFactor(final EmissionValueKey evk, final double emissionFactor) {
    emissionFactors.put(evk, emissionFactor);
  }
}
