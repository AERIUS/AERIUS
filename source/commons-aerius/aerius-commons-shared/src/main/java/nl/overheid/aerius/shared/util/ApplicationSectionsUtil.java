/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;

/**
 * Util class to help with centralizing which sections of the application are used/expected,
 * depending on certain aspects of the current product or a calculation type.
 */
public final class ApplicationSectionsUtil {

  private ApplicationSectionsUtil() {
    // private constructor for util class
  }

  public static boolean isCustomPointsUsed(final CalculationJobType calculationJobType) {
    return calculationJobType != CalculationJobType.DEPOSITION_SUM;
  }

  public static boolean isCustomPointsUsed(final ProductProfile productProfile) {
    return productProfile != ProductProfile.LBV_POLICY;
  }

}
