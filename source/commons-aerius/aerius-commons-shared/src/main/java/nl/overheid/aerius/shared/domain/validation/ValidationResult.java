/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.validation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.exception.AeriusException;

public class ValidationResult implements Serializable {

  private static final long serialVersionUID = 1L;

  private List<AeriusException> errors = new ArrayList<AeriusException>();
  private List<AeriusException> warnings = new ArrayList<AeriusException>();

  public void add(final ValidationResult vr) {
    errors.addAll(vr.getErrors());
    warnings.addAll(vr.getWarnings());
  }

  public void addError(final AeriusException error) {
    errors.add(error);
  }

  public void addWarning(final AeriusException warning) {
    warnings.add(warning);
  }

  public List<AeriusException> getErrors() {
    return errors;
  }

  public List<AeriusException> getWarnings() {
    return warnings;
  }

}
