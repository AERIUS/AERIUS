/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

/**
 * Subsets of hexagons filtering on overlapping hexagons.
 * Used for displaying edge effects in the UI
 */
public enum OverlappingHexagonType {

  /**
   * All hexagons with results (i.e. no filtering).
   * Default option
   */
  ALL_HEXAGONS,

  /**
   * Overlapping hexagons (hexagons with results from all sources/situations only)
   * Only uses hexagons without any edge effects
   */
  OVERLAPPING_HEXAGONS_ONLY,

  /**
   * Non-overlapping hexagons (hexagons which not all source/situation contribute to).
   * Only uses hexagons with edge effects.
   */
  NON_OVERLAPPING_HEXAGONS_ONLY,

}
