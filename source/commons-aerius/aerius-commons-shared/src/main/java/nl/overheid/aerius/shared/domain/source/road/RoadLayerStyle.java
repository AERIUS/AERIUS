/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source.road;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum RoadLayerStyle {

  ROAD_TYPE(false, null),
  MAX_SPEED(false, null),
  STAGNATION(true, null),
  TRAFFIC_VOLUME(true, null),
  ROAD_BARRIER(false, null),
  ROAD_BARRIER_TYPE(false, ROAD_BARRIER),
  ROAD_BARRIER_HEIGHT(false, ROAD_BARRIER),
  ROAD_BARRIER_POROSITY(false, ROAD_BARRIER),
  ELEVATION_TYPE(false, null),
  ELEVATION_HEIGHT(false, null),
  ROAD_EMISSION(false, null),
  TUNNEL_FACTOR(false, null),
  ;

  /**
   * Whether the styling depends on the type of vehicle (light_traffic, ...)
   */
  private final boolean dependsVehicleType;

  private final RoadLayerStyle parentStyle;

  RoadLayerStyle(final boolean dependsVehicleType, final RoadLayerStyle parentStyle) {
    this.dependsVehicleType = dependsVehicleType;
    this.parentStyle = parentStyle;
  }

  public boolean dependsOnVehicleType() {
    return dependsVehicleType;
  }

  public RoadLayerStyle getParentStyle() {
    return parentStyle;
  }

  public List<RoadLayerStyle> getSubStyles() {
    return Arrays.stream(RoadLayerStyle.values()).filter(rls -> rls.parentStyle == this).collect(Collectors.toList());
  }

  public boolean hasSubStyles() {
    return !getSubStyles().isEmpty();
  }

  public boolean isSubStyle() {
    return getParentStyle() != null;
  }
}
