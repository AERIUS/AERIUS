/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Data object for a farm animal housing (system) including its emission factors.<br>
 * <br>
 * A animal housing type has an id, code, name, description, animal category .<br>
 * <br>
 * There are additional housing systems that can be "stacked on" this housing type.
 * These generally decrease the emissions of the type.
 */
public class FarmAnimalHousingCategory extends AbstractEmissionCategory {

  private static final long serialVersionUID = 1L;

  /**
   * Indication of the unit of the emission factors.
   * This also dictates which properties are expected on a source, like number of animals and/or number of days.
   */
  private FarmEmissionFactorType farmEmissionFactorType;

  /**
   * Emission factor per animal, exact unit depends on farmEmissionFactorType.
   */
  private @JsProperty Map<Substance, Double> emissionFactors = new HashMap<>();

  /**
   * Animal category code for this animal housing
   */
  private String animalCategoryCode;

  /**
   * Default public constructor.
   */
  public FarmAnimalHousingCategory() {
    // No-op.
  }

  public FarmAnimalHousingCategory(final int id, final String code, final String name, final String description,
      final String animalCategoryCode) {
    super(id, code, name, description);
    this.animalCategoryCode = animalCategoryCode;
  }

  public FarmEmissionFactorType getFarmEmissionFactorType() {
    return farmEmissionFactorType;
  }

  public void setFarmEmissionFactorType(final FarmEmissionFactorType farmEmissionFactorType) {
    this.farmEmissionFactorType = farmEmissionFactorType;
  }

  public Map<Substance, Double> getEmissionFactors() {
    return emissionFactors;
  }

  public String getAnimalCategoryCode() {
    return animalCategoryCode;
  }

  public void setAnimalCategoryCode(final String animalCategoryCode) {
    this.animalCategoryCode = animalCategoryCode;
  }

  public void addEmissionFactor(final Substance substance, final Double emissionFactor) {
    this.emissionFactors.put(substance, emissionFactor);
  }

  @Override
  public String toString() {
    return super.toString()
        + ", emissionFactors=" + emissionFactors
        + ", farmEmissionFactorType=" + farmEmissionFactorType
        + ", animalCategory=" + animalCategoryCode
        + "]";
  }
}
