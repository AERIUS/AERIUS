/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.characteristics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;

/**
 * Class to handle everything related to time-varying profiles (designed for NCA/ADMS context).
 *
 * Should keep a list of standard profiles, as well as ways to determine defaults as needed.
 */
public class TimeVaryingProfiles implements Serializable {
  public static final String CONTINUOUS = "CONTINUOUS";

  private static final long serialVersionUID = 2L;

  // Map<CustomTimeVaryingProfileType, List standard profiles>
  private @JsProperty Map<CustomTimeVaryingProfileType, List<StandardTimeVaryingProfile>> standardTimeVaryingProfiles = new HashMap<>();
  // Map<CustomTimeVaryingProfileType, Map<sector id, profile code>>
  private @JsProperty Map<CustomTimeVaryingProfileType, Map<Integer, String>> sectorDefaultStandardProfileCodes = new HashMap<>();
  // Map<CustomTimeVaryingProfileType, Map<sector id, Map<year, profile code>>>
  private @JsProperty Map<CustomTimeVaryingProfileType, Map<Integer, Map<Integer, String>>> sectorYearDefaultStandardProfileCodes = new HashMap<>();

  /**
   * @param type time-varying profile type
   * @param sector The sector ID to retrieve the default profile for
   * @param year The year to retrieve the default profile for.
   * @return The profile to use as default (or null if not available, in which case a continuous profile can be assumed).
   */
  public StandardTimeVaryingProfile defaultProfileForSectorAndYear(final CustomTimeVaryingProfileType type, final int sector, final int year) {
    // First try by sector + year, if not available, try by sector
    final String code = sectorYearDefaultStandardProfileCodes.getOrDefault(type, new HashMap<>())
        .getOrDefault(sector, new HashMap<>())
        .getOrDefault(year, sectorDefaultStandardProfileCodes.getOrDefault(type, new HashMap<>()).getOrDefault(sector, null));
    return code == null ? null : AbstractCategory.determineByCode(standardTimeVaryingProfiles.get(type), code);
  }

  /**
   * @param code The standard time-varying profile code to determine the StandardTimeVaryingProfile object for.
   * @return The right StandardTimeVaryingProfile object, or null if no match could be made based on the code.
   */
  public StandardTimeVaryingProfile determineStandardTimeVaryingProfile(final CustomTimeVaryingProfileType type, final String code) {
    return AbstractCategory.determineByCode(standardTimeVaryingProfiles.get(type), code);
  }

  public List<StandardTimeVaryingProfile> getStandardTimeVaryingProfiles(final CustomTimeVaryingProfileType type) {
    return standardTimeVaryingProfiles.getOrDefault(type, new ArrayList<>());
  }

  public void setStandardTimeVaryingProfiles(final CustomTimeVaryingProfileType type, final List<StandardTimeVaryingProfile> profiles) {
    this.standardTimeVaryingProfiles.put(type, profiles);
  }

  public void setSectorDefaultStandardProfileCodes(final CustomTimeVaryingProfileType type, final Map<Integer, String> profiles) {
    this.sectorDefaultStandardProfileCodes.put(type, profiles);
  }

  public void setSectorYearDefaultStandardProfileCodes(final CustomTimeVaryingProfileType type, final Map<Integer, Map<Integer, String>> profiles) {
    this.sectorYearDefaultStandardProfileCodes.put(type, profiles);
  }

}
