/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;

/**
 * {@link OPSReceptor} with additional information related to sub receptor data.
 */
public class OPSSubReceptor extends OPSReceptor implements IsSubPoint {

  private static final long serialVersionUID = 3L;

  private AeriusPointType parentPointType;
  private int level;
  // geometry is only used in the chunker and doesn't need to be serialized, therefore made transient.
  private transient Polygon geometry;

  public OPSSubReceptor(final AeriusPoint point, final AeriusPointType pointType, final int level) {
    super(point, pointType);
    this.level = level;
  }

  @Override
  public AeriusPointType getParentPointType() {
    return parentPointType;
  }

  @Override
  public void setParent(final AeriusPoint parentPoint) {
    setParentId(parentPoint.getId());
    this.parentPointType = parentPoint.getPointType();
  }

  @Override
  public int getLevel() {
    return level;
  }

  public void setLevel(final int level) {
    this.level = level;
  }

  @Override
  public Polygon getArea() {
    return geometry;
  }

  @Override
  public void setArea(final Polygon geometry) {
    this.geometry = geometry;
  }

}

