/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import java.io.Serializable;

/**
 * Interface to identify a chunk that is sent to a model worker.
 * Some calculations require to be calculated by either different workers, or the same worker but different year.
 * These results need to be combined to a single results for each point.
 * The {@link EngineDataKey} is used to identify the different results.
 * A calculation is typical split up in chunks.
 * A chunk contains the points to be calculated in that chunk.
 * If multiple {@link EngineDataKey} are used there should be chunks with the same set of points for each {@link EngineDataKey}.
 */
public interface EngineDataKey extends Serializable {
  /**
   * @return engine data key identifier
   */
  String key();
}
