/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * On-Road Mobile Source Category class. For each mobile source per substance the
 * emission factors, that is factor per km per vehicle, is available in the
 * object.
 */
public class OnRoadMobileSourceCategory extends AbstractEmissionCategory {

  private static final long serialVersionUID = 4L;

  /**
   * Emission factors per substance. Where emission factor is factor per km per vehicle.
   */
  private @JsProperty Map<OnRoadEmissionFactorKey, Double> emissionFactors = new HashMap<>();

  public double getEmissionFactor(final Substance substance, final String roadType, final int year) {
    return emissionFactors.containsKey(new OnRoadEmissionFactorKey(substance, roadType, year))
        ? emissionFactors.get(new OnRoadEmissionFactorKey(substance, roadType, year))
        : 0;
  }

  public ArrayList<Substance> getKeys() {
    final HashSet<Substance> keys = new HashSet<>();
    for (final OnRoadEmissionFactorKey key : emissionFactors.keySet()) {
      keys.add(key.substance);
    }
    return new ArrayList<>(keys);
  }

  public void setEmissionFactor(final Substance substance, final String roadType, final int year, final double emissionFactor) {
    emissionFactors.put(new OnRoadEmissionFactorKey(substance, roadType, year), Double.valueOf(emissionFactor));
  }

  @Override
  public String toString() {
    return super.toString() + ", emissionFactors=" + emissionFactors;
  }

  static class OnRoadEmissionFactorKey implements Serializable {

    private static final long serialVersionUID = 4L;

    private Substance substance;
    private String roadType;
    private int year;

    @SuppressWarnings("unused")
    private OnRoadEmissionFactorKey() {
      //Needed for GWT.
    }

    public OnRoadEmissionFactorKey(final Substance substance, final String roadType, final int year) {
      this.substance = substance;
      this.roadType = roadType;
      this.year = year;
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj != null && this.getClass() == obj.getClass()) {
        final OnRoadEmissionFactorKey other = (OnRoadEmissionFactorKey) obj;
        return ((roadType == null && other.roadType == null) || (roadType != null && roadType.equals(other.roadType)))
            && substance == ((OnRoadEmissionFactorKey) obj).substance
            && year == ((OnRoadEmissionFactorKey) obj).year;
      }
      return false;
    }

    @Override
    public int hashCode() {
      return (31 * (17 + roadType.hashCode())) + substance.hashCode() + year;
    }
  }
}
