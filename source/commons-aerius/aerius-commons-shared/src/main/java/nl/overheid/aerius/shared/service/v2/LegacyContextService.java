/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service.v2;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service with all methods to communicate contextual data with the server from
 * the client. This is both global context data which is static during a user
 * session as well as user specific context data.
 */
@RemoteServiceRelativePath(ServiceURLConstants.CONTEXT_SERVICE_PATH)
public interface LegacyContextService extends RemoteService {
  /**
   * Returns the Context object for the given product profile.
   *
   * @param productProfile product profile to get the context for
   * @return Application context data
   * @throws AeriusException exception if anything fails on the server.
   */
  ApplicationConfiguration getContext(ProductProfile productProfile) throws AeriusException;

  /**
   * Called when the user closes the browser window.
   * @param lastCalculationKey optional key of last calculation, only relevant for calculator
   */
  void closeSession(String lastCalculationKey);
}
