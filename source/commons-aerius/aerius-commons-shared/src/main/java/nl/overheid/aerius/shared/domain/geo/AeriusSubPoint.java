/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;

/**
 *
 */
public class AeriusSubPoint extends AeriusPoint implements IsSubPoint {

  private static final long serialVersionUID = 1L;

  private int level;

  /**
   * Construct an actual subpoint.
   * This subpoint will get the same id and level as the supplied sub point.
   * @param feature The feature to obtain geometry information from.
   * @param subPoint The sub point to copy information from.
   */
  public AeriusSubPoint(final CalculationPointFeature feature, final SubPoint subPoint) {
    super(subPoint.getSubPointId(), subPoint.getReceptorId(), AeriusPointType.SUB_RECEPTOR, feature.getGeometry().getX(),
        feature.getGeometry().getY());
    this.level = subPoint.getLevel();
  }

  /**
   * Construct a subpoint representing the receptor point.
   * This subpoint will have sub point ID 0 and level 0.
   * @param feature The feature to obtain geometry information from.
   * @param receptorPoint The receptor point.
   */
  public AeriusSubPoint(final CalculationPointFeature feature, final ReceptorPoint receptorPoint) {
    super(0, receptorPoint.getReceptorId(), AeriusPointType.SUB_RECEPTOR, feature.getGeometry().getX(),
        feature.getGeometry().getY());
    this.level = 0;
  }

  /**
   * Construct a subpoint representing the receptor point.
   * This subpoint will have sub point ID 0 and level 0.
   * @param feature The feature to obtain geometry information from.
   * @param receptorPoint The receptor point.
   */
  public AeriusSubPoint(final AeriusPoint receptorPoint, final int receptorId) {
    super(0, receptorId, AeriusPointType.SUB_RECEPTOR, receptorPoint.getX(), receptorPoint.getY());
    this.level = 0;
  }

  @Override
  public AeriusPointType getParentPointType() {
    // NO-OP for this specific type
    return null;
  }

  @Override
  public void setParent(final AeriusPoint parentPoint) {
    // NO-OP
  }

  @Override
  public int getLevel() {
    return level;
  }

  @Override
  public Polygon getArea() {
    // NO-OP for this specific type
    return null;
  }

  @Override
  public void setArea(final Polygon geometry) {
    // NO-OP
  }

}
