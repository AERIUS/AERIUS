/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ResultStatisticType {

  /**
   * The summed cartographic surface calculated in m^2.
   */
  SUM_CARTOGRAPHIC_SURFACE("sumCartographicSurface"),
  /**
   * The summed cartographic surface with an increase in deposition calculated in m^2.
   */
  SUM_CARTOGRAPHIC_SURFACE_INCREASE("sumCartographicSurfaceIncrease"),
  /**
   * The summed cartographic surface with a decrease in deposition calculated in m^2.
   */
  SUM_CARTOGRAPHIC_SURFACE_DECREASE("sumCartographicSurfaceDecrease"),
  /**
   * The number of receptors.
   */
  COUNT_RECEPTORS("countReceptors"),
  /**
   * The number of receptors with an increase in deposition.
   */
  COUNT_RECEPTORS_INCREASE("countReceptorsIncrease"),
  /**
   * The number of receptors with a decrease in deposition.
   */
  COUNT_RECEPTORS_DECREASE("countReceptorsDecrease"),
  /**
   * The number of calculation points.
   */
  COUNT_CALCULATION_POINTS("countCalculationPoints"),
  /**
   * The number of calculation points with an increase in deposition.
   */
  COUNT_CALCULATION_POINTS_INCREASE("countCalculationPointsIncrease"),
  /**
   * The number of calculation points with a decrease in deposition.
   */
  COUNT_CALCULATION_POINTS_DECREASE("countCalculationPointsDecrease"),
  /**
   * The maximum background value of receptors within the calculation. In case of deposition, this is in mol/ha/y.
   */
  MAX_BACKGROUND("maxBackground"),
  /**
   * The contribution of the calculation. In case of deposition, this is in mol/ha/y.
   */
  MAX_CONTRIBUTION("maxContribution"),
  /**
   * The total result of the contribution of the calculation and the background value. In case of deposition, this is in mol/ha/y.
   */
  MAX_TOTAL("maxTotal"),
  /**
   * The largest increase in deposition for the project calculation
   */
  MAX_INCREASE("maxIncrease"),
  /**
   * The largest decrease in deposition for the project calculation
   */
  MAX_DECREASE("maxDecrease"),
  /**
   * Highest temporary increase of deposition of the temporary situations.
   */
  MAX_TEMP_INCREASE("maxTempIncrease"),
  /**
   * Specify if demand fits
   */
  DEVELOPMENT_SPACE_DEMAND_CHECK("developmentSpaceDemandCheck"),

  /**
   * Highest percentage of the critical levels, this is in %.
   */
  MAX_PERCENTAGE_CRITICAL_LEVEL("maxPercentageCriticalLevel"),

  /**
   * Highest total (calculation and background value) percentage of the critical levels, this is in %.
   */
  MAX_TOTAL_PERCENTAGE_CRITICAL_LEVEL("maxTotalPercentageCriticalLevel"),

  /**
   * A threshold value as defined by a policy
   */
  PROCUREMENT_POLICY_THRESHOLD_VALUE("procurementPolicyThresholdValue"),

  /**
   * Percentage result of the threshold value as defined by a policy
   */
  PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE("procurementPolicyThresholdPercentage"),

  /**
   * Statistic type for the marker to indicate a passing policy threshold
   */
  PROCUREMENT_POLICY_THRESHOLD_PASS("procurementPolicyThresholdPass"),

  /**
   * Statistic type for the marker to indicate a failing to pass policy threshold
   */
  PROCUREMENT_POLICY_THRESHOLD_FAIL("procurementPolicyThresholdFail"),

  /**
   * Sum of the deposition or concentration
   */
  SUM_CONTRIBUTION("sumContribution");

  private final String jsonKey;

  private ResultStatisticType(final String jsonKey) {
    this.jsonKey = jsonKey;
  }

  @JsonValue
  public String getJsonKey() {
    return jsonKey;
  }

  /**
   * Returns the ResultStatisticType associated by the jsonKey
   */
  public static ResultStatisticType getValueByJsonKey(final String jsonKey) {
    for (final ResultStatisticType value : values()) {
      if (value.getJsonKey().equals(jsonKey)) {
        return value;
      }
    }
    return null;
  }

}
