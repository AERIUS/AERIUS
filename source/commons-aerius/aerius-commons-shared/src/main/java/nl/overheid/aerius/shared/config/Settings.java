/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.shared.config;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;

/**
 * Data object to store generic configuration settings.
 */
public class Settings implements Serializable {

  private static final long serialVersionUID = 1L;
  // Generic settings
  private @JsProperty Map<Enum<?>, Object> settings = new HashMap<>();

  public boolean hasSetting(final Enum<?> key) {
    return settings.containsKey(key) && settings.get(key) != null;
  }

  public <T extends Object> T getSetting(final Enum<?> key) {
    return getSetting(key, false);
  }

  @SuppressWarnings("unchecked")
  public <T extends Object> T getSetting(final Enum<?> key, final boolean soft) {
    checkMap(settings, key, "setting", soft);

    return (T) settings.get(key);
  }

  public void setSetting(final Enum<?> key, final Object obj) {
    settings.put(key, obj);
  }

  private <K extends Object, V extends Object> void checkMap(final Map<K, V> map, final K key, final String description, final boolean soft) {
    if (!map.containsKey(key) && !soft) {
      throw new IllegalStateException("Required " + description + " '" + key + "' not present.");
    }
  }
}
