/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;

/**
 * CalculationMarkers are related to {@link ResultStatisticType}, but the same ResultStatisticType can refer to a different marker type depending on
 * the {@link ScenarioResultType}.
 *
 * This class decribes the available calculation markers and their relation with ResultStatisticType
 */
public enum CalculationMarker {
  /**
   * Purple marker
   */
  M1_HIGHEST_TOTAL(4),

  /**
   * Pink marker
   */
  M2A_HIGHEST_CONTRIBUTION(0),

  /**
   * Pink marker (+)
   */
  M2B_HIGHEST_INCREASE(1),

  /**
   * Pink marker (-)
   */
  M2C_HIGHEST_DECREASE(2),

  /**
   * Blue marker
   */
  M4_MAX_CUMULATION(5),

  /**
   * Orange marker
   */
  M5_MAX_TOTAL_WITH_EFFECT(3),

  /**
   * Passes procurement policy threshold
   */
  PROCUREMENT_POLICY_PASS(6),

  /**
   * Fails to pass procurement policy threshold
   */
  PROCUREMENT_POLICY_FAIL(7);

  private int order;

  private CalculationMarker(final int order) {
    this.order = order;
  }

  public int getOrder() {
    return order;
  }

  /**
   * Returns the calculation marker for a statisticType given a resultType
   */
  public static CalculationMarker getCalculationMarker(final List<ResultView> resultViews, final ScenarioResultType resultType,
      final ResultStatisticType statisticType, final PdfProductType pdfProductType) {
    CalculationMarker marker = null;

    switch (resultType) {
    case SITUATION_RESULT:
      marker = getScenarioContributionMarker(statisticType);
      break;
    case PROJECT_CALCULATION:
      marker = getProcessContributionResultMarker(resultViews, statisticType, pdfProductType);
      break;
    case IN_COMBINATION:
      marker = getInCombinationResultMarker(resultViews, statisticType, pdfProductType);
      break;
    case ARCHIVE_CONTRIBUTION:
      marker = getArchiveContributionResultMarker(resultViews, statisticType, pdfProductType);
      break;
    case MAX_TEMPORARY_EFFECT:
      marker = getTemporaryResultMarker(statisticType);
      break;
    case DEPOSITION_SUM:
      marker = getDepositionSumMarker(statisticType);
      break;
    }

    return marker;
  }

  /**
   * Returns the calculation markers for a set of statisticTypes given a resultType
   */
  public static Set<CalculationMarker> getCalculationMarkers(final List<ResultView> resultViews, final ScenarioResultType resultType,
      final Iterable<ResultStatisticType> statisticTypes, final PdfProductType pdfProductType) {
    final Set<CalculationMarker> markers = new HashSet<>();

    for (final ResultStatisticType statisticType : statisticTypes) {
      markers.add(getCalculationMarker(resultViews, resultType, statisticType, pdfProductType));
    }
    markers.removeIf(m -> m == null);

    return markers;
  }

  private static CalculationMarker getScenarioContributionMarker(final ResultStatisticType statisticType) {
    switch (statisticType) {
    case MAX_TOTAL:
      return M1_HIGHEST_TOTAL;
    case MAX_CONTRIBUTION:
      return M2A_HIGHEST_CONTRIBUTION;
    default:
      return null;
    }
  }

  private static CalculationMarker getInCombinationResultMarker(final List<ResultView> resultViews, final ResultStatisticType statisticType,
      final PdfProductType pdfProductType) {
    // Same as PC
    return getProcessContributionResultMarker(resultViews, statisticType, pdfProductType);
  }

  private static CalculationMarker getArchiveContributionResultMarker(final List<ResultView> resultViews, final ResultStatisticType statisticType,
      final PdfProductType pdfProductType) {
    // Same as PC
    return getProcessContributionResultMarker(resultViews, statisticType, pdfProductType);
  }

  private static CalculationMarker getProcessContributionResultMarker(final List<ResultView> resultViews, final ResultStatisticType statisticType,
      final PdfProductType pdfProductType) {
    switch (statisticType) {
    case MAX_INCREASE:
      return M2B_HIGHEST_INCREASE;
    case MAX_DECREASE:
      return pdfProductType != PdfProductType.OWN2000_PERMIT ? M2C_HIGHEST_DECREASE : null;
    case MAX_PERCENTAGE_CRITICAL_LEVEL:
      // If a %CL result view is available, return its marker
      return resultViews.contains(ResultView.PERCENTAGE_CRITICAL_LEVELS) ? M4_MAX_CUMULATION : null;
    case MAX_TOTAL:
      return M1_HIGHEST_TOTAL;
    default:
      return null;
    }
  }

  private static CalculationMarker getTemporaryResultMarker(final ResultStatisticType statisticType) {
    switch (statisticType) {
    case MAX_TEMP_INCREASE:
      return M5_MAX_TOTAL_WITH_EFFECT;
    default:
      return null;
    }
  }

  private static CalculationMarker getDepositionSumMarker(final ResultStatisticType statisticType) {
    switch (statisticType) {
    case PROCUREMENT_POLICY_THRESHOLD_PASS:
      return PROCUREMENT_POLICY_PASS;
    case PROCUREMENT_POLICY_THRESHOLD_FAIL:
      return PROCUREMENT_POLICY_FAIL;
    default:
      return null;
    }
  }
}
