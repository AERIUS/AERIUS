/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

/**
 * States a Job can be in. The names are used in the database (lowercase), so take care when changing.
 */
public enum JobState {
  /**
   * State is undefined.
   */
  UNDEFINED(0, false),
  /**
   * Job created.
   */
  CREATED(0, false),
  /**
   * Job is put on the queue.
   */
  QUEUING(1, false),
  /**
   * Job is picked up and assembled to be calculated.
   */
  PREPARING(2, false),
  /**
   * Job is running the model calculations.
   */
  CALCULATING(3, false),
  /**
   * Job calculation is completed, and post calculation steps are performed (e.g. calculation of summary).
   */
  POST_PROCESSING(4, false),
  /**
   * Job cancelled.
   */
  CANCELLED(5, true),
  /**
   * Job is ended with a error.
   */
  ERROR(5, true),
  /**
   * Job is deemed deleted and can be removed whenever.
   */
  DELETED(5, true),
  /**
   * Job completed.
   */
  COMPLETED(5, true);

  public static final int STEPS = 5;

  private int statePosition;
  private final boolean finalState;

  JobState(final int statePosition, final boolean finalState) {
    this.statePosition = statePosition;
    this.finalState = finalState;
  }

  /**
   * @return Returns the position of the state. This is a number.
   */
  public int getStatePosition() {
    return statePosition;
  }

  public boolean isBefore(final int step) {
    return statePosition < step;
  }

  public boolean isAfter(final int step) {
    return statePosition > step;
  }

  /**
   * @return boolean indicating if this state can be considered final or not.
   */
  public boolean isFinalState() {
    return finalState;
  }

  /**
   * @return boolean indicating if this state can be considered a failure (i.e. the final failure state)
   */
  public boolean isFailureState() {
    return this == ERROR || this == CANCELLED || this == DELETED;
  }

  /**
   * @return boolean indicating if this state can be considered completed (i.e. the final success state)
   */
  public boolean isSuccessState() {
    return this == COMPLETED;
  }

  /**
   * Returns the name as represented in the database.
   * @return name as represented in the database
   */
  public Object toDatabaseString() {
    return name().toLowerCase();
  }

  public static JobState safeValueOf(final String value) {
    try {
      return value == null ? null : valueOf(value.toUpperCase());
    } catch (final IllegalArgumentException e) {
      return null;
    }
  }
}
