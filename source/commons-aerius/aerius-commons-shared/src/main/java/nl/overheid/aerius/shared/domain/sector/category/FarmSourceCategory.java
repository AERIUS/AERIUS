/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.shared.domain.sector.category;

import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Data object to contain Farm Source Categories.
 * A combination of a sector and farm animal, with the appropriate FarmEmissionFactorType.
 * e.g. If a combination for a sector and farm animal is known, that means an emission factor for 1 or more substances is known.
 * The unit of that emission factor is stored here to show appropriate UI elements.
 */
public class FarmSourceCategory extends AbstractEmissionCategory {

  private static final long serialVersionUID = 1L;

  private int sectorId;
  private FarmAnimalCategory farmAnimalCategory;
  private FarmEmissionFactorType farmEmissionFactorType;

  private @JsProperty Map<Substance, Double> emissionFactors = new HashMap<>();

  public FarmSourceCategory() {
    // no-op
  }

  public FarmSourceCategory(final int id, final String code, final String name, final String description,
      final int sectorId, final FarmAnimalCategory farmAnimalCategory, final FarmEmissionFactorType farmEmissionFactorType) {
    super(id, code, name, description);
    this.sectorId = sectorId;
    this.farmAnimalCategory = farmAnimalCategory;
    this.farmEmissionFactorType = farmEmissionFactorType;
  }

  public int getSectorId() {
    return sectorId;
  }

  public void setSectorId(final int sectorId) {
    this.sectorId = sectorId;
  }

  public FarmAnimalCategory getFarmAnimalCategory() {
    return farmAnimalCategory;
  }

  public void setFarmAnimalCategory(final FarmAnimalCategory farmAnimalCategory) {
    this.farmAnimalCategory = farmAnimalCategory;
  }

  public FarmEmissionFactorType getFarmEmissionFactorType() {
    return farmEmissionFactorType;
  }

  public void setFarmEmissionFactorType(final FarmEmissionFactorType farmEmissionFactorType) {
    this.farmEmissionFactorType = farmEmissionFactorType;
  }

  public Map<Substance, Double> getEmissionFactors() {
    return emissionFactors;
  }

  public void addEmissionFactor(final Substance substance, final Double emissionFactor) {
    this.emissionFactors.put(substance, emissionFactor);
  }
}
