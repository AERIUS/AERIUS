/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

/**
 * Data object for a reductive farm lodging system including its reduction factor.<br>
 * The object is published in the RAV (Regeling Ammoniak en Veehouderij) list.<br>
 * <br>
 * A reductive farm lodging system has an id, code, name and description.<br>
 * It reduces emissions in a lodging and can be stacked on top of regular {@link FarmLodgingCategory}.
 */
public class FarmReductiveLodgingSystemCategory extends AbstractFarmLodgingSystemCategory {

  private static final long serialVersionUID = -2849475902288849225L;

  /**
   * Reduction factor per reductive lodging system as a fraction
   */
  private double reductionFactor;

  /**
   * Default public constructor.
   */
  public FarmReductiveLodgingSystemCategory() {
    // No-op.
  }

  public FarmReductiveLodgingSystemCategory(final int id, final String code, final String name, final String description,
      final double reductionFactor, final boolean scrubber) {
    super(id, code, name, description, scrubber);
    this.reductionFactor = reductionFactor;
  }

  public double getReductionFactor() {
    return reductionFactor;
  }

  public void setReductionFactor(final double reductionFactor) {
    this.reductionFactor = reductionFactor;
  }

  @Override
  public String toString() {
    return super.toString() + ", reductionFactor=" + reductionFactor + "]";
  }
}
