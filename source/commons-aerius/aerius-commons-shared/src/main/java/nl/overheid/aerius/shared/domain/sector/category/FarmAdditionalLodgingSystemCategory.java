/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

/**
 * Data object for an additional farm lodging system including its emission factor.<br>
 * The object is published in the RAV (Regeling Ammoniak en Veehouderij) list.<br>
 * <br>
 * An additional farm lodging system has an id, code, name and description.<br>
 * <br>
 * It generates extra emissions in a lodging and can be stacked on top of regular {@link FarmLodgingCategory}.
 */
public class FarmAdditionalLodgingSystemCategory extends AbstractFarmLodgingSystemCategory {

  private static final long serialVersionUID = -397435672857458159L;

  /**
   * Extra emission factor per additional lodging system in kg NH3/year
   */
  private double emissionFactor;

  /**
   * Default public constructor.
   */
  public FarmAdditionalLodgingSystemCategory() {
    // No-op.
  }

  public FarmAdditionalLodgingSystemCategory(final int id, final String code, final String name, final String description,
      final double emissionFactor, final boolean scrubber) {
    super(id, code, name, description, scrubber);
    this.emissionFactor = emissionFactor;
  }

  public double getEmissionFactor() {
    return emissionFactor;
  }

  public void setEmissionFactor(final double emissionFactor) {
    this.emissionFactor = emissionFactor;
  }

  @Override
  public String toString() {
    return super.toString() + ", emissionFactor=" + emissionFactor + "]";
  }
}
