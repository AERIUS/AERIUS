/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

/**
 * Enum to differentiate Calculator Products.
 */
public enum ProductProfile {
  /**
   * Standard Calculator.
   */
  CALCULATOR("Calculator"),
  /**
   * Dutch LBV(-plus) Policy Calculator.
   */
  LBV_POLICY("Check");

  private String productName;

  private ProductProfile(final String productName) {
    this.productName = productName;
  }

  public String getProductName() {
    return productName;
  }

  /**
   * Safely returns the product profile given a string value.
   *
   * @param productProfile string to derive product type enum value of
   * @return prouct type enum or default CALCULATOR if not matches
   */
  public static ProductProfile safeValueOf(final String productProfile) {
    try {
      return productProfile == null ? CALCULATOR : valueOf(productProfile);
    } catch (final IllegalArgumentException e) {
      return CALCULATOR;
    }
  }
}
