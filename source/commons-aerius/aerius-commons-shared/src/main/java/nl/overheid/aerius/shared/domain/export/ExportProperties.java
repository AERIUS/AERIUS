/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.export;

import java.io.Serializable;
import java.util.Set;

import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;


/**
 * Data class containing exports related to sources and/or calculations..
 */
public class ExportProperties extends ExportData implements Serializable {

  private static final long serialVersionUID = 2L;

  private String name;
  private ExportType exportType;
  private Set<ExportAppendix> appendices;
  private ScenarioResults scenarioResults;

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public ExportType getExportType() {
    return exportType;
  }

  public void setExportType(final ExportType exportType) {
    this.exportType = exportType;
  }

  public Set<ExportAppendix> getAppendices() {
    return appendices;
  }

  public void setAppendices(final Set<ExportAppendix> appendices) {
    this.appendices = appendices;
  }

  public ScenarioResults getScenarioResults() {
    return scenarioResults;
  }

  public void setScenarioResults(final ScenarioResults scenarioResults) {
    this.scenarioResults = scenarioResults;
  }
}
