/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

/**
 * A farm animal category.<br>
 * Farm lodging systems and measures are divided in various animal categories.
 */
public class FarmAnimalCategory extends AbstractCategory {

  private static final long serialVersionUID = -1406637610600579586L;

  private AnimalType animalType;

  public FarmAnimalCategory() {
    // No-op.
  }

  public FarmAnimalCategory(final int id, final String code, final String name, final String description) {
    this(id, code, name, description, AnimalType.getByCode(code));
  }

  public FarmAnimalCategory(final int id, final String code, final String name, final String description, final AnimalType animalType) {
    super(id, code, name, description);
    this.animalType = animalType;
  }

  public AnimalType getAnimalType() {
    return animalType;
  }

  public void setAnimalType(final AnimalType animalType) {
    this.animalType = animalType;
  }

}
