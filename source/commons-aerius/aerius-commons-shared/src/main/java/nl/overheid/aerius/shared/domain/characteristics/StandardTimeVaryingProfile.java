/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.characteristics;

import java.util.ArrayList;
import java.util.List;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;

/**
 * Category representing a standard time-varying profile.
 */
public class StandardTimeVaryingProfile extends AbstractCategory {

  private static final long serialVersionUID = 2L;

  private CustomTimeVaryingProfileType type;
  private @JsProperty List<Double> values;

  public StandardTimeVaryingProfile() {
    // No-op.
  }

  public StandardTimeVaryingProfile(final int id, final String code, final String name, final String description,
      final CustomTimeVaryingProfileType type, final List<Double> values) {
    super(id, code, name, description);
    if (type.getExpectedNumberOfValues() != values.size()) {
      throw new IllegalArgumentException("Incorrect values list size for type " + type
          + " for Standard ADMS time-varying profile: " + values.size());
    }
    this.type = type;
    this.values = new ArrayList<>(values);
  }

  public CustomTimeVaryingProfileType getType() {
    return type;
  }

  public List<Double> getValues() {
    return new ArrayList<>(values);
  }

}
