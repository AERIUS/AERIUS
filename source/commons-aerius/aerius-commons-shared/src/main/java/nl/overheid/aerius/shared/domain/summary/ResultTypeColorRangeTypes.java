/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Class to keep track of the ColorRangeType that should be used for result types.
 * In this case, result type means a combination of ScenarioResultType and EmissionResultKey.
 */
public class ResultTypeColorRangeTypes implements Serializable {

  private static final long serialVersionUID = 1L;

  public static class ResultTypeColorRangeTypeKey implements Serializable {
    private static final long serialVersionUID = 1L;

    private ScenarioResultType scenarioResultType;
    private EmissionResultKey emissionResultKey;

    protected ResultTypeColorRangeTypeKey() {
      // Needed for GWT
    }

    public ResultTypeColorRangeTypeKey(final ScenarioResultType scenarioResultType, final EmissionResultKey emissionResultKey) {
      this.scenarioResultType = scenarioResultType;
      this.emissionResultKey = emissionResultKey;
    }

    @Override
    public int hashCode() {
      return Objects.hash(emissionResultKey, scenarioResultType);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      } else if (obj == null || getClass() != obj.getClass()) {
        return false;
      } else {
        final ResultTypeColorRangeTypeKey other = (ResultTypeColorRangeTypeKey) obj;
        return emissionResultKey == other.emissionResultKey && scenarioResultType == other.scenarioResultType;
      }
    }
  }

  private @JsProperty Map<ResultTypeColorRangeTypeKey, ColorRangeType> colorRangeTypes = new HashMap<>();

  public void add(final ScenarioResultType scenarioResultType, final EmissionResultKey emissionResultKey, final ColorRangeType colorRangeType) {
    final ResultTypeColorRangeTypeKey key = new ResultTypeColorRangeTypeKey(scenarioResultType, emissionResultKey);
    colorRangeTypes.put(key, colorRangeType);
  }

  public ColorRangeType determineColorRangeType(final ScenarioResultType scenarioResultType, final EmissionResultKey emissionResultKey) {
    final ResultTypeColorRangeTypeKey key = new ResultTypeColorRangeTypeKey(scenarioResultType, emissionResultKey);
    return colorRangeTypes.getOrDefault(key, ColorRangeType.CONTRIBUTION_DEPOSITION);
  }

}
