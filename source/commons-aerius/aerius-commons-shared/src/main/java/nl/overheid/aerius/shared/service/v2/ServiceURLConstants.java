/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.shared.service.v2;

/**
 * Class with static fields with paths to servlets.
 */
public final class ServiceURLConstants {

  private static final String API_VERSION = "v2";

  /**
   * Base path for all services.
   */
  public static final String BASE_SERVICE_PATH =  API_VERSION + "/service";

  /**
   * URL pattern for service dispatcher servlets.
   */
  public static final String DISPATCHER_SERVLET_MAPPING =  "/application/" + BASE_SERVICE_PATH + "/*";

  /**
   * Context service path, relative to the base service path.
   */
  public static final String CONTEXT_SERVICE_RELATIVE_PATH = "/context";

  /**
   * Context service path.
   */
  public static final String CONTEXT_SERVICE_PATH = BASE_SERVICE_PATH + CONTEXT_SERVICE_RELATIVE_PATH;

  private ServiceURLConstants() {
    // constants class
  }
}
