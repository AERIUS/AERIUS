/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.List;

import jsinterop.annotations.JsProperty;

/**
 * Container for all ColdStartCategory objects.
 */
public class ColdStartCategories implements Serializable {

  private static final long serialVersionUID = 1L;

  private @JsProperty List<ColdStartSourceCategory> standardCategories;
  private @JsProperty List<ColdStartSourceCategory> specificCategories;

  public List<ColdStartSourceCategory> getStandardCategories() {
    return standardCategories;
  }

  public void setStandardCategories(final List<ColdStartSourceCategory> standardCategories) {
    this.standardCategories = standardCategories;
  }

  public List<ColdStartSourceCategory> getSpecificCategories() {
    return specificCategories;
  }

  public void setSpecificCategories(final List<ColdStartSourceCategory> specificCategories) {
    this.specificCategories = specificCategories;
  }

  /**
   * @param code The cold start source category code to determine the standard ColdStartSourceCategory object for.
   * @return The right ColdStartSourceCategory object, or null if no match could be made based on the code.
   */
  public ColdStartSourceCategory determineStandardByCode(final String code) {
    return AbstractCategory.determineByCode(standardCategories, code);
  }

  /**
   * @param code The cold start source category code to determine the specific ColdStartSourceCategory object for.
   * @return The right ColdStartSourceCategory object, or null if no match could be made based on the code.
   */
  public ColdStartSourceCategory determineSpecificByCode(final String code) {
    return AbstractCategory.determineByCode(specificCategories, code);
  }

}
