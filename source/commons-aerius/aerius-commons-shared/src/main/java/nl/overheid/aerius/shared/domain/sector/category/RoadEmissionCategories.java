/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory.RoadEmissionCategoryKey;

/**
 * Container for all RoadEmissionCategory objects.
 *
 * The algorithm assumes if there is a category with strict speed there always is a category with the same speed but non strict.
 */
public class RoadEmissionCategories implements Serializable {

  private static final long serialVersionUID = 3L;

  private @JsProperty List<SimpleCategory> areas = new ArrayList<>();
  private @JsProperty List<SimpleCategory> roadTypes = new ArrayList<>();
  private @JsProperty List<SimpleCategory> vehicleTypes = new ArrayList<>();

  private @JsProperty Set<String> hideRoadTypeCodes = new HashSet<>();

  private @JsProperty Map<String, Set<String>> areasToRoadTypes = new HashMap<>();

  private @JsProperty Map<RoadCategorySetPropertiesKey, RoadCategorySetProperties> categorySetProperties = new HashMap<>();

  private @JsProperty Map<RoadEmissionCategoryKey, Set<RoadEmissionCategory>> roadCategories = new HashMap<>();

  /**
   * Adds the given road emission category.
   * @param category The road emission category to add.
   * @return True if the category has been added, false if it already existed.
   */
  public boolean add(final RoadEmissionCategory category) {
    final RoadEmissionCategoryKey key = category.getKey();

    addToCategorySetProperties(key, category);

    addToAreaToRoadTypes(key);

    // Finally add the category to the correct set of categories.
    final Set<RoadEmissionCategory> existingCategories = roadCategories.computeIfAbsent(key, someKey -> new HashSet<>());
    return existingCategories.add(category);
  }

  private void addToCategorySetProperties(final RoadEmissionCategoryKey key, final RoadEmissionCategory category) {
    // Add CategorySetProperties
    final RoadCategorySetPropertiesKey setPropertiesKey = new RoadCategorySetPropertiesKey(key);
    final RoadCategorySetProperties setProperties =
        categorySetProperties.computeIfAbsent(setPropertiesKey, someKey -> new RoadCategorySetProperties(category.isSpeedRelevant()));
    if (setProperties.isSpeedEffect() != category.isSpeedRelevant()) {
      // Do not allow category sets that have both categories where speed is relevant and categories where speed is not relevant.
      throw new IllegalArgumentException("Inconsistent speed definitions for " + key);
    }
    if (category.isSpeedRelevant()) {
      setProperties.addSpeed(category.getMaximumSpeed(), category.isStrictEnforcement());
    }
    // Take note of the gradient per vehicle type (used for interpolation, UK)
    setProperties.addGradient(key.getVehicleTypeCode(), category.getGradient());
  }

  private void addToAreaToRoadTypes(final RoadEmissionCategoryKey key) {
    // Add area to road type combination
    areasToRoadTypes
        .computeIfAbsent(key.getAreaCode(), newKey -> new HashSet<>())
        .add(key.getRoadTypeCode());

  }

  /**
   * @param areaCode The road area code for which to list the speed categories.
   * @param roadTypeCode The road type code for which to list the speed categories.
   * @return The set of speed categories for the given road type,
   * or an empty set if the codes are not valid or if maximum speed is not applicable.
   */
  public List<Integer> getMaximumSpeedCategories(final String areaCode, final String roadTypeCode) {
    final Set<Integer> maxSpeeds;
    final RoadCategorySetPropertiesKey key = new RoadCategorySetPropertiesKey(areaCode, roadTypeCode);
    if (categorySetProperties.containsKey(key)) {
      maxSpeeds = categorySetProperties.get(key).getSpeeds();
    } else {
      maxSpeeds = new HashSet<>();
    }

    return maxSpeeds.stream().sorted().collect(Collectors.toList());
  }

  /**
   * @param areaCode The road area code for which to list the gradient categories.
   * @param roadTypeCode The road type code for which to list the gradient categories.
   * @param vehicleTypeCode The vehicle type code for which to list the gradient categories
   * @return The set of gradient categories for the given road type and vehicle,
   * or an empty set if the codes are not valid or if gradient is not applicable.
   */
  public List<Integer> getGradientCategories(final String areaCode, final String roadTypeCode, final String vehicleTypeCode) {
    final Set<Integer> gradients;
    final RoadCategorySetPropertiesKey key = new RoadCategorySetPropertiesKey(areaCode, roadTypeCode);
    if (categorySetProperties.containsKey(key)) {
      gradients = categorySetProperties.get(key).getGradients(vehicleTypeCode);
    } else {
      gradients = new HashSet<>();
    }

    return gradients.stream().sorted().collect(Collectors.toList());
  }

  /**
   * @param areaCode The road area code for which to list the speed categories with strict enforcement.
   * @param roadTypeCode The road type code for which to list the speed categories with strict enforcement.
   * @return The set of speed categories for the given road type,
   * or an empty set if the codes are not valid or if maximum speed is not applicable.
   */
  public List<Integer> getMaximumSpeedCategoriesStrict(final String areaCode, final String roadTypeCode) {
    final Set<Integer> maxSpeeds;
    final RoadCategorySetPropertiesKey key = new RoadCategorySetPropertiesKey(areaCode, roadTypeCode);
    if (categorySetProperties.containsKey(key)) {
      maxSpeeds = categorySetProperties.get(key).getSpeedsStrictEnforcement();
    } else {
      maxSpeeds = new HashSet<>();
    }

    return maxSpeeds.stream().sorted().collect(Collectors.toList());
  }

  /**
   * Finds the road category that has higher or equal speed and same other properties.
   *
   * @param roadType road type
   * @param vehicleType vehicle type
   * @param strictEnforcement true if strict enforcement
   * @param maxSpeed speed
   * @param speedType Speed type of non freeway
   * @return road category or null if no category found.
   */
  public RoadEmissionCategory findClosestCategory(final String areaCode, final String roadTypeCode, final String vehicleTypeCode,
      final Boolean strictEnforced, final Integer maximumSpeed, final Double gradient) {
    final int maximumSpeedUnBoxed = maximumSpeed == null ? 0 : maximumSpeed;
    final int gradientUnBoxed = gradient == null ? 0 : gradient.intValue();
    final RoadEmissionCategoryKey key = new RoadEmissionCategoryKey(areaCode, roadTypeCode, vehicleTypeCode);
    return findClosestCategoryUnboxed(key, Boolean.TRUE.equals(strictEnforced), maximumSpeedUnBoxed, gradientUnBoxed);
  }

  private RoadEmissionCategory findClosestCategoryUnboxed(final RoadEmissionCategoryKey key,
      final boolean strictEnforced, final int maximumSpeed, final int gradient) {
    final RoadEmissionCategory candidate;
    if (roadCategories.containsKey(key)) {
      final RoadEmissionCategory cat = new RoadEmissionCategory(key, strictEnforced, maximumSpeed, gradient);
      // First try to get a candidate based on speed.
      final RoadEmissionCategory speedCandidate = getCandidate(roadCategories.get(key), cat, x -> x.isStrictEnforcement() == strictEnforced,
          RoadEmissionCategories::compareOnSpeed);
      // Next determine the candidate for gradient, by looking at all categories with that max speed.
      candidate = getCandidate(roadCategories.get(key), cat,
          x -> x.isStrictEnforcement() == strictEnforced && x.getMaximumSpeed() == speedCandidate.getMaximumSpeed(),
          RoadEmissionCategories::compareOnGradient);
    } else {
      candidate = null;
    }

    return strictEnforced
        ? findClosestIfStrict(maximumSpeed, candidate, findClosestCategoryUnboxed(key, false, maximumSpeed, gradient))
        : candidate;
  }

  private RoadEmissionCategory getCandidate(final Set<RoadEmissionCategory> categories, final RoadEmissionCategory target,
      final Predicate<RoadEmissionCategory> filter, final Comparator<RoadEmissionCategory> comparator) {
    final TreeSet<RoadEmissionCategory> correctCategories = categories.stream()
        .filter(filter)
        .collect(Collectors.toCollection(() -> new TreeSet<>(comparator)));
    final RoadEmissionCategory ceiling = correctCategories.ceiling(target);
    return ceiling == null ? correctCategories.floor(target) : ceiling;
  }

  private static int compareOnSpeed(final RoadEmissionCategory o1, final RoadEmissionCategory o2) {
    return Integer.compare(o1.getMaximumSpeed(), o2.getMaximumSpeed());
  }

  private static int compareOnGradient(final RoadEmissionCategory o1, final RoadEmissionCategory o2) {
    return Integer.compare(o1.getGradient(), o2.getGradient());
  }

  /**
   * Finds the closest category if looking for a strict enforced speed category.
   * @param speed given speed
   * @param nonStrictCat the category found using strict enforcement.
   * @param strictCat the category found using strict enforcement.
   * @return
   */
  private static RoadEmissionCategory findClosestIfStrict(final int speed, final RoadEmissionCategory strictCat,
      final RoadEmissionCategory nonStrictCat) {
    final RoadEmissionCategory result;
    if (strictCat == null) {
      result = nonStrictCat;
    } else if (strictCat.getMaximumSpeed() == speed) {
      result = strictCat;
    } else {
      result = findClosestOther(speed, strictCat, nonStrictCat);
    }
    return result;
  }

  /**
   * Find the closest if the speed doesn't matches any speed in categories list.
   * @param speed given speed
   * @param nonStrictCat the category found using strict enforcement.
   * @param strictCat the category found using strict enforcement.
   * @return
   */
  private static RoadEmissionCategory findClosestOther(final int speed, final RoadEmissionCategory strictCat,
      final RoadEmissionCategory nonStrictCat) {
    final boolean nsG = speed >= nonStrictCat.getMaximumSpeed();
    final boolean sG = speed >= strictCat.getMaximumSpeed();
    final RoadEmissionCategory result;
    if (nsG && sG) {
      // If both categories below speed take non-strict but only if it has a higher speed as the strict.
      result = nonStrictCat.getMaximumSpeed() > strictCat.getMaximumSpeed() ? nonStrictCat : strictCat;
    } else if (nsG) {
      // If not strict higher speed, but strict is below given speed take not strict.
      result = nonStrictCat;
    } else if (sG) {
      // If strict higher speed, but not strict is below given speed take strict.
      result = strictCat;
    } else {
      // If speed is below both categories take the lowest (i.e. the one closest to the speed).
      result = strictCat.getMaximumSpeed() > nonStrictCat.getMaximumSpeed() ? nonStrictCat : strictCat;
    }
    return result;
  }

  /**
   * Get the exact road category for the list of categories.
   * @param category category to look up.
   * @return null if not equals matching road category
   */
  public RoadEmissionCategory getRoadEmissionCategory(final RoadEmissionCategory category) {
    if (roadCategories.containsKey(category.getKey())) {
      return roadCategories.get(category.getKey()).stream()
          .filter(x -> x.equals(category))
          .findFirst().orElse(null);
    } else {
      return null;
    }
  }

  public List<SimpleCategory> getAreas() {
    return areas;
  }

  public void setAreas(final List<SimpleCategory> areas) {
    this.areas = areas;
  }

  public List<SimpleCategory> getRoadTypes() {
    return roadTypes;
  }

  public void setRoadTypes(final List<SimpleCategory> roadTypes) {
    this.roadTypes = roadTypes;
  }

  public Set<String> getHideRoadTypeCodes() {
    return hideRoadTypeCodes;
  }

  public void setHideRoadTypeCodes(final Set<String> hideRoadTypeCodes) {
    this.hideRoadTypeCodes = hideRoadTypeCodes;
  }

  public List<SimpleCategory> getVehicleTypes() {
    return vehicleTypes;
  }

  public void setVehicleTypes(final List<SimpleCategory> vehicleTypes) {
    this.vehicleTypes = vehicleTypes;
  }

  public List<String> getVehicleTypesCodes() {
    return vehicleTypes.stream()
        .map(SimpleCategory::getCode)
        .collect(Collectors.toList());
  }

  public SimpleCategory getRoadType(final String roadTypeCode) {
    return roadTypes.stream()
        .filter(x -> x.getCode().equalsIgnoreCase(roadTypeCode))
        .findFirst().orElse(null);
  }

  public SimpleCategory getArea(final String areaCode) {
    return areas.stream()
        .filter(x -> x.getCode().equalsIgnoreCase(areaCode))
        .findFirst().orElse(null);
  }

  public List<SimpleCategory> getRoadTypes(final String areaCode) {
    final Set<String> correctRoadTypes = areasToRoadTypes.get(areaCode);
    return correctRoadTypes == null ? new ArrayList<>()
        : roadTypes.stream()
            .filter(roadType -> correctRoadTypes.contains(roadType.getCode()))
            .filter(roadType -> !hideRoadTypeCodes.contains(roadType.getCode()))
            .collect(Collectors.toList());
  }
}
