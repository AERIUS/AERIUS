/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.Min;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.EmissionValueKey;

/**
 * Data object for Road Emission factor.
 *
 * NOTE: does not extend {@link AbstractEmissionCategory} as it does not have an ID or code at the moment.
 */
public class RoadEmissionCategory implements Serializable, Comparable<RoadEmissionCategory> {

  public static class RoadEmissionCategoryKey implements Serializable {

    private static final long serialVersionUID = 1L;

    private String areaCode;
    private String roadTypeCode;
    private String vehicleTypeCode;

    protected RoadEmissionCategoryKey() {
      // Needed for GWT
    }

    public RoadEmissionCategoryKey(final String areaCode, final String roadTypeCode, final String vehicleTypeCode) {
      this.areaCode = areaCode;
      this.roadTypeCode = roadTypeCode;
      this.vehicleTypeCode = vehicleTypeCode;
    }

    protected String getAreaCode() {
      return areaCode;
    }

    protected String getRoadTypeCode() {
      return roadTypeCode;
    }

    protected String getVehicleTypeCode() {
      return vehicleTypeCode;
    }

    @Override
    public int hashCode() {
      return Objects.hash(areaCode, roadTypeCode, vehicleTypeCode);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      } else if (obj == null || getClass() != obj.getClass()) {
        return false;
      } else {
        final RoadEmissionCategoryKey other = (RoadEmissionCategoryKey) obj;
        return Objects.equals(areaCode, other.areaCode) && Objects.equals(roadTypeCode, other.roadTypeCode)
            && Objects.equals(vehicleTypeCode, other.vehicleTypeCode);
      }
    }

    @Override
    public String toString() {
      return "RoadEmissionCategoryKey [areaCode=" + areaCode + ", roadTypeCode=" + roadTypeCode + ", vehicleTypeCode=" + vehicleTypeCode + "]";
    }

  }

  private static final long serialVersionUID = 6L;

  private RoadEmissionCategoryKey key;
  // if the speed is 0, it's irrelevant.
  private int maximumSpeed;
  private boolean strictEnforcement;
  private int gradient;

  /**
   * Emission factor in g/24h/km
   */
  private @JsProperty Map<EmissionValueKey, Double> emissionFactors = new HashMap<>();
  private @JsProperty Map<EmissionValueKey, Double> stagnatedEmissionFactors = new HashMap<>();

  protected RoadEmissionCategory() {
    // Needed for GWT.
  }

  public RoadEmissionCategory(final RoadEmissionCategoryKey key, final boolean strictEnforcement, final int maximumSpeed, final int gradient) {
    this.key = key;
    this.strictEnforcement = strictEnforcement;
    this.maximumSpeed = maximumSpeed;
    this.gradient = gradient;
  }

  @Override
  public int compareTo(final RoadEmissionCategory other) {
    return compareToHelper(
        Integer.compare(key.hashCode(), other.key.hashCode()),
        //Ensuring categories that are not strict enforced get sorted first
        Boolean.compare(other.isStrictEnforcement(), strictEnforcement),
        Integer.compare(maximumSpeed, other.getMaximumSpeed()),
        Integer.compare(gradient, other.getGradient()));
  }

  private static int compareToHelper(final int... compares) {
    for (final int cmp : compares) {
      if (cmp != 0) {
        return cmp;
      }
    }
    return 0;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    } else if (obj == null || getClass() != obj.getClass()) {
      return false;
    } else {
      final RoadEmissionCategory other = (RoadEmissionCategory) obj;
      return Objects.equals(key, other.key)
          && maximumSpeed == other.maximumSpeed
          && strictEnforcement == other.strictEnforcement
          && gradient == other.gradient;
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(key, maximumSpeed, strictEnforcement);
  }

  public RoadEmissionCategoryKey getKey() {
    return key;
  }

  @Min(value = 0)
  public int getMaximumSpeed() {
    return maximumSpeed;
  }

  public boolean isStrictEnforcement() {
    return strictEnforcement;
  }

  public double getEmissionFactor(final EmissionValueKey evk) {
    return emissionFactors.containsKey(evk) ? emissionFactors.get(evk) : 0;
  }

  public double getStagnatedEmissionFactor(final EmissionValueKey evk) {
    return stagnatedEmissionFactors.containsKey(evk) ? stagnatedEmissionFactors.get(evk) : 0;
  }

  public ArrayList<EmissionValueKey> getEmissionValueKeys() {
    return new ArrayList<EmissionValueKey>(emissionFactors.keySet());
  }

  public void setEmissionFactor(final EmissionValueKey evk, final double emissionFactor) {
    emissionFactors.put(evk, emissionFactor);
  }

  public void setStagnatedEmissionFactor(final EmissionValueKey evk, final double stagnatedEmissionFactor) {
    stagnatedEmissionFactors.put(evk, stagnatedEmissionFactor);
  }

  public int getGradient() {
    return gradient;
  }

  public boolean isSpeedRelevant() {
    return maximumSpeed > 0;
  }

  @Override
  public String toString() {
    return "RoadEmissionCategory [key=" + key + ",maximumSpeed=" + maximumSpeed + ",strictEnforcement=" + strictEnforcement
        + ",gradient=" + gradient + "]";
  }
}
