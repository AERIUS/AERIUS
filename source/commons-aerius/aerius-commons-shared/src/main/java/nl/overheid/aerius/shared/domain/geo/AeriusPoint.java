/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.HasId;

/**
 * Base class for all AERIUS points on the map. A point can represent a receptor, meaning on the hexagon grid or a free/normal point.
 */
public class AeriusPoint extends SridPoint implements Serializable, HasId {

  private static final long serialVersionUID = 4L;

  /**
   * Identifies how this point should be interpreted.
   */
  public enum AeriusPointType {
    /**
     * Hexagon of x m2, point is at the center of the hexagon.
     */
    RECEPTOR(false),
    /**
     * Point inside a hexagon as part of a sub receptor computation.
     */
    SUB_RECEPTOR(true),
    /**
     * Normal point.
     */
    POINT(false),
    /**
     * Point inside a hexagon as part of a sub point computation.
     */
    SUB_POINT(true);

    boolean subReceptor;

    AeriusPointType(final boolean subReceptor) {
      this.subReceptor = subReceptor;
    }

    public boolean isSubReceptor() {
      return subReceptor;
    }
  }

  private int id;
  private Integer parentId;
  private String gmlId;
  private AeriusPointType pointType;
  private String label;
  private Double height;

  private transient String gml;

  // Needed by GWT.
  public AeriusPoint() {
    pointType = AeriusPointType.RECEPTOR;
  }

  /**
   * Initializes this AeriusPoint with the given ID.
   *
   * @param id the ID
   */
  public AeriusPoint(final int id) {
    this(id, AeriusPointType.RECEPTOR);
  }

  public AeriusPoint(final AeriusPointType pointType) {
    this.pointType = pointType;
  }

  public AeriusPoint(final int id, final AeriusPointType pointType) {
    this.id = id;
    this.pointType = pointType;
  }

  /**
   * Constructs this AeriusPoint with the given location.
   *
   * @param x x coordinate
   * @param y y coordinate
   */
  public AeriusPoint(final double x, final double y) {
    this(0, 0, AeriusPointType.RECEPTOR, x, y);
  }

  /**
   * Constructs this AeriusPoint with the given id, parentId, and location.
   *
   * @param id
   * @param parentId
   * @param pointType
   * @param x
   * @param y
   */
  public AeriusPoint(final int id, final Integer parentId, final AeriusPointType pointType, final double x, final double y) {
    this(id, parentId, pointType, x, y, null);
  }

  /**
   * Constructs this AeriusPoint with the given id, parentId, location and height.
   *
   * @param id
   * @param parentId
   * @param pointType
   * @param x
   * @param y
   * @param height
   */
  public AeriusPoint(final int id, final Integer parentId, final AeriusPointType pointType, final double x, final double y, final Double height) {
    super(x, y);
    this.id = id;
    this.parentId = parentId;
    this.pointType = pointType;
    this.height = height;
  }

  protected AeriusPoint copy(final AeriusPoint copy) {
    copy.id = id;
    copy.parentId = parentId;
    copy.gmlId = gmlId;
    copy.pointType = pointType;
    copy.label = label;
    copy.setX(getX());
    copy.setY(getY());
    copy.setHeight(getHeight());
    copy.gml = gml;
    return copy;
  }

  /**
   * Objects are equal if same type and same id or if obj is a point and the super is equal.
   */
  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof AeriusPoint) {
      final AeriusPoint other = (AeriusPoint) obj;
      return pointType == other.getPointType()
          && id == other.getId()
          && equalParentId(other);
    } else {
      return super.equals(obj);
    }
  }

  private boolean equalParentId(final AeriusPoint other) {
    final boolean bothNull = parentId == null && other.getParentId() == null;
    final boolean bothNotNullAndEqual = parentId != null && other.getParentId() != null && parentId.intValue() == other.getParentId().intValue();
    return bothNull || bothNotNullAndEqual;
  }

  @Override
  public int hashCode() {
    return id + 31 * pointType.hashCode();
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  public Integer getParentId() {
    return parentId;
  }

  public void setParentId(final Integer parentId) {
    this.parentId = parentId;
  }

  public String getGmlId() {
    return gmlId;
  }

  public void setGmlId(final String gmlId) {
    this.gmlId = gmlId;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  public void setPointType(final AeriusPointType pointType) {
    this.pointType = pointType;
  }

  public AeriusPointType getPointType() {
    return pointType;
  }

  public String getGml() {
    return gml;
  }

  public void setGml(final String gml) {
    this.gml = gml;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(final Double height) {
    this.height = height;
  }

  @Override
  public String toString() {
    return "AeriusPoint [id=" + id + "(" + pointType + "), parentId=" + parentId + ", gmlId=" + gmlId + ", " + super.toString() + "]";
  }
}
