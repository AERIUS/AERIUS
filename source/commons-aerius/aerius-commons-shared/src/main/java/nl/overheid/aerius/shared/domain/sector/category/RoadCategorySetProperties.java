/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import jsinterop.annotations.JsProperty;

class RoadCategorySetProperties implements Serializable {

  private static final long serialVersionUID = 2L;

  private boolean speedEffect;
  private @JsProperty Set<Integer> speeds = new HashSet<>();
  private @JsProperty Set<Integer> speedsStrictEnforcement = new HashSet<>();
  private @JsProperty Map<String, Set<Integer>> gradientsPerVehicleType = new HashMap<>();

  protected RoadCategorySetProperties() {
    // Needed for GWT.
  }

  RoadCategorySetProperties(final boolean speedEffect) {
    this.speedEffect = speedEffect;
  }

  void addSpeed(final int speed, final boolean strictEnforcement) {
    if (strictEnforcement) {
      speedsStrictEnforcement.add(speed);
    } else {
      speeds.add(speed);
    }
  }

  boolean isSpeedEffect() {
    return speedEffect;
  }

  Set<Integer> getSpeeds() {
    return speeds;
  }

  Set<Integer> getSpeedsStrictEnforcement() {
    return speedsStrictEnforcement;
  }

  void addGradient(final String vehicleType, final int gradient) {
    gradientsPerVehicleType.computeIfAbsent(vehicleType, key -> new HashSet<>())
        .add(gradient);
  }

  Set<Integer> getGradients(final String vehicleType) {
    return gradientsPerVehicleType.getOrDefault(vehicleType, new HashSet<>());
  }

}
