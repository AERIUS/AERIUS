/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Data class of ops receptor data.
 */
public class OPSReceptor extends AeriusPoint {

  private static final long serialVersionUID = 2L;

  private OPSTerrainProperties terrainProperties;

  /**
   * Constructor.
   */
  public OPSReceptor() {
    /* No-op. */
  }

  public OPSReceptor(final int id) {
    super(id);
  }

  public OPSReceptor(final int id, final double x, final double y) {
    super(id, null, AeriusPointType.POINT, x, y);
  }

  public OPSReceptor(final AeriusPoint point) {
    this(point, point.getPointType());
  }

  public OPSReceptor(final AeriusPoint point, final AeriusPointType pointType) {
    super(point.getId(), point.getParentId(), pointType, point.getX(), point.getY(), point.getHeight());
    if (point instanceof OPSReceptor) {
      final OPSReceptor opsp = (OPSReceptor) point;
      terrainProperties = opsp.terrainProperties == null ? null : opsp.terrainProperties.copy();
    }
  }

  @Override
  @Min(value = OPSLimits.X_COORDINATE_MINIMUM)
  @Max(value = OPSLimits.X_COORDINATE_MAXIMUM)
  public double getX() {
    return super.getX();
  }

  @Override
  @Min(value = OPSLimits.Y_COORDINATE_MINIMUM)
  @Max(value = OPSLimits.Y_COORDINATE_MAXIMUM)
  public double getY() {
    return super.getY();
  }

  public OPSTerrainProperties getTerrainProperties() {
    return terrainProperties;
  }

  public void setTerrainProperties(final OPSTerrainProperties terrainProperties) {
    this.terrainProperties = terrainProperties;
  }

  public boolean hasNoTerrainData() {
    return terrainProperties == null || terrainProperties.hasNoTerrainData();
  }

  public boolean hasTerrainData() {
    return !hasNoTerrainData();
  }

  public boolean hasNoHeight() {
    return getHeight() == null;
  }

  public boolean hasHeight() {
    return !hasNoHeight();
  }

  @Override
  public String toString() {
    return "OPSReceptor [terrainProperties=" + terrainProperties + ", " + super.toString();
  }
}
