/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.characteristics.TimeVaryingProfiles;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.ShippingNode;

/**
 * Data object with all sector categories.
 */
public class SectorCategories implements Serializable {

  private static final long serialVersionUID = 3L;

  // Source
  private TimeVaryingProfiles timeVaryingProfiles;

  // Sectors
  private @JsProperty List<Sector> sectors;

  // Shipping
  private @JsProperty List<ShippingNode> shippingSnappableNodes;

  // Emission categories
  private FarmLodgingCategories farmLodgingCategories;
  private FarmAnimalHousingCategories farmAnimalHousingCategories;
  private @JsProperty List<FarmlandCategory> farmlandCategories;
  private @JsProperty List<FarmSourceCategory> farmSourceCategories;
  private RoadEmissionCategories roadEmissionCategories;
  private @JsProperty List<OffRoadMobileSourceCategory> offRoadMobileSourceCategories;
  private @JsProperty List<OnRoadMobileSourceCategory> onRoadMobileSourceCategories;
  private @JsProperty List<MaritimeShippingCategory> maritimeShippingCategories;
  private @JsProperty InlandShippingCategories inlandShippingCategories;
  private @JsProperty ColdStartCategories coldStartCategories;

  /**
   * Returns the {@link Sector} matching the sectorId or {@link Sector#SECTOR_UNDEFINED}
   * if the sectorId wasn't found.
   *
   * @param sectorId The ID to search for
   * @return Sector or sector undefined if no matching id
   */
  public Sector getSectorById(final int sectorId) {
    for (final Sector s : sectors) {
      if (s.getSectorId() == sectorId) {
        return s;
      }
    }
    return Sector.SECTOR_UNDEFINED;
  }

  /**
   * @param subSectorId The subsector ID to determine the Sector object for.
   * @return The right Sector object, or Sector.SECTOR_DEFAULT if no match could be made based on the ID.
   */
  public Sector determineSectorById(final int subSectorId) {
    // Default the sector in case the default sector cannot be found in the provided list
    Sector rightSector = Sector.SECTOR_DEFAULT;

    // Find the correct sector
    if (sectors != null) {
      for (final Sector sector : sectors) {
        if (sector.getSectorId() == subSectorId) {
          rightSector = sector;
          break;
        }
      }
    }
    return rightSector;
  }

  /**
   * @param code The farm lodging category code to determine the FarmLodgingCategory object for.
   * @return The right FarmLodgingCategory object, or null if no match could be made based on the code.
   */
  public FarmLodgingCategory determineFarmLodgingCategoryByCode(final String code) {
    return farmLodgingCategories.determineFarmLodgingCategoryByCode(code);
  }

  /**
   * @param code The farmland category code to determine the FarmlandCategory object for.
   * @return The right FarmlandCategory object, or null if no match could be made based on the code.
   */
  public FarmlandCategory determineFarmlandCategoryByCode(final String code) {
    return determineCategoryByCode(farmlandCategories, code);
  }

  /**
   * @param code The farm source category code to determine the FarmSourceCategory object for.
   * @return The right FarmSourceCategory object, or null if no match could be made based on the code.
   */
  public FarmSourceCategory determineFarmSourceByCode(final String code) {
    return determineCategoryByCode(farmSourceCategories, code);
  }

  /**
   * @param code The mobile source category code to determine the OnRoadMobileSourceCategory object for.
   * @return The right OnRoadMobileSourceCategory object, or null if no match could be made based on the code.
   */
  public OnRoadMobileSourceCategory determineOnRoadMobileSourceCategoryByCode(final String code) {
    return determineCategoryByCode(onRoadMobileSourceCategories, code);
  }

  /**
   * @param code The mobile source category code to determine the OffRoadMobileSourceCategory object for.
   * @return The right OffRoadMobileSourceCategory object, or null if no match could be made based on the code.
   */
  public OffRoadMobileSourceCategory determineOffRoadMobileSourceCategoryByCode(final String code) {
    return determineCategoryByCode(offRoadMobileSourceCategories, code);
  }

  /**
   * @param code The maritime shipping category code to determine the MaritimeShippingCategory object for.
   * @return The right MaritimeShippingCategory object, or null if no match could be made based on the code.
   */
  public MaritimeShippingCategory determineMaritimeShippingCategoryByCode(final String code) {
    return determineCategoryByCode(maritimeShippingCategories, code);
  }

  private static <T extends AbstractCategory> T determineCategoryByCode(final List<T> categories, final String code) {
    return AbstractCategory.determineByCode(categories, code);
  }

  public List<Sector> getSectors() {
    return sectors;
  }

  public void setSectors(final List<Sector> sectors) {
    this.sectors = sectors;
  }

  public SectorGroup findSectorGroupFromSectorId(final int sectorId) {
    return getSectors()
        .stream().filter(v -> v.getSectorId() == sectorId)
        .findFirst()
        .map(v -> v.getSectorGroup())
        .orElse(null);
  }

  public FarmAnimalHousingCategories getFarmAnimalHousingCategories() {
    return farmAnimalHousingCategories;
  }

  public void setFarmAnimalHousingCategories(final FarmAnimalHousingCategories farmAnimalHousingCategories) {
    this.farmAnimalHousingCategories = farmAnimalHousingCategories;
  }

  @Deprecated
  public FarmLodgingCategories getFarmLodgingCategories() {
    return farmLodgingCategories;
  }

  @Deprecated
  public void setFarmLodgingCategories(final FarmLodgingCategories farmLodgingCategories) {
    this.farmLodgingCategories = farmLodgingCategories;
  }

  public List<FarmlandCategory> getFarmlandCategories() {
    return farmlandCategories;
  }

  public void setFarmlandCategories(final List<FarmlandCategory> farmlandCategories) {
    this.farmlandCategories = farmlandCategories;
  }

  public List<FarmSourceCategory> getFarmSourceCategories() {
    return farmSourceCategories;
  }

  public void setFarmSourceCategories(final List<FarmSourceCategory> farmSourceCategories) {
    this.farmSourceCategories = farmSourceCategories;
  }

  public List<FarmSourceCategory> getFarmSourceCategories(final int sectorId) {
    return this.farmSourceCategories.stream().filter(c -> c.getSectorId() == sectorId).collect(Collectors.toList());
  }

  public ColdStartCategories getColdStartCategories() {
    return coldStartCategories;
  }

  public void setColdStartCategories(final ColdStartCategories coldStartCategories) {
    this.coldStartCategories = coldStartCategories;
  }

  public RoadEmissionCategories getRoadEmissionCategories() {
    return roadEmissionCategories;
  }

  public void setRoadEmissionCategories(final RoadEmissionCategories roadEmissionCategories) {
    this.roadEmissionCategories = roadEmissionCategories;
  }

  public List<OffRoadMobileSourceCategory> getOffRoadMobileSourceCategories() {
    return offRoadMobileSourceCategories;
  }

  public void setOffRoadMobileSourceCategories(final List<OffRoadMobileSourceCategory> offRoadMobileSourceCategories) {
    this.offRoadMobileSourceCategories = offRoadMobileSourceCategories;
  }

  public List<OnRoadMobileSourceCategory> getOnRoadMobileSourceCategories() {
    return onRoadMobileSourceCategories;
  }

  public void setOnRoadMobileSourceCategories(final List<OnRoadMobileSourceCategory> onRoadMobileSourceCategories) {
    this.onRoadMobileSourceCategories = onRoadMobileSourceCategories;
  }

  public List<MaritimeShippingCategory> getMaritimeShippingCategories() {
    return maritimeShippingCategories;
  }

  public void setMaritimeShippingCategories(final List<MaritimeShippingCategory> maritimeShippingCategories) {
    this.maritimeShippingCategories = maritimeShippingCategories;
  }

  public InlandShippingCategories getInlandShippingCategories() {
    return inlandShippingCategories;
  }

  public void setInlandShippingCategories(final InlandShippingCategories inlandShippingCategories) {
    this.inlandShippingCategories = inlandShippingCategories;
  }

  public List<ShippingNode> getShippingSnappableNodes() {
    return shippingSnappableNodes;
  }

  public void setShippingSnappableNodes(final List<ShippingNode> shippingSnappableNodes) {
    this.shippingSnappableNodes = shippingSnappableNodes;
  }

  public void setTimeVaryingProfiles(final TimeVaryingProfiles timeVaryingProfiles) {
    this.timeVaryingProfiles = timeVaryingProfiles;
  }

  public TimeVaryingProfiles getTimeVaryingProfiles() {
    return timeVaryingProfiles;
  }
}
