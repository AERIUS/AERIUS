/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Data object for an additional farm animal housing system including its reduction factors.<br>
 * <br>
 * A additional farm housing system has an id, code, name and description.<br>
 * It generally reduces emissions in a animal housing system and can be stacked on top of regular {@link FarmAnimalHousingCategory}.
 */
public class FarmAdditionalHousingSystemCategory extends AbstractEmissionCategory {

  private static final long serialVersionUID = 1L;

  /**
   * Indication if the additional system is an air scrubber.
   */
  private boolean airScrubber;
  /**
   * Reduction fractions for the additional housing system, per housing category.
   * Key is the animal code.
   */
  private @JsProperty Map<String, Map<Substance, Double>> reductionFractions = new HashMap<>();

  /**
   * Default public constructor.
   */
  public FarmAdditionalHousingSystemCategory() {
    // No-op.
  }

  public FarmAdditionalHousingSystemCategory(final int id, final String code, final String name, final String description,
      final boolean airScrubber) {
    super(id, code, name, description);
    this.airScrubber = airScrubber;
  }

  public boolean isAirScrubber() {
    return airScrubber;
  }

  public void setAirScrubber(final boolean airScrubber) {
    this.airScrubber = airScrubber;
  }

  public Map<String, Map<Substance, Double>> getReductionFractions() {
    return reductionFractions;
  }

  public void addReductionFraction(final String animalCode, final Substance substance, final Double reductionFraction) {
    this.reductionFractions.computeIfAbsent(animalCode, (key) -> new HashMap<>()).put(substance, reductionFraction);
  }

  @Override
  public String toString() {
    return super.toString() + ", airScrubber=" + airScrubber + ", reductionFactors=" + reductionFractions + "]";
  }
}
