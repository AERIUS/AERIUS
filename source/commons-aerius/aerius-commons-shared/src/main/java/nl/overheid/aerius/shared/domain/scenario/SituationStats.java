/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario;

import java.io.Serializable;

/**
 * Object to store statics about a scenario situation.
 */
public class SituationStats implements Serializable {
  private static final long serialVersionUID = 4L;

  private int sources;
  private int buildings;
  private int calculationPoints;
  private int calculationResults;
  private int timeVaryingProfiles;
  private boolean archive;

  public int getSources() {
    return sources;
  }

  public void setSources(final int sources) {
    this.sources = sources;
  }

  public int getBuildings() {
    return buildings;
  }

  public void setBuildings(final int buildings) {
    this.buildings = buildings;
  }

  public int getCalculationPoints() {
    return calculationPoints;
  }

  public void setCalculationPoints(final int calculationPoints) {
    this.calculationPoints = calculationPoints;
  }

  public int getCalculationResults() {
    return calculationResults;
  }

  public void setCalculationResults(final int calculationResults) {
    this.calculationResults = calculationResults;
  }

  public int getTimeVaryingProfiles() {
    return timeVaryingProfiles;
  }

  public void setTimeVaryingProfiles(final int timeVaryingProfiles) {
    this.timeVaryingProfiles = timeVaryingProfiles;
  }

  public boolean isArchive() {
    return archive;
  }

  public void setArchive(final boolean archive) {
    this.archive = archive;
  }

}
