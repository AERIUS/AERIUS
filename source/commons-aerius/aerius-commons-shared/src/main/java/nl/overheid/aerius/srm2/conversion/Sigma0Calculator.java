/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrier;

/**
 * Calculation of vertical dispersion sigmaz,0 for SRM2 calculation
 * @see http://wetten.overheid.nl/BWBR0022817/Bijlage2/geldigheidsdatum_07-05-2014
 */
public final class Sigma0Calculator {
  public static final double NON_URBAN_ROAD_SIGMA0 = 2.5;
  public static final double FREEWAY_SIGMA0 = 3.0;
  public static final int MAX_ROAD_HEIGHT = 12; // meter
  public static final int MIN_ROAD_HEIGHT = -6; // meter

  public static final int MIN_BARRIER_HEIGHT = 1; // meter
  public static final int MAX_BARRIER_HEIGHT = 6; // meter
  public static final int MAX_BARRIER_DISTANCE = 50; // meter

  private Sigma0Calculator() {
    // util
  }

  /**
   * Limit the elevation height between -6 and 12 meters as specific:
   *
   * @see http://www.infomil.nl/publish/pages/74578/volledige_handleiding_rekentoo_-_nov_2014.pdf
   * @param elevationHeight elevation height
   * @return limited elevation height
   */
  public static int getElevationHeight(final int elevationHeight) {
    return elevationHeight < 0 ? Math.max(MIN_ROAD_HEIGHT, elevationHeight) : Math.min(MAX_ROAD_HEIGHT, elevationHeight);
  }

  /**
   * The sigma0 for SRM2 calculations.
   *
   * For height part the calculation is:
   * <pre>
   * height_use =  height * road_type_factor.
   * if height_use > 12 => 12
   * If height_use < -6 => -6
   * </pre>
   * @return The sigma0 for SRM2 calculations. Sigma0 is the starting value for vertical dispersion.
   *
   */
  static double getSigma0(final boolean isFreeway, final RoadElevation elevation, final int elevationHeight, final SRM2RoadSideBarrier barrierLeft,
      final SRM2RoadSideBarrier barrierRight) {
    double sigma0 = isFreeway ? FREEWAY_SIGMA0 : NON_URBAN_ROAD_SIGMA0;
    sigma0 += Math.abs(getElevationHeight(elevationHeight) * elevation.getHeightFactor());
    sigma0 += getBarrierCorrection(barrierLeft);
    sigma0 += getBarrierCorrection(barrierRight);

    return sigma0;
  }

  /**
   * @return The correction on sigma0 due to this barrier.
   */
  private static double getBarrierCorrection(final SRM2RoadSideBarrier barrier) {
    double correction = 0.0;

    if (useBarrierCorrection(barrier)) {
      correction = Math.min(barrier.getHeight(), MAX_BARRIER_HEIGHT) * barrier.getBarrierType().getHeightFactor();
    }
    return correction;
  }

  /**
   * @return True if barrier exceeds minimum height and is within the maximum distance of the road.
   */
  private static boolean useBarrierCorrection(final SRM2RoadSideBarrier barrier) {
    return barrier != null && barrier.getDistance() < MAX_BARRIER_DISTANCE && barrier.getHeight() >= MIN_BARRIER_HEIGHT;
  }
}
