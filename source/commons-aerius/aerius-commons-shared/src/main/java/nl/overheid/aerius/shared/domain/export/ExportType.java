/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.export;

/**
 * Type of data to export.
 */
public enum ExportType {
  /**
   * Output ADMS input files.
   */
  ADMS,
  /**
   * Perform a calculation for showing results in the user interface. No documents will be generated.
   */
  CALCULATION_UI,
  /**
   * Generic CSV file export.
   */
  CSV,
  /**
   * CSV export specific for CIMLK
   */
  CIMLK_CSV,
  /**
   * Output a GML file with only sources.
   */
  GML_SOURCES_ONLY,
  /**
   * Output a GML file with sources and available results.
   */
  GML_WITH_RESULTS,
  /**
   * Output a GML file with sources and available results, and separate GML file for used sectors.
   */
  GML_WITH_SECTORS_RESULTS,
  /**
   * Output OPS input files.
   */
  OPS,
  /**
   * Output a Permit Application Appendix PDF.
   */
  PDF_PAA,
  /**
   * Output a Procurement Policy PDF.
   */
  PDF_PROCUREMENT_POLICY,
  /**
   * Output a Single Scenario PDF.
   */
  PDF_SINGLE_SCENARIO,
  /**
   * Output Register project calculation in JSON format.
   */
  REGISTER_JSON_PROJECT_CALCULATION,
  /**
   * Output a Register Permit Demand Application Appendix PDF.
   */
  REGISTER_PDF_PAA_DEMAND;

  /**
   * @return Return true if export type is only generating source files and doesn't do any calculations.
   */
  public boolean isSourceExport() {
    return this == ADMS || this == OPS || this == GML_SOURCES_ONLY;
  }

  /**
   * @return Returns true if export type is doing a calculation that is not done in the UI.
   */
  public boolean isResultsExport() {
    return this == CSV || this == CIMLK_CSV || this == GML_WITH_RESULTS || this == GML_WITH_SECTORS_RESULTS || this == PDF_PAA;
  }

  /**
   * @return Returns true if the export type is expected to determine summaries.
   */
  public boolean isExportWithSummaries() {
    return this == CALCULATION_UI || this == PDF_PAA || this == REGISTER_PDF_PAA_DEMAND || this == REGISTER_JSON_PROJECT_CALCULATION
        || this == PDF_PROCUREMENT_POLICY || this == PDF_SINGLE_SCENARIO;
  }

  /**
   * @return Returns true if the export type is expected to generate a PDF.
   */
  public boolean isPdfExportType() {
    return this == PDF_PAA || this == REGISTER_PDF_PAA_DEMAND || this == PDF_PROCUREMENT_POLICY || this == PDF_SINGLE_SCENARIO;
  }
}
