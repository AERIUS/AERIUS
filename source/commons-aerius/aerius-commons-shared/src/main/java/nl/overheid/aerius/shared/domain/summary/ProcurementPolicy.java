/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.Locale;

import nl.overheid.aerius.shared.domain.Theme;

/**
 * The currently supported policies in Calculator.
 */
public enum ProcurementPolicy {

  /**
   * Lbv-plus regeling
   */
  WNB_LBV_PLUS(Theme.OWN2000, SummaryHexagonType.ABOVE_CL_HEXAGONS, OverlappingHexagonType.ALL_HEXAGONS),

  /**
   * Lbv regeling
   */
  WNB_LBV(Theme.OWN2000, SummaryHexagonType.EXCEEDING_HEXAGONS, OverlappingHexagonType.ALL_HEXAGONS);

  private final Theme theme;
  private final SummaryHexagonType summaryHexagonType;
  private final OverlappingHexagonType overlappingHexagonType;

  ProcurementPolicy(final Theme theme, final SummaryHexagonType summaryHexagonType, final OverlappingHexagonType overlappingHexagonType) {
    this.theme = theme;
    this.summaryHexagonType = summaryHexagonType;
    this.overlappingHexagonType = overlappingHexagonType;
  }

  public Theme getTheme() {
    return theme;
  }

  public SummaryHexagonType getSummaryHexagonType() {
    return summaryHexagonType;
  }

  public OverlappingHexagonType getOverlappingHexagonType() {
    return overlappingHexagonType;
  }

  public String toDatabaseString() {
    return name().toLowerCase(Locale.ROOT);
  }
}
