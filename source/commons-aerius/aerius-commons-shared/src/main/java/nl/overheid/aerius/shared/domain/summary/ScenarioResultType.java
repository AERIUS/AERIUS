/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;

public enum ScenarioResultType {
  /**
   * Alternative terms: Scenario contribution, Situatieresultaat
   */
  SITUATION_RESULT(0),

  /**
   * Alternative terms: Process contribution, Projectberekening
   */
  PROJECT_CALCULATION(1),

  /**
   * Alternative terms: Maximaal tijdelijk effect
   */
  MAX_TEMPORARY_EFFECT(2),

  /**
   * Alternative terms: Combination calculation, Combinatieberekening
   */
  IN_COMBINATION(3),

  /**
   * Alternative terms: Depositievracht
   */
  DEPOSITION_SUM(4),

  /**
   * Results retrieved from archive
   */
  ARCHIVE_CONTRIBUTION(5);

  private final int order;

  private ScenarioResultType(final int order) {
    this.order = order;
  }

  public int getOrder() {
    return order;
  }

  public static List<ScenarioResultType> getResultTypesForSituationType(final SituationType situationType,
      final CalculationJobType calculationJobType, final boolean useInCombinationArchive) {
    final List<ScenarioResultType> scenarioResultTypes = new ArrayList<>();

    if (calculationJobType == CalculationJobType.DEPOSITION_SUM
        && situationType == SituationType.REFERENCE) {
      scenarioResultTypes.add(ScenarioResultType.DEPOSITION_SUM);
    }

    if (situationType != SituationType.UNKNOWN) {
      scenarioResultTypes.add(ScenarioResultType.SITUATION_RESULT);
    }

    if (calculationJobType == CalculationJobType.PROCESS_CONTRIBUTION
        && situationType == SituationType.PROPOSED) {
      scenarioResultTypes.add(ScenarioResultType.PROJECT_CALCULATION);
    }

    if (calculationJobType == CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION
        && situationType == SituationType.PROPOSED) {
      scenarioResultTypes.add(ScenarioResultType.PROJECT_CALCULATION);
      scenarioResultTypes.add(ScenarioResultType.IN_COMBINATION);
      if (useInCombinationArchive) {
        scenarioResultTypes.add(ScenarioResultType.ARCHIVE_CONTRIBUTION);
      }
    }

    if (calculationJobType == CalculationJobType.MAX_TEMPORARY_EFFECT
        && situationType == SituationType.TEMPORARY) {
      scenarioResultTypes.add(ScenarioResultType.MAX_TEMPORARY_EFFECT);
    }

    return scenarioResultTypes;
  }

  /**
   * Get the default result type belonging to a jobType and situationType
   *
   * @param jobType calculation job type
   * @param situationType situation type
   * @return default result type
   */
  public static ScenarioResultType getDefault(final CalculationJobType jobType, final SituationType situationType) {
    if (jobType == CalculationJobType.SINGLE_SCENARIO) {
      return ScenarioResultType.SITUATION_RESULT;
    } else if (jobType == CalculationJobType.PROCESS_CONTRIBUTION && situationType == SituationType.PROPOSED) {
      return ScenarioResultType.PROJECT_CALCULATION;
    } else if (jobType == CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION && situationType == SituationType.PROPOSED) {
      return ScenarioResultType.IN_COMBINATION;
    } else if (jobType == CalculationJobType.MAX_TEMPORARY_EFFECT && situationType == SituationType.TEMPORARY) {
      return ScenarioResultType.MAX_TEMPORARY_EFFECT;
    } else if (jobType == CalculationJobType.DEPOSITION_SUM && situationType == SituationType.REFERENCE) {
      return ScenarioResultType.DEPOSITION_SUM;
    } else {
      return ScenarioResultType.SITUATION_RESULT;
    }
  }

  /**
   * Get the list of scenario result types that are possible for the specified calculation job type.
   */
  public static List<ScenarioResultType> getResultTypesForJobType(final CalculationJobType calculationJobType) {
    if (calculationJobType == null) {
      return Arrays.asList(ScenarioResultType.values());
    }
    final List<ScenarioResultType> scenarioResultTypes = new ArrayList<>();

    scenarioResultTypes.add(ScenarioResultType.SITUATION_RESULT);

    if (calculationJobType == CalculationJobType.DEPOSITION_SUM) {
      scenarioResultTypes.add(ScenarioResultType.DEPOSITION_SUM);
    }

    if (calculationJobType == CalculationJobType.PROCESS_CONTRIBUTION) {
      scenarioResultTypes.add(ScenarioResultType.PROJECT_CALCULATION);
    }

    if (calculationJobType == CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION) {
      scenarioResultTypes.add(ScenarioResultType.PROJECT_CALCULATION);
      scenarioResultTypes.add(ScenarioResultType.IN_COMBINATION);
      scenarioResultTypes.add(ScenarioResultType.ARCHIVE_CONTRIBUTION);
    }

    if (calculationJobType == CalculationJobType.MAX_TEMPORARY_EFFECT) {
      scenarioResultTypes.add(ScenarioResultType.MAX_TEMPORARY_EFFECT);
    }

    return scenarioResultTypes;
  }
}
