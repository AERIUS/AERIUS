/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared;

public final class RequestMappings {

  private static final String SLASH = "/";

  public static final long MAX_LIST_SIZE_SANITY = 1000;

  public static final String SYSTEM_INFO = "/system-info";
  public static final String SYSTEM_INFO_UPDATE = "/system-info-update";

  public static final String JOB_KEY_BARE = "jobKey";
  public static final String JOB_KEY = "{" + JOB_KEY_BARE + "}";
  public static final String SITUATION_CODE_BARE = "situationCode";
  public static final String SITUATION_CODE = "{" + SITUATION_CODE_BARE + "}";
  public static final String RESULT_TYPE_BARE = "resultType";
  public static final String RESULT_TYPE = "{" + RESULT_TYPE_BARE + "}";
  public static final String IMPORT_CODE_BARE = "importCode";
  public static final String IMPORT_CODE = "{" + IMPORT_CODE_BARE + "}";
  public static final String EXPORT_CODE_BARE = "exportCode";
  public static final String EXPORT_CODE = "{" + EXPORT_CODE_BARE + "}";
  public static final String FILE_CODE_BARE = "fileCode";
  public static final String FILE_CODE = "{" + FILE_CODE_BARE + "}";
  public static final String FILE_CODES_BARE = "fileCodes";
  public static final String FILE_CODES = "{" + FILE_CODES_BARE + "}";
  public static final String FILE = "file";

  /** CONNECT PARAMS **/
  private static final String CONNECT_API_VERSION = "v8";
  private static final String UI = "ui";
  private static final String IMPORT = "import";
  private static final String RESULTS = "results";
  private static final String CALCULATION = "calculation";
  private static final String CANCEL = "cancel";
  private static final String DELETE = "delete";
  private static final String INFO = "info";
  private static final String JOB_INFO = "jobInfo";
  private static final String SHIPPING = "shipping";
  private static final String SOURCE = "source";
  private static final String CALCULATION_POINTS = "calculationPoints";
  private static final String DETERMINE = "determine";
  private static final String SUGGEST_INLAND_SHIPPING_WATERWAY = "suggestwaterway";
  private static final String SUMMARY = "summary";
  private static final String DECISION_FRAMEWORK_RESULTS = "decisionFrameworkResults";
  private static final String REFRESH_EMISSIONS = "refreshEmissions";
  private static final String FILE_TYPE_VALIDATION_RESULT = "validationresult";
  private static final String FILE_TYPE_VALIDATION_REPORT = "validationreport";
  private static final String FILE_TYPE_JSON = "json";

  private static final String USER = "user";
  private static final String VALIDATE_API_KEY = "validateApiKey";

  /** CONNECT **/
  public static final String CONNECT_VERSION = SLASH + CONNECT_API_VERSION + SLASH;

  public static final String CONNECT_UI_IMPORT = CONNECT_VERSION + UI + SLASH + IMPORT;
  public static final String CONNECT_UI_IMPORT_RESULTS = CONNECT_UI_IMPORT + SLASH + RESULTS;

  private static final String CONNECT_UI_RETRIEVE_FILE = CONNECT_UI_IMPORT + SLASH + FILE_CODE + SLASH;
  public static final String CONNECT_UI_RETRIEVE_VALIDATION_RESULT = CONNECT_UI_RETRIEVE_FILE + FILE_TYPE_VALIDATION_RESULT;
  public static final String CONNECT_UI_RETRIEVE_VALIDATION_REPORT = CONNECT_UI_RETRIEVE_FILE + FILE_TYPE_VALIDATION_REPORT;
  public static final String CONNECT_UI_RETRIEVE_JSON = CONNECT_UI_RETRIEVE_FILE + FILE_TYPE_JSON;

  private static final String KEEP_FILE = "?keepFile=true";
  public static final String CONNECT_UI_RETRIEVE_JSON_KEEP_FILE = CONNECT_UI_RETRIEVE_FILE + FILE_TYPE_JSON + KEEP_FILE;

  public static final String CONNECT_UI_DELETE_FILE = CONNECT_UI_IMPORT + SLASH + FILE_CODE;

  public static final String CONNECT_UI_FILE = CONNECT_VERSION + UI + SLASH + FILE;

  public static final String CONNECT_UI_CALCULATE = CONNECT_VERSION + UI + SLASH + CALCULATION;
  public static final String CONNECT_UI_CALCULATE_INFO = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + JOB_KEY;

  public static final String CONNECT_UI_CALCULATE_DELETE = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + JOB_KEY;
  public static final String CONNECT_UI_CALCULATE_DELETE_WITH_POST = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + JOB_KEY + SLASH + DELETE;
  public static final String CONNECT_UI_CALCULATE_CANCEL = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + JOB_KEY + SLASH + CANCEL;

  public static final String CONNECT_UI_RECEPTOR_STATIC_INFO = CONNECT_VERSION + UI + SLASH + INFO;

  public static final String CONNECT_UI_RECEPTOR_JOB_INFO = CONNECT_VERSION + UI + SLASH + JOB_INFO;

  public static final String CONNECT_UI_CALCULATION_SUMMARY = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + JOB_KEY
      + SLASH + SITUATION_CODE + SLASH + RESULT_TYPE;

  public static final String CONNECT_UI_REFRESH_EMISSIONS = CONNECT_VERSION + UI + SLASH + SOURCE
      + SLASH + REFRESH_EMISSIONS;
  public static final String CONNECT_UI_SUGGEST_INLAND_SHIPPING_WATERWAY = CONNECT_VERSION + UI + SLASH + SHIPPING
      + SLASH + SUGGEST_INLAND_SHIPPING_WATERWAY;

  public static final String CONNECT_UI_CALCULATION_POINTS_DETERMINE = CONNECT_VERSION + UI + SLASH + CALCULATION_POINTS + SLASH + DETERMINE;

  public static final String CONNECT_RESULTS_SUMMARY = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + JOB_KEY
      + SLASH + SUMMARY;

  public static final String CONNECT_DECISION_FRAMEWORK_RESULTS = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + JOB_KEY
      + SLASH + DECISION_FRAMEWORK_RESULTS;

  public static final String CONNECT_HEXAGON_TYPE_PARAM = "summaryHexagonType";
  public static final String CONNECT_OVERLAPPING_HEXAGON_TYPE_PARAM = "summaryOverlappingHexagonType";
  public static final String CONNECT_PROCUREMENT_POLICY_PARAM = "procurementPolicy";

  public static final String CONNECT_EMISSION_RESULT_KEY_PARAM = "emissionResultKey";

  public static final String CONNECT_USER_VALIDATE = CONNECT_VERSION + USER + SLASH + VALIDATE_API_KEY;

  private RequestMappings() {}
}
