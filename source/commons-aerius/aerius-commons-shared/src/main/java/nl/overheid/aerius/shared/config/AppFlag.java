package nl.overheid.aerius.shared.config;

/**
 * Theme specific features can be disabled/enabled by setting flags.
 */
public enum AppFlag {
  /**
   * Enable ADMS export output
   */
  ENABLE_ADMS_EXPORT,
  /**
   * Enable cooky policy popup functionality
   */
  ENABLE_COOKIE_POLICY_POPUP,
  /**
   * Enable demo mode for fast calculation output
   */
  ENABLE_DEMO_CALCULATION,
  /**
   * Enable internal is used for the pdf and the endpoints to connect en geoserver
   */
  ENABLE_INTERNAL,
  /**
   * Enable OPS export output
   */
  ENABLE_OPS_EXPORT,
  /**
   * Enable Met site layer
   */
  ENABLE_MET_SITES_LAYER,
  /**
   * Show the map on the jobs page.
   */
  SHOW_MAP_ON_JOB_PAGE,
  /**
   * Enable hexagon indicators in info panel
   */
  SHOW_HEXAGON_INDICATORS,
  /**
   * Enable when the minimum height of a building is larger than zero.
   */
  HAS_BUILDING_MINIMUM_HEIGHT_LARGER_ZERO,
  /**
   * Enable calculation point characteristics.
   *
   * Of note: Part of the features related to this are (for now) NCA/ADMS-specific
   */
  HAS_CALCULATION_POINTS_CHARACTERISTICS,
  /**
   * Whether the theme has a project category setting for the calculation job
   */
  HAS_CALCULATION_PROJECT_CATEGORY,
  /**
   * Whether the theme has a permit area setting for the calculation job
   */
  HAS_CALCULATION_PERMIT_AREA,
  /**
   * Whether the theme has the development pressure search calculation job options
   */
  HAS_DEVELOPMENT_PRESSURE_SEARCH,
  /**
   * Whether the theme has an about menu item (with external content)
   */
  SHOW_ABOUT_MENU,
  /**
   * Whether the theme has a contact menu item (with external content)
   */
  SHOW_CONTACT_MENU,
  /**
   * If set any option with regards to abroad areas are to be shown.
   */
  SHOW_ABROAD_OPTIONS,
  /**
   * Show the directive in when displaying the assessment title.
   */
  SHOW_ASSESSMENT_AREA_DIRECTIVES,
  /**
   * Show a warning when entering a standard farm animal housing code.
   */
  SHOW_FARM_ANIMAL_HOUSING_STANDARD_CODE_WARNING,
  /**
   * Show a link to the privacy statement (in the export menu)
   */
  SHOW_PRIVACY_STATEMENT,
  /**
   * Show overlapping
   */
  SHOWS_OVERLAPPING_EFFECTS,
  /**
   * If set geometries are validated to be convex.
   */
  VALIDATE_CONVEX_GEOMETRIES,
  /**
   * If set, use/expect g/km/s units for custom road vehicle definition.
   * If not set, g/km is used instead.
   */
  USE_ROAD_CUSTOM_UNITS_G_KM_S,
  /**
   * Show report appendices
   */
  SHOW_REPORT_APPENDICES,
  /**
   * Show the zone of influence map layer.
   */
  ZONE_OF_INFLUENCE_MAP_LAYER,
  /**
   * Show the archive projects marker map layer.
   */
  ARCHIVE_PROJECTS_MAP_LAYER,
  /**
   * Show less metadata information about nature site and habitats in infomarker.
   */
  REDUCED_NATURE_METADATA_FIELDS
}
