/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import jsinterop.annotations.JsProperty;

import nl.aerius.geo.shared.LayerProps;
import nl.aerius.search.domain.SearchCapability;
import nl.aerius.search.domain.SearchRegion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.legend.ColorItem;
import nl.overheid.aerius.shared.domain.legend.ColorItemType;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.CalculationMarker;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.MainTab;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.EmissionCalculationMethod;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.sector.SectorPropertiesSet;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.source.road.RoadLayerStyle;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ResultTypeColorRangeTypes;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.building.BuildingLimits;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Data class with all application dynamic configuration.
 */
public class ApplicationConfiguration implements Serializable {
  private static final long serialVersionUID = 1L;

  private Settings settings = new Settings();
  private ReceptorGridSettings receptorGridSettings = null;
  private SectorCategories sectorCategories = null;
  private @JsProperty List<SimpleCategory> natureAreaDirectives = new ArrayList<>();
  private @JsProperty List<SimpleCategory> calculationPermitAreas = new ArrayList<>();
  private @JsProperty List<SimpleCategory> calculationProjectCategories = new ArrayList<>();
  private @JsProperty Map<String, Map<String, Integer>> defaultCalculationDistances = new HashMap<>();

  private Theme theme;

  // Specific layers
  private @JsProperty List<LayerProps> baseLayers = new ArrayList<>();

  // Specific layers
  private @JsProperty Map<String, LayerProps> layers = new LinkedHashMap<>();

  private @JsProperty Map<ColorItemType, List<ColorItem>> colorItems = new HashMap<>();
  private @JsProperty Map<ColorRangeType, List<ColorRange>> colorRanges = new HashMap<>();

  private @JsProperty ResultTypeColorRangeTypes resultTypeColorRangeTypes;

  /**
   * Maps sector id to the way it should be viewed/edited in the user interface.
   */
  private @JsProperty Map<Integer, EmissionCalculationMethod> emisionSourceUIViewType;

  private @JsProperty EmissionResultValueDisplaySettings emissionResultValueDisplaySettings;

  private @JsProperty Map<String, String> meteoYears;
  private @JsProperty List<MetSite> metSites;
  private int calculationYearDefault;
  private @JsProperty List<Integer> calculationYears;

  private CharacteristicsType characteristicsType;
  private @JsProperty List<EmissionResultKey> emissionResultKeys;
  private @JsProperty List<Substance> substances;
  private @JsProperty List<Substance> emissionSubstancesRoad;
  private @JsProperty List<Substance> importSubstances;
  private @JsProperty List<RoadLayerStyle> roadLayerStyle;
  private @JsProperty List<CalculationMarker> calculationMarkers;
  private @JsProperty double roadEmissionDisplayConversionFactor;
  private CalculationMethod defaultCalculationMethod;
  private @JsProperty List<CalculationMethod> calculationMethods;
  private @JsProperty List<AssessmentCategory> assessmentCategories;
  private CalculationJobType defaultCalculationJobType;
  private @JsProperty List<CalculationJobType> calculationJobTypes;
  private @JsProperty Map<CalculationJobType, JobType> defaultExportJobTypes = new HashMap<>();
  private @JsProperty SectorPropertiesSet sectorPropertiesSet;
  private @JsProperty List<ResultView> resultViews;
  private int maxNumberOfSituations;
  private double assessmentPointHeightDefault;
  private @JsProperty List<SituationType> availableSituationTypes;
  private @JsProperty SituationType defaultSituationType;
  private @JsProperty List<SectorGroup> sectorGroups;
  private SearchRegion searchRegion;
  private @JsProperty List<SearchCapability> searchCapabilities;
  private @JsProperty List<String> convertibleGeometrySystems;
  private @JsProperty Map<String, String> projections;
  private @JsProperty BuildingLimits buildingLimits;
  private @JsProperty List<EmissionResultKey> supportedSummaryResultTypes;
  private @JsProperty List<ResultStatisticType> supportedSummaryStatisticTypes;
  private @JsProperty List<SummaryHexagonType> supportedSummaryHexagonTypes;
  private @JsProperty Set<InputTypeViewMode> inputTypeViewModes;
  private @JsProperty List<FarmEmissionFactorType> customFarmLodgingEmissionFactorTypes;
  private @JsProperty List<FarmEmissionFactorType> customFarmAnimalHousingEmissionFactorTypes;
  private @JsProperty List<FarmEmissionFactorType> customManureStorageEmissionFactorTypes;
  private @JsProperty Set<AppFlag> appFlags;
  private double calculationDistanceDefault;
  private @JsProperty List<String> acceptedImportFileTypes;
  private boolean displayLoginSettings;
  private @JsProperty List<MainTab> availableTabs;
  private @JsProperty List<CalculationJobType> availableExportReportOptions;
  private @JsProperty Map<Integer, SectorIcon> sectorIcons;
  private @JsProperty Map<String, ColorRangeType> roadTrafficVolumeColors;
  private String manualBaseUrl;
  private String registerBaseUrl;

  public void setTheme(Theme theme) {
    this.theme = theme;
  }

  /**
   * @return the settings
   */
  public Settings getSettings() {
    return settings;
  }

  /**
   * private to ensure no formatter will add final
   */
  @SuppressWarnings("unused")
  private void setSettings(final Settings settings) {
    this.settings = settings;
  }

  /**
   * @return the receptorGridSettings
   */
  public ReceptorGridSettings getReceptorGridSettings() {
    return receptorGridSettings;
  }

  /**
   * @param receptorGridSettings the receptorGridSettings to set
   */
  public void setReceptorGridSettings(final ReceptorGridSettings receptorGridSettings) {
    this.receptorGridSettings = receptorGridSettings;
  }

  /**
   * @return the sectorCategories
   */
  public SectorCategories getSectorCategories() {
    return sectorCategories;
  }

  /**
   * @param sectorCategories the sectorCategories to set
   */
  public void setSectorCategories(final SectorCategories sectorCategories) {
    this.sectorCategories = sectorCategories;
  }

  public List<SimpleCategory> getNatureAreaDirectives() {
    return natureAreaDirectives;
  }

  public void setNatureAreaDirectives(final List<SimpleCategory> natureAreaDirectives) {
    this.natureAreaDirectives = natureAreaDirectives;
  }

  public List<SimpleCategory> getCalculationPermitAreas() {
    return calculationPermitAreas;
  }

  public void setCalculationPermitAreas(final List<SimpleCategory> calculationPermitAreas) {
    this.calculationPermitAreas = calculationPermitAreas;
  }

  public List<SimpleCategory> getCalculationProjectCategories() {
    return calculationProjectCategories;
  }

  public void setCalculationProjectCategories(final List<SimpleCategory> calculationProjectCategories) {
    this.calculationProjectCategories = calculationProjectCategories;
  }

  public Map<String, Map<String, Integer>> getDefaultCalculationDistances() {
    return defaultCalculationDistances;
  }

  public void setDefaultCalculationDistances(final Map<String, Map<String, Integer>> defaultCalculationDistances) {
    this.defaultCalculationDistances = defaultCalculationDistances;
  }

  /**
   *
   * @return the list of allowed sectors
   */
  public List<SectorGroup> getSectorGroups() {
    return sectorGroups;
  }

  public void setSectorGroups(final List<SectorGroup> sectorGroups) {
    this.sectorGroups = sectorGroups;
  }

  /**
   * @return the theme
   */
  public Theme getTheme() {
    return theme;
  }

  public CharacteristicsType getCharacteristicsType() {
    return characteristicsType;
  }

  public void setCharacteristicsType(final CharacteristicsType characteristicsType) {
    this.characteristicsType = characteristicsType;
  }

  /**
   * @return the baseLayers
   */
  public List<LayerProps> getBaseLayers() {
    return baseLayers;
  }

  /**
   * @param baseLayers the baseLayers to set
   */
  public void setBaseLayers(final List<LayerProps> baseLayers) {
    this.baseLayers = baseLayers;
  }

  /**
   * @return the layers
   */
  public List<LayerProps> getLayers() {
    return new ArrayList<>(layers.values());
  }

  public List<FarmEmissionFactorType> getCustomFarmAnimalHousingEmissionFactorTypes() {
    return customFarmAnimalHousingEmissionFactorTypes;
  }

  public void setCustomFarmAnimalHousingEmissionFactorTypes(
      final List<FarmEmissionFactorType> customFarmAnimalHousingEmissionFactorTypes) {
    this.customFarmAnimalHousingEmissionFactorTypes = customFarmAnimalHousingEmissionFactorTypes;
  }

  @Deprecated
  public List<FarmEmissionFactorType> getCustomFarmLodgingEmissionFactorTypes() {
    return customFarmLodgingEmissionFactorTypes;
  }

  @Deprecated
  public void setCustomFarmLodgingEmissionFactorTypes(
      final List<FarmEmissionFactorType> customFarmLodgingEmissionFactorTypes) {
    this.customFarmLodgingEmissionFactorTypes = customFarmLodgingEmissionFactorTypes;
  }

  public List<FarmEmissionFactorType> getCustomManureStorageEmissionFactorTypes() {
    return customManureStorageEmissionFactorTypes;
  }

  public void setCustomManureStorageEmissionFactorTypes(
      final List<FarmEmissionFactorType> customManureStorageEmissionFactorTypes) {
    this.customManureStorageEmissionFactorTypes = customManureStorageEmissionFactorTypes;
  }

  /**
   * private setter to ensure the layers field can never be final
   */
  @SuppressWarnings("unused")
  private void setLayers(final Map<String, LayerProps> layers) {
    this.layers = layers;
  }

  public void setLayer(final String id, final LayerProps layerProps) {
    layers.put(id, layerProps);
  }

  public Map<ColorItemType, List<ColorItem>> getColorItems() {
    return colorItems;
  }

  public void setColorItems(final Map<ColorItemType, List<ColorItem>> colorItems) {
    this.colorItems = colorItems;
  }

  public List<ColorItem> getColorItems(final ColorItemType colorItemType) {
    return this.colorItems.getOrDefault(colorItemType, Collections.emptyList());
  }

  public Map<ColorRangeType, List<ColorRange>> getColorRanges() {
    return colorRanges;
  }

  public void setColorRanges(final Map<ColorRangeType, List<ColorRange>> colorRanges) {
    this.colorRanges = colorRanges;
  }

  public List<ColorRange> getColorRange(final ColorRangeType colorRangeType) {
    return this.colorRanges.getOrDefault(colorRangeType, Collections.emptyList());
  }

  public ResultTypeColorRangeTypes getResultTypeColorRangeTypes() {
    return resultTypeColorRangeTypes;
  }

  public void setResultTypeColorRangeTypes(final ResultTypeColorRangeTypes resultTypeColorRangeTypes) {
    this.resultTypeColorRangeTypes = resultTypeColorRangeTypes;
  }

  public EmissionResultValueDisplaySettings getEmissionResultValueDisplaySettings() {
    return emissionResultValueDisplaySettings;
  }

  public void setEmissionResultValueDisplaySettings(final EmissionResultValueDisplaySettings emissionResultValueDisplaySettings) {
    this.emissionResultValueDisplaySettings = emissionResultValueDisplaySettings;
  }

  /**
   * @return the emisionSourceUIViewType
   */
  public Map<Integer, EmissionCalculationMethod> getEmisionSourceUIViewType() {
    return emisionSourceUIViewType;
  }

  /**
   * @param emisionSourceUIViewType the emisionSourceUIViewType to set
   */
  public void setEmisionSourceUIViewType(final Map<Integer, EmissionCalculationMethod> emisionSourceUIViewType) {
    this.emisionSourceUIViewType = emisionSourceUIViewType;
  }

  /**
   * @return the calculationYearDefault
   */
  public int getCalculationYearDefault() {
    return calculationYearDefault;
  }

  /**
   * @return the calculationYears
   */
  public List<Integer> getCalculationYears() {
    return calculationYears;
  }

  /**
   * @param calculationYearDefault the calculationYearDefault to set
   */
  public void setCalculationYearDefault(final int calculationYearDefault) {
    this.calculationYearDefault = calculationYearDefault;
  }

  /**
   * @param calculationYears the calculationYears to set
   */
  public void setCalculationYears(final List<Integer> calculationYears) {
    this.calculationYears = calculationYears;
  }

  /**
   * @return the emission result keys that are available in this configuration
   */
  public List<EmissionResultKey> getEmissionResultKeys() {
    return emissionResultKeys;
  }

  public void setEmissionResultKeys(final List<EmissionResultKey> emissionResultKeys) {
    this.emissionResultKeys = emissionResultKeys;
  }

  /**
   * @return the emission substances that can be used in this configuration
   */
  public List<Substance> getSubstances() {
    return substances;
  }

  public void setSubstances(final List<Substance> substances) {
    this.substances = substances;
  }

  /**
   * @return supported emission substances for road sources.
   */
  public List<Substance> getEmissionSubstancesRoad() {
    return emissionSubstancesRoad;
  }

  public void setEmissionSubstancesRoad(final List<Substance> emissionSubstancesRoad) {
    this.emissionSubstancesRoad = emissionSubstancesRoad;
  }

  /**
   * @return the substances that can be imported in this configuration
   */
  public List<Substance> getImportSubstances() {
    return importSubstances;
  }

  public void setImportSubstances(final List<Substance> importSubstances) {
    this.importSubstances = importSubstances;
  }

  /**
   * @return the roadstyles that can be used in the road source map
   */
  public List<RoadLayerStyle> getRoadLayerStyle() {
    return roadLayerStyle;
  }

  public void setRoadLayerStyle(final List<RoadLayerStyle> roadLayerStyle) {
    this.roadLayerStyle = roadLayerStyle;
  }

  /**
   * @return Factor to convert kg/y of road emissions to the unit to display to the user
   */
  public double getRoadEmissionDisplayConversionFactor() {
    return roadEmissionDisplayConversionFactor;
  }

  public void setRoadEmissionDisplayConversionFactor(final double roadEmissionDisplayConversionFactor) {
    this.roadEmissionDisplayConversionFactor = roadEmissionDisplayConversionFactor;
  }

  /**
   * @return the calculation method that can be used in this configuration
   */
  public List<CalculationMethod> getCalculationMethods() {
    return calculationMethods;
  }

  public List<AssessmentCategory> getAssessmentCategories() {
    return assessmentCategories;
  }

  public void setCalculationMethods(final List<CalculationMethod> calculationMethods) {
    this.calculationMethods = calculationMethods;
  }

  public void setAssessmentCategories(final List<AssessmentCategory> assessmentCategories) {
    this.assessmentCategories = assessmentCategories;
  }

  /**
   * @Return the default calculation method in this configuration
   */
  public CalculationMethod getDefaultCalculationMethod() {
    return defaultCalculationMethod;
  }

  public void setDefaultCalculationMethod(final CalculationMethod calculationMethod) {
    this.defaultCalculationMethod = calculationMethod;
  }

  public List<CalculationJobType> getAvailableExportReportOptions() {
    return availableExportReportOptions;
  }

  public void setAvailableExportReportOptions(final List<CalculationJobType> availableExportReportOptions) {
    this.availableExportReportOptions = availableExportReportOptions;
  }

  /**
   * @Return the SectorPropertiesSet in this configuration
   */
  public SectorPropertiesSet getSectorPropertiesSet() {
    return sectorPropertiesSet;
  }

  public void setSectorPropertiesSet(final SectorPropertiesSet sectorPropertiesSet) {
    this.sectorPropertiesSet = sectorPropertiesSet;
  }

  public int getMaxNumberOfSituations() {
    return maxNumberOfSituations;
  }

  public void setMaxNumberOfSituations(final int maxNumberOfSituations) {
    this.maxNumberOfSituations = maxNumberOfSituations;
  }

  public double getAssessmentPointHeightDefault() {
    return assessmentPointHeightDefault;
  }

  public void setAssessmentPointHeightDefault(final double assessmentPointHeightDefault) {
    this.assessmentPointHeightDefault = assessmentPointHeightDefault;
  }

  public List<ResultView> getResultViews() {
    return resultViews;
  }

  public void setResultViews(final List<ResultView> resultViews) {
    this.resultViews = resultViews;
  }

  public List<SituationType> getAvailableSituationTypes() {
    return availableSituationTypes;
  }

  public void setAvailableSituationTypes(final List<SituationType> availableSituationTypes) {
    this.availableSituationTypes = availableSituationTypes;
  }

  public SituationType getDefaultSituationType() {
    return defaultSituationType;
  }

  public void setDefaultSituationType(final SituationType defaultSituationType) {
    this.defaultSituationType = defaultSituationType;
  }

  public SearchRegion getSearchRegion() {
    return searchRegion;
  }

  public void setSearchRegion(final SearchRegion searchRegion) {
    this.searchRegion = searchRegion;
  }

  public List<SearchCapability> getSearchCapabilities() {
    return searchCapabilities;
  }

  public void setSearchCapabilities(final List<SearchCapability> searchCapabilities) {
    this.searchCapabilities = searchCapabilities;
  }

  public List<String> getConvertibleGeometrySystems() {
    return convertibleGeometrySystems;
  }

  public void setConvertibleGeometrySystems(final List<String> convertibleGeometrySystems) {
    this.convertibleGeometrySystems = convertibleGeometrySystems;
  }

  public Map<String, String> getProjections() {
    return projections;
  }

  public void setProjections(final HashMap<String, String> projections) {
    this.projections = projections;
  }

  public BuildingLimits getBuildingLimits() {
    return buildingLimits;
  }

  public void setBuildingLimits(final BuildingLimits buildingLimits) {
    this.buildingLimits = buildingLimits;
  }

  public List<MetSite> getMetSites() {
    return metSites;
  }

  public void setMetSites(final List<MetSite> metSites) {
    this.metSites = metSites;
  }

  public void setCalculationDistanceDefault(final double calculationDistanceDefault) {
    this.calculationDistanceDefault = calculationDistanceDefault;
  }

  public double getCalculationDistanceDefault() {
    return calculationDistanceDefault;
  }

  public List<ResultStatisticType> getSupportedSummaryStatisticTypes() {
    return supportedSummaryStatisticTypes;
  }

  public void setSupportedSummaryStatisticTypes(final List<ResultStatisticType> supportedSummaryStatisticTypes) {
    this.supportedSummaryStatisticTypes = supportedSummaryStatisticTypes;
  }

  public List<SummaryHexagonType> getSupportedSummaryHexagonTypes() {
    return supportedSummaryHexagonTypes;
  }

  public void setSupportedSummaryHexagonTypes(final List<SummaryHexagonType> supportedSummaryHexagonTypes) {
    this.supportedSummaryHexagonTypes = supportedSummaryHexagonTypes;
  }

  public List<EmissionResultKey> getSupportedSummaryResultTypes() {
    return supportedSummaryResultTypes;
  }

  public void setSupportedSummaryResultTypes(final List<EmissionResultKey> supportedSummaryResultTypes) {
    this.supportedSummaryResultTypes = supportedSummaryResultTypes;
  }

  public Set<InputTypeViewMode> getInputTypeViewModes() {
    return this.inputTypeViewModes;
  }

  public void setInputTypeViewModes(final Set<InputTypeViewMode> inputTypeViewModes) {
    this.inputTypeViewModes = inputTypeViewModes;
  }

  public boolean isAppFlagEnabled(final AppFlag flag) {
    return appFlags.contains(flag);
  }

  public void setAppFlags(final Set<AppFlag> appFlags) {
    this.appFlags = appFlags;
  }

  public List<String> getAcceptedImportFileTypes() {
    return acceptedImportFileTypes;
  }

  public void setAcceptedImportFileTypes(final List<String> acceptedImportFileTypes) {
    this.acceptedImportFileTypes = acceptedImportFileTypes;
  }

  public boolean isDisplayLoginSettings() {
    return displayLoginSettings;
  }

  public void setDisplayLoginSettings(final boolean dsiplayLoginSettings) {
    this.displayLoginSettings = dsiplayLoginSettings;
  }

  public void setAvailableTabs(final List<MainTab> availableTabs) {
    this.availableTabs = availableTabs;
  }

  public List<MainTab> getAvailableResultTabs() {
    return availableTabs;
  }

  public List<CalculationJobType> getCalculationJobTypes() {
    return calculationJobTypes;
  }

  public void setCalculationJobTypes(final List<CalculationJobType> calculationJobTypes) {
    this.calculationJobTypes = calculationJobTypes;
  }

  public boolean isAllowed(final CalculationJobType calculationJobType) {
    return calculationJobTypes == null || calculationJobTypes.contains(calculationJobType);
  }

  public Map<CalculationJobType, JobType> getDefaultExportJobTypes() {
    return defaultExportJobTypes;
  }

  public void setDefaultExportJobTypes(final Map<CalculationJobType, JobType> defaultExportJobTypes) {
    this.defaultExportJobTypes = defaultExportJobTypes;
  }

  public JobType getDefaultExportTypeForJobType(final CalculationJobType jobType) {
    return defaultExportJobTypes.getOrDefault(jobType, JobType.CALCULATION);
  }

  public CalculationJobType getDefaultCalculationJobType() {
    return defaultCalculationJobType;
  }

  public void setDefaultCalculationJobType(final CalculationJobType defaultCalculationJobType) {
    this.defaultCalculationJobType = defaultCalculationJobType;
  }

  public void setSectorIcons(final Map<Integer, SectorIcon> sectorIcons) {
    this.sectorIcons = sectorIcons;
  }

  public Map<Integer, SectorIcon> getSectorIcons() {
    return sectorIcons;
  }

  public Map<String, List<ColorRange>> getRoadTrafficVolumeColorRanges() {
    return getRoadTrafficVolumeColors().entrySet().stream().collect(
        Collectors.toMap(Map.Entry::getKey, entry -> getColorRange(entry.getValue())));
  }

  public void setRoadTrafficVolumeColors(final Map<String, ColorRangeType> roadTrafficVolumeColors) {
    this.roadTrafficVolumeColors = roadTrafficVolumeColors;
  }

  public Map<String, ColorRangeType> getRoadTrafficVolumeColors() {
    return roadTrafficVolumeColors;
  }

  public String getManualBaseUrl() {
    return manualBaseUrl;
  }

  public boolean hasManual() {
    return manualBaseUrl != null;
  }

  public void setManualBaseUrl(final String manualBaseUrl) {
    this.manualBaseUrl = manualBaseUrl;
  }

  public String getRegisterBaseUrl() {
    return registerBaseUrl;
  }

  public boolean hasRegister() {
    return registerBaseUrl != null;
  }

  public void setRegisterBaseUrl(final String registerBaseUrl) {
    this.registerBaseUrl = registerBaseUrl;
  }
}
