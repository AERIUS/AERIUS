/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

/**
 * Type of engine used to perform the calculations.
 */
public enum CalculationEngine implements EngineDataKey {
  /**
   * CERC ADMS
   * Calculates deposition and concentrations using the CERC ADMS dispersion model software.
   */
  ADMS,
  /**
   * AERIUS SRM2 implementation.
   * Calculates deposition and concentrations according to the SRM2 model for road traffic.
   */
  ASRM2,
  /**
   * RIVM OPS (Operationele Prioritaire Stoffen).
   * Calculates deposition and concentrations for several substances.
   */
  OPS;

  /**
   * Returns the enum given the enum as string. If null or an illegal type is passed OPS is returned.
   * @param value String representation of enum
   * @return enum or OPS in case of null or invalid value
   */
  public static CalculationEngine safeValueOf(final String value) {
    try {
      return value == null ? OPS : valueOf(value.toUpperCase());
    } catch (final IllegalArgumentException e) {
      return OPS;
    }
  }

  @Override
  public String key() {
    return name();
  }

}

