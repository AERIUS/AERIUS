/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.backgrounddata;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.calculation.MetDatasetType;

/**
 * Data class for Met site dataset, representing an actual Met site file.
 */
public class MetSiteDataset implements Serializable {

  private static final long serialVersionUID = 2L;

  private MetDatasetType datasetType;
  private String year;
  private String remarks;

  public MetDatasetType getDatasetType() {
    return datasetType;
  }

  public void setDatasetType(final MetDatasetType datasetType) {
    this.datasetType = datasetType;
  }

  public String getYear() {
    return year;
  }

  public void setYear(final String year) {
    this.year = year;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(final String remarks) {
    this.remarks = remarks;
  }

}
