/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import java.io.Serializable;
import java.util.Arrays;

/**
 *
 */
public class OPSTerrainProperties implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * z0 - Average roughness(m).
   */
  private final Double averageRoughness;

  /**
   * landuse - The dominant land use (enumerated value, 1-9).
   */
  private final LandUse landUse;

  /**
   * Distribution of land uses (percentages).
   */
  private final int[] landUses;

  /*
   * Default constructor for serialization purposes
   */
  OPSTerrainProperties() {
    this(null, null, null);
  }

  public OPSTerrainProperties(final Double averageRoughness, final LandUse landuse) {
    this(averageRoughness, landuse, null);
  }

  public OPSTerrainProperties(final Double averageRoughness, final LandUse landuse, final int[] landUses) {
    this.averageRoughness = averageRoughness;
    this.landUse = landuse;
    if (landUses != null && landUses.length != LandUse.OPS_LAND_USE_CLASSES) {
      throw new IllegalArgumentException("The number of land uses must be exactly " + LandUse.OPS_LAND_USE_CLASSES
          + " but was " + landUses.length);
    }
    this.landUses = landUses;
  }

  public Double getAverageRoughness() {
    return averageRoughness;
  }

  public LandUse getLandUse() {
    return landUse;
  }

  public int[] getLandUses() {
    return landUses;
  }

  public boolean hasNoTerrainData() {
    return averageRoughness == null || landUse == null;
  }

  public boolean hasTerrainData() {
    return !hasNoTerrainData();
  }

  public OPSTerrainProperties copy() {
    return new OPSTerrainProperties(averageRoughness, landUse, landUses);
  }

  @Override
  public String toString() {
    return "OPSTerrainProperties [averageRoughness=" + averageRoughness + ", landUse=" + landUse + ", landUses="
        + Arrays.toString(landUses) + "]";
  }
}
