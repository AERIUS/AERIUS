/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jsinterop.annotations.JsProperty;

/**
 * Holds lists with domain objects that are relevant for farm animal housing.
 */
public class FarmAnimalHousingCategories implements Serializable {

  private static final long serialVersionUID = 2L;

  /**
   * List of all farm animal types.
   */
  private @JsProperty List<FarmAnimalCategory> animalCategories;

  /**
   * List of all farm animal housing types (systems).
   */
  private @JsProperty List<FarmAnimalHousingCategory> animalHousingCategories;

  /**
   * Map of the basic housing category codes per housing code.
   */
  private @JsProperty Map<String, String> animalBasicHousingCategoryCodes;

  /**
   * List of all farm additional animal housing systems.
   */
  private @JsProperty List<FarmAdditionalHousingSystemCategory> additionalHousingSystemCategories;

  /**
   * Map of the allowed additional systems per housing system.
   */
  private @JsProperty Map<String, Set<String>> housingAllowedAdditionalSystems;

  public List<FarmAnimalCategory> getAnimalCategories() {
    return animalCategories;
  }

  public void setAnimalCategories(final List<FarmAnimalCategory> animalCategories) {
    this.animalCategories = animalCategories;
  }

  public List<FarmAnimalHousingCategory> getAnimalHousingCategories() {
    return animalHousingCategories;
  }

  public void setAnimalHousingCategories(final List<FarmAnimalHousingCategory> animalHousingCategories) {
    this.animalHousingCategories = animalHousingCategories;
  }

  public Map<String, String> getAnimalBasicHousingCategoryCodes() {
    return animalBasicHousingCategoryCodes;
  }

  public void setAnimalBasicHousingCategoryCodes(final Map<String, String> animalBasicHousingCategoryCodes) {
    this.animalBasicHousingCategoryCodes = animalBasicHousingCategoryCodes;
  }

  public List<FarmAdditionalHousingSystemCategory> getAdditionalHousingSystemCategories() {
    return additionalHousingSystemCategories;
  }

  public void setAdditionalHousingSystemCategories(final List<FarmAdditionalHousingSystemCategory> additionalHousingSystemCategories) {
    this.additionalHousingSystemCategories = additionalHousingSystemCategories;
  }

  public Map<String, Set<String>> getHousingAllowedAdditionalSystems() {
    return housingAllowedAdditionalSystems;
  }

  public void setHousingAllowedAdditionalSystems(final Map<String, Set<String>> housingAllowedAdditionalSystems) {
    this.housingAllowedAdditionalSystems = housingAllowedAdditionalSystems;
  }

  /**
   * @param code The farm animal category code to determine the FarmAnimalCategory object for.
   * @return The right FarmAnimalCategory object, or null if no match could be made based on the code.
   */
  public FarmAnimalCategory determineAnimalCategoryByCode(final String code) {
    return AbstractCategory.determineByCode(animalCategories, code);
  }

  /**
   * @param code The farm animal housing category code to determine the FarmAnimalHousingCategory object for.
   * @return The right FarmAnimalHousingCategory object, or null if no match could be made based on the code.
   */
  public FarmAnimalHousingCategory determineHousingCategoryByCode(final String code) {
    return AbstractCategory.determineByCode(animalHousingCategories, code);
  }

  /**
   * @param code The additional farm animal housing category code to determine the FarmAdditionalHousingSystemCategory object for.
   * @return The right FarmAdditionalHousingSystemCategory object, or null if no match could be made based on the code.
   */
  public FarmAdditionalHousingSystemCategory determineAdditionalHousingCategoryByCode(final String code) {
    return AbstractCategory.determineByCode(additionalHousingSystemCategories, code);
  }
}
