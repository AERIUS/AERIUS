/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.constants;

/**
 * Global constants stored in the database and shared among the client and
 * server application. Constants NOT used in the client should be placed
 * in {@link nl.overheid.aerius.enums.ConstantsEnum}.
 *
 * <p>These can be accessed via the context's
 * {@link nl.overheid.aerius.shared.domain.context.Context#getSetting(SharedConstantsEnum)} method.</p>
 *
 * <p>The values of the entries are stored in the database tables <code>public.constants</code>
 * and <code>system.constants</code>.</p>
 */
public enum SharedConstantsEnum {
  /**
   * ID for jira collector
   */
  COLLECT_COLLECTOR_ID,
  /**
   * show feedback link for jira
   */
  COLLECT_FEEDBACK,
  /**
   * When defined the user must specify the given key as parameter to ?feedback in the url to show feedback link for jira
   */
  COLLECT_FEEDBACK_KEY,
  /**
   * Specific url for feedback.
   */
  COLLECT_FEEDBACK_URL,
  /**
   * Whether to enable Google Tag manager.
   */
  GOOGLE_TAG_MANAGER_ENABLE,
  /**
   * The container ID to use with Google Tag Manager.
   */
  GOOGLE_TAG_MANAGER_CONTAINER_ID,
  /**
   * Manual url.
   */
  MANUAL_URL,
  /**
   * Quick start url
   */
  QUICK_START_URL,
  /**
   * Next steps url
   */
  NEXT_STEPS_URL,
  /**
   * Optional secondary next steps url
   */
  NEXT_STEPS_SECONDARY_URL,
  /**
   * URL to details about methods and data used in the current release.
   */
  RELEASE_DETAILS_URL,
  /**
   * Database version as defined in system
   */
  DATABASE_VERSION,
  /**
   * AERIUS version as defined in the manifest file
   */
  AERIUS_VERSION,
  /**
   * Polling time for system info message.
   * Used for the polling time for the request.
   */
  SYSTEM_INFO_POLLING_TIME,
  /**
   * Calculation boundary as rendered on the map
   * Contains the geometry as WKT string
   */
  CALCULATOR_BOUNDARY,
  /**
   * Internal connect url, used during PDF generation to reach connect using fully qualified url.
   */
  CONNECT_INTERNAL_URL,
  /**
   * Internal geoserver url, used during PDF generation to reach geoserver using fully qualified url.
   */
  GEOSERVER_INTERNAL_URL,
  /**
   * Search endpoint url
   */
  SEARCH_ENDPOINT_URL,
  /**
   * Denotes on which side of the road cars drive.
   * Not used in the calculation but only in the visualization.
   */
  DRIVING_SIDE,
  /**
   * The default netting factor
   */
  DEFAULT_NETTING_FACTOR,
  /**
   * The default calculation point placement radius
   */
  DEFAULT_CALCULATION_POINTS_PLACEMENT_RADIUS,
  /**
   * Base url for the Register application.
   *
   * Note: Should NOT contain a trailing /
   */
  REGISTER_BASE_URL,
  /**
   * Base url for the external manual/help site.
   *
   * Note: Should NOT contain a trailing /
   */
  MANUAL_BASE_URL,
  /**
   * Version for the external manual/help site.
   */
  MANUAL_VERSION,
  /**
   * Language for the external manual/help site, defaults to DEFAULT_LOCALE, and can be :language to be overridden with user locale.
   */
  MANUAL_LANGUAGE,
  /**
   * Maximum length of a line allowed in the calculator in meters.
   */
  CALCULATOR_LIMITS_MAX_LINE_LENGTH,
  /**
   * Maximum surface allowed in the calculator in hectares.
   */
  CALCULATOR_LIMITS_MAX_POLYGON_SURFACE,

  /**
   * Whether the easter module is enabled.
   */
  EASTER_MODULE_ENABLE;
}
