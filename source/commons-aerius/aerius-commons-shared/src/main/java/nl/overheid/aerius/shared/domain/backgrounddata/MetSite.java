/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.shared.domain.backgrounddata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.HasId;

/**
 * Data class for Met site.
 */
public class MetSite implements Serializable, HasId {

  private static final long serialVersionUID = 1L;

  private int id;
  private String name;
  private double x;
  private double y;
  private @JsProperty List<MetSiteDataset> datasets = new ArrayList<>();

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public double getX() {
    return x;
  }

  public void setX(final double x) {
    this.x = x;
  }

  public double getY() {
    return y;
  }

  public void setY(final double y) {
    this.y = y;
  }

  public List<MetSiteDataset> getDatasets() {
    return datasets;
  }

  public void setDatasets(final List<MetSiteDataset> datasets) {
    this.datasets = datasets;
  }

}
