/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

/**
 * Utility class for working with colors.
 */
public final class ColorUtil {
  private static final int MIN_HEX_LENGTH = 3;
  private static final int MAX_HEX_LENGTH = 6;
  private static final String WHITE_RGB = "ffffff";
  private static final String YELLOW_RGB = "ffff00";
  private static final String BLACK_RGB = "000000";
  private static final String ORANGE_RGB = "FFA500";

  /**
   * Util class to get web colors from string or sector.
   */
  private ColorUtil() {}

  /**
   * Returns the color string prefixed with '#'. The string must be the
   * hexadecimal notation. e.g FFDDAA.
   *
   * @param color color to prefix
   * @return color prefix with '#'
   */
  public static String webColor(final String color) {
    return color == null || color.isEmpty() || color.charAt(0) == '#' || color.length() != MIN_HEX_LENGTH && color.length() != MAX_HEX_LENGTH
        ? color : "#" + color;
  }

  public static String white() {
    return webColor(WHITE_RGB);
  }

  public static String yellow() {
    return webColor(YELLOW_RGB);
  }

  public static String orange() {
    return webColor(ORANGE_RGB);
  }

  public static String black() {
    return webColor(BLACK_RGB);
  }

}
