/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

public enum PdfProductType {
  /** Calculator PDF */
  OWN2000_CALCULATOR(true, "calculator-logo.svg", "AERIUS_projectberekening"),
  /** Calculator Edge Effect Appendix */
  OWN2000_EDGE_EFFECT_APPENDIX(false, "calculator-logo.svg", "AERIUS_randeffect_projectberekening"),
  /** Calculator Extra Assessment Appendix */
  OWN2000_EXTRA_ASSESSMENT_APPENDIX(false, "calculator-logo.svg", "AERIUS_extra_beoordeling"),
  /** Register Permit PDF */
  OWN2000_PERMIT(false, "register-logo.svg", "AERIUS_projectberekening"),
  /** LBV and LBV+ PDF */
  OWN2000_LBV_POLICY(true, "check-logo.svg", "AERIUS_depositievracht"),
  /** Single Scenario PDF */
  OWN2000_SINGLE_SCENARIO(false, "calculator-logo.svg", "AERIUS_berekening"),
  /** Calculator UK PDF */
  NCA_CALCULATOR(true, "calculator-logo-uk.svg", "APAS_annex");

  private boolean containGml;
  private final String logoResource;
  private final String filePrefix;

  PdfProductType(final boolean containGml, final String logoResource, final String filePrefix) {
    this.containGml = containGml;
    this.logoResource = logoResource;
    this.filePrefix = filePrefix;
  }

  public String getLogoResource() {
    return logoResource;
  }

  public String getFilePrefix() {
    return filePrefix;
  }

  public boolean containsGmls() {
    return containGml;
  }

  public static PdfProductType safeValueOf(final String value) {
    try {
      return value == null ? null : valueOf(value.toUpperCase());
    } catch (final IllegalArgumentException e) {
      return null;
    }
  }
}
