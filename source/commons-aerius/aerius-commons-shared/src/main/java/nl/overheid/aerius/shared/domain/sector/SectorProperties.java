/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.CalculationEngine;

/**
 * Class for properties of a sector which are used by the application to give
 * a representation of the sector and guide the application. For representation
 * a color and an image property can be set. Where image is not the actual image,
 * but an enum that indicates an image is available. The application
 * implementation should make the mapping between the enum and the actual image.
 * EmissionCalculationMethod is an indicator for the application to decide which type
 * of emission editor to use.
 */
public class SectorProperties implements Serializable {

  private static final long serialVersionUID = 1L;

  private EmissionCalculationMethod method;
  private CalculationEngine calculationEngine;

  public static final SectorProperties SECTOR_UNDEFINED = new SectorProperties(EmissionCalculationMethod.GENERIC, CalculationEngine.OPS);

  // Needed for GWT.
  public SectorProperties() {
  }

  public SectorProperties(final String method, final String calculationEngine) {
    this(EmissionCalculationMethod.safeValueOf(method), CalculationEngine.safeValueOf(calculationEngine));
  }

  public SectorProperties(final EmissionCalculationMethod method, final CalculationEngine calculationEngine) {
    this.method = method;
    this.calculationEngine = calculationEngine;
  }

  public CalculationEngine getCalculationEngine() {
    return calculationEngine;
  }

  public EmissionCalculationMethod getMethod() {
    return method;
  }

  public void setCalculationEngine(final CalculationEngine calculationEngine) {
    this.calculationEngine = calculationEngine;
  }

  public void setMethod(final EmissionCalculationMethod method) {
    this.method = method;
  }

  @Override
  public String toString() {
    return "SectorProperties [method=" + method + ", calculationEngine=" + calculationEngine + "]";
  }
}
