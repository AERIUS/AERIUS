/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector;

import java.util.HashMap;
import java.util.Map;

/**
 * Boilerplate for sector icons/colors
 */
public final class SectorIconUtil {
  private SectorIconUtil() {}

  public static Map<Integer, SectorIcon> rbl() {
    return own2000();
  }

  public static Map<Integer, SectorIcon> nca() {
    // Use OwN2000 as the base / fallback
    final Map<Integer, SectorIcon> icons = own2000();

    icons.put(1050, new SectorIcon("WASTE_MANAGEMENT", "4c2c92"));
    icons.put(1100, new SectorIcon("INDUSTRY_FOOD_INDUSTRY", "4c2c92"));
    icons.put(1300, new SectorIcon("INDUSTRY_CHEMICAL_INDUSTRY", "4c2c92"));
    icons.put(1400, new SectorIcon("INDUSTRY_BUILDING_MATERIALS", "4c2c92"));
    icons.put(1500, new SectorIcon("INDUSTRY_BASE_MATERIAL", "4c2c92"));
    icons.put(1700, new SectorIcon("INDUSTRY_METAL_INDUSTRY", "4c2c92"));
    icons.put(1800, new SectorIcon("INDUSTRY", "4c2c92"));

    icons.put(1900, new SectorIcon("ENERGY_LARGE_COMBUSTION_PLANT", "9bb7d9"));
    icons.put(1910, new SectorIcon("ENERGY_MEDIUM_COMBUSTION_PLANT", "9bb7d9"));
    icons.put(1920, new SectorIcon("ENERGY_GENERATOR_SPECIFIED", "9bb7d9"));
    icons.put(2100, new SectorIcon("ENERGY", "9bb7d9"));

    icons.put(3111, new SectorIcon("ROAD_FREEWAY", "d4351c"));
    icons.put(3112, new SectorIcon("ROAD_NON_URBAN", "d4351c"));
    icons.put(3113, new SectorIcon("ROAD_URBAN", "d4351c"));
    icons.put(3100, new SectorIcon("ROAD_OTHER", "d4351c"));
    icons.put(3210, new SectorIcon("ROAD_FARM", "d4351c"));
    icons.put(3220, new SectorIcon("ROAD_CONSTRUCTION_INDUSTRY", "d4351c"));

    icons.put(4130, new SectorIcon("FARMLAND_PASTURE", "00703c"));
    icons.put(4140, new SectorIcon("FARMLAND_MANURE", "00703c"));
    icons.put(4110, new SectorIcon("FARM_LODGE", "00703c"));
    icons.put(4120, new SectorIcon("FARM_MANURE_STORAGE", "00703c"));
    icons.put(4150, new SectorIcon("FARMLAND", "00703c"));
    icons.put(4200, new SectorIcon("FARMLAND_FERTILIZER", "00703c"));
    icons.put(4320, new SectorIcon("GREENHOUSE", "00703c"));
    icons.put(4400, new SectorIcon("FARMLAND_ORGANIC_PROCESSES", "00703c"));
    icons.put(4600, new SectorIcon("AGRICULTURE", "00703c"));

    return icons;
  }

  public static Map<Integer, SectorIcon> own2000() {
    final Map<Integer, SectorIcon> map = new HashMap<>();

    map.put(1050, new SectorIcon("WASTE_MANAGEMENT", "6B15CB"));
    map.put(1100, new SectorIcon("INDUSTRY_FOOD_INDUSTRY", "6B15CB"));
    map.put(1300, new SectorIcon("INDUSTRY_CHEMICAL_INDUSTRY", "6B15CB"));
    map.put(1400, new SectorIcon("INDUSTRY_BUILDING_MATERIALS", "6B15CB"));
    map.put(1500, new SectorIcon("INDUSTRY_BASE_MATERIAL", "6B15CB"));
    map.put(1700, new SectorIcon("INDUSTRY_METAL_INDUSTRY", "6B15CB"));
    map.put(1800, new SectorIcon("INDUSTRY", "6B15CB"));

    map.put(1900, new SectorIcon("ENERGY_LARGE_COMBUSTION_PLANT", "0F789B"));
    map.put(1910, new SectorIcon("ENERGY_MEDIUM_COMBUSTION_PLANT", "0F789B"));
    map.put(1920, new SectorIcon("ENERGY_GENERATOR_SPECIFIED", "0F789B"));
    map.put(2100, new SectorIcon("ENERGY", "0F789B"));

    map.put(3111, new SectorIcon("ROAD_FREEWAY", "CB181D"));
    map.put(3112, new SectorIcon("ROAD_NON_URBAN", "CB181D"));
    map.put(3113, new SectorIcon("ROAD_URBAN", "CB181D"));
    map.put(3150, new SectorIcon("COLD_START_PARKING_GARAGE", "CB181D"));
    map.put(3160, new SectorIcon("COLD_START_OTHER", "CB181D"));
    map.put(3100, new SectorIcon("ROAD_OTHER", "CB181D"));
    map.put(3210, new SectorIcon("ROAD_FARM", "CC79A7"));
    map.put(3220, new SectorIcon("ROAD_CONSTRUCTION_INDUSTRY", "CC79A7"));
    map.put(3230, new SectorIcon("MINING", "CC79A7"));

    map.put(3530, new SectorIcon("ROAD_CONSUMER", "CC79A7"));

    map.put(3640, new SectorIcon("AVIATION_AERODROME", "993300"));
    map.put(3610, new SectorIcon("AVIATION_TAKE_OFF", "993300"));
    map.put(3630, new SectorIcon("AVIATION_TAXI", "993300"));
    map.put(3620, new SectorIcon("AVIATION_TOUCH_DOWN", "993300"));
    map.put(3710, new SectorIcon("RAIL_EMPLACEMENT", "AAA406"));
    map.put(3720, new SectorIcon("RAIL_TRANSPORT", "AAA406"));

    map.put(4130, new SectorIcon("FARMLAND_PASTURE", "427F08"));
    map.put(4140, new SectorIcon("FARMLAND_MANURE", "427F08"));
    map.put(4110, new SectorIcon("FARM_LODGE", "427F08"));
    map.put(4120, new SectorIcon("FARM_MANURE_STORAGE", "427F08"));
    map.put(4150, new SectorIcon("FARMLAND", "427F08"));
    map.put(4200, new SectorIcon("FARMLAND_FERTILIZER", "427F08"));
    map.put(4320, new SectorIcon("GREENHOUSE", "427F08"));
    map.put(4400, new SectorIcon("FARMLAND_ORGANIC_PROCESSES", "427F08"));
    map.put(4600, new SectorIcon("AGRICULTURE", "427F08"));

    map.put(7510, new SectorIcon("SHIPPING_DOCK", "084594"));
    map.put(7520, new SectorIcon("SHIPPING_MARITIME_MOORING", "084594"));
    map.put(7530, new SectorIcon("SHIPPING_MARITIME_NCP", "084594"));
    map.put(7610, new SectorIcon("SHIPPING_INLAND_DOCK", "084594"));
    map.put(7620, new SectorIcon("SHIPPING_INLAND", "084594"));

    map.put(8200, new SectorIcon("CONSUMERS", "F47A0F"));
    map.put(8210, new SectorIcon("RECREATION", "F47A0F"));
    map.put(8640, new SectorIcon("OFFICES_SHOPS", "F47A0F"));

    map.put(9999, new SectorIcon("OTHER", "084594"));

    return map;
  }
}
