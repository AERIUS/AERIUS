/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.Objects;

/**
 * Controls the dropdown 'Display' or 'Weergave' in the results UI
 */
public class ResultDisplaySet {

  private final SummaryHexagonType summaryHexagonType;
  private final OverlappingHexagonType overlappingHexagonType;
  private final ProcurementPolicy procurementPolicy;

  public ResultDisplaySet(final SummaryHexagonType summaryHexagonType, final OverlappingHexagonType overlappingHexagonType, final ProcurementPolicy procurementPolicy) {
    this.summaryHexagonType = summaryHexagonType;
    this.overlappingHexagonType = overlappingHexagonType;
    this.procurementPolicy = procurementPolicy;
  }

  public SummaryHexagonType getSummaryHexagonType() {
    return summaryHexagonType;
  }

  public OverlappingHexagonType getOverlappingHexagonType() {
    return overlappingHexagonType;
  }

  public ProcurementPolicy getPolicy() {
    return procurementPolicy;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final ResultDisplaySet that = (ResultDisplaySet) o;
    return summaryHexagonType == that.summaryHexagonType && overlappingHexagonType == that.overlappingHexagonType && procurementPolicy
        == that.procurementPolicy;
  }

  @Override
  public int hashCode() {
    return Objects.hash(summaryHexagonType, overlappingHexagonType, procurementPolicy);
  }
}
