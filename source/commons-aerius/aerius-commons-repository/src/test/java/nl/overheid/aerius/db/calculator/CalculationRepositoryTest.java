/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.jupiter.api.Test;
import org.postgresql.util.PSQLException;

import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.ReceptorResult;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveProject;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Needs a proper database to test against, testing DB operations for calculations.
 */
class CalculationRepositoryTest extends CalculationRepositoryTestBase {

  private static final Substance TEST_SUBSTANCE_2 = Substance.NH3;
  private static final int YEAR = 2020;

  @Test
  void testInsertCalculation() throws SQLException {
    final Calculation calculation = new Calculation();
    calculation.setYear(YEAR);
    final Calculation createdCalculation = CalculationRepository.insertCalculation(getConnection(), calculation, null, null);
    assertNotEquals(0, createdCalculation.getCalculationId(), "Calculation ID");
  }

  @Test
  void testGetCalculation() throws SQLException {
    Calculation fromDB = null;
    fromDB = CalculationRepository.getCalculation(getConnection(), -1);
    assertNull(fromDB, "Calculation null if unknown ID");
    fromDB = CalculationRepository.getCalculation(getConnection(), calculation.getCalculationId());
    assertNotNull(fromDB, "Calculation");
    assertEquals(calculation.getCalculationId(), fromDB.getCalculationId(), "Calculation ID");
    assertEquals(calculation.getState(), fromDB.getState(), "Calculation state");
  }

  @Test
  void testRemoveCalculationResults() throws SQLException {
    CalculationRepository.removeCalculation(getConnection(), calculation.getCalculationId());
    final Calculation delCalculation = CalculationRepository.getCalculation(getConnection(), calculation.getCalculationId());
    assertNull(delCalculation, "Calculation should not be in the database anymore for id:" + calculation.getCalculationId());
  }

  private static CalculationSetOptions getOptions() {
    final CalculationSetOptions options = new CalculationSetOptions();
    options.getSubstances().add(Substance.NH3);
    options.getSubstances().add(Substance.NOX);
    options.getSubstances().add(Substance.NO2);
    options.getEmissionResultKeys().add(EmissionResultKey.NH3_DEPOSITION);
    options.getEmissionResultKeys().add(EmissionResultKey.NOX_DEPOSITION);
    options.setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
    return options;
  }

  private static List<EmissionSourceFeature> createSources() {
    final List<EmissionSourceFeature> esl = new ArrayList<>();
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final EmissionSource es = new GenericEmissionSource();
    feature.setProperties(es);
    esl.add(feature);
    return esl;
  }

  @Test
  void testUpdateCalculationState() throws SQLException {
    Boolean updated = CalculationRepository.updateCalculationState(getConnection(), calculation.getCalculationId(), CalculationState.RUNNING);
    assertNotNull(updated, "Updated");
    assertTrue(updated, "Updated");
    calculation = CalculationRepository.getCalculation(getConnection(), calculation.getCalculationId());
    assertEquals(CalculationState.RUNNING, calculation.getState(), "Status");
    updated = CalculationRepository.updateCalculationState(getConnection(), calculation.getCalculationId(), CalculationState.CANCELLED);
    assertTrue(updated, "Updated");
    calculation = CalculationRepository.getCalculation(getConnection(), calculation.getCalculationId());
    assertEquals(CalculationState.CANCELLED, calculation.getState(), "Status");
    updated = CalculationRepository.updateCalculationState(getConnection(), calculation.getCalculationId(), CalculationState.COMPLETED);
    assertTrue(updated, "Updated");
    calculation = CalculationRepository.getCalculation(getConnection(), calculation.getCalculationId());
    assertEquals(CalculationState.COMPLETED, calculation.getState(), "Status");
    updated = CalculationRepository.updateCalculationState(getConnection(), calculation.getCalculationId(), CalculationState.INITIALIZED);
    assertTrue(updated, "Updated");
    calculation = CalculationRepository.getCalculation(getConnection(), calculation.getCalculationId());
    assertEquals(CalculationState.INITIALIZED, calculation.getState(), "Status");
    final boolean unknownUpdated = CalculationRepository.updateCalculationState(getConnection(), -1, CalculationState.RUNNING);
    assertFalse(unknownUpdated, "Update for non-existing calculation ID");
  }

  @Test
  void testInsertCalculationResultsUnsafe() throws SQLException {
    final List<AeriusResultPoint> calculationResult1 = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION,
        EmissionResultKey.NH3_DEPOSITION);
    final Connection connection = getConnection();
    final int inserted1 = CalculationRepository.insertCalculationResultsUnsafe(connection, calculation.getCalculationId(),
        calculationResult1);
    // 5 receptors * 2 result types = 10 results.
    // However, since NH3_DEPOSITION is included, NOXNH3_DEPOSITION will be included as well, so 5 * 3
    assertEquals(15, inserted1, "Number of emission results inserted");

    //inserting a second time works, but only with different substances.
    final List<AeriusResultPoint> calculationResult2 = getExampleOPSOutputData(EmissionResultKey.PM10_CONCENTRATION,
        EmissionResultKey.PM10_EXCEEDANCE_DAYS);
    final int inserted2 = CalculationRepository.insertCalculationResultsUnsafe(connection, calculation.getCalculationId(),
        calculationResult2);
    // 5 receptors * 2 result types = 10 results. However, num excess days won't be persisted. Hence 5 results.
    assertEquals(5, inserted2, "Number of emission results inserted");

    //try to insert the first again, and it results in an exception.
    try {
      CalculationRepository.insertCalculationResultsUnsafe(connection, calculation.getCalculationId(),
          calculationResult1);
      fail("expected a SQL exception at this point");
    } catch (final SQLException e) {
      assertEquals("23505", e.getSQLState(), "SQL state");
    }
  }

  @Test
  void testInsertArchiveContribution() throws SQLException {
    final List<AeriusResultPoint> calculationResult1 = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION,
        EmissionResultKey.NH3_DEPOSITION);
    final Connection connection = getConnection();
    CalculationRepository.insertArchiveContribution(connection, calculation.getCalculationId(),
        calculationResult1);

    Map<Integer, EmissionResultKey> emissionResultKeys =
        CalculationRepository.getCalculationResultIdSetsEmissionResultKey(connection, calculation.getCalculationId());
    // 2 result types inserted
    // However, since NH3_DEPOSITION is included, NOXNH3_DEPOSITION will be included as well, so 3
    assertEquals(3, emissionResultKeys.size(), "Number of result types");
    for (final Entry<Integer, EmissionResultKey> entry : emissionResultKeys.entrySet()) {
      final List<ReceptorResult> receptorResults = CalculationRepository.getArchiveContributionReceptorsFromSet(connection, entry.getKey());
      assertEquals(5, receptorResults.size(), "Number of receptors for ERK " + entry.getValue());
    }

    // Inserting again should work, but should not actually insert anything
    CalculationRepository.insertArchiveContribution(connection, calculation.getCalculationId(), calculationResult1);
    emissionResultKeys = CalculationRepository.getCalculationResultIdSetsEmissionResultKey(connection, calculation.getCalculationId());
    assertEquals(3, emissionResultKeys.size(), "Number of result types");
    for (final Entry<Integer, EmissionResultKey> entry : emissionResultKeys.entrySet()) {
      final List<ReceptorResult> receptorResults = CalculationRepository.getArchiveContributionReceptorsFromSet(connection, entry.getKey());
      assertEquals(5, receptorResults.size(), "Number of receptors for ERK " + entry.getValue());
    }

    //inserting with different substances does work.
    final List<AeriusResultPoint> calculationResult2 = getExampleOPSOutputData(EmissionResultKey.PM10_CONCENTRATION,
        EmissionResultKey.PM10_EXCEEDANCE_DAYS);
    CalculationRepository.insertArchiveContribution(connection, calculation.getCalculationId(), calculationResult2);

    emissionResultKeys = CalculationRepository.getCalculationResultIdSetsEmissionResultKey(connection, calculation.getCalculationId());
    // Now 1 extra ERK is persisted. num excess days won't be persisted.
    assertEquals(4, emissionResultKeys.size(), "Number of result types");
    for (final Entry<Integer, EmissionResultKey> entry : emissionResultKeys.entrySet()) {
      final List<ReceptorResult> receptorResults = CalculationRepository.getArchiveContributionReceptorsFromSet(connection, entry.getKey());
      assertEquals(5, receptorResults.size(), "Number of receptors for ERK " + entry.getValue());
    }
  }

  @Test
  void testGetCalculationResultSets() throws SQLException {
    insertCalculationResults();
    final Collection<EmissionResultKey> set = CalculationRepository.getCalculationResultSets(getConnection(), calculation.getCalculationId());
    assertEquals(4, set.size(), "Should have 4 emission result types in database");
  }

  @Test
  void testInsertCalculationPoints() throws SQLException {
    final int calculationPointSetId = insertCalculationPoints();
    // the ID returned is a serial, so can't guess what number it'll be besides not being 0.
    assertNotEquals(0, calculationPointSetId, "Number of emission results inserted");

    // Check if creating a calculation with that set ID works.
    final Calculation createdCalculation = CalculationRepository.insertCalculation(getConnection(), calculation, calculationPointSetId, null);
    assertNotEquals(0, createdCalculation.getCalculationId(), "Calculation ID");
  }

  @Test
  void testInsertCalculationPointResults() throws SQLException {
    final List<CalculationPointFeature> calculationPointList = getExampleCalculationPointOutputData();
    final List<AeriusResultPoint> results = toAeriusResultPoints(TEST_SUBSTANCE_2, calculationPointList);
    final int inserted = CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(),
        results);
    // 6 results, 5 result types for one receptor, 3 for the other.
    assertEquals(8, inserted, "Number of emission results inserted");
  }

  @Test
  void testInsertCalculationPointResultsWithoutResults() throws SQLException {
    final List<CalculationPointFeature> calculationPointList = getExampleCalculationPointOutputData();
    final List<AeriusResultPoint> results = toAeriusResultPoints(TEST_SUBSTANCE_2, calculationPointList);
    results.forEach(x -> x.clearResults());
    final int inserted = CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(),
        results);
    // 0 results, just testing if this situation doesn't trigger an error.
    assertEquals(0, inserted, "Number of emission results inserted");
  }

  @Test
  void testInsertCalculationResultsBatch() throws SQLException {
    final int firstBatchSize = 10000;
    final int secondBatchSize = 500;

    // 10km is about 10000 entries.
    final List<AeriusResultPoint> results1 = createResults(firstBatchSize);
    final int inserted1 = CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(), results1);
    assertEquals(firstBatchSize * 3, inserted1, "Number of inserted emission results"); // Times 2 because 2 emission result types.

    final List<AeriusResultPoint> results2 = createResults(secondBatchSize);
    // This second batch should return 0 inserted as they are all already in the database.
    final int inserted2 = CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(), results2);
    assertEquals(firstBatchSize * 3, inserted1 + inserted2, "Number of inserted emission results");
  }

  @Test
  void testInsertCalculationSubPoints() throws SQLException {
    final int calculationSubPointSetId = insertCalculationSubPoints();
    // the ID returned is a serial, so can't guess what number it'll be besides not being 0.
    assertNotEquals(0, calculationSubPointSetId, "Number of emission results inserted");

    // Check if creating a calculation with that set ID works.
    final Calculation createdCalculation = CalculationRepository.insertCalculation(getConnection(), calculation, null, calculationSubPointSetId);
    assertNotEquals(0, createdCalculation.getCalculationId(), "Calculation ID");
  }

  @Test
  void testInsertCalculationSubPointsSafe() throws SQLException {
    final int calculationSubPointSetId = CalculationRepository.insertSubPointSet(getConnection());
    final List<IsSubPoint> subPoints = getExampleSubPointsData();
    // Check if calling the safe function repeatedly works.
    CalculationRepository.insertCalculationSubPointsForSetSafe(getConnection(), calculationSubPointSetId, subPoints);
    CalculationRepository.insertCalculationSubPointsForSetSafe(getConnection(), calculationSubPointSetId, subPoints);
    CalculationRepository.insertCalculationSubPointsForSetSafe(getConnection(), calculationSubPointSetId, subPoints);

    // To check that the safe function isn't useless, try the normal one, this should throw an exception.
    final PSQLException exception = assertThrows(PSQLException.class,
        () -> CalculationRepository.insertCalculationSubPointsForSet(getConnection(), calculationSubPointSetId, subPoints),
        "Inserting normally (not safe) is expected to throw if subpoints are present.");
    assertEquals("23505", exception.getSQLState(), "Error message expected");
  }

  @Test
  void testInsertCalculationSubPointResults() throws SQLException {
    final List<IsSubPoint> calculationPointList = getExampleSubPointsData();
    final List<AeriusResultPoint> results = toAeriusResultSubPoints(TEST_SUBSTANCE_2, calculationPointList);
    final int inserted = CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(),
        results);
    // 45 results, 5 result types (NOXNH3 gets added as a bonus) times 9 subpoints..
    assertEquals(45, inserted, "Number of emission results inserted");
  }

  @Test
  void testInsertOverlappingHexagonsAndAddingEdgeInformation() throws SQLException {
    final List<AeriusPoint> points = List.of(
        new AeriusPoint(RECEPTOR_POINT_ID_1, AeriusPointType.RECEPTOR),
        new AeriusPoint(RECEPTOR_POINT_ID_2, AeriusPointType.RECEPTOR),
        new AeriusPoint(2, AeriusPointType.POINT));
    final int inserted = CalculationRepository.insertOverlappingHexagons(getConnection(), calculation.getCalculationId(),
        points);
    assertEquals(2, inserted, "Number of overlapping hexagons inserted");

    final List<CalculationPointFeature> resultPoints = List.of(
        createReceptorFeature(RECEPTOR_POINT_ID_1),
        createReceptorFeature(RECEPTOR_POINT_ID_2),
        createReceptorFeature(RECEPTOR_POINT_ID_3));

    // Sanity check
    for (final CalculationPointFeature feature : resultPoints) {
      assertTrue(feature.getProperties() instanceof ReceptorPoint, "All points should be a receptor point still");
      final ReceptorPoint receptorPoint = (ReceptorPoint) feature.getProperties();
      assertNull(receptorPoint.getEdgeEffect(), "Sanity check that edge effect hasn't been set elsewhere");
    }

    CalculationRepository.addEdgeEffectInformation(getConnection(), calculation.getCalculationId(), resultPoints);

    for (final CalculationPointFeature feature : resultPoints) {
      assertTrue(feature.getProperties() instanceof ReceptorPoint, "All points should be a receptor point still");
      final ReceptorPoint receptorPoint = (ReceptorPoint) feature.getProperties();
      assertNotNull(receptorPoint.getEdgeEffect(), "Edge effect should have been set");
      assertEquals(RECEPTOR_POINT_ID_3 == receptorPoint.getReceptorId(), receptorPoint.getEdgeEffect(),
          "Edge effect should be correct (true when not inserted as overlapping) for receptor " + receptorPoint.getReceptorId());
    }
  }

  @Test
  void testCalculationResultByCalculationResultSetId() throws SQLException {
    final int batchSize = 2;
    final int resultSets = 3;

    // just use a couple receptors that happen to be consecutive that are considered relevant-hexagons
    final List<AeriusResultPoint> results1 = createResults(batchSize, 4774524);
    CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(), results1);
    final List<Integer> calculationResultSets = CalculationRepository.getCalculationResultIdSets(getConnection(), calculation.getCalculationId());
    final Map<Integer, EmissionResultKey> calculationResultSetsEmissionResultKey =
        CalculationRepository.getCalculationResultIdSetsEmissionResultKey(getConnection(), calculation.getCalculationId());

    assertEquals(resultSets, calculationResultSets.size(), "Number of emission result sets");
    assertEquals(resultSets, calculationResultSetsEmissionResultKey.size(), "Number of emission result sets");

    final int[] calculationIds = {calculation.getCalculationId()};
    for (final Entry<Integer, EmissionResultKey> resultKey : calculationResultSetsEmissionResultKey.entrySet()) {
      final List<ReceptorResult> results = CalculationRepository.getCalculationResultsFromSet(getConnection(), calculationIds, resultKey.getKey(),
          SummaryHexagonType.RELEVANT_HEXAGONS);
      assertEquals(batchSize, results.size(), "Number of inserted emission results");
    }

  }

  private static ArrayList<AeriusResultPoint> createResults(final int size) {
    return createResults(size, 1);
  }

  private static ArrayList<AeriusResultPoint> createResults(final int size, final int startingFrom) {
    final ArrayList<AeriusResultPoint> results = new ArrayList<>();
    for (int i = startingFrom; i < startingFrom + size; i++) {
      final AeriusResultPoint rp = new AeriusResultPoint(new AeriusPoint());
      rp.setId(i);
      rp.setEmissionResult(EmissionResultKey.NH3_CONCENTRATION, RECEPTOR_1_CONCENTRATION + i);
      rp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, RECEPTOR_1_DEPOSITION + i);
      results.add(rp);
    }
    return results;
  }

  @Test
  void testInsertCalculationResultsWithoutOptions() throws SQLException {
    final List<AeriusResultPoint> results = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION,
        EmissionResultKey.NH3_DEPOSITION);

    assertThrows(IllegalArgumentException.class, () -> CalculationRepository.insertCalculationResults(getConnection(), -1, results),
        "Should throw an SQL exception because you can't insert a calculation result for a calculating without calculation options.");
  }

  @Test
  void testInsertDuplicatePartialCalculationResults() throws SQLException {
    insertCalculationResults();
    final List<AeriusResultPoint> partialCalculationResult = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION,
        EmissionResultKey.NH3_DEPOSITION);
    final AeriusResultPoint rp = new AeriusResultPoint(new AeriusPoint());
    rp.setId(99);
    rp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 160.0);
    partialCalculationResult.add(rp);
    // Shouldn't insert the previously inserted results again.
    final int inserted = CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(),
        partialCalculationResult);
    assertEquals(2, inserted, "Number of emission results inserted");
  }

  @Test
  void testGetCalculatedPoints() throws SQLException {
    final int calculationPointSetId = insertCalculationPoints();
    removeCalculationResults();
    createCalculation(calculationPointSetId);
    insertCalculationResults();
    final List<CalculationPointFeature> calculationPointList = getExampleCalculationPointOutputData();
    final List<AeriusResultPoint> results = toAeriusResultPoints(TEST_SUBSTANCE_2, calculationPointList);
    CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(), results);
    final List<CalculationPointFeature> calculatedPoints =
        CalculationRepository.getCalculatedPoints(getConnection(), calculation.getCalculationId());
    int calculationPoints = 0;
    int receptorPoints = 0;
    for (final CalculationPointFeature feature : calculatedPoints) {
      final CalculationPoint calculatedPoint = feature.getProperties();
      assertNotEquals(0, calculatedPoint.getId(), "ID should be set");
      assertNotEquals(0.0, feature.getGeometry().getX(), 1E-8, "X coord should be set");
      assertNotEquals(0.0, feature.getGeometry().getY(), 1E-8, "Y coord should be set");
      if (calculatedPoint instanceof CustomCalculationPoint) {
        calculationPoints++;
        assertNotNull(calculatedPoint.getLabel(), "Label shouldn't be null");
      } else if (calculatedPoint instanceof ReceptorPoint) {
        receptorPoints++;
      } else {
        fail("Did not expect point of type " + calculatedPoint);
      }
    }
    assertEquals(5, receptorPoints, "Nr of receptors");
    assertEquals(2, calculationPoints, "Nr of calculation points");
  }

  @Test
  void testInsertCalculationsWithoutPoints() throws SQLException {
    final List<CalculationPointFeature> calculationPoints = new ArrayList<>();
    final ScenarioCalculations cs = getExampleScenarioCalculations(calculationPoints);

    CalculationRepository.insertCalculations(getConnection(), cs, true, false);
    assertNull(cs.getCalculationPointSetTracker().getCalculationPointSetId(), "Without calculation points, no point set ID");
    assertPersistedCalculation(cs);
  }

  @Test
  void testInsertCalculationsWithPoints() throws SQLException {
    final List<CalculationPointFeature> calculationPoints = new ArrayList<>();
    calculationPoints.add(createPoint(1, 1, 1));
    calculationPoints.add(createPoint(2, 30, 40));
    final ScenarioCalculations cs = getExampleScenarioCalculations(calculationPoints);

    CalculationRepository.insertCalculations(getConnection(), cs, true, false);
    assertNotNull(cs.getCalculationPointSetTracker().getCalculationPointSetId(),
        "With calculation list filled with points, there should be a calculation point set ID");
    assertPersistedCalculation(cs);
  }

  @Test
  void testArchiveMetaData() throws SQLException, IOException, AeriusException {
    final ArchiveMetaData metaData = new ArchiveMetaData();
    final OffsetDateTime retrievalDateTime = OffsetDateTime.of(2030, 10, 25, 9, 43, 50, 560, ZoneOffset.of("Z"));
    metaData.setRetrievalDateTime(retrievalDateTime);
    final ArchiveProject project1 = new ArchiveProject();
    project1.setId("id-1");
    project1.setName("first name");
    project1.setAeriusVersion("DEV");
    final ArchiveProject project2 = new ArchiveProject();
    project2.setId("id-2");
    project2.setName("second name");
    project2.setAeriusVersion("NON_DEV");
    metaData.setArchiveProjects(List.of(project1, project2));

    CalculationRepository.insertArchiveMetadata(getConnection(), calculation.getCalculationId(), metaData);

    final ArchiveMetaData metaDataFromDb = CalculationRepository.getArchiveMetadata(getConnection(), calculation.getCalculationId());
    assertNotNull(metaDataFromDb, "Metadata from database should be present once inserted");
    assertEquals(retrievalDateTime, metaDataFromDb.getRetrievalDateTime(), "Retrieval date");
    assertEquals(2, metaDataFromDb.getArchiveProjects().size(), "Number of projects");
    assertEqualProject(project1, metaDataFromDb.getArchiveProjects().get(0));
    assertEqualProject(project2, metaDataFromDb.getArchiveProjects().get(1));
  }

  private void assertEqualProject(final ArchiveProject expected, final ArchiveProject actual) {
    assertEquals(expected.getId(), actual.getId(), "archive project id");
    assertEquals(expected.getName(), actual.getName(), "archive project name");
    assertEquals(expected.getAeriusVersion(), actual.getAeriusVersion(), "archive project version");
  }

  @Test
  void testArchiveMetaDataNotPresent() throws SQLException, IOException, AeriusException {
    final ArchiveMetaData metaDataFromDb = CalculationRepository.getArchiveMetadata(getConnection(), calculation.getCalculationId());
    assertNull(metaDataFromDb, "Returned object should be null if no metadata inserted");
  }

  private CalculationPointFeature createPoint(final int id, final double x, final double y) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setGeometry(new Point(x, y));
    final CustomCalculationPoint properties = new CustomCalculationPoint();
    properties.setCustomPointId(id);
    feature.setProperties(properties);
    return feature;
  }

  private void assertPersistedCalculation(final ScenarioCalculations cs) throws SQLException {
    for (final Calculation calculation : cs.getCalculations()) {
      assertNotEquals(0, calculation.getCalculationId(), "Calculation ID");
      assertNotNull(CalculationRepository.getCalculation(getConnection(), calculation.getCalculationId()), "Calculation in database");
    }
  }

  private ScenarioCalculations getExampleScenarioCalculations(final List<CalculationPointFeature> calculationPoints) {
    final Scenario scenario = new Scenario(Theme.OWN2000);
    final ScenarioSituation situation = new ScenarioSituation();
    situation.getEmissionSourcesList().addAll(createSources());
    situation.setYear(LocalDate.now().getYear());
    scenario.getSituations().add(situation);
    scenario.getCustomPointsList().addAll(calculationPoints);
    scenario.setOptions(getOptions());

    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);

    assertNull(scenarioCalculations.getCalculationPointSetTracker().getCalculationPointSetId(), "Calculation point set ID");
    for (final Calculation calculation : scenarioCalculations.getCalculations()) {
      assertEquals(0, calculation.getCalculationId(), "Calculation ID");
    }
    return scenarioCalculations;
  }

}
