/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.checksum;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;

/**
 * Unit test for {@link ChecksumRepository}.
 */
class ChecksumRepositoryTest extends BaseDBTest {

  private static final long CHECKSUM_RECEPTORS = -11118633998L;

  @Test
  void testGetApplicableTables() throws SQLException {
    final ArrayList<String> excludeTables = new ArrayList<>(Arrays.asList("public.receptors"));
    final List<String> tables = ChecksumRepository.getApplicableTables(getConnection(), excludeTables);
    assertNotNull(tables, "Applicable tables list shouldn't be null");
    assertFalse(tables.isEmpty(), "Applicable tables list shouldn't be empty");
    assertNotEquals(-1, tables.indexOf("public.hexagons"), "Missing expected table");
    assertEquals(-1, tables.indexOf("public.receptors"), "Exclude filter does not work");
  }

  @Test
  void testGetTablesContentChecksum() throws SQLException {
    // Build exclude table list that excludes everything except 2 tables -- otherwise unit test is too slow
    final List<String> excludeTables = ChecksumRepository.getApplicableTables(getConnection(), new ArrayList<String>());
    excludeTables.remove("public.receptors");
    excludeTables.remove("i18n.system_info_messages");

    final Map<String, Long> checksums = ChecksumRepository.getTablesContentChecksum(getConnection(), excludeTables);
    assertNotNull(checksums, "Checksums map shouldn't be null");
    assertEquals(2, checksums.size(), "Exclude list should have excluded all but 2 tables");

    // Test receptors because it is unlikely to change contents - and if it does it's under our own control.
    assertEquals(CHECKSUM_RECEPTORS, checksums.get("public.receptors").longValue(), "Unexpected checksum of receptors");
    assertNotEquals(0L, checksums.get("public.receptors").longValue(), "Checksum of receptors table should not be 0");
    assertEquals(0L, checksums.get("i18n.system_info_messages").longValue(), "Checksum of empty table should be 0");
  }

  @Test
  void testGetTableContentChecksum() throws SQLException {
    // Test receptors because it is unlikely to change contents
    final long checksum = ChecksumRepository.getTableContentChecksum(getConnection(), "public.receptors");
    assertEquals(CHECKSUM_RECEPTORS, checksum, "Unexpected checksum of receptors");
  }

  @Test
  void testGetDatabaseStructureChecksum() throws SQLException {
    final long checksum = ChecksumRepository.getDatabaseStructureChecksum(getConnection());
    assertNotEquals(0L, checksum, "Checksum of structure should not be 0");
  }

}
