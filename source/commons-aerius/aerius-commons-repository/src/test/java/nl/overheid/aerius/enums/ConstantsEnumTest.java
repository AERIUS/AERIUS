/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.enums;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.test.BaseTestDatabase;

/**
 * Test class for {@link ConstantRepository}.
 */
class ConstantsEnumTest extends BaseTestDatabase {

  @Test
  void testConstants() throws SQLException {
    final Connection connection = getConnection();

    for (final ConstantsEnum constant : ConstantsEnum.values()) {
      if (constant.isRequired()) {
        try {
          final String value = ConstantRepository.getString(connection, constant);
          assertFalse(value.isEmpty(), "Empty constant: " + constant);
        } catch (final IllegalArgumentException e) {
          fail(constant + " does not exist in database ");
        }
      }
    }
  }

}
