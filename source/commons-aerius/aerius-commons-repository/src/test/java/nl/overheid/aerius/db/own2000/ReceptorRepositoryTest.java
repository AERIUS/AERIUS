/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.own2000;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.ops.LandUse;

/**
 * Test class for {@link ReceptorRepository}.
 */
class ReceptorRepositoryTest extends BaseDBTest {

  private static final String SQL_GET_LAND_USE_ENUM = "SELECT unnest(enum_range(NULL::land_use_classification))";

  @Test
  void testGetPermitRequiredPoints() throws SQLException {
    final Map<Integer, List<AeriusPoint>> points = ReceptorRepository.getPermitRequiredPoints(getConnection(), RECEPTOR_UTIL);
    assertNotNull(points, "Receptors");
    assertNotEquals(0, points.size(), "Number of receptors");
  }

  @Test
  void testLandUseEnum() throws SQLException {
    // Checks for typos in the Java enum values
    final LandUse[] expectedLandUses = LandUse.values();
    final List<LandUse> actualLandUses = new ArrayList<>();
    try (final PreparedStatement ps = getConnection().prepareStatement(SQL_GET_LAND_USE_ENUM)) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        actualLandUses.add(LandUse.safeValueOf(rs.getString(1)));
      }
    }
    assertArrayEquals(expectedLandUses, actualLandUses.toArray(), "Land use enum in Java and DB");
  }
}
