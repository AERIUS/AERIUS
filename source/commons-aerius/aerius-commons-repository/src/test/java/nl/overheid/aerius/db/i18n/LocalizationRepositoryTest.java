/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.i18n;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.SQLException;
import java.util.Properties;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseTestDatabase;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Unit test for {@link LocalizationRepository}.
 */
class LocalizationRepositoryTest extends BaseTestDatabase {

  @Test
  void testGetLocalizedMessagesCalculator() throws SQLException {
    final Properties props = LocalizationRepository.getLocalizedMessages(getConnection(), LocaleUtils.getDefaultLocale(),
        DBMessages.getLocalizationObjects(getPMF()));
    assertNotNull(props, "Returned properties shouldn't be 0.");
    assertNotEquals(0, props.size(), "Should be something in the properties.");
  }
}
