/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator.calculationpoint;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

class CalculationPointRepositoryTest extends BaseDBTest {

  @Test
  void testDeterminePointsAutomatically() throws SQLException, AeriusException {
    int distanceFromSource;
    //assessment areas
    distanceFromSource = 25_000;
    DeterminedCalculationPointsCollector result = new DeterminedCalculationPointsCollector();
    CalculationPointRepository.determinePointsInlandAssessmentAreas(getConnection(), result, getExampleSourceLists(), distanceFromSource);
    assertNotEquals(0, result.getAmountOfAssessmentAreas(), "Should be some results");
    assertNotEquals(0, result.getPoints().size(), "Should be some results");
    assertAllCoordinatesRounded(result.getPoints());
    //relevant habitats
    distanceFromSource = 25_000;
    result = new DeterminedCalculationPointsCollector();
    CalculationPointRepository.determinePointsInlandHabitats(getConnection(), result, getExampleSourceLists(), distanceFromSource);
    assertNotEquals(0, result.getAmountOfAssessmentAreas(), "Should be some results");
    assertNotEquals(0, result.getPoints().size(), "Should be some results");
    assertAllCoordinatesRounded(result.getPoints());
    //check no results for distance 0.
    distanceFromSource = 0;
    result = new DeterminedCalculationPointsCollector();
    CalculationPointRepository.determinePointsInlandAssessmentAreas(getConnection(), result, getExampleSourceLists(), distanceFromSource);
    assertEquals(0, result.getAmountOfAssessmentAreas(), "Should be no results");
    assertEquals(0, result.getPoints().size(), "Should be no results");
    assertAllCoordinatesRounded(result.getPoints());
    //check no results for negative distance.
    distanceFromSource = -10_000;
    result = new DeterminedCalculationPointsCollector();
    CalculationPointRepository.determinePointsInlandAssessmentAreas(getConnection(), result, getExampleSourceLists(), distanceFromSource);
    assertEquals(0, result.getAmountOfAssessmentAreas(), "Should be no results");
    assertEquals(0, result.getPoints().size(), "Should be no results");
    assertAllCoordinatesRounded(result.getPoints());
    //check no results for no sources.
    distanceFromSource = 25_000;
    result = new DeterminedCalculationPointsCollector();
    CalculationPointRepository.determinePointsInlandAssessmentAreas(getConnection(), result, new ArrayList<>(), distanceFromSource);
    assertEquals(0, result.getAmountOfAssessmentAreas(), "Should be no results");
    assertEquals(0, result.getPoints().size(), "Should be no results");
    assertAllCoordinatesRounded(result.getPoints());
  }

  private ArrayList<EmissionSourceFeature> getExampleSourceLists() {
    final ArrayList<EmissionSourceFeature> sourceLists = new ArrayList<>();
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setProperties(new GenericEmissionSource());
    feature.setGeometry(new Point(168491, 447926));
    sourceLists.add(feature);
    return sourceLists;
  }

  private void assertAllCoordinatesRounded(final List<AeriusPoint> calculationPoints) {
    for (final AeriusPoint point : calculationPoints) {
      final double theX = point.getX();
      assertEquals(Math.round(theX), theX, 0d, "The x coordinate should have been rounded before");
      final double theY = point.getY();
      assertEquals(Math.round(theY), theY, 0d, "The y coordinate should have been rounded before");
    }
  }
}
