/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.legend.ColorItem;
import nl.overheid.aerius.shared.domain.legend.ColorItemType;

/**
 * Test class for ColorItemsRepository.
 */
class ColorItemsRepositoryTest extends BaseDBTest {

  @Test
  void testGetColorItems() throws SQLException {
    final Map<ColorItemType, List<ColorItem>> colorItems = ColorItemsRepository.getColorItems(getConnection());
    assertNotNull(colorItems, "Returned list should not be null");
    assertFalse(colorItems.isEmpty(), "Returned list should not be empty");

    final List<ColorItem> colorItem = colorItems.get(ColorItemType.ROAD_TYPE);
    assertNotNull(colorItem, "(ROAD_TYPE) list should not be null");
    assertFalse(colorItem.isEmpty(), "(ROAD_TYPE) list should not be empty");
  }

}
