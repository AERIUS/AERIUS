/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.i18n;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class to test MessageRepository and MessagesEnum values.
 */
class MessagesEnumTest extends BaseDBTest {

  static List<Object[]> data() throws SQLException, IOException {
    final List<Object[]> tests = new ArrayList<>();

    for (final Locale locale : LocaleUtils.KNOWN_LOCALES) {
      tests.add(new Object[] { locale });
    }
    return tests;
  }

  @ParameterizedTest
  @MethodSource("data")
  void testMessages(final Locale locale) throws SQLException {
    final Connection connection = getConnection();

    for (final MessagesEnum message : MessagesEnum.values()) {
      try {
        final String value = MessageRepository.getString(connection, message, locale);
        assertFalse(value.isEmpty(), "Empty message: " + message);
      } catch (final IllegalArgumentException e) {
        fail(message + " (" + locale.getLanguage() + ") does not exist in database.");
      }
    }
  }

}
