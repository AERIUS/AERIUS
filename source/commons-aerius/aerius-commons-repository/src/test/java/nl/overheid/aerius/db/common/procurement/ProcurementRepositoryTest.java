/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.procurement;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticsMarker;
import nl.overheid.aerius.shared.domain.summary.SituationResultsAreaSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
class ProcurementRepositoryTest extends BaseDBTest implements AreaInformationSupplier {

  private static final int FIRST_ASSESSMENT_AREA_ID = 57;
  private static final int SECOND_ASSESSMENT_AREA_ID = 65;

  @ParameterizedTest
  @MethodSource("casesForEmptyStart")
  void testAddProcurementStatisticsEmptyStart(final ProcurementPolicy policy, final boolean expectsNationalStatistics) throws AeriusException {
    final ProcurementRepository repository = new ProcurementRepository(getPMF(), this);
    final SituationResultsStatistics statistics = new SituationResultsStatistics();
    final List<SituationResultsAreaSummary> areaSummaries = List.of();
    final SituationResultsSummary summary = new SituationResultsSummary(statistics, areaSummaries, new ArrayList<>(), null, null);
    repository.addProcurementStatistics(summary, policy);

    assertEquals(expectsNationalStatistics ? 2 : 0, statistics.size(), "Number of statistics added when starting with empty stats");
    assertTrue(areaSummaries.isEmpty(), "Area summaries should be empty in any case");
  }

  private static Stream<Arguments> casesForEmptyStart() {
    return Stream.of(
        Arguments.of(ProcurementPolicy.WNB_LBV, false),
        Arguments.of(ProcurementPolicy.WNB_LBV_PLUS, true));
  }

  @ParameterizedTest
  @MethodSource("casesForFilledStart")
  void testAddProcurementStatistics(final ProcurementPolicy policy, final boolean expectsNationalStatistics, final boolean expectsAreaStatistics)
      throws AeriusException {
    final ProcurementRepository repository = new ProcurementRepository(getPMF(), this);
    final SituationResultsStatistics statistics = new SituationResultsStatistics();
    final double mockedSumContribution = 900000.0;
    statistics.put(ResultStatisticType.SUM_CONTRIBUTION, mockedSumContribution);
    final AssessmentArea firstArea = new AssessmentArea();
    firstArea.setId(FIRST_ASSESSMENT_AREA_ID);
    final SituationResultsAreaSummary firstAreaSummary = new SituationResultsAreaSummary(firstArea, true, null, null, null, null);
    firstAreaSummary.getStatistics().put(ResultStatisticType.SUM_CONTRIBUTION, 450.0);
    final AssessmentArea secondArea = new AssessmentArea();
    secondArea.setId(SECOND_ASSESSMENT_AREA_ID);
    final SituationResultsAreaSummary secondAreaSummary = new SituationResultsAreaSummary(secondArea, true, null, null, null, null);
    secondAreaSummary.getStatistics().put(ResultStatisticType.SUM_CONTRIBUTION, 1760.0);
    final List<SituationResultsAreaSummary> areaSummaries = List.of(firstAreaSummary, secondAreaSummary);
    final SituationResultsSummary summary = new SituationResultsSummary(statistics, areaSummaries, new ArrayList<>(), null, null);
    repository.addProcurementStatistics(summary, policy);

    assertEquals(expectsNationalStatistics ? 3 : 1, statistics.size(), "Number of statistics added");
    if (expectsNationalStatistics) {
      assertNotEquals(0.0, statistics.get(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_VALUE), "Threshold shouldn't be 0");
      assertNotEquals(0.0, statistics.get(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE), "Threshold percentage shouldn't be 0");
      assertEquals(100.0 * mockedSumContribution / statistics.get(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_VALUE),
          statistics.get(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE), 1E-5, "Threshold percentage shouldn't be 0");
    }

    assertEquals(2, areaSummaries.size(), "Area statistics should stay same size");
    for (final SituationResultsAreaSummary area : areaSummaries) {
      final SituationResultsStatistics areaStatistics = area.getStatistics();
      assertEquals(expectsAreaStatistics ? 3 : 1, areaStatistics.size(), "Number of statistics added for each area");
      if (expectsAreaStatistics) {
        assertNotEquals(0.0, areaStatistics.get(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_VALUE),
            "Area threshold shouldn't be 0");
        assertNotEquals(0.0, areaStatistics.get(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE),
            "Area threshold percentage shouldn't be 0");
        assertEquals(
            100.0 * areaStatistics.get(ResultStatisticType.SUM_CONTRIBUTION)
                / areaStatistics.get(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_VALUE),
            areaStatistics.get(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE), 1E-5,
            "Area threshold percentage should make sense");
        //First area is veluwe and low sum, expect it to be under threshold
        //Second area is veluwe and high sum, expect it to be above threshold
        assertEquals(area == firstAreaSummary, areaStatistics.get(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE) < 100.0,
            "Threshold sanity check for area " + area.getAssessmentArea().getId());
      }
    }
    assertEquals(expectsAreaStatistics ? 2 : 0, summary.getMarkers().size(), "Correct number of markers");
    for (final ResultStatisticsMarker marker : summary.getMarkers()) {
      assertEquals(1, marker.getStatisticsTypes().size(), "Exactly one statistic type");
      assertTrue(marker.getStatisticsTypes().contains(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_FAIL) ||
          marker.getStatisticsTypes().contains(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PASS), "Should fail or pass threshold");
    }
  }

  private static Stream<Arguments> casesForFilledStart() {
    return Stream.of(
        Arguments.of(ProcurementPolicy.WNB_LBV, false, true),
        Arguments.of(ProcurementPolicy.WNB_LBV_PLUS, true, false));
  }

  @Override
  public AssessmentArea determineAssessmentArea(final int assessmentAreaId) {
    final AssessmentArea fakeArea = new AssessmentArea();
    fakeArea.setId(assessmentAreaId);
    fakeArea.setName("Gebied " + assessmentAreaId);
    fakeArea.setMarkerLocation(new Point(1, 1));
    return fakeArea;
  }

  @Override
  public HabitatType determineHabitatType(final int habitatTypeId) {
    return null;
  }

  @Override
  public boolean determineHabitatTypeSensitiveness(final int habitatTypeId, final EmissionResultKey emissionResultKey) {
    return false;
  }

  @Override
  public Double determineHabitatCriticalLevel(final int habitatTypeId, final EmissionResultKey emissionResultKey) {
    return 0.0;
  }

}
