/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.db.common.configuration;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseTestDatabase;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ApplicationConfigurationInitializer}.
 */
class AppConfigurationRepositoryTest extends BaseTestDatabase {

  @Test
  void testGetCalculatorContext() throws SQLException, AeriusException {
    try (Connection con = getConnection()) {
      final ApplicationConfiguration config = ApplicationConfigurationInitializer.createApplicationConfiguration(con, getMessagesKey(),
          AeriusCustomer.RIVM, ProductProfile.CALCULATOR);
      assertNotNull(config, "Context for calculator");
      validateBaseContext(config);
    }
  }

  private void validateBaseContext(final ApplicationConfiguration configuration) {
    final SectorCategories categories = configuration.getSectorCategories();

    assertNotNull(categories.getSectors(), "sectors");
    assertNotNull(categories.getFarmAnimalHousingCategories(), "farmAnimalHousingCategories");
    assertNotNull(categories.getFarmLodgingCategories(), "farmLodgingCategories");
    assertNotNull(categories.getColdStartCategories(), "coldStartCategories");
    assertNotNull(categories.getRoadEmissionCategories(), "roadEmissionCategories");
    assertNotNull(categories.getOnRoadMobileSourceCategories(), "onRoadMobileSourceCategories");
    assertNotNull(categories.getOffRoadMobileSourceCategories(), "offRoadMobileSourceCategories");
    assertNotNull(categories.getMaritimeShippingCategories(), "maritimeShippingCategories");
    assertNotNull(categories.getInlandShippingCategories(), "inlandShippingCategories");
    assertNotNull(categories.getShippingSnappableNodes(), "shippingSnappableNodes");
  }

}
