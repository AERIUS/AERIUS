/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.i18n;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link SystemInfoMessageRepository}.
 */
class SystemInfoMessageRepositoryTest extends BaseDBTest {

  private final String MESSAGE = "Some message as system info.";

  static List<Object[]> data() throws SQLException, IOException {
    final List<Object[]> tests = new ArrayList<>();

    for (final Locale locale : LocaleUtils.KNOWN_LOCALES) {
      tests.add(new Object[] {locale});
    }
    return tests;
  }

  @ParameterizedTest
  @MethodSource("data")
  void testNoExisitingMessages(final Locale locale) throws SQLException {
    final Connection connection = getConnection();
    try {
      final String value = SystemInfoMessageRepository.getMessage(connection, locale);
      assertTrue(value.isEmpty(), "Message should me empty ");
    } catch (final IllegalArgumentException e) {
      fail("Entry for system info message " + " (" + locale.getLanguage() + ") does not exist in database");
    }
  }

  @ParameterizedTest
  @MethodSource("data")
  void testExistingMessages(final Locale locale) throws SQLException {
    final Connection connection = getConnection();
    SystemInfoMessageRepository.insertMessage(connection, MESSAGE, locale.getLanguage());
    try {
      final String value = SystemInfoMessageRepository.getMessage(connection, locale);
      assertEquals(MESSAGE, value, "Message should be same");
    } catch (final IllegalArgumentException e) {
      fail("Entry for system info message " + " (" + locale.getLanguage() + ") does not exist in database");
    }
  }

  @ParameterizedTest
  @MethodSource("data")
  void testFallbackMessages(final Locale locale) throws SQLException {
    final Connection connection = getConnection();
    // only set the default language to test fallback message for other languages
    SystemInfoMessageRepository.insertMessage(connection, MESSAGE, Locale.getDefault().getLanguage());

    try {
      final String value = SystemInfoMessageRepository.getMessage(connection, locale);
      assertEquals(MESSAGE, value, "Message should be same");
    } catch (final IllegalArgumentException e) {
      fail("Entry for system info message " + " (" + locale.getLanguage() + ") does not exist in database");
    }
  }

}
