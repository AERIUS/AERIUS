/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.connect.ConnectUserRepository;
import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Unit test for {@link JobRepository}.
 */
class JobRepositoryTest extends CalculationRepositoryTestBase {

  private static final String TEST_API_KEY = "wnaR9FavGRGv8RXCmdfXKEqeIt1DTZUS";
  private static final String TEST_EMAIL = "test@example.com";

  private static final String SQL_UPDATE_JOB_PROGRESS_LOG = "UPDATE jobs.job_progress_log SET state_time = ? WHERE state = ?::job_state_type"
      + " AND job_id = (SELECT job_id FROM jobs.jobs WHERE key = ? LIMIT 1)";

  @Test
  void testGetUnexistingJobId() throws SQLException {
    final int jobId = JobRepository.getJobId(getConnection(), "");
    assertEquals(0, jobId, "No job id should be found");
  }

  @Test
  void testGetUnexistingJobProgress() throws SQLException {
    final JobProgress jp = JobRepository.getProgress(getConnection(), "");
    assertNull(jp, "No job progress should be found");
  }

  @Test
  void testCreateEmptyJobWithoutCalculations() throws SQLException, AeriusException {
    final ConnectUser user = setupUser();
    final String jobKey = createJob(getConnection(), user, JobType.CALCULATION, false);
    assertFalse(jobKey.isEmpty(), "jobKey shouldn't be empty");

    final JobProgress jp = JobRepository.getProgress(getConnection(), jobKey);
    validateEmptyProgress(jp);
    assertSame(JobState.QUEUING, jp.getState(), "State may must be QUEUING");
    assertNull(jp.getName(), "Name should not be set");
  }

  @Test
  void testCreateEmptyJobWithCalculations() throws SQLException, AeriusException {
    final ConnectUser user = setupUser();
    final String jobKey = createJob(getConnection(), user, JobType.CALCULATION, false);
    insertCalculationResults();
    final Map<String, String> situationVersions = Map.of(calculation.getSituationReference(), "someVersion");
    calculation.getSituation().setName("some name");
    calculation.getSituation().setType(SituationType.OFF_SITE_REDUCTION);
    JobRepository.attachCalculations(getConnection(), jobKey, Collections.singletonList(calculation), situationVersions);

    final JobProgress jp = JobRepository.getProgress(getConnection(), jobKey);
    validateEmptyProgress(jp);
    assertEquals(JobState.QUEUING, jp.getState(), "State must be QUEUING");
    final SituationCalculations situationCalculations = JobRepository.getSituationCalculations(getConnection(), jobKey);
    assertEquals(1, situationCalculations.size(), "Number of situation calculations");
    assertEquals(calculation.getCalculationId(), situationCalculations.get(0).getCalculationId(), "calculation ID");
    assertEquals(calculation.getSituationReference(), situationCalculations.get(0).getSituationId(), "situation ID");
    assertEquals(SituationType.OFF_SITE_REDUCTION, situationCalculations.get(0).getSituationType(), "situation type");
    assertEquals("some name", situationCalculations.get(0).getSituationName(), "situation name");
    assertEquals("someVersion", situationCalculations.get(0).getSituationVersion(), "situation version");
  }

  @Test
  void testUpdateJobProgress() throws SQLException, AeriusException {
    final ConnectUser user = setupUser();
    final String testName = "My very long and weird job name.. GI%4j5h4g4jgR$_ 43-A";
    final String jobKey = JobRepository.createJob(getConnection(), user, JobType.CALCULATION, Optional.of(testName), false);
    JobRepository.updateJobStatus(getConnection(), jobKey, JobState.QUEUING);
    JobProgress jp;

    JobRepository.increaseNumberOfPointsCalculatedCounter(getConnection(), jobKey, 1);
    jp = JobRepository.getProgress(getConnection(), jobKey);
    assertEquals(JobType.CALCULATION, jp.getType(), "Type must be a calculation");
    assertEquals(1, jp.getNumberOfPointsCalculated(), "Number of points counter must be incremented");
    assertEquals(testName, jp.getName(), "Name must be updated");
    assertNotNull(jp.getStartDateTime(), "Start time must be set");
    assertFalse(jp.getStartDateTime().getTime() > new Date().getTime(), "Start time can't be in the future");

    JobRepository.increaseNumberOfPointsCalculatedCounter(getConnection(), jobKey, 12345);
    jp = JobRepository.getProgress(getConnection(), jobKey);
    assertEquals(1 + 12345, jp.getNumberOfPointsCalculated(), "Number of points calculated counter must be incremented");

    JobRepository.updateJobStatus(getConnection(), jobKey, JobState.COMPLETED);
    jp = JobRepository.getProgress(getConnection(), jobKey);
    assertNotNull(jp.getEndDateTime(), "End time must be set");
    assertFalse(jp.getEndDateTime().getTime() > new Date().getTime(), "End time can't be in the future");

    JobRepository.setResultUrl(getConnection(), jobKey, "abc");
    jp = JobRepository.getProgress(getConnection(), jobKey);
    assertNotNull(jp.getResultUrl(), "Result url must be set");

    // Ensure getProgressForUser gives same result
    final List<JobProgress> jpl = JobRepository.getProgressForUser(getConnection(), user);
    assertEquals(1, jpl.size(), "User must have 1 progress result");
    jp = jpl.get(0);
    assertEquals(1 + 12345, jp.getNumberOfPointsCalculated(), "Number of points calculated counter must be incremented");
    assertNotNull(jp.getStartDateTime(), "Start time must be set");
    assertFalse(jp.getStartDateTime().getTime() > new Date().getTime(), "Start time can't be in the future");
    assertNotNull(jp.getEndDateTime(), "End time must be set");
    assertFalse(jp.getEndDateTime().getTime() > new Date().getTime(), "End time can't be in the future");
    assertNotNull(jp.getResultUrl(), "Result url must be set");
    assertNotNull(jp.getKey(), "Key should be present");
  }

  @Test
  void testGetCalculationsProgressForUser() throws SQLException, AeriusException {
    List<JobProgress> progresses;

    final ConnectUser user = setupUser();
    assertJobProgressAmount(user, 0);
    final String jobKey = createJob(getConnection(), user, JobType.CALCULATION, false);
    createJob(getConnection(), user, JobType.CALCULATION, false);

    assertJobProgressAmount(user, 2);

    progresses = JobRepository.getProgressForUser(getConnection(), user);
    final JobProgress jp1a = progresses.get(0); // order by job_id
    final JobProgress jp2a = progresses.get(1);
    assertEquals(0, jp1a.getNumberOfPointsCalculated(), "Number of points calculated counter must be 0 (1st entry)");
    assertEquals(0, jp2a.getNumberOfPointsCalculated(), "Number of points calculated counter must be 0 (2nd entry)");
    assertNotNull(jp1a.getStartDateTime(), "Start time must be set (1st entry)");
    assertNotNull(jp2a.getStartDateTime(), "Start time must be set (2nd entry)");

    JobRepository.increaseNumberOfPointsCalculatedCounter(getConnection(), jobKey, 12345);
    progresses = JobRepository.getProgressForUser(getConnection(), user);
    final JobProgress jp1b = progresses.get(0);
    final JobProgress jp2b = progresses.get(1);
    assertEquals(12345, jp1b.getNumberOfPointsCalculated(), "Number of points calculated counter must be 0 (1st entry)");
    assertEquals(0, jp2b.getNumberOfPointsCalculated(), "Number of points calculated counter must still be 0 (2nd entry)");
    assertNotNull(jp1b.getStartDateTime(), "Start time must be set (2nd entry)");
    assertNotNull(jp2b.getStartDateTime(), "Start time must be set (2nd entry)");
  }

  @Test
  void testGetCalculationsProgressForUserAndJobStates() throws SQLException, AeriusException {
    List<JobProgress> progresses;

    final ConnectUser user = setupUser();
    final String jobKey = createJob(getConnection(), user, JobType.CALCULATION, false);
    assertJobProgressAmount(user, 1);

    progresses = JobRepository.getProgressForUserAndJobStates(getConnection(), user, Set.of(JobState.CALCULATING, JobState.CANCELLED));
    assertEquals(0, progresses.size(), "Should not be returned initially");
    JobRepository.updateJobStatus(getConnection(), jobKey, JobState.CALCULATING);
    progresses = JobRepository.getProgressForUserAndJobStates(getConnection(), user, Set.of(JobState.CALCULATING, JobState.CANCELLED));
    assertEquals(1, progresses.size(), "Should be returned after update the job state");
  }

  @Test
  void testRemoveJobsWithMinAge() throws SQLException, AeriusException {
    final ConnectUser user = setupUser();
    final int jobCountOffset = JobRepository.removeJobsWithMinAge(getConnection(), 5);
    final String jobKey1 = insertJobWithResults(user, JobType.CALCULATION, false);
    final String jobKey2 = insertJobWithResults(user, JobType.REPORT, false);
    final String jobKey3 = insertJobWithResults(user, JobType.CALCULATION, false);

    final Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MINUTE, -10); // so the timestamps created have the amount of days + 10 minutes of age
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    final Date endTimeJob1 = calendar.getTime();
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    final Date startTimeJob2 = calendar.getTime();
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    final Date endTimeJob3 = calendar.getTime();

    updateJobLogTime(getConnection(), jobKey1, JobState.COMPLETED, new Timestamp(endTimeJob1.getTime()));
    updateJobLogTime(getConnection(), jobKey2, JobState.CREATED, new Timestamp(startTimeJob2.getTime()));
    updateJobLogTime(getConnection(), jobKey3, JobState.COMPLETED, new Timestamp(endTimeJob3.getTime()));

    // start point - I am expecting 3 jobs
    assertJobProgressAmount(user, 3);

    assertEquals(jobCountOffset, JobRepository.removeJobsWithMinAge(getConnection(), 5), "There should be 0 removals");
    assertJobProgressAmount(user, 3);

    assertEquals(jobCountOffset + 1, JobRepository.removeJobsWithMinAge(getConnection(), 3), "There should be 1 removal");
    assertJobProgressAmount(user, 2);

    assertEquals(jobCountOffset + 2, JobRepository.removeJobsWithMinAge(getConnection(), 0), "There should be 2 removals");
    assertJobProgressAmount(user, 0);
  }

  private static <T> void updateJobLogTime(final Connection con, final String jobKey, final JobState state, final Timestamp timestamp)
      throws SQLException {
    try (final PreparedStatement ps = con.prepareStatement(SQL_UPDATE_JOB_PROGRESS_LOG)) {
      ps.setTimestamp(1, timestamp);
      ps.setObject(2, state.toDatabaseString());
      ps.setString(3, jobKey);
      ps.execute();
    }
  }

  private String insertJobWithResults(final ConnectUser user, final JobType jobType, final boolean protect) throws SQLException, AeriusException {
    final String jobKey = createJob(getConnection(), user, jobType, protect);
    JobRepository.updateJobStatus(getConnection(), jobKey, JobState.PREPARING);
    insertCalculationResults();
    JobRepository.attachCalculations(getConnection(), jobKey, Collections.singletonList(calculation));
    CalculationRepository.updateCalculationState(getConnection(), calculation.getCalculationId(), CalculationState.COMPLETED);
    JobRepository.updateJobStatus(getConnection(), jobKey, JobState.COMPLETED);
    newCalculation();
    return jobKey;
  }

  @Test
  void testSetDeleteStateProtectedJobs() throws SQLException, AeriusException {
    final ConnectUser user = setupUser();
    assertJobProgressAmount(user, 0);

    final String jobKey = createJob(getConnection(), user, JobType.CALCULATION, true);
    final String jobKeyOther = createJob(getConnection(), user, JobType.CALCULATION, false);
    assertJobStateNotDeleted(jobKey, JobState.QUEUING);

    JobRepository.updateJobStatus(getConnection(), jobKey, JobState.QUEUING);
    assertJobStateNotDeleted(jobKey, JobState.QUEUING);
    JobRepository.updateJobStatus(getConnection(), jobKey, JobState.CALCULATING);
    assertJobStateNotDeleted(jobKey, JobState.CALCULATING);
    assertEquals(JobState.QUEUING, JobRepository.getProgress(getConnection(), jobKeyOther).getState(), "Unrelated job should not change status");
  }

  @Test
  void testSetDeleteProtectedJobs() throws SQLException, AeriusException {
    final ConnectUser user = setupUser();
    final String jobKey = insertJobWithResults(user, JobType.CALCULATION, true);
    final String jobKeyOther = insertJobWithResults(user, JobType.CALCULATION, true);

    // Try removing protected calculation results
    assertFalse(JobRepository.removeJobCalculations(getConnection(), jobKey, false), "Should return false as no results should be removed");
    final SituationCalculations sc1 = JobRepository.getSituationCalculations(getConnection(), jobKey);
    assertEquals(1, sc1.getCalculationIds().size(), "Calculations should still be in database");

    // Try removing protected job
    final int jobId = JobRepository.getJobId(getConnection(), jobKey);
    JobRepository.removeJob(getConnection(), jobId, false);
    assertJobStateNotDeleted(jobKey, JobState.COMPLETED);

    // Remove protected calculation results
    assertTrue(JobRepository.removeJobCalculations(getConnection(), jobKey, true), "Should return ture as results should be removed");
    final SituationCalculations sc2 = JobRepository.getSituationCalculations(getConnection(), jobKey);
    assertEquals(0, sc2.getCalculationIds().size(), "Calculations should be gone");

    // Try removing protected job
    JobRepository.removeJob(getConnection(), jobId, true);
    assertNull(JobRepository.getProgress(getConnection(), jobKey), "Job should be gone");
    assertNotNull(JobRepository.getProgress(getConnection(), jobKeyOther), "Other unrelated job should not be gone");
  }

  @Test
  void testQueuePosition() throws SQLException, AeriusException {
    // Add first job, should be at front of queue
    final String jobKey1 = createJob(getConnection(), null, JobType.CALCULATION, false);
    assertEquals(0, JobRepository.getProgress(getConnection(), jobKey1).getQueuePosition(), "Job should be at front of queue.");

    // Add job at other queue, should influence queue position
    createJob(getConnection(), null, JobType.CALCULATION, false);

    // Add second job should be on position 2
    final String jobKey2 = createJob(getConnection(), null, JobType.CALCULATION, false);
    assertEquals(2, JobRepository.getProgressWithoutDeleted(getConnection(), jobKey2).getQueuePosition(), "Job should be at position 2.");

    // Complete first job, job 2 should be at position 1.
    JobRepository.updateJobStatus(getConnection(), jobKey1, JobState.COMPLETED);
    assertEquals(1, JobRepository.getProgressWithoutDeleted(getConnection(), jobKey2).getQueuePosition(), "Job should be at position 1.");

    // Complete second job, job 2 should return 0.
    JobRepository.updateJobStatus(getConnection(), jobKey2, JobState.COMPLETED);
    assertEquals(0, JobRepository.getProgressWithoutDeleted(getConnection(), jobKey2).getQueuePosition(), "Completed job should return 0.");
  }

  @Test
  void testEstimatedTime() throws SQLException, AeriusException, InterruptedException {
    final ConnectUser user = setupUser();
    final String testName = "My very long and weird job name.. GI%4j5h4g4jgR$_ 43-A";
    final String jobKey = JobRepository.createJob(getConnection(), user, JobType.CALCULATION, Optional.of(testName), false);
    JobRepository.updateJobStatus(getConnection(), jobKey, JobState.CALCULATING);
    // First test if the estimated time is larger than 0 because we have nothing calculated yet.
    JobRepository.setNumberOfPoints(getConnection(), jobKey, 100);
    assertEquals(-1, JobRepository.getProgress(getConnection(), jobKey).getEstimatedRemainingCalculationTime(), "Estimated time should be -1.");

    JobRepository.increaseNumberOfPointsCalculatedCounter(getConnection(), jobKey, 1);
    Thread.sleep(2000); // delay a bit to have time frame to compute the estimated time from.
    final int startEstimatedTime = JobRepository.getProgress(getConnection(), jobKey).getEstimatedRemainingCalculationTime();
    assertNotEquals(-1, JobRepository.getProgress(getConnection(), jobKey).getEstimatedRemainingCalculationTime(), "Estimated time should not be -1.");

    Thread.sleep(3000);
    // Some calculation done. So the time estimation should be smaller.
    JobRepository.increaseNumberOfPointsCalculatedCounter(getConnection(), jobKey, 50);
    final int halfWayTime = JobRepository.getProgress(getConnection(), jobKey).getEstimatedRemainingCalculationTime();
    assertTrue(halfWayTime * 2 < startEstimatedTime,
        "We have computed half the points, therefor time estimation should be at least smaller than half of the original estimation. halfWayTime:"
            + halfWayTime + ", startEstimatedTime:" + startEstimatedTime);

    // Everything calculated. Now time estimation should be 0.
    JobRepository.increaseNumberOfPointsCalculatedCounter(getConnection(), jobKey, 50);
    assertEquals(0, JobRepository.getProgress(getConnection(), jobKey).getEstimatedRemainingCalculationTime(),
        "Estimated time when completed should be 0.");
  }

  private static String createJob(final Connection connection, final ConnectUser user, final JobType calculation, final boolean protect)
      throws SQLException, AeriusException {
    final String jobKey = JobRepository.createJob(connection, user, calculation, protect);

    JobRepository.updateJobStatus(connection, jobKey, JobState.QUEUING);
    return jobKey;
  }

  private void assertJobStateNotDeleted(final String jobKey, final JobState expectedState) throws SQLException {
    // Try setting the state to deleted on a protected job. This should not succeed.
    JobRepository.updateJobStatus(getConnection(), jobKey, JobState.DELETED);
    assertEquals(expectedState, JobRepository.getProgress(getConnection(), jobKey).getState(), "Protected job should not get deleted state");
  }

  private ConnectUser setupUser() throws SQLException, AeriusException {
    final ConnectUser user = new ConnectUser();
    user.setApiKey(TEST_API_KEY);
    user.setEmailAddress(TEST_EMAIL);
    ConnectUserRepository.createUser(getConnection(), user);
    return user;
  }

  private void assertJobProgressAmount(final ConnectUser user, final int amount) throws SQLException {
    assertEquals(amount, JobRepository.getProgressForUser(getConnection(), user).size(), "Couldn't find the expected amount of jobs");
  }

  private void validateEmptyProgress(final JobProgress jp) throws SQLException {
    assertNotNull(jp, "Job progress should be found");
    assertEquals(JobType.CALCULATION, jp.getType(), "Job type must be a calculation");
    assertEquals(0, jp.getNumberOfPointsCalculated(), "Number of points calculated counter must be 0");
    assertNotNull(jp.getStartDateTime(), "Start time must be set");
    assertNull(jp.getEndDateTime(), "End time may not be set");
    assertNull(jp.getResultUrl(), "Result url may not be set");
    assertNotNull(jp.getKey(), "jobKey should be present");
  }

}
