/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Test class for {@link ReceptorGridSettingsRepository}.
 */
class ReceptorGridSettingsRepositoryTest extends BaseDBTest {

  @Test
  void testSrid() throws SQLException {
    assertEquals(EPSG.RDNEW.getSrid(), ReceptorGridSettingsRepository.getSrid(getConnection()), "SRID not correctly set");
  }

  @Test
  void testGridBBox() throws SQLException {
    final BBox bbox = ReceptorGridSettingsRepository.getCalculatorGridBBox(getConnection());
    assertTrue(bbox.getMaxX() > 0, "MaxX not correctly set:" + bbox.getMaxX());
    assertTrue(bbox.getMaxY() > 0, "MaxY not correctly set:" + bbox.getMaxY());
  }

  @Test
  void testZoomLevels() throws SQLException {
    final List<HexagonZoomLevel> zoomLevels = ReceptorGridSettingsRepository.getZoomLevels(getConnection());
    assertEquals(5, zoomLevels.size(), "Number of zoomlevels not as expected");
    assertEquals(10000, zoomLevels.get(0).getHexagonSurface(), 0.1E-5, "Surface level1 not as expected");
  }

  @Test
  void testNumberOfHexagonRows() throws SQLException {
    assertEquals(1529, ReceptorGridSettingsRepository.getReceptorGridSettings(getPMF()).getHexHor(),
        "Expected number of hexagons in a row not as expected.");
  }
}
