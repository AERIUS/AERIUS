/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.emissions.shipping.ShippingLaden;

/**
 * Test class for {@link InlandCategoryKey}.
 */
class InlandCategoryKeyTest {

  @Test
  void testEquals() {
    final InlandCategoryKey[] keys = {
      new InlandCategoryKey(WaterwayDirection.IRRELEVANT, ShippingLaden.LADEN, 1),
      new InlandCategoryKey(WaterwayDirection.IRRELEVANT, ShippingLaden.UNLADEN, 1),
      new InlandCategoryKey(WaterwayDirection.DOWNSTREAM, ShippingLaden.LADEN, 1),
      new InlandCategoryKey(WaterwayDirection.DOWNSTREAM, ShippingLaden.UNLADEN, 1),
      new InlandCategoryKey(WaterwayDirection.UPSTREAM, ShippingLaden.LADEN, 1),
      new InlandCategoryKey(WaterwayDirection.UPSTREAM, ShippingLaden.UNLADEN, 1),
    };

    for (int i = 0; i < (keys.length - 1); i++) {
      final InlandCategoryKey key1 = keys[i];
      for (int j = i + 1; j < keys.length; j++) {
        final InlandCategoryKey key2 = keys[j];
        assertNotEquals(key1, key2, "Keys should not be the same:" + key1 + "," + key2);
      }
    }
  }
}
