/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.connect;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.connect.ConnectCalculationPointSetMetadata;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;

/**
 * Test class for {@link ConnectCalculationPointSetsRepository}.
*/
class ConnectCalculationPointSetsRepositoryTest extends BaseDBTest {

  private static final String TEST_EMAIL = "aerius@example.com";
  private ConnectUser scenarioUser;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    final ConnectUser tempScenarioUser = new ConnectUser();
    tempScenarioUser.setApiKey("NotImportant");
    tempScenarioUser.setEmailAddress(TEST_EMAIL);
    tempScenarioUser.setEnabled(true);
    ConnectUserRepository.createUser(getConnection(), tempScenarioUser);
    scenarioUser = ConnectUserRepository.getUserByEmailAddress(getConnection(), TEST_EMAIL);
    assertNotNull(scenarioUser, "Creating ScenarioUser failed.");
  }

  @Test
  void insertUserSingleCalculationPointSet() throws SQLException {
    final ConnectCalculationPointSetMetadata setMetadata = storeOneUCPS("BargerveenCPSet");
    checkStoredUCPS(setMetadata);
  }

  @Test
  void getUserCalculationPointSets() throws Exception {
    final List<ConnectCalculationPointSetMetadata> beforeList = ConnectCalculationPointSetsRepository
        .getUserCalculationPointSetsForUser(getConnection(), scenarioUser.getId());
    assertTrue(beforeList.isEmpty(), "List of UserCalculationPointSets should be empty.");
    final List<ConnectCalculationPointSetMetadata> setMetadataList = new ArrayList<>();
    setMetadataList.add(storeOneUCPS("BargerveenCPSet"));
    setMetadataList.add(storeOneUCPS("NogEenBargerveenCPSet"));
    final List<ConnectCalculationPointSetMetadata> list2 = ConnectCalculationPointSetsRepository
        .getUserCalculationPointSetsForUser(getConnection(), scenarioUser.getId());
    assertEquals(setMetadataList.size(), list2.size(), "List of UserCalculationPointSets should be two long.");
  }

  @Test
  void getUserCalculationPointSetIsFailed() throws Exception {
    final ConnectCalculationPointSetMetadata storedSetMetadata = ConnectCalculationPointSetsRepository
        .getUserCalculationPointSetByName(getConnection(), scenarioUser.getId(), "SomeNameNotPresent");
    assertNull(storedSetMetadata);
  }

  private ConnectCalculationPointSetMetadata storeOneUCPS(final String setName) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setGeometry(new Point(265755, 521869));
    final CustomCalculationPoint calculationPoint = new CustomCalculationPoint();
    calculationPoint.setCustomPointId(1);
    calculationPoint.setLabel("Bargerveen");
    feature.setProperties(calculationPoint);
    final List<CalculationPointFeature> calculationPoints = new ArrayList<>();
    calculationPoints.add(feature);
    final ConnectCalculationPointSetMetadata setMetadata = new ConnectCalculationPointSetMetadata();
    setMetadata.setName(setName);
    setMetadata.setUserId(scenarioUser.getId());
    setMetadata.setDescription("BargerveenDescription");
    assertDoesNotThrow(
        () -> ConnectCalculationPointSetsRepository.unsafeInsertUserCalculationPointSet(getConnection(), setMetadata, calculationPoints),
        "Inserting a set of one calculationpoint failed totally unexpectedly.");
    return setMetadata;
  }

  /**
   * @param setMetadata
   * @throws SQLException
   */
  private void checkStoredUCPS(final ConnectCalculationPointSetMetadata setMetadata) throws SQLException {
    final ConnectCalculationPointSetMetadata storedSetMetadata = ConnectCalculationPointSetsRepository
        .getUserCalculationPointSetByName(getConnection(), scenarioUser.getId(), setMetadata.getName());
    assertNotNull(storedSetMetadata);
  }
}
