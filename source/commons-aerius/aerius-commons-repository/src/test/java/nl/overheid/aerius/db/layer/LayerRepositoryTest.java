/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.layer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.aerius.geo.shared.LayerProps;
import nl.aerius.geo.shared.LayerWMSProps;
import nl.aerius.geo.shared.LayerWMTSProps;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link LayerRepository}.
 */
class LayerRepositoryTest extends BaseDBTest {

  private static final int TEST_LEGEND_ID = 1;
  private static final String TEST_WMS_LAYER_NAME = "calculator:wms_habitat_areas_sensitivity_view";
  private static final String TEST_WMTS_LAYER_NAME = "standaard";

  static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> connectionTests = new ArrayList<>();
    final Object[] calculator = new Object[3];
    calculator[0] = getPMF();
    calculator[1] = "Calculator";
    calculator[2] = new DBMessagesKey(LocaleUtils.getDefaultLocale());
    connectionTests.add(calculator);
    return connectionTests;
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetLayers(final PMF pmf, final String testDescription, final DBMessagesKey messagesKey) throws SQLException, AeriusException {
    final List<LayerProps> layers = LayerRepository.getLayers(pmf.getConnection(), Theme.OWN2000, messagesKey);
    assertNotNull(layers, "Returned list");
    assertFalse(layers.isEmpty(), "Returned list shouldn't be empty");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetLegend(final PMF pmf, final String testDescription, final DBMessagesKey messagesKey) throws SQLException {
    final Legend legend = LayerRepository.getLegend(pmf.getConnection(), TEST_LEGEND_ID);
    assertTrue(legend instanceof ColorLabelsLegend, "Should be a legend with ID " + TEST_LEGEND_ID + ", but found " + legend);
    final ColorLabelsLegend colorLegend = (ColorLabelsLegend) legend;
    assertSame(LegendType.CIRCLE, colorLegend.getIcon(), "First one isn't a hexagon-kind");
    assertEquals(4, colorLegend.getLabels().length, "Nr of legend names");
    assertEquals(4, colorLegend.getColors().length, "Nr of colors");
    assertEquals("Habitatrichtlijn", colorLegend.getLabels()[0], "First legend name");
    assertEquals("#F4E798", colorLegend.getColors()[0], "First legend color");
    assertEquals("Vogelrichtlijn, Habitatrichtlijn", colorLegend.getLabels()[2], "Third legend name");
    assertEquals("#CFE2A1", colorLegend.getColors()[2], "Third legend color");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetLayer(final PMF pmf, final String testDescription, final DBMessagesKey messagesKey) throws SQLException, AeriusException {
    LayerProps layerProps;
    layerProps = LayerRepository.getLayer(pmf.getConnection(), TEST_WMS_LAYER_NAME, messagesKey);
    assertTrue(layerProps instanceof LayerWMSProps, "Should be a WMS layer with name " + TEST_WMS_LAYER_NAME + ", but found " + layerProps);
    final LayerWMSProps wmsProps = (LayerWMSProps) layerProps;
    assertEquals(TEST_WMS_LAYER_NAME, wmsProps.getName(), "Name");
    assertEquals(13, wmsProps.getId(), "ID");
    assertEquals(188000, wmsProps.getMinScale(), 1E-6, "min scale");
    assertNull(wmsProps.getMaxScale(), "max scale");
    assertEquals(0.8, wmsProps.getOpacity(), 1E-6, "opacity");
    assertNotNull(wmsProps.getLegend(), "Legend");
    assertNotNull(wmsProps.getUrl(), "URL");

    layerProps = LayerRepository.getLayer(pmf.getConnection(), TEST_WMTS_LAYER_NAME, messagesKey);
    assertTrue(layerProps instanceof LayerWMTSProps, "Should be a WMTS layer with name " + TEST_WMTS_LAYER_NAME + ", but found " + layerProps);
    final LayerWMTSProps tmsProps = (LayerWMTSProps) layerProps;
    assertEquals(TEST_WMTS_LAYER_NAME, tmsProps.getName(), "Name");
    assertEquals(5, tmsProps.getId(), "ID");
    assertNull(tmsProps.getMinScale(), "min scale");
    assertNull(tmsProps.getMaxScale(), "max scale");
    assertEquals(0.8, tmsProps.getOpacity(), 1E-6, "max scale");
    assertNull(tmsProps.getLegend(), "Legend");
    assertEquals("png8", tmsProps.getType(), "Type");
    assertNotNull(tmsProps.getUrl(), "URL");
    assertEquals("1.0.0", tmsProps.getServiceVersion(), "Service");
    assertEquals("&copy; OSM &amp; Kadaster", tmsProps.getAttribution(), "Attribution");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetBaseLayer(final PMF pmf, final String testDescription, final DBMessagesKey messagesKey) throws SQLException, AeriusException {
    final List<LayerProps> layerProps = LayerRepository.getBaseLayers(pmf.getConnection(), Theme.OWN2000, messagesKey);

    assertEquals(2, layerProps.size(), "Should be 2 layers in there");
  }

}
