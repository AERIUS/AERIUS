/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.geo.AreaType;
import nl.overheid.aerius.shared.domain.geo.WKTGeometryWithBox;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;

/**
 * Test class for {@link GeometryRepository}.
 */
class GeometryRepositoryTest extends BaseDBTest {

  private static final Point AREA_POINT = new Point(168010, 447122);
  private static final float TOLERANCE = 100.0F;
  private static final String NAME_GUIDE = "Bi";
  private static final Integer AREA_ID = 65;

  @Test
  void testToleranceAreaPointNameGuide() throws SQLException {
    final WKTGeometryWithBox geometry =
        GeometryRepository.getSimplifiedGeometry(getConnection(), AreaType.NATURA2000_AREA, AREA_ID, AREA_POINT, TOLERANCE, NAME_GUIDE);
    assertResult(geometry);
  }

  @Test
  void testToleranceAreaPoint() throws SQLException {
    final WKTGeometryWithBox geometry =
        GeometryRepository.getSimplifiedGeometry(getConnection(), AreaType.NATURA2000_AREA, AREA_ID, AREA_POINT, TOLERANCE, null);
    assertResult(geometry);
  }

  @Test
  void testToleranceNameGuide() throws SQLException {
    final WKTGeometryWithBox geometry =
        GeometryRepository.getSimplifiedGeometry(getConnection(), AreaType.NATURA2000_AREA, AREA_ID, null, TOLERANCE, NAME_GUIDE);
    assertResult(geometry);
  }

  @Test
  void testTolerance() throws SQLException {
    final WKTGeometryWithBox geometry =
        GeometryRepository.getSimplifiedGeometry(getConnection(), AreaType.NATURA2000_AREA, AREA_ID, null, 100F, null);
    assertResult(geometry);
  }

  private void assertResult(final WKTGeometryWithBox geometry) {
    assertNotNull(geometry, "Geometry not null");
    assertNotNull(geometry.getBoundingBox(), "Bounding Box not null");
  }

}
