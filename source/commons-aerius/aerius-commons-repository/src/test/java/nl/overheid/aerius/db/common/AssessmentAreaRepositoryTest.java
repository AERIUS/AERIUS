/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.locationtech.jts.geom.Geometry;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.geo.HexagonUtil;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Test class for {@link AssessmentAreaRepository}.
 */
class AssessmentAreaRepositoryTest extends BaseDBTest {

  private static final int BINNENVELD_ID = 65;

  static List<Arguments> data() {
    return List.of(Arguments.of(getPMF(), "Calculator"));
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetNatura2kAreas(final PMF pmf, final String testDescription) throws SQLException {
    final List<AssessmentArea> n2kAreas = AssessmentAreaRepository.getNatura2kAreas(pmf.getConnection());
    assertNotNull(n2kAreas, testDescription + ": Areas shouldn't be null");
    assertFalse(n2kAreas.isEmpty(), testDescription + ": Areas should be found");
    for (final AssessmentArea area : n2kAreas) {
      assertNotEquals(0, area.getId(), testDescription + ": Area ID");
      assertNotNull(area.getName(), testDescription + ": Area name");
    }
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetAssessmentArea(final PMF pmf, final String testDescription) throws SQLException {
    AssessmentArea area = AssessmentAreaRepository.getAssessmentArea(pmf.getConnection(), 0);
    assertNull(area, testDescription + ": AssessmentArea should be null");

    area = AssessmentAreaRepository.getAssessmentArea(pmf.getConnection(), BINNENVELD_ID);
    assertNotNull(area, testDescription + ": AssessmentArea shouldn't be null");
    assertEquals(BINNENVELD_ID, area.getId(), testDescription + ": Area ID");
    assertEquals(BINNENVELD_ID, area.getId(), testDescription + ": Area AssessmentAreaID");
    assertEquals("Binnenveld", area.getName(), testDescription + ": Area name");
    assertNotNull(area.getBounds(), testDescription + ": Area bounding box");
    assertNotNull(area.getMarkerLocation(), testDescription + ": Area marker location");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetAssessmentAreas(final PMF pmf, final String testDescription) throws SQLException {
    final Map<Integer, AssessmentArea> areas = AssessmentAreaRepository.getAssessmentAreas(pmf.getConnection());
    assertNotNull(areas, testDescription + ": Areas shouldn't be null");
    assertFalse(areas.isEmpty(), testDescription + ": Areas should be found");

    for (final Entry<Integer, AssessmentArea> entry : areas.entrySet()) {
      final AssessmentArea area = entry.getValue();
      assertNotEquals(0, area.getId(), testDescription + ": Area ID");
      assertEquals(entry.getKey().intValue(), area.getId(), testDescription + ": Area ID");
      assertNotNull(area.getName(), testDescription + ": Area name");
      assertNotNull(area.getBounds(), testDescription + ": Area bounds");
      assertNotNull(area.getMarkerLocation(), testDescription + ": Area marker location");
    }
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetNatureAreaDirectives(final PMF pmf, final String testDescription) throws SQLException {
    final List<SimpleCategory> directives = AssessmentAreaRepository.getNatureAreaDirectives(pmf.getConnection());
    assertNotNull(directives, testDescription + ": Directives shouldn't be null");
    assertFalse(directives.isEmpty(), testDescription + ": Directives should be found");

    for (final SimpleCategory directive : directives) {
      assertNotEquals(0, directive.getId(), testDescription + ": Directive ID");
      assertNotNull(directive.getCode(), testDescription + ": Directive code");
      assertNotNull(directive.getName(), testDescription + ": Directive name");
    }
  }

  @Test
  void testRelevantHabitatsInsideGeometry() throws SQLException, AeriusException {
    final nl.overheid.aerius.shared.domain.v2.geojson.Point point = RECEPTOR_UTIL.getPointFromReceptorId(4776053);
    final Polygon hexagon = HexagonUtil.createHexagon(point, RECEPTOR_GRID_SETTINGS.getZoomLevel1());
    final Polygon area = new Polygon();
    // Area for sub receptor triangle: S4776053-489
    area.setCoordinates(new double[][][]
        {{{187084.3805, 464587.3788}, {187080.503, 464594.0949}, {187088.2581, 464594.0949}, {187084.3805, 464587.3788}}});
    final Geometry geometry = AssessmentAreaRepository.relevantHabitatsInsideGeometry(getConnection(), hexagon);
    assertNotNull(geometry, "Should be relevant habitats for this receptor");
    assertTrue(geometry.intersects(GeometryUtil.getGeometry(area)), "Should intersect with this area");
    // Random other point. Technically could intersect because habitat geometry that is checked against
    // is union of habitats inside reference hexagon and that could overlap with geometry of random point tested here.
    assertFalse(geometry.intersects(GeometryUtil.getGeometry(RECEPTOR_UTIL.getPointFromReceptorId(4708740))),
        "Should not intersect with random other hexagon");
  }

  @Test
  void testNatureAreaInsideGeometry() throws SQLException, AeriusException {
    // Take point 7391773 somewhere in the waddenzee area, this is a hexagon covered by nature area, but without habitats.
    final int receptorId = 7391773;
    final nl.overheid.aerius.shared.domain.v2.geojson.Point point = RECEPTOR_UTIL.getPointFromReceptorId(receptorId);
    final Polygon hexagon = HexagonUtil.createHexagon(point, RECEPTOR_GRID_SETTINGS.getZoomLevel1());
    final Polygon area = new Polygon();
    // Area for sub receptor triangle: S7391773-31
    area.setCoordinates(new double[][][]
        {{{112639.8693, 556577.281}, {112632.1143, 556563.8489}, {112647.6244, 556563.8489}, {112639.8693, 556577.281}}});
    final Geometry areaGgeometry = AssessmentAreaRepository.relevantAssessmentAreaInsideGeometry(getConnection(), hexagon);

    assertNotNull(areaGgeometry, "Should return an area geometry that intersects with the hexagon");
    assertTrue(areaGgeometry.intersects(GeometryUtil.getGeometry(area)), "Nature area geometry should intersect with this subreceptor area");
    assertTrue(areaGgeometry.intersects(GeometryUtil.getGeometry(RECEPTOR_UTIL.getPointFromReceptorId(receptorId))),
        "Area geometry should intersect with receptor point");

    final Geometry habitatGeometry = AssessmentAreaRepository.relevantHabitatsInsideGeometry(getConnection(), hexagon);
    assertNull(habitatGeometry, "Should not find a geometry to use for intersection because no habitats on location of hexagon");
  }
}
