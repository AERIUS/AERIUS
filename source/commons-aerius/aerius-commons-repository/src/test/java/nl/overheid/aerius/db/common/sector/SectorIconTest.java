/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.sector.SectorIconUtil;

/**
 * Tests if the {@link SectorIcon} enum contains all known sectors.
 */
class SectorIconTest extends BaseDBTest {

  static List<Object[]> data() throws SQLException, IOException {
    BaseDBTest.setUpBeforeClass();
    try (Connection con = getPMF().getConnection()) {
      final List<Sector> sectors = SectorRepository.getSectors(con, getMessagesKey());

      return sectors.stream().map(s -> new Object[] {s}).collect(Collectors.toList());
    }
  }

  @ParameterizedTest
  @MethodSource("data")
  void testSectorIcon(final Sector sector) {
    assertNotNull(SectorIconUtil.own2000().get(sector.getSectorId()), "Sector " + sector.getName() + " has no sectorIcon");
  }
}
