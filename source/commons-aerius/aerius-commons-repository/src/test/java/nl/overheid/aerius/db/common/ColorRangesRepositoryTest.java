/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.ResultTypeColorRangeTypes;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;

/**
 * Test class for ColorRangesRepository.
 */
class ColorRangesRepositoryTest extends BaseDBTest {

  @Test
  void testGetColorRanges() throws SQLException {
    final Map<ColorRangeType, List<ColorRange>> colorRanges = ColorRangesRepository.getColorRanges(getConnection());
    assertNotNull(colorRanges, "Returned list should not be null");
    assertFalse(colorRanges.isEmpty(), "Returned list should not be empty");

    final List<ColorRange> colorRange = colorRanges.get(ColorRangeType.CONTRIBUTION_DEPOSITION);
    assertNotNull(colorRange, "(CONTRIBUTION_DEPOSITION) list should not be null");
    assertFalse(colorRange.isEmpty(), "(CONTRIBUTION_DEPOSITION) list should not be empty");
  }

  @Test
  void testGetResultTypeColorRangeTypes() throws SQLException {
    final ResultTypeColorRangeTypes resultTypeColorRangeTypes = ColorRangesRepository.getResultTypeColorRangeTypes(getConnection());
    assertNotNull(resultTypeColorRangeTypes, "Returned list should not be null");

    assertEquals(ColorRangeType.CONTRIBUTION_DEPOSITION,
        resultTypeColorRangeTypes.determineColorRangeType(ScenarioResultType.SITUATION_RESULT, EmissionResultKey.NOXNH3_DEPOSITION),
        "Correct type for project calculation");

    assertEquals(ColorRangeType.DIFFERENCE_DEPOSITION,
        resultTypeColorRangeTypes.determineColorRangeType(ScenarioResultType.PROJECT_CALCULATION, EmissionResultKey.NOXNH3_DEPOSITION),
        "Correct type for project calculation");
    assertEquals(ColorRangeType.DIFFERENCE_DEPOSITION,
        resultTypeColorRangeTypes.determineColorRangeType(ScenarioResultType.MAX_TEMPORARY_EFFECT, EmissionResultKey.NOXNH3_DEPOSITION),
        "Correct type for max temporary effect");

  }
}
