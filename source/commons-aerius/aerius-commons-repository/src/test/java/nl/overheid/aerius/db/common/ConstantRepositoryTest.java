/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseTestDatabase;

/**
 * Test class for {@link ConstantRepository}.
 */
class ConstantRepositoryTest extends BaseTestDatabase {

  private enum Dummy {
    DUMMY;
  }

  @Test
  void testGetNotExists() throws SQLException {
    final Connection connection = getConnection();
    final Integer value = ConstantRepository.getNumber(connection, Dummy.DUMMY, Integer.class, false);
    assertNull(value, "value should be null");
  }

  @Test
  void testGetNotExistsMustExist() throws SQLException {
    final Connection connection = getConnection();

    assertThrows(SQLException.class, () -> ConstantRepository.getNumber(connection, Dummy.DUMMY, Integer.class, true),
        "\"Expected exception for getting a none existing constant that should exist.");
  }

  @Test
  void testSetNotExists() throws SQLException {
    final Connection connection = getConnection();

    assertThrows(SQLException.class, () -> ConstantRepository.setString(connection, Dummy.DUMMY, Dummy.DUMMY.name()),
        "Expected exception for trying to set an non existing constant.");
  }
}
