/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.sector.ShippingNode;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;

/**
 * Needs a proper (filled) database to test against, testing DB operations for the shipping sector.
 */
class ShippingRepositoryTest extends BaseDBTest {

  private static final Geometry TEST_INLAND_ROUTE = constructLineString(new double[][] {{208000, 455000}, {208000, 456000}});
  private static final String TEST_INLAND_ROUTE_TYPE = "IJssel";
  private static final Geometry TEST_INLAND_LOCK_ROUTE = constructLineString(new double[][] {{126720, 427954}, {125624, 426261}});
  private static final String TEST_INLAND_LOCK_ROUTE_TYPE = "CEMT_IV";
  private static final String EXAMPLE_SHIP_CODE = "OO100";

  @Test
  void testGetShippingSnappableNodes() throws SQLException {
    final ArrayList<ShippingNode> nodes = ShippingRepository.getShippingSnappableNodes(getConnection());
    assertNotNull(nodes, "There should be some nodes in the DB");
    assertFalse(nodes.isEmpty(), "There should be some nodes in the DB");
  }

  @Test
  void testGetShippingRouteCategory() throws SQLException, AeriusException {
    final Geometry geometry = constructLineString(new double[][] {{10000, 10000}, {10100, 10100}});
    final List<MaritimeShippingRoutePoint> routePoints = ShippingRepository.getMaritimeMooringShippingInlandRoute(
        getConnection(), geometry, "OO100", 25, true, false);
    assertNotNull(routePoints, "Route shouldn't be null");
    assertFalse(routePoints.isEmpty(), "Route should consist of some points");
    for (final MaritimeShippingRoutePoint routePoint : routePoints) {
      assertTrue(routePoint.getX() > 10000 && routePoint.getX() < 10100, "Source X for routePoint:" + routePoint);
      assertTrue(routePoint.getX() > 10000 && routePoint.getX() < 10100, "Source Y for routePoint:" + routePoint);
      assertNotEquals(0.0, routePoint.getMeasure(), 1E-3, "Measure for routePoint > 0:" + routePoint);
      assertNotEquals(0.0, routePoint.getManeuverFactor(), 1E-3, "Maneuver factor for routePoint > 0:" + routePoint);
    }
  }

  @Test
  void testGetShippingRouteGrossTonnage() throws SQLException, AeriusException {
    final Geometry geometry = constructLineString(new double[][] {{10000, 10000}, {10100, 10100}});
    final List<MaritimeShippingRoutePoint> routePoints = ShippingRepository.getMaritimeMooringShippingInlandRouteForGrossTonnage(
        getConnection(), geometry, 70000, 25, true, false);
    assertNotNull(routePoints, "Route shouldn't be null");
    assertFalse(routePoints.isEmpty(), "Route should consist of some points");
    for (final MaritimeShippingRoutePoint routePoint : routePoints) {
      assertTrue(routePoint.getX() > 10000 && routePoint.getX() < 10100, "Source X for routePoint:" + routePoint);
      assertTrue(routePoint.getX() > 10000 && routePoint.getX() < 10100, "Source Y for routePoint:" + routePoint);
      assertNotEquals(0.0, routePoint.getMeasure(), 1E-3, "Measure for routePoint > 0:" + routePoint);
      assertNotEquals(0.0, routePoint.getManeuverFactor(), 1E-3, "Maneuver factor for routePoint > 0:" + routePoint);
    }
  }

  @Test
  void testGetMaritimeShippingRoutePoints() throws SQLException, AeriusException {
    final Geometry route = constructLineString(new double[][] {{1000, 1000}, {1000, 2000}});
    final List<MaritimeShippingRoutePoint> routePoints = ShippingRepository.getMaritimeShippingRoutePoints(getConnection(), route, 25);
    assertNotNull(routePoints, "Route shouldn't be null");
    assertEquals(40, routePoints.size(), "Route should consist of specified number of points");
    for (final MaritimeShippingRoutePoint routePoint : routePoints) {
      assertEquals(1000, routePoint.getX(), 1E-3, "Source X for routePoint:" + routePoint);
      assertTrue(routePoint.getY() > 1000 && routePoint.getY() < 2000, "Source Y for routePoint:" + routePoint);
      assertNotEquals(0.0, routePoint.getMeasure(), 1E-3, "Measure for routePoint > 0:" + routePoint);
    }
  }

  @Test
  void testGetInlandShippingRoutePoints() throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints = ShippingRepository.getInlandShippingRoutePoints(getConnection(), TEST_INLAND_ROUTE);
    assertNotNull(routePoints, "Route shouldn't be null");
    assertEquals(40, routePoints.size(), "Route should consist of specified number of points");
    for (final InlandShippingRoutePoint routePoint : routePoints) {
      assertEquals(208000, routePoint.getX(), 1E-3, "Source X for routePoint:" + routePoint);
      assertTrue(routePoint.getY() > 455000 && routePoint.getY() < 456000, "Source Y for routePoint:" + routePoint);
      assertNotEquals(0.0, routePoint.getMeasure(), 1E-3, "Measure for routePoint > 0:" + routePoint);
      assertEquals(1.0, routePoint.getLockFactor(), 1E-3, "Lockfactor for routePoint:" + routePoint);
      assertNull(routePoint.getDirection(), "Direction for routePoint:" + routePoint);
      assertNull(routePoint.getWaterwayCategoryCode(), "Waterway type for routePoint:" + routePoint);
    }
  }

  @Test
  void testGetInlandShippingRoutePointsWithLocks() throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints = ShippingRepository.getInlandShippingRoutePoints(getConnection(), TEST_INLAND_LOCK_ROUTE);
    assertNotNull(routePoints, "Route shouldn't be null");
    assertEquals(84, routePoints.size(), "Route should consist of specified number of points");
    boolean foundLock = false;
    for (final InlandShippingRoutePoint routePoint : routePoints) {
      if (Math.abs(routePoint.getLockFactor() - 1.0) > 1E-3) {
        foundLock = true;
        break;
      }
    }
    assertTrue(foundLock, "Lock should be found in the route");
  }

  @Test
  void testGetInlandShippingRoutePointsWithWaterways() throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints = ShippingRepository.getInlandShippingRoutePointsWithWaterways(getConnection(),
        TEST_INLAND_ROUTE);
    assertNotNull(routePoints, "Route shouldn't be null");
    assertEquals(40, routePoints.size(), "Route should consist of specified number of points");
    for (final InlandShippingRoutePoint routePoint : routePoints) {
      assertEquals(208000, routePoint.getX(), 1E-3, "Source X for routePoint:" + routePoint);
      assertTrue(routePoint.getY() > 455000 && routePoint.getY() < 456000, "Source Y for routePoint:" + routePoint);
      assertNotEquals(0.0, routePoint.getMeasure(), 1E-3, "Measure for routePoint > 0:" + routePoint);
      assertEquals(1.0, routePoint.getLockFactor(), 1E-3, "Lockfactor for routePoint:" + routePoint);
      assertEquals(WaterwayDirection.DOWNSTREAM, routePoint.getDirection(), "Direction for routePoint:" + routePoint);
      assertEquals(TEST_INLAND_ROUTE_TYPE, routePoint.getWaterwayCategoryCode(), "Waterway type for routePoint:" + routePoint);
    }
  }

  @Test
  void testSuggestInlandShippingWaterway() throws SQLException, AeriusException {
    final List<InlandWaterway> iwt1 = ShippingRepository.suggestInlandShippingWaterway(getConnection(), TEST_INLAND_ROUTE);
    assertEquals(1, iwt1.size(), "Number of suggested waterways");
    assertEquals(TEST_INLAND_ROUTE_TYPE, iwt1.get(0).getWaterwayCode(), "Suggested waterway");
    final List<InlandWaterway> iwt2 = ShippingRepository.suggestInlandShippingWaterway(getConnection(), TEST_INLAND_LOCK_ROUTE);
    assertEquals(3, iwt2.size(), "Number of suggested waterways");
    assertEquals(TEST_INLAND_LOCK_ROUTE_TYPE, iwt2.get(0).getWaterwayCode(), "Suggested waterway");
  }

  @Test
  void testInvalidGeometries() throws SQLException, AeriusException {
    assertInvalidGeometry(new Point(1000, 1000));
    final Polygon polygon = new Polygon();
    polygon.setCoordinates(new double[][][] {{{1000, 1000}, {1001, 1000}, {1001, 1001}, {1000, 1001}, {1000, 1000}}});
    assertInvalidGeometry(polygon);
  }

  private void assertInvalidGeometry(final Geometry geometry) throws SQLException, AeriusException {
    try {
      ShippingRepository.getMaritimeMooringShippingInlandRoute(getConnection(), geometry, EXAMPLE_SHIP_CODE, 25, true, false);
    } catch (final AeriusException e) {
      assertRouteGeometryNotAllowed(e);
    }

    try {
      ShippingRepository.getMaritimeShippingRoutePoints(getConnection(), geometry, 25);
      failExpectedInvalidGeometryException(geometry);
    } catch (final AeriusException e) {
      assertRouteGeometryNotAllowed(e);
    }
    try {
      ShippingRepository.getInlandShippingRoutePointsWithWaterways(getConnection(), geometry);
      failExpectedInvalidGeometryException(geometry);
    } catch (final AeriusException e) {
      assertRouteGeometryNotAllowed(e);
    }
    try {
      ShippingRepository.getInlandShippingRoutePoints(getConnection(), geometry);
      failExpectedInvalidGeometryException(geometry);
    } catch (final AeriusException e) {
      assertRouteGeometryNotAllowed(e);
    }
    try {
      ShippingRepository.suggestInlandShippingWaterway(getConnection(), geometry);
      failExpectedInvalidGeometryException(geometry);
    } catch (final AeriusException e) {
      assertRouteGeometryNotAllowed(e);
    }
  }

  private void failExpectedInvalidGeometryException(final Geometry geometry) {
    fail("Expected exception due to use of invalid geometry:" + geometry);
  }

  private void assertRouteGeometryNotAllowed(final AeriusException e) {
    assertEquals(ImaerExceptionReason.SHIPPING_ROUTE_GEOMETRY_NOT_ALLOWED, e.getReason(), "Exception should be no valid geometry");
  }

  private static LineString constructLineString(final double[][] coords) {
    final LineString lineString = new LineString();
    lineString.setCoordinates(coords);
    return lineString;
  }

}
