/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.adms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.shared.domain.calculation.MetDatasetType;
import nl.overheid.aerius.shared.domain.calculation.MetSurfaceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;

/**
 * Test class for {@link MetSiteRepository}.
 */
@ExtendWith(MockitoExtension.class)
class MetSiteRepositoryTest {
  private static final float MIN_MONIN_OBUKHOV_LENGTH = (float) (ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT + 10.0);
  private static final int MET_SITE_ID = 123;

  @Mock private Connection connection;
  @Mock private PreparedStatement preparedStatement;
  @Mock private ResultSet resultSet;

  @Test
  void testGetSurfaceCharacteristics() throws SQLException {
    doReturn(preparedStatement).when(connection).prepareStatement(any());
    doReturn(resultSet).when(preparedStatement).executeQuery();
    doReturn(true).when(resultSet).next();
    doReturn(3.0F).when(resultSet).getFloat(eq("roughness"));
    doReturn(4.0F).when(resultSet).getFloat(eq("albedo"));
    doReturn(5.0F).when(resultSet).getFloat(eq("priestly_taylor"));
    doReturn(MIN_MONIN_OBUKHOV_LENGTH).when(resultSet).getFloat(eq("min_monin_obukhov_length"));
    doReturn(true).when(resultSet).getBoolean(eq("wind_in_sectors"));

    final Optional<MetSiteSurfaceCharacteristics> result =
        MetSiteRepository.getMetSiteSurfaceCharacteristics(connection, MET_SITE_ID, MetDatasetType.NWP_3KM2, "2024");

    assertTrue(result.isPresent(), "Should return data");
    final MetSurfaceCharacteristics characteristics = result.get().metSurfaceCharacteristics();

    assertEquals(3.0, characteristics.getRoughness(), "Roughness is not set correctly");
    assertEquals(4.0, characteristics.getSurfaceAlbedo(), "Albedo is not set correctly");
    assertEquals(5.0, characteristics.getPriestleyTaylorParameter(), "PriestleyTaylor is not set correctly");
    assertEquals(MIN_MONIN_OBUKHOV_LENGTH, characteristics.getMinMoninObukhovLength(), "MinMoninObukhovLength is not set correctly");
    assertTrue(characteristics.isWindInSectors(), "Wind in sectors is not set correctly");
  }
}
