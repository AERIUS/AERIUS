/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator.calculationpoint;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

class DeterminedCalculationPointsCollectorTest {

  @Test
  void testSinglePoint() {
    final DeterminedCalculationPointsCollector collector = new DeterminedCalculationPointsCollector();
    collector.add(1, 1, 1, 1000, "Label 1");
    final List<AeriusPoint> points = collector.getPoints();
    assertEquals(1, points.size());
    assertEquals("Label 1 (1 km)", points.get(0).getLabel());
  }

  @Test
  void testMergedPoint() {
    final DeterminedCalculationPointsCollector collector = new DeterminedCalculationPointsCollector();
    collector.add(1, 1, 1, 1000, "Label 1");
    collector.add(1, 1, 2, 1000, "Label 2");
    final List<AeriusPoint> points = collector.getPoints();
    assertEquals(1, points.size());
    assertEquals("Label 1 & Label 2 (1 km)", points.get(0).getLabel());
  }

  @Test
  void testSortedPoint() {
    final DeterminedCalculationPointsCollector collector = new DeterminedCalculationPointsCollector();
    // area 1
    collector.add(1, 1, 1, 1000, "Label 1-1");
    collector.add(2, 2, 1, 8000, "Label 1-2");
    collector.add(3, 3, 1, 12000, "Label 1-3");
    // area 2
    collector.add(4, 4, 2, 800, "Label 2-1");
    collector.add(2, 2, 2, 8000, "Label 2-2");
    collector.add(5, 5, 2, 15000, "Label 2-3");
    final List<AeriusPoint> points = collector.getPoints();
    assertEquals(5, points.size());
    assertEquals("Label 2-1 (<1 km)", points.get(0).getLabel());
    assertEquals("Label 1-2 & Label 2-2 (8 km)", points.get(1).getLabel());
    assertEquals("Label 2-3 (15 km)", points.get(2).getLabel());
    assertEquals("Label 1-1 (1 km)", points.get(3).getLabel());
    assertEquals("Label 1-3 (12 km)", points.get(4).getLabel());
  }

}
