/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.common.results.ResultsSummaryRepository;
import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultSetType;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.info.ScenarioEmissionResults;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Needs a proper database to test against, testing DB operations for calculations.
 */
class CalculationInfoRepositoryTest extends CalculationRepositoryTestBase {

  private static final Substance TEST_SUBSTANCE_2 = Substance.NH3;

  @Test
  void testGetCalculationResults() throws SQLException {
    // add some custom point results as well, to test that we don't retrieve them along with the other results.
    getTestCustomPointResults();
    final List<AeriusResultPoint> referenceResult = getTestReferenceResults();

    final List<CalculationPointFeature> emptyResult = CalculationInfoRepository.getCalculationResults(getConnection(), -1);
    assertNotNull(emptyResult, "Calculation result");
    assertEquals(0, emptyResult.size(), "Number of receptor points in empty calculation result");

    final List<CalculationPointFeature> results = CalculationInfoRepository.getCalculationResults(getConnection(),
        calculation.getCalculationId());
    assertNotNull(results, "Calculation result");
    assertEquals(referenceResult.size(), results.size(), "Number of result points");

    matchCalculationResults(referenceResult, results);
  }

  @Test
  void testGetCustomPointsCalculationResults() throws SQLException {
    final List<AeriusResultPoint> customResults = getTestCustomPointResults();
    // add some receptor results as well, to test that we don't retrieve them along with the custom results.
    getTestReferenceResults();

    final Map<Integer, Map<EmissionResultKey, Double>> emptyResult = CalculationInfoRepository.getCustomPointsCalculationResults(
        getConnection(), -1);
    assertNotNull(emptyResult, "Calculation result");
    assertEquals(0, emptyResult.size(), "Number of custom points in empty calculation result");

    final Map<Integer, Map<EmissionResultKey, Double>> results = CalculationInfoRepository.getCustomPointsCalculationResults(
        getConnection(), calculation.getCalculationId());
    assertEquals(customResults.size(), results.size(), "Number of result points");

    matchCalculationResults(customResults, results);
  }

  @Test
  void testGetCalculationSectorResults() throws SQLException {
    // add some custom point results as well, to test that we don't retrieve them along with the other results.
    getTestCustomPointResults();
    final List<AeriusResultPoint> referenceResult = getTestReferenceResults();

    final List<CalculationPointFeature> emptyResult = CalculationInfoRepository.getCalculationSectorResults(getConnection(), -1,
        TEST_CALCULATION_RESULTS_SECTOR_ID);
    assertNotNull(emptyResult, "Calculation result");
    assertEquals(0, emptyResult.size(), "Number of receptor points in empty calculation result");

    final List<CalculationPointFeature> partialResult = CalculationInfoRepository.getCalculationSectorResults(getConnection(),
        calculation.getCalculationId(), TEST_CALCULATION_RESULTS_SECTOR_ID);
    assertNotNull(partialResult, "Calculation result");
    assertEquals(referenceResult.size(), partialResult.size(), "Number of result points");

    matchCalculationResults(referenceResult, partialResult);
  }

  @Test
  void testAddCustomPointsCalculationSectorResults() throws SQLException {
    final List<AeriusResultPoint> customResults = getTestCustomPointResults();
    // add some receptor results as well, to test that we don't retrieve them along with the custom results.
    getTestReferenceResults();

    final Map<Integer, Map<EmissionResultKey, Double>> emptyResult = CalculationInfoRepository.getCustomPointsCalculationSectorResults(
        getConnection(), -1, TEST_CALCULATION_RESULTS_SECTOR_ID);
    assertNotNull(emptyResult, "Calculation result");
    assertEquals(0, emptyResult.size(), "Number of custom points in empty calculation result");

    final Map<Integer, Map<EmissionResultKey, Double>> results = CalculationInfoRepository.getCustomPointsCalculationSectorResults(
        getConnection(), calculation.getCalculationId(), TEST_CALCULATION_RESULTS_SECTOR_ID);
    assertEquals(customResults.size(), results.size(), "Number of result points");

    matchCalculationResults(customResults, results);
  }

  private List<AeriusResultPoint> getTestCustomPointResults() throws SQLException {
    final int calculationPointSetId = insertCalculationPoints();
    //trick to set the right calculation point set id:
    //The calc id returned from insertCalculationPoints is actually the calculation point set ID...
    removeCalculationResults();
    createCalculation(calculationPointSetId);
    final List<CalculationPointFeature> calculationPointList = getExampleCalculationPointOutputData();
    final List<AeriusResultPoint> results = toAeriusResultPoints(TEST_SUBSTANCE_2, calculationPointList);
    CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(), results);
    CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(), CalculationResultSetType.SECTOR,
        TEST_CALCULATION_RESULTS_SECTOR_ID, results);
    return results;
  }

  private List<AeriusResultPoint> getTestReferenceResults() throws SQLException {
    insertCalculationResults();
    return getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
  }

  private void matchCalculationResults(final List<AeriusResultPoint> referenceResult, final List<CalculationPointFeature> calculationResult) {
    int matched = 0;
    for (final CalculationPointFeature point : calculationResult) {
      for (final AeriusResultPoint referencePoint : referenceResult) {
        if (referencePoint.getId() == point.getProperties().getId()) {
          matched++;
          assertNotEquals(AeriusPointType.POINT, referencePoint.getPointType(), "Should not get custom point results here");
          assertEquals(referencePoint.getEmissionResult(EmissionResultKey.NH3_CONCENTRATION),
              point.getProperties().getResults().get(EmissionResultKey.NH3_CONCENTRATION),
              1E-3, "Concentration");
          assertEquals(referencePoint.getEmissionResult(EmissionResultKey.NH3_DEPOSITION),
              point.getProperties().getResults().get(EmissionResultKey.NH3_DEPOSITION),
              1E-3, "Deposition");
        }
      }
    }
    assertEquals(referenceResult.size(), matched, "Number of matched points");
  }

  private void matchCalculationResults(final List<AeriusResultPoint> referenceResult, final Map<Integer, Map<EmissionResultKey, Double>> results) {
    int matched = 0;
    for (final Entry<Integer, Map<EmissionResultKey, Double>> entry : results.entrySet()) {
      for (final AeriusResultPoint referencePoint : referenceResult) {
        if (referencePoint.getId() == entry.getKey()) {
          matched++;
          assertEquals(referencePoint.getEmissionResult(EmissionResultKey.NH3_CONCENTRATION),
              entry.getValue().get(EmissionResultKey.NH3_CONCENTRATION),
              1E-3, "Concentration");
          assertEquals(referencePoint.getEmissionResult(EmissionResultKey.NH3_DEPOSITION),
              entry.getValue().get(EmissionResultKey.NH3_DEPOSITION),
              1E-3, "Deposition");
        }
      }
    }
    assertEquals(referenceResult.size(), matched, "Number of matched points");
  }

  @Test
  void testGetEmissionResults() throws SQLException {
    insertCalculationResults();

    final EmissionResults noEmissionResults = CalculationInfoRepository.getEmissionResults(getConnection(),
        calculation.getCalculationId(), -1);
    assertTrue(noEmissionResults.isEmpty(), "No emission results for non-existing receptor ID");
    final EmissionResults emissionResults = CalculationInfoRepository.getEmissionResults(getConnection(),
        calculation.getCalculationId(), RECEPTOR_POINT_ID_1);
    assertEquals(RECEPTOR_1_DEPOSITION, emissionResults.get(EmissionResultKey.NH3_DEPOSITION), 0.00001, "Emission results");
  }

  @Test
  void testGetScenarioResults() throws SQLException, AeriusException {
    final String jobKey = JobRepository.createJob(getConnection(), null, JobType.CALCULATION, false);
    final int jobId = JobRepository.getJobId(getConnection(), jobKey);

    insertCalculationResults();

    calculation.getSituation().setType(SituationType.PROPOSED);
    JobRepository.attachCalculations(getConnection(), jobKey, List.of(calculation));

    final Map<ScenarioResultType, ScenarioEmissionResults> beforeAggregating = CalculationInfoRepository.getScenarioResults(getConnection(),
        jobKey, RECEPTOR_POINT_ID_1);
    assertTrue(beforeAggregating.isEmpty(), "No scenario results before result summaries are inserted");

    final SituationCalculations situationCalculations = JobRepository.getSituationCalculations(getConnection(), jobKey);
    new ResultsSummaryRepository(getPMF()).insertResultsSummaries(jobId, situationCalculations, CalculationJobType.PROCESS_CONTRIBUTION);

    final Map<ScenarioResultType, ScenarioEmissionResults> scenarioResults = CalculationInfoRepository.getScenarioResults(getConnection(),
        jobKey, RECEPTOR_POINT_ID_1);
    assertNotNull(scenarioResults.get(ScenarioResultType.PROJECT_CALCULATION), "Should have a project calculation result now.");
    assertEquals(RECEPTOR_1_DEPOSITION,
        scenarioResults.get(ScenarioResultType.PROJECT_CALCULATION).getResults().get(EmissionResultKey.NOXNH3_DEPOSITION), 0.00001,
        "Scenario results");
    // Assuming background deposition is > 1 (which usually is the case for known receptors), to avoid rounding artifacts in test causing it to succeed.
    assertTrue(
        scenarioResults.get(ScenarioResultType.PROJECT_CALCULATION).getTotalResults().get(EmissionResultKey.NOXNH3_DEPOSITION) > RECEPTOR_1_DEPOSITION
            + 1,
        "Scenario total results should be higher than the scenario result as it includes background");

    final Map<ScenarioResultType, ScenarioEmissionResults> nonExistingReceptorResult = CalculationInfoRepository.getScenarioResults(getConnection(),
        jobKey, -1);
    assertTrue(nonExistingReceptorResult.isEmpty(), "No scenario results for non-existing receptor ID");
  }

  @Test
  void testGetArchivecontribution() throws SQLException {
    insertArchiveContribution(calculation.getCalculationId());

    final List<CalculationPointFeature> emptyResult = CalculationInfoRepository.getArchiveContributions(getConnection(), -1);
    assertNotNull(emptyResult, "Calculation result");
    assertEquals(0, emptyResult.size(), "Number of receptor points in empty calculation result");

    final List<CalculationPointFeature> results = CalculationInfoRepository.getArchiveContributions(getConnection(),
        calculation.getCalculationId());
    assertNotNull(results, "Calculation result");
    assertEquals(5, results.size(), "Number of result points");
  }

}
