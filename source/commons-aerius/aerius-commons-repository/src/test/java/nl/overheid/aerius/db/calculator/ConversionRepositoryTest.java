/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.postgis.LineString;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Needs a proper database to test against, testing DB operations for conversions.
 */
class ConversionRepositoryTest extends BaseDBTest {

  @Test
  void testConvertToPoints() throws SQLException {
    final int numberOfCoordinates = 5;
    final ArrayList<org.postgis.Point> linePoints = new ArrayList<>();
    //create linestring with (0,0), (0,4000), (0,8000), (0,12000), (0,16000)
    for (int i = 0; i < numberOfCoordinates; i++) {
      final org.postgis.Point linePoint = new org.postgis.Point(0 * i, 4000 * i);
      linePoints.add(linePoint);
    }
    final LineString lineString = new LineString(linePoints.toArray(new org.postgis.Point[linePoints.size()]));
    List<Point> convertedPoints = new ArrayList<>();
    try (final Connection connection = getConnection()) {
      final Double maxSegementSize = 76.4;
      convertedPoints = ConversionRepository.convertToPoints(connection, lineString, maxSegementSize);
      assertEquals(new BigDecimal((numberOfCoordinates - 1) * 4000).divide(new BigDecimal(maxSegementSize), 0,
          RoundingMode.UP).intValue(),
          convertedPoints.size(),
          "Number of segments");
    }
    final double segmentSize = new BigDecimal((numberOfCoordinates - 1) * 4000).divide(new BigDecimal(convertedPoints.size()), 4,
        RoundingMode.HALF_UP).doubleValue();
    for (int i = 0; i < convertedPoints.size(); i++) {
      final Point convertedPoint = convertedPoints.get(i);
      assertEquals(0, convertedPoint.getX(), 1E-2, "X-coord of point " + i);
      assertEquals(i * segmentSize + segmentSize / 2.0, convertedPoint.getY(), 1E-2, "Y-coord of point " + i);
    }
  }

  @Test
  void testValidateConvertToPoints() throws SQLException, AeriusException {
    //check if the DB version of line -> points gives us the same results as the java version.
    //Try some random linestring.
    final nl.overheid.aerius.shared.domain.v2.geojson.LineString aeriusLineString = new nl.overheid.aerius.shared.domain.v2.geojson.LineString();
    final double[][] coordinates = new double[][] {{231, 12311}, {1155, 382424}, {122342, 2349}};
    aeriusLineString.setCoordinates(coordinates);
    final LineString lineString = new LineString(
        Stream.of(coordinates).map(c -> new org.postgis.Point(c[0], c[1])).collect(Collectors.toList()).toArray(new org.postgis.Point[0]));
    List<Point> convertedPointsDB = new ArrayList<>();
    final Double maxSegmentSize = 76.4;
    try (final Connection connection = getConnection()) {
      convertedPointsDB = ConversionRepository.convertToPoints(connection, lineString, maxSegmentSize);
    }
    final List<Point> convertedPoints = GeometryUtil.convertToPoints(aeriusLineString, maxSegmentSize);
    assertEquals(convertedPoints.size(), convertedPointsDB.size(), "Lists should have same size.");
    for (int i = 0; i < convertedPoints.size(); i++) {
      final Point convertedPoint = convertedPoints.get(i);
      final Point convertedPointDB = convertedPointsDB.get(i);
      assertEquals(convertedPoint.getX(), convertedPointDB.getX(), 1E-5, "X-coord of point " + i);
      assertEquals(convertedPoint.getY(), convertedPointDB.getY(), 1E-5, "Y-coord of point " + i);
    }
  }
}
