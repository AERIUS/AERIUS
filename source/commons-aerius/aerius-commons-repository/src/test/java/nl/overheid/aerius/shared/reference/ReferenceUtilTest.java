/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.reference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.Instant;

import org.junit.jupiter.api.Test;

/**
 * Unit test for {@link ReferenceUtil}.
 */
class ReferenceUtilTest extends ReferenceGeneratorV2Test {

  @Override
  protected ReferenceGeneratorV2 createReferenceGeneratorV2() {
    return (ReferenceGeneratorV2) ReferenceGeneratorFactory.createCurrentReferenceGenerator();
  }

  @Override
  protected String generatePermitReference() {
    return ReferenceUtil.generatePermitReference();
  }

  @Override
  protected String generateMeldingReference() {
    return ReferenceUtil.generateMeldingReference();
  }

  @Override
  protected boolean validatePermitReference(final String reference) {
    return ReferenceUtil.validatePermitReference(reference);
  }

  @Override
  protected boolean validateMeldingReference(final String reference) {
    return ReferenceUtil.validateMeldingReference(reference);
  }

  @Override
  protected ReferenceType getReferenceType(final String reference) throws Exception {
    return createReferenceGeneratorV2().getReferenceType(reference);
  }

  @Override
  protected byte getVersion(final String reference) throws Exception {
    return ReferenceUtil.getVersion(reference);
  }

  @Test
  void testBackwardsCompatibilityV1() throws Exception {
    String reference = "12zx4wHDk9";
    assertEquals(ReferenceGeneratorV1.VERSION_ID, getVersion(reference), reference + " is not detected as V1");
    assertTrue(validateMeldingReference(reference), reference + " (V1) not correct");

    reference = "2EQrgWQBNh";
    assertEquals(ReferenceGeneratorV1.VERSION_ID, getVersion(reference), reference + " is not detected as V1");
    assertTrue(validatePermitReference(reference), reference + " (V1) not correct");
  }

  /*
   * Unit test that can be used to quickly check when a reference was generated.
   * Just put in a reference to test, run the test and check the fail message for the guestimation of when it was generated.
   */
  @Test
  void testDetermineDateTime() throws InvalidReferenceException {
    final String referenceToTest = "RxpvADtDHSTf";
    assertTrue(ReferenceUtil.validateAnyReference(referenceToTest), "Reference wasn't valid");
    final byte version = ReferenceUtil.getVersion(referenceToTest);
    if (version == ReferenceGeneratorV1.VERSION_ID || version == ReferenceGeneratorV2.VERSION_ID) {
      final Instant decodedInstant = createReferenceGeneratorV2().getGeneratedTime(referenceToTest);
      assertEquals(Instant.parse("2015-12-15T08:43:58.700Z"), decodedInstant, "Decoded time");
    } else {
      fail("unexpected version: " + version);
    }
  }
}
