/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.decision;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.AssessmentAreaThresholdResults;
import nl.overheid.aerius.shared.domain.summary.ThresholdResult;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Tests for {@link DecisionFrameworkRepository}.
 * Note: Database used in testing does not have thresholds, so can only do a check if queries work.
 */
class DecisionFrameworkRepositoryTest extends CalculationRepositoryTestBase implements AreaInformationSupplier {

  @Test
  void testGetDecisionMakingThresholdResults() throws SQLException, AeriusException {
    final DecisionFrameworkRepository repository = new DecisionFrameworkRepository(getPMF(), this);
    final int jobId = createJob();

    final Map<EmissionResultKey, ThresholdResult> result = repository.getDecisionMakingThresholdResults(jobId, calculation.getCalculationId());
    assertNotNull(result, "Result shouldn't be null");
  }

  @Test
  void testIsAboveDecisionMakingThreshold() throws SQLException, AeriusException {
    final DecisionFrameworkRepository repository = new DecisionFrameworkRepository(getPMF(), this);
    final int jobId = createJob();

    final boolean result = repository.isAboveDecisionMakingThreshold(jobId, calculation.getCalculationId());
    assertFalse(result, "No threshold, so shouldn't be above it");
  }

  @Test
  void testGetAssessmentAreaThresholdResults() throws SQLException, AeriusException {
    final DecisionFrameworkRepository repository = new DecisionFrameworkRepository(getPMF(), this);
    final int jobId = createJob();

    final List<AssessmentAreaThresholdResults> result = repository.getAssessmentAreaThresholdResults(jobId, calculation.getCalculationId());
    assertNotNull(result, "Result shouldn't be null");
  }

  @Test
  void testIsAboveAssessmentAreaThreshold() throws SQLException, AeriusException {
    final DecisionFrameworkRepository repository = new DecisionFrameworkRepository(getPMF(), this);
    final int jobId = createJob();

    final boolean result = repository.isAboveAssessmentAreaThreshold(jobId, calculation.getCalculationId());
    assertFalse(result, "No threshold, so shouldn't be above it");
  }

  @Disabled("Only works with UK classifications, normal unittest database does not have any classifications.")
  @Test
  void testInsertDevelopmentPressureSearchResult() throws SQLException, AeriusException {
    final DecisionFrameworkRepository repository = new DecisionFrameworkRepository(getPMF(), this);
    final int jobId = createJob();

    assertNull(repository.getDevelopmentPressureClass(jobId), "Nothing inserted, nothing expected");

    repository.insertDevelopmentPressureSearchResult(jobId, 0);

    assertEquals("VERY_LOW", repository.getDevelopmentPressureClass(jobId).getCode(), "With 0 nearby sources classification");

    // Should be possible to upsert
    repository.insertDevelopmentPressureSearchResult(jobId, 2);

    assertEquals("LOW", repository.getDevelopmentPressureClass(jobId).getCode(), "With 2 nearby sources classification");

    repository.insertDevelopmentPressureSearchResult(jobId, 1000);

    assertEquals("HIGH", repository.getDevelopmentPressureClass(jobId).getCode(), "With a lot of nearby sources classification");

    repository.insertDevelopmentPressureClass(jobId, "LOW");

    assertEquals("LOW", repository.getDevelopmentPressureClass(jobId).getCode(), "When inserted by code");
  }

  @Test
  void testGetDevelopmentPressureClass() throws SQLException, AeriusException {
    final DecisionFrameworkRepository repository = new DecisionFrameworkRepository(getPMF(), this);
    final int jobId = createJob();

    assertNull(repository.getDevelopmentPressureClass(jobId), "Nothing inserted, nothing expected. Query should work though.");
  }

  private int createJob() throws SQLException, AeriusException {
    final String correlationIdentifier = JobRepository.createJob(getConnection(), JobType.CALCULATION, false);
    JobRepository.attachCalculations(getConnection(), correlationIdentifier, List.of(calculation));
    return JobRepository.getJobId(getConnection(), correlationIdentifier);
  }

  @Override
  public AssessmentArea determineAssessmentArea(final int assessmentAreaId) {
    final AssessmentArea fakeArea = new AssessmentArea();
    fakeArea.setId(assessmentAreaId);
    fakeArea.setName("Gebied " + assessmentAreaId);
    fakeArea.setMarkerLocation(new Point(1, 1));
    return fakeArea;
  }

  @Override
  public HabitatType determineHabitatType(final int habitatTypeId) {
    return null;
  }

  @Override
  public boolean determineHabitatTypeSensitiveness(final int habitatTypeId, final EmissionResultKey emissionResultKey) {
    return false;
  }

  @Override
  public Double determineHabitatCriticalLevel(final int habitatTypeId, final EmissionResultKey emissionResultKey) {
    return null;
  }

}
