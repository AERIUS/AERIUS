/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.common.results.ProjectCalculationRepository.CalculationResultsType;
import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculation;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.ReceptorResult;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 *
 */
class ProjectCalculationRepositoryTest extends CalculationRepositoryTestBase {

  /**
   * With this scale, 2 results are expected: RECEPTOR_POINT_ID_1 and RECEPTOR_POINT_ID_2.
   */
  private static final double RESULT_SCALE_NORMAL = 1.0;

  /**
   * With this scale, 1 results is expected: RECEPTOR_POINT_ID_1.
   * Deposition result of RECEPTOR_POINT_ID_1 is 0.05, so with this scale that becomes 0.005
   * As threshold is 0.0049995, this means it's still returned.
   * Other receptor has lower results (0.0049 after scaling), so is filtered out.
   */
  private static final double RESULT_SCALE_THRESHOLD = 0.1;

  @Test
  void testGetProjectResults() throws SQLException, AeriusException {
    final ProjectCalculationRepository repository = new ProjectCalculationRepository(getPMF());

    insertCalculationResultsNormal();

    createJob(SituationType.PROPOSED);

    final List<ReceptorResult> retrievedResults =
        repository.getCalculationResults(CalculationResultsType.PROJECT, calculation.getCalculationId(), Substance.NH3,
            SummaryHexagonType.RELEVANT_HEXAGONS, OverlappingHexagonType.ALL_HEXAGONS);

    assertEquals(2, retrievedResults.size(), "Number of results");
  }

  @Test
  void testGetProjectResultsUnderThreshold() throws SQLException, AeriusException {
    final ProjectCalculationRepository repository = new ProjectCalculationRepository(getPMF());

    insertCalculationResultsThreshold();

    createJob(SituationType.PROPOSED);

    final List<ReceptorResult> retrievedResults =
        repository.getCalculationResults(CalculationResultsType.PROJECT, calculation.getCalculationId(), Substance.NH3,
            SummaryHexagonType.RELEVANT_HEXAGONS, OverlappingHexagonType.ALL_HEXAGONS);

    assertEquals(1, retrievedResults.size(), "Number of results");
  }

  @Test
  void testGetSituationResults() throws SQLException, AeriusException {
    final ProjectCalculationRepository repository = new ProjectCalculationRepository(getPMF());

    insertCalculationResultsNormal();

    final List<ReceptorResult> retrievedResults =
        repository.getCalculationResults(CalculationResultsType.SITUATION, calculation.getCalculationId(), Substance.NH3,
            SummaryHexagonType.RELEVANT_HEXAGONS, OverlappingHexagonType.ALL_HEXAGONS);

    assertEquals(2, retrievedResults.size(), "Number of results");
  }

  @Test
  void testGetSituationResultsUnderThreshold() throws SQLException, AeriusException {
    final ProjectCalculationRepository repository = new ProjectCalculationRepository(getPMF());

    insertCalculationResultsThreshold();

    final List<ReceptorResult> retrievedResults =
        repository.getCalculationResults(CalculationResultsType.SITUATION, calculation.getCalculationId(), Substance.NH3,
            SummaryHexagonType.RELEVANT_HEXAGONS, OverlappingHexagonType.ALL_HEXAGONS);

    assertEquals(1, retrievedResults.size(), "Number of results");
  }

  private void insertCalculationResultsNormal() throws SQLException {
    insertCalculationResults(RESULT_SCALE_NORMAL);
  }

  private void insertCalculationResultsThreshold() throws SQLException {
    insertCalculationResults(RESULT_SCALE_THRESHOLD);
  }


  private void insertCalculationResults(final double scale) throws SQLException {
    final List<AeriusResultPoint> result = new ArrayList<>();

    calculationResultScale = scale;

    result.add(createResult(RECEPTOR_POINT_ID_1, EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION,
        100, 0.05, 100));
    result.add(createResult(RECEPTOR_POINT_ID_2, EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION,
        100, 0.049, 100));

    // 2 receptors * 2 result types = 4 results.
    // However, since NH3_DEPOSITION is included, NOXNH3_DEPOSITION will be included as well, so 2 * 3
    insertResults(result, 6);
  }

  private void createJob(final SituationType situationType) throws SQLException, AeriusException {
    final String correlationIdentifier = JobRepository.createJob(getConnection(), JobType.CALCULATION, false);
    JobRepository.attachCalculations(getConnection(), correlationIdentifier, List.of(calculation));

    final int jobId = JobRepository.getJobId(getConnection(), correlationIdentifier);

    final SituationCalculations situationCalculations = new SituationCalculations();
    final SituationCalculation proposedCalculation = new SituationCalculation();
    proposedCalculation.setCalculationId(calculation.getCalculationId());
    proposedCalculation.setSituationType(situationType);
    situationCalculations.add(proposedCalculation);

    final ResultsSummaryRepository resultsRepository = new ResultsSummaryRepository(getPMF(), new ReceptorUtil(RECEPTOR_GRID_SETTINGS));
    resultsRepository.insertResultsSummaries(jobId, situationCalculations, CalculationJobType.PROCESS_CONTRIBUTION);
  }

}
