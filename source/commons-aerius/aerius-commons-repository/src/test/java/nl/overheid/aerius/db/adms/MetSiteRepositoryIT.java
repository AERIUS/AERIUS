/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.adms;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.calculation.MetDatasetType;

/**
 * Integration test class for MetSiteRepository
 */
class MetSiteRepositoryIT extends BaseDBTest {

  @Test
  void testGetMetSites() throws SQLException {
    final List<MetSite> metSites = MetSiteRepository.getMetSites(getConnection());

    assertNotNull(metSites, "Returned list should not be null");
    // Nothing more to test at this point: testing against NL database, and that doesn't have content
    // Just testing if queries work.
  }

  @Test
  void testGetMetSiteSurfaceCharacteristics() throws SQLException {
    final Optional<MetSiteSurfaceCharacteristics> characteristics =
        MetSiteRepository.getMetSiteSurfaceCharacteristics(getConnection(), 1, MetDatasetType.NWP_3KM2, "2020");

    assertNotNull(characteristics, "Returned optional should not be null");
    assertTrue(characteristics.isEmpty(), "It should be empty, as test database has no content");
    // Nothing more to test at this point: testing against NL database, and that doesn't have content
    // Just testing if queries work.
  }

}
