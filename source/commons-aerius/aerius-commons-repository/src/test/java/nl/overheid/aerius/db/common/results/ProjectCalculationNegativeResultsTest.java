/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticsMarker;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsAreaSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsHabitatSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.summary.SummaryRequest;
import nl.overheid.aerius.shared.domain.summary.SurfaceChartResults;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Tests against a PROPOSED situation which is smaller than the REFERENCE situation.
 * This should yield in negative results.
 */
class ProjectCalculationNegativeResultsTest extends ResultsTestBase {

  @BeforeEach
  public void prepareTests() throws AeriusException, SQLException {
    super.prepareTests(CalculationJobType.PROCESS_CONTRIBUTION);
  }

  @Test
  void testDetermineReceptorResultsSummary() throws AeriusException {
    final SummaryRequest summaryRequest = toRequest(SummaryHexagonType.RELEVANT_HEXAGONS);
    final SituationResultsSummary results = resultsRepository.determineReceptorResultsSummary(situationCalculations, summaryRequest,
        this);

    validateReceptorResults(results);
  }

  @Override
  protected void validateOverall(final SituationResultsStatistics statistics) {
    final SituationResultsStatistics expected = new SituationResultsStatistics();
    addExpectedOverallStats(expected, false);
    expected.put(ResultStatisticType.MAX_TOTAL, 7403.3877);
    expected.put(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL, -0.002);
    expected.put(ResultStatisticType.MAX_INCREASE, null);
    expected.put(ResultStatisticType.MAX_DECREASE, 117.115);
    assertStatistics(expected, statistics);
  }

  @Override
  protected void validateAreaStatistics(final List<SituationResultsAreaSummary> areaStatistics) {
    assertAreas(List.of(ASSESSMENT_AREA_1_NAME, ASSESSMENT_AREA_4_NAME, ASSESSMENT_AREA_2_NAME), areaStatistics);

    final SituationResultsAreaSummary areaSummary = areaStatistics.get(0);
    final SituationResultsStatistics expected = veluweStats(6000.0);
    assertStatistics(expected, areaSummary.getStatistics());
  }

  @Override
  protected void validateHabitatStatistics(final List<SituationResultsHabitatSummary> habitatSummaries) {
    assertEquals(1, habitatSummaries.size(), "Number of habitats with results");

    final SituationResultsHabitatSummary habitatSummary = habitatSummaries.get(0);
    assertDrogeHeiden(habitatSummary);
    final SituationResultsStatistics expected = veluweStats(6000.0);
    expected.put(ResultStatisticType.MAX_BACKGROUND, 4776.053);
    assertStatistics(expected, habitatSummary.getStatistics());
  }

  private SituationResultsStatistics veluweStats(final double area) {
    final SituationResultsStatistics expected = new SituationResultsStatistics();
    addExpectedAreaStats(expected, area, false);
    expected.put(ResultStatisticType.MAX_TOTAL, 4658.938);
    expected.put(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL, -16.40266);
    expected.put(ResultStatisticType.MAX_TOTAL_PERCENTAGE_CRITICAL_LEVEL, 652.5123);
    expected.put(ResultStatisticType.MAX_INCREASE, null);
    expected.put(ResultStatisticType.MAX_DECREASE, 117.115);
    return expected;
  }

  @Override
  protected void validateChartStatistics(final List<SurfaceChartResults> chartResults) {
    assertChartStatistics(12, 0, Double.NEGATIVE_INFINITY, 6000.0, chartResults);
  }

  @Override
  protected void validateStatisticMarkers(final List<ResultStatisticsMarker> markers) {
    assertStatisticMarkers(List.of(RECEPTOR_POINT_ID_6, RECEPTOR_POINT_ID_1, RECEPTOR_POINT_ID_2),
        Set.of(ResultStatisticType.MAX_TOTAL, ResultStatisticType.MAX_DECREASE, ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL),
        markers);
  }

  @Test
  void testDetermineCustomCalculationPointResultsSummary() throws AeriusException {
    final SummaryRequest summaryRequest = toRequest(SummaryHexagonType.CUSTOM_CALCULATION_POINTS);
    final SituationResultsSummary results = resultsRepository.determineCustomCalculationPointResultsSummary(situationCalculations,
        summaryRequest);

    validateCustomCalculationPoints(results, -234.23, -1.23, false);
  }

  @Test
  void testDetermineExtraAssessmentResultsSummary() throws AeriusException {
    final SummaryRequest summaryRequest = toRequest(SummaryHexagonType.EXTRA_ASSESSMENT_HEXAGONS);
    final SituationResultsSummary results = resultsRepository.determineReceptorResultsSummary(situationCalculations, summaryRequest,
        this);

    validateReceptorResultsExtraAssessment(results);
  }

  @Override
  protected void validateOverallExtraAssessment(final SituationResultsStatistics statistics) {
    final SituationResultsStatistics expected = new SituationResultsStatistics();
    addExpectedStatsExtraAssessment(expected, false);
    expected.put(ResultStatisticType.MAX_TOTAL, 4278.997);
    expected.put(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL, -0.006);
    expected.put(ResultStatisticType.MAX_INCREASE, null);
    expected.put(ResultStatisticType.MAX_DECREASE, 0.03);
    assertStatistics(expected, statistics);
  }

  @Override
  protected void validateAreaStatisticsExtraAssessment(final List<SituationResultsAreaSummary> areaStatistics) {
    assertAreasExtraAssessment(areaStatistics);

    final SituationResultsAreaSummary areaSummary = areaStatistics.get(0);
    final SituationResultsStatistics expected = binnenveldStatsExtraAssessment();
    assertStatistics(expected, areaSummary.getStatistics());
  }

  @Override
  protected void validateHabitatStatisticsExtraAssessment(final List<SituationResultsHabitatSummary> habitatSummaries) {
    assertHabitatsExtraAssessment(habitatSummaries);

    final SituationResultsHabitatSummary habitatSummary = habitatSummaries.get(0);
    final SituationResultsStatistics expected = binnenveldStatsExtraAssessment();
    expected.put(ResultStatisticType.MAX_BACKGROUND, 4279.027);
    assertStatistics(expected, habitatSummary.getStatistics());
  }

  private SituationResultsStatistics binnenveldStatsExtraAssessment() {
    final SituationResultsStatistics expected = new SituationResultsStatistics();
    addExpectedStatsExtraAssessment(expected, false);
    expected.put(ResultStatisticType.MAX_TOTAL, 4278.997);
    expected.put(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL, -0.006);
    expected.put(ResultStatisticType.MAX_TOTAL_PERCENTAGE_CRITICAL_LEVEL, 855.7994);
    expected.put(ResultStatisticType.MAX_INCREASE, null);
    expected.put(ResultStatisticType.MAX_DECREASE, 0.03);
    return expected;
  }

  @Override
  protected void validateChartStatisticsExtraAssessment(final List<SurfaceChartResults> chartResults) {
    assertChartStatistics(12, 5, -1.07, 1.0, chartResults);
  }

  @Override
  protected void validateStatisticMarkersExtraAssessment(final List<ResultStatisticsMarker> markers) {
    assertStatisticMarkers(List.of(RECEPTOR_POINT_ID_7),
        Set.of(ResultStatisticType.MAX_TOTAL, ResultStatisticType.MAX_DECREASE, ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL),
        markers);
  }

  private SummaryRequest toRequest(final SummaryHexagonType hexagontype) {
    return new SummaryRequest(ScenarioResultType.PROJECT_CALCULATION, jobId, calculationIdSmallProposed,
        hexagontype, OverlappingHexagonType.ALL_HEXAGONS, EmissionResultKey.NOXNH3_DEPOSITION, null);
  }

}
