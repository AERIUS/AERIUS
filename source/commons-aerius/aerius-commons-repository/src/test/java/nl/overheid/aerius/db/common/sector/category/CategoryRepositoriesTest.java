/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.common.sector.InlandCategoryEmissionFactorKey;
import nl.overheid.aerius.db.common.sector.InlandCategoryKey;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;
import nl.overheid.aerius.shared.domain.sector.category.ColdStartCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.ShippingMovementType;
import nl.overheid.aerius.shared.emissions.shipping.ShippingLaden;

/**
 * Needs a proper database to test against, testing DB operations for categories.
 * Tests for now only check if queries work, not if the returned values are as expected. That'd require a strict test database.
 */
class CategoryRepositoriesTest extends BaseDBTest {

  private static final int YEAR = 2030;
  private static final int FARM_LODGING_TYPE_ID_WITH_ADDITIONAL = 184;
  private static final int FARM_LODGING_TYPE_ID_WITH_REDUCTIVE = 45;

  /**
   * Test method for
   * {@link nl.overheid.aerius.db.common.sector.category.RoadEmissionCategoryRepository#findAllRoadEmissionCategories(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  void testFindAllRoadEmissionCategories() throws SQLException {
    final RoadEmissionCategories categories = RoadEmissionCategoryRepository.findAllRoadEmissionCategories(getConnection(), getMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories.getAreas());
    testReturnedCategories(categories.getRoadTypes());
    testReturnedCategories(categories.getVehicleTypes());
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.FarmLodgingCategoryRepository#getFarmLodgingCategories}.
   * @throws SQLException
   */
  @Test
  @Disabled("No longer contains data in nl-latest version")
  void testGetFarmLodgingCategories() throws SQLException {
    final FarmLodgingCategories categories = FarmLodgingCategoryRepository.getFarmLodgingCategories(getConnection(), getMessagesKey());
    testReturnedCategories(categories.getFarmAdditionalLodgingSystemCategories());
    testReturnedCategories(categories.getFarmReductiveLodgingSystemCategories());
    testFarmLodgingCategories(categories.getFarmLodgingSystemCategories());
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.FarmAnimalHousingCategoryRepository#getFarmAnimalHousingCategories}.
   * @throws SQLException
   */
  @Test
  void testGetFarmAnimalHousingCategories() throws SQLException {
    final FarmAnimalHousingCategories categories = FarmAnimalHousingCategoryRepository.getFarmAnimalHousingCategories(getConnection(), getMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories.getAnimalCategories());
    testReturnedCategories(categories.getAnimalHousingCategories());
    testReturnedCategories(categories.getAdditionalHousingSystemCategories());
    assertNotNull(categories.getAnimalBasicHousingCategoryCodes(), "Retrieved animal basic housing map should not be null");
    assertFalse(categories.getAnimalBasicHousingCategoryCodes().isEmpty(), "Retrieved animal basic housing map should not be empty");
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.FarmlandEmissionCategoryRepository#findAllFarmlandEmissionCategories}.
   * @throws SQLException
   */
  @Test
  void testFindAllFarmlandCategories() throws SQLException {
    final List<FarmlandCategory> categories = FarmlandEmissionCategoryRepository.findAllFarmlandEmissionCategories(getConnection(), getMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
  }

  @Test
  void testSpecificColdStartSourceCharacteristics() throws SQLException {
    final OPSSourceCharacteristics characteristics = ColdStartCategoryRepository.findSpecificColdStartCharacteristics(getConnection(),
        "LBALEUR4");
    assertNotNull(characteristics, "Retrieved cold start specific characteristics should not be null");

    assertNotNull(characteristics.getDiurnalVariation(), "Cold start specific diurnal variation should not be null");
  }

  @Test
  void testStandardColdStartSourceCharacteristics() throws SQLException {
    final OPSSourceCharacteristics characteristics = ColdStartCategoryRepository.findStandardColdStartCharacteristics(getConnection(),
        "NORMAL_FREIGHT");
    assertNotNull(characteristics, "Retrieved cold start standard characteristics should not be null");

    assertNotNull(characteristics, "Cold start standard diurnal variation should not be null");
  }

  @Test
  void testColdStartSourceCategories() throws SQLException {
    final ColdStartCategories categories = ColdStartCategoryRepository.findAllColdStartCategories(getConnection(), getMessagesKey());
    assertNotNull(categories, "Retrieved cold start categories should not be null");
    testReturnedCategories(categories.getStandardCategories());
    testReturnedCategories(categories.getSpecificCategories());
  }

  /**
   * Test method for
   * {@link nl.overheid.aerius.db.common.sector.category.MobileSourceCategoryRepository#findOffRoadMobileSourceCategories(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  void testFindOffRoadMobileSourceCategories() throws SQLException {
    final List<OffRoadMobileSourceCategory> categories = MobileSourceCategoryRepository.findOffRoadMobileSourceCategories(getConnection(),
        getMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
  }

  /**
   * Test method for
   * {@link nl.overheid.aerius.db.common.sector.category.MobileSourceCategoryRepository#findOffRoadMobileSourceCategories(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  void testFindOnRoadMobileSourceCategories() throws SQLException {
    final List<OnRoadMobileSourceCategory> categories = MobileSourceCategoryRepository.findOnRoadMobileSourceCategories(getConnection(),
        getMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
  }

  /**
   * Test method for
   * {@link nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository#findAllMaritimeShippingCategories(Connection, nl.overheid.aerius.i18n.DBMessages.DBMessagesKey)}
   * @throws SQLException
   */
  @Test
  void testFindAllMaritimeShippingCategories() throws SQLException {
    final List<MaritimeShippingCategory> categories = getTestMaritimeShippingCategories();
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
  }

  @Test
  void testFindAllInlandShipCategories() throws SQLException {
    final List<InlandShippingCategory> categories = getTestInlandShippingCategories();
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
  }

  @Test
  void testGetInlandWaterwayCategories() throws SQLException {
    final List<InlandWaterwayCategory> categories = ShippingCategoryRepository.getInlandWaterwayCategories(getConnection(), getMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
  }

  @Test
  void testGetInlandShippingCategories() throws SQLException {
    final InlandShippingCategories categories = ShippingCategoryRepository.findInlandShippingCategories(getConnection(), getMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    final InlandWaterwayCategory cemtCategory = categories.getWaterwayCategoryByCode("CEMT_II");
    assertNotNull(cemtCategory, "Existing waterway type: CEMT_II");
    assertEquals(1, cemtCategory.getDirections().size(), "Number of directions for CEMT_II");
    final InlandWaterwayCategory waalCategory = categories.getWaterwayCategoryByCode("Waal");
    assertNotNull(waalCategory, "Another existing waterway type: Waal");
    assertEquals(2, waalCategory.getDirections().size(), "Number of directions for Waal");
    final InlandShippingCategory shipCategory = categories.getShipCategoryByCode("BII-6L");
    assertNotNull(shipCategory, "Existing ship category");
    assertFalse(categories.isValidCombination(shipCategory, cemtCategory), "Ship not allowed on CEMT_II");
    assertTrue(categories.isValidCombination(shipCategory, waalCategory), "Ship allowed on Waal");
  }

  @Test
  void testGetMaritimeCategoryEmissionFactors() throws SQLException {
    final MaritimeShippingCategory category = getTestMaritimeShippingCategories().get(0);
    final Map<Substance, Double> emissionFactors = ShippingCategoryRepository.findMaritimeCategoryEmissionFactors(getConnection(), category,
        ShippingMovementType.INLAND, YEAR);
    assertNotNull(emissionFactors, "Retrieved emissionFactors should not be null");
  }

  @Test
  void testGetMaritimeCategoryDockedEmissionFactors() throws SQLException {
    final MaritimeShippingCategory category = getTestMaritimeShippingCategories().get(0);
    final Map<Substance, Double> emissionFactors = ShippingCategoryRepository.findMaritimeCategoryDockedEmissionFactors(getConnection(), category,
        YEAR);
    assertNotNull(emissionFactors, "Retrieved emissionFactors should not be null");
  }

  @Test
  void testGetMaritimeShippingCharacteristics() throws SQLException {
    final MaritimeShippingCategory category = getTestMaritimeShippingCategories().get(0);
    final OPSSourceCharacteristics characteristics = ShippingCategoryRepository.getCharacteristics(getConnection(), category,
        ShippingMovementType.INLAND, YEAR);
    assertNotNull(characteristics, "Retrieved characteristics should not be null");
  }

  @Test
  void testGetInlandCategoryRouteEmissionFactors() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors = ShippingCategoryRepository
        .findInlandCategoryRouteEmissionFactors(getConnection(), category, YEAR);
    assertNotNull(emissionFactors, "Retrieved emissionFactors should not be null");
    boolean foundLaden = false;
    boolean foundUnladen = false;
    final Set<WaterwayDirection> directions = new HashSet<>();
    for (final Entry<InlandCategoryEmissionFactorKey, Double> entry : emissionFactors.entrySet()) {
      assertNotEquals(0.0, entry.getValue().doubleValue(), 1E-6, "Retrieved emissionFactor");
      if (entry.getKey().isLaden()) {
        foundLaden = true;
      } else {
        foundUnladen = true;
      }
      directions.add(entry.getKey().getDirection());
    }
    assertTrue(foundLaden, "Expected emissionfactors for laden ships.");
    assertTrue(foundUnladen, "Expected emissionfactors for unladen ships.");
    assertEquals(WaterwayDirection.values().length, directions.size(), "Directions found " + directions);
  }

  @Test
  void testGetInlandCategoryRouteCharacteristics() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<InlandCategoryKey, OPSSourceCharacteristics> characteristics = ShippingCategoryRepository
        .getInlandCategoryRouteCharacteristics(getConnection(), category);
    assertNotNull(characteristics, "Retrieved characteristics should not be null");
    for (final Entry<InlandCategoryKey, OPSSourceCharacteristics> entry : characteristics.entrySet()) {
      assertNotEquals(0, entry.getValue().getHeatContent(), "Retrieved heat content");
      assertNotEquals(0, entry.getValue().getEmissionHeight(), "Retrieved emission height");
    }
  }

  @Test
  void testGetInlandCategoryMooringEmissionFactors() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<Substance, Double> emissionFactors = ShippingCategoryRepository.findInlandCategoryMooringEmissionFactors(getConnection(), category,
        YEAR);
    assertNotNull(emissionFactors, "Retrieved emissionFactors should not be null");
    for (final Entry<Substance, Double> entry : emissionFactors.entrySet()) {
      assertNotEquals(0.0, entry.getValue(), 1E-3, "Retrieved emissionFactor");
    }
  }

  @Test
  void testGetInlandCategoryMooringCharacteristics() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<ShippingLaden, OPSSourceCharacteristics> characteristics = ShippingCategoryRepository
        .getInlandCategoryMooringCharacteristics(getConnection(), category);
    assertNotNull(characteristics, "Retrieved characteristics should not be null");
    assertEquals(2, characteristics.size(), "Should have characteristics for both laden and unladen.");
    for (final Entry<ShippingLaden, OPSSourceCharacteristics> entry : characteristics.entrySet()) {
      assertNotEquals(0, entry.getValue().getHeatContent(), "Retrieved heat content");
      assertNotEquals(0, entry.getValue().getEmissionHeight(), "Retrieved emission height");
    }
  }

  private void testReturnedCategories(final List<? extends AbstractCategory> categories) {
    assertNotNull(categories, "Testing categories should not be null");
    assertFalse(categories.isEmpty(), "Testing categories should not be empty");
    final HashSet<Integer> ids = new HashSet<>();
    for (final AbstractCategory category : categories) {
      assertFalse(ids.contains(category.getId()),
          category.getClass().getSimpleName() + " category list has duplicate IDs. ID: " + category.getId());
      ids.add(category.getId());
      assertNotNull(category.getCode(), category.getClass().getSimpleName() + " code was null. ID: " + category.getId());
      assertNotNull(category.getName(), category.getClass().getSimpleName() + " name was null. ID: " + category.getId());
      // description can be null
    }
  }

  private void testFarmLodgingCategories(final List<FarmLodgingCategory> categories) {
    testReturnedCategories(categories);
    for (final FarmLodgingCategory cat : categories) {
      assertFalse(cat.getFarmLodgingSystemDefinitions().isEmpty(), cat.getClass().getSimpleName() + " has no lodging system definitions. ID: " +
          cat.getId());
      final HashSet<Integer> ids = new HashSet<>();
      for (final FarmLodgingSystemDefinition def : cat.getFarmLodgingSystemDefinitions()) {
        assertFalse(ids.contains(def.getId()), def.getClass().getSimpleName() + " category list has duplicate IDs. ID: " +
            def.getId() + ", lodging ID: " + cat.getId());
        ids.add(def.getId());
        assertNotNull(def.getCode(), def.getClass().getSimpleName() + " code was null. ID: " + def.getId() +
            ", lodging ID: " + cat.getId());
        assertNotNull(def.getName(), def.getClass().getSimpleName() + " name was null. ID: " + def.getId() +
            ", lodging ID: " + cat.getId());
        // description can be null
      }

      if (cat.getId() == FARM_LODGING_TYPE_ID_WITH_ADDITIONAL) {
        assertFalse(cat.getFarmAdditionalLodgingSystemCategories().isEmpty(),
            cat.getClass().getSimpleName() + " has no additional lodging systems. ID: " +
                cat.getId());
      }

      if (cat.getId() == FARM_LODGING_TYPE_ID_WITH_REDUCTIVE) {
        assertFalse(cat.getFarmReductiveLodgingSystemCategories().isEmpty(),
            cat.getClass().getSimpleName() + " has no reductive lodging systems. ID: " +
                cat.getId());
      }
    }
  }

  private List<MaritimeShippingCategory> getTestMaritimeShippingCategories() throws SQLException {
    return ShippingCategoryRepository.findAllMaritimeShippingCategories(getConnection(), getMessagesKey());
  }

  private List<InlandShippingCategory> getTestInlandShippingCategories() throws SQLException {
    return ShippingCategoryRepository.findAllInlandShippingCategories(getConnection(), getMessagesKey());
  }

}
