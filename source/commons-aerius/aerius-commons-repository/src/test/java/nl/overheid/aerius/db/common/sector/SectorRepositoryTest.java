/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;

/**
 * Needs a proper database to test against, testing DB operations for sectors.
 */
class SectorRepositoryTest extends BaseDBTest {

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.SectorRepository#getSectors(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  void testGetSectors() throws SQLException {
    final ArrayList<Sector> sectors = SectorRepository.getSectors(getConnection(), getMessagesKey());
    assertNotNull(sectors, "Retrieved sectors should not be null");
    final HashSet<Integer> sectorIds = new HashSet<>();
    for (final Sector sector : sectors) {
      if (sectorIds.contains(sector.getSectorId())) {
        fail("Found duplicate sector id: " + sector.getSectorId());
      } else {
        sectorIds.add(sector.getSectorId());
      }
    }
  }

  @Test
  void testGetOPSSourceCharacteristics() throws SQLException {
    final OPSSourceCharacteristics characteristics =
        SectorRepository.getOPSSourceCharacteristics(getConnection(),
            SectorRepository.getSectors(getConnection(), getMessagesKey()).get(0));
    assertNotNull(characteristics, "Retrieved characteristics should not be null");
  }

  @Test
  void testGetOPSSourceCharacteristicsSpecificNH3() throws SQLException {
    // Sector: 4130 - Beweiding/pasture
    final Sector sector = new Sector(4130, SectorGroup.AGRICULTURE, "");
    final OPSSourceCharacteristics characteristics = SectorRepository.getOPSSourceCharacteristics(getConnection(), sector);
    assertNotNull(characteristics, "Retrieved characteristics should not be null");
    assertEquals(0.5, characteristics.getEmissionHeight(), 1E-3, "Emission height");
    assertEquals(0.3, characteristics.getSpread(), 1E-3, "Spread");
    assertEquals(0, characteristics.getHeatContent(), 1E-3, "Heatcontent");
    assertNull(characteristics.getDiameter(), "Diameter from DB always absent");
    assertEquals(DiurnalVariation.FERTILISER, characteristics.getDiurnalVariation(), "Diurnal variation");
    assertEquals(0, characteristics.getParticleSizeDistribution(), "PSD");
  }

  @Test
  void testGetOPSSourceCharacteristicsSpecific() throws SQLException {
    // Sector: 1100 - Voedings- en genotmiddelen
    final Sector sector = new Sector(1100, SectorGroup.INDUSTRY, "");
    final OPSSourceCharacteristics characteristics = SectorRepository.getOPSSourceCharacteristics(getConnection(), sector);
    assertNotNull(characteristics, "Retrieved characteristics should not be null");
    assertEquals(15.0, characteristics.getEmissionHeight(), 1E-3, "Emission height");
    assertEquals(7.5, characteristics.getSpread(), 1E-3, "Spread");
    assertEquals(0.34, characteristics.getHeatContent(), 1E-3, "Heatcontent");
    assertNull(characteristics.getDiameter(), "Diameter from DB always absent");
    assertEquals(DiurnalVariation.INDUSTRIAL_ACTIVITY, characteristics.getDiurnalVariation(), "Diurnal variation");
    assertEquals(0, characteristics.getParticleSizeDistribution(), "PSD");
  }
}
