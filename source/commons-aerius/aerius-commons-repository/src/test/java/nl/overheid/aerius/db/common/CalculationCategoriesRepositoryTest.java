/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;

/**
 * Test class for CalculationCategoriesRepository
 *
 * Only testing if queries work, as test database (NL) does not contain data for these tables.
 */
class CalculationCategoriesRepositoryTest extends BaseDBTest {

  @Test
  void testGetCalculationCountries() throws SQLException {
    final List<SimpleCategory> countries = CalculationCategoriesRepository.getCalculationPermitAreas(getConnection());
    assertNotNull(countries, "Returned list should not be null");
  }

  @Test
  void testGetCalculationProjectCategories() throws SQLException {
    final List<SimpleCategory> projectCategories = CalculationCategoriesRepository.getCalculationProjectCategories(getConnection());
    assertNotNull(projectCategories, "Returned list should not be null");
  }

  @Test
  void testGetDefaultDistances() throws SQLException {
    final Map<String, Map<String, Integer>> projectCategories = CalculationCategoriesRepository.getDefaultDistances(getConnection());
    assertNotNull(projectCategories, "Returned map should not be null");
  }

}
