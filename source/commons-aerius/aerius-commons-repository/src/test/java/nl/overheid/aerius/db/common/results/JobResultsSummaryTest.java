/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.EnumSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.JobSummary;
import nl.overheid.aerius.shared.domain.summary.JobSummaryProjectRecord;
import nl.overheid.aerius.shared.domain.summary.JobSummarySituationRecord;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Needs a proper database to test against, testing DB operations for calculations.
 */
class JobResultsSummaryTest extends ResultsTestBase {

  private static final Set<ResultStatisticType> EXPECTED_PROJECT_TYPES = EnumSet.of(
      ResultStatisticType.MAX_INCREASE, ResultStatisticType.MAX_TOTAL, ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL,
      ResultStatisticType.MAX_TOTAL_PERCENTAGE_CRITICAL_LEVEL);

  @BeforeEach
  public void prepareTests() throws AeriusException, SQLException {
    super.prepareTests(CalculationJobType.PROCESS_CONTRIBUTION);
  }

  @Test
  void testDetermineJobSummary() throws AeriusException {
    final JobSummary results = resultsRepository.determineJobSummary(jobId, this);

    assertEquals(24, results.getSituationRecords().size(), "Number of situation records");
    final Map<Integer, Long> recordsPerArea = results.getSituationRecords().stream()
        .collect(Collectors.groupingBy(x -> x.getAssessmentArea().getId(), Collectors.counting()));
    for (final JobSummarySituationRecord situationRecord : results.getSituationRecords()) {
      assertTrue(situationRecord.getMaxContributions().containsKey(EmissionResultKey.NOXNH3_DEPOSITION), "Should contain NOXNH3 deposition result");
      assertTrue(situationRecord.getMaxContributions().get(EmissionResultKey.NOXNH3_DEPOSITION) > 0, "Value should be positive (or at least not 0)");
    }
    assertEquals(Set.of(ASSESSMENT_AREA_1, ASSESSMENT_AREA_2, ASSESSMENT_AREA_3, ASSESSMENT_AREA_4), recordsPerArea.keySet(),
        "Found assessment areas");
    for (final Entry<Integer, Long> entry : recordsPerArea.entrySet()) {
      assertEquals(6, entry.getValue(), "Number of situation records for area " + entry.getKey());
    }

    assertEquals(8, results.getProjectRecords().size(), "Number of project records");
    final Map<Integer, Long> projectRecordsPerArea = results.getProjectRecords().stream()
        .collect(Collectors.groupingBy(x -> x.getAssessmentArea().getId(), Collectors.counting()));
    for (final JobSummaryProjectRecord projectRecord : results.getProjectRecords()) {
      assertEquals(EXPECTED_PROJECT_TYPES, projectRecord.getMaxValues().keySet(), "Expected project statistic types");
    }
    assertEquals(Set.of(ASSESSMENT_AREA_1, ASSESSMENT_AREA_2, ASSESSMENT_AREA_3, ASSESSMENT_AREA_4), projectRecordsPerArea.keySet(),
        "Found assessment areas");
    for (final Entry<Integer, Long> entry : projectRecordsPerArea.entrySet()) {
      assertEquals(2, entry.getValue(), "Number of situation records for area " + entry.getKey());
    }
  }

}
