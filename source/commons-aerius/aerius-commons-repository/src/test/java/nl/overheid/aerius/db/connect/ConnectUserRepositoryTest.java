/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.connect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ConnectUserRepository}.
 */
class ConnectUserRepositoryTest extends BaseDBTest {

  private static final String TEST_API_KEY1 = "wnaR9FavGRGv8RXCmdfXKEqeIt1DTZUS";
  private static final String TEST_API_KEY2 = "SPWQ9l9IelzKFfJQnvyQLpEYlrQDrNH0";
  private static final String TEST_EMAIL1 = "test@example.com";
  private static final String TEST_EMAIL2 = "abc@example.com";

  @Test
  void testCreateAndGetUser() throws SQLException, AeriusException {
    final ConnectUser userIn1 = new ConnectUser();
    userIn1.setApiKey(TEST_API_KEY1);
    userIn1.setEmailAddress(TEST_EMAIL1);
    assertEquals(0, userIn1.getId(), "User id must not be set");
    ConnectUserRepository.createUser(getConnection(), userIn1);
    assertNotEquals(0, userIn1.getId(), "User id must be set");

    final ConnectUser userOut1 = ConnectUserRepository.getUserByApiKey(getConnection(), TEST_API_KEY1);
    assertNotNull(userOut1, "User must be found by API-key");
    assertEquals(userIn1.getId(), userOut1.getId(), "User id must match");
    assertEquals(TEST_API_KEY1, userOut1.getApiKey(), "API-key must match");

    final ConnectUser userOut2 = ConnectUserRepository.getUserByEmailAddress(getConnection(), TEST_EMAIL1);
    assertNotNull(userOut2, "User must be found by e-mail");
    assertEquals(userIn1.getId(), userOut2.getId(), "User id must match");
    assertEquals(TEST_EMAIL1, userOut2.getEmailAddress(), "E-mail must match");

    assertEquals(userOut1, userOut2, "Users must match");
    assertEquals(userOut1.getId(), userOut2.getId(), "User id's must match");
    assertEquals(userOut1.isEnabled(), userOut2.isEnabled(), "User Enabled must match");
    assertEquals(userOut1.getMaxConcurrentJobs(), userOut2.getMaxConcurrentJobs(), "User MaxConcurrentCalculations must match");

    // Secondary user with different properties
    final ConnectUser userIn2 = new ConnectUser();
    userIn2.setApiKey(TEST_API_KEY2);
    userIn2.setEmailAddress(TEST_EMAIL2);
    userIn2.setEnabled(false);
    final int maxConcurrent = ConstantRepository.getInteger(getConnection(), ConstantsEnum.CONNECT_MAX_CONCURRENT_JOBS_FOR_NEW_USER);
    userIn2.setMaxConcurrentJobs(maxConcurrent + 1);
    ConnectUserRepository.createUser(getConnection(), userIn2);
    final ConnectUser userOut3 = ConnectUserRepository.getUserByApiKey(getConnection(), TEST_API_KEY2);

    assertNotEquals(userOut1, userOut3, "Users must not match");
    assertNotEquals(userOut1.getId(), userOut3.getId(), "User id's must not match");
    assertFalse(userOut3.isEnabled(), "User Enabled must be false");
    assertEquals(maxConcurrent, userOut3.getMaxConcurrentJobs(), "User MaxConcurrentCalculations should be the db value");
  }

  @Test
  void testCreateAndGetUserCaseInsentive() throws SQLException, AeriusException {
    final ConnectUser userIn1 = new ConnectUser();
    userIn1.setApiKey(TEST_API_KEY1);
    userIn1.setEmailAddress(TEST_EMAIL1.toUpperCase());
    assertEquals(0, userIn1.getId(), "User id must not be set");
    ConnectUserRepository.createUser(getConnection(), userIn1);
    assertNotEquals(0, userIn1.getId(), "User id must be set");

    final ConnectUser userOut1 = ConnectUserRepository.getUserByEmailAddress(getConnection(), TEST_EMAIL1.toLowerCase());
    assertNotNull(userOut1, "User must be found by lowercase email");
    assertEquals(userIn1.getId(), userOut1.getId(), "User id must match");

    final ConnectUser userIn2 = new ConnectUser();
    userIn2.setApiKey(TEST_API_KEY2);
    userIn2.setEmailAddress(TEST_EMAIL2.toLowerCase());
    ConnectUserRepository.createUser(getConnection(), userIn2);

    final ConnectUser userOut2 = ConnectUserRepository.getUserByEmailAddress(getConnection(), TEST_EMAIL2.toUpperCase());
    assertNotNull(userOut2, "User must be found by uppercase email");
    assertEquals(userIn2.getId(), userOut2.getId(), "User id must match");
  }

  @Test
  void testCreateExistingApiUser() throws SQLException, AeriusException {
    final ConnectUser userIn = new ConnectUser();
    userIn.setApiKey(TEST_API_KEY1);
    userIn.setEmailAddress(TEST_EMAIL1);
    ConnectUserRepository.createUser(getConnection(), userIn);
    userIn.setEmailAddress(TEST_EMAIL2);
    assertThrows(AeriusException.class, () -> ConnectUserRepository.createUser(getConnection(), userIn),
        "Expected exception for creating an existing user.");
  }

  @Test
  void testCreateExistingEmailUser() throws SQLException, AeriusException {
    final ConnectUser userIn = new ConnectUser();
    userIn.setApiKey(TEST_API_KEY1);
    userIn.setEmailAddress(TEST_EMAIL1);
    ConnectUserRepository.createUser(getConnection(), userIn);
    userIn.setApiKey(TEST_API_KEY2);
    assertThrows(AeriusException.class, () -> ConnectUserRepository.createUser(getConnection(), userIn),
        "Expected exception for creating a user with an existing e-mail address.");
  }

}
