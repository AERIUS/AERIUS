/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.reference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Unit test for {@link ReferenceGeneratorV2}.
 */
class ReferenceGeneratorV2Test {

  protected ReferenceGeneratorV2 createReferenceGeneratorV2() {
    return (ReferenceGeneratorV2) ReferenceGeneratorFactory.createReferenceGenerator(ReferenceGeneratorV2.VERSION_ID);
  }

  protected String generateMeldingReference() {
    return createReferenceGeneratorV2().generateReference(ReferenceType.MELDING);
  }

  protected String generatePermitReference() {
    return createReferenceGeneratorV2().generateReference(ReferenceType.PERMIT);
  }

  protected String generateLbvReference() {
    return createReferenceGeneratorV2().generateReference(ReferenceType.LBV);
  }

  protected boolean validateMeldingReference(final String reference) {
    return createReferenceGeneratorV2().validateReference(ReferenceType.MELDING, reference);
  }

  protected boolean validatePermitReference(final String reference) {
    return createReferenceGeneratorV2().validateReference(ReferenceType.PERMIT, reference);
  }

  protected boolean validateLbvReference(final String reference) {
    return createReferenceGeneratorV2().validateReference(ReferenceType.LBV, reference);
  }

  protected ReferenceType getReferenceType(final String reference) throws Exception {
    return createReferenceGeneratorV2().getReferenceType(reference);
  }

  protected byte getVersion(final String reference) throws Exception {
    return createReferenceGeneratorV2().getVersion(reference);
  }

  @Test
  void testReferenceTypesDifferent() {
    for (int i = 0; i < 10; i++) {
      assertNotEquals(generateMeldingReference(), generatePermitReference(),
          "Melding reference and permit reference generated at same time should still never be equal");
    }
  }

  @Test
  void testReferenceTimesDifferent() throws InterruptedException {
    assertGeneration(() -> generateMeldingReference());
    assertGeneration(() -> generatePermitReference());
    assertGeneration(() -> generateLbvReference());
  }

  private void assertGeneration(final Supplier<String> referenceGenerator) throws InterruptedException {
    final String a = referenceGenerator.get();
    Thread.sleep(150);
    final String b = referenceGenerator.get();
    assertNotEquals(a, b, "References generated with an interval > 100 ms should never be equal");
  }

  @Test
  void testReferenceRandomness() {
    int equalCount = 0;
    for (int i = 0; i < 100000; i++) {
      if (generatePermitReference().equals(generatePermitReference())) {
        equalCount++;
      }
    }
    assertFalse(equalCount > 10, "Too much references with equal values while random bytes should prevent this");
  }

  @ParameterizedTest
  @MethodSource("correctReferences")
  void testCorrectReferences(final ReferenceType referenceType, final List<String> initialReferences, final Supplier<String> generator)
      throws Exception {
    final List<String> references = new ArrayList<>();

    // Add some manual and live references
    references.addAll(initialReferences);
    references.add(generator.get());
    Thread.sleep(150);
    references.add(generator.get());
    Thread.sleep(250);
    references.add(generator.get());

    for (final String reference : references) {
      final ReferenceType type = getReferenceType(reference);
      final byte version = getVersion(reference);

      assertEquals(referenceType, type, "Reference type");
      assertEquals(ReferenceGeneratorV2.VERSION_ID, version, "Reference version");
      final ReferenceGeneratorV2 referenceGenerator = createReferenceGeneratorV2();
      assertTrue(referenceGenerator.validateReference(referenceType, reference), reference + " not correct");
      Stream.of(ReferenceType.values()).filter(rt -> rt != referenceType).forEach(
          rt -> assertFalse(referenceGenerator.validateReference(rt, reference), reference + " should not validate as reference"));
    }
  }

  private static Stream<Arguments> correctReferences() {
    final ReferenceGeneratorV2Test test = new ReferenceGeneratorV2Test();

    return Stream.of(
        Arguments.of(ReferenceType.MELDING, List.of("2BC7RQUjnqGU", "znD3nkTUYmS"), wrap(test::generateMeldingReference)),
        Arguments.of(ReferenceType.PERMIT, List.of("RjqZTvBGHvWq", "S5k1WaEG3KYs"), wrap(test::generatePermitReference)),
        Arguments.of(ReferenceType.LBV, List.of("qTK59TSAA3Q4", "qH1ESix6bCek"), wrap(test::generateLbvReference)));
  }

  // Use wrapper otherwise Arguments.of doesn't like argument :-(
  private static Supplier<String> wrap(final Supplier<String> supplier) {
    return supplier;
  }

  @Test
  void testIncorrectReferences() throws Exception {
    final List<String> references = new ArrayList<>();

    // Add some manual and live incorrect references
    references.add("1uE8hNWmk");
    references.add("3Rz2TRNUHX");
    references.add("X");
    references.add("");
    references.add("S5k1WaEG3KYs=");
    references.add(generateMeldingReference() + " ");

    for (final String reference : references) {
      assertFalse(validateMeldingReference(reference), reference + " should not validate as reference");
      assertFalse(validatePermitReference(reference), reference + " should not validate as reference");
    }
  }
}
