/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOXNH3_DEPOSITION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.info.HabitatAreaInfo;
import nl.overheid.aerius.shared.domain.info.HabitatSurfaceType;
import nl.overheid.aerius.shared.domain.info.HabitatTypeInfo;
import nl.overheid.aerius.shared.domain.info.Natura2000Info;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Unit test for {@link ReceptorInfoRepository}.
 */
class ReceptorInfoRepositoryTest extends BaseDBTest {

  private static final int RECEPTOR_POINT_ID = 4286669;
  private static final int NATURA2000_ID = 65;
  private static final int YEAR = 2030;
  // Receptor ID that is on the exact edge of 2 zoomlevel 4 hexagons where st_intersects does not work.
  private static final int EDGE_RECEPTOR_POINT_ID = 4324599;
  private static final int EXTRA_ASSESSMENT_RECEPTOR_ID = 4262215;

  @Test
  void testGetNaturaAreaInfo() throws SQLException {
    List<Natura2000Info> infos = ReceptorInfoRepository.getNaturaAreaInfo(getConnection(), 0, getMessagesKey());
    assertNotNull(infos, "Result shouldn't be null");
    assertEquals(0, infos.size(), "Number of nature areas");
    infos = ReceptorInfoRepository.getNaturaAreaInfo(getConnection(), RECEPTOR_POINT_ID, getMessagesKey());
    assertNotNull(infos, "Result shouldn't be null");
    assertEquals(1, infos.size(), "Number of nature areas");
    final Natura2000Info info = infos.get(0);
    assertEquals(NATURA2000_ID, info.getId(), "Assessment Area id");
    assertEquals(NATURA2000_ID, info.getId(), "Natura 2000 Area id");
    assertEquals("" + NATURA2000_ID, info.getCode(), "Natura 2000 Area code");
    assertEquals("Binnenveld", info.getName(), "Nature area name");
    assertEquals(2, info.getHabitats().size(), "Number of habitat types in nature area");
    for (final HabitatAreaInfo areaInfo : info.getHabitats()) {
      validateHabitatAreaForAssessmentArea(areaInfo);
    }
    assertEquals("Habitatrichtlijn", info.getProtection(), "Nature area protection");
    assertNull(info.getGeometry(), "Geometry shouldn't be filled: too much info");
    assertEquals(3, info.getExtraAssessmentHabitats().size(), "Number of extra assessment habitat types in nature area");
    for (final HabitatTypeInfo typeInfo : info.getExtraAssessmentHabitats()) {
      validateHabitatTypeForAssessmentArea(typeInfo);
    }
  }

  private void validateHabitatTypeForAssessmentArea(final HabitatTypeInfo areaInfo) {
    assertNotNull(areaInfo.getCode(), "Code");
    assertNotNull(areaInfo.getName(), "Name");
    assertNotNull(areaInfo.getQualityGoal(), "Quality goal");
    assertNotNull(areaInfo.getExtentGoal(), "Extent goal");
    assertNotNull(areaInfo.getCriticalLevels(), "Critical levels");
    assertTrue(areaInfo.getCriticalLevels().hasResult(NOXNH3_DEPOSITION), "Critical level for nitrogen deposition");
    assertNotEquals(0.0, areaInfo.getCriticalLevels().get(NOXNH3_DEPOSITION), "Cartographic surface");
  }

  private void validateHabitatAreaForAssessmentArea(final HabitatAreaInfo areaInfo) {
    validateHabitatTypeForAssessmentArea(areaInfo);
    assertNotEquals(0.0, areaInfo.getCartographicSurface(), "Cartographic surface");
    for (final HabitatSurfaceType surfaceType : HabitatSurfaceType.values()) {
      assertTrue(areaInfo.getAdditionalSurfaces().containsKey(surfaceType), "Surface for " + surfaceType);
      assertNotEquals(0.0, areaInfo.getAdditionalSurface(surfaceType), "Surface value for " + surfaceType);
    }
  }

  @Test
  void testGetReceptorHabitatAreaInfo() throws SQLException {
    final List<HabitatAreaInfo> infos = ReceptorInfoRepository.getReceptorHabitatAreaInfo(getConnection(), RECEPTOR_POINT_ID,
        getMessagesKey());
    assertNotNull(infos, "Result shouldn't be empty");
    assertEquals(1, infos.size(), "Number of habitat types");
    final HabitatAreaInfo info = infos.get(0);
    assertEquals("H7140A", info.getCode(), "Code of habitat type");
    assertEquals("Overgangs- en trilvenen (trilvenen)", info.getName(), "Name of habitat type");
    assertEquals(1214, info.getCriticalLevels().get(NOXNH3_DEPOSITION), 0.0, "KDW of habitat type");
    assertNotEquals(0.0, info.getCartographicSurface(), 1E-3, "Hexagon overlap of habitat type");
  }

  @Test
  void testGetReceptorExtraAssessmentHabitatsInfo() throws SQLException {
    final List<HabitatTypeInfo> infos = ReceptorInfoRepository.getReceptorExtraAssessmentHabitatsInfo(getConnection(), EXTRA_ASSESSMENT_RECEPTOR_ID,
        getMessagesKey());
    assertNotNull(infos, "Result shouldn't be empty");
    assertEquals(2, infos.size(), "Number of habitat types");
    final HabitatTypeInfo info = infos.get(0);
    assertEquals("H6410", info.getCode(), "Code of habitat type");
    assertEquals("Blauwgraslanden", info.getName(), "Name of habitat type");
    assertEquals(786, info.getCriticalLevels().get(NOXNH3_DEPOSITION), 0.0, "KDW of habitat type");
    final HabitatTypeInfo info2 = infos.get(1);
    assertEquals("H7140A", info2.getCode(), "Code of habitat type");
    assertEquals("Overgangs- en trilvenen (trilvenen)", info2.getName(), "Name of habitat type");
    assertEquals(1214, info2.getCriticalLevels().get(NOXNH3_DEPOSITION), 0.0, "KDW of habitat type");
  }

  @Test
  void testGetReceptorHexagonTypes() throws SQLException {
    final List<SummaryHexagonType> hexagonTypes = ReceptorInfoRepository.getHexagonTypes(getConnection(), RECEPTOR_POINT_ID);
    assertNotNull(hexagonTypes, "Result shouldn't be null");
    assertEquals(1, hexagonTypes.size(), "Number of hexagon types");
    assertTrue(hexagonTypes.contains(SummaryHexagonType.RELEVANT_HEXAGONS), "HexagonTypes should contain RELEVANT_HEXAGONS");
  }

  @Test
  void testGetBackgroundEmissionResult() throws SQLException, AeriusException {
    assertGetBackgroundEmissionResult(null);
  }

  @Test
  void testGetBackgroundEmissionResultPoint() throws SQLException, AeriusException {
    assertGetBackgroundEmissionResult(NOXNH3_DEPOSITION);
  }

  void assertGetBackgroundEmissionResult(final EmissionResultKey erk) throws SQLException, AeriusException {
    EmissionResults info = getBackgroundEmissionResult(RECEPTOR_POINT_ID, 13, erk);
    assertTrue(info.isEmpty(), "Emission results info object should be empty when retrieving wrong year");
    info = getBackgroundEmissionResult(1, YEAR, erk);
    assertTrue(info.isEmpty(), "Emission results info object should be empty when retrieving outside NL");
    info = getBackgroundEmissionResult(RECEPTOR_POINT_ID, YEAR, erk);
    assertNotNull(info, "Emission results info object");
    assertNotEquals(0, info.get(NOXNH3_DEPOSITION), 1, "Background NH3+NOx deposition");

    final EmissionResults edgeInfo = getBackgroundEmissionResult(EDGE_RECEPTOR_POINT_ID, YEAR, erk);
    assertNotNull(edgeInfo, "Emission results edge info object");
    assertNotEquals(0, edgeInfo.get(NOXNH3_DEPOSITION), 1, "Background NH3+NOx deposition for edge receptor");
  }

  private EmissionResults getBackgroundEmissionResult(final int receptorPointId, final int year, final EmissionResultKey erk)
      throws SQLException, AeriusException {
    final Point point = getReceptorPoint(receptorPointId);
    if (erk == null) {
      return ReceptorInfoRepository.getBackgroundEmissionResult(getConnection(), point, receptorPointId, year);
    } else {
      final ReceptorInfoRepositoryBean repository = new ReceptorInfoRepositoryBean(getPMF());

      return repository.getBackgroundEmissionResultERK(point, receptorPointId, year, erk);
    }
  }

  private Point getReceptorPoint(final int id) {
    return RECEPTOR_UTIL.getPointFromReceptorId(id);
  }
}
