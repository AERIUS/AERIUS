/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.SQLException;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.db.common.GeneralRepositoryBean;
import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculation;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.CustomCalculationPointResult;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticsMarker;
import nl.overheid.aerius.shared.domain.summary.SituationResultsAreaSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsHabitatSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SurfaceChartResults;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

abstract class ResultsTestBase extends CalculationRepositoryTestBase implements AreaInformationSupplier {

  protected static final double SURFACE_DELTA = 0.01;
  protected static final double MAX_TOTAL_DELTA = 10;

  protected static final int ASSESSMENT_AREA_1 = 57;
  protected static final String ASSESSMENT_AREA_1_NAME = "Veluwe";
  protected static final int ASSESSMENT_AREA_2 = 2;
  protected static final String ASSESSMENT_AREA_2_NAME = "Duinen en Lage Land Texel";
  protected static final int ASSESSMENT_AREA_3 = 65;
  protected static final String ASSESSMENT_AREA_3_NAME = "Binnenveld";
  protected static final int ASSESSMENT_AREA_4 = 114;
  protected static final String ASSESSMENT_AREA_4_NAME = "Krammer-Volkerak";

  protected final ResultsSummaryRepository resultsRepository = new ResultsSummaryRepository(getPMF(), new ReceptorUtil(RECEPTOR_GRID_SETTINGS));

  protected int jobId;
  protected SituationCalculations situationCalculations;

  protected int calculationIdProposed;
  protected Calculation calculationProposed;
  protected int calculationIdSmallProposed;
  protected Calculation calculationSmallProposed;
  protected int calculationIdReference;
  protected Calculation calculationReference;
  protected int calculationIdTemporary;
  protected Calculation calculationTemporary;
  protected int calculationIdCombinationProposed;
  protected Calculation calculationCombinationProposed;
  protected int calculationIdCombinationReference;
  protected Calculation calculationCombinationReference;

  protected final GeneralRepositoryBean generalRepositoryBean = new GeneralRepositoryBean(getPMF());

  protected void prepareTests(final CalculationJobType jobType) throws SQLException, AeriusException {
    prepareTests(jobType, false);
  }

  protected void prepareTests(final CalculationJobType jobType, final boolean includeArchiveContribution) throws SQLException, AeriusException {
    final int calculationPointSetId = insertCalculationPoints();
    removeCalculationResults();
    createCalculation(calculationPointSetId);
    final List<CalculationPointFeature> calculationPointList = getExampleCalculationPointOutputData();

    calculationIdProposed = calculation.getCalculationId();
    calculationResultScale = 3.0;
    insertCalculationResults();
    calculationResultScale = 6.0;
    List<AeriusResultPoint> results = toAeriusResultPoints(Substance.NH3, calculationPointList);
    insertResults(results, 8);
    calculationProposed = calculation;
    calculationProposed.getSituation().setType(SituationType.PROPOSED);
    if (includeArchiveContribution) {
      calculationResultScale = 11.0;
      insertArchiveContribution(calculationIdProposed);
    }

    newCalculation(calculationPointSetId);
    calculationIdSmallProposed = calculation.getCalculationId();
    calculationResultScale = 0.5;
    insertCalculationResults();
    calculationResultScale = 1.0;
    results = toAeriusResultPoints(Substance.NH3, calculationPointList);
    insertResults(results, 8);
    calculationSmallProposed = calculation;
    calculationSmallProposed.getSituation().setType(SituationType.PROPOSED);

    newCalculation(calculationPointSetId);
    calculationIdReference = calculation.getCalculationId();
    calculationResultScale = 1.0;
    insertCalculationResults();
    calculationResultScale = 2.0;
    results = toAeriusResultPoints(Substance.NH3, calculationPointList);
    insertResults(results, 8);
    calculationReference = calculation;
    calculationReference.getSituation().setType(SituationType.REFERENCE);

    newCalculation(calculationPointSetId);
    calculationIdTemporary = calculation.getCalculationId();
    calculationResultScale = 1.5;
    insertCalculationResults();
    calculationResultScale = 3.0;
    results = toAeriusResultPoints(Substance.NH3, calculationPointList);
    insertResults(results, 8);
    calculationTemporary = calculation;
    calculationTemporary.getSituation().setType(SituationType.TEMPORARY);

    newCalculation(calculationPointSetId);
    calculationIdCombinationProposed = calculation.getCalculationId();
    calculationResultScale = 1.1;
    insertCalculationResults();
    calculationResultScale = 2.2;
    results = toAeriusResultPoints(Substance.NH3, calculationPointList);
    insertResults(results, 8);
    calculationCombinationProposed = calculation;
    calculationCombinationProposed.getSituation().setType(SituationType.COMBINATION_PROPOSED);

    newCalculation(calculationPointSetId);
    calculationIdCombinationReference = calculation.getCalculationId();
    calculationResultScale = 0.9;
    insertCalculationResults();
    calculationResultScale = 1.8;
    results = toAeriusResultPoints(Substance.NH3, calculationPointList);
    insertResults(results, 8);
    calculationCombinationReference = calculation;
    calculationCombinationReference.getSituation().setType(SituationType.COMBINATION_REFERENCE);

    jobId = createJob();

    situationCalculations = new SituationCalculations();
    final SituationCalculation proposedCalculation = new SituationCalculation();
    proposedCalculation.setCalculationId(calculationIdProposed);
    proposedCalculation.setSituationType(SituationType.PROPOSED);
    situationCalculations.add(proposedCalculation);

    final SituationCalculation smallProposedCalculation = new SituationCalculation();
    smallProposedCalculation.setCalculationId(calculationIdSmallProposed);
    smallProposedCalculation.setSituationType(SituationType.PROPOSED);
    situationCalculations.add(smallProposedCalculation);

    final SituationCalculation referenceCalculation = new SituationCalculation();
    referenceCalculation.setCalculationId(calculationIdReference);
    referenceCalculation.setSituationType(SituationType.REFERENCE);
    situationCalculations.add(referenceCalculation);

    final SituationCalculation temporaryCalculation = new SituationCalculation();
    temporaryCalculation.setCalculationId(calculationIdTemporary);
    temporaryCalculation.setSituationType(SituationType.TEMPORARY);
    situationCalculations.add(temporaryCalculation);

    final SituationCalculation combinationProposedCalculation = new SituationCalculation();
    combinationProposedCalculation.setCalculationId(calculationIdCombinationProposed);
    combinationProposedCalculation.setSituationType(SituationType.COMBINATION_PROPOSED);
    situationCalculations.add(combinationProposedCalculation);

    final SituationCalculation combinationReferenceCalculation = new SituationCalculation();
    combinationReferenceCalculation.setCalculationId(calculationIdCombinationReference);
    combinationReferenceCalculation.setSituationType(SituationType.COMBINATION_REFERENCE);
    situationCalculations.add(combinationReferenceCalculation);

    resultsRepository.insertResultsSummaries(jobId, situationCalculations, jobType);
  }

  protected void validateReceptorResults(final SituationResultsSummary results) {
    assertNotNull(results, "Results should not be null");

    validateOverall(results, this::validateOverall);
    validateAreaStatistics(results, this::validateAreaStatistics);
    validateHabitatStatistics(results, this::validateHabitatStatistics);
    validateChartStatistics(results, this::validateChartStatistics);
    validateStatisticMarkers(results, this::validateStatisticMarkers);
  }

  protected void validateCustomCalculationPoints(final SituationResultsSummary results, final double expectedResult1, final double expectedResult2,
      final ResultStatisticType maxValueStat) {
    validateCustomCalculationPoints(results, expectedResult1, expectedResult2);
    final SituationResultsStatistics expected = new SituationResultsStatistics();
    expected.put(ResultStatisticType.COUNT_CALCULATION_POINTS, 2.0);
    expected.put(maxValueStat, expectedResult1);
    assertStatistics(expected, results.getStatistics());
  }

  protected void validateCustomCalculationPoints(final SituationResultsSummary results, final double expectedResult1, final double expectedResult2,
      final boolean increase) {
    validateCustomCalculationPoints(results, expectedResult1, expectedResult2);
    final SituationResultsStatistics expected = new SituationResultsStatistics();
    expected.put(ResultStatisticType.COUNT_CALCULATION_POINTS, 2.0);
    expected.put(ResultStatisticType.COUNT_CALCULATION_POINTS_INCREASE, increase ? 2.0 : 0.0);
    expected.put(ResultStatisticType.COUNT_CALCULATION_POINTS_DECREASE, increase ? 0.0 : 2.0);
    expected.put(ResultStatisticType.MAX_INCREASE, increase ? expectedResult1 : 0.0);
    expected.put(ResultStatisticType.MAX_DECREASE, increase ? 0.0 : expectedResult1);
    assertStatistics(expected, results.getStatistics());
  }

  private void validateCustomCalculationPoints(final SituationResultsSummary results, final double expectedResult1, final double expectedResult2) {
    final List<CustomCalculationPointResult> customPointResults = results.getCustomPointResults();
    assertEquals(2, customPointResults.size(), "Custom point results");
    for (final CustomCalculationPointResult pointResult : customPointResults) {
      if (pointResult.getCustomPointId() == 1) {
        assertEquals(expectedResult1, pointResult.getResult(), 0.001, "Result for point 1");
      } else if (pointResult.getCustomPointId() == 2) {
        assertEquals(expectedResult2, pointResult.getResult(), 0.001, "Result for point 2");
      } else {
        fail("Unexpected custom point id: " + pointResult.getCustomPointId());
      }
    }
  }

  protected void validateReceptorResultsExtraAssessment(final SituationResultsSummary results) {
    assertNotNull(results, "Results should not be null");

    validateOverall(results, this::validateOverallExtraAssessment);
    validateAreaStatistics(results, this::validateAreaStatisticsExtraAssessment);
    validateHabitatStatistics(results, this::validateHabitatStatisticsExtraAssessment);
    validateChartStatistics(results, this::validateChartStatisticsExtraAssessment);
    validateStatisticMarkers(results, this::validateStatisticMarkersExtraAssessment);
  }

  private void validateOverall(final SituationResultsSummary results, final Consumer<SituationResultsStatistics> validateMethod) {
    final SituationResultsStatistics statistics = results.getStatistics();
    assertNotNull(statistics, "Overall statistics should not be null");
    assertFalse(statistics.containsKey(null), "Overall statistics hould not contain a null key");
    validateMethod.accept(statistics);
  }

  protected void validateOverall(final SituationResultsStatistics statistics) {
    fail("Not implemented for this test case");
  }

  protected void validateOverallExtraAssessment(final SituationResultsStatistics statistics) {
    fail("Not implemented for this test case");
  }

  private void validateAreaStatistics(final SituationResultsSummary results, final Consumer<List<SituationResultsAreaSummary>> validateMethod) {
    final List<SituationResultsAreaSummary> areaStatistics = results.getAreaStatistics();
    assertNotNull(areaStatistics, "Area statistics should not be null");
    for (final SituationResultsAreaSummary areaSummary : areaStatistics) {
      assertFalse(areaSummary.getStatistics().containsKey(null), "Should not contain a null key");
    }
    validateMethod.accept(areaStatistics);
  }

  protected void validateAreaStatistics(final List<SituationResultsAreaSummary> areaStatistics) {
    fail("Not implemented for this test case");
  }

  protected void validateAreaStatisticsExtraAssessment(final List<SituationResultsAreaSummary> areaStatistics) {
    fail("Not implemented for this test case");
  }

  private void validateHabitatStatistics(final SituationResultsSummary results, final Consumer<List<SituationResultsHabitatSummary>> validateMethod) {
    final SituationResultsAreaSummary areaSummary = results.getAreaStatistics().get(0);
    final List<SituationResultsHabitatSummary> habitatSummaries = areaSummary.getHabitatSummaries();
    assertNotNull(habitatSummaries, "Habitat statistics should not be null");
    for (final SituationResultsHabitatSummary habitatSummary : habitatSummaries) {
      assertFalse(habitatSummary.getStatistics().containsKey(null), "Should not contain a null key");
    }
    validateMethod.accept(habitatSummaries);
  }

  protected void validateHabitatStatistics(final List<SituationResultsHabitatSummary> habitatSummaries) {
    fail("Not implemented for this test case");
  }

  protected void validateHabitatStatisticsExtraAssessment(final List<SituationResultsHabitatSummary> habitatSummaries) {
    fail("Not implemented for this test case");
  }

  private void validateChartStatistics(final SituationResultsSummary results, final Consumer<List<SurfaceChartResults>> validateMethod) {
    final SituationResultsAreaSummary areaSummary = results.getAreaStatistics().get(0);
    final List<SurfaceChartResults> chartResults = areaSummary.getEmissionResultChartResults();
    assertNotNull(chartResults, "Chart results should not be null");
    validateMethod.accept(chartResults);
  }

  protected void validateChartStatistics(final List<SurfaceChartResults> chartResults) {
    fail("Not implemented for this test case");
  }

  protected void validateChartStatisticsExtraAssessment(final List<SurfaceChartResults> chartResults) {
    fail("Not implemented for this test case");
  }

  protected void assertChartStatistics(final int expectedCategories, final int indexCategoryWithResult,
      final double expectedLowerBound, final double expectedSurface, final List<SurfaceChartResults> chartResults) {
    assertEquals(expectedCategories, chartResults.size(), "Number of chart categories");

    final SurfaceChartResults chartResult = chartResults.get(indexCategoryWithResult);
    assertEquals(expectedLowerBound, chartResult.getLowerBound(), 0.001, "Lower bound of chart category");
    // Next assert is more about asserting the test case than anything else. If the result is 0, we're not really testing anything.
    assertNotEquals(0.0, chartResult.getCartographicSurface(), SURFACE_DELTA, "Expected surface shouldn't be 0, change test");
    assertEquals(expectedSurface, chartResult.getCartographicSurface(), SURFACE_DELTA, "Surface of chart category");
  }

  private void validateStatisticMarkers(final SituationResultsSummary results, final Consumer<List<ResultStatisticsMarker>> validateMethod) {
    final List<ResultStatisticsMarker> markers = results.getMarkers();
    assertNotNull(markers, "Markers should not be null");
    validateMethod.accept(markers);
  }

  protected void validateStatisticMarkers(final List<ResultStatisticsMarker> markers) {
    fail("Not implemented for this test case");
  }

  protected void assertStatisticMarkers(final List<Integer> expectedReceptors, final Set<ResultStatisticType> expectedStatistics,
      final List<ResultStatisticsMarker> markers) {
    assertEquals(expectedReceptors.size(), markers.size(), "Number of markers");
    final List<Integer> receptorIds = markers.stream().map(ResultStatisticsMarker::getReceptorId).sorted().toList();
    assertEquals(expectedReceptors, receptorIds, "Receptor IDs");
    markers.stream().forEach(marker -> assertEquals(
        expectedStatistics,
        marker.getStatisticsTypes(), "statistics on marker"));
  }

  protected void validateStatisticMarkersExtraAssessment(final List<ResultStatisticsMarker> markers) {
    fail("Not implemented for this test case");
  }

  protected void assertStatistics(final SituationResultsStatistics expectedStatistics, final SituationResultsStatistics statistics) {
    assertEquals(expectedStatistics.keySet(), statistics.keySet(), "Statistics found");

    for (final Entry<ResultStatisticType, Double> entry : expectedStatistics.entrySet()) {
      final ResultStatisticType statistic = entry.getKey();
      final double delta = switch (statistic) {
      case SUM_CARTOGRAPHIC_SURFACE, SUM_CARTOGRAPHIC_SURFACE_INCREASE, SUM_CARTOGRAPHIC_SURFACE_DECREASE -> SURFACE_DELTA;
      case MAX_TOTAL -> MAX_TOTAL_DELTA;
      default -> 0.001;
      };
      if (entry.getValue() == null) {
        assertNull(statistics.get(statistic), "Null for " + statistic);
      } else {
        assertEquals(entry.getValue(), statistics.get(statistic), delta, "Value for " + statistic);
      }
    }
  }

  protected void assertAreas(final List<String> expectedAreaNames, final List<SituationResultsAreaSummary> areaStatistics) {
    assertEquals(expectedAreaNames.size(), areaStatistics.size(), "Number of areas with results");

    final List<String> foundAreaNames = areaStatistics.stream()
        .map(SituationResultsAreaSummary::getAssessmentArea)
        .map(AssessmentArea::getName)
        .toList();
    assertEquals(expectedAreaNames, foundAreaNames, "Areas found in order");
  }

  protected void assertAreasExtraAssessment(final List<SituationResultsAreaSummary> areaStatistics) {
    assertAreas(List.of(ASSESSMENT_AREA_3_NAME), areaStatistics);
  }

  protected void assertHabitatsExtraAssessment(final List<SituationResultsHabitatSummary> habitatSummaries) {
    assertEquals(1, habitatSummaries.size(), "Number of habitats with results");

    assertHabitat(habitatSummaries.get(0), "Overgangs- en trilvenen (veenmosrietlanden)", 500);
  }

  protected void addExpectedOverallStats(final SituationResultsStatistics expected) {
    expected.put(ResultStatisticType.COUNT_RECEPTORS, 3.0);
    expected.put(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE, 8757.02);
  }

  protected void addExpectedOverallStats(final SituationResultsStatistics expected, final boolean increase) {
    addExpectedOverallStats(expected);
    expected.put(ResultStatisticType.COUNT_RECEPTORS_INCREASE, increase ? 3.0 : 0.0);
    expected.put(ResultStatisticType.COUNT_RECEPTORS_DECREASE, increase ? 0.0 : 3.0);
    expected.put(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE_INCREASE, increase ? 8757.02 : 0.0);
    expected.put(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE_DECREASE, increase ? 0.0 : 8757.02);
  }

  protected void addExpectedAreaStats(final SituationResultsStatistics expected, final double area) {
    expected.put(ResultStatisticType.COUNT_RECEPTORS, 1.0);
    expected.put(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE, area);
  }

  protected void addExpectedAreaStats(final SituationResultsStatistics expected, final double area, final boolean increase) {
    addExpectedAreaStats(expected, area);
    expected.put(ResultStatisticType.COUNT_RECEPTORS_INCREASE, increase ? 1.0 : 0.0);
    expected.put(ResultStatisticType.COUNT_RECEPTORS_DECREASE, increase ? 0.0 : 1.0);
    expected.put(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE_INCREASE, increase ? area : 0.0);
    expected.put(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE_DECREASE, increase ? 0.0 : area);
  }

  protected void addExpectedStatsExtraAssessment(final SituationResultsStatistics expected) {
    expected.put(ResultStatisticType.COUNT_RECEPTORS, 1.0);
    expected.put(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE, 1.0);
  }

  protected void addExpectedStatsExtraAssessment(final SituationResultsStatistics expected, final boolean increase) {
    addExpectedStatsExtraAssessment(expected);
    expected.put(ResultStatisticType.COUNT_RECEPTORS_INCREASE, increase ? 1.0 : 0.0);
    expected.put(ResultStatisticType.COUNT_RECEPTORS_DECREASE, increase ? 0.0 : 1.0);
    expected.put(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE_INCREASE, increase ? 1.0 : 0.0);
    expected.put(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE_DECREASE, increase ? 0.0 : 1.0);
  }

  protected void assertDrogeHeiden(final SituationResultsHabitatSummary habitatSummary) {
    assertHabitat(habitatSummary, "Droge heiden", 714);
  }

  protected void assertHabitat(final SituationResultsHabitatSummary habitatSummary, final String expectedName, final double expectedCriticalLevel) {
    assertEquals(expectedName, habitatSummary.getHabitatType().getName(), "Habitat with result");
    assertEquals(expectedCriticalLevel, habitatSummary.getMinimumCriticalLevel(), "Habitat critical level");
  }

  private int createJob() throws SQLException, AeriusException {
    final String correlationIdentifier = JobRepository.createJob(getConnection(), JobType.CALCULATION, false);
    JobRepository.attachCalculations(getConnection(), correlationIdentifier, List.of(calculationProposed, calculationSmallProposed,
        calculationReference, calculationTemporary, calculationCombinationProposed, calculationCombinationReference));
    return JobRepository.getJobId(getConnection(), correlationIdentifier);
  }

  @Override
  public AssessmentArea determineAssessmentArea(final int assessmentAreaId) {
    final Optional<AssessmentArea> area = generalRepositoryBean.determineAssessmentArea(assessmentAreaId);
    return area.orElseGet(() -> {
      final AssessmentArea fakeArea = new AssessmentArea();
      fakeArea.setId(assessmentAreaId);
      fakeArea.setName("Gebied " + assessmentAreaId);
      return fakeArea;
    });
  }

  @Override
  public HabitatType determineHabitatType(final int habitatTypeId) {
    final Optional<HabitatType> habitatType = generalRepositoryBean.determineHabitatType(habitatTypeId);
    return habitatType.orElseGet(() -> {
      final HabitatType fakeHabitatType = new HabitatType();
      fakeHabitatType.setId(habitatTypeId);
      fakeHabitatType.setName("Habitat " + habitatTypeId);
      return fakeHabitatType;
    });
  }

  @Override
  public boolean determineHabitatTypeSensitiveness(final int habitatTypeId, final EmissionResultKey emissionResultKey) {
    return generalRepositoryBean.determineHabitatTypeSensitiveness(habitatTypeId)
        .map(sensitivenessMap -> sensitivenessMap.get(emissionResultKey))
        .orElse(false);
  }

  @Override
  public Double determineHabitatCriticalLevel(final int habitatTypeId, final EmissionResultKey emissionResultKey) {
    return generalRepositoryBean.determineHabitatTypeCriticalLevels(habitatTypeId)
        .map(criticalLevels -> criticalLevels.get(emissionResultKey))
        .orElse(null);
  }
}
