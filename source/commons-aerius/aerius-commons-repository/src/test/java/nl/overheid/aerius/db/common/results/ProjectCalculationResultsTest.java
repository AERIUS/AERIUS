/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ProjectSituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticsMarker;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsAreaSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsHabitatSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.summary.SummaryRequest;
import nl.overheid.aerius.shared.domain.summary.SurfaceChartResults;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Needs a proper database to test against, testing DB operations for calculations.
 */
class ProjectCalculationResultsTest extends ResultsTestBase {

  @BeforeEach
  public void prepareTests() throws AeriusException, SQLException {
    super.prepareTests(CalculationJobType.PROCESS_CONTRIBUTION);
  }

  @Test
  void testDetermineReceptorResultsSummary() throws AeriusException {
    final SummaryRequest summaryRequest = toRequest(SummaryHexagonType.RELEVANT_HEXAGONS);

    final SituationResultsSummary results = resultsRepository.determineReceptorResultsSummary(situationCalculations, summaryRequest,
        this);

    validateReceptorResults(results);

    validateOmittedAreas(results);
  }

  @Override
  protected void validateOverall(final SituationResultsStatistics statistics) {
    final SituationResultsStatistics expected = new SituationResultsStatistics();
    addExpectedOverallStats(expected, true);
    expected.put(ResultStatisticType.MAX_TOTAL, 7406.463);
    expected.put(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL, 65.611);
    expected.put(ResultStatisticType.MAX_INCREASE, 468.46002);
    expected.put(ResultStatisticType.MAX_DECREASE, null);
    assertStatistics(expected, statistics);
  }

  @Override
  protected void validateAreaStatistics(final List<SituationResultsAreaSummary> areaStatistics) {
    assertAreas(List.of(ASSESSMENT_AREA_1_NAME, ASSESSMENT_AREA_2_NAME, ASSESSMENT_AREA_4_NAME), areaStatistics);

    final SituationResultsAreaSummary areaSummary = areaStatistics.get(0);
    final SituationResultsStatistics expected = veluweStats();
    assertStatistics(expected, areaSummary.getStatistics());
  }

  @Override
  protected void validateHabitatStatistics(final List<SituationResultsHabitatSummary> habitatSummaries) {
    assertEquals(1, habitatSummaries.size(), "Number of habitats with results");

    final SituationResultsHabitatSummary habitatSummary = habitatSummaries.get(0);
    assertDrogeHeiden(habitatSummary);
    final SituationResultsStatistics expected = veluweStats();
    expected.put(ResultStatisticType.MAX_BACKGROUND, 4776.053);
    assertStatistics(expected, habitatSummary.getStatistics());
  }

  private SituationResultsStatistics veluweStats() {
    final SituationResultsStatistics expected = new SituationResultsStatistics();
    addExpectedAreaStats(expected, 6000.0, true);
    expected.put(ResultStatisticType.MAX_TOTAL, 5244.513);
    expected.put(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL, 65.611);
    expected.put(ResultStatisticType.MAX_TOTAL_PERCENTAGE_CRITICAL_LEVEL, 734.52563);
    expected.put(ResultStatisticType.MAX_INCREASE, 468.46002);
    expected.put(ResultStatisticType.MAX_DECREASE, null);
    return expected;
  }

  @Override
  protected void validateChartStatistics(final List<SurfaceChartResults> chartResults) {
    assertChartStatistics(12, 11, 20.0, 6000.0, chartResults);
  }

  @Override
  protected void validateStatisticMarkers(final List<ResultStatisticsMarker> markers) {
    assertStatisticMarkers(List.of(RECEPTOR_POINT_ID_6, RECEPTOR_POINT_ID_1, RECEPTOR_POINT_ID_2),
        Set.of(ResultStatisticType.MAX_TOTAL, ResultStatisticType.MAX_INCREASE, ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL),
        markers);
  }

  private void validateOmittedAreas(final SituationResultsSummary results) {
    assertEquals(ProjectSituationResultsSummary.class, results.getClass(), "Type of SituationResultsSummary for project calculations");
    assertEquals(0, ((ProjectSituationResultsSummary) results).getOmittedAreas().size(),
        "Number of areas with deposition in situation calculation and project calculation");
  }

  @Test
  void testDetermineCustomCalculationPointResultsSummary() throws AeriusException {
    final SummaryRequest summaryRequest = toRequest(SummaryHexagonType.CUSTOM_CALCULATION_POINTS);
    final SituationResultsSummary results = resultsRepository.determineCustomCalculationPointResultsSummary(situationCalculations,
        summaryRequest);

    validateCustomCalculationPoints(results, 936.92, 4.92, true);
  }

  @Test
  void testDetermineExtraAssessmentResultsSummary() throws AeriusException {
    final SummaryRequest summaryRequest = toRequest(SummaryHexagonType.EXTRA_ASSESSMENT_HEXAGONS);
    final SituationResultsSummary results = resultsRepository.determineReceptorResultsSummary(situationCalculations, summaryRequest,
        this);

    validateReceptorResultsExtraAssessment(results);

    validateOmittedAreas(results);
  }

  @Override
  protected void validateOverallExtraAssessment(final SituationResultsStatistics statistics) {
    final SituationResultsStatistics expected = new SituationResultsStatistics();
    addExpectedStatsExtraAssessment(expected, true);
    expected.put(ResultStatisticType.MAX_TOTAL, 4279.147);
    expected.put(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL, 0.024);
    expected.put(ResultStatisticType.MAX_INCREASE, 0.12);
    expected.put(ResultStatisticType.MAX_DECREASE, null);
    assertStatistics(expected, statistics);
  }

  @Override
  protected void validateAreaStatisticsExtraAssessment(final List<SituationResultsAreaSummary> areaStatistics) {
    assertAreasExtraAssessment(areaStatistics);

    final SituationResultsAreaSummary areaSummary = areaStatistics.get(0);
    final SituationResultsStatistics expected = binnenveldStatsExtraAssessment();
    assertStatistics(expected, areaSummary.getStatistics());
  }

  @Override
  protected void validateHabitatStatisticsExtraAssessment(final List<SituationResultsHabitatSummary> habitatSummaries) {
    assertHabitatsExtraAssessment(habitatSummaries);

    final SituationResultsHabitatSummary habitatSummary = habitatSummaries.get(0);
    final SituationResultsStatistics expected = binnenveldStatsExtraAssessment();
    expected.put(ResultStatisticType.MAX_BACKGROUND, 4279.027);
    assertStatistics(expected, habitatSummary.getStatistics());
  }

  private SituationResultsStatistics binnenveldStatsExtraAssessment() {
    final SituationResultsStatistics expected = new SituationResultsStatistics();
    addExpectedStatsExtraAssessment(expected, true);
    expected.put(ResultStatisticType.MAX_TOTAL, 4279.147);
    expected.put(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL, 0.024);
    expected.put(ResultStatisticType.MAX_TOTAL_PERCENTAGE_CRITICAL_LEVEL, 855.8294);
    expected.put(ResultStatisticType.MAX_INCREASE, 0.12);
    expected.put(ResultStatisticType.MAX_DECREASE, null);
    return expected;
  }

  @Override
  protected void validateChartStatisticsExtraAssessment(final List<SurfaceChartResults> chartResults) {
    assertChartStatistics(12, 6, 0.0, 1.0, chartResults);
  }

  @Override
  protected void validateStatisticMarkersExtraAssessment(final List<ResultStatisticsMarker> markers) {
    assertStatisticMarkers(List.of(RECEPTOR_POINT_ID_7),
        Set.of(ResultStatisticType.MAX_TOTAL, ResultStatisticType.MAX_INCREASE, ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL),
        markers);
  }

  private SummaryRequest toRequest(final SummaryHexagonType hexagontype) {
    return new SummaryRequest(ScenarioResultType.PROJECT_CALCULATION, jobId, calculationIdProposed,
        hexagontype, OverlappingHexagonType.ALL_HEXAGONS, EmissionResultKey.NOXNH3_DEPOSITION, null);
  }
}
