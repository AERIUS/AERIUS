/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.reference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Unit test for {@link ReferenceGeneratorV1}.
 */
class ReferenceGeneratorV1Test {

  protected ReferenceGeneratorV1 createReferenceGeneratorV1() {
    return (ReferenceGeneratorV1) ReferenceGeneratorFactory.createReferenceGenerator(ReferenceGeneratorV1.VERSION_ID);
  }

  protected String generatePermitReference() {
    return createReferenceGeneratorV1().generateReference(ReferenceType.PERMIT);
  }

  protected String generateMeldingReference() {
    return createReferenceGeneratorV1().generateReference(ReferenceType.MELDING);
  }

  protected boolean validatePermitReference(final String reference) {
    return createReferenceGeneratorV1().validateReference(ReferenceType.PERMIT, reference);
  }

  protected boolean validateMeldingReference(final String reference) {
    return createReferenceGeneratorV1().validateReference(ReferenceType.MELDING, reference);
  }

  protected ReferenceType getReferenceType(final String reference) throws Exception {
    return createReferenceGeneratorV1().getReferenceType(reference);
  }

  protected byte getVersion(final String reference) throws Exception {
    return createReferenceGeneratorV1().getVersion(reference);
  }

  @Test
  void testReferenceTypesDifferent() {
    for (int i = 0; i < 10; i++) {
      assertNotEquals(generateMeldingReference(), generatePermitReference(),
          "Melding reference and permit reference generated at same time should still never be equal");
    }
  }

  @Test
  void testReferenceTimesDifferent() throws InterruptedException {
    String a, b;

    a = generatePermitReference();
    Thread.sleep(150);
    b = generatePermitReference();
    assertNotEquals(a, b, "References generated with an interval > 100 ms should never be equal");

    a = generateMeldingReference();
    Thread.sleep(150);
    b = generateMeldingReference();
    assertNotEquals(a, b, "References generated with an interval > 100 ms should never be equal");
  }

  @Test
  void testCorrectPermitReferences() throws Exception {
    final List<String> references = new ArrayList<>();

    // Add some manual and live references
    references.add("2EQrgWQBNh");
    references.add("2FWbckKPa2");
    references.add(generatePermitReference());
    Thread.sleep(150);
    references.add(generatePermitReference());
    Thread.sleep(250);
    references.add(generatePermitReference());

    for (final String reference : references) {
      final ReferenceType type = getReferenceType(reference);
      final byte version = getVersion(reference);

      assertEquals(ReferenceType.PERMIT, type, "Reference type");
      assertEquals(ReferenceGeneratorV1.VERSION_ID, version, "Reference version");
      assertTrue(validatePermitReference(reference), reference + " not correct");
      assertFalse(validateMeldingReference(reference), reference + " should not validate as melding reference");
    }
  }

  @Test
  void testCorrectMeldingReferences() throws Exception {
    final List<String> references = new ArrayList<>();

    // Add some manual and live references
    references.add("1uD8hNWmk");
    references.add("12zx4wHDk9");
    references.add(generateMeldingReference());
    Thread.sleep(150);
    references.add(generateMeldingReference());
    Thread.sleep(250);
    references.add(generateMeldingReference());

    for (final String reference : references) {
      final ReferenceType type = getReferenceType(reference);
      final byte version = getVersion(reference);

      assertEquals(ReferenceType.MELDING, type, "Reference type");
      assertEquals(ReferenceGeneratorV1.VERSION_ID, version, "Reference version");
      assertTrue(validateMeldingReference(reference), reference + " not correct");
      assertFalse(validatePermitReference(reference), reference + " should not validate as permit reference");
    }
  }

  @Test
  void testIncorrectReferences() throws Exception {
    final List<String> references = new ArrayList<>();

    // Add some manual and live incorrect references
    references.add("1uE8hNWmk");
    references.add("3Rz2TRNUHX");
    references.add("X");
    references.add("");
    references.add("539iP6b92R=");
    references.add(generateMeldingReference() + " ");

    // Unsupported in V1: Priority (sub)projects
    references.add("3ScNvm3Csm");
    references.add("3Tm7A3R4Zg");
    references.add("4eXyeZn4yi");
    references.add("4fUkhgHzgw");

    for (final String reference : references) {
      assertFalse(validateMeldingReference(reference), reference + " should not validate as reference");
      assertFalse(validatePermitReference(reference), reference + " should not validate as reference");
    }
  }
}
