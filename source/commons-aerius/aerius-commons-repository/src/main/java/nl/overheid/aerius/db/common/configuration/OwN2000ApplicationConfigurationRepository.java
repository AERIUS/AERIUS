/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.configuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.aerius.search.domain.SearchCapability;
import nl.aerius.search.domain.SearchRegion;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.MainTab;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.sector.SectorIconUtil;
import nl.overheid.aerius.shared.domain.v2.building.BuildingLimits;

public class OwN2000ApplicationConfigurationRepository extends AbstractApplicationConfigurationRepository {

  private static final List<EmissionResultKey> EMISSION_RESULT_KEYS = Arrays.asList(EmissionResultKey.NOXNH3_DEPOSITION);

  private static final List<MainTab> OWN2000_MAIN_TABS = Arrays.asList(MainTab.SCENARIO_RESULTS);
  private static final List<CalculationJobType> CALCULATION_JOB_TYPES = Arrays.asList(CalculationJobType.PROCESS_CONTRIBUTION,
      CalculationJobType.MAX_TEMPORARY_EFFECT, CalculationJobType.SINGLE_SCENARIO);

  @Override
  protected Theme getTheme() {
    return Theme.OWN2000;
  }

  @Override
  protected List<EmissionResultKey> getEmissionResultKeys() {
    return EMISSION_RESULT_KEYS;
  }

  @Override
  protected SearchRegion getSearchRegion() {
    return SearchRegion.NL;
  }

  @Override
  protected List<SearchCapability> getSearchCapabilities() {
    return Arrays.asList(SearchCapability.BASIC_INFO, SearchCapability.RECEPTOR, SearchCapability.COORDINATE, SearchCapability.ASSESSMENT_AREA);
  }

  @Override
  protected void addProjections(final ApplicationConfiguration appConfig) {
    addSingleProjection(appConfig, Proj4BoilerPlate.EPSG_28992, Proj4BoilerPlate.RDNEW_PROJ4_DEF);
  }

  @Override
  protected List<CalculationJobType> getCalculationJobTypes() {
    return CALCULATION_JOB_TYPES;
  }

  @Override
  protected Map<CalculationJobType, JobType> getDefaultExportJobTypes() {
    final Map<CalculationJobType, JobType> defaultExportJobTypes = new HashMap<>();
    defaultExportJobTypes.put(CalculationJobType.PROCESS_CONTRIBUTION, JobType.REPORT);
    return defaultExportJobTypes;
  }

  @Override
  protected List<CalculationJobType> getAvailableExportReportOptions() {
    return Arrays.asList(CalculationJobType.PROCESS_CONTRIBUTION);
  }

  @Override
  protected BuildingLimits getBuildingLimits() {
    return OPSLimits.INSTANCE;
  }

  @Override
  protected Set<AppFlag> getAppFlags() {
    final Set<AppFlag> set = new HashSet<>();

    set.add(AppFlag.ENABLE_INTERNAL);
    set.add(AppFlag.ENABLE_OPS_EXPORT);
    set.add(AppFlag.SHOWS_OVERLAPPING_EFFECTS);
    set.add(AppFlag.SHOW_HEXAGON_INDICATORS);
    set.add(AppFlag.SHOW_REPORT_APPENDICES);
    set.add(AppFlag.SHOW_ABROAD_OPTIONS);
    set.add(AppFlag.SHOW_FARM_ANIMAL_HOUSING_STANDARD_CODE_WARNING);
    set.add(AppFlag.SHOW_MAP_ON_JOB_PAGE);
    return set;
  }

  @Override
  protected List<MainTab> getAvailableTabs() {
    return OWN2000_MAIN_TABS;
  }

  @Override
  protected Map<Integer, SectorIcon> getSectorIcons() {
    return SectorIconUtil.own2000();
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected Enum getManualHost() {
    return ConstantsEnum.MANUAL_BASE_URL_CALCULATOR;
  }

  @Override
  protected Map<String, ColorRangeType> getRoadTrafficVolumeColors() {
    final Map<String, ColorRangeType> colors = new HashMap<>();
    colors.put("", ColorRangeType.ROAD_TRAFFIC_VOLUME);
    colors.put("light_traffic", ColorRangeType.ROAD_TRAFFIC_VOLUME_LIGHT_TRAFFIC);
    colors.put("normal_freight", ColorRangeType.ROAD_TRAFFIC_VOLUME_NORMAL_TRAFFIC);
    colors.put("heavy_freight", ColorRangeType.ROAD_TRAFFIC_VOLUME_HEAVY_FREIGHT);
    colors.put("auto_bus", ColorRangeType.ROAD_TRAFFIC_VOLUME_AUTO_BUS);
    return colors;
  }
}
