/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.util.EnumUtil;

/**
 * DB Class to obtain constants from the database.
 */
public final class ConstantRepository {

  // The logger.
  private static final Logger LOGGER = LoggerFactory.getLogger(ConstantRepository.class);

  // The SQL queries used in this service.
  private static final String GET_CONSTANT_QUERY = "SELECT value FROM system.constants_view WHERE key = ? ";

  private static final String SET_CONSTANT_QUERY = "SELECT system.ae_set_constant(?, ?) ";

  private static final String GET_SHAREDCONSTANTS_QUERY = "SELECT key, value FROM system.constants_view WHERE key LIKE 'SHARED_%' ";

  // Not allowed to instantiate.
  private ConstantRepository() {
  }

  /**
   * Get a constant enum value from the database. Runtime exception if not found.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * The function will return null if the enum value cannot be mapped by EnumUtil.
   * @param pmf The Persistance Manager Factory to used to obtain a connection.
   * @param constantEnum The constant to get the value for.
   * @param enumClass The enum class to map the value to.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static <E extends Enum<E>> E getEnum(final PMF pmf, final Enum<?> constantEnum, final Class<E> enumClass) {
    try (final Connection connection = pmf.getConnection()) {
      return getEnum(connection, constantEnum, enumClass);
    } catch (final SQLException e) {
      LOGGER.error("Error fetching constant from DB exception", e);
      throw new IllegalArgumentException(e);
    }
  }

  /**
   * Get a constant enum value from the database. Runtime exception if not found.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * The function will return null if the enum value cannot be mapped by EnumUtil.
   * @param connection The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @param enumClass The enum class to map the value to.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static <E extends Enum<E>> E getEnum(final Connection connection, final Enum<?> constantEnum, final Class<E> enumClass) throws SQLException {
    final String stringEnum = getString(connection, constantEnum, true);
    final E returnValue = EnumUtil.get(enumClass, stringEnum);

    if (returnValue == null) {
      throw new IllegalArgumentException("The key '" + constantEnum.name() + "' has a value '" + stringEnum
          + "' that cannot be matched to a valid value " + " of enum '" + enumClass + "'");
    }

    return returnValue;
  }

  /**
   * Get a constant value from the database. Runtime exception if not found.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * @param connection The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static String getString(final Connection connection, final Enum<?> constantEnum) throws SQLException {
    return getString(connection, constantEnum, true);
  }

  /**
   * Get a constant value from the database.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * @param connection The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @param mustExist If true, the constant must exist, else a runtime exception will be thrown.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static String getString(final Connection connection, final Enum<?> constantEnum, final boolean mustExist) throws SQLException {
    return findConstantByKey(connection, constantEnum.name(), mustExist);
  }

  /**
   * Get a constant value from the database. Runtime exception if not found.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * @param pmf The Persistance Manager Factory to used to obtain a connection.
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * <p>Will throw a runtime exception when a SQL exception occurred.
   */
  public static String getString(final PMF pmf, final Enum<?> constantEnum) {
    return getString(pmf, constantEnum, true);
  }

  /**
   * Get a constant value from the database.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * @param pmf The Persistance Manager Factory to used to obtain a connection.
   * @param constantEnum The constant to get the value for.
   * @param mustExist If true, the constant must exist, else a runtime exception will be thrown.
   * @return The value of the constant.
   * <p>Will throw a runtime exception when a SQL exception occurred.
   */
  public static String getString(final PMF pmf, final Enum<?> constantEnum, final boolean mustExist) {
    return findConstantByKey(pmf, constantEnum.name(), mustExist);
  }

  /**
   * Get a constant value from the database. Runtime exception if not found.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * Boolean is determined by using {@link Boolean#parseBoolean(String)}. Same rules apply.
   * @param pmf The Persistance Manager Factory to used to obtain a connection.
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * <p>Will throw a runtime exception when a SQL exception occurred.
   */
  public static boolean getBoolean(final PMF pmf, final Enum<?> constantEnum) {
    return Boolean.parseBoolean(getString(pmf, constantEnum, true));
  }

  /**
   * Get a constant value from the database. Runtime exception if not found.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * Boolean is determined by using {@link Boolean#parseBoolean(String)}. Same rules apply.
   * @param pmf The Persistance Manager Factory to used to obtain a connection.
   * @param constantEnum The constant to get the value for.
   * @param mustExist If true, the constant must exist, else a runtime exception will be thrown.
   * @return The value of the constant.
   * <p>Will throw a runtime exception when a SQL exception occurred.
   */
  public static boolean getBoolean(final PMF pmf, final Enum<?> constantEnum, final boolean mustExist) {
    return Boolean.parseBoolean(getString(pmf, constantEnum, mustExist));
  }

  /**
   * Get a constant value from the database. Runtime exception if not found.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * Boolean is determined by using {@link Boolean#parseBoolean(String)}. Same rules apply.
   * @param con The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static Boolean getBoolean(final Connection con, final Enum<?> constantEnum) throws SQLException {
    return Boolean.parseBoolean(getString(con, constantEnum, true));
  }

  /**
   * Get a constant value from the database. Runtime exception if not found.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * Boolean is determined by using {@link Boolean#parseBoolean(String)}. Same rules apply.
   * @param con The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @param mustExist If true, the constant must exist, else a runtime exception will be thrown.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static Boolean getBoolean(final Connection con, final Enum<?> constantEnum, final boolean mustExist) throws SQLException {
    return Boolean.parseBoolean(getString(con, constantEnum, mustExist));
  }

  /**
   * Get a constant value from the database, and cast it to the specified Number type.
   * @param connection The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @param t Class reference for the type to return.
   * @param <N> Type that will be returned.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static <N extends Number> N getNumber(final Connection connection, final Enum<?> constantEnum, final Class<N> t) throws SQLException {
    return getNumber(connection, constantEnum, t, true);
  }

  /**
   * Get a constant value from the database, and cast it to Integer.
   * @param pmf The Persistance Manager Factory to used to obtain a connection.
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static Integer getInteger(final PMF pmf, final Enum<?> constantEnum) throws SQLException {
    return getNumber(pmf, constantEnum, Integer.class);
  }

  /**
   * Get a constant value from the database, and cast it to Integer.
   * @param connection The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static Integer getInteger(final Connection connection, final Enum<?> constantEnum) throws SQLException {
    return getInteger(connection, constantEnum, true);
  }

  /**
   * Get a constant value from the database, and cast it to Integer.
   *
   * @param connection The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @param mustExist If true, the constant must exist, else a runtime exception will be thrown.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static Integer getInteger(final Connection connection, final Enum<?> constantEnum, final boolean mustExist) throws SQLException {
    return getNumber(connection, constantEnum, Integer.class, mustExist);
  }

  /**
   * Get a constant value from the database, and cast it to Double.
   * @param pmf The Persistance Manager Factory to used to obtain a connection.
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static Double getDouble(final PMF pmf, final Enum<?> constantEnum) throws SQLException {
    return getNumber(pmf, constantEnum, Double.class);
  }

  /**
   * Get a constant value from the database, and cast it to Double.
   * @param connection The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static Double getDouble(final Connection connection, final Enum<?> constantEnum) throws SQLException {
    return getNumber(connection, constantEnum, Double.class);
  }

  /**
   * Get a constant value from the database, and cast it to the specified Number type.
   * @param pmf The PMF that will be used to get a connection with the database.
   * @param constantEnum The constant to get the value for.
   * @param t Class reference for the type to return.
   * @param <N> Type that will be returned.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static <N extends Number> N getNumber(final PMF pmf, final Enum<?> constantEnum, final Class<N> t) throws SQLException {
    try (final Connection connection = pmf.getConnection()) {
      return getNumber(connection, constantEnum, t);
    }
  }

  /**
   * Get a constant value from the database, and cast it to the specified Number type.<br/>
   * All classes known to extend Number take a String value in their constructor,
   * which is why the constructor invocation for the specified type will never fail.
   *
   * @param connection The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @param t Class reference for the type to return.
   * @param <N> Type that will be returned.
   * @param mustExist If true, the constant must exist, else a runtime exception will be thrown.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static <N extends Number> N getNumber(final Connection connection, final Enum<?> constantEnum, final Class<N> t, final boolean mustExist)
      throws SQLException {
    return getObject(connection, constantEnum, t, mustExist);
  }

  /**
   * Get a constant value from the database, and cast it to the specified Object type.<br/>
   * All values in the constant table are of type {@link String},
   * the specified Object type <b>must</b> implement a Constructor with 1 {@link String} parameter,
   * able to interpret it as the target object type.
   * This method will fail if the given type does not meet that requirement.
   *
   * @param connection The connection to used (won't be closed).
   * @param constantEnum The constant to get the value for.
   * @param t Class reference for the type to return.
   * @param mustExist If true, the constant must exist, else a runtime exception will be thrown.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  private static <O extends Object> O getObject(final Connection connection, final Enum<?> constantEnum, final Class<O> t, final boolean mustExist)
      throws SQLException {
    try {
      final String value = findConstantByKey(connection, constantEnum.name(), mustExist);
      return value == null ? null : t.getConstructor(new Class[] {String.class}).newInstance(value);
    } catch (final Exception e) {
      LOGGER.error("[getObject] Could not invoke String constructor for Object type [{}]", t.getName());

      // Stacktrace will be logged on a higher level
      throw new SQLException(e);
    }
  }

  /**
   * Get constant value. The connection is NOT closed after usage.
   * If constant is not found, a runtime exception will be thrown.
   *
   * @param con
   *          The DB connection
   * @param key
   *          The key to fetch
   *
   * @return The value of the constant
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static String findConstantByKey(final Connection con, final String key) throws SQLException {
    return findConstantByKey(con, key, true);
  }

  /**
   * Get constant value.
   *
   * @param pmf
   *          The PMF to use to obtain a connection from.
   * @param key
   *          The key to fetch
   * @param mustExist
   *          If true and constant is not found, a runtime exception will be thrown.
   * @return The value of the constant, null if not found (and mustExist = false)
   * <p>Will throw a runtime exception when a SQL exception occurred.
   */
  public static String findConstantByKey(final PMF pmf, final String key, final boolean mustExist) {
    String constantValue = null;
    try (final Connection connection = pmf.getConnection()) {
      constantValue = findConstantByKey(connection, key, mustExist);
    } catch (final SQLException e) {
      LOGGER.debug("Error fetching constant for key: {}", key, e);
      throw new IllegalArgumentException(e);
    }
    if (constantValue == null) {
      LOGGER.debug("Constant not found for key: {}", key);
    }
    return constantValue;
  }

  /**
   * Get constant value. The connection is NOT closed after usage.
   *
   * @param con
   *          The DB connection
   * @param key
   *          The key to fetch
   * @param mustExist
   *          If true and constant is not found, a runtime exception will be thrown.
   * @return The value of the constant, null if not found (and mustExist = false)
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static String findConstantByKey(final Connection con, final String key, final boolean mustExist)
      throws SQLException {
    String constantValue = null;
    try (final PreparedStatement constantPS = con.prepareStatement(GET_CONSTANT_QUERY)) {
      constantPS.setString(1, key);
      final ResultSet rst = constantPS.executeQuery();

      if (rst.next()) {
        constantValue = rst.getString(1);
      } else if (mustExist) {
        throw new IllegalArgumentException("The key '" + key
            + "' doesn't exist in the database public/system.constants table, while the key must exist");
      }
    } catch (final SQLException se) {
      LOGGER.error("An SQL exception occured in findConstantByKey for key {}", key, se);
      throw se;
    }
    return constantValue;
  }

  /**
   * Get the shared constants. The connection is NOT closed after usage.
   *
   * @param con The DB connection.
   * @return Map with constant key/value.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static HashMap<String, String> getSharedConstants(final Connection con) throws SQLException {
    LOGGER.debug("Fetching shared constants");
    final HashMap<String, String> sharedConstants = new HashMap<>();

    try (final Statement constantPS = con.createStatement();
        final ResultSet rst = constantPS.executeQuery(GET_SHAREDCONSTANTS_QUERY)) {

      while (rst.next()) {
        sharedConstants.put(rst.getString(1), rst.getString(2));
      }
    } catch (final SQLException se) {
      LOGGER.error("An SQL exception occured in getSharedConstants", se);
      throw se;
    }

    return sharedConstants;
  }

  /**
   * Update a constant value in the database. The constant must exist.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   *
   * @param connection The connection to be used (won't be closed).
   * @param constantEnum The constant to set the value for. When it does not exist, an exception is thrown.
   * @param value The value of the constant
   * @throws SQLException When an error occurred communicating with the database or the constant is not found.
   */
  public static void setString(final Connection connection, final Enum<?> constantEnum, final String value) throws SQLException {
    setConstantByKey(connection, constantEnum.name(), value);
  }

  /**
   * Set constant value. The constant must exist. The connection is NOT closed after usage.
   *
   * @param con The DB connection
   * @param key The key of the constant to set
   * @param value The value of the constant to set
   * @throws SQLException If an error occurred communicating with the database or the constant is not found.
   */
  public static void setConstantByKey(final Connection con, final String key, final String value)
      throws SQLException {
    try (final PreparedStatement ps = con.prepareStatement(SET_CONSTANT_QUERY)) {
      QueryUtil.setValues(ps, key, value);
      ps.execute();
    } catch (final SQLException se) {
      LOGGER.error("An SQL exception occured in setConstantByKey for key {}", key, se);
      throw se;
    }
  }

}
