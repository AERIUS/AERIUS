/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.configuration;

import java.sql.Connection;
import java.sql.SQLException;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.shared.config.Settings;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.util.EnvUtils;

@SuppressWarnings("rawtypes")
final class SettingsUtil {
  private SettingsUtil() {
  }

  public static void addSetting(final Connection con, final Settings settings, final SharedConstantsEnum constant) throws SQLException {
    settings.setSetting(constant, ConstantRepository.getString(con, constant));
  }

  public static void addOptionalSetting(final Connection con, final Settings settings, final Enum constant) throws SQLException {
    settings.setSetting(constant, ConstantRepository.getString(con, constant, false));
  }

  public static <E extends Enum<E>> void addEnumSetting(final Connection con, final Settings settings,
      final SharedConstantsEnum constant, final Class<E> enumClass) throws SQLException {
    settings.setSetting(constant, ConstantRepository.getEnum(con, constant, enumClass));
  }

  public static void addOptionalIntSetting(final Connection con, final Settings settings, final Enum constant) throws SQLException {
    settings.setSetting(constant, ConstantRepository.getNumber(con, constant, Integer.class, false));
  }

  public static void addIntSetting(final Connection con, final Settings settings, final Enum constant) throws SQLException {
    settings.setSetting(constant, ConstantRepository.getNumber(con, constant, Integer.class));
  }

  public static void addDoubleSetting(final Connection con, final Settings settings, final Enum constant) throws SQLException {
    settings.setSetting(constant, ConstantRepository.getNumber(con, constant, Double.class));
  }

  public static void addBooleanSetting(final Connection con, final Settings settings, final Enum constant) throws SQLException {
    settings.setSetting(constant, ConstantRepository.getBoolean(con, constant));
  }

  public static void addOptionalSettingPreferEnvironment(final Connection con, final Settings settings,
      final Enum constant) throws SQLException {
    addOptionalSettingPreferEnvironment(con, settings, constant, constant);
  }

  public static void addSettingPreferEnvironment(final Connection con, final Settings settings, final Enum constant) throws SQLException {
    addSettingPreferEnvironment(con, settings, constant, constant);
  }

  public static void addOptionalSettingPreferEnvironment(final Connection con, final Settings settings,
      final Enum destConstant, final Enum sourceConstant) throws SQLException {
    final String val = getOptionalSettingPreferEnvironment(con, sourceConstant);
    if (val != null) {
      settings.setSetting(destConstant, val);
    }
  }

  public static void addSettingPreferEnvironment(final Connection con, final Settings settings, final Enum destConstant,
      final Enum sourceConstant) throws SQLException {
    settings.setSetting(destConstant, getSettingPreferEnvironment(con, sourceConstant));
  }

  public static String getOptionalSettingPreferEnvironment(final Connection con, final Enum constant) throws SQLException {
    final String envProperty = EnvUtils.getEnvProperty(constant.name());
    return envProperty != null ? envProperty : ConstantRepository.getString(con, constant, false);
  }

  public static String getSettingPreferEnvironment(final Connection con, final Enum constant) throws SQLException {
    final String envProperty = EnvUtils.getEnvProperty(constant.name());
    return envProperty != null ? envProperty : ConstantRepository.getString(con, constant);
  }
}
