/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.configuration;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.aerius.search.domain.SearchCapability;
import nl.aerius.search.domain.SearchRegion;
import nl.overheid.aerius.db.adms.MetSiteRepository;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.MainTab;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.sector.SectorIconUtil;
import nl.overheid.aerius.shared.domain.source.road.RoadLayerStyle;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.building.BuildingLimits;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Configuration repository for NCA customer variant.
 */
public class NcaApplicationConfigurationRepository extends AbstractApplicationConfigurationRepository {

  private static final List<ResultView> NCA_RESULT_VIEWS = Arrays.asList(ResultView.PERCENTAGE_CRITICAL_LEVELS, ResultView.DEPOSITION_DISTRIBUTION,
      ResultView.MARKERS, ResultView.HABITAT_TYPES);

  private static final List<CalculationMethod> CALCULATION_TYPES = Arrays.asList(CalculationMethod.QUICK_RUN, CalculationMethod.FORMAL_ASSESSMENT,
      CalculationMethod.CUSTOM_POINTS, CalculationMethod.NATURE_AREA);

  private static final List<SituationType> NCA_SITUATION_TYPES = Arrays.asList(SituationType.REFERENCE, SituationType.TEMPORARY,
      SituationType.PROPOSED, SituationType.OFF_SITE_REDUCTION, SituationType.COMBINATION_REFERENCE, SituationType.COMBINATION_PROPOSED);

  private static final List<EmissionResultKey> EMISSION_RESULT_KEYS = Arrays.asList(EmissionResultKey.NOXNH3_DEPOSITION,
      EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NH3_CONCENTRATION);

  private static final List<SectorGroup> NCA_ALLOWED_SECTORGROUPS = Arrays.asList(SectorGroup.ENERGY, SectorGroup.AGRICULTURE,
      SectorGroup.ROAD_TRANSPORTATION, SectorGroup.INDUSTRY, SectorGroup.OTHER);

  private static final List<AssessmentCategory> ASSESSMENT_CATEGORIES = Arrays.asList(AssessmentCategory.ECOLOGY, AssessmentCategory.HUMAN_HEALTH,
      AssessmentCategory.MONITORING);

  private static final List<Substance> EMISSION_SUBSTANCES_ROAD = Arrays.asList(Substance.NOX, Substance.NH3);

  private static final List<CalculationJobType> CALCULATION_JOB_TYPES = Arrays.asList(CalculationJobType.PROCESS_CONTRIBUTION,
      CalculationJobType.MAX_TEMPORARY_EFFECT, CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION, CalculationJobType.SINGLE_SCENARIO);

  private static final List<CalculationJobType> AVAILABLE_EXPORT_TYPES = Arrays.asList(CalculationJobType.PROCESS_CONTRIBUTION,
      CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION);

  private static final List<RoadLayerStyle> ROAD_LAYER_STYLES = Arrays.asList(RoadLayerStyle.ROAD_TYPE, RoadLayerStyle.MAX_SPEED,
      RoadLayerStyle.TRAFFIC_VOLUME, RoadLayerStyle.ROAD_BARRIER, RoadLayerStyle.ROAD_BARRIER_TYPE, RoadLayerStyle.ROAD_BARRIER_HEIGHT,
      RoadLayerStyle.ROAD_BARRIER_POROSITY, RoadLayerStyle.ROAD_EMISSION);
  // Conversion factor to convert kg/m/y to g/km/s => (1000 * 1000) / (365 * 24 * 3600)
  private static final double KG_TO_G_KM_TO_S_YEAR_TO_SECONDS = 1_000_000 / (365 * 24 * 3600D);

  private static final double CALCULATION_DISTANCE_DEFAULT = 7_500D;

  private static final List<MainTab> ALL_MAIN_TABS = Arrays.asList(MainTab.CALCULATED_INFO, MainTab.SUMMARY, MainTab.DECISION_FRAMEWORK,
      MainTab.SCENARIO_RESULTS);

  @Override
  protected Theme getTheme() {
    return Theme.NCA;
  }

  @Override
  protected Set<AppFlag> getAppFlags() {
    final Set<AppFlag> set = new HashSet<>();

    set.add(AppFlag.ENABLE_ADMS_EXPORT);
    set.add(AppFlag.ENABLE_COOKIE_POLICY_POPUP);
    set.add(AppFlag.ENABLE_DEMO_CALCULATION);
    set.add(AppFlag.ENABLE_INTERNAL);
    set.add(AppFlag.ENABLE_MET_SITES_LAYER);
    set.add(AppFlag.HAS_BUILDING_MINIMUM_HEIGHT_LARGER_ZERO);
    set.add(AppFlag.HAS_CALCULATION_POINTS_CHARACTERISTICS);
    set.add(AppFlag.HAS_CALCULATION_PROJECT_CATEGORY);
    set.add(AppFlag.HAS_CALCULATION_PERMIT_AREA);
    set.add(AppFlag.HAS_DEVELOPMENT_PRESSURE_SEARCH);
    set.add(AppFlag.SHOW_ABOUT_MENU);
    set.add(AppFlag.SHOW_CONTACT_MENU);
    set.add(AppFlag.SHOW_ASSESSMENT_AREA_DIRECTIVES);
    set.add(AppFlag.SHOW_PRIVACY_STATEMENT);
    set.add(AppFlag.SHOW_MAP_ON_JOB_PAGE);
    set.add(AppFlag.VALIDATE_CONVEX_GEOMETRIES);
    set.add(AppFlag.USE_ROAD_CUSTOM_UNITS_G_KM_S);
    set.add(AppFlag.ZONE_OF_INFLUENCE_MAP_LAYER);
    set.add(AppFlag.ARCHIVE_PROJECTS_MAP_LAYER);
    set.add(AppFlag.REDUCED_NATURE_METADATA_FIELDS);
    return set;
  }

  @Override
  protected List<MetSite> getMetSites(final Connection con) throws SQLException {
    return MetSiteRepository.getMetSites(con);
  }

  @Override
  protected double getCalculationDistanceDefault() {
    return CALCULATION_DISTANCE_DEFAULT;
  }

  @Override
  protected List<SituationType> getAvailableSituationTypes() {
    return NCA_SITUATION_TYPES;
  }

  @Override
  protected List<ResultView> getResultViews() {
    return NCA_RESULT_VIEWS;
  }

  @Override
  protected CalculationMethod getDefaultCalculationMethod() {
    return CalculationMethod.QUICK_RUN;
  }

  @Override
  protected List<SectorGroup> getSectorGroups() {
    return NCA_ALLOWED_SECTORGROUPS;
  }

  @Override
  protected SearchRegion getSearchRegion() {
    return SearchRegion.UK;
  }

  @Override
  protected List<SearchCapability> getSearchCapabilities() {
    return Arrays.asList(SearchCapability.BASIC_INFO, SearchCapability.RECEPTOR, SearchCapability.COORDINATE, SearchCapability.ASSESSMENT_AREA);
  }

  @Override
  protected List<FarmEmissionFactorType> getCustomFarmAnimalHousingEmissionFactorTypes() {
    return Arrays.asList(FarmEmissionFactorType.PER_ANIMAL_PER_DAY, FarmEmissionFactorType.PER_ANIMAL_PER_YEAR);
  }

  @Override
  @Deprecated
  protected List<FarmEmissionFactorType> getCustomFarmLodgingEmissionFactorTypes() {
    return Arrays.asList(FarmEmissionFactorType.PER_ANIMAL_PER_DAY, FarmEmissionFactorType.PER_ANIMAL_PER_YEAR);
  }

  @Override
  protected List<CalculationMethod> getCalculationMethods() {
    return CALCULATION_TYPES;
  }

  @Override
  protected List<AssessmentCategory> getAssessmentCategories() {
    return ASSESSMENT_CATEGORIES;
  }

  @Override
  protected List<EmissionResultKey> getEmissionResultKeys() {
    return EMISSION_RESULT_KEYS;
  }

  @Override
  protected List<String> getConvertibleGeometrySystems() {
    return Arrays.asList(Proj4BoilerPlate.EPSG_29903);
  }

  @Override
  protected void addProjections(final ApplicationConfiguration appConfig) {
    final HashMap<String, String> projections = new HashMap<>();
    projections.put(Proj4BoilerPlate.EPSG_27700, Proj4BoilerPlate.BNG_PROJ4_DEF);
    projections.put(Proj4BoilerPlate.EPSG_29902, Proj4BoilerPlate.TM65_PROJ4_DEF);
    projections.put(Proj4BoilerPlate.EPSG_29903, Proj4BoilerPlate.TM75_PROJ4_DEF);

    appConfig.setProjections(projections);
  }

  @Override
  protected BuildingLimits getBuildingLimits() {
    return ADMSLimits.INSTANCE;
  }

  @Override
  protected List<RoadLayerStyle> getRoadLayerStyles() {
    return ROAD_LAYER_STYLES;
  }

  @Override
  protected List<Substance> getEmissionSubstancesRoad() {
    return EMISSION_SUBSTANCES_ROAD;
  }

  /**
   * Convert road emission from kg/m/y to g/km/s
   */
  @Override
  protected double getRoadEmissionDisplayConversionFactor() {
    return KG_TO_G_KM_TO_S_YEAR_TO_SECONDS;
  }

  @Override
  protected List<CalculationJobType> getCalculationJobTypes() {
    return CALCULATION_JOB_TYPES;
  }

  @Override
  protected List<CalculationJobType> getAvailableExportReportOptions() {
    return AVAILABLE_EXPORT_TYPES;
  }

  @Override
  protected List<EmissionResultKey> getSupportedSummaryResultTypes() {
    return Arrays.asList(EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NOXNH3_DEPOSITION);
  }

  @Override
  protected List<ResultStatisticType> getSupportedSummaryStatisticTypes() {
    return Arrays.asList(ResultStatisticType.MAX_INCREASE, ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL, ResultStatisticType.MAX_TOTAL,
        ResultStatisticType.MAX_TOTAL_PERCENTAGE_CRITICAL_LEVEL);
  }

  @Override
  protected List<SummaryHexagonType> getSupportedSummaryHexagonTypes() {
    return Arrays.asList(SummaryHexagonType.CUSTOM_CALCULATION_POINTS, SummaryHexagonType.RELEVANT_HEXAGONS);
  }

  @Override
  protected Set<InputTypeViewMode> getInputTypeViewModes() {
    return Stream.of(InputTypeViewMode.values()).collect(Collectors.toSet());
  }

  @Override
  protected Map<Integer, SectorIcon> getSectorIcons() {
    return SectorIconUtil.nca();
  }

  @Override
  protected Map<String, ColorRangeType> getRoadTrafficVolumeColors() {
    final Map<String, ColorRangeType> colors = new HashMap<>();
    colors.put("", ColorRangeType.ROAD_TRAFFIC_VOLUME);
    colors.put("car", ColorRangeType.ROAD_TRAFFIC_VOLUME_CAR);
    colors.put("tax", ColorRangeType.ROAD_TRAFFIC_VOLUME_TAX);
    colors.put("bus", ColorRangeType.ROAD_TRAFFIC_VOLUME_BUS);
    colors.put("hgv", ColorRangeType.ROAD_TRAFFIC_VOLUME_HGV);
    colors.put("lgv", ColorRangeType.ROAD_TRAFFIC_VOLUME_LGV);
    colors.put("mot", ColorRangeType.ROAD_TRAFFIC_VOLUME_MOT);
    return colors;
  }

  @Override
  protected List<MainTab> getAvailableTabs() {
    return ALL_MAIN_TABS;
  }
}
