/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.legend.ColorItem;
import nl.overheid.aerius.shared.domain.legend.ColorItemType;
import nl.overheid.aerius.shared.util.ColorUtil;

public final class ColorItemsRepository {

  private enum RepositoryAttribute implements Attribute {
    COLOR_ITEM_TYPE,
    ITEM_VALUE,
    COLOR,
    LINE_WIDTH,
    SORT_ORDER;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ROOT);
    }
  }

  private static final Attributes COLOR_ITEM_ATTRIBUTES = new Attributes(
      RepositoryAttribute.COLOR_ITEM_TYPE,
      RepositoryAttribute.ITEM_VALUE,
      RepositoryAttribute.COLOR,
      RepositoryAttribute.LINE_WIDTH,
      RepositoryAttribute.SORT_ORDER);

  private static final Query COLOR_ITEMS_QUERY = QueryBuilder.from("system.color_items")
      .select(COLOR_ITEM_ATTRIBUTES)
      .orderBy(RepositoryAttribute.COLOR_ITEM_TYPE, RepositoryAttribute.SORT_ORDER)
      .getQuery();

  /**
   * Retrieves all available color items from the database.
   *
   * @param con db connection
   * @return Map with ColorItemType to the content of the color items
   * @throws SQLException
   */
  public static Map<ColorItemType, List<ColorItem>> getColorItems(final Connection con) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(COLOR_ITEMS_QUERY.get())) {

      final ResultSet rs = stmt.executeQuery();
      final Map<ColorItemType, List<ColorItem>> result = new HashMap<>();

      while (rs.next()) {
        final ColorItemType colorItemType = QueryUtil.getEnum(rs, RepositoryAttribute.COLOR_ITEM_TYPE, ColorItemType.class);
        result.putIfAbsent(colorItemType, new ArrayList<>());

        final ColorItem colorItem = new ColorItem(
            QueryUtil.getString(rs, RepositoryAttribute.ITEM_VALUE),
            ColorUtil.webColor(QueryUtil.getString(rs, RepositoryAttribute.COLOR)),
            QueryUtil.getInt(rs, RepositoryAttribute.LINE_WIDTH),
            QueryUtil.getInt(rs, RepositoryAttribute.SORT_ORDER));
        result.get(colorItemType).add(colorItem);
      }

      return result;
    }
  }

}
