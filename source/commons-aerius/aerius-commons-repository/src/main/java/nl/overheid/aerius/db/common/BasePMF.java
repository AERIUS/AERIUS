/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.SQLException;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.enums.ConstantsEnum;

/**
 * Abstract base class for the Persistence Manager Factory (PMF) classes. This
 * class provides convenience methods to get the database version.
 */
public abstract class BasePMF implements PMF {

  /**
   * Same as #getDatabaseVersion(Connection) only without the need to give a connection.
   * The connection is automatically created and closed.
   * @return current database version.
   * @throws SQLException On SQL error.
   */
  @Override
  public String getDatabaseVersion() throws SQLException {
    try (Connection con = getConnection()) {
      return getDatabaseVersion(con);
    }
  }

  /**
   * Returns the database version, which is set as comment of the current schema.
   * @param con The connection to use (will not be closed).
   * @return The database version.
   * @throws SQLException On SQL error.
   */
  public static String getDatabaseVersion(final Connection con) throws SQLException {
    return ConstantRepository.getString(con, ConstantsEnum.CURRENT_DATABASE_VERSION);
  }

}
