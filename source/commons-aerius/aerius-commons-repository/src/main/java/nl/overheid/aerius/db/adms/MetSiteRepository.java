/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.adms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.postgis.PGgeometry;
import org.postgis.Point;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.EntityCollector;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.DefaultWhereClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSiteDataset;
import nl.overheid.aerius.shared.domain.calculation.MetDatasetType;
import nl.overheid.aerius.shared.domain.calculation.MetSurfaceCharacteristics;

/**
 * Database Repository class to get Meteorological Site information used in ADMS calculations.
 */
public class MetSiteRepository {

  private enum RepositoryAttribute implements Attribute {
    MET_SITE_ID,
    DATASET_TYPE,
    ROUGHNESS,
    ALBEDO,
    PRIESTLY_TAYLOR,
    MIN_MONIN_OBUKHOV_LENGTH,
    WIND_IN_SECTORS,
    REMARKS;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ROOT);
    }
  }

  private static final Query GET_ALL_SURFACE_CHARACTERISTICS = QueryBuilder.from("met_sites")
      .join(new JoinClause("met_site_datasets", RepositoryAttribute.MET_SITE_ID))
      .select(RepositoryAttribute.MET_SITE_ID, QueryAttribute.NAME, QueryAttribute.GEOMETRY,
          RepositoryAttribute.DATASET_TYPE, QueryAttribute.YEAR, RepositoryAttribute.REMARKS)
      .orderBy(QueryAttribute.NAME)
      .getQuery();

  private static final Query GET_SURFACE_CHARACTERISTICS = QueryBuilder.from("met_sites")
      .join(new JoinClause("met_site_datasets", RepositoryAttribute.MET_SITE_ID))
      .select(RepositoryAttribute.ROUGHNESS, RepositoryAttribute.ALBEDO, RepositoryAttribute.PRIESTLY_TAYLOR,
          RepositoryAttribute.MIN_MONIN_OBUKHOV_LENGTH, RepositoryAttribute.WIND_IN_SECTORS)
      .select(new SelectClause("ST_Y(ST_Transform(geometry, 4326))", QueryAttribute.Y_COORD.attribute()))
      .where(RepositoryAttribute.MET_SITE_ID, QueryAttribute.YEAR)
      .where(new DefaultWhereClause(RepositoryAttribute.DATASET_TYPE, "met_dataset_type"))
      .getQuery();

  private final PMF pmf;

  public MetSiteRepository(final PMF pmf) {
    this.pmf = pmf;
  }

  public Optional<MetSiteSurfaceCharacteristics> getMetSiteSurfaceCharacteristics(final int metSiteId, final MetDatasetType datasetType,
      final String year) throws SQLException {
    try (final Connection connection = pmf.getConnection()) {
      return getMetSiteSurfaceCharacteristics(connection, metSiteId, datasetType, year);
    }
  }

  public static Optional<MetSiteSurfaceCharacteristics> getMetSiteSurfaceCharacteristics(final Connection connection, final int metSiteId,
      final MetDatasetType datasetType, final String year) throws SQLException {
    try (final PreparedStatement stmt = connection.prepareStatement(GET_SURFACE_CHARACTERISTICS.get())) {
      GET_SURFACE_CHARACTERISTICS.setParameter(stmt, RepositoryAttribute.MET_SITE_ID, metSiteId);
      GET_SURFACE_CHARACTERISTICS.setParameter(stmt, RepositoryAttribute.DATASET_TYPE,
          datasetType == null ? null : datasetType.name().toLowerCase(Locale.ROOT));
      GET_SURFACE_CHARACTERISTICS.setParameter(stmt, QueryAttribute.YEAR, year);
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        return Optional.of(getMetSiteSurfaceCharacteristics(rs));
      }
      return Optional.empty();
    }
  }

  public static List<MetSite> getMetSites(final Connection connection) throws SQLException {
    final MetSiteCollector collector = new MetSiteCollector();

    try (final PreparedStatement stmt = connection.prepareStatement(GET_ALL_SURFACE_CHARACTERISTICS.get())) {
      final ResultSet rs = stmt.executeQuery();
      collector.collectEntities(rs);
    }

    return collector.getEntities();
  }

  private static MetSiteSurfaceCharacteristics getMetSiteSurfaceCharacteristics(final ResultSet rs) throws SQLException {
    final MetSurfaceCharacteristics characteristics = MetSurfaceCharacteristics.builder()
        .roughness(QueryUtil.getDouble(rs, RepositoryAttribute.ROUGHNESS))
        .surfaceAlbedo(QueryUtil.getDouble(rs, RepositoryAttribute.ALBEDO))
        .priestleyTaylorParameter(QueryUtil.getDouble(rs, RepositoryAttribute.PRIESTLY_TAYLOR))
        .minMoninObukhovLength(QueryUtil.getDouble(rs, RepositoryAttribute.MIN_MONIN_OBUKHOV_LENGTH))
        .windInSectors(QueryUtil.getBoolean(rs, RepositoryAttribute.WIND_IN_SECTORS))
        .build();

    return new MetSiteSurfaceCharacteristics(characteristics, QueryUtil.getDouble(rs, QueryAttribute.Y_COORD));
  }

  private static class MetSiteCollector extends EntityCollector<MetSite> {

    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryUtil.getInt(rs, RepositoryAttribute.MET_SITE_ID);
    }

    @Override
    public MetSite fillEntity(final ResultSet rs) throws SQLException {
      final MetSite metSite = new MetSite();
      metSite.setName(QueryAttribute.NAME.getString(rs));
      if (QueryUtil.getObject(rs, QueryAttribute.GEOMETRY) instanceof final PGgeometry pgGeometry
          && pgGeometry.getGeometry() instanceof final Point point) {
        metSite.setX(point.getX());
        metSite.setY(point.getY());
      }
      return metSite;
    }

    @Override
    public void appendEntity(final MetSite entity, final ResultSet rs) throws SQLException {
      final MetSiteDataset dataset = new MetSiteDataset();
      dataset.setDatasetType(MetDatasetType.safeValueOf(QueryUtil.getString(rs, RepositoryAttribute.DATASET_TYPE)));
      dataset.setYear(QueryAttribute.YEAR.getString(rs));
      dataset.setRemarks(QueryUtil.getString(rs, RepositoryAttribute.REMARKS));
      entity.getDatasets().add(dataset);
    }

  }
}
