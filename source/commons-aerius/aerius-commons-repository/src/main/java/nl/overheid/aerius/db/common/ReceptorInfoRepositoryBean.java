/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.shared.domain.info.StaticReceptorInfo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

public class ReceptorInfoRepositoryBean {

  private static final Logger LOG = LoggerFactory.getLogger(ReceptorInfoRepositoryBean.class);

  private final PMF pmf;

  public ReceptorInfoRepositoryBean(final PMF pmf) {
    this.pmf = pmf;
  }

  public void fillReceptorInfoForNature(final StaticReceptorInfo info, final int receptorId, final DBMessagesKey dbMessagesKey)
      throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      info.setNaturaInfo(ReceptorInfoRepository.getNaturaAreaInfo(con, receptorId, dbMessagesKey));
      info.setHabitatTypeInfo(ReceptorInfoRepository.getReceptorHabitatAreaInfo(con, receptorId, dbMessagesKey));
      info.setExtraAssessmentHabitatTypeInfo(ReceptorInfoRepository.getReceptorExtraAssessmentHabitatsInfo(con, receptorId, dbMessagesKey));
    } catch (final SQLException e) {
      LOG.error("SQL error while filling ReceptorInfo for receptor {}", receptorId, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public List<SummaryHexagonType> getHexagonTypes(final int receptorId) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ReceptorInfoRepository.getHexagonTypes(con, receptorId);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting hexagon types for receptor", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public EmissionResults getBackgroundEmissionResult(final Point point, final int receptorId, final int year) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ReceptorInfoRepository.getBackgroundEmissionResult(con, point, receptorId, year);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting Backgroud Info from receptor", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public EmissionResults getBackgroundEmissionResultERK(final Point point, final int receptorId, final int year, final EmissionResultKey erk)
      throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ReceptorInfoRepository.getBackgroundEmissionResultERK(con, point, receptorId, year, erk);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting Backgroud Info from receptor for specific emssion result", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }
}
