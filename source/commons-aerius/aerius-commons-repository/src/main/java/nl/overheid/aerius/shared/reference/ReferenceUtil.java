/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.reference;

import nl.overheid.aerius.shared.domain.ProductProfile;

/**
 * Utility class that exposes only the relevant reference generating and validation functionality.
 * Outside of this package, we don't need to know about the specific implementations of the different
 * {@link ReferenceGenerator} versions.
 */
public final class ReferenceUtil {

  private ReferenceUtil() {
  }

  /**
   * Generate a reference based on the product profile.
   *
   * @param productProfile
   * @return new reference
   */
  public static String generateReference(final ProductProfile productProfile) {
    return productProfile == ProductProfile.LBV_POLICY ? generateLbvReference() :  generatePermitReference();
  }

  /**
   * Generates a permit reference.
   */
  public static String generatePermitReference() {
    return ReferenceGeneratorFactory.createCurrentReferenceGenerator().generateReference(ReferenceType.PERMIT);
  }

  /**
   * Generates a Lbv reference.
   */
  public static String generateLbvReference() {
    return ReferenceGeneratorFactory.createCurrentReferenceGenerator().generateReference(ReferenceType.LBV);
  }

  /**
   * Generates a melding reference.
   * @deprecated Melding is not supported anymore
   */
  @Deprecated
  public static String generateMeldingReference() {
    return ReferenceGeneratorFactory.createCurrentReferenceGenerator().generateReference(ReferenceType.MELDING);
  }

  /**
   * Validates that a string is a valid permit reference. See {@link #validateReference}.
   */
  public static boolean validatePermitReference(final String reference) {
    return validateReference(ReferenceType.PERMIT, reference);
  }

  /**
   * Validates that a string is a valid permit reference. See {@link #validateReference}.
   */
  public static boolean validateMeldingReference(final String reference) {
    return validateReference(ReferenceType.MELDING, reference);
  }

  /**
   * Validates that a string is a valid reference according to {@link ReferenceGenerator#validateReference}.
   * Because of backwards compatibility, all {@link ReferenceGenerator} versions from the current one down
   * to the first version are tried.
   */
  private static boolean validateReference(final ReferenceType type, final String reference) {
    boolean valid = false;
    for (byte version = ReferenceGeneratorFactory.CURRENT_VERSION; version >= 0 && !valid; version--) {
      valid = ReferenceGeneratorFactory.createReferenceGenerator(version).validateReference(type, reference);
    }
    return valid;
  }

  /**
   * Validates that a string is a valid reference according to {@link ReferenceGenerator#validateReference}.
   * Because of backwards compatibility, all {@link ReferenceGenerator} versions from the current one down
   * to the first version are tried.
   */
  public static boolean validateAnyReference(final String reference) {
    boolean valid = false;
    for (byte version = ReferenceGeneratorFactory.CURRENT_VERSION; version >= 0 && !valid; version--) {
      valid = ReferenceGeneratorFactory.createReferenceGenerator(version).validateReference(reference);
    }
    return valid;
  }

  /**
   * Attempts to find the version used to generate the reference.
   * Because of backwards compatibility, all {@link ReferenceGenerator} versions from the current one down
   * to the first version are tried to see if the version number can be decoded.
   * @throws Exception When version number could not be decoded.
   */
  public static byte getVersion(final String reference) throws InvalidReferenceException {
    Byte referenceVersion = null;
    for (byte version = ReferenceGeneratorFactory.CURRENT_VERSION; version >= 0 && referenceVersion == null; version--) {
      try {
        referenceVersion = ReferenceGeneratorFactory.createReferenceGenerator(version).getVersion(reference);
      } catch (final RuntimeException | InvalidReferenceException e) {
        // do nothing, try earlier version
      }
    }
    if (referenceVersion == null) {
      throw new InvalidReferenceException(reference);
    }
    return referenceVersion;
  }
}
