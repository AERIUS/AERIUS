/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.reference;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Class that can generate a reference for a permit, melding, or priority (sub)project.
 * It's an expansion of the V1 functionality. This version adds two random bytes to the
 * payload to increase uniqueness. This is because duplicate references can occur when multiple
 * workers start up simultaneously (e.g. after a reboot) and pick items from the queue in the
 * same decisecond. See Mantis #1791.
 */
class ReferenceGeneratorV2 extends ReferenceGeneratorV1 {

  public static final byte VERSION_ID = 1;

  private static final List<ReferenceType> SUPPORTED_REFERENCE_TYPES = Arrays.asList(
      ReferenceType.MELDING,
      ReferenceType.PERMIT,
      ReferenceType.LBV);

  private static final int NUMBYTES_IDENTIFIER = 1;
  private static final int NUMBYTES_PAYLOAD_TIME = 4;
  private static final int NUMBYTES_PAYLOAD_RANDOM = 2;
  private static final int NUMBYTES_CHECKSUM = 2;
  private static final int NUMBYTES_PAYLOAD = NUMBYTES_PAYLOAD_TIME + NUMBYTES_PAYLOAD_RANDOM;
  private static final int NUMBYTES_TOTAL = NUMBYTES_IDENTIFIER + NUMBYTES_PAYLOAD + NUMBYTES_CHECKSUM;

  private static final Random random = new Random();

  @Override
  public byte getGeneratorVersion() {
    return VERSION_ID;
  }

  /**
   * Generates the payload for this version, which uses the current time plus two random bytes.
   */
  @Override
  protected byte[] generatePayload() {
    final byte[] timePayload = makeByteArray(generateCurrentTimePayload(), NUMBYTES_PAYLOAD_TIME);
    final byte[] randomPayload = new byte[NUMBYTES_PAYLOAD_RANDOM];
    random.nextBytes(randomPayload);
    return concatByteArrays(timePayload, randomPayload);
  }

  /**
   * Returns whether the given reference type is supported by this ReferenceGenerator.
   */
  @Override
  protected boolean isReferenceTypeSupported(final ReferenceType type) {
    return SUPPORTED_REFERENCE_TYPES.contains(type);
  }

  /**
   * Validate the checksum of the reference its data part.
   */
  @Override
  protected boolean validateChecksum(final String reference) {
    try {
      final byte[] bytes = decodeReference(reference);
      final byte[] data = Arrays.copyOfRange(bytes, 0, NUMBYTES_IDENTIFIER + NUMBYTES_PAYLOAD);
      final byte[] checksum = Arrays.copyOfRange(bytes, NUMBYTES_IDENTIFIER + NUMBYTES_PAYLOAD, NUMBYTES_TOTAL);
      return Arrays.equals(checksum, makeByteArray(CyclicRedundancyCheck.crc16(data), NUMBYTES_CHECKSUM));
    } catch (final RuntimeException | InvalidReferenceException e) {
      return false;
    }
  }

  @Override
  protected int getExpectedBytesTotal() {
    return NUMBYTES_TOTAL;
  }

}
