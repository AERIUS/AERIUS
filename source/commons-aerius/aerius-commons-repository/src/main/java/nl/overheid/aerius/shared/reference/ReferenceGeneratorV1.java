/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.reference;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import com.google.bitcoin.core.Base58;

/**
 * Class that can generate a reference for a permit or a melding. The reference is made based
 * on the current system time because we need something unique and the request id's are not yet
 * known at this time. The reference is self checking using a checksum (CRC16). It contains a
 * version and type number. The result is encoded into BASE58 for ease of input.
 * Validation functions are also included.
 */
class ReferenceGeneratorV1 implements ReferenceGenerator {

  public static final byte VERSION_ID = 0;

  private static final List<ReferenceType> SUPPORTED_REFERENCE_TYPES = Arrays.asList(
      ReferenceType.MELDING,
      ReferenceType.PERMIT);

  private static final long CURRENTTIMEMILLIS_OFFSET = 1422766800000L; // January 1st 2015
  private static final double CURRENTTIMEMILLIS_PRECISION = 100.0; // ms -> ds (decisecond)

  private static final int NUMBYTES_IDENTIFIER = 1;
  private static final int NUMBYTES_PAYLOAD = 4;
  private static final int NUMBYTES_CHECKSUM = 2;
  private static final int NUMBYTES_TOTAL = NUMBYTES_IDENTIFIER + NUMBYTES_PAYLOAD + NUMBYTES_CHECKSUM;

  private static final byte BITMASK_3_BITS = 0b00000111;
  private static final byte BITMASK_5_BITS = 0b00011111;
  private static final int BITMASK_SHIFT = 5;

  @Override
  public byte getGeneratorVersion() {
    return VERSION_ID;
  }

  /**
   * Generates a reference of the given type and this version.
   */
  @Override
  public String generateReference(final ReferenceType type) {
    if (!isReferenceTypeSupported(type)) {
      throw new IllegalArgumentException("ReferenceType not supported");
    }

    final byte[] payload = generatePayload();
    final byte[] identifier = {generateIdentifier(type)};
    final byte[] data = concatByteArrays(identifier, payload);
    final byte[] checksum = makeByteArray(CyclicRedundancyCheck.crc16(data), NUMBYTES_CHECKSUM);
    final byte[] reference = concatByteArrays(data, checksum);
    return Base58.encode(reference);
  }

  /**
   * Generates the payload for this version, which uses the current time.
   */
  protected byte[] generatePayload() {
    return makeByteArray(generateCurrentTimePayload(), NUMBYTES_PAYLOAD);
  }

  /**
   * Converts the current time in milliseconds to deciseconds and turns it into an appropriate
   * integer value. The returned integer is reversed (bitwise) for more entropy.
   */
  protected int generateCurrentTimePayload() {
    return Integer.reverse((int) (Integer.MIN_VALUE + Math.round((System.currentTimeMillis() - CURRENTTIMEMILLIS_OFFSET)
        / CURRENTTIMEMILLIS_PRECISION)));
  }

  /**
   * Take first 3 bits of type, and first 5 bits of version. Then shift type 5 positions to the
   * left and merge then into a single byte.
   */
  protected byte generateIdentifier(final ReferenceType type) {
    final byte reftype = (byte) type.ordinal();
    return (byte) (((reftype & BITMASK_3_BITS) << BITMASK_SHIFT) | (getGeneratorVersion() & BITMASK_5_BITS));
  }

  @Override
  public boolean validateReference(final ReferenceType type, final String reference) {
    try {
      return isReferenceTypeSupported(type)
          && validateReference(reference)
          && getReferenceType(reference) == type;
    } catch (final RuntimeException | InvalidReferenceException e) {
      return false;
    }
  }

  @Override
  public boolean validateReference(final String reference) {
    try {
      return validateChecksum(reference)
          && getVersion(reference) == getGeneratorVersion();
    } catch (final RuntimeException | InvalidReferenceException e) {
      return false;
    }
  }

  /**
   * Returns whether the given reference type is supported by this ReferenceGenerator.
   */
  protected boolean isReferenceTypeSupported(final ReferenceType type) {
    return SUPPORTED_REFERENCE_TYPES.contains(type);
  }

  /**
   * Validate the checksum of the reference its data part.
   */
  protected boolean validateChecksum(final String reference) {
    try {
      final byte[] bytes = decodeReference(reference);
      final byte[] data = Arrays.copyOfRange(bytes, 0, NUMBYTES_IDENTIFIER + NUMBYTES_PAYLOAD);
      final byte[] checksum = Arrays.copyOfRange(bytes, NUMBYTES_IDENTIFIER + NUMBYTES_PAYLOAD, NUMBYTES_TOTAL);
      return Arrays.equals(checksum, makeByteArray(CyclicRedundancyCheck.crc16(data), NUMBYTES_CHECKSUM));
    } catch (final RuntimeException | InvalidReferenceException e) {
      return false;
    }
  }

  @Override
  public byte getVersion(final String reference) throws InvalidReferenceException {
    final byte[] bytes = decodeReference(reference);
    return (byte) (bytes[0] & BITMASK_5_BITS);
  }

  @Override
  public Instant getGeneratedTime(final String reference) throws InvalidReferenceException {
    final byte[] decoded = decodeReference(reference);
    // For this version, first byte supplies version, bytes 1-4 are for time.
    final byte[] timePayload = Arrays.copyOfRange(decoded, 1, 5);
    final int timeValue = new BigInteger(timePayload).intValue();
    final int reversedInt = Integer.reverse(timeValue);
    // For now this seems to work, but at some point we might run into some integer overflow issues.
    // That would be when:
    // Math.round((System.currentTimeMillis() - CURRENTTIMEMILLIS_OFFSET) / CURRENTTIMEMILLIS_PRECISION)) > Integer.MAX_VALUE * 2l
    // So at millisecond Integer.MAX_VALUE * 2l * CURRENTTIMEMILLIS_PRECISION + CURRENTTIMEMILLIS_OFFSET?
    // That might be somewhere around 2028-09-11T05:38:49.400Z.
    final long offset = ReferenceGeneratorV1.CURRENTTIMEMILLIS_OFFSET;
    final long timeMillis = Math.round((reversedInt - Integer.MIN_VALUE * 1L) * ReferenceGeneratorV1.CURRENTTIMEMILLIS_PRECISION)
        + offset;
    return Instant.ofEpochMilli(timeMillis);
  }

  /**
   * Returns the reference type that is hidden in the reference string
   * @throws InvalidReferenceException When the string could not be decoded using base58
   */
  protected ReferenceType getReferenceType(final String reference) throws InvalidReferenceException {
    final byte[] bytes = decodeReference(reference);
    final int index = (bytes[0] >> BITMASK_SHIFT) & BITMASK_3_BITS;
    return ReferenceType.values()[index];
  }

  /**
   * Base58-decodes string reference into byte array
   * @throws InvalidReferenceException When the string could not be decoded using base58
   */
  protected byte[] decodeReference(final String reference) throws InvalidReferenceException {
    final byte[] bytes = Base58.decode(reference);
    if (bytes.length != getExpectedBytesTotal()) {
      throw new InvalidReferenceException(reference);
    }
    return bytes;
  }

  protected int getExpectedBytesTotal() {
    return NUMBYTES_TOTAL;
  }

  /**
   * Gets the value as bytes, ensuring that the returned array has the exact given length.
   */
  protected static final byte[] makeByteArray(final long value, final int length) {
    final byte[] byteArray = BigInteger.valueOf(value).toByteArray();
    final byte[] fixedByteArray = new byte[length];
    System.arraycopy(byteArray, 0, fixedByteArray, 0, byteArray.length);
    return fixedByteArray;
  }

  /**
   * Concats a series of byte arrays into one single byte array.
   */
  protected static final byte[] concatByteArrays(final byte[]... byteArrays) {
    int length = 0;
    for (final byte[] inByteArray : byteArrays) {
      length += inByteArray.length;
    }

    if (length > 0) {
      final byte[] outByteArray = new byte[length];
      int pos = 0;
      for (final byte[] inByteArray : byteArrays) {
        System.arraycopy(inByteArray, 0, outByteArray, pos, inByteArray.length);
        pos += inByteArray.length;
      }
      return outByteArray;
    } else {
      return null;
    }
  }

}
