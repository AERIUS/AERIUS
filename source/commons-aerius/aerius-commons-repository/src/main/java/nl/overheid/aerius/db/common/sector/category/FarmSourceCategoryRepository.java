/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmSourceCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Repository class for Farm Source Categories.
 */
public final class FarmSourceCategoryRepository {

  private static final Query FARM_SOURCE_CATEGORY_QUERY = QueryBuilder
      .from("farm_source_emission_factors_view")
      .select(QueryAttribute.FARM_SOURCE_CATEGORY_ID, QueryAttribute.FARM_ANIMAL_CATEGORY_ID, QueryAttribute.FARM_EMISSION_FACTOR_TYPE,
          QueryAttribute.SUBSTANCE_ID, QueryAttribute.EMISSION_FACTOR, QueryAttribute.SECTOR_ID)
      .select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES)
      .getQuery();

  private FarmSourceCategoryRepository() {
    // Util class
  }

  public static List<FarmSourceCategory> findAllFarmSourceCategories(final Connection connection, final DBMessagesKey messagesKey)
      throws SQLException {
    final Map<Integer, FarmAnimalCategory> farmAnimalCategories = FarmAnimalCategoryRepository.getFarmAnimalCategories(connection, messagesKey);
    final FarmSourceCategoryCollector farmSourceCategoryCollector = new FarmSourceCategoryCollector(QueryAttribute.FARM_SOURCE_CATEGORY_ID,
        messagesKey, farmAnimalCategories);
    farmSourceCategoryCollector.collect(connection, FARM_SOURCE_CATEGORY_QUERY);
    return farmSourceCategoryCollector.getEntities();
  }

  private static class FarmSourceCategoryCollector extends AbstractCategoryCollector<FarmSourceCategory> {

    private final Map<Integer, FarmAnimalCategory> farmAnimalCategories;

    protected FarmSourceCategoryCollector(final Attribute categoryId, final DBMessagesKey messagesKey,
        final Map<Integer, FarmAnimalCategory> farmAnimalCategories) {
      super(categoryId, messagesKey);
      this.farmAnimalCategories = farmAnimalCategories;
    }

    @Override
    FarmSourceCategory getNewCategory() throws SQLException {
      return new FarmSourceCategory();
    }

    @Override
    void setDescription(final FarmSourceCategory category, final ResultSet rs) throws SQLException {
      DBMessages.setFarmSourceMessages(category, messagesKey);
    }

    @Override
    void setRemainingInfo(final FarmSourceCategory category, final ResultSet rs) throws SQLException {
      category.setSectorId(QueryUtil.getInt(rs, QueryAttribute.SECTOR_ID));
      category.setFarmEmissionFactorType(QueryUtil.getEnum(rs, QueryAttribute.FARM_EMISSION_FACTOR_TYPE, FarmEmissionFactorType.class));
      category.setFarmAnimalCategory(farmAnimalCategories.get(QueryUtil.getInt(rs, QueryAttribute.FARM_ANIMAL_CATEGORY_ID)));
      category.addEmissionFactor(Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs)),
          QueryUtil.getDouble(rs, QueryAttribute.EMISSION_FACTOR));
    }

    @Override
    public void appendEntity(final FarmSourceCategory category, final ResultSet rs) throws SQLException {
      category.addEmissionFactor(Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs)),
          QueryUtil.getDouble(rs, QueryAttribute.EMISSION_FACTOR));
    }
  }
}
