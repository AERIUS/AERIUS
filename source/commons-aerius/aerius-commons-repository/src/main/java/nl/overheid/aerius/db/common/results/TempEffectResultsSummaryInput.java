/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.util.List;

import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.scenario.SituationType;

public class TempEffectResultsSummaryInput {
  private final List<Integer> temporaryCalculationsIds;
  private final int referenceCalculationId;
  private final int offSiteReductionCalculationId;

  public TempEffectResultsSummaryInput(final List<Integer> temporaryCalculationsIds, final int referenceCalculationId, final int offSiteReductionCalculationId) {
    this.temporaryCalculationsIds = temporaryCalculationsIds;
    this.referenceCalculationId = referenceCalculationId;
    this.offSiteReductionCalculationId = offSiteReductionCalculationId;
  }

  public List<Integer> getTemporaryCalculationsIds() {
    return temporaryCalculationsIds;
  }

  public int getReferenceCalculationId() {
    return referenceCalculationId;
  }

  public int getOffSiteReductionCalculationId() {
    return offSiteReductionCalculationId;
  }

  public static TempEffectResultsSummaryInput fromSituationCalculations(final SituationCalculations situationCalculations) {
    return new TempEffectResultsSummaryInput(
        situationCalculations.getCalculationIdsOfSituationType(SituationType.TEMPORARY),
        situationCalculations.getCalculationIdOfSingleSituationType(SituationType.REFERENCE).orElse(0),
        situationCalculations.getCalculationIdOfSingleSituationType(SituationType.OFF_SITE_REDUCTION).orElse(0)
    );
  }
}
