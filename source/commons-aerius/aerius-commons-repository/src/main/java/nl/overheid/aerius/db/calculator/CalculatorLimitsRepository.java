/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.EmissionSourceLimits;

/**
 * Repository to get the Calculator limits. Specific for UI limits.
 */
public class CalculatorLimitsRepository {

  private static final Logger LOG = LoggerFactory.getLogger(CalculatorLimitsRepository.class);

  private final PMF pmf;

  public CalculatorLimitsRepository(final PMF pmf) {
    this.pmf = pmf;
  }

  /**
   * Retrieves the calculator limits from the database.
   *
   * @param con The connection to use.
   * @return calculator limits
   * @throws AeriusException
   */
  public EmissionSourceLimits getCalculatorLimits() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final EmissionSourceLimits cl = new EmissionSourceLimits();
      cl.setMaxSources(ConstantRepository.getNumber(con, ConstantsEnum.CALCULATOR_LIMITS_MAX_SOURCES, Integer.class));
      cl.setMaxLineLength(ConstantRepository.getNumber(con, ConstantsEnum.CALCULATOR_LIMITS_MAX_LINE_LENGTH, Integer.class));
      cl.setMaxPolygonSurface(ConstantRepository.getNumber(con, ConstantsEnum.CALCULATOR_LIMITS_MAX_POLYGON_SURFACE, Integer.class));
      return cl;
    } catch (final SQLException e) {
      LOG.error("SQL error getting calculator limits", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

}
