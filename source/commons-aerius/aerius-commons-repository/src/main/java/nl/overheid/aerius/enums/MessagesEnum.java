/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.enums;

/**
 * Messages constants that can be used everywhere,except the client and of which
 * the value is stored in the database. These messages are only messages that
 * are expected to be changed during the application life cycle, such as email
 * body text. In general messages text should be stored in the i18n modules.
 */
public enum MessagesEnum {

  /**
   * PDF OwN2000 calculation front page title
   */
  PDF_OWN2000_FRONT_PAGE_TITLE,

  /**
   * PDF OwN2000 calculation page title
   */
  PDF_OWN2000_PAGE_TITLE,

  /**
   * PDF nca calculation front page title
   */
  PDF_NCA_FRONT_PAGE_TITLE,

  /**
   * PDF nca calculation front page title row two
   */
  PDF_NCA_FRONT_PAGE_SUB_TITLE,

  /**
   * PDF nca calculation page title
   */
  PDF_NCA_PAGE_TITLE,

  /**
   * PDF OwN2000 edge effect front page title
   */
  PDF_OWN2000_EDGE_EFFECT_REPORT_FRONT_PAGE_TITLE,

  /**
   * PDF OwN2000 edge effect front page sub title
   */
  PDF_OWN2000_EDGE_EFFECT_REPORT_FRONT_PAGE_SUB_TITLE,

  /**
   * PDF OwN2000 edge effect page title
   */
  PDF_OWN2000_EDGE_EFFECT_REPORT_PAGE_TITLE,

  /**
   * PDF OwN2000 edge effect page sub title
   */
  PDF_OWN2000_EDGE_EFFECT_REPORT_PAGE_SUB_TITLE,

  /**
   * PDF OwN2000 edge effect page reference title
   */
  PDF_OWN2000_EDGE_EFFECT_REPORT_REFERENCE_TITLE,

  /**
   * PDF OwN2000 extra assessment front page title
   */
  PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_FRONT_PAGE_TITLE,

  /**
   * PDF OwN2000 extra assessment front page sub title
   */
  PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_FRONT_PAGE_SUB_TITLE,

  /**
   * PDF OwN2000 extra assessment page title
   */
  PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_PAGE_TITLE,

  /**
   * PDF OwN2000 extra assessment page sub title
   */
  PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_PAGE_SUB_TITLE,

  /**
   * PDF OwN2000 extra assessment page reference title
   */
  PDF_OWN2000_EXTRA_ASSESSMENT_REPORT_REFERENCE_TITLE,

  /**
   * PDF permit demand front page title
   */
  PDF_PERMIT_DEMAND_FRONT_PAGE_TITLE,

  /**
   * PDF permit demand not available page title
   */
  PDF_PERMIT_DEMAND_NOT_AVAILABLE_TITLE,

  /**
   * PDF permit demand available page title
   */
  PDF_PERMIT_DEMAND_AVAILABLE_TITLE,

  /**
   * PDF OwN2000 LBV Policy front page title
   */
  PDF_OWN2000_LBV_POLICY_FRONT_PAGE_TITLE,

  /**
   * PDF OwN2000 LBV Policy page title
   */
  PDF_OWN2000_LBV_POLICY_PAGE_TITLE,

  /**
   * PDF OwN2000 Single Scenario front page title
   */
  PDF_OWN2000_SINGLE_SCENARIO_FRONT_PAGE_TITLE,

  /**
   * PDF OwN2000 Single Scenario page title
   */
  PDF_OWN2000_SINGLE_SCENARIO_PAGE_TITLE,
}
