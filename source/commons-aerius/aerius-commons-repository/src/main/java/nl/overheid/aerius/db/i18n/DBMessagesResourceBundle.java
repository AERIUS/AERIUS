/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.i18n;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.LocalizationRepository.LocalizationObject;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * {@link ResourceBundle} that got its resources from a database.
 */
public final class DBMessagesResourceBundle extends ResourceBundle {
  private static final Logger LOG = LoggerFactory.getLogger(DBMessagesResourceBundle.class);

  private final Properties lookup;

  private DBMessagesResourceBundle(final Properties lookup) {
    this.lookup = lookup;
  }

  @Override
  protected Object handleGetObject(final String key) {
    final Object object = lookup.get(key);

    if (object == null) {
      LOG.warn("Unlocalized resource: {} for locale {}", key, getLocale());

      // FIXME Temp work-around for unlocalized resources (Default locale should contain all resources, which it doesn't, yet, and Locale.ROOT
      // could/should account for left-overs)
      if (getLocale().getLanguage().equals(LocaleUtils.getDefaultLocale().getLanguage())) {
        return "??:??";
      }
    }

    return object;
  }

  @Override
  public Enumeration<String> getKeys() {
    return Collections.enumeration(lookup.stringPropertyNames());
  }

  /**
   * {@link java.util.ResourceBundle.Control} for the {@link DBMessagesResourceBundle}.
   *
   * Gets localized resources for the given {@link Locale} from the {@link LocalizationRepository}.
   *
   * Does not (yet) care about regions. Only languages.
   */
  static class Control extends ResourceBundle.Control {
    private final PMF pmf;
    private final List<LocalizationObject> localizationObjects;

    /**
     * Constructor taking a {@link PMF} which will be used to connect to the
     * database.
     *
     * @param pmf
     *   to use for a database connection.
     */
    public Control(final PMF pmf, final List<LocalizationObject> localizationObjects) {
      this.pmf = pmf;
      this.localizationObjects = localizationObjects;
    }

    @Override
    public final ResourceBundle newBundle(final String baseName, final Locale locale, final String format,
        final ClassLoader loader, final boolean reload) {
      LOG.trace("Loading new bundle: {} > {} ", baseName, locale);

      // Since we only care about language and not about region, we're doing this check.
      if (locale.getLanguage().isEmpty()) {
        return null;
      }

      try (Connection con = pmf.getConnection()) {
        // Get the translations for the requested locale
        final DBMessagesResourceBundle dbMessagesResourceBundle =
            new DBMessagesResourceBundle(LocalizationRepository.getLocalizedMessages(con, locale, localizationObjects));

        // If the requested locale is not the default locale, set the default locale as parent
        final Locale defaultLocale = LocaleUtils.getDefaultLocale();
        if (!locale.getLanguage().equals(defaultLocale.getLanguage())) {
          LOG.info("Falling back to {} from {}", defaultLocale.getLanguage(), locale.getLanguage());
          dbMessagesResourceBundle.setParent(ResourceBundle.getBundle(baseName, defaultLocale, this));
        }

        return dbMessagesResourceBundle;
      } catch (final SQLException e) {
        LOG.warn("Could not retrieve translations for locale {} (falling back)", locale.toString(), e);
      }

      return null;
    }

    @Override
    public Locale getFallbackLocale(final String baseName, final Locale locale) {
      if (baseName == null) {
        throw new NullPointerException();
      }
      final Locale defaultLocale = LocaleUtils.getDefaultLocale();
      return locale.equals(defaultLocale) ? null : defaultLocale;
    }
  }

}
