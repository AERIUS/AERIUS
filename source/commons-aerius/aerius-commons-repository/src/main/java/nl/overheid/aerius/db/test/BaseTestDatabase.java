/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.test;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.Transaction;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Base class for Database tests. This class initializes the database connection
 * configuration in @Before state and closes the connection in @After.
 */
public class BaseTestDatabase {

  private static final Logger LOGGER = LoggerFactory.getLogger(BaseTestDatabase.class);

  protected static ExecutorService EXECUTOR;
  protected static Properties PROPS;

  private static final String TEST_DATABASE_PROPERTIES = "testdatabase.properties";
  private static final TestPMF PMF = new TestPMF(true);

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    EXECUTOR = Executors.newCachedThreadPool();
    PROPS = new Properties();
    PROPS.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(TEST_DATABASE_PROPERTIES));
    overrideWithSystemPropertyIfPresent(PROPS, "database.url");
    overrideWithSystemPropertyIfPresent(PROPS, "database.username");
    overrideWithSystemPropertyIfPresent(PROPS, "database.password");
    initPMF(PMF, PROPS.getProperty("database.url"), PROPS);
    assumeTrue(BaseTestDatabase::checkDatabaseAvailable, "Database not available");
    assumeTrue(BaseTestDatabase.checkDatabaseIsCalculator(), "Database not AERIUS Calculator DB");
  }

  @AfterAll
  public static void afterClass() {
    EXECUTOR.shutdown();
  }

  private static boolean checkDatabaseAvailable() {
    try (final Connection con = PMF.getConnection()) {
      return true;
    } catch (final RuntimeException e) {
      // DBMessages.init is called somewhere in the PMF, causing a runtime exception with cause SQLException
      // This is the main indication that database is not available.
      if (e.getCause() instanceof final SQLException sqlException) {
        return isDatabaseAvailableIndication(sqlException);
      } else {
        throw e;
      }
    } catch (final SQLException e) {
      return isDatabaseAvailableIndication(e);
    }
  }

  private static boolean isDatabaseAvailableIndication(final SQLException e) {
    // Use SQL state 08001 as indication that database is not available.
    return e instanceof final PSQLException psqlException && !"08001".equals(psqlException.getSQLState());
  }

  private static boolean checkDatabaseIsCalculator() throws SQLException {
    try (final Connection con = PMF.getConnection();
        final PreparedStatement stmt =
            con.prepareStatement("SELECT 1 FROM information_schema.tables WHERE table_name = 'jobs' AND table_schema = 'jobs'")) {
      final ResultSet rst = stmt.executeQuery();
      return rst.next();
    }
  }

  private static void overrideWithSystemPropertyIfPresent(final Properties props, final String property) {
    final String propertyValue = System.getProperty(property);
    if (propertyValue != null) {
      props.setProperty(property, propertyValue);
    }
  }

  @BeforeEach
  public void setUp() throws Exception {
    // When someone uses this class from a unit test, we need to ensure transaction management still
    // results in a clean unit test database.
    Transaction.setNested();
  }

  @AfterEach
  public void tearDown() throws Exception {
    PMF.close();
  }

  protected static void initPMF(final TestPMF pmf, final String databaseURL, final Properties props) {
    pmf.setJdbcURL(databaseURL);
    pmf.setDbUsername(props.getProperty("database.username"));
    pmf.setDbPassword(props.getProperty("database.password"));
  }

  protected static Properties getProperties() {
    return PROPS;
  }

  protected static TestPMF getPMF() {
    return PMF;
  }

  protected Connection getConnection() throws SQLException {
    return PMF.getConnection();
  }

  protected static DBMessagesKey getMessagesKey() {
    return new DBMessagesKey(LocaleUtils.getDefaultLocale());
  }

  /**
   * Convenience method to get an input stream on a file. The caller should close
   * the stream.
   *
   * @param fileName file to read, it's relative to the calling class package
   * @return input stream to file
   * @throws FileNotFoundException
   */
  protected InputStream getFileInputStream(final String fileName) throws FileNotFoundException {
    final InputStream is = getClass().getResourceAsStream(fileName);
    if (is == null) {
      throw new FileNotFoundException("Input file not found:" + fileName);
    }
    return new BufferedInputStream(is);
  }

  /**
   * Execute debug query which will print the data (to stderr) as generic as possible.
   * This is needed as debugging the database is hard when using transactions, this allows us to query the database inside the current transaction.
   * @param con The connection to use.
   * @param query The query to use for the preparedStatement.
   * @param objects The objects to set in the preparedStatement.
   * @throws SQLException On DB error.
   */
  protected void executeDebugQuery(final Connection con, final String query, final Object... objects) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(query)) {
      QueryUtil.setValues(stmt, objects);

      final ResultSet rst = stmt.executeQuery();
      final int columnCount = rst.getMetaData().getColumnCount();
      while (rst.next()) {
        final String[] row = new String[columnCount];
        for (int i = 0; i < columnCount; i++) {
          row[i] = rst.getString(i + 1);
        }

        LOGGER.error("[{}]", String.join(" | ", row));
      }
    }
  }
}
