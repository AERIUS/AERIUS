/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.postgis.PGgeometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.db.geo.PGisUtils;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.DefaultWhereClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.db.util.WhereClause;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.summary.CustomCalculationPointResult;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticsMarker;
import nl.overheid.aerius.shared.domain.summary.SituationResultsAreaSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsEdgeReceptor;
import nl.overheid.aerius.shared.domain.summary.SituationResultsHabitatSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatisticReceptor;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatisticReceptors;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryRequest;
import nl.overheid.aerius.shared.domain.summary.SurfaceChartResults;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

abstract class ResultsSummaryImpl<I> {

  private static final Logger LOG = LoggerFactory.getLogger(ResultsSummaryImpl.class);

  protected enum RepositoryAttribute implements Attribute {
    SUM_CARTOGRAPHIC_SURFACE,
    SUM_CARTOGRAPHIC_SURFACE_INCREASE,
    SUM_CARTOGRAPHIC_SURFACE_DECREASE,
    MAX_CONTRIBUTION,
    MAX_INCREASE,
    MAX_DECREASE,
    MAX_TOTAL,

    COLOR_RANGE_TYPE,
    LOWER_BOUND,

    HEXAGON_TYPE,
    OVERLAPPING_HEXAGON_TYPE,
    EMISSION_RESULT_TYPE,
    SCENARIO_RESULT_TYPE,
    RESULT_STATISTIC_TYPE,
    EMISSION_RESULT_CHART_TYPE;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ROOT);
    }
  }

  protected enum EmissionResultChartType {
    EMISSION_RESULT, PERCENTAGE_CL
  }

  protected static final WhereClause WHERE_HEXAGON_TYPE = new StaticWhereClause(RepositoryAttribute.HEXAGON_TYPE.name() + " = ?::hexagon_type",
      RepositoryAttribute.HEXAGON_TYPE);

  protected static final WhereClause WHERE_OVERLAPPING_HEXAGON_TYPE =
      new StaticWhereClause(RepositoryAttribute.OVERLAPPING_HEXAGON_TYPE.name() + " = ?::overlapping_hexagon_type",
          RepositoryAttribute.OVERLAPPING_HEXAGON_TYPE);

  protected static final WhereClause WHERE_SCENARIO_RESULT_TYPE = new StaticWhereClause(
      RepositoryAttribute.SCENARIO_RESULT_TYPE.name() + " = ?::scenario_result_type", RepositoryAttribute.SCENARIO_RESULT_TYPE);

  protected static final WhereClause WHERE_EMISSION_RESULT_TYPE = new StaticWhereClause(
      RepositoryAttribute.EMISSION_RESULT_TYPE.name() + " = ?::emission_result_type", RepositoryAttribute.EMISSION_RESULT_TYPE);

  protected static final WhereClause WHERE_CHART_TYPE = new StaticWhereClause(
      RepositoryAttribute.EMISSION_RESULT_CHART_TYPE.name() + " = ?::emission_result_chart_type", RepositoryAttribute.EMISSION_RESULT_CHART_TYPE);

  protected static final WhereClause WHERE_SUBSTANCE_ID = new DefaultWhereClause(QueryAttribute.SUBSTANCE_ID);

  protected static final WhereClause[] COMMON_WHERE_CLAUSES = new WhereClause[] {
      WHERE_HEXAGON_TYPE, WHERE_OVERLAPPING_HEXAGON_TYPE, WHERE_SCENARIO_RESULT_TYPE, WHERE_EMISSION_RESULT_TYPE, WHERE_SUBSTANCE_ID
  };

  private static final int RESULT_COMPARISON_FACTOR = 100;

  protected final PMF pmf;
  protected final ReceptorUtil receptorUtil;

  /**
   * Only use this constructor if you're certain the repository won't be used to retrieve information.
   */
  ResultsSummaryImpl(final PMF pmf) {
    this(pmf, null);
  }

  ResultsSummaryImpl(final PMF pmf, final ReceptorUtil receptorUtil) {
    this.pmf = pmf;
    this.receptorUtil = receptorUtil;
  }

  protected abstract I createSummaryInput(final SituationCalculations situationCalculations, final Integer calculationId);

  protected abstract List<I> createSummaryInputs(final SituationCalculations situationCalculations);

  protected abstract PreparedStatement prepareOverallStatisticsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput) throws SQLException;

  protected abstract PreparedStatement prepareAreaStatisticsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput) throws SQLException;

  protected abstract PreparedStatement prepareHabitatStatisticsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput, final int assessmentAreaId) throws SQLException;

  protected abstract PreparedStatement prepareChartStatisticsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput, final int assessmentAreaId, final EmissionResultChartType chartType) throws SQLException;

  protected abstract PreparedStatement prepareStatisticMarkersStatement(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput) throws SQLException;

  protected abstract PreparedStatement prepareReceptorStatisticsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput) throws SQLException;

  /**
   * Function to indicate that this implementation supports custom calculation point.
   * If true is returned, the prepareCustomCalculationPointResultsStatement function will be used and it's not safe to return null.
   * If false it won't be used and it's safe to return null.
   */
  protected boolean supportsCustomCalculationPoints() {
    return true;
  }

  protected abstract PreparedStatement prepareCustomCalculationPointResultsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput) throws SQLException;

  protected abstract void preprocessSummaryStep(final Connection connection, final int jobId, final I summaryInput) throws SQLException;

  protected abstract PreparedStatement prepareInsertResultsSummaryStatement(final Connection connection, final int jobId, final I summaryInput)
      throws SQLException;

  protected abstract Map<Integer, List<SituationResultsEdgeReceptor>> determineEdgeReceptors(final Connection connection,
      final SummaryRequest summaryRequest, final I summaryInput) throws SQLException, AeriusException;

  protected abstract int compare(final SituationResultsStatistics statistics1, final SituationResultsStatistics statistics2);

  public SituationResultsSummary determineReceptorResultsSummary(final SituationCalculations situationCalculations,
      final SummaryRequest summaryRequest, final AreaInformationSupplier areaInformationSupplier) throws AeriusException {
    final I summaryInput = createSummaryInput(situationCalculations, summaryRequest.getCalculationId());
    try (final Connection con = pmf.getConnection()) {
      final SituationResultsStatistics overallStatistics = determineOverallStatistics(con, summaryRequest, summaryInput);
      final List<SituationResultsAreaSummary> areaSummaries = determineAreaStatistics(con, summaryRequest, summaryInput,
          areaInformationSupplier);
      final List<ResultStatisticsMarker> markers = determineStatisticMarkers(con, summaryRequest, summaryInput);
      final SituationResultsStatisticReceptors receptors = determineReceptorStatistics(con, summaryRequest, summaryInput, areaInformationSupplier);
      return new SituationResultsSummary(overallStatistics, areaSummaries, markers, receptors, List.of());
    } catch (final SQLException e) {
      LOG.error("SQL error while getting results summary", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public List<CustomCalculationPointResult> determineCustomCalculationPointResults(final SituationCalculations situationCalculations,
      final SummaryRequest summaryRequest) throws AeriusException {
    final I summaryInput = createSummaryInput(situationCalculations, summaryRequest.getCalculationId());
    try (final Connection con = pmf.getConnection()) {
      return determineCustomCalculationPointResults(con, summaryRequest, summaryInput);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting custom point results", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public SituationResultsStatistics determineOverallStatistics(final Connection connection, final SummaryRequest summaryRequest, final I summaryInput)
      throws SQLException {
    try (final PreparedStatement stmt = prepareOverallStatisticsStatement(connection, summaryRequest, summaryInput)) {
      final SituationResultsStatistics result = new SituationResultsStatistics();

      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final ResultStatisticType statisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);
          final Double value = QueryAttribute.VALUE.getNullableDouble(rs);
          result.put(statisticType, value);
        }
      }
      return result;
    }
  }

  private List<SituationResultsAreaSummary> determineAreaStatistics(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput, final AreaInformationSupplier areaInformationSupplier)
      throws SQLException, AeriusException {
    final Map<Integer, SituationResultsAreaSummary> resultsPerArea = new HashMap<>();
    final Map<Integer, List<SituationResultsEdgeReceptor>> edgeReceptorsPerArea =
        determineEdgeReceptors(connection, summaryRequest, summaryInput);
    try (final PreparedStatement stmt = prepareAreaStatisticsStatement(connection, summaryRequest, summaryInput)) {

      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
          if (!resultsPerArea.containsKey(assessmentAreaId)) {
            final AssessmentArea area = areaInformationSupplier.determineAssessmentArea(assessmentAreaId);
            final List<SurfaceChartResults> emissionResultChartResults = determineEmissionResultChartResults(connection, summaryRequest, summaryInput,
                assessmentAreaId, EmissionResultChartType.EMISSION_RESULT);
            final List<SurfaceChartResults> percentageCriticalLoadResults =
                determineEmissionResultChartResults(connection, summaryRequest, summaryInput,
                    assessmentAreaId, EmissionResultChartType.PERCENTAGE_CL);
            final List<SituationResultsHabitatSummary> habitatSummaries = determineHabitatStatistics(connection, summaryRequest, summaryInput,
                assessmentAreaId, areaInformationSupplier);
            final List<SituationResultsEdgeReceptor> edgeReceptors =
                edgeReceptorsPerArea.getOrDefault(assessmentAreaId, Collections.emptyList());
            final boolean sensitive = isAreaSensitive(habitatSummaries, summaryRequest, areaInformationSupplier);
            resultsPerArea.put(assessmentAreaId,
                new SituationResultsAreaSummary(area, sensitive, emissionResultChartResults, percentageCriticalLoadResults, habitatSummaries,
                    edgeReceptors));
          }
          final ResultStatisticType statisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);
          final Double value = QueryAttribute.VALUE.getNullableDouble(rs);
          resultsPerArea.get(assessmentAreaId).getStatistics().put(statisticType, value);
        }
      }
      final List<SituationResultsAreaSummary> results = new ArrayList<>(resultsPerArea.values());
      Collections.sort(results, this::compare);
      return results;
    }
  }

  private static boolean isAreaSensitive(final List<SituationResultsHabitatSummary> habitatSummaries, final SummaryRequest summaryRequest,
      final AreaInformationSupplier areaInformationSupplier) {
    // An area is deemed sensitive if any of the habitats is sensitive.
    return habitatSummaries.stream()
        .map(habitat -> habitat.getHabitatType().getId())
        .anyMatch(
            habitatTypeId -> areaInformationSupplier.determineHabitatTypeSensitiveness(habitatTypeId, summaryRequest.getEmissionResultKey()));
  }

  private List<SituationResultsHabitatSummary> determineHabitatStatistics(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput, final int assessmentAreaId, final AreaInformationSupplier areaInformationSupplier) throws SQLException {
    final Map<Integer, SituationResultsHabitatSummary> resultsPerHabitat = new HashMap<>();
    try (final PreparedStatement stmt = prepareHabitatStatisticsStatement(connection, summaryRequest, summaryInput, assessmentAreaId)) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int habitatTypeId = QueryAttribute.CRITICAL_DEPOSITION_AREA_ID.getInt(rs);
          final SituationResultsHabitatSummary summary = resultsPerHabitat.computeIfAbsent(habitatTypeId,
              id -> createNewHabitatSummary(habitatTypeId, summaryRequest, areaInformationSupplier));
          final ResultStatisticType statisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);
          final Double value = QueryAttribute.VALUE.getNullableDouble(rs);
          summary.getStatistics().put(statisticType, value);
        }
      }
      final List<SituationResultsHabitatSummary> results = new ArrayList<>(resultsPerHabitat.values());
      Collections.sort(results, this::compare);
      return results;
    }
  }

  private static SituationResultsHabitatSummary createNewHabitatSummary(final int habitatTypeId, final SummaryRequest summaryRequest,
      final AreaInformationSupplier areaInformationSupplier) {
    final HabitatType habitatType = areaInformationSupplier.determineHabitatType(habitatTypeId);
    final Double minimumCriticalLevel = areaInformationSupplier.determineHabitatCriticalLevel(habitatTypeId,
        summaryRequest.getEmissionResultKey());
    return new SituationResultsHabitatSummary(habitatType, minimumCriticalLevel);
  }

  private List<SurfaceChartResults> determineEmissionResultChartResults(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput, final int assessmentAreaId, final EmissionResultChartType chartType) throws SQLException {
    final List<SurfaceChartResults> chartResults = new ArrayList<>();
    try (final PreparedStatement stmt = prepareChartStatisticsStatement(connection, summaryRequest, summaryInput, assessmentAreaId, chartType)) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final double lowerBound = QueryUtil.getDouble(rs, RepositoryAttribute.LOWER_BOUND);
          final double cartographicSurface = QueryAttribute.CARTOGRAPHIC_SURFACE.getDouble(rs);
          chartResults.add(new SurfaceChartResults(lowerBound, cartographicSurface));
        }
      }
      return chartResults;
    }
  }

  private List<ResultStatisticsMarker> determineStatisticMarkers(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput) throws SQLException {
    final Map<Integer, ResultStatisticsMarker> markersPerReceptor = new HashMap<>();
    try (final PreparedStatement stmt = prepareStatisticMarkersStatement(connection, summaryRequest, summaryInput)) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
          if (receptorId != 0) {
            if (!markersPerReceptor.containsKey(receptorId)) {
              final Point point = receptorUtil.getPointFromReceptorId(receptorId);

              markersPerReceptor.put(receptorId, new ResultStatisticsMarker(receptorId, point));
            }
            final ResultStatisticType statisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);
            markersPerReceptor.get(receptorId).getStatisticsTypes().add(statisticType);
          }
        }
      }
      return new ArrayList<>(markersPerReceptor.values());
    }
  }

  private SituationResultsStatisticReceptors determineReceptorStatistics(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput, final AreaInformationSupplier areaInformationSupplier)
      throws SQLException {
    final SituationResultsStatisticReceptors receptorMap = new SituationResultsStatisticReceptors();
    try (final PreparedStatement stmt = prepareReceptorStatisticsStatement(connection, summaryRequest, summaryInput)) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
          final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
          final AssessmentArea area = areaInformationSupplier.determineAssessmentArea(assessmentAreaId);
          final ResultStatisticType statisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);

          final SituationResultsStatisticReceptor receptor = new SituationResultsStatisticReceptor(receptorId, area.getName());
          receptorMap.put(statisticType, receptor);
        }
      }
      return receptorMap;
    }
  }

  private List<CustomCalculationPointResult> determineCustomCalculationPointResults(final Connection connection, final SummaryRequest summaryRequest,
      final I summaryInput) throws SQLException {
    final List<CustomCalculationPointResult> results = new ArrayList<>();
    if (!supportsCustomCalculationPoints()) {
      return results;
    }
    try (final PreparedStatement stmt = prepareCustomCalculationPointResultsStatement(connection, summaryRequest, summaryInput)) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int calculationPointId = QueryAttribute.CALCULATION_POINT_ID.getInt(rs);
          final Double result = QueryUtil.getNullableDouble(rs, QueryAttribute.RESULT);
          final Point point = PGisUtils.getPoint((PGgeometry) QueryUtil.getObject(rs, QueryAttribute.GEOMETRY));

          results.add(new CustomCalculationPointResult(calculationPointId, point, result));
        }
      }
      return results;
    }
  }

  public void preprocessForSummary(final int jobId, final SituationCalculations situationCalculations)
      throws AeriusException {
    final List<I> summaryInputs = createSummaryInputs(situationCalculations);
    for (final I summaryInput : summaryInputs) {
      try (final Connection con = pmf.getConnection()) {
        preprocessSummaryStep(con, jobId, summaryInput);
      } catch (final SQLException e) {
        LOG.error("SQL error while inserting situation results area summary", e);
        throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
      }
    }
  }

  public void insertResultsSummaries(final int jobId, final SituationCalculations situationCalculations)
      throws AeriusException {
    final List<I> summaryInputs = createSummaryInputs(situationCalculations);
    for (final I summaryInput : summaryInputs) {
      try (final Connection con = pmf.getConnection();
          final PreparedStatement stmt = prepareInsertResultsSummaryStatement(con, jobId, summaryInput)) {
        stmt.execute();
      } catch (final SQLException e) {
        LOG.error("SQL error while inserting situation results area summary", e);
        throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
      }
    }
  }

  private int compare(final SituationResultsHabitatSummary habitatSummary1, final SituationResultsHabitatSummary habitatSummary2) {
    int compared = compare(habitatSummary1.getStatistics(), habitatSummary2.getStatistics());
    if (compared == 0) {
      compared = compareSurfaceOrPoints(habitatSummary1.getStatistics(), habitatSummary2.getStatistics());
    }
    if (compared == 0) {
      compared = habitatSummary1.getHabitatType().getName().compareTo(habitatSummary2.getHabitatType().getName());
    }
    return compared;
  }

  private int compare(final SituationResultsAreaSummary areaSummary1, final SituationResultsAreaSummary areaSummary2) {
    int compared = compare(areaSummary1.getStatistics(), areaSummary2.getStatistics());
    if (compared == 0) {
      compared = compareSurfaceOrPoints(areaSummary1.getStatistics(), areaSummary2.getStatistics());
    }
    if (compared == 0) {
      compared = areaSummary1.getAssessmentArea().getName().compareTo(areaSummary2.getAssessmentArea().getName());
    }
    return compared;
  }

  private static int compareSurfaceOrPoints(final SituationResultsStatistics statistics1, final SituationResultsStatistics statistics2) {
    int sorted = 0;
    if (statistics1.containsKey(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE)) {
      sorted = compareValues(
          statistics1.get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE),
          statistics2.get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE));
    }
    if (sorted == 0 && statistics1.containsKey(ResultStatisticType.COUNT_CALCULATION_POINTS)) {
      sorted = compareValues(
          statistics1.get(ResultStatisticType.COUNT_CALCULATION_POINTS),
          statistics2.get(ResultStatisticType.COUNT_CALCULATION_POINTS));
    }
    return sorted;
  }

  protected static int compareValues(final Double value1, final Double value2) {
    // ensure values are compared so high values are first, and compare based on 2
    // digit precision.
    return Long.compare(
        Math.round(Optional.ofNullable(value2).orElse(0.0) * RESULT_COMPARISON_FACTOR),
        Math.round(Optional.ofNullable(value1).orElse(0.0) * RESULT_COMPARISON_FACTOR));
  }

  protected void setCommonQueryValues(final Query query, final PreparedStatement stmt, final SummaryRequest summaryRequest) throws SQLException {
    query.setParameter(stmt, RepositoryAttribute.SCENARIO_RESULT_TYPE, summaryRequest.getResultType().name().toLowerCase(Locale.ROOT));
    query.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, summaryRequest.getHexagonType().name().toLowerCase(Locale.ROOT));
    query.setParameter(stmt, RepositoryAttribute.OVERLAPPING_HEXAGON_TYPE,
        summaryRequest.getOverlappingHexagonType().name().toLowerCase(Locale.ROOT));
    query.setParameter(stmt, RepositoryAttribute.EMISSION_RESULT_TYPE,
        summaryRequest.getEmissionResultKey().getEmissionResultType().name().toLowerCase(Locale.ROOT));
    query.setParameter(stmt, QueryAttribute.SUBSTANCE_ID, summaryRequest.getEmissionResultKey().getSubstance().getId());
  }
}
