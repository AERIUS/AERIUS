/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.postgis.PGbox2d;
import org.postgis.PGgeometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.geo.PGisUtils;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.geo.AreaType;
import nl.overheid.aerius.shared.domain.geo.WKTGeometryWithBox;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;

/**
 * Repository class for geometry related queries.
 */
public final class GeometryRepository {
  private static final Logger LOG = LoggerFactory.getLogger(GeometryRepository.class);

  private static final String QUERY_GET_SIMPLIFIED_GEOMETRY_AND_BB = "SELECT ST_SimplifyPreserveTopology(geometry, ?) AS geometry"
      + ", Box2D(geometry) AS boundingbox" + " FROM %s ";
  private static final String QUERY_BY_ID = " WHERE %s_id = ? ";
  private static final String QUERY_BY_POINT = " WHERE ST_Intersects(ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()), geometry) ";
  private static final String QUERY_NAME_SORT = "ORDER BY name ILIKE '%' || ? || '%' DESC";

  private GeometryRepository() {
    // Repository class.
  }

  public static WKTGeometryWithBox getSimplifiedGeometry(final Connection con, final AreaType areaType, final Integer areaId, final Point areaPoint,
      final Float tolerance, final String nameGuide) throws SQLException {
    return getGeometry(con, QUERY_GET_SIMPLIFIED_GEOMETRY_AND_BB, areaType, areaId, areaPoint, tolerance, nameGuide);
  }

  private static WKTGeometryWithBox getGeometry(final Connection con, final String baseQuery, final AreaType areaType, final Integer areaId,
      final Point areaPoint, final Float tolerance, final String nameGuide) throws SQLException {
    // Start with a sanity check that shouldn't be able to happen. Getting caught by this indicates a broken codepath.
    if (areaType == null) {
      LOG.error("AreaType is null, this should not happen.");
      throw new IllegalArgumentException("AreaType is null.");
    }

    final String query = prepareGeometryQuery(baseQuery, areaType, areaPoint, nameGuide);

    WKTGeometryWithBox result = null;
    try (final PreparedStatement stmt = con.prepareStatement(query)) {
      preparePreparedStatement(areaId, areaPoint, tolerance, nameGuide, stmt);

      final ResultSet rst = stmt.executeQuery();
      if (rst.next()) {
        final WKTGeometry geometry = PGisUtils.getGeometry((PGgeometry) rst.getObject("geometry"));
        final BBox boundingbox = PGisUtils.getBox((PGbox2d) rst.getObject("boundingbox"));
        result = new WKTGeometryWithBox(geometry, boundingbox);
      }
    }
    return result;
  }

  private static void preparePreparedStatement(final Integer areaId, final Point areaPoint, final Float tolerance, final String nameGuide,
      final PreparedStatement stmt) throws SQLException {
    int fieldIndex = 1;
    if (tolerance != null) {
      stmt.setFloat(fieldIndex, tolerance);
      fieldIndex++;
    }
    if (areaPoint == null) {
      stmt.setInt(fieldIndex, areaId);
    } else {
      stmt.setDouble(fieldIndex, areaPoint.getX());
      fieldIndex++;
      stmt.setDouble(fieldIndex, areaPoint.getY());
    }
    fieldIndex++;
    if (nameGuide != null) {
      stmt.setString(fieldIndex, nameGuide);
    }
  }

  private static String prepareGeometryQuery(final String baseQuery, final AreaType areaType, final Point areaPoint, final String nameGuide) {
    // Build query
    String query = String.format(baseQuery, areaType.toDatabasePluralObjectName());
    if (areaPoint == null) {
      query += QUERY_BY_ID;
      query = String.format(query, areaType.toDatabaseObjectName());
    } else {
      query += QUERY_BY_POINT;
    }
    if (nameGuide != null) {
      query += QUERY_NAME_SORT;
    }
    return query;
  }
}
