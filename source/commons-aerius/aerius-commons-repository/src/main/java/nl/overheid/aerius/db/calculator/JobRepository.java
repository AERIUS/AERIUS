/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.db.DatabaseErrorCode;
import nl.overheid.aerius.db.Transaction;
import nl.overheid.aerius.db.util.ArrayWhereClause;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.InsertBuilder;
import nl.overheid.aerius.db.util.InsertClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.NullableWhereClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.db.util.WhereClause;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculation;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

public final class JobRepository {

  protected enum RepositoryAttribute implements Attribute {
    JOB_ID,
    TYPE,
    PROTECTED,
    STATE,
    QUEUE_POSITION,
    PICK_UP_TIME,
    CALCULATING_TIME,
    START_TIME,
    END_TIME,
    NUMBER_OF_POINTS,
    NUMBER_OF_POINTS_CALCULATED,
    ERROR_MESSAGE,
    RESULT_URL;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private static final Logger LOG = LoggerFactory.getLogger(JobRepository.class);

  private static final String TABLE_JOBS = "jobs.jobs";
  private static final String TABLE_JOB_CALCULATIONS = "jobs.job_calculations";
  private static final String VIEW_JOB_PROGRESS = "jobs.job_progress_view";
  private static final String JOB_STATE_TYPE = "job_state_type";

  private static final WhereClause FILTER_DELETED_JOBS = new StaticWhereClause("state != 'deleted'");

  private static final Attributes FIELDS_JOB_PROGRESS = new Attributes(
      RepositoryAttribute.TYPE,
      RepositoryAttribute.STATE,
      RepositoryAttribute.START_TIME,
      RepositoryAttribute.CALCULATING_TIME,
      RepositoryAttribute.END_TIME,
      RepositoryAttribute.NUMBER_OF_POINTS,
      RepositoryAttribute.NUMBER_OF_POINTS_CALCULATED,
      RepositoryAttribute.RESULT_URL,
      RepositoryAttribute.ERROR_MESSAGE,
      RepositoryAttribute.PROTECTED,
      QueryAttribute.KEY,
      QueryAttribute.NAME);

  private static final Query QUERY_CREATE_JOB = InsertBuilder.into(TABLE_JOBS)
      .insert(QueryAttribute.KEY)
      .insert(QueryAttribute.USER_ID)
      .insert(new InsertClause(RepositoryAttribute.TYPE.attribute(), "?::job_type", RepositoryAttribute.TYPE))
      .insert(RepositoryAttribute.PROTECTED)
      .getQuery();
  private static final Query QUERY_ADD_JOB_CALCULATIONS = InsertBuilder.into(TABLE_JOB_CALCULATIONS)
      .insert(RepositoryAttribute.JOB_ID, QueryAttribute.CALCULATION_ID, QueryAttribute.SITUATION_REFERENCE,
          QueryAttribute.SITUATION_NAME, QueryAttribute.SITUATION_VERSION)
      .insert(new InsertClause(QueryAttribute.SITUATION_TYPE.attribute(), "?::situation_type", QueryAttribute.SITUATION_TYPE))
      .getQuery();
  private static final Query QUERY_GET_JOB_ID = QueryBuilder.from(TABLE_JOBS)
      .select(RepositoryAttribute.JOB_ID)
      .where(QueryAttribute.KEY)
      .getQuery();
  private static final Query QUERY_GET_JOB_PROGRESS = QueryBuilder.from(VIEW_JOB_PROGRESS)
      .select(RepositoryAttribute.JOB_ID)
      .select(RepositoryAttribute.QUEUE_POSITION)
      .select(FIELDS_JOB_PROGRESS)
      .where(QueryAttribute.KEY)
      .getQuery();
  private static final Query QUERY_GET_JOB_PROGRESS_WITHOUT_DELETED = QueryBuilder.from(VIEW_JOB_PROGRESS)
      .select(RepositoryAttribute.JOB_ID)
      .select(RepositoryAttribute.QUEUE_POSITION)
      .select(FIELDS_JOB_PROGRESS)
      .where(QueryAttribute.KEY)
      .where(FILTER_DELETED_JOBS)
      .getQuery();
  private static final Query QUERY_GET_JOB_PROGRESS_FOR_USER = QueryBuilder.from(VIEW_JOB_PROGRESS)
      .select(RepositoryAttribute.JOB_ID)
      .select(FIELDS_JOB_PROGRESS)
      .where(QueryAttribute.USER_ID)
      .where(FILTER_DELETED_JOBS)
      .orderBy(RepositoryAttribute.JOB_ID)
      .getQuery();
  private static final Query QUERY_GET_JOB_PROGRESS_FOR_USER_AND_STATES = QueryBuilder.from(VIEW_JOB_PROGRESS)
      .select(RepositoryAttribute.JOB_ID)
      .select(FIELDS_JOB_PROGRESS)
      .where(QueryAttribute.USER_ID)
      .where(new ArrayWhereClause(RepositoryAttribute.STATE))
      .where(FILTER_DELETED_JOBS)
      .orderBy(RepositoryAttribute.JOB_ID)
      .getQuery();
  private static final Query QUERY_GET_JOB_PROGRESS_FOR_USER_AND_KEY = QueryBuilder.from(VIEW_JOB_PROGRESS)
      .select(RepositoryAttribute.JOB_ID)
      .select(RepositoryAttribute.QUEUE_POSITION)
      .select(FIELDS_JOB_PROGRESS)
      .where(new NullableWhereClause(QueryAttribute.USER_ID))
      .where(QueryAttribute.KEY)
      .where(FILTER_DELETED_JOBS)
      .getQuery();
  private static final Query QUERY_GET_SITUATION_CALCULATIONS = QueryBuilder.from(TABLE_JOB_CALCULATIONS)
      .join(new JoinClause(TABLE_JOBS, QueryAttribute.JOB_ID))
      .select(QueryAttribute.CALCULATION_ID, QueryAttribute.SITUATION_REFERENCE, QueryAttribute.SITUATION_TYPE,
          QueryAttribute.SITUATION_NAME, QueryAttribute.SITUATION_VERSION)
      .where(QueryAttribute.KEY)
      .getQuery();
  private static final Query QUERY_GET_SITUATION_CALCULATION = QueryBuilder.from(TABLE_JOB_CALCULATIONS)
      .join(new JoinClause(TABLE_JOBS, QueryAttribute.JOB_ID))
      .select(QueryAttribute.CALCULATION_ID)
      .where(QueryAttribute.KEY, QueryAttribute.SITUATION_REFERENCE)
      .getQuery();
  private static final Query QUERY_GET_JOB_FOR_USER = QueryBuilder.from(TABLE_JOBS)
      .select(RepositoryAttribute.JOB_ID)
      .where(new NullableWhereClause(QueryAttribute.USER_ID))
      .where(QueryAttribute.KEY)
      .getQuery();

  private static final String QUERY_DONE_JOBS_WITH_MIN_AGE = "SELECT job_id"
      + " FROM " + VIEW_JOB_PROGRESS
      + " WHERE state in ('cancelled', 'completed', 'deleted')"
      + "   AND (end_time <= now() - (interval '1 day' * ?)"
      + "        OR (end_time IS NULL AND start_time <= now() - (interval '1 day' * ?)))";
  private static final String QUERY_DELETE_JOB = "SELECT jobs.ae_delete_job(?, ?)";
  private static final String QUERY_DELETE_JOB_CALCULATIONS = "SELECT jobs.ae_delete_job_calculations(job_id, ?) FROM " + TABLE_JOBS
      + " WHERE " + TABLE_JOBS + ".key = ?";

  private static final String QUERY_GET_JOB_STATE = "SELECT state FROM jobs.jobs WHERE key = ?";

  private static final String SQL_UPDATE_JOB_STATE_FIELD = "SELECT jobs.ae_update_job_status(?, ?::job_state_type)";
  private static final String SQL_UPDATE_JOB_FIELD = "UPDATE " + TABLE_JOBS + " SET %s = ? "
      + " WHERE key = ?";
  private static final String SQL_INCREMENT_JOB_FIELD = "UPDATE " + TABLE_JOBS + " SET %1$s = %1$s + ? "
      + " WHERE key = ?";

  private static final String QUERY_COPY_JOB_RESULTS = "SELECT jobs.ae_copy_job(?, ?)";

  private JobRepository() {
    // Not allowed to instantiate.
  }

  /**
   * Create job without a user.
   * @param con The connection to use.
   * @param type The type of job to create.
   * @param protect if true job is protected from unconditional deleting of the job.
   * @return jobKey The jobKey to use as key.
   * @throws SQLException Database errors.
   * @throws AeriusException
   */
  public static String createJob(final Connection con, final JobType type, final boolean protect) throws SQLException, AeriusException {
    return createJob(con, null, type, Optional.empty(), protect);
  }

  /**
   * Create job for user.
   * @param con The connection to use.
   * @param user The user to create job for.
   * @param type The type of job to create.
   * @param protect if true job is protected from unconditional deleting of the job
   * @return jobKey The jobKey to use as key.
   * @throws SQLException Database errors.
   * @throws AeriusException
   */
  public static String createJob(final Connection con, final ConnectUser user, final JobType type, final boolean protect)
      throws SQLException, AeriusException {
    return createJob(con, user, type, Optional.empty(), protect);
  }

  /**
   * Create job for user.
   * @param con The connection to use.
   * @param user The user to create job for.
   * @param type The type of job to create.
   * @param name Optional name of the job.
   * @param protect if true job is protected from unconditional deleting of the job
   * @return jobKey The jobKey to use as key.
   * @throws SQLException Database errors.
   * @throws AeriusException
   */
  public static String createJob(final Connection con, final ConnectUser user, final JobType type, final Optional<String> name, final boolean protect)
      throws SQLException, AeriusException {
    final String jobKey = createJobKey(protect);
    insertJob(con, user, jobKey, type, protect);

    if (name.isPresent()) {
      setName(con, jobKey, name.get());
    }

    return jobKey;
  }

  private static String createJobKey(final boolean protect) {
    return FileServerFile.createId(protect ? JobIdentifier.PROTECTED_PREFIX : JobIdentifier.TEMP_PREFIX);
  }

  /**
   * Associate calculation id's to a job.
   */
  public static void attachCalculations(final Connection con, final String jobKey, final List<Calculation> calculations) throws SQLException {
    attachCalculations(con, jobKey, calculations, Map.of());
  }

  /**
   * Associate calculation id's to a job.
   */
  public static void attachCalculations(final Connection con, final String jobKey, final List<Calculation> calculations,
      final Map<String, String> situationVersions) throws SQLException {
    final int jobId = getJobId(con, jobKey);

    try (final PreparedStatement ps = con.prepareStatement(QUERY_ADD_JOB_CALCULATIONS.get())) {
      QUERY_ADD_JOB_CALCULATIONS.setParameter(ps, RepositoryAttribute.JOB_ID, jobId);
      for (final Calculation calculation : calculations) {
        final String situationReference = calculation.getSituationReference();
        QUERY_ADD_JOB_CALCULATIONS.setParameter(ps, QueryAttribute.CALCULATION_ID, calculation.getCalculationId());
        QUERY_ADD_JOB_CALCULATIONS.setParameter(ps, QueryAttribute.SITUATION_REFERENCE, situationReference);
        String situationType = null;
        if (calculation.getSituationType() != null) {
          situationType = calculation.getSituationType().name().toLowerCase();
        }
        QUERY_ADD_JOB_CALCULATIONS.setParameter(ps, QueryAttribute.SITUATION_TYPE, situationType);
        QUERY_ADD_JOB_CALCULATIONS.setParameter(ps, QueryAttribute.SITUATION_NAME, calculation.getSituation().getName());
        QUERY_ADD_JOB_CALCULATIONS.setParameter(ps, QueryAttribute.SITUATION_VERSION,
            situationVersions.getOrDefault(situationReference, AeriusVersion.getVersionNumber()));
        ps.addBatch();
      }
      ps.executeBatch();

      // Reset hexagon counter
      resetNumberOfPointsCalculated(con, jobKey);
    } catch (final SQLException e) {
      LOG.error("Error attaching calculations to job {}", jobId, e);
      throw e;
    }
  }

  /**
   * Get the job id that belongs to a certain correlation identifier.
   * Returns 0 when no job is found for that identifier.
   */
  public static int getJobId(final Connection con, final String jobKey) throws SQLException {
    int jobId = 0;
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_GET_JOB_ID.get())) {
      QUERY_GET_JOB_ID.setParameter(stmt, QueryAttribute.KEY, jobKey);
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        jobId = QueryUtil.getInt(rs, RepositoryAttribute.JOB_ID);
      }
    }
    return jobId;
  }

  /**
   * Fetch the job progress from the database using the job key as lookup.
   * Returns null when no progress record is found for that job key.
   */
  public static JobProgress getProgress(final Connection con, final String jobKey) throws SQLException {
    return getProgress(con, QUERY_GET_JOB_PROGRESS, null, jobKey);
  }

  /**
   * Fetch the job progress from the database using the job key as lookup, but if the job is deleted it returns null.
   * Returns null when no progress record is found for that job key or if job has state deleted.
   */
  public static JobProgress getProgressWithoutDeleted(final Connection con, final String jobKey) throws SQLException {
    return getProgress(con, QUERY_GET_JOB_PROGRESS_WITHOUT_DELETED, null, jobKey);
  }

  /**
   * Fetches job progress objects for a single job of the given user.
   */
  public static JobProgress getProgressForUserAndKey(final Connection con, final ConnectUser user, final String jobKey) throws SQLException {
    return getProgress(con, QUERY_GET_JOB_PROGRESS_FOR_USER_AND_KEY, user, jobKey);
  }

  private static JobProgress getProgress(final Connection con, final Query query, final ConnectUser user, final String jobKey) throws SQLException {
    JobProgress jobProgress = null;

    try (final PreparedStatement stmt = con.prepareStatement(query.get())) {
      if (user != null) {
        query.setParameter(stmt, QueryAttribute.USER_ID, user.getId());
      }
      query.setParameter(stmt, QueryAttribute.KEY, jobKey);

      final ResultSet rst = stmt.executeQuery();
      if (rst.next()) {
        jobProgress = new JobProgress();
        fillJobProgress(jobProgress, rst);
        jobProgress.setQueuePosition(QueryUtil.getInt(rst, RepositoryAttribute.QUEUE_POSITION));
      }
    }
    return jobProgress;
  }

  /**
   * Fetches job progress objects for all jobs of the given user.
   */
  public static List<JobProgress> getProgressForUser(final Connection con, final ConnectUser user) throws SQLException {
    final List<JobProgress> progresses = new ArrayList<>();
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_GET_JOB_PROGRESS_FOR_USER.get())) {
      QUERY_GET_JOB_PROGRESS_FOR_USER.setParameter(stmt, QueryAttribute.USER_ID, user.getId());

      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        final JobProgress jobProgress = new JobProgress();
        fillJobProgress(jobProgress, rs);
        progresses.add(jobProgress);
      }
    }
    return progresses;
  }

  /**
   * Fetches job progress objects for all jobs of the given user and given a set of job states.
   */
  public static List<JobProgress> getProgressForUserAndJobStates(final Connection con, final ConnectUser user,
      final Set<JobState> jobStates) throws SQLException {
    final List<JobProgress> progresses = new ArrayList<>();
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_GET_JOB_PROGRESS_FOR_USER_AND_STATES.get())) {
      QUERY_GET_JOB_PROGRESS_FOR_USER_AND_STATES.setParameter(stmt, QueryAttribute.USER_ID, user.getId());
      QUERY_GET_JOB_PROGRESS_FOR_USER_AND_STATES.setParameter(stmt, RepositoryAttribute.STATE, QueryUtil.toSQLArray(con, JOB_STATE_TYPE, jobStates));

      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        final JobProgress jobProgress = new JobProgress();
        fillJobProgress(jobProgress, rs);
        progresses.add(jobProgress);
      }
    }
    return progresses;
  }

  public static SituationCalculations getSituationCalculations(final Connection con, final String jobKey) throws SQLException {
    final SituationCalculations result = new SituationCalculations();
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_GET_SITUATION_CALCULATIONS.get())) {
      QUERY_GET_SITUATION_CALCULATIONS.setParameter(stmt, QueryAttribute.KEY, jobKey);

      final ResultSet rst = stmt.executeQuery();
      while (rst.next()) {
        final String situationReference = QueryAttribute.SITUATION_REFERENCE.getString(rst);
        if (situationReference != null) {
          final SituationCalculation situationCalculation = new SituationCalculation();
          situationCalculation.setSituationId(situationReference);
          situationCalculation.setCalculationId(QueryAttribute.CALCULATION_ID.getInt(rst));
          situationCalculation.setSituationType(QueryAttribute.SITUATION_TYPE.getEnum(rst, SituationType.class));
          situationCalculation.setSituationName(QueryAttribute.SITUATION_NAME.getString(rst));
          situationCalculation.setSituationVersion(QueryAttribute.SITUATION_VERSION.getString(rst));
          result.add(situationCalculation);
        }
      }
    }

    return result;
  }

  public static ArchiveMetaData getArchiveMetaData(final Connection con, final String jobKey) throws SQLException {
    // TODO: depends on how archive metadata is saved.
    return null;
  }

  public static Optional<Integer> getSituationCalculation(final Connection con, final String jobKey, final String situationReference)
      throws SQLException {
    Optional<Integer> result = Optional.empty();
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_GET_SITUATION_CALCULATION.get())) {
      QUERY_GET_SITUATION_CALCULATION.setParameter(stmt, QueryAttribute.KEY, jobKey);
      QUERY_GET_SITUATION_CALCULATION.setParameter(stmt, QueryAttribute.SITUATION_REFERENCE, situationReference);

      final ResultSet rst = stmt.executeQuery();
      if (rst.next()) {
        result = Optional.of(QueryAttribute.CALCULATION_ID.getInt(rst));
      }
    }

    return result;

  }

  /**
   * Remove job fully. This includes calculations/progress and such.
   * @param con The connection to use.
   * @param jobId The job ID of the job to remove.
   * @param forceDelete if true protected jobs will be removed.
   * @return If a job is removed.
   */
  public static boolean removeJob(final Connection con, final int jobId, final boolean forceDelete) {
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_DELETE_JOB)) {
      QueryUtil.setValues(stmt, jobId, forceDelete);
      stmt.execute();
    } catch (final SQLException e) {
      LOG.error("Error removing job {}", jobId, e);
      return false;
    }

    return true;
  }

  /**
   * Remove only the calculations of a job.
   * @param con The connection to use.
   * @param jobKey The job key of the job to remove the calculations from.
   * @param forceDelete if true calculations of protected jobs will be removed.
   * @return If a calculations are removed.
   */
  public static boolean removeJobCalculations(final Connection con, final String jobKey, final boolean forceDelete) {
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_DELETE_JOB_CALCULATIONS)) {
      QueryUtil.setValues(stmt, forceDelete, jobKey);
      final ResultSet rs = stmt.executeQuery();

      return rs.next() && rs.getBoolean(1);
    } catch (final SQLException e) {
      LOG.error("Error removing job with key {}", jobKey, e);
      return false;
    }
  }

  /**
   * Remove jobs that are finished ('completed' or 'cancelled') with the age given.
   * The age is computed based on the time of completion (endTime).
   * In case there is no endTime present (because of a bug or something else that is wrong)
   *  the age is computed based on the time of job creation (startTime).
   * @param con The connection to use.
   * @param ageInDays The age in days to use.
   * @return amount of jobs removed.
   * @throws SQLException In case of a database error.
   */
  public static int removeJobsWithMinAge(final Connection con, final int ageInDays) throws SQLException {
    int amountRemoved = 0;
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_DONE_JOBS_WITH_MIN_AGE)) {
      QueryUtil.setValues(stmt, ageInDays, ageInDays);
      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        if (removeJob(con, QueryUtil.getInt(rs, RepositoryAttribute.JOB_ID), false)) {
          amountRemoved++;
        }
      }
    } catch (final SQLException e) {
      LOG.error("Fetching jobs to remove failed. Hard.", e);
    }

    return amountRemoved;
  }

  /**
   * Set the number of points on a job.
   *
   * @param con The connection to use.
   * @param jobKey An identifier used by the worker to associate itself with this job
   * @param numberOfPoints The number of points to set
   * @throws SQLException In case of a database error.
   */
  public static void setNumberOfPoints(final Connection con, final String jobKey, final int numberOfPoints) throws SQLException {
    updateField(con, jobKey, RepositoryAttribute.NUMBER_OF_POINTS, numberOfPoints);
  }

  /**
   * Increase the number of calculated points counter of the given job with the given increment.
   *
   * @param con The connection to use.
   * @param jobKey An identifier used by the worker to associate itself with this job
   * @param increment The amount to add to the counter
   * @throws SQLException In case of a database error.
   */
  public static void increaseNumberOfPointsCalculatedCounter(final Connection con, final String jobKey, final double increment) throws SQLException {
    incrementField(con, jobKey, RepositoryAttribute.NUMBER_OF_POINTS_CALCULATED, increment);
  }

  /**
   * Set the number of calculated points counter of the given job to zero.
   */
  public static void resetNumberOfPointsCalculated(final Connection con, final String jobKey) throws SQLException {
    updateField(con, jobKey, RepositoryAttribute.NUMBER_OF_POINTS_CALCULATED, 0);
  }

  /**
   * Set the jobState for the job.
   */
  public static boolean updateJobStatus(final Connection con, final String jobKey, final JobState state) throws SQLException {
    try (final PreparedStatement updatePS = con.prepareStatement(SQL_UPDATE_JOB_STATE_FIELD)) {
      QueryUtil.setValues(updatePS, jobKey, state.toDatabaseString());

      final ResultSet rs = updatePS.executeQuery();
      if (rs.next()) {
        return rs.getBoolean(1);
      }
    }
    return false;
  }

  /**
   * @return Returns the job state as set in the database
   */
  public static JobState getJobStatus(final Connection con, final String jobKey) throws SQLException {
    try (final PreparedStatement updatePS = con.prepareStatement(QUERY_GET_JOB_STATE)) {
      QueryUtil.setValues(updatePS, jobKey);

      final ResultSet rs = updatePS.executeQuery();
      if (rs.next()) {
        return QueryUtil.getEnum(rs, RepositoryAttribute.STATE, JobState.class);
      }
    }
    return JobState.UNDEFINED;
  }

  /**
   * Set the result url of the given job.
   */
  public static void setResultUrl(final Connection con, final String jobKey, final String resultUrl) throws SQLException {
    updateField(con, jobKey, RepositoryAttribute.RESULT_URL, resultUrl);
  }

  /**
   * Set the error message text if Job is stopped with a error and change jobState to ERROR.
   * @param con database connection.
   * @param jobKey An identifier used by the worker to associate itself with this job.
   * @param message the error message that was encountered.
   * @throws SQLException
   */
  public static void setErrorMessage(final Connection con, final String jobKey, final String message) throws SQLException {
    if (updateJobStatus(con, jobKey, JobState.ERROR)) {
      updateField(con, jobKey, RepositoryAttribute.ERROR_MESSAGE, message);
    }
  }

  /**
   * Returns if a given user and jobKey exist.
   * @param con database connection.
   * @param user the user object.
   * @param jobKey the jobKey.
   * @return true if the combination is found.
   * @throws SQLException
   */
  public static boolean isJobFromUser(final Connection con, final ConnectUser user, final String jobKey) throws SQLException {
    return isExistingJob(con, Optional.of(user), jobKey);
  }

  /**
   * Returns if a given jobKey exist and is not linked to a user.
   * @param con database connection.
   * @param jobKey the jobKey.
   * @return true if the job is found and does not belong to a user.
   * @throws SQLException
   */
  public static boolean isJobWithoutUser(final Connection con, final String jobKey) throws SQLException {
    return isExistingJob(con, Optional.empty(), jobKey);
  }

  private static boolean isExistingJob(final Connection con, final Optional<ConnectUser> user, final String jobKey) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_GET_JOB_FOR_USER.get())) {
      QUERY_GET_JOB_FOR_USER.setParameter(stmt, QueryAttribute.USER_ID, user.map(ConnectUser::getId).orElse(null));
      QUERY_GET_JOB_FOR_USER.setParameter(stmt, QueryAttribute.KEY, jobKey);
      final ResultSet rs = stmt.executeQuery();
      return rs.next();
    }
  }

  /**
   * Set the name of the given job.
   */
  private static void setName(final Connection con, final String jobKey, final String name) throws SQLException {
    updateField(con, jobKey, QueryAttribute.NAME, name);
  }

  /**
   * Insert an empty job for the given user.
   *
   * @param con The connection to use.
   * @param user The user to create the job for.
   * @param jobType The job type.
   * @param jobKey An identifier used by the worker to associate itself with this job.
   * @param protect if true job is protected from unconditional deleting of the job.
   * @throws SQLException In case of a database error.
   * @throws AeriusException
   */
  private static int insertJob(final Connection con, final ConnectUser user, final String jobKey, final JobType jobType, final boolean protect)
      throws SQLException, AeriusException {
    try {
      final Transaction transaction = new Transaction(con);
      try (final PreparedStatement psCreate = con.prepareStatement(QUERY_CREATE_JOB.get(), Statement.RETURN_GENERATED_KEYS)) {
        QUERY_CREATE_JOB.setParameter(psCreate, QueryAttribute.KEY, jobKey);
        QUERY_CREATE_JOB.setParameter(psCreate, QueryAttribute.USER_ID, user == null ? null : user.getId());
        QUERY_CREATE_JOB.setParameter(psCreate, RepositoryAttribute.TYPE, jobType.toString());
        QUERY_CREATE_JOB.setParameter(psCreate, RepositoryAttribute.PROTECTED, protect);

        psCreate.executeUpdate();
        final ResultSet rs = psCreate.getGeneratedKeys();
        rs.next();
        final int jobId = rs.getInt(1);

        // Use function to update status to trigger log
        updateJobStatus(con, jobKey, JobState.CREATED);
        return jobId;

      } catch (final SQLException e) {
        transaction.rollback();
        LOG.error("Error creating job for user {}", user == null ? "NO-USER" : user.getEmailAddress(), e);
        throw e;
      } finally {
        transaction.commit();
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error with transaction");
    }
  }

  private static <T> void updateField(final Connection con, final String jobKey, final Attribute attribute, final T value) throws SQLException {
    final String sql = String.format(SQL_UPDATE_JOB_FIELD, attribute.attribute());
    try (final PreparedStatement ps = con.prepareStatement(sql)) {
      QueryUtil.setValues(ps, value, jobKey);
      ps.executeUpdate();
    }
  }

  private static void fillJobProgress(final JobProgress jobProgress, final ResultSet rs) throws SQLException {
    jobProgress.setType(QueryUtil.getEnum(rs, RepositoryAttribute.TYPE, JobType.class));
    jobProgress.setKey(QueryUtil.getString(rs, QueryAttribute.KEY));
    jobProgress.setName(QueryUtil.getString(rs, QueryAttribute.NAME));
    jobProgress.setProtect(QueryUtil.getBoolean(rs, RepositoryAttribute.PROTECTED));
    jobProgress.setState(QueryUtil.getEnum(rs, RepositoryAttribute.STATE, JobState.class));
    jobProgress.setStartDateTime(QueryUtil.getDate(rs, RepositoryAttribute.START_TIME));
    jobProgress.setEndDateTime(QueryUtil.getDate(rs, RepositoryAttribute.END_TIME));
    jobProgress.setNumberOfPoints(QueryUtil.getInt(rs, RepositoryAttribute.NUMBER_OF_POINTS));
    jobProgress.setNumberOfPointsCalculated(QueryUtil.getDouble(rs, RepositoryAttribute.NUMBER_OF_POINTS_CALCULATED));
    jobProgress.setResultUrl(QueryUtil.getString(rs, RepositoryAttribute.RESULT_URL));
    jobProgress.setErrorMessage(QueryUtil.getString(rs, RepositoryAttribute.ERROR_MESSAGE));
    calculateRemainingTime(jobProgress, QueryUtil.getDate(rs, RepositoryAttribute.CALCULATING_TIME));
  }

  /**
   * Calculate the estimated remaining time by calculating the amount of time it took to calculate the current number of points calculated,
   * and extrapolate that number to the total of points to be calculate. Than subtract the time from the points already calculated.
   *
   * @param jobProgress
   * @param calculatingStartTime time the calculation process started.
   */
  private static void calculateRemainingTime(final JobProgress jobProgress, final Date calculatingStartTime) {
    if (calculatingStartTime != null && jobProgress.getState() == JobState.CALCULATING) {
      final int numberOfPoints = jobProgress.getNumberOfPoints();
      final double numberOfPointsCalculated = jobProgress.getNumberOfPointsCalculated();
      final LocalDateTime pastDateTime = LocalDateTime.ofInstant(calculatingStartTime.toInstant(), ZoneId.systemDefault());
      final long seconds = ChronoUnit.SECONDS.between(pastDateTime, LocalDateTime.now());

      jobProgress.setEstimatedRemainingCalculationTime(
          numberOfPointsCalculated > 0 ? (int) Math.round(seconds * ((numberOfPoints / numberOfPointsCalculated) - 1.0)) : -1);
    }
  }

  private static <T> void incrementField(final Connection con, final String jobKey, final Attribute attribute, final T value)
      throws SQLException {
    final String sql = String.format(SQL_INCREMENT_JOB_FIELD, attribute.attribute());

    try (final PreparedStatement ps = con.prepareStatement(sql)) {
      QueryUtil.setValues(ps, value, jobKey);
      ps.executeUpdate();
    }
  }

  public static void copyJobResults(final Connection con, final String fromJobKey, final String toJobKey) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_COPY_JOB_RESULTS)) {
      stmt.setString(1, fromJobKey);
      stmt.setString(2, toJobKey);
      stmt.execute();
    }
  }
}
