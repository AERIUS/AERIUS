/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.ResultSet;
import java.sql.SQLException;

import nl.overheid.aerius.db.common.EntityCollector;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;

/**
 * Abstract base class for collecting category data from the database.
 * Categories all have code, name and description columns.
 */
abstract class AbstractCategoryCollector<T extends AbstractCategory> extends EntityCollector<T> {

  static final Attributes BASE_CATEGORY_ATTRIBUTES = new Attributes(QueryAttribute.CODE, QueryAttribute.NAME);

  protected final DBMessagesKey messagesKey;
  private final Attribute categoryId;

  protected AbstractCategoryCollector(final Attribute categoryId, final DBMessagesKey messagesKey) {
    this.categoryId = categoryId;
    this.messagesKey = messagesKey;
  }

  @Override
  public int getKey(final ResultSet rs) throws SQLException {
    return QueryUtil.getInt(rs, categoryId);
  }

  /**
   * Fills the entry, implement {@link #setRemainingInfo(AbstractCategory, ResultSet)} to read additional attributes.
   */
  @Override
  public final T fillEntity(final ResultSet rs) throws SQLException {
    final T category = getNewCategory();
    category.setId(getKey(rs));
    category.setCode(QueryAttribute.CODE.getString(rs));
    category.setName(QueryAttribute.NAME.getString(rs));

    setDescription(category, rs);

    setRemainingInfo(category, rs);

    return category;
  }

  /**
   * Create the new category for this specific collector.
   */
  abstract T getNewCategory() throws SQLException;

  /**
   * implement to let the description be something else than the description returned by query.
   */
  abstract void setDescription(final T category, final ResultSet rs) throws SQLException;

  /**
   * override to set information that is available in each resultset (everything but the basic ID, code, name, description).
   */
  void setRemainingInfo(final T category, final ResultSet rs) throws SQLException {
    //default no-op.
  }

}
