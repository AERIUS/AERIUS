/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;

/**
 * Repository class to use for calculation categories and their associated properties.
 */
public final class CalculationCategoriesRepository {

  private enum RepositoryAttribute implements Attribute {
    CALCULATION_PERMIT_AREA_ID,
    CALCULATION_PROJECT_CATEGORY_ID,
    PERMIT_AREA_CODE,
    PROJECT_CATEGORY_CODE,
    DEFAULT_CALCULATION_DISTANCE;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ROOT);
    }
  }

  private static final Query ALL_PERMIT_AREAS = QueryBuilder.from("calculation_permit_areas")
      .select(RepositoryAttribute.CALCULATION_PERMIT_AREA_ID, QueryAttribute.CODE, QueryAttribute.NAME, QueryAttribute.DESCRIPTION)
      .orderBy(RepositoryAttribute.CALCULATION_PERMIT_AREA_ID)
      .getQuery();

  private static final Query ALL_PROJECT_CATEGORIES = QueryBuilder.from("calculation_project_categories")
      .select(RepositoryAttribute.CALCULATION_PROJECT_CATEGORY_ID, QueryAttribute.CODE, QueryAttribute.NAME, QueryAttribute.DESCRIPTION)
      .orderBy(RepositoryAttribute.CALCULATION_PROJECT_CATEGORY_ID)
      .getQuery();

  private static final Query DEFAULT_DISTANCES = QueryBuilder.from("calculation_default_distances_view")
      .select(RepositoryAttribute.PERMIT_AREA_CODE, RepositoryAttribute.PROJECT_CATEGORY_CODE, RepositoryAttribute.DEFAULT_CALCULATION_DISTANCE)
      .getQuery();

  // Not allowed to instantiate.
  private CalculationCategoriesRepository() {
  }

  /**
   * @param con The connection to use.
   * @return All calculation permit areas.
   * @throws SQLException In case of database exceptions.
   */
  public static List<SimpleCategory> getCalculationPermitAreas(final Connection con) throws SQLException {
    return getSimpleCategories(con, ALL_PERMIT_AREAS, RepositoryAttribute.CALCULATION_PERMIT_AREA_ID);
  }

  /**
   * @param con The connection to use.
   * @return All calculation project categories.
   * @throws SQLException In case of database exceptions.
   */
  public static List<SimpleCategory> getCalculationProjectCategories(final Connection con) throws SQLException {
    return getSimpleCategories(con, ALL_PROJECT_CATEGORIES, RepositoryAttribute.CALCULATION_PROJECT_CATEGORY_ID);
  }

  /**
   * @param con The connection to use.
   * @return Map with the default distance for permit area (first key) and project category (second key) combinations.
   * @throws SQLException In case of database exceptions.
   */
  public static Map<String, Map<String, Integer>> getDefaultDistances(final Connection con) throws SQLException {
    final Map<String, Map<String, Integer>> defaultCalculationDistances = new HashMap<>();
    try (final PreparedStatement stmt = con.prepareStatement(DEFAULT_DISTANCES.get())) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final String permitAreaCode = QueryUtil.getString(rs, RepositoryAttribute.PERMIT_AREA_CODE);
          final String projectCategoryCode = QueryUtil.getString(rs, RepositoryAttribute.PROJECT_CATEGORY_CODE);
          final int defaultCalculationDistance = QueryUtil.getInt(rs, RepositoryAttribute.DEFAULT_CALCULATION_DISTANCE);
          defaultCalculationDistances
              .computeIfAbsent(permitAreaCode, k -> new HashMap<>())
              .put(projectCategoryCode, defaultCalculationDistance);
        }
      }
    }
    return defaultCalculationDistances;
  }

  private static List<SimpleCategory> getSimpleCategories(final Connection con, final Query query, final RepositoryAttribute idAttribute)
      throws SQLException {
    final List<SimpleCategory> categories = new ArrayList<>();
    try (final PreparedStatement stmt = con.prepareStatement(query.get())) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final SimpleCategory category = new SimpleCategory(
              QueryUtil.getInt(rs, idAttribute),
              QueryAttribute.CODE.getString(rs),
              QueryAttribute.NAME.getString(rs),
              QueryAttribute.DESCRIPTION.getString(rs));
          categories.add(category);
        }
      }
    }
    return categories;
  }

}
