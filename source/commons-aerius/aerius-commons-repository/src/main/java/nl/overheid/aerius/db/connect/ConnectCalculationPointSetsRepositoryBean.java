/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.connect;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.connect.ConnectCalculationPointSetMetadata;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 *
 */
public class ConnectCalculationPointSetsRepositoryBean {

  private static final Logger LOG = LoggerFactory.getLogger(ConnectCalculationPointSetsRepositoryBean.class);

  private final PMF pmf;

  public ConnectCalculationPointSetsRepositoryBean(final PMF pmf) {
    this.pmf = pmf;
  }

  public void insertCalculationPointSet(final ConnectCalculationPointSetMetadata metadata,
      final List<CalculationPointFeature> calculationPoints) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      ConnectCalculationPointSetsRepository.insertUserCalculationPointSet(con, metadata, calculationPoints);
    } catch (final SQLException e) {
      LOG.error("SQL error while inserting calculation point set", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public ConnectCalculationPointSetMetadata getCalculationPointSetByName(final ConnectUser user, final String name)
      throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ConnectCalculationPointSetsRepository.getUserCalculationPointSetByName(con, user.getId(), name);
    } catch (final SQLException e) {
      LOG.error("SQL error while retrieving specific calculation point set metadata", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public List<CalculationPointFeature> getCalculationPointsByName(final ConnectUser user, final String name)
      throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final ConnectCalculationPointSetMetadata calculationPointSet =
          ConnectCalculationPointSetsRepository.getUserCalculationPointSetByName(con, user.getId(), name);
      return ConnectCalculationPointSetsRepository.getUserCalculationPointsFromSet(con, calculationPointSet.getSetId());
    } catch (final SQLException | ReflectiveOperationException e) {
      LOG.error("Error while retrieving specific calculation point set", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public List<ConnectCalculationPointSetMetadata> getCalculationPointSets(final ConnectUser user) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ConnectCalculationPointSetsRepository.getUserCalculationPointSetsForUser(con, user.getId());
    } catch (final SQLException e) {
      LOG.error("SQL error while retrieving calculation point sets metadata", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public void deleteCalculationPointSet(final int setId) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      ConnectCalculationPointSetsRepository.deleteUserCalculationPointSet(con, setId);
    } catch (final SQLException e) {
      LOG.error("SQL error while deleting calculation point set", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }
}
