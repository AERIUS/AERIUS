/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.summary.JobSummary;
import nl.overheid.aerius.shared.domain.summary.JobSummaryProjectRecord;
import nl.overheid.aerius.shared.domain.summary.JobSummarySituationRecord;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 *
 */
class JobResultsSummaryRepository {

  private static final Logger LOG = LoggerFactory.getLogger(JobResultsSummaryRepository.class);

  private enum RepositoryAttribute implements Attribute {
    MAX_CONTRIBUTION,
    MAX_VALUE,
    RESULT_STATISTIC_TYPE;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ROOT);
    }
  }

  private static class SituationRecordKey {
    private final int assessmentAreaId;
    private final String situationReference;

    SituationRecordKey(final int assessmentAreaId, final String situationReference) {
      this.assessmentAreaId = assessmentAreaId;
      this.situationReference = situationReference;
    }

    @Override
    public int hashCode() {
      return Objects.hash(assessmentAreaId, situationReference);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null || getClass() != obj.getClass()) {
        return false;
      }
      final SituationRecordKey other = (SituationRecordKey) obj;
      return assessmentAreaId == other.assessmentAreaId && Objects.equals(situationReference, other.situationReference);
    }

  }

  private static class ProjectRecordKey {
    private final int assessmentAreaId;
    private final String situationReference;
    private final EmissionResultKey emissionResultKey;

    ProjectRecordKey(final int assessmentAreaId, final String situationReference, final EmissionResultKey emissionResultKey) {
      this.assessmentAreaId = assessmentAreaId;
      this.situationReference = situationReference;
      this.emissionResultKey = emissionResultKey;
    }

    @Override
    public int hashCode() {
      return Objects.hash(assessmentAreaId, situationReference, emissionResultKey);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null || getClass() != obj.getClass()) {
        return false;
      }
      final ProjectRecordKey other = (ProjectRecordKey) obj;
      return assessmentAreaId == other.assessmentAreaId && Objects.equals(situationReference, other.situationReference)
          && emissionResultKey == other.emissionResultKey;
    }

  }

  private static final Query QUERY_JOB_SITUATIONS_SUMMARY = QueryBuilder
      .from("jobs.ae_job_summary_situations(?)", QueryAttribute.JOB_ID)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.SITUATION_REFERENCE,
          QueryAttribute.EMISSION_RESULT_TYPE, QueryAttribute.SUBSTANCE_ID, RepositoryAttribute.MAX_CONTRIBUTION)
      .orderBy(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.SITUATION_REFERENCE)
      .getQuery();

  private static final Query QUERY_JOB_PROJECTS_SUMMARY = QueryBuilder
      .from("jobs.ae_job_summary_projects(?)", QueryAttribute.JOB_ID)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.SITUATION_REFERENCE,
          QueryAttribute.EMISSION_RESULT_TYPE, QueryAttribute.SUBSTANCE_ID, RepositoryAttribute.RESULT_STATISTIC_TYPE,
          RepositoryAttribute.MAX_VALUE)
      .orderBy(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.SITUATION_REFERENCE, QueryAttribute.EMISSION_RESULT_TYPE,
          QueryAttribute.SUBSTANCE_ID)
      .getQuery();

  private final PMF pmf;

  JobResultsSummaryRepository(final PMF pmf) {
    this.pmf = pmf;
  }

  JobSummary determineJobSummary(final int jobId, final AreaInformationSupplier areaInformationSupplier) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final List<JobSummarySituationRecord> situationRecords = determineSituationRecords(con, jobId, areaInformationSupplier);
      final List<JobSummaryProjectRecord> projectRecords = determineProjectRecords(con, jobId, areaInformationSupplier);
      return new JobSummary(situationRecords, projectRecords);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting job results summary", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private static List<JobSummarySituationRecord> determineSituationRecords(final Connection con, final int jobId,
      final AreaInformationSupplier areaInformationSupplier) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_JOB_SITUATIONS_SUMMARY.get())) {
      QUERY_JOB_SITUATIONS_SUMMARY.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
      final Map<SituationRecordKey, JobSummarySituationRecord> recordMap = new HashMap<>();
      final List<JobSummarySituationRecord> result = new ArrayList<>();

      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
          final String situationReference = QueryAttribute.SITUATION_REFERENCE.getString(rs);
          final SituationRecordKey key = new SituationRecordKey(assessmentAreaId, situationReference);
          final JobSummarySituationRecord situationRecord = recordMap.computeIfAbsent(key, (someKey) -> {
            final AssessmentArea area = areaInformationSupplier.determineAssessmentArea(assessmentAreaId);
            final JobSummarySituationRecord newRecord = new JobSummarySituationRecord(area, situationReference);
            result.add(newRecord);
            return newRecord;
          });
          final EmissionResultType emissionResultType = QueryUtil.getEnum(rs, QueryAttribute.EMISSION_RESULT_TYPE, EmissionResultType.class);
          final int substanceId = QueryAttribute.SUBSTANCE_ID.getInt(rs);
          final EmissionResultKey emissionResultKey = EmissionResultKey.valueOf(Substance.substanceFromId(substanceId), emissionResultType);
          final double maxContribution = QueryUtil.getDouble(rs, RepositoryAttribute.MAX_CONTRIBUTION);
          situationRecord.getMaxContributions().put(emissionResultKey, maxContribution);
        }
      }
      return result;
    }
  }

  private static List<JobSummaryProjectRecord> determineProjectRecords(final Connection con, final int jobId,
      final AreaInformationSupplier areaInformationSupplier) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_JOB_PROJECTS_SUMMARY.get())) {
      QUERY_JOB_PROJECTS_SUMMARY.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
      final Map<ProjectRecordKey, JobSummaryProjectRecord> recordMap = new HashMap<>();
      final List<JobSummaryProjectRecord> result = new ArrayList<>();

      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
          final String situationReference = QueryAttribute.SITUATION_REFERENCE.getString(rs);
          final EmissionResultType emissionResultType = QueryUtil.getEnum(rs, QueryAttribute.EMISSION_RESULT_TYPE, EmissionResultType.class);
          final int substanceId = QueryAttribute.SUBSTANCE_ID.getInt(rs);
          final EmissionResultKey emissionResultKey = EmissionResultKey.valueOf(Substance.substanceFromId(substanceId), emissionResultType);
          final ProjectRecordKey key = new ProjectRecordKey(assessmentAreaId, situationReference, emissionResultKey);
          final JobSummaryProjectRecord projectRecord = recordMap.computeIfAbsent(key, (someKey) -> {
            final AssessmentArea area = areaInformationSupplier.determineAssessmentArea(assessmentAreaId);
            final JobSummaryProjectRecord newRecord = new JobSummaryProjectRecord(area, situationReference, emissionResultKey);
            result.add(newRecord);
            return newRecord;
          });
          final ResultStatisticType resultStatisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);
          final Double maxValue = QueryUtil.getNullableDouble(rs, RepositoryAttribute.MAX_VALUE);
          projectRecord.getMaxValues().put(resultStatisticType, maxValue);
        }
      }
      return result;
    }
  }

}
