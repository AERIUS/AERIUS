/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.i18n;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A {@link LocalizationRepository} is a class that may be used to retrieve
 * localized resources from a database. This implementation may differ for each product,
 * and will be initialized reflectively at runtime.
 */
public final class LocalizationRepository {

  /**
   * Maybe not the best name, but this enum can be used for localization stuff.
   */
  public enum LocalizationObject {
    /**
     * Sectors.
     */
    SECTOR("sectors"),
    /**
     * Farm lodging types.
     */
    FARM_LODGING_TYPE("farm_lodging_types"),
    /**
     * Farm additional lodging systems.
     */
    FARM_ADDITIONAL_LODGING_SYSTEM("farm_additional_lodging_systems"),
    /**
     * Farm lodging types.
     */
    FARM_REDUCTIVE_LODGING_SYSTEM("farm_reductive_lodging_systems"),
    /**
     * Farm lodging fodder measures.
     */
    FARM_LODGING_FODDER_MEASURE("farm_lodging_fodder_measures"),
    /**
     * Farm animal categories.
     */
    FARM_ANIMAL_CATEGORY("farm_animal_categories"),
    /**
     * Farm animal housing categories.
     */
    FARM_ANIMAL_HOUSING_CATEGORY("farm_animal_housing_categories"),
    /**
     * Farm additional animal housing systems.
     */
    FARM_ADDITIONAL_HOUSING_SYSTEM("farm_additional_housing_systems"),
    /**
     * Farm lodging system definitions.
     */
    FARM_LODGING_SYSTEM_DEFINITION("farm_lodging_system_definitions"),
    /**
     * Farmland types.
     */
    FARMLAND_CATEGORY("farmland_categories"),

    /**
     * Farm source categories
     */
    FARM_SOURCE_CATEGORY("farm_source_categories"),
    /**
     * Habitat types.
     */
    HABITAT_TYPE("habitat_types"),
    /**
     * Mobile sources off road categories.
     */
    MOBILE_SOURCE_OFF_ROAD_CATEGORY("mobile_source_off_road_categories"),
    /**
     * Mobile sources on road categories.
     */
    MOBILE_SOURCE_ON_ROAD_CATEGORY("mobile_source_on_road_categories"),
    /**
     * Shipping Maritime Categories.
     */
    MARITIME_SHIPPING_CATEGORY("shipping_maritime_categories"),
    /**
     * Shipping Inland Categories.
     */
    INLAND_SHIPPING_CATEGORY("shipping_inland_categories"),
    /**
     * Shipping Inland Waterway Categories.
     */
    INLAND_WATERWAY_CATEGORY("shipping_inland_waterway_categories"),
    /**
     * Road area categories.
     */
    ROAD_AREA_CATEGORY("road_area_categories"),
    /**
     * Road type categories.
     */
    ROAD_TYPE_CATEGORY("road_type_categories"),
    /**
     * Road vehicle type categories.
     */
    ROAD_VEHICLE_CATEGORY("road_vehicle_categories"),
    /**
     * layers.
     */
    LAYERS("layers");

    private final String dbTable;
    private final Set<LocalizationProperty> properties = new HashSet<>();

    LocalizationObject(final String dbTable) {
      this.dbTable = dbTable;
    }

    /**
     * Get one of the enum constants by the table name.
     * @param tableName The table name to use when looking up.
     * @return null if not found.
     */
    public static LocalizationObject getObjectByTableName(final String tableName) {
      for (final LocalizationObject value : values()) {
        if (value.getDbTable().equalsIgnoreCase(tableName)) {
          return value;
        }
      }

      return null;
    }

    Set<LocalizationProperty> getProperties() {
      return properties;
    }

    String getAllQuery() {
      final StringBuilder builder = new StringBuilder();
      builder.append("SELECT * FROM i18n.");
      builder.append(dbTable);
      builder.append(" WHERE ").append(COLUMN_LANGUAGE_CODE).append(" = ?::i18n.language_code_type");
      return builder.toString();
    }

    /**
     * Convenience method to get a key for a localization.
     * @param property The property to get the key for.
     * @param id The ID to get the key for.
     * @return The key to be used in a properties object for instance.
     */
    public String getKey(final LocalizationProperty property, final String id) {
      return this.name() + "-" + property.name() + "-" + id;
    }

    /**
     * Convenience method to get a key for a localization.
     * @param property The property to get the key for.
     * The toString method of the object is used for the key.
     * @param id The ID to get the key for.
     * @return The key to be used in a properties object for instance.
     */
    public String getKey(final LocalizationProperty property, final Object id) {
      return getKey(property, id.toString());
    }

    /**
     * Convenience method to get a key for a localization.
     * @param property The property to get the key for.
     * @param id The ID to get the key for.
     * @return The key to be used in a properties object for instance.
     */
    public String getKey(final LocalizationProperty property, final int id) {
      return getKey(property, Integer.toString(id));
    }

    public String getDbTable() {
      return dbTable;
    }

  }

  /**
   * Properties which can be used for localization.
   */
  public enum LocalizationProperty {

    /**
     * The name property.
     */
    NAME,

    /**
     * The description property of an object.
     */
    DESCRIPTION;

    static LocalizationProperty getByColumnName(final String columnName) {
      for (final LocalizationProperty value : values()) {
        if (value.toString().equalsIgnoreCase(columnName)) {
          return value;
        }
      }

      return null;
    }

  }

  private static final Logger LOG = LoggerFactory.getLogger(LocalizationRepository.class);

  private static final String COLUMN_LANGUAGE_CODE = "language_code";
  private static final String COLUMN_ID_ENDSWITH = "_id";

  private LocalizationRepository() {
  }

  /**
   * Compose and return a {@link Properties} object containing all localizable resources for the
   * given product.
   *
   * @param con {@link Connection} to use.
   * @param locale {@link Locale} to get messages for.
   * @param localizationObjects {@link LocalizationObject} to get messages for.
   *
   * @return Properties Object containing all resources for the given {@link Locale}.
   * @throws SQLException in case of DB errors.
   */
  public static Properties getLocalizedMessages(final Connection con, final Locale locale,
      final List<LocalizationObject> localizationObjects) throws SQLException {
    final Properties prop = new Properties();

    // Stick all resources into the Properties object (this may be changed later on.)
    for (final LocalizationObject object : localizationObjects) {
      putAllLocalizedResourceWithKey(con, prop, locale, object);
    }

    return prop;
  }

  /**
   * Default (convenience) method of putting resources into a Properties object.
   * Key(s) used for the resources is based on the LocalizationObject and its LocalizationProperties.
   *
   * @param con {@link Connection} to use.
   * @param prop {@link Properties} object to stick resources into.
   * @param loc {@link Locale} to get.
   * @param localizationObject {@link LocalizationObject} to get the resources for.
   * @param format Id/key format.
   * @throws SQLException in case of DB errors.
   */
  private static void putAllLocalizedResourceWithKey(final Connection con,
      final Properties prop, final Locale loc, final LocalizationObject localizationObject) throws SQLException {
    try (PreparedStatement ps = con.prepareStatement(localizationObject.getAllQuery())) {
      ps.setString(1, loc.getLanguage());

      final ResultSet rst = ps.executeQuery();
      final ResultSetMetaData rstMetaData = rst.getMetaData();

      while (rst.next()) {
        String idValue = null;

        for (int i = 0; i < rstMetaData.getColumnCount(); ++i) {
          final int columnNumber = i + 1;
          final String columnName = rstMetaData.getColumnName(columnNumber);
          final String columnValue = rst.getString(columnNumber);

          if (columnName.endsWith(COLUMN_ID_ENDSWITH)) {
            idValue = columnValue;
          } else if (!COLUMN_LANGUAGE_CODE.equals(columnName)) {
            final LocalizationProperty property = LocalizationProperty.getByColumnName(columnName);

            if (property == null) {
              LOG.warn("Unknown column '{}' encountered when getting translations for locale {} and localizationObject {}. (id={},value={})",
                  columnName, loc, localizationObject, idValue, columnValue);
              continue;
            }
            if (idValue == null) {
              LOG.warn("Couldn't find ID column for localizationObject {} when getting translations for locale {}.",
                  localizationObject, loc);
              continue;
            }

            prop.put(localizationObject.getKey(property, idValue), columnValue);
          }
        }
      }
    } catch (final SQLException e) {
      LOG.error("SQL Exception while getting translations for locale {} and localizationObject {}.",
          loc.toString(), localizationObject, e);
      throw e;
    }
  }
}
