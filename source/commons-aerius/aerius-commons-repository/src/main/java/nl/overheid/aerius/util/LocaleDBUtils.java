/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.Locale;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;

/**
 * Utility class to get locale and if not present use the default locale set in the database.
 */
public final class LocaleDBUtils {

  /**
   * Get the default locale or the locale set in the DB.
   * @param pmf The pmf to use.
   * @return The default locale.
   */
  public static Locale getDefaultLocale(final PMF pmf) {
    LocaleUtils.setDefaultLocale(ConstantRepository.getString(pmf, ConstantsEnum.DEFAULT_LOCALE));
    return LocaleUtils.getDefaultLocale();
  }

}
