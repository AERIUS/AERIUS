/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.DatabaseErrorCode;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.exception.AeriusException;

public final class SituationResultRepository {

  private static final Logger LOG = LoggerFactory.getLogger(SituationResultRepository.class);

  private static final String VALUE = "value";

  private static final String INSERT_SCENARIO_CALCULATION_ASSESEMENT_AREA_STATISTICS = "INSERT INTO jobs.scenario_calculation_assessment_area_statistics"
      + " (calculation_result_set_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type, job_id, value)"
      + " VALUES (?, ?::scenario_result_type, ?::hexagon_type, ?::overlapping_hexagon_type, ?, ?::result_statistic_type, ?, ?)";

  private static final String DETERMINE_PERMIT_DEMAND_CHECK = "SELECT bool_and(value != 0) as value from jobs.scenario_calculation_assessment_area_statistics"
      + " INNER JOIN jobs.jobs USING (job_id)"
      + " WHERE key = ?"
      + "   AND scenario_result_type = 'project_calculation'"
      + "   AND result_statistic_type = 'development_space_demand_check'";

  private SituationResultRepository() {
    // Util class
  }

  /**
   * Save development space demand check direct in the assessment area statistics
   * this value contains if the demand check fits or not.
   * @param con database connection.
   * @param calculationResultSetId calculation result set id.
   * @param assessmentAreaId assessment area id.
   * @param jobId the jobId.
   * @param value value to insert.
   * @throws AeriusException
   */
  public static void insertSituationResultDevelopmentSpaceDemand(final Connection con, final int calculationResultSetId, final int assessmentAreaId,
      final int jobId, final double value) throws AeriusException {

    try (final PreparedStatement insertPS = con.prepareStatement(INSERT_SCENARIO_CALCULATION_ASSESEMENT_AREA_STATISTICS)) {
      QueryUtil.setValues(insertPS, calculationResultSetId, "project_calculation", "exceeding_hexagons", "all_hexagons", assessmentAreaId,
          "development_space_demand_check", jobId, value);
      insertPS.executeUpdate();
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error with inserting situation result for PDF permit");
    }
  }

  /**
   * Determine if any nature area development space demand is zero in that case the project
   * will not fit in the the demand.
   *
   * @param con database connection.
   * @param jobKey the job key.
   * @return boolean if the project fits for all nature areas.
   * @throws AeriusException
   */
  public static boolean determineSituationResultDevelopmentSpaceDemand(final Connection con, final String jobKey) throws AeriusException {
    try (final PreparedStatement stmt = con.prepareStatement(DETERMINE_PERMIT_DEMAND_CHECK)) {
      QueryUtil.setValues(stmt, jobKey);

      final ResultSet rs = stmt.executeQuery();
      boolean result = false;
      while (rs.next()) {
        result = rs.getBoolean(VALUE);
      }
      return result;

    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error with determining Development Space Demand for PDF permit");
    }
  }

}
