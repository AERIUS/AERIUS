/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.enums;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;

/**
 * Global constants stored in the database and that can only be used on the server (i.e. everywhere
 * except the client). Constants that also need to be accessed in the client should
 * be placed in {@link nl.overheid.aerius.shared.constants.SharedConstantsEnum}.
 *
 * <p>These can be accessed live from the database via the methods in
 * {@link nl.overheid.aerius.db.common.ConstantRepository}.</p>
 *
 * <p>The values of the entries are stored in the database tables <code>public.constants</code>
 * and <code>system.constants</code>.</p>
 */
public enum ConstantsEnum {

  /**
   * organization managing AERIUS product version.
   */
  CUSTOMER,
  /**
   * Email address used as the from e-mail of e-mails send.
   */
  NOREPLY_EMAIL(true),
  /**
   *
   */
  CURRENT_DATABASE_VERSION(true),
  /**
   * The type of release for the currently running application (valid values: PRODUCTION, CONCEPT, DEPRECATED).
   */
  RELEASE(true),
  /**
   * Maximum number of sources allowed in the calculator.
   */
  CALCULATOR_LIMITS_MAX_SOURCES(true),
  /**
   * Maximum length of a line allowed in the calculator in meters.
   */
  CALCULATOR_LIMITS_MAX_LINE_LENGTH(true),
  /**
   * Maximum surface allowed in the calculator in hectares.
   */
  CALCULATOR_LIMITS_MAX_POLYGON_SURFACE(true),
  /**
   * Length of a segment when dividing a line in points in meters.
   */
  CONVERT_LINE_TO_POINTS_SEGMENT_SIZE(true),
  /**
   * Size used when converting polygon to grid based points.
   */
  CONVERT_POLYGON_TO_POINTS_GRID_SIZE(true),

  // --- CHUNKER related constants for the UI ---

  /**
   * Number of source on which to take the large size ui queue.
   */
  CALCULATOR_LARGE_CALCULATIONS_SPLIT(true),

  /**
   * An 'OPS Calculation Time Unit' is determined by factoring the number of sources with the number of receptors.
   * The resulting number linearly scales with the time it theoretically takes OPS to finish a calculation.
   *
   * This constant represents the ideal value - expressed in 'OPS Calculation Time Units' - for any one chunk.
   */
  CHUNKER_ENGINE_UNITS_UI_MAX(true),

  /**
   * The minimum number of receptors to calculate in a single chunk for the UI. This limit is imposed because
   * the overhead introduced by a low number of receptors becomes substantial.
   */
  CHUNKER_RECEPTORS_UI_MIN(true),

  /**
   * The maximum number of receptors to calculate in a single chunk for the UI. This limit is imposed because
   * the responsiveness of the UI is related to the number of receptors it needs to process.
   */
  CHUNKER_RECEPTORS_UI_MAX(true),

  // --- CHUNKER related constants for the Worker ---

  /**
   * The maximum 'Calculation Engine Time Unit' to use by the worker.
   */
  CHUNKER_ENGINE_UNITS_WORKER_MAX(true),

  /**
   * The minimum number of receptors to calculate in a single chunk for a worker. This limit is imposed because
   * the overhead introduced by a low number of receptors becomes substantial.
   */
  CHUNKER_RECEPTORS_WORKER_MIN(true),

  /**
   * The maximum number of receptors to calculate in a single chunk for a worker. Limit is imposed for consistency.
   */
  CHUNKER_RECEPTORS_WORKER_MAX(true),

  // --- Options for NCA/ADMS quick run calculations

  /**
   * The maximum 'Calculation Engine Time Unit' to use by the worker.
   */
  CHUNKER_ENGINE_UNITS_QUICKRUN_MAX(false),

  /**
   * The minimum number of receptors to calculate in a single chunk for a worker. This limit is imposed because
   * the overhead introduced by a low number of receptors becomes substantial.
   */
  CHUNKER_RECEPTORS_QUICKRUN_MIN(false),

  /**
   * The maximum number of receptors to calculate in a single chunk for a worker. Limit is imposed for consistency.
   */
  CHUNKER_RECEPTORS_QUICKRUN_MAX(false),

  // ----------

  /**
   * Maximum number of engine sources in a single calculation in the calculator ui.
   */
  MAX_ENGINE_SOURCES_UI(true),

  /**
   * Melding threshold value (drempelwaarde). If a receptor has more deposition than this, at least a
   * "melding" must be made.
   * (Note: if we do not want to use the database term "pronouncement" in Java (for "melding"), then we
   * need to add an "alias" functionality to this Enum to handle the different constant names.)
   */
  PRONOUNCEMENT_THRESHOLD_VALUE(true),

  /**
   * Determines if it is allowed to generate a new API key for Connect.
   */
  CONNECT_GENERATING_APIKEY_ENABLED(true),
  /**
   * Specifies the amount of max concurrent jobs for a new user.
   */
  CONNECT_MAX_CONCURRENT_JOBS_FOR_NEW_USER(true),
  /**
   * Specifies the default for period job rate limit.
   */
  CONNECT_DEFAULT_PERIOD_JOB_RATE_LIMIT(true),

  /**
   * The conversion factor for emission results, to be used in concert with unit display settings.
   */
  EMISSION_RESULT_DISPLAY_CONVERSION_FACTOR(true),

  /**
   * The display unit for emission results, to be used in concert with conversion factor.
   */
  EMISSION_RESULT_DISPLAY_UNIT(true),

  /**
   * The number of decimal points to display emission results for.
   */
  EMISSION_RESULT_DISPLAY_ROUNDING_LENGTH(true),

  /**
   * The number of precise decimal points to display emission results for.
   */
  EMISSION_RESULT_DISPLAY_PRECISE_ROUNDING_LENGTH(true),

  /**
   * The number of decimal points to display deposition sum results for.
   */
  EMISSION_RESULT_DISPLAY_DEPOSITION_SUM_ROUNDING_LENGTH(true),

  /**
   * The minimum calculation year
   */
  MIN_YEAR,

  /**
   * The maximum calculation year
   */
  MAX_YEAR,

  /**
   * Surface of zoom level 1.
   */
  SURFACE_ZOOM_LEVEL_1(true),

  /**
   * The number of hexagons in a horizontal row.
   */
  HEXAGON_ROWS(true),

  /**
   * Number of zoom levels.
   */
  MAX_ZOOM_LEVEL(true),

  /**
   * Default locale.
   */
  DEFAULT_LOCALE(true),

  /**
   * The (source) characteristics type.
   */
  CHARACTERISTICS_TYPE,

  /**
   * PASSKEY a UUID secret string that is used to update system info messages in the databse
   * Used to update system info messages tables.
   */
  SYSTEM_INFO_PASSKEY,

  /**
   * Contains the OPS model version to use.
   */
  MODEL_VERSION_OPS,

  /**
   * Contains the SRM model version to use in OwN2000 theme.
   */
  MODEL_VERSION_SRM_OWN2000,

  /**
   * Contains the SRM model version to use in RBL theme.
   */
  MODEL_VERSION_SRM_RBL,

  /**
   * Contains the ADMS model version to use.
   */
  MODEL_VERSION_ADMS,

  /**
   * Contains the IMAER version to use.
   */
  IMAER_VERSION,

  /**
   * Variety for {@link SharedConstantsEnum#MANUAL_BASE_URL} but LBV profile only
   */
  MANUAL_BASE_URL_LBV_POLICY,

  /**
   * Variety for {@link SharedConstantsEnum#MANUAL_BASE_URL} but Calculator only
   */
  MANUAL_BASE_URL_CALCULATOR,

  /**
   * Variety for {@link SharedConstantsEnum#QUICK_START_URL} but LBV profile only
   */
  QUICK_START_URL_LBV_POLICY,
  /**
   * Which AERIUS versions are allowed to import results in Calculator
   */
  IMPORT_RESULTS_ALLOWED_VERSIONS;

  /**
   * If true this constant must be in the database.
   */
  private final boolean required;

  private ConstantsEnum() {
    this(false);
  }

  private ConstantsEnum(final boolean required) {
    this.required = required;
  }

  public boolean isRequired() {
    return required;
  }
}
