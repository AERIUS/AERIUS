/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import nl.overheid.aerius.db.BatchInserter;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultSetType;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations.CalculationPointSetTracker;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.result.ReceptorResult;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.util.EnumUtil;

/**
 * DB Class to insert/modify and get Calculation specific objects from the database.
 */
public final class CalculationRepository {

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(CalculationRepository.class);

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
      .registerModule(new JavaTimeModule())
      .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

  /**
   * Batch size interval between batch commits to the database.
   *
   * A high value (>1000) will take a long time to complete, an even higher value will cause OutOfMemory, a low value (<20) is inefficient because
   * then batching would have no real effect.
   */
  private static final int BATCH_SIZE = 500;
  private static final int UNSAFE_BATCH_SIZE = 1000;

  // SQL queries for calculation(s)
  private static final String GET_CALCULATION =
      "SELECT calculation_id, calculation_point_set_id, calculation_sub_point_set_id, year, state "
          + " FROM jobs.calculations WHERE calculation_id = ? ";
  private static final String INSERT_CALCULATION =
      "INSERT INTO jobs.calculations (calculation_point_set_id, calculation_sub_point_set_id, year) VALUES (?,?,?)";
  private static final String UPDATE_CALCULATION_STATS =
      "UPDATE jobs.calculations "
          + " SET number_of_sources = ?, number_of_substances = ? WHERE calculation_id = ? ";
  private static final String UPDATE_CALCULATION_STATE =
      "UPDATE jobs.calculations "
          + " SET state = ?::calculation_state_type WHERE calculation_id = ? ";

  // SQL queries for calculation custom point sets
  private static final String CREATE_CALCULATION_CUSTOM_POINT_SET =
      "INSERT INTO jobs.calculation_point_sets DEFAULT VALUES";
  private static final String INSERT_CALCULATION_CUSTOM_POINTS =
      "INSERT INTO jobs.calculation_points (calculation_point_set_id, calculation_point_id, label, nearest_receptor_id, geometry)"
          + " VALUES (?,?,?,?,ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()))";

  // SQL queries for calculation sub point sets
  private static final String CREATE_CALCULATION_SUB_POINT_SET =
      "INSERT INTO jobs.calculation_sub_point_sets DEFAULT VALUES";
  private static final String INSERT_CALCULATION_SUB_POINTS =
      "INSERT INTO jobs.calculation_sub_points (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id, level, geometry)"
          + " VALUES (?,?,?,?,ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()))";
  private static final String INSERT_CALCULATION_SUB_POINTS_SAFE =
      "INSERT INTO jobs.calculation_sub_points (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id, level, geometry)"
          + " VALUES (?,?,?,?,ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid())) ON CONFLICT DO NOTHING";

  // SQL queries for calculation result sets
  private static final String INSERT_CALCULATION_RESULT_SET =
      "INSERT INTO jobs.calculation_result_sets (calculation_id, result_set_type, result_set_type_key, result_type, substance_id) "
          + " VALUES (?, ?::calculation_result_set_type, ?, ?::emission_result_type, ?)";
  private static final String GET_CALCULATION_RESULT_SETS =
      "SELECT * FROM jobs.calculation_result_sets WHERE calculation_id = ?";
  private static final String GET_CALCULATION_RESULT_SET_ID =
      "SELECT calculation_result_set_id FROM jobs.calculation_result_sets "
          + " WHERE calculation_id = ? AND result_type = ?::emission_result_type AND substance_id = ? "
          + " AND result_set_type = ?::calculation_result_set_type AND result_set_type_key = ?";

  // SQL queries for calculation results inserts
  private static final String SAFE_INSERT_CALCULATION_RESULTS =
      "INSERT INTO jobs.calculation_results (calculation_result_set_id, receptor_id, result) "
          + "SELECT ?,?,? WHERE NOT EXISTS (SELECT calculation_result_set_id FROM jobs.calculation_results "
          + "WHERE calculation_result_set_id = ? AND receptor_id = ? LIMIT 1)";
  private static final String UNSAFE_INSERT_CALCULATION_RESULTS =
      "INSERT INTO jobs.calculation_results (calculation_result_set_id, receptor_id, result) "
          + "VALUES (?,?,?)";
  private static final String SAFE_INSERT_CALCULATION_POINT_RESULTS =
      "INSERT INTO jobs.calculation_point_results (calculation_result_set_id, calculation_point_id, result) "
          + " SELECT ?,?,? WHERE NOT EXISTS (SELECT calculation_result_set_id FROM jobs.calculation_point_results "
          + " WHERE calculation_result_set_id = ? AND calculation_point_id = ? LIMIT 1) ";
  private static final String UNSAFE_INSERT_CALCULATION_POINT_RESULTS =
      "INSERT INTO jobs.calculation_point_results (calculation_result_set_id, calculation_point_id, result) "
          + "VALUES (?,?,?)";
  private static final String SAFE_INSERT_SUB_POINT_RESULTS =
      "INSERT INTO jobs.calculation_sub_point_results (calculation_result_set_id, calculation_sub_point_id, receptor_id, result) "
          + " SELECT ?,?,?,? WHERE NOT EXISTS (SELECT calculation_result_set_id FROM jobs.calculation_sub_point_results "
          + " WHERE calculation_result_set_id = ? AND calculation_sub_point_id = ? and receptor_id = ? LIMIT 1) ";
  private static final String UNSAFE_INSERT_SUB_POINT_RESULTS =
      "INSERT INTO jobs.calculation_sub_point_results (calculation_result_set_id, calculation_sub_point_id, receptor_id, result) "
          + "VALUES (?,?,?,?)";

  //SQL queries for archive contribution results inserts
  private static final String INSERT_ARCHIVE_CONTRIBUTION_RESULTS =
      "SELECT jobs.ae_insert_archive_contribution_receptor(?,?,?)";
  private static final String INSERT_ARCHIVE_CONTRIBUTION_SUB_POINT_RESULTS =
      "SELECT jobs.ae_insert_archive_contribution_sub_point(?,?,?,?)";

  // Misc other SQL queries.
  private static final String INSERT_CALCULATION_OVERLAPPING_RECEPTORS =
      "INSERT INTO jobs.calculation_overlapping_receptors (calculation_id, receptor_id)"
          + " VALUES (?,?)";

  private static final Query GET_CALCULATION_OVERLAPPING_RECEPTORS = QueryBuilder.from("jobs.calculation_overlapping_receptors")
      .select(QueryAttribute.RECEPTOR_ID)
      .where(QueryAttribute.CALCULATION_ID).getQuery();

  private static final Query GET_ALL_RECEPTORS = QueryBuilder.from("jobs.calculation_results_view")
      .select(QueryAttribute.RECEPTOR_ID).distinct()
      .where(QueryAttribute.CALCULATION_ID).getQuery();

  private static final Query GET_ALL_CALCULATION_POINTS = QueryBuilder.from("jobs.calculation_point_results_view")
      .select(QueryAttribute.CALCULATION_POINT_ID, QueryAttribute.LABEL)
      .select(new SelectClause("ST_X(geometry)", QueryAttribute.X_COORD.attribute()),
          new SelectClause("ST_Y(geometry)", QueryAttribute.Y_COORD.attribute()))
      .distinct()
      .where(QueryAttribute.CALCULATION_ID).getQuery();

  private static final String DELETE_CALCULATION = "SELECT jobs.ae_delete_calculation(?)";

  private static final String GET_CALCULATION_RESULTS_FROM_SET =
      "SELECT receptor_id, result FROM jobs.calculation_results"
          + " INNER JOIN jobs.ae_receptors_with_hexagon_type(?) USING (receptor_id)"
          + " WHERE calculation_result_set_id = ? AND hexagon_type = ?::hexagon_type";

  private static final String GET_ARCHIVE_CONTRIBUTION_RECEPTORS_FROM_SET =
      "SELECT receptor_id, result FROM jobs.archive_contribution_results"
          + " WHERE calculation_result_set_id = ?";

  private static final String INSERT_ARCHIVE_METADATA =
      "INSERT INTO jobs.archive_metadata (calculation_id, metadata) VALUES (?,?::json)";

  private static final Query GET_ARCHIVE_METADATA = QueryBuilder.from("jobs.archive_metadata")
      .select(QueryAttribute.METADATA)
      .where(QueryAttribute.CALCULATION_ID).getQuery();

  //Not allowed to instantiate.
  private CalculationRepository() {
  }

  /**
   * @param connection The connection to use
   * @param calculationId The id of the calculation trying to retrieve
   * @return The calculation object, null if not found. Calculation options (if existing) are NOT set. (use getCalculationOptions to get those.)
   * @throws SQLException If an error occurred communicating with the DB.
   */
  public static Calculation getCalculation(final Connection connection, final int calculationId) throws SQLException {
    final Calculation calculation = new Calculation();
    calculation.setCalculationId(calculationId);
    return getCalculation(connection, calculation);
  }

  private static Calculation getCalculation(final Connection connection, final Calculation calculation) throws SQLException {
    final Calculation foundCalculation;
    try (final PreparedStatement selectPS = connection.prepareStatement(GET_CALCULATION)) {
      selectPS.setLong(1, calculation.getCalculationId());
      try (final ResultSet rs = selectPS.executeQuery()) {
        // calculation_id, state, year
        if (rs.next()) {
          calculation.setCalculationId(rs.getInt("calculation_id"));
          calculation.setState(EnumUtil.get(CalculationState.class, rs.getString("state")));
          calculation.setYear(rs.getInt("year"));
          calculation.setCustomCalculationPointSetId(rs.getObject("calculation_point_set_id") == null
              ? null
              : rs.getInt("calculation_point_set_id"));
          calculation.setCustomCalculationSubPointSetId(rs.getObject("calculation_sub_point_set_id") == null
              ? null
              : rs.getInt("calculation_sub_point_set_id"));
          foundCalculation = calculation;
        } else {
          foundCalculation = null;
        }
      }
    }
    return foundCalculation;
  }

  /**
   * @param connection The connection to use.
   * @param calculation the calculation object to persist to the database and fill with some extra information.
   * @param calculationPointSetId The calculation point set ID that the calculation will use.
   * @param calculationSubPointSetId The calculation sub point set ID that the calculation will use.
   * @return The filled calculation.
   * @throws SQLException If an error occurred communicating with the DB.
   */
  public static Calculation insertCalculation(final Connection connection, final Calculation calculation, final Integer calculationPointSetId,
      final Integer calculationSubPointSetId) throws SQLException {
    // ensure a calculation has a set it belongs to, even if it's the only one.
    try (final PreparedStatement insertPS = connection.prepareStatement(INSERT_CALCULATION, Statement.RETURN_GENERATED_KEYS)) {
      if (calculationPointSetId == null) {
        insertPS.setNull(1, Types.INTEGER);
      } else {
        insertPS.setInt(1, calculationPointSetId);
      }
      if (calculationSubPointSetId == null) {
        insertPS.setNull(2, Types.INTEGER);
      } else {
        insertPS.setInt(2, calculationSubPointSetId);
      }
      insertPS.setInt(3, calculation.getYear());
      insertPS.executeUpdate();
      try (final ResultSet rs = insertPS.getGeneratedKeys()) {
        if (rs.next()) {
          calculation.setCalculationId(rs.getInt(1));
        } else {
          throw new SQLException("No generated key obtained while saving new calculation.");
        }
      }
    }
    return getCalculation(connection, calculation);
  }

  public static void setNumberOfSources(final Connection connection, final Integer calculationId, final int numberOfSources,
      final int numberOfSubstances) throws SQLException {
    try (final PreparedStatement updatePS = connection.prepareStatement(UPDATE_CALCULATION_STATS)) {
      updatePS.setInt(1, numberOfSources);
      updatePS.setInt(2, numberOfSubstances);
      updatePS.setInt(3, calculationId);
      updatePS.executeUpdate();
    }
  }

  /**
   * @param connection The connection to use.
   * @param calculationId The id of the calculation to update the state for.
   * @param state The state to set for this particular calculation.
   * @return True if update was successful (calculation was found in DB and updated). False if calculation could not be found.
   * @throws SQLException If an error occurred communicating with the DB.
   */
  public static boolean updateCalculationState(final Connection connection, final int calculationId, final CalculationState state)
      throws SQLException {
    boolean updated = false;
    try (final PreparedStatement updatePS = connection.prepareStatement(UPDATE_CALCULATION_STATE)) {
      updatePS.setString(1, state.toString());
      updatePS.setInt(2, calculationId);
      updated = updatePS.executeUpdate() > 0;
    }
    return updated;
  }

  /**
   * Remove all calculation results for a calculation id.
   *
   * @param connection The connection to use
   * @param calculationId if of calculation to remove
   */
  public static void removeCalculation(final Connection connection, final int calculationId) {
    try {
      try (final PreparedStatement selectPS = connection.prepareStatement(DELETE_CALCULATION)) {
        selectPS.setInt(1, calculationId);
        selectPS.execute();
      }
    } catch (final SQLException e) {
      LOG.error("Delete of calculation {} failed.", calculationId, e);
    }
  }

  /**
   * @param connection The connection to use
   * @param calculationPoints The calculation points to persist as a set to the database.
   * @param <T> The type of AeriusPoints to insert.
   * @return The ID that the inserted set has obtained.
   * @throws SQLException In case of database errors.
   */
  public static int insertCalculationPointSet(final Connection connection, final List<CalculationPointFeature> calculationPoints)
      throws SQLException {
    final int calculationPointSetId;

    try (final PreparedStatement insertPS = connection.prepareStatement(CREATE_CALCULATION_CUSTOM_POINT_SET, Statement.RETURN_GENERATED_KEYS)) {
      insertPS.executeUpdate();
      try (final ResultSet rs = insertPS.getGeneratedKeys()) {
        if (rs.next()) {
          calculationPointSetId = rs.getInt(1);
        } else {
          throw new SQLException("No generated key obtained while creating new calculation point set.");
        }
      }
    }
    return insertCalculationPointsForSet(connection, INSERT_CALCULATION_CUSTOM_POINTS, calculationPointSetId, calculationPoints);
  }

  /**
   * @param connection
   * @param destinationQuery
   * @param calculationPointSetId
   * @param calculationPoints
   * @return
   * @throws SQLException
   */
  static int insertCalculationPointsForSet(final Connection connection, final String destinationQuery,
      final int calculationPointSetId, final List<CalculationPointFeature> calculationPoints) throws SQLException {
    final ReceptorGridSettings rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(connection);
    final ReceptorUtil receptorUtil = new ReceptorUtil(rgs);
    final BatchInserter<CalculationPointFeature> inserter = new BatchInserter<CalculationPointFeature>() {
      @Override
      public void setParameters(final PreparedStatement ps, final CalculationPointFeature feature) throws SQLException {
        final Point point = feature.getGeometry();
        final CalculationPoint calculationPoint = feature.getProperties();
        final int nearestReceptorId = receptorUtil.getReceptorIdFromCoordinate(point.getX(), point.getY(), rgs.getZoomLevel1());
        int paramIdx = 1;
        ps.setInt(paramIdx++, calculationPointSetId);
        ps.setInt(paramIdx++, calculationPoint.getId());
        ps.setString(paramIdx++, calculationPoint.getLabel());
        ps.setInt(paramIdx++, nearestReceptorId);
        ps.setDouble(paramIdx++, point.getX());
        ps.setDouble(paramIdx++, point.getY());
      }
    };
    inserter.setBatchSize(BATCH_SIZE);
    final int inserted = inserter.insertBatch(connection, destinationQuery, calculationPoints);
    if (inserted != calculationPoints.size()) {
      throw new SQLException("Did not save all calculation points! calculation point set id: " + calculationPointSetId + ". Inserted points: "
          + inserted + ". Expected: " + calculationPoints.size());
    }

    return calculationPointSetId;
  }

  /**
   * @param connection The connection to use
   * @return The ID that the inserted set has obtained.
   * @throws SQLException In case of database errors.
   */
  public static int insertSubPointSet(final Connection connection) throws SQLException {
    final int calculationSubPointSetId;

    try (final PreparedStatement insertPS = connection.prepareStatement(CREATE_CALCULATION_SUB_POINT_SET, Statement.RETURN_GENERATED_KEYS)) {
      insertPS.executeUpdate();
      try (final ResultSet rs = insertPS.getGeneratedKeys()) {
        if (rs.next()) {
          calculationSubPointSetId = rs.getInt(1);
        } else {
          throw new SQLException("No generated key obtained while creating new calculation sub point set.");
        }
      }
    }
    return calculationSubPointSetId;
  }

  public static int insertCalculationSubPointsForSet(final Connection connection, final int calculationPointSetId,
      final List<IsSubPoint> calculationPoints) throws SQLException {
    return insertCalculationSubPointsForSet(connection, calculationPointSetId, calculationPoints, INSERT_CALCULATION_SUB_POINTS);
  }

  public static int insertCalculationSubPointsForSetSafe(final Connection connection, final int calculationPointSetId,
      final List<IsSubPoint> calculationPoints) throws SQLException {
    return insertCalculationSubPointsForSet(connection, calculationPointSetId, calculationPoints, INSERT_CALCULATION_SUB_POINTS_SAFE);
  }

  private static int insertCalculationSubPointsForSet(final Connection connection, final int calculationPointSetId,
      final List<IsSubPoint> calculationPoints, final String query) throws SQLException {
    final BatchInserter<IsSubPoint> inserter = new BatchInserter<IsSubPoint>() {
      @Override
      public void setParameters(final PreparedStatement ps, final IsSubPoint point) throws SQLException {
        int paramIdx = 1;
        ps.setInt(paramIdx++, calculationPointSetId);
        ps.setInt(paramIdx++, point.getId());
        ps.setInt(paramIdx++, point.getParentId());
        ps.setInt(paramIdx++, point.getLevel());
        ps.setDouble(paramIdx++, point.getX());
        ps.setDouble(paramIdx++, point.getY());
      }
    };
    inserter.setBatchSize(BATCH_SIZE);
    inserter.insertBatch(connection, query, calculationPoints);

    return calculationPointSetId;
  }

  public static Collection<EmissionResultKey> getCalculationResultSets(final Connection connection, final int calculationId) throws SQLException {
    final Set<EmissionResultKey> erks = EnumSet.noneOf(EmissionResultKey.class);

    try (final PreparedStatement selectPS = connection.prepareStatement(GET_CALCULATION_RESULT_SETS)) {
      QueryUtil.setValues(selectPS, calculationId);
      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          erks.add(EmissionResultKey.valueOf(Substance.substanceFromId(rs.getInt("substance_id")),
              EmissionResultType.safeValueOf((String) rs.getObject("result_type"))));
        }
      }
    }
    return erks;
  }

  public static List<Integer> getCalculationResultIdSets(final Connection connection, final int calculationId) throws SQLException {
    final List<Integer> results = new ArrayList<>();

    try (final PreparedStatement selectPS = connection.prepareStatement(GET_CALCULATION_RESULT_SETS)) {
      QueryUtil.setValues(selectPS, calculationId);
      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          results.add(rs.getInt("calculation_result_set_id"));
        }
      }
    }
    return results;
  }

  public static Map<Integer, EmissionResultKey> getCalculationResultIdSetsEmissionResultKey(final Connection connection, final int calculationId)
      throws SQLException {
    final Map<Integer, EmissionResultKey> results = new HashMap<>();

    try (final PreparedStatement selectPS = connection.prepareStatement(GET_CALCULATION_RESULT_SETS)) {
      QueryUtil.setValues(selectPS, calculationId);
      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          results.put(rs.getInt("calculation_result_set_id"),
              EmissionResultKey.valueOf(Substance.substanceFromId(rs.getInt("substance_id")),
                  EmissionResultType.safeValueOf((String) rs.getObject("result_type"))));
        }
      }
    }
    return results;
  }

  private static int getCalculationResultSetId(final Connection connection, final int calculationId, final EmissionResultKey key,
      final CalculationResultSetType crsType, final int resetSetTypeKey) throws SQLException {
    int resultSetId;
    try (final PreparedStatement selectPS = connection.prepareStatement(GET_CALCULATION_RESULT_SET_ID)) {
      QueryUtil.setValues(selectPS, calculationId, key.getEmissionResultType().toDatabaseString(), key.getSubstance().getId(),
          crsType.toDatabaseString(), resetSetTypeKey);
      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          resultSetId = rs.getInt("calculation_result_set_id");
        } else {
          resultSetId = -1;
        }
      }
    }
    // if no id in db create one
    if (resultSetId == -1) {
      try {
        resultSetId = insertCalculationResultSet(connection, calculationId, key, crsType, resetSetTypeKey);
      } catch (final PSQLException e) {
        if (PMF.UNIQUE_VIOLATION.equals(e.getSQLState())) {
          // poor man fallback in case 2 threads try to create the result set at the same time.
          resultSetId = getCalculationResultSetId(connection, calculationId, key, crsType, resetSetTypeKey);
        } else {
          throw e;
        }
      }
    }
    return resultSetId;
  }

  /**
   * Creates a new calculation in the database. Including the calculation points (no results).
   *
   * @param con The connection to use
   * @param scenarioCalculations the scenario calculations to store
   * @param customPointsInDatabase if true store custom points in the database
   * @param subPointsInDatabase if true store sub receptor points in the database
   * @throws SQLException If an error occurred communicating with the DB
   */
  public static void insertCalculations(final Connection con, final ScenarioCalculations scenarioCalculations, final boolean customPointsInDatabase,
      final boolean subPointsInDatabase) throws SQLException {
    final CalculationPointSetTracker pointSetTracker = scenarioCalculations.getCalculationPointSetTracker();
    if (customPointsInDatabase && !scenarioCalculations.getCalculationPoints().isEmpty()
        && pointSetTracker.getCalculationPointSetId() == null) {
      pointSetTracker.setCalculationPointSetId(insertCalculationPointSet(con, scenarioCalculations.getCalculationPoints()));
    }
    if (subPointsInDatabase && pointSetTracker.getCalculationSubPointSetId() == null) {
      pointSetTracker.setCalculationSubPointSetId(insertSubPointSet(con));
    }
    for (final Calculation calculation : scenarioCalculations.getCalculations()) {
      insertCalculation(con, calculation, pointSetTracker.getCalculationPointSetId(), pointSetTracker.getCalculationSubPointSetId());
    }
  }

  private static int insertCalculationResultSet(final Connection connection, final int calculationId, final EmissionResultKey key,
      final CalculationResultSetType crsType, final int resetSetTypeKey) throws SQLException {
    try (final PreparedStatement insertPS = connection.prepareStatement(INSERT_CALCULATION_RESULT_SET, Statement.RETURN_GENERATED_KEYS)) {
      QueryUtil.setValues(insertPS, calculationId, crsType.toDatabaseString(), resetSetTypeKey, key.getEmissionResultType().toDatabaseString(),
          key.getSubstance().getId());
      insertPS.executeUpdate();
      try (final ResultSet insertRS = insertPS.getGeneratedKeys()) {
        if (insertRS.next()) {
          return insertRS.getInt(1);
        } else {
          throw new SQLException("No generated key obtained while saving new calculation result set. (" + calculationId + ", " + key + ")");
        }
      }
    }
  }

  public static int insertCalculationResults(final Connection connection, final int calculationId, final List<AeriusResultPoint> result)
      throws SQLException {
    return insertCalculationResults(connection, calculationId, CalculationResultSetType.TOTAL, 0, result);
  }

  /**
   * Insert results from a calculation. If calculation is not found, an illegal argument exception will occur. If there is already a result in the
   * database for the corresponding substance and receptor ID, the new value will NOT overwrite the existing value. It won't cause an exception either
   * and will just continue to insert the next result.
   *
   * @param connection The connection to use
   * @param calculationId id of the calculation
   * @param result Contains all results (where each receptor ID should correspond to supplied AeriusPoints)
   * @return The number of rows inserted
   * @throws SQLException If an error occurred communicating with the DB or when any of the ID's are unknown
   */
  public static int insertCalculationResults(final Connection connection, final int calculationId, final CalculationResultSetType crsType,
      final int resetSetTypeKey, final List<AeriusResultPoint> result) throws SQLException {
    return insertSafeCalculationResults(connection, calculationId, crsType, resetSetTypeKey, result);
  }

  public static int insertCalculationResultsUnsafe(final Connection connection, final int calculationId, final List<AeriusResultPoint> result)
      throws SQLException {
    return insertUnsafeCalculationResults(connection, calculationId, CalculationResultSetType.TOTAL, 0, result);
  }

  /**
   * Insert results from a calculation _unsafely_. If calculation is not found, an illegal argument exception will occur.
   *
   * Use only when you can guarantee there will be no duplicates.
   *
   * @param connection The connection to use.
   * @param calculationId id of the calculation
   * @param result contains the results (where each receptor ID should correspond to supplied AeriusPoints).
   * @return The number of rows inserted.
   * @throws SQLException If an error occurred communicating with the DB or when any of the ID's are unknown.
   */
  public static int insertCalculationResultsUnsafe(final Connection connection, final int calculationId, final CalculationResultSetType crsType,
      final int resetSetTypeKey, final List<AeriusResultPoint> result) throws SQLException {
    return insertUnsafeCalculationResults(connection, calculationId, crsType, resetSetTypeKey, result);
  }

  private static Map<AeriusPointType, List<PointResult>> toPointResults(final Connection connection, final int calculationId,
      final CalculationResultSetType crsType, final int resetSetTypeKey, final List<AeriusResultPoint> result) throws SQLException {
    final Calculation calculation = getCalculation(connection, calculationId);
    if (calculation == null) {
      throw new IllegalArgumentException("Calculation should exist in DB.");
    }
    // each emission result key has their own set in DB, only query for it once though.
    final Map<EmissionResultKey, Integer> resultSetIdMap = new EnumMap<>(EmissionResultKey.class);
    final Map<AeriusPointType, List<PointResult>> insertables = new EnumMap<>(AeriusPointType.class);
    for (final AeriusResultPoint receptorPoint : result) {
      if (!insertables.containsKey(receptorPoint.getPointType())) {
        insertables.put(receptorPoint.getPointType(), new ArrayList<>());
      }
      final List<PointResult> points = insertables.get(receptorPoint.getPointType());
      for (final EmissionResultKey emissionResultKey : determineEmissionResultKeys(receptorPoint)) {
        if (emissionResultKey.getEmissionResultType() != EmissionResultType.EXCEEDANCE_DAYS) {
          if (!resultSetIdMap.containsKey(emissionResultKey)) {
            resultSetIdMap.put(emissionResultKey,
                getCalculationResultSetId(connection, calculation.getCalculationId(), emissionResultKey, crsType, resetSetTypeKey));
          }
          points.add(new PointResult(receptorPoint.getId(), receptorPoint.getParentId(), resultSetIdMap.get(emissionResultKey),
              receptorPoint.getEmissionResult(emissionResultKey)));
        }
      }
    }
    return insertables;
  }

  private static Set<EmissionResultKey> determineEmissionResultKeys(final AeriusResultPoint resultPoint) {
    final Set<EmissionResultKey> correctKeys = resultPoint.getEmissionResults().isEmpty()
        ? Set.of()
        : EnumSet.copyOf(resultPoint.getEmissionResults().getHashMap().keySet());
    if (correctKeys.contains(EmissionResultKey.NOX_DEPOSITION) || correctKeys.contains(EmissionResultKey.NH3_DEPOSITION)) {
      correctKeys.add(EmissionResultKey.NOXNH3_DEPOSITION);
    }
    return correctKeys;
  }

  private static int insertSafeCalculationResults(final Connection con, final int calculationId, final CalculationResultSetType crsType,
      final int resetSetTypeKey, final List<AeriusResultPoint> result) throws SQLException {
    final Map<AeriusPointType, List<PointResult>> splitResults = toPointResults(con, calculationId, crsType, resetSetTypeKey, result);
    int inserted = 0;
    if (splitResults.containsKey(AeriusPointType.RECEPTOR)) {
      inserted += insertCalculationResults(con, splitResults.get(AeriusPointType.RECEPTOR), SAFE_INSERT_CALCULATION_RESULTS, true, false);
    }
    if (splitResults.containsKey(AeriusPointType.POINT)) {
      inserted += insertCalculationResults(con, splitResults.get(AeriusPointType.POINT), SAFE_INSERT_CALCULATION_POINT_RESULTS, true, false);
    }
    if (splitResults.containsKey(AeriusPointType.SUB_RECEPTOR)) {
      inserted += insertCalculationResults(con, splitResults.get(AeriusPointType.SUB_RECEPTOR), SAFE_INSERT_SUB_POINT_RESULTS, true, true);
    }
    return inserted;
  }

  private static int insertUnsafeCalculationResults(final Connection con, final int calculationId, final CalculationResultSetType crsType,
      final int resetSetTypeKey, final List<AeriusResultPoint> result) throws SQLException {
    final Map<AeriusPointType, List<PointResult>> splitResults = toPointResults(con, calculationId, crsType, resetSetTypeKey, result);
    int inserted = 0;
    if (splitResults.containsKey(AeriusPointType.RECEPTOR)) {
      inserted += insertCalculationResults(con, splitResults.get(AeriusPointType.RECEPTOR), UNSAFE_INSERT_CALCULATION_RESULTS, false, false);
    }
    if (splitResults.containsKey(AeriusPointType.POINT)) {
      inserted += insertCalculationResults(con, splitResults.get(AeriusPointType.POINT), UNSAFE_INSERT_CALCULATION_POINT_RESULTS, false, false);
    }
    if (splitResults.containsKey(AeriusPointType.SUB_RECEPTOR)) {
      inserted += insertCalculationResults(con, splitResults.get(AeriusPointType.SUB_RECEPTOR), UNSAFE_INSERT_SUB_POINT_RESULTS, false, true);
    }
    return inserted;
  }

  private static int insertCalculationResults(final Connection connection, final List<PointResult> insertables, final String query,
      final boolean safeInserts, final boolean isSubPoint) throws SQLException {
    final BatchInserter<PointResult> inserter = new BatchInserter<CalculationRepository.PointResult>() {

      @Override
      public void setParameters(final PreparedStatement ps, final PointResult object) throws SQLException {
        int paramIdx = 1;
        ps.setInt(paramIdx++, object.getResultSetId());
        ps.setInt(paramIdx++, object.getPointId());
        if (isSubPoint) {
          ps.setInt(paramIdx++, object.getParentId());
        }
        // Negative values not allowed; safety check here:
        if (object.getResult() < 0) {
          LOG.debug("Result < 0: resultsetId:{}, id:{}, value:{}", object.getResultSetId(), object.getPointId(), object.getResult());
        }
        ps.setDouble(paramIdx++, Math.max(0, object.getResult()));
        if (safeInserts) {
          ps.setInt(paramIdx++, object.getResultSetId());
          ps.setInt(paramIdx++, object.getPointId());
          if (isSubPoint) {
            ps.setInt(paramIdx++, object.getParentId());
          }
        }
      }
    };
    inserter.setBatchSize(safeInserts ? BATCH_SIZE : UNSAFE_BATCH_SIZE);
    return inserter.insertBatch(connection, query, insertables);
  }

  public static void insertArchiveContribution(final Connection con, final int proposedCalculationId, final List<AeriusResultPoint> result)
      throws SQLException {
    // by definition the archive contribution is only available for total result sets.
    final Map<AeriusPointType, List<PointResult>> splitResults =
        toPointResults(con, proposedCalculationId, CalculationResultSetType.TOTAL, 0, result);
    if (splitResults.containsKey(AeriusPointType.RECEPTOR)) {
      insertArchiveContributionResults(con, splitResults.get(AeriusPointType.RECEPTOR), INSERT_ARCHIVE_CONTRIBUTION_RESULTS, false);
    }
    if (splitResults.containsKey(AeriusPointType.SUB_RECEPTOR)) {
      insertArchiveContributionResults(con, splitResults.get(AeriusPointType.SUB_RECEPTOR), INSERT_ARCHIVE_CONTRIBUTION_SUB_POINT_RESULTS, true);
    }
    if (splitResults.containsKey(AeriusPointType.POINT)) {
      LOG.debug("Got archive results for custom calculation points ({}), ignoring these.", splitResults.get(AeriusPointType.POINT).size());
    }
  }

  private static int insertArchiveContributionResults(final Connection connection, final List<PointResult> insertables, final String query,
      final boolean isSubPoint) throws SQLException {
    final BatchInserter<PointResult> inserter = new BatchInserter<CalculationRepository.PointResult>() {

      @Override
      public void setParameters(final PreparedStatement ps, final PointResult object) throws SQLException {
        int paramIdx = 1;
        ps.setInt(paramIdx++, object.getResultSetId());
        ps.setInt(paramIdx++, object.getPointId());
        if (isSubPoint) {
          ps.setInt(paramIdx++, object.getParentId());
        }
        ps.setDouble(paramIdx++, object.getResult());
      }
    };
    inserter.setBatchSize(BATCH_SIZE);
    return inserter.insertBatch(connection, query, insertables);
  }

  /**
   * Retrieve the points for a certain calculation (without results).
   *
   * @param connection The connection to use
   * @param calculationId The ID of the calculation to insert the options for
   * @return The (current) CalculationResult that match this calculation ID
   * @throws SQLException If an error occurred communicating with the DB
   */
  public static List<CalculationPointFeature> getCalculatedPoints(final Connection connection, final int calculationId) throws SQLException {
    final List<CalculationPointFeature> points = new ArrayList<>();

    points.addAll(getReceptors(connection, calculationId));
    points.addAll(getCalculationPoints(connection, calculationId));

    return points;
  }

  public static List<ReceptorResult> getCalculationResultsFromSet(final Connection connection,
      final int[] calculationIds, final int calculationResultSetId, final SummaryHexagonType hexagonType) throws SQLException {
    final List<ReceptorResult> results = new ArrayList<>();
    try (final PreparedStatement ps = connection.prepareStatement(GET_CALCULATION_RESULTS_FROM_SET)) {
      QueryUtil.setValues(ps, calculationIds, calculationResultSetId, hexagonType.name().toLowerCase(Locale.ROOT));
      try (final ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          final ReceptorResult result = new ReceptorResult(
              QueryUtil.getInt(rs, QueryAttribute.RECEPTOR_ID),
              QueryUtil.getDouble(rs, QueryAttribute.RESULT));
          results.add(result);
        }
      }
    }
    return results;
  }

  public static List<ReceptorResult> getArchiveContributionReceptorsFromSet(final Connection connection, final int calculationResultSetId)
      throws SQLException {
    final List<ReceptorResult> results = new ArrayList<>();
    try (final PreparedStatement ps = connection.prepareStatement(GET_ARCHIVE_CONTRIBUTION_RECEPTORS_FROM_SET)) {
      QueryUtil.setValues(ps, calculationResultSetId);
      try (final ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          final ReceptorResult result = new ReceptorResult(
              QueryUtil.getInt(rs, QueryAttribute.RECEPTOR_ID),
              QueryUtil.getDouble(rs, QueryAttribute.RESULT));
          results.add(result);
        }
      }
    }
    return results;
  }

  private static List<CalculationPointFeature> getReceptors(final Connection connection, final int calculationId) throws SQLException {
    final List<CalculationPointFeature> receptors = new ArrayList<>();
    final ReceptorUtil receptorUtil = new ReceptorUtil(ReceptorGridSettingsRepository.getReceptorGridSettings(connection));
    try (final PreparedStatement ps = connection.prepareStatement(GET_ALL_RECEPTORS.get())) {
      GET_ALL_RECEPTORS.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);

      try (final ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);

          receptors.add(CalculationRepositoryUtil.toReceptorFeature(receptorUtil, receptorId));
        }
      }
    }
    return receptors;
  }

  private static List<CalculationPointFeature> getCalculationPoints(final Connection connection, final int calculationId) throws SQLException {
    final List<CalculationPointFeature> points = new ArrayList<>();
    try (final PreparedStatement ps = connection.prepareStatement(GET_ALL_CALCULATION_POINTS.get())) {
      GET_ALL_CALCULATION_POINTS.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);

      try (final ResultSet rs = ps.executeQuery()) {

        while (rs.next()) {
          final int calculationPointId = QueryAttribute.CALCULATION_POINT_ID.getInt(rs);
          final double x = QueryUtil.getDouble(rs, QueryAttribute.X_COORD);
          final double y = QueryUtil.getDouble(rs, QueryAttribute.Y_COORD);
          final String label = QueryAttribute.LABEL.getString(rs);

          points.add(CalculationRepositoryUtil.toCustomFeature(calculationPointId, x, y, label));
        }
      }
    }
    return points;
  }

  public static int insertOverlappingHexagons(final Connection connection, final int calculationId, final List<AeriusPoint> points)
      throws SQLException {
    final List<AeriusPoint> receptorPoints = points.stream()
        .filter(point -> point.getPointType() == AeriusPointType.RECEPTOR)
        .collect(Collectors.toList());
    final BatchInserter<AeriusPoint> inserter = new BatchInserter<AeriusPoint>() {
      @Override
      public void setParameters(final PreparedStatement ps, final AeriusPoint point) throws SQLException {
        ps.setInt(1, calculationId);
        ps.setInt(2, point.getId());
      }
    };
    inserter.setBatchSize(BATCH_SIZE);
    final int inserted = inserter.insertBatch(connection, INSERT_CALCULATION_OVERLAPPING_RECEPTORS, receptorPoints);
    if (inserted != receptorPoints.size()) {
      throw new SQLException("Did not save all overlapping points! calculation id: " + calculationId + ". Inserted points: "
          + inserted + ". Expected: " + receptorPoints.size());
    }
    return inserted;
  }

  public static void addEdgeEffectInformation(final Connection connection, final int calculationId,
      final Collection<CalculationPointFeature> calculationPointFeatures) throws SQLException {
    final List<ReceptorPoint> receptorPoints = calculationPointFeatures.stream()
        .map(CalculationPointFeature::getProperties)
        .filter(ReceptorPoint.class::isInstance)
        .map(ReceptorPoint.class::cast)
        .collect(Collectors.toList());
    if (!receptorPoints.isEmpty()) {
      final Set<Integer> overlappingReceptors = getOverlappingReceptors(connection, calculationId);
      receptorPoints.forEach(receptorPoint -> receptorPoint.setEdgeEffect(!overlappingReceptors.contains(receptorPoint.getReceptorId())));
    }
  }

  private static Set<Integer> getOverlappingReceptors(final Connection connection, final int calculationId) throws SQLException {
    final Set<Integer> overlappingReceptorIds = new HashSet<>();
    try (final PreparedStatement statement = connection.prepareStatement(GET_CALCULATION_OVERLAPPING_RECEPTORS.get())) {
      GET_CALCULATION_OVERLAPPING_RECEPTORS.setParameter(statement, QueryAttribute.CALCULATION_ID, calculationId);

      try (final ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          overlappingReceptorIds.add(QueryAttribute.RECEPTOR_ID.getInt(rs));
        }
      }
    }
    return overlappingReceptorIds;
  }

  public static void insertArchiveMetadata(final Connection con, final int calculationId, final ArchiveMetaData archiveMetaData)
      throws SQLException, IOException {
    try (final PreparedStatement insertPS = con.prepareStatement(INSERT_ARCHIVE_METADATA)) {
      int paramIdx = 1;
      insertPS.setInt(paramIdx++, calculationId);
      insertPS.setString(paramIdx++, OBJECT_MAPPER.writeValueAsString(archiveMetaData));
      insertPS.executeUpdate();
    }
  }

  public static ArchiveMetaData getArchiveMetadata(final Connection con, final int calculationId)
      throws SQLException, IOException {
    ArchiveMetaData metadata = null;
    try (final PreparedStatement statement = con.prepareStatement(GET_ARCHIVE_METADATA.get())) {
      GET_ARCHIVE_METADATA.setParameter(statement, QueryAttribute.CALCULATION_ID, calculationId);

      try (final ResultSet rs = statement.executeQuery()) {
        if (rs.next()) {
          metadata = OBJECT_MAPPER.readValue(QueryAttribute.METADATA.getString(rs), ArchiveMetaData.class);
        }
      }
    }
    return metadata;
  }

  private static class PointResult {

    private final int pointId;
    private final Integer parentId;
    private final int resultSetId;
    private final double result;

    PointResult(final int pointId, final Integer parentId, final int resultSetId, final double result) {
      this.pointId = pointId;
      this.parentId = parentId;
      this.resultSetId = resultSetId;
      this.result = result;
    }

    public int getPointId() {
      return pointId;
    }

    public Integer getParentId() {
      return parentId;
    }

    public int getResultSetId() {
      return resultSetId;
    }

    public double getResult() {
      return result;
    }

    @Override
    public String toString() {
      return "PointResult [resultSetId=" + resultSetId + ", pointId=" + pointId + ", parentId=" + parentId + ", result=" + result + "]";
    }
  }
}
