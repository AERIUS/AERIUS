/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;

import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultSetType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;

/**
 * Base class for calculation related tests.
 */
public abstract class CalculationRepositoryTestBase extends BaseDBTest {

  //x: 187026 y: 464594, Veluwe
  protected static final int RECEPTOR_POINT_ID_1 = 4776053;
  //x: 112298 y: 556953, Duinen en Lage Land Texel
  protected static final int RECEPTOR_POINT_ID_2 = 7404003;
  //point outside nature area; buried the body here.
  protected static final int RECEPTOR_POINT_ID_3 = 4758851;
  //x: 174184 y: 326512, Foreign area
  protected static final int RECEPTOR_POINT_ID_4 = 846454;
  //x: 270595 y: 474158, Foreign area
  protected static final int RECEPTOR_POINT_ID_5 = 5048664;
  //x: 84659 y: 413014, point shared between Kramerak-Volkerak and Haringvliet, only first area has relevant habitat
  protected static final int RECEPTOR_POINT_ID_6 = 3307663;
  //x: 168134 y: 447132, point in Binnenveld only available in extra_assessment_receptors, not in receptors_to_critical_deposition_areas
  protected static final int RECEPTOR_POINT_ID_7 = 4279027;

  protected static final double RECEPTOR_1_CONCENTRATION = 42.91;
  protected static final double RECEPTOR_1_DEPOSITION = 234.23;
  protected static final double RECEPTOR_1_NUM_EXCESS_DAYS = 60;
  protected static final double RECEPTOR_2_CONCENTRATION = 4.82;
  protected static final double RECEPTOR_2_DEPOSITION = 1.23;
  protected static final double RECEPTOR_2_NUM_EXCESS_DAYS = 10;
  protected static final double RECEPTOR_3_CONCENTRATION = 9.31;
  protected static final double RECEPTOR_3_DEPOSITION = 9.1;
  protected static final double RECEPTOR_3_NUM_EXCESS_DAYS = 9;
  protected static final double RECEPTOR_4_CONCENTRATION = 23.2;
  protected static final double RECEPTOR_4_DEPOSITION = 23.1;
  protected static final double RECEPTOR_4_NUM_EXCESS_DAYS = 15;
  protected static final double RECEPTOR_5_CONCENTRATION = 0.2;
  protected static final double RECEPTOR_5_DEPOSITION = 0.1;
  protected static final double RECEPTOR_5_NUM_EXCESS_DAYS = 0.15;
  protected static final double RECEPTOR_6_CONCENTRATION = 0.18;
  protected static final double RECEPTOR_6_DEPOSITION = 0.08;
  protected static final double RECEPTOR_6_NUM_EXCESS_DAYS = 0.13;
  protected static final double RECEPTOR_7_CONCENTRATION = 0.16;
  protected static final double RECEPTOR_7_DEPOSITION = 0.06;
  protected static final double RECEPTOR_7_NUM_EXCESS_DAYS = 0.11;

  protected static final int TEST_CALCULATION_RESULTS_SECTOR_ID = 3220;
  private static final int YEAR = 2030;

  protected Calculation calculation;

  protected double calculationResultScale = 1.0; // Change this to create different results

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    refreshCalculationSet();
  }

  protected void insertCalculationResults() throws SQLException {
    insertCalculationResultsNH3();
    insertCalculationResultPM10();
  }

  protected int insertCalculationPoints() throws SQLException {
    final List<CalculationPointFeature> calculationPointList = getExampleCalculationPointOutputData();
    return CalculationRepository.insertCalculationPointSet(getConnection(), calculationPointList);
  }

  protected int insertCalculationSubPoints() throws SQLException {
    final int subPointSetId = CalculationRepository.insertSubPointSet(getConnection());
    final List<IsSubPoint> subPoints = getExampleSubPointsData();
    CalculationRepository.insertCalculationSubPointsForSet(getConnection(), subPointSetId, subPoints);
    return subPointSetId;
  }

  protected void insertCalculationResultsNH3() throws SQLException {
    final List<AeriusResultPoint> calculationResult = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
    // 5 receptors * 2 result types = 10 results.
    // However, since NH3_DEPOSITION is included, NOXNH3_DEPOSITION will be included as well, so 5 * 3
    final int numberOfResults = 15;
    insertResults(calculationResult, numberOfResults);
  }

  protected void insertCalculationResultPM10() throws SQLException {
    final List<AeriusResultPoint> calculationResult = getExampleOPSOutputData(EmissionResultKey.PM10_CONCENTRATION,
        EmissionResultKey.PM10_EXCEEDANCE_DAYS);
    final int numberOfResults = 5;
    // 5 receptors * 2 result types = 6 results. However, num excess days won't be persisted. Hence 3 results.
    insertResults(calculationResult, numberOfResults);
  }

  protected void insertResults(final List<AeriusResultPoint> calculationResult, final int numberOfResults) throws SQLException {
    final int numTotal = CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(),
        calculationResult);
    assertResultsEquals("Number of emission results inserted", numberOfResults, numTotal);

    final int numSector = CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(),
        CalculationResultSetType.SECTOR, TEST_CALCULATION_RESULTS_SECTOR_ID, calculationResult);
    assertResultsEquals("Number of emission results inserted", numberOfResults, numSector);
  }

  protected void insertArchiveContribution(final int calculationId) throws SQLException {
    insertArchiveContributionNH3(calculationId);
    insertArchiveContributionPM10(calculationId);
  }

  protected void insertArchiveContributionNH3(final int calculationId) throws SQLException {
    final List<AeriusResultPoint> calculationResult = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
    insertArchiveContribution(calculationId, calculationResult);
  }

  protected void insertArchiveContributionPM10(final int calculationId) throws SQLException {
    final List<AeriusResultPoint> calculationResult = getExampleOPSOutputData(EmissionResultKey.PM10_CONCENTRATION,
        EmissionResultKey.PM10_EXCEEDANCE_DAYS);
    insertArchiveContribution(calculationId, calculationResult);
  }

  protected void insertArchiveContribution(final int calculationId, final List<AeriusResultPoint> calculationResult) throws SQLException {
    CalculationRepository.insertArchiveContribution(getConnection(), calculationId, calculationResult);
  }

  protected void assertResultsEquals(final String message, final int numberOfResults, final int numSector) {
    assertEquals(numberOfResults, numSector, message);
  }

  protected List<AeriusResultPoint> getExampleOPSOutputData(final EmissionResultKey concentrationKey, final EmissionResultKey otherKey) {
    final List<AeriusResultPoint> result = new ArrayList<>();

    result.add(createResult(RECEPTOR_POINT_ID_1, concentrationKey, otherKey,
        RECEPTOR_1_CONCENTRATION, RECEPTOR_1_DEPOSITION, RECEPTOR_1_NUM_EXCESS_DAYS));
    result.add(createResult(RECEPTOR_POINT_ID_2, concentrationKey, otherKey,
        RECEPTOR_2_CONCENTRATION, RECEPTOR_2_DEPOSITION, RECEPTOR_2_NUM_EXCESS_DAYS));
    result.add(createResult(RECEPTOR_POINT_ID_3, concentrationKey, otherKey,
        RECEPTOR_3_CONCENTRATION, RECEPTOR_3_DEPOSITION, RECEPTOR_3_NUM_EXCESS_DAYS));

    //point in foreign country, somewhere in belgium... Disabled for now as it breaks quite some unittests
    //    final AeriusResultPoint rp4 = new AeriusResultPoint();
    //    rp4.setId(RECEPTOR_POINT_ID_4);
    //    ReceptorUtil.setAeriusPointFromId(rp4);
    //    rp4.setEmissionResult(concentrationKey, RECEPTOR_4_CONCENTRATION * calculationResultScale);
    //    rp4.setEmissionResult(otherKey, (otherKey.getEmissionResultType() == EmissionResultType.DEPOSITION ? RECEPTOR_4_DEPOSITION
    //        : RECEPTOR_4_NUM_EXCESS_DAYS) * calculationResultScale);
    //    result.add(rp4);

    //another point in another foreign country, this time germany
    //    final AeriusResultPoint rp5 = new AeriusResultPoint();
    //    rp5.setId(RECEPTOR_POINT_ID_5);
    //    ReceptorUtil.setAeriusPointFromId(rp5);
    //    rp5.setEmissionResult(concentrationKey, RECEPTOR_5_CONCENTRATION * calculationResultScale);
    //    rp5.setEmissionResult(otherKey, (otherKey.getEmissionResultType() == EmissionResultType.DEPOSITION ? RECEPTOR_5_DEPOSITION
    //        : RECEPTOR_5_NUM_EXCESS_DAYS) * calculationResultScale);
    //    result.add(rp5);

    result.add(createResult(RECEPTOR_POINT_ID_6, concentrationKey, otherKey,
        RECEPTOR_6_CONCENTRATION, RECEPTOR_6_DEPOSITION, RECEPTOR_6_NUM_EXCESS_DAYS));
    result.add(createResult(RECEPTOR_POINT_ID_7, concentrationKey, otherKey,
        RECEPTOR_7_CONCENTRATION, RECEPTOR_7_DEPOSITION, RECEPTOR_7_NUM_EXCESS_DAYS));

    return result;
  }

  protected AeriusResultPoint createResult(final int id, final EmissionResultKey concentrationKey, final EmissionResultKey otherKey,
      final double... emissions) {
    final AeriusResultPoint rp1 = new AeriusResultPoint(new AeriusPoint());
    rp1.setId(id);
    setCoordinatesFromId(rp1);
    if (concentrationKey != null) {
      rp1.setEmissionResult(concentrationKey, emissions[0] * calculationResultScale);
    }
    rp1.setEmissionResult(otherKey, (otherKey.getEmissionResultType() == EmissionResultType.DEPOSITION ? emissions[1] : emissions[2])
        * calculationResultScale);
    return rp1;
  }

  protected List<CalculationPointFeature> getExampleCalculationPointOutputData() {
    final List<CalculationPointFeature> result = new ArrayList<>();

    final CalculationPointFeature feature1 = new CalculationPointFeature();
    feature1.setGeometry(RECEPTOR_UTIL.getPointFromReceptorId(RECEPTOR_POINT_ID_1));

    final CustomCalculationPoint customPoint1 = new CustomCalculationPoint();
    customPoint1.setCustomPointId(1);
    customPoint1.setLabel("Test receptor 1");
    feature1.setProperties(customPoint1);
    result.add(feature1);

    final CalculationPointFeature feature2 = new CalculationPointFeature();
    feature2.setGeometry(RECEPTOR_UTIL.getPointFromReceptorId(RECEPTOR_POINT_ID_2));

    final CustomCalculationPoint customPoint2 = new CustomCalculationPoint();
    customPoint2.setCustomPointId(2);
    customPoint2.setLabel("Test receptor 2");
    feature2.setProperties(customPoint2);
    result.add(feature2);

    return result;
  }

  protected List<IsSubPoint> getExampleSubPointsData() {
    final List<IsSubPoint> result = new ArrayList<>();

    //Let's mock some subpoints around 1 of the receptors
    final Point point = RECEPTOR_UTIL.getPointFromReceptorId(RECEPTOR_POINT_ID_1);
    int id = 1;
    for (int displaceX = -1; displaceX <= 1; displaceX++) {
      for (int displaceY = -1; displaceY <= 1; displaceY++) {
        final IsSubPoint subPoint = mock(IsSubPoint.class);
        when(subPoint.getId()).thenReturn(id);
        when(subPoint.getPointType()).thenReturn(AeriusPointType.SUB_RECEPTOR);
        when(subPoint.getParentId()).thenReturn(RECEPTOR_POINT_ID_1);
        when(subPoint.getX()).thenReturn(point.getX() + displaceX);
        when(subPoint.getY()).thenReturn(point.getY() + displaceY);
        when(subPoint.getLevel()).thenReturn(2);
        result.add(subPoint);
        id++;
      }
    }

    return result;
  }

  protected CalculationPointFeature createReceptorFeature(final int receptorId) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setGeometry(RECEPTOR_UTIL.getPointFromReceptorId(receptorId));
    final ReceptorPoint receptorPoint = new ReceptorPoint();
    receptorPoint.setReceptorId(receptorId);
    feature.setProperties(receptorPoint);
    return feature;
  }

  protected List<AeriusResultPoint> toAeriusResultPoints(final Substance substance, final List<CalculationPointFeature> exampleData) {
    final List<AeriusResultPoint> results = new ArrayList<>();
    for (final CalculationPointFeature feature : exampleData) {
      if (feature.getProperties().getId() == 1) {
        final AeriusResultPoint resultPoint = new AeriusResultPoint(1, 0, AeriusPointType.POINT,
            feature.getGeometry().getX(), feature.getGeometry().getY());
        resultPoint.setLabel(feature.getProperties().getLabel());
        resultPoint.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.CONCENTRATION),
            RECEPTOR_1_CONCENTRATION * calculationResultScale);
        resultPoint.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.DEPOSITION),
            RECEPTOR_1_DEPOSITION * calculationResultScale);
        resultPoint.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.DRY_DEPOSITION),
            (RECEPTOR_1_DEPOSITION * calculationResultScale) / 2 + 1);
        resultPoint.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.WET_DEPOSITION),
            (RECEPTOR_1_DEPOSITION * calculationResultScale) / 2 - 1);
        results.add(resultPoint);
      } else if (feature.getProperties().getId() == 2) {
        final AeriusResultPoint resultPoint = new AeriusResultPoint(2, 0, AeriusPointType.POINT,
            feature.getGeometry().getX(), feature.getGeometry().getY());
        resultPoint.setLabel(feature.getProperties().getLabel());
        resultPoint.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.CONCENTRATION),
            RECEPTOR_2_CONCENTRATION * calculationResultScale);
        resultPoint.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.DEPOSITION),
            RECEPTOR_2_DEPOSITION * calculationResultScale);
        results.add(resultPoint);
      }
    }
    return results;
  }

  protected List<AeriusResultPoint> toAeriusResultSubPoints(final Substance substance, final List<IsSubPoint> exampleData) {
    final List<AeriusResultPoint> results = new ArrayList<>();
    for (final IsSubPoint subPoint : exampleData) {
      final AeriusResultPoint resultPoint = new AeriusResultPoint(subPoint.getId(), subPoint.getParentId(),
          subPoint.getPointType(), subPoint.getX(), subPoint.getY());
      resultPoint.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.CONCENTRATION),
          RECEPTOR_1_CONCENTRATION * calculationResultScale + subPoint.getId());
      resultPoint.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.DEPOSITION),
          RECEPTOR_1_DEPOSITION * calculationResultScale + subPoint.getId());
      resultPoint.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.DRY_DEPOSITION),
          (RECEPTOR_1_DEPOSITION * calculationResultScale) / 2 + 1 + subPoint.getId());
      resultPoint.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.WET_DEPOSITION),
          (RECEPTOR_1_DEPOSITION * calculationResultScale) / 2 - 1 + subPoint.getId());
      results.add(resultPoint);
    }
    return results;
  }

  private void refreshCalculationSet() throws SQLException {
    removeCalculationResults();
    createCalculation(null);
  }

  protected void createCalculation(final Integer calculationPointSetId) throws SQLException {
    if (calculation == null) {
      calculation = new Calculation();
      calculation.setYear(YEAR);
      calculation = CalculationRepository.insertCalculation(getConnection(), calculation, calculationPointSetId, null);
    }
  }

  protected void removeCalculationResults() throws SQLException {
    if (calculation != null) {
      CalculationRepository.removeCalculation(getConnection(), calculation.getCalculationId());
      calculation = null;
    }
  }

  protected void newCalculation() throws SQLException {
    newCalculation(null);
  }

  protected void newCalculation(final Integer calculationPointSetId) throws SQLException {
    calculation = null;
    createCalculation(calculationPointSetId);
  }

}
