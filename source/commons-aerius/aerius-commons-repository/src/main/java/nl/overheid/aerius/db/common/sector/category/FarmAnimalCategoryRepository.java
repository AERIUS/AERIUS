/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalCategory;

/**
 * Repository class for Farm Animal Categories.
 */
public class FarmAnimalCategoryRepository {

  static final Query FARM_ANIMAL_CATEGORIES_DEPRECATED_QUERY = QueryBuilder
      .from("farm_animal_categories_deprecated")
      .select(QueryAttribute.FARM_ANIMAL_CATEGORY_ID, QueryAttribute.CODE, QueryAttribute.NAME, QueryAttribute.DESCRIPTION)
      .getQuery();

  static final Query FARM_ANIMAL_CATEGORIES_QUERY = QueryBuilder
      .from("farm_animal_categories")
      .select(QueryAttribute.FARM_ANIMAL_CATEGORY_ID, QueryAttribute.CODE, QueryAttribute.NAME, QueryAttribute.FARM_ANIMAL_TYPE)
      .getQuery();

  public static Map<Integer, FarmAnimalCategory> getFarmAnimalCategoriesDeprecated(final Connection connection,
      final DBMessagesKey messagesKey) throws SQLException {
    final FarmAnimalCategoryCollector farmAnimalCategoryCollector =
        collectCategories(connection, messagesKey, FARM_ANIMAL_CATEGORIES_DEPRECATED_QUERY, false);
    return farmAnimalCategoryCollector.getEntityMap();
  }

  public static Map<Integer, FarmAnimalCategory> getFarmAnimalCategories(final Connection connection,
      final DBMessagesKey messagesKey) throws SQLException {
    final FarmAnimalCategoryCollector farmAnimalCategoryCollector = collectCategories(connection, messagesKey, FARM_ANIMAL_CATEGORIES_QUERY, true);
    return farmAnimalCategoryCollector.getEntityMap();
  }

  private static FarmAnimalCategoryCollector collectCategories(final Connection connection,
      final DBMessagesKey messagesKey, final Query query, final boolean hasAnimalTypeField) throws SQLException {
    final FarmAnimalCategoryCollector farmAnimalCategoryCollector = new FarmAnimalCategoryCollector(messagesKey, hasAnimalTypeField);
    farmAnimalCategoryCollector.collect(connection, query);
    return farmAnimalCategoryCollector;
  }

  static class FarmAnimalCategoryCollector extends AbstractCategoryCollector<FarmAnimalCategory> {

    private final boolean hasAnimalTypeField;

    protected FarmAnimalCategoryCollector(final DBMessagesKey messagesKey, final boolean includeAnimalType) {
      super(QueryAttribute.FARM_ANIMAL_CATEGORY_ID, messagesKey);
      this.hasAnimalTypeField = includeAnimalType;
    }

    @Override
    FarmAnimalCategory getNewCategory() throws SQLException {
      return new FarmAnimalCategory();
    }

    @Override
    void setDescription(final FarmAnimalCategory category, final ResultSet rs) throws SQLException {
      if (hasAnimalTypeField) {
        DBMessages.setFarmAnimalCategoryMessages(category, messagesKey);
      } else {
        category.setDescription(QueryAttribute.DESCRIPTION.getString(rs));
      }
    }

    @Override
    void setRemainingInfo(final FarmAnimalCategory category, final ResultSet rs) throws SQLException {
      if (hasAnimalTypeField) {
        category.setAnimalType(QueryUtil.getEnum(rs, QueryAttribute.FARM_ANIMAL_TYPE, AnimalType.class));
      } else {
        // Fallback for deprecated version which depends on code convention, can be removed if deprecated is no longer used.
        category.setAnimalType(AnimalType.getByCode(category.getCode()));
      }
    }

  }
}
