/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.own2000;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * DB Class used for AeriusPoint specific queries with the database.
 */
public final class ReceptorRepository {

  private static final Query AERIUSPOINTS_PERMIT_REQUIRED = QueryBuilder.from("permit_required_receptors_view")
      .join(new JoinClause("receptors_to_assessment_areas", QueryAttribute.RECEPTOR_ID)).getQuery();

  private ReceptorRepository() {
    // Don't allow to instantiate.
  }

  /**
   * Queries the permit required receptor points.
   * Average roughness and dominant land use per receptor id are returned as well.
   *
   * @param con Database connection
   * @param ru receptor util
   * @return list of permit required receptors, including avg roughness and dominant land use
   * @throws SQLException exception if query fails
   */
  public static Map<Integer, List<AeriusPoint>> getPermitRequiredPoints(final Connection con, final ReceptorUtil ru) throws SQLException {
    final Map<Integer, List<AeriusPoint>> pointMap = new HashMap<>();
    try (final PreparedStatement stmt = con.prepareStatement(AERIUSPOINTS_PERMIT_REQUIRED.get())) {
      final ResultSet result = stmt.executeQuery();

      while (result.next()) {
        final int assessId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(result);
        final List<AeriusPoint> points = pointMap.computeIfAbsent(assessId, aid -> new ArrayList<>());
        final OPSReceptor point = new OPSReceptor(QueryAttribute.RECEPTOR_ID.getInt(result));
        point.setCoordinates(ru.getPointFromReceptorId(point.getId()).getCoordinates());
        points.add(point);
      }
      return pointMap;
    }
  }

}
