/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * Utility class to create specific {@link CalculationPointFeature} objects.
 */
final class CalculationRepositoryUtil {

  private CalculationRepositoryUtil() {
    // Util class
  }

  static CalculationPointFeature toCustomFeature(final int calculationPointId, final double x, final double y, final String label) {
    final CustomCalculationPoint customPoint = new CustomCalculationPoint();

    customPoint.setCustomPointId(calculationPointId);
    customPoint.setLabel(label);
    return createPointFeature(calculationPointId, new Point(x, y), customPoint);
  }

  static CalculationPointFeature toReceptorFeature(final ReceptorUtil receptorUtil, final int receptorId) {
    final ReceptorPoint receptor = new ReceptorPoint();

    receptor.setReceptorId(receptorId);
    return createPointFeature(receptorId, receptorUtil.getPointFromReceptorId(receptorId), receptor);
  }

  static CalculationPointFeature toSubPointFeature(final int subPointId, final int receptorId, final int level, final double x, final double y) {
    final SubPoint subPoint = new SubPoint();

    subPoint.setSubPointId(subPointId);
    subPoint.setReceptorId(receptorId);
    subPoint.setLevel(level);
    return createPointFeature(subPointId, new Point(x, y), subPoint);
  }

  private static CalculationPointFeature createPointFeature(final int id, final Point point, final CalculationPoint properties) {
    final CalculationPointFeature feature = new CalculationPointFeature();

    feature.setId(String.valueOf(id));
    feature.setGeometry(point);
    feature.setProperties(properties);
    return feature;
  }
}
