/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.configuration;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.aerius.geo.shared.LayerProps;
import nl.aerius.search.domain.SearchCapability;
import nl.aerius.search.domain.SearchRegion;
import nl.overheid.aerius.db.common.ColorItemsRepository;
import nl.overheid.aerius.db.common.ColorRangesRepository;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.i18n.LocalizationRepository.LocalizationObject;
import nl.overheid.aerius.db.i18n.LocalizationRepository.LocalizationProperty;
import nl.overheid.aerius.db.layer.LayerRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.MainTab;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.source.road.RoadLayerStyle;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.building.BuildingLimits;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Abstract base class for customer-specific configuration repositories.
 * Override methods to return another value as the default provided in this class.
 */
public abstract class AbstractApplicationConfigurationRepository {
  private static final List<ResultView> RESULT_VIEWS = Arrays.asList(ResultView.DEPOSITION_DISTRIBUTION, ResultView.MARKERS,
      ResultView.HABITAT_TYPES);
  private static final List<CalculationMethod> CALCULATION_TYPES = Arrays.asList(CalculationMethod.FORMAL_ASSESSMENT,
      CalculationMethod.CUSTOM_POINTS);
  private static final List<AssessmentCategory> ASSESSMENT_CATEGORIES = Arrays.asList();
  private static final List<Substance> EMISSION_SUBSTANCES_ROAD = Arrays.asList(Substance.NOX, Substance.NO2, Substance.NH3);
  private static final List<Substance> SUBSTANCES = Arrays.asList(Substance.NOX, Substance.NH3);
  // Order of list is used in frontend when displaying lists of situation types or dropdowns per situation type.
  private static final List<SituationType> AVAILABLE_SITUATION_TYPES = Arrays.asList(SituationType.PROPOSED, SituationType.REFERENCE,
      SituationType.TEMPORARY, SituationType.OFF_SITE_REDUCTION);
  private static final List<SectorGroup> ALLOWED_SECTORGROUPS = Arrays.asList(SectorGroup.ENERGY, SectorGroup.AGRICULTURE,
      SectorGroup.ROAD_TRANSPORTATION, SectorGroup.INDUSTRY, SectorGroup.LIVE_AND_WORK, SectorGroup.RAIL_TRANSPORTATION,
      SectorGroup.AVIATION, SectorGroup.SHIPPING, SectorGroup.MOBILE_EQUIPMENT, SectorGroup.OTHER);
  private static final List<RoadLayerStyle> ROAD_LAYER_STYLES = Arrays.asList(RoadLayerStyle.ROAD_TYPE, RoadLayerStyle.MAX_SPEED,
      RoadLayerStyle.TRAFFIC_VOLUME, RoadLayerStyle.STAGNATION, RoadLayerStyle.ROAD_BARRIER, RoadLayerStyle.ROAD_BARRIER_TYPE,
      RoadLayerStyle.ROAD_BARRIER_HEIGHT, RoadLayerStyle.ELEVATION_TYPE, RoadLayerStyle.ELEVATION_HEIGHT, RoadLayerStyle.TUNNEL_FACTOR);
  private static final int MAX_NUMBER_OF_SITUATIONS = 6;
  private static final double ASSESSMENT_POINT_HEIGHT_DEFAULT = 1.5;

  private static final List<String> ACCEPTABLE_IMPORT_FILE_TYPES = Stream.of(ImportType.values())
      .map(v -> "." + v.getExtension())
      .collect(Collectors.toCollection(() -> new ArrayList<>()));
  private static final List<MainTab> ALL_MAIN_TABS = Arrays.asList(MainTab.CALCULATED_INFO, MainTab.SUMMARY, MainTab.SCENARIO_RESULTS);

  private static final String MANUAL_PRODUCT = "calculator";

  /**
   * Fills the application configuration with customer-specific settings.
   *
   * @param con database connection
   * @param messagesKey message key
   * @return filled configuration
   * @throws SQLException when database error occurs
   * @throws AeriusException when error occurs
   */
  public final void fill(final Connection con, final ApplicationConfiguration appConfig, final DBMessagesKey messagesKey)
      throws SQLException, AeriusException {
    appConfig.setTheme(getTheme());

    appConfig.setCharacteristicsType(getCharacteristicsType(con));
    appConfig.setCalculationYears(getCalculationYears(con));
    appConfig.setCalculationYearDefault(getCalculationYearDefault());
    appConfig.setAvailableSituationTypes(getAvailableSituationTypes());
    appConfig.setDefaultSituationType(getDefaultSituationType());
    appConfig.setEmissionResultKeys(getEmissionResultKeys());
    appConfig.setSubstances(getSubstances());
    appConfig.setEmissionSubstancesRoad(getEmissionSubstancesRoad());
    appConfig.setImportSubstances(getImportSubstances());
    appConfig.setRoadLayerStyle(getRoadLayerStyles());
    appConfig.setRoadEmissionDisplayConversionFactor(getRoadEmissionDisplayConversionFactor());
    appConfig.setCalculationJobTypes(getCalculationJobTypes());
    appConfig.setDefaultExportJobTypes(getDefaultExportJobTypes());
    appConfig.setAvailableExportReportOptions(getAvailableExportReportOptions());
    appConfig.setCalculationMethods(getCalculationMethods());
    appConfig.setAssessmentCategories(getAssessmentCategories());
    appConfig.setDefaultCalculationMethod(getDefaultCalculationMethod());
    appConfig.setResultViews(getResultViews());
    appConfig.setSectorGroups(getSectorGroups());
    appConfig.setEmissionResultValueDisplaySettings(getEmissionResultValueDisplaySettings(con));
    appConfig.setMaxNumberOfSituations(getMaxNumberOfSituations());
    appConfig.setAssessmentPointHeightDefault(getAssessmentPointHeightDefault());
    appConfig.setMetSites(getMetSites(con));
    appConfig.setCalculationDistanceDefault(getCalculationDistanceDefault());
    appConfig.setSearchRegion(getSearchRegion());
    appConfig.setSearchCapabilities(getSearchCapabilities());
    appConfig.setConvertibleGeometrySystems(getConvertibleGeometrySystems());
    appConfig.setSectorIcons(getSectorIcons());
    addProjections(appConfig);
    appConfig.setBuildingLimits(getBuildingLimits());
    appConfig.setEmisionSourceUIViewType(SectorRepository.getEmissionCalculationMethods(con));
    appConfig.setSectorPropertiesSet(SectorRepository.getSectorProperties(con));
    appConfig.setSupportedSummaryResultTypes(getSupportedSummaryResultTypes());
    appConfig.setSupportedSummaryStatisticTypes(getSupportedSummaryStatisticTypes());
    appConfig.setSupportedSummaryHexagonTypes(getSupportedSummaryHexagonTypes());
    appConfig.setInputTypeViewModes(getInputTypeViewModes());
    appConfig.setCustomFarmLodgingEmissionFactorTypes(getCustomFarmLodgingEmissionFactorTypes());
    appConfig.setCustomFarmAnimalHousingEmissionFactorTypes(getCustomFarmAnimalHousingEmissionFactorTypes());
    appConfig.setCustomManureStorageEmissionFactorTypes(getCustomManureStorageEmissionFactorTypes());
    appConfig.setAppFlags(getAppFlags());
    appConfig.setRoadTrafficVolumeColors(getRoadTrafficVolumeColors());
    appConfig.setManualBaseUrl(getManualBaseUrl(con));
    appConfig.setRegisterBaseUrl(getRegisterBaseUrl(con));
    // Add layers
    setLayers(con, appConfig, messagesKey);
    addSettings(con, appConfig);

    // Add color related information
    appConfig.setColorRanges(ColorRangesRepository.getColorRanges(con));
    appConfig.setColorItems(ColorItemsRepository.getColorItems(con));
    appConfig.setResultTypeColorRangeTypes(ColorRangesRepository.getResultTypeColorRangeTypes(con));

    appConfig.setAcceptedImportFileTypes(getAcceptableImportFileTypes());
    // @reviewers, this should be false before merging
    appConfig.setDisplayLoginSettings(false);
    appConfig.setAvailableTabs(getAvailableTabs());
  }

  protected abstract Map<Integer, SectorIcon> getSectorIcons();

  private void setLayers(final Connection con, final ApplicationConfiguration appConfig, final DBMessagesKey messagesKey)
      throws SQLException, AeriusException {
    final List<LayerProps> baseLayers = LayerRepository.getBaseLayers(con, appConfig.getTheme(), messagesKey);

    for (final LayerProps layer : baseLayers) {
      layer.setTitle(DBMessages.getLocalizedString(messagesKey, LocalizationObject.LAYERS, LocalizationProperty.DESCRIPTION, layer.getId()));
    }

    appConfig.setBaseLayers(baseLayers);

    LayerRepository.getLayers(con, appConfig.getTheme(), messagesKey).forEach(layer -> {
      layer.setTitle(DBMessages.getLocalizedString(messagesKey, LocalizationObject.LAYERS, LocalizationProperty.DESCRIPTION, layer.getId()));
      appConfig.setLayer(layer.getName(), layer);
    });
  }

  protected Set<AppFlag> getAppFlags() {
    return new HashSet<>();
  }

  protected double getCalculationDistanceDefault() {
    return 0D;
  }

  protected List<MetSite> getMetSites(final Connection con) throws SQLException {
    return new ArrayList<>();
  }

  protected List<String> getConvertibleGeometrySystems() {
    return Arrays.asList();
  }

  /**
   * Returns the Theme enum value for this configuration.
   *
   * @return Theme enum value
   */
  protected abstract Theme getTheme();

  protected abstract SearchRegion getSearchRegion();

  protected abstract List<SearchCapability> getSearchCapabilities();

  protected List<FarmEmissionFactorType> getCustomFarmAnimalHousingEmissionFactorTypes() {
    return Arrays.asList(FarmEmissionFactorType.PER_ANIMAL_PER_YEAR);
  }

  @Deprecated
  protected List<FarmEmissionFactorType> getCustomFarmLodgingEmissionFactorTypes() {
    return Arrays.asList(FarmEmissionFactorType.PER_ANIMAL_PER_YEAR);
  }

  protected List<FarmEmissionFactorType> getCustomManureStorageEmissionFactorTypes() {
    return Arrays.asList(FarmEmissionFactorType.PER_TONNES_PER_YEAR,
        FarmEmissionFactorType.PER_METERS_SQUARED_PER_YEAR, FarmEmissionFactorType.PER_METERS_SQUARED_PER_DAY,
        FarmEmissionFactorType.PER_YEAR);
  }

  protected CharacteristicsType getCharacteristicsType(final Connection con) throws SQLException {
    return ConstantRepository.getEnum(con, ConstantsEnum.CHARACTERISTICS_TYPE, CharacteristicsType.class);
  }

  /**
   * Implementations should add all supported projections to the configuration.
   *
   * @param config application configuration to add projections to
   */
  protected abstract void addProjections(final ApplicationConfiguration config);

  /**
   * Implementation can call this util method in case a single projection is supported.
   *
   * @param config application configuration to add projection to
   * @param projectionKey projection key
   * @param projectionDefinition projection definition
   */
  protected void addSingleProjection(final ApplicationConfiguration config, final String projectionKey,
      final String projectionDefinition) {
    final HashMap<String, String> projections = new HashMap<>();
    projections.put(projectionKey, projectionDefinition);
    config.setProjections(projections);
  }

  protected List<Integer> getCalculationYears(final Connection con) throws SQLException {
    final Integer minYear = ConstantRepository.getNumber(con, ConstantsEnum.MIN_YEAR, Integer.class);
    final Integer maxYear = ConstantRepository.getNumber(con, ConstantsEnum.MAX_YEAR, Integer.class);
    final List<Integer> years = new ArrayList<>();
    for (int year = minYear; year <= maxYear; year++) {
      years.add(year);
    }
    return years;
  }

  protected int getCalculationYearDefault() {
    return Year.now().getValue();
  }

  protected List<SituationType> getAvailableSituationTypes() {
    return AVAILABLE_SITUATION_TYPES;
  }

  protected SituationType getDefaultSituationType() {
    return SituationType.PROPOSED;
  }

  protected abstract List<EmissionResultKey> getEmissionResultKeys();

  protected List<Substance> getSubstances() {
    return SUBSTANCES;
  }

  protected List<Substance> getEmissionSubstancesRoad() {
    return EMISSION_SUBSTANCES_ROAD;
  }

  protected List<Substance> getImportSubstances() {
    return SUBSTANCES;
  }

  protected List<RoadLayerStyle> getRoadLayerStyles() {
    return ROAD_LAYER_STYLES;
  }

  /**
   * @return Factor to convert road emissions to display value. Default converts to value per meter: kg/year -> kg/m/year.
   */
  protected double getRoadEmissionDisplayConversionFactor() {
    return 1.0;
  }

  abstract List<CalculationJobType> getCalculationJobTypes();

  protected Map<CalculationJobType, JobType> getDefaultExportJobTypes() {
    return new HashMap<>();
  }

  abstract List<CalculationJobType> getAvailableExportReportOptions();

  protected List<CalculationMethod> getCalculationMethods() {
    return CALCULATION_TYPES;
  }

  protected CalculationMethod getDefaultCalculationMethod() {
    return CalculationMethod.FORMAL_ASSESSMENT;
  }

  protected List<AssessmentCategory> getAssessmentCategories() {
    return ASSESSMENT_CATEGORIES;
  }

  protected List<ResultView> getResultViews() {
    return RESULT_VIEWS;
  }

  protected List<SectorGroup> getSectorGroups() {
    return ALLOWED_SECTORGROUPS;
  }

  protected EmissionResultValueDisplaySettings getEmissionResultValueDisplaySettings(final Connection con) throws SQLException {
    final double conversionFactor = ConstantRepository.getDouble(con, ConstantsEnum.EMISSION_RESULT_DISPLAY_CONVERSION_FACTOR);
    final DepositionValueDisplayType conversionUnit = DepositionValueDisplayType
        .valueOf(ConstantRepository.getString(con, ConstantsEnum.EMISSION_RESULT_DISPLAY_UNIT));
    final int depositionValueRoundingLength = ConstantRepository.getInteger(con, ConstantsEnum.EMISSION_RESULT_DISPLAY_ROUNDING_LENGTH);
    final int depositionValuePreciseRoundingLength = ConstantRepository.getInteger(con,
        ConstantsEnum.EMISSION_RESULT_DISPLAY_PRECISE_ROUNDING_LENGTH);
    final int depositionValueSumRoundingLength = ConstantRepository.getInteger(con,
        ConstantsEnum.EMISSION_RESULT_DISPLAY_DEPOSITION_SUM_ROUNDING_LENGTH);

    return new EmissionResultValueDisplaySettings(conversionUnit, conversionFactor, depositionValueRoundingLength,
        depositionValuePreciseRoundingLength, depositionValueSumRoundingLength);
  }

  protected Map<SituationType, Integer> getMaxNumberOfSituationsPerType() {
    final Map<SituationType, Integer> maxNumberOfSituationsPerType = new HashMap<>();
    maxNumberOfSituationsPerType.put(SituationType.REFERENCE, 1);
    maxNumberOfSituationsPerType.put(SituationType.OFF_SITE_REDUCTION, 1);

    return maxNumberOfSituationsPerType;
  }

  protected int getMaxNumberOfSituations() {
    return MAX_NUMBER_OF_SITUATIONS;
  }

  protected double getAssessmentPointHeightDefault() {
    return ASSESSMENT_POINT_HEIGHT_DEFAULT;
  }

  protected abstract BuildingLimits getBuildingLimits();

  protected List<EmissionResultKey> getSupportedSummaryResultTypes() {
    return Arrays.asList(EmissionResultKey.NOXNH3_DEPOSITION);
  }

  protected List<ResultStatisticType> getSupportedSummaryStatisticTypes() {
    return Arrays.asList(ResultStatisticType.MAX_INCREASE, ResultStatisticType.MAX_TOTAL);
  }

  protected List<SummaryHexagonType> getSupportedSummaryHexagonTypes() {
    return Arrays.asList(SummaryHexagonType.ABOVE_CL_HEXAGONS, SummaryHexagonType.CUSTOM_CALCULATION_POINTS, SummaryHexagonType.EXCEEDING_HEXAGONS,
        SummaryHexagonType.RELEVANT_HEXAGONS, SummaryHexagonType.EXTRA_ASSESSMENT_HEXAGONS);
  }

  protected Set<InputTypeViewMode> getInputTypeViewModes() {
    return Stream.of(InputTypeViewMode.EMISSION_SOURCES, InputTypeViewMode.BUILDING).collect(Collectors.toSet());
  }

  protected List<String> getAcceptableImportFileTypes() {
    return ACCEPTABLE_IMPORT_FILE_TYPES;
  }

  protected List<MainTab> getAvailableTabs() {
    return ALL_MAIN_TABS;
  }

  abstract Map<String, ColorRangeType> getRoadTrafficVolumeColors();

  private String getManualBaseUrl(final Connection con) throws SQLException {
    final String host = SettingsUtil.getOptionalSettingPreferEnvironment(con, getManualHost());
    if (host == null) {
      return null;
    }
    return getManualBasePath(con).getBaseUrl(host);
  }

  @SuppressWarnings("rawtypes")
  protected Enum getManualHost() {
    return SharedConstantsEnum.MANUAL_BASE_URL;
  }

  protected ManualBasePath getManualBasePath(final Connection con) throws SQLException {
    final String version = SettingsUtil.getSettingPreferEnvironment(con, SharedConstantsEnum.MANUAL_VERSION);
    final String manualLanguage = SettingsUtil.getOptionalSettingPreferEnvironment(con, SharedConstantsEnum.MANUAL_LANGUAGE);

    final String language = manualLanguage == null
        ? ConstantRepository.getString(con, ConstantsEnum.DEFAULT_LOCALE)
        : manualLanguage;

    return new ManualBasePath(MANUAL_PRODUCT, version, language);
  }

  private String getRegisterBaseUrl(final Connection con) throws SQLException {
    return SettingsUtil.getOptionalSettingPreferEnvironment(con, SharedConstantsEnum.REGISTER_BASE_URL);
  }

  protected void addSettings(final Connection con, final ApplicationConfiguration config) throws SQLException {
    // By default, don't add any settings.
    // If you'd want to add something here, ask yourself why you wouldn't add it to ApplicationConfiguration instead.
  }
}
