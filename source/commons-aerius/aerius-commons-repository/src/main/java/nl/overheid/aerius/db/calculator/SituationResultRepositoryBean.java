/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

public class SituationResultRepositoryBean {

  private static final Logger LOG = LoggerFactory.getLogger(SituationResultRepositoryBean.class);

  private final PMF pmf;

  public SituationResultRepositoryBean(final PMF pmf) {
    this.pmf = pmf;
  }

  public boolean determinePermitDemandCheck(String jobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return SituationResultRepository.determineSituationResultDevelopmentSpaceDemand(con, jobKey);
    } catch (final SQLException e) {
      LOG.error("SQL error while determinint permit demand check", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }


}
