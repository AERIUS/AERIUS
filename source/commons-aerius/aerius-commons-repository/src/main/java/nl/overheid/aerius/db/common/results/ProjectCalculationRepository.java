/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.ReceptorResult;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

public class ProjectCalculationRepository {

  public enum CalculationResultsType {
    PROJECT, SITUATION;
  }

  private static final Logger LOGGER = LoggerFactory.getLogger(ProjectCalculationRepository.class);

  private static final Query QUERY_PROJECT_CALCULATOR_RESULT = QueryBuilder
      .from("jobs.ae_scenario_project_calculation_deposition_per_substance(?, ?, ?::hexagon_type, ?::overlapping_hexagon_type)",
          QueryAttribute.CALCULATION_ID, QueryAttribute.SUBSTANCE_ID,
          QueryAttribute.HEXAGON_TYPE, QueryAttribute.OVERLAPPING_HEXAGON_TYPE)
      .select(QueryAttribute.RECEPTOR_ID, QueryAttribute.RESULT)
      .getQuery();

  private static final Query QUERY_SITUATION_CALCULATOR_RESULT = QueryBuilder
      .from("jobs.ae_scenario_situation_calculation_deposition_per_substance(?, ?, ?::hexagon_type, ?::overlapping_hexagon_type)",
          QueryAttribute.CALCULATION_ID, QueryAttribute.SUBSTANCE_ID,
          QueryAttribute.HEXAGON_TYPE, QueryAttribute.OVERLAPPING_HEXAGON_TYPE)
      .select(QueryAttribute.RECEPTOR_ID, QueryAttribute.RESULT)
      .getQuery();

  private final PMF pmf;

  public ProjectCalculationRepository(final PMF pmf) {
    this.pmf = pmf;
  }

  public List<ReceptorResult> getCalculationResults(final CalculationResultsType calculationResultsType, final int calculationId,
      final Substance substance, final SummaryHexagonType hexagonType, final OverlappingHexagonType overlappingHexagonType) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return getCalculationResults(con, calculationResultsType, calculationId, substance.getId(), hexagonType, overlappingHexagonType);
    } catch (final SQLException e) {
      LOGGER.error("SQL error while retrieving project calculation results", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private static List<ReceptorResult> getCalculationResults(final Connection connection, final CalculationResultsType calculationResultsType,
      final int calculationId, final int substanceId, final SummaryHexagonType hexagonType, final OverlappingHexagonType overlappingHexagonType)
          throws SQLException {
    final boolean project = calculationResultsType == CalculationResultsType.PROJECT;
    final Query query = project ? QUERY_PROJECT_CALCULATOR_RESULT : QUERY_SITUATION_CALCULATOR_RESULT;

    try (final PreparedStatement stmt = connection.prepareStatement(query.get())) {
      QUERY_PROJECT_CALCULATOR_RESULT.setParameter(stmt, QueryAttribute.CALCULATION_ID, calculationId);
      QUERY_PROJECT_CALCULATOR_RESULT.setParameter(stmt, QueryAttribute.SUBSTANCE_ID, substanceId);
      QUERY_PROJECT_CALCULATOR_RESULT.setParameter(stmt, QueryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase(Locale.ROOT));
      QUERY_PROJECT_CALCULATOR_RESULT.setParameter(stmt, QueryAttribute.OVERLAPPING_HEXAGON_TYPE,
          overlappingHexagonType.name().toLowerCase(Locale.ROOT));
      final ResultSet rs = stmt.executeQuery();
      final List<ReceptorResult> results = new ArrayList<>();

      while (rs.next()) {
        final ReceptorResult result = new ReceptorResult(
            QueryUtil.getInt(rs, QueryAttribute.RECEPTOR_ID),
            QueryUtil.getDouble(rs, QueryAttribute.RESULT));
        results.add(result);
      }
      return results;
    }
  }

}
