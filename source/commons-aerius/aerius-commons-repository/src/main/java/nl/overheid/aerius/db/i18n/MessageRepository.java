/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.i18n;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * DB Class to obtain localized messages from the database.
 */
public final class MessageRepository {

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(MessageRepository.class);

  // The SQL queries used in this service.
  private static final String GET_MESSAGE_QUERY =
      "SELECT message FROM i18n.messages WHERE key = ? AND language_code = ?::i18n.language_code_type";

  // Not allowed to instantiate.
  private MessageRepository() {
  }

  /**
   * Get a constant value from the database. The connection is NOT closed after usage.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * @param connection The connection to used (won't be closed).
   * @param messageEnum The constant to get the value for.
   * @return The value of the constant.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static String getString(final Connection connection, final MessagesEnum messageEnum, final String language) throws SQLException {
    final String key = messageEnum.name();
    String messageValue = null;
    try (final PreparedStatement stmt = connection.prepareStatement(GET_MESSAGE_QUERY)) {
      QueryUtil.setValues(stmt, key, language);
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        messageValue = rs.getString(1);
      } else {
        throw new IllegalArgumentException("The message " + key + " (" + language + ") does not exist in table i18n.messages");
      }
    } catch (final SQLException e) {
      LOG.error("Error fetching message {} ({})", key, language, e);
      throw e;
    }
    return messageValue;
  }

  /**
   * Get a constant value from the database.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * @param pmf The Persistence Manager Factory to used to obtain a connection.
   * @param messageEnum The constant to get the value for.
   * @return The value of the constant.
   * <p>Will throw a runtime exception when a SQL exception occurred.
   */
  public static String getString(final PMF pmf, final MessagesEnum messageEnum, final String language) {
    String message = null;
    try (final Connection connection = pmf.getConnection()) {
      message = getString(connection, messageEnum, language);
    } catch (final SQLException e) {
      LOG.error("Error fetching message {} ({})", messageEnum.name(), language, e);
      throw new IllegalArgumentException(new AeriusException(AeriusExceptionReason.SQL_ERROR));
    }
    return message;
  }

  public static String getString(final Connection connection, final MessagesEnum messageEnum, final Locale locale) throws SQLException {
    return getString(connection, messageEnum, locale.getLanguage());
  }

  public static String getString(final PMF pmf, final MessagesEnum messageEnum, final Locale locale) {
    return getString(pmf, messageEnum, locale.getLanguage());
  }

}
