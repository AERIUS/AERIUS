/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.procurement;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.db.util.WhereClause;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticsMarker;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Repository to help with retrieving procurement related information.
 */
public class ProcurementRepository {

  private static final Logger LOG = LoggerFactory.getLogger(ProcurementRepository.class);

  private enum ThresholdType {
    SUM_DEPOSITION_NATIONAL,
    SUM_DEPOSITION_ASSESSMENT_AREA
  }

  private enum RepositoryAttribute implements Attribute {
    PROCUREMENT_POLICY,
    THRESHOLD_TYPE,
    THRESHOLD;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ROOT);
    }
  }

  protected static final WhereClause WHERE_PROCUREMENT_POLICY =
      new StaticWhereClause(RepositoryAttribute.PROCUREMENT_POLICY.name() + " = ?::procurement_policy_type",
          RepositoryAttribute.PROCUREMENT_POLICY);

  private static final Query PROCUREMENT_THRESHOLDS_BY_POLICY = QueryBuilder
      .from("procurement_thresholds")
      .select(RepositoryAttribute.THRESHOLD_TYPE, QueryAttribute.KEY, RepositoryAttribute.THRESHOLD)
      .where(WHERE_PROCUREMENT_POLICY)
      .getQuery();

  private static final BigDecimal PERCENTAGE_FACTOR = BigDecimal.valueOf(100);
  private final PMF pmf;
  private final AreaInformationSupplier areaInformationSupplier;

  public ProcurementRepository(final PMF pmf, final AreaInformationSupplier areaInformationSupplier) {
    this.pmf = pmf;
    this.areaInformationSupplier = areaInformationSupplier;
  }

  /**
   * Add procurement specific statistics to supplied summary, based on the supplied policy.
   */
  public void addProcurementStatistics(final SituationResultsSummary resultsSummary, final ProcurementPolicy policy) throws AeriusException {
    final Map<Integer, SituationResultsStatistics> statisticsByArea = resultsSummary.getAreaStatistics().stream()
        .collect(Collectors.toMap(s -> s.getAssessmentArea().getId(), s -> s.getStatistics()));
    try (final Connection con = pmf.getConnection();
        final PreparedStatement statement = con.prepareStatement(PROCUREMENT_THRESHOLDS_BY_POLICY.get())) {

      PROCUREMENT_THRESHOLDS_BY_POLICY.setParameter(statement, RepositoryAttribute.PROCUREMENT_POLICY, policy.toDatabaseString());
      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        handleResultSet(rs, resultsSummary, statisticsByArea);
      }
    } catch (final SQLException e) {
      LOG.error("SQL error while retrieving procurement thresholds", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private void handleResultSet(final ResultSet rs, final SituationResultsSummary resultsSummary,
      final Map<Integer, SituationResultsStatistics> statisticsByArea)
      throws SQLException, AeriusException {
    final ThresholdType thresholdType = QueryUtil.getEnum(rs, RepositoryAttribute.THRESHOLD_TYPE, ThresholdType.class);
    final double threshold = QueryUtil.getDouble(rs, RepositoryAttribute.THRESHOLD);
    if (thresholdType == ThresholdType.SUM_DEPOSITION_NATIONAL) {
      addStatistics(resultsSummary.getStatistics(), threshold);
    } else if (thresholdType == ThresholdType.SUM_DEPOSITION_ASSESSMENT_AREA) {
      final int key = QueryAttribute.KEY.getInt(rs);
      if (statisticsByArea.containsKey(key)) {
        addStatistics(statisticsByArea.get(key), threshold);
        resultsSummary.getMarkers().add(createAreaMarker(statisticsByArea.get(key), threshold, areaInformationSupplier.determineAssessmentArea(key)));
      }
    } else {
      LOG.error("Unimplemented threshold type {}", thresholdType);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private static void addStatistics(final SituationResultsStatistics statistics, final double threshold) {
    statistics.put(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_VALUE, threshold);
    final BigDecimal depositionSum = BigDecimal.valueOf(statistics.getOrDefault(ResultStatisticType.SUM_CONTRIBUTION, 0.0));
    final BigDecimal thresholdBD = BigDecimal.valueOf(threshold);
    statistics.put(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE,
        depositionSum.divide(thresholdBD, MathContext.DECIMAL64).multiply(PERCENTAGE_FACTOR).doubleValue());
  }

  private static ResultStatisticsMarker createAreaMarker(final SituationResultsStatistics statistics, final double threshold,
      final AssessmentArea area) {
    final BigDecimal depositionSum = BigDecimal.valueOf(statistics.getOrDefault(ResultStatisticType.SUM_CONTRIBUTION, 0.0));
    final BigDecimal thresholdBD = BigDecimal.valueOf(threshold);
    ResultStatisticType type;
    if (depositionSum.compareTo(thresholdBD) >= 0) { // >=
      type = ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PASS;
    } else {
      type = ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_FAIL;
    }
    final ResultStatisticsMarker marker = new ResultStatisticsMarker(0, area.getMarkerLocation());
    marker.getStatisticsTypes().add(type);
    return marker;
  }

}
