/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db;

import java.sql.SQLException;

import org.slf4j.Logger;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Database error code states.
 *
 * Normally when a database function throws an exception, it bubbles back to the user and becomes a
 * generic AeriusException message. But sometimes we want to communicate a more specific error
 * message. A db function can then throw an exception using an ERRCODE, which are defined in this
 * enum along with a mapping to the AeriusException Reason.
 *
 * The ERRCODE can be accessed using SQLException.getState(). This way, it is easy to convert a
 * SQLException to an AeriusException with has proper localized message strings.
 */
public enum DatabaseErrorCode {
  OTHER(AeriusExceptionReason.SQL_ERROR);

  private final AeriusExceptionReason exceptionReason;

  private DatabaseErrorCode(final AeriusExceptionReason exceptionReason) {
    this.exceptionReason = exceptionReason;
  }

  public AeriusExceptionReason getExceptionReason() {
    return exceptionReason;
  }

  private static DatabaseErrorCode fromSQLState(final String sqlState) {
    for (final DatabaseErrorCode code : values()) {
      if (code.name().equalsIgnoreCase(sqlState)) {
        return code;
      }
    }
    return DatabaseErrorCode.OTHER;
  }

  /**
   * Converts a {@link SQLException} to an {@link AeriusException}. If the error code was {@link DatabaseErrorCode#OTHER} this means it was an
   * unknown error and it should be logged. If it's a known error (code) it is a user error and there is no reason to log it. The arguments passed in
   * the varargs parameter are passed to the {@link AeriusException} are used to create an error message with context information.
   * @param sqlException the {@link SQLException} to convert
   * @param log logger to log unknown sql errors
   * @param message the message to log when it's an unknown sql error
   * @param arguments arguments for {@link AeriusException}
   * @return the converted {@link AeriusException}
   */
  public static AeriusException createAeriusException(final SQLException sqlException, final Logger log, final String message,
      final String... arguments) {
    final DatabaseErrorCode errorCode = DatabaseErrorCode.fromSQLState(sqlException.getSQLState());
    if (errorCode == DatabaseErrorCode.OTHER) {
      log.error(message, sqlException);
    }
    return new AeriusException(errorCode.getExceptionReason(), arguments);
  }
}
