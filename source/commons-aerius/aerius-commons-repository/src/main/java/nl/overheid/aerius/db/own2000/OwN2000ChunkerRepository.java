/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.own2000;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.geo.SridPoint;

/**
 * Repository class for OwN2000 Chunker related query methods.
 */
public class OwN2000ChunkerRepository {

  private static final String ASSESMENT_AREA_DISTANCES =
      "SELECT assessment_area_id, name, ST_Distance(geometry, ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()))::real as distance"
          + " FROM assessment_areas WHERE type = 'natura2000_area'"
          + " ORDER BY ST_Distance(geometry, ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()))::real";

  private static final String MAX_CALCULATION_RESULT_BY_ASSESSMENT_AREA =
      "SELECT max(deposition) as max, count(deposition) as count FROM "
          + " receptors_to_assessment_areas"
          + " JOIN permit_required_receptors_view USING (receptor_id)"
          + " JOIN jobs.calculation_summed_deposition_results_view USING (receptor_id) "
          + " WHERE assessment_area_id = ? AND calculation_id = ?";

  /**
   * Returns the highest deposition value for the given calculation on a given assessment area.
   *
   * @param con
   * @param assessmentAreaId
   * @param calculationId
   * @return
   * @throws SQLException
   */
  public AResult getCalculationResultsForAssessmentArea(final Connection con, final int assessmentAreaId, final int calculationId)
      throws SQLException {
    final AResult returnResult;
    try (PreparedStatement statement = con.prepareStatement(MAX_CALCULATION_RESULT_BY_ASSESSMENT_AREA)) {
      QueryUtil.setValues(statement, assessmentAreaId, calculationId);
      try (final ResultSet result = statement.executeQuery()) {
        if (result.next()) {
          returnResult = new AResult(result.getDouble("max"), result.getInt("count"));
        } else {
          returnResult = new AResult(0, 0);
        }
      }
    }
    return returnResult;
  }

  /**
   * Returns a map of assessment area ids as key and the distance to the given point in meters to that area.
   *
   * @param con database connection
   * @param point point to measure distance to
   * @return
   * @throws SQLException
   */
  public Map<Integer, Integer> getDistanceSetForPoint(final Connection con, final SridPoint point) throws SQLException {
    final Map<Integer, Integer> set = new HashMap<>();
    try (PreparedStatement statement = con.prepareStatement(ASSESMENT_AREA_DISTANCES)) {
      QueryUtil.setValues(statement, point.getX(), point.getY(), point.getX(), point.getY());
      try (final ResultSet result = statement.executeQuery()) {
        while (result.next()) {
          final int aaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(result);
          final int distance = QueryAttribute.DISTANCE.getInt(result);
          set.put(aaId, distance);
        }
      }
    }
    return set;
  }

  /**
   * Data object to store the highest deposition value and the number of receptors calculated (in a specific natura 2000 area).
   */
  public static class AResult implements Serializable {
    private static final long serialVersionUID = 3139175147458006539L;

    private final double max;
    private final int count;

    public AResult(final double max, final int count) {
      this.max = max;
      this.count = count;
    }

    public int getCount() {
      return count;
    }

    public double getMax() {
      return max;
    }
  }
}
