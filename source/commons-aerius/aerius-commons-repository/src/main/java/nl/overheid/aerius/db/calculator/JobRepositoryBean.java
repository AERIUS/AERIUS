/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

public class JobRepositoryBean {

  private static final Logger LOG = LoggerFactory.getLogger(JobRepositoryBean.class);

  private final PMF pmf;

  public JobRepositoryBean(final PMF pmf) {
    this.pmf = pmf;
  }

  /**
   * Get the job id that belongs to a certain correlation identifier.
   * Returns 0 when no job is found for that identifier.
   */
  public int getJobId(final String correlationId) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return JobRepository.getJobId(con, correlationId);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting job id", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Fetches job progress objects for all jobs of the given user.
   */
  public List<JobProgress> getProgressForUser(final ConnectUser user) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return JobRepository.getProgressForUser(con, user);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting job progress for user", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Fetches job progress objects for all jobs of the given user and given a set of job states.
   */
  public List<JobProgress> getProgressForUserAndJobStates(final ConnectUser user, final Set<JobState> jobStates) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return JobRepository.getProgressForUserAndJobStates(con, user, jobStates);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting job progress for user", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Fetches job progress objects for a single job of the given user.
   */
  public Optional<JobProgress> getProgressForUserAndKey(final ConnectUser user, final String jobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return Optional.ofNullable(JobRepository.getProgressForUserAndKey(con, user, jobKey));
    } catch (final SQLException e) {
      LOG.error("SQL error while getting job progress for user and jobkey", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Fetches job progress objects for a single job
   */
  public Optional<JobProgress> getProgressForKey(final String jobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return Optional.ofNullable(JobRepository.getProgressWithoutDeleted(con, jobKey));
    } catch (final SQLException e) {
      LOG.error("SQL error while getting job progress for jobkey without user", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Fetches situation - calculation information for a specific jobkey. Job key is expected to exist.
   */
  public SituationCalculations getSituationCalculations(final String jobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return JobRepository.getSituationCalculations(con, jobKey);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting situation calculations", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Fetches archive metadata information for a specific jobkey. Returns null if job does not exist or if there was no metadata.
   */
  public ArchiveMetaData getArchiveMetaData(final String jobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final SituationCalculations calculations = JobRepository.getSituationCalculations(con, jobKey);
      final Optional<Integer> calculationId = calculations.getCalculationIdOfSingleSituationType(SituationType.PROPOSED);
      return calculationId.isPresent() ? CalculationRepository.getArchiveMetadata(con, calculationId.get()) : null;
    } catch (final SQLException | IOException e) {
      LOG.error("Error while getting archive metadata", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Returns true if a given user and jobKey combination exist, false if not.
   */
  public boolean isJobFromUser(final ConnectUser user, final String jobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return JobRepository.isJobFromUser(con, user, jobKey);
    } catch (final SQLException e) {
      LOG.error("SQL error while determining if job key and user match", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Returns true if a given jobKey exists and does not belong to a user, false if not.
   */
  public boolean isJobWithoutUser(final String jobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return JobRepository.isJobWithoutUser(con, jobKey);
    } catch (final SQLException e) {
      LOG.error("SQL error while determining if job key and user match", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Set the jobState for the job to cancelled (unless already set to completed or error).
   */
  public void cancelJob(final String jobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      JobRepository.updateJobStatus(con, jobKey, JobState.CANCELLED);
    } catch (final SQLException e) {
      LOG.error("SQL error while cancelling job", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Remove job fully. This includes calculations/progress and such.
   */
  public void deleteJob(final String jobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      JobRepository.updateJobStatus(con, jobKey, JobState.DELETED);
    } catch (final SQLException e) {
      LOG.error("SQL error while cancelling job", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Set a Job to an error state.
   */
  public void setErrorMessage(final String jobKey, final String message) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      JobRepository.setErrorMessage(con, jobKey, message);
    } catch (final SQLException e) {
      LOG.error("SQL error while setting job error message", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Create job for user.
   */
  public String createJob(final ConnectUser user, final JobType type, final Optional<String> name, final boolean protect) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return JobRepository.createJob(con, user, type, name, protect);
    } catch (final SQLException e) {
      LOG.error("SQL error while creating a job", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Create job without user.
   */
  public String createJob(final JobType type) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return JobRepository.createJob(con, type, false);
    } catch (final SQLException e) {
      LOG.error("SQL error while creating a job", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Update the job status.
   */
  public void updateJobStatus(final String jobKey, final JobState state) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      JobRepository.updateJobStatus(con, jobKey, state);
    } catch (final SQLException e) {
      LOG.error("SQL error while creating a job", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Attach the calculation id to the job.
   */
  public void attachCalculations(final String jobKey, final List<Calculation> calcs) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      JobRepository.attachCalculations(con, jobKey, calcs);
    } catch (final SQLException e) {
      LOG.error("SQL error while saving attach calculation job", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * See {@link JobRepository#setNumberOfPoints(Connection, String, int)}
   */
  public void setNumberOfPoints(final Connection con, final String jobKey, final int totalNrOfPoints) throws SQLException {
    JobRepository.setNumberOfPoints(con, jobKey, totalNrOfPoints);
  }

  public void copyJobResults(final String fromJobKey, final String toJobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      JobRepository.copyJobResults(con, fromJobKey, toJobKey);
    } catch (final SQLException e) {
      LOG.error("SQL error while copyJobResults", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }
}
