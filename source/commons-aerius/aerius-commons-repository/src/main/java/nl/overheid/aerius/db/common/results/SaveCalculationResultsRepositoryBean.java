/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.db.calculator.SituationResultRepository;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

public class SaveCalculationResultsRepositoryBean {

  private static final Logger LOG = LoggerFactory.getLogger(SaveCalculationResultsRepositoryBean.class);

  private final PMF pmf;

  public SaveCalculationResultsRepositoryBean(final PMF pmf) {
    this.pmf = pmf;
  }

  public void insertCalculations(final ScenarioCalculations scenarioCalculations) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      CalculationRepository.insertCalculations(con, scenarioCalculations, false, false);
    } catch (final SQLException e) {
      LOG.error("SQL error while inserting calculation", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public void insertResultsSummaries(final int jobId, final SituationCalculations situationCalculations, final CalculationJobType calculationJobType)
      throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final ResultsSummaryRepository resultsSummaryRepository = new ResultsSummaryRepository(pmf);
      resultsSummaryRepository.insertResultsSummaries(jobId, situationCalculations, calculationJobType);
    } catch (final SQLException e) {
      LOG.error("SQL error while saving recieved result summary for area", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public void insertPermitDemand(final int calculationResultSetId, final int jobId, final int areaId,
      final double demandCheck) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      SituationResultRepository.insertSituationResultDevelopmentSpaceDemand(con, calculationResultSetId, areaId, jobId,
          demandCheck);
    } catch (final SQLException e) {
      LOG.error("SQL error while saving project demand check", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public void insertCalculationResults(final int calculationId, final List<AeriusResultPoint> emissionResults) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      CalculationRepository.insertCalculationResults(con, calculationId, emissionResults);
    } catch (final SQLException e) {
      LOG.error("SQL error while saving recieved Calculation Results", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public void updateCalculationState(final ScenarioCalculations scenarioCalculations, final CalculationState state) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      for (final Calculation calculation : scenarioCalculations.getCalculations()) {
        CalculationRepository.updateCalculationState(con, calculation.getCalculationId(), state);
      }
    } catch (final SQLException e) {
      LOG.error("SQL error while updating calculationState", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

}
