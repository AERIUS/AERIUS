/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalHousingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Service class for FarmAnimalHousingCategories.
 */
public final class FarmAnimalHousingCategoryRepository {

  private static final String DESCRIPTION_FORMAT = "%s (%s)";

  private static final Query FARM_ANIMAL_HOUSING_CATEGORIES_QUERY = QueryBuilder
      .from("farm_animal_housing_categories")
      .join(new JoinClause("farm_housing_emission_factors", QueryAttribute.FARM_ANIMAL_HOUSING_CATEGORY_ID))
      .select(QueryAttribute.FARM_ANIMAL_HOUSING_CATEGORY_ID, QueryAttribute.FARM_ANIMAL_CATEGORY_ID, QueryAttribute.FARM_EMISSION_FACTOR_TYPE,
          QueryAttribute.SUBSTANCE_ID, QueryAttribute.EMISSION_FACTOR)
      .select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES)
      .orderBy(QueryAttribute.FARM_ANIMAL_HOUSING_CATEGORY_ID)
      .getQuery();

  private static final Query FARM_ADDITIONAL_HOUSING_SYSTEMS_QUERY = QueryBuilder
      .from("farm_additional_housing_systems")
      .join(new JoinClause("farm_additional_housing_factors", QueryAttribute.FARM_ADDITIONAL_HOUSING_SYSTEM_ID))
      .select(QueryAttribute.FARM_ADDITIONAL_HOUSING_SYSTEM_ID, QueryAttribute.AIR_SCRUBBER,
          QueryAttribute.FARM_ANIMAL_HOUSING_CATEGORY_ID, QueryAttribute.SUBSTANCE_ID, QueryAttribute.REDUCTION_FACTOR)
      .select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES)
      .orderBy(QueryAttribute.FARM_ADDITIONAL_HOUSING_SYSTEM_ID)
      .getQuery();

  private static final Query FARM_ANIMAL_BASIC_HOUSING = QueryBuilder
      .from("farm_animal_basic_housing")
      .select(QueryAttribute.FARM_ANIMAL_HOUSING_CATEGORY_ID, QueryAttribute.BASIC_HOUSING_CATEGORY_ID)
      .getQuery();

  private static final Query FARM_HOUSING_ALLOWED_ADDITIONAL_SYSTEMS_ = QueryBuilder
      .from("farm_housing_categories_additional_systems")
      .select(QueryAttribute.FARM_ANIMAL_HOUSING_CATEGORY_ID, QueryAttribute.FARM_ADDITIONAL_HOUSING_SYSTEM_ID)
      .getQuery();

  private FarmAnimalHousingCategoryRepository() {
  }

  /**
   * Returns all farm animal housing categories, and related categories/information, from the database.
   *
   * @param connection Connection to use
   * @param messagesKey DBMessagesKey to get descriptions from
   * @return FarmAnimalHousingCategories object containing the lists.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static FarmAnimalHousingCategories getFarmAnimalHousingCategories(final Connection connection, final DBMessagesKey messagesKey)
      throws SQLException {
    final Map<Integer, FarmAnimalCategory> animalMap = FarmAnimalCategoryRepository.getFarmAnimalCategories(connection, messagesKey);

    final FarmAnimalHousingCollector housingCollector = new FarmAnimalHousingCollector(messagesKey, animalMap);
    housingCollector.collect(connection, FARM_ANIMAL_HOUSING_CATEGORIES_QUERY);
    final Map<Integer, FarmAnimalHousingCategory> housingMap = housingCollector.getEntityMap();
    final FarmAdditionalHousingSystemCollector additionalHousingCollector = new FarmAdditionalHousingSystemCollector(messagesKey, housingMap);
    additionalHousingCollector.collect(connection, FARM_ADDITIONAL_HOUSING_SYSTEMS_QUERY);

    final Map<String, String> animalBasicHousingCategoryCodes = getBasicHousingLinks(connection, housingMap);
    final Map<Integer, FarmAdditionalHousingSystemCategory> additionalSystemMap = additionalHousingCollector.getEntityMap();
    final Map<String, Set<String>> housingAllowedAdditionalSystems = getHousingAllowedAdditionalSystems(connection, housingMap, additionalSystemMap);

    final FarmAnimalHousingCategories categories = new FarmAnimalHousingCategories();
    categories.setAnimalCategories(new ArrayList<>(animalMap.values()));
    categories.setAnimalHousingCategories(housingCollector.getEntities());
    categories.setAdditionalHousingSystemCategories(additionalHousingCollector.getEntities());
    categories.setAnimalBasicHousingCategoryCodes(animalBasicHousingCategoryCodes);
    categories.setHousingAllowedAdditionalSystems(housingAllowedAdditionalSystems);
    return categories;
  }

  private static Map<String, String> getBasicHousingLinks(final Connection connection,
      final Map<Integer, FarmAnimalHousingCategory> housingMap) throws SQLException {
    final Map<String, String> animalBasicHousingCategoryCodes = new HashMap<>();
    try (PreparedStatement ps = connection.prepareStatement(FARM_ANIMAL_BASIC_HOUSING.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        // No presence checking, should fail if not present.
        final FarmAnimalHousingCategory housing = housingMap.get(QueryAttribute.FARM_ANIMAL_HOUSING_CATEGORY_ID.getInt(rs));
        final FarmAnimalHousingCategory basicHousing = housingMap.get(QueryAttribute.BASIC_HOUSING_CATEGORY_ID.getInt(rs));
        animalBasicHousingCategoryCodes.put(housing.getCode(), basicHousing.getCode());
      }
    }
    return animalBasicHousingCategoryCodes;
  }

  private static Map<String, Set<String>> getHousingAllowedAdditionalSystems(final Connection connection,
      final Map<Integer, FarmAnimalHousingCategory> housingMap, final Map<Integer, FarmAdditionalHousingSystemCategory> additionalSystemMap)
      throws SQLException {
    final Map<String, Set<String>> housingAllowedAdditionalSystemsCodes = new HashMap<>();
    try (PreparedStatement ps = connection.prepareStatement(FARM_HOUSING_ALLOWED_ADDITIONAL_SYSTEMS_.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        // No presence checking, should fail if not present.
        final FarmAnimalHousingCategory housingCategory = housingMap.get(QueryAttribute.FARM_ANIMAL_HOUSING_CATEGORY_ID.getInt(rs));
        final FarmAdditionalHousingSystemCategory additionalhousing =
            additionalSystemMap.get(QueryAttribute.FARM_ADDITIONAL_HOUSING_SYSTEM_ID.getInt(rs));
        housingAllowedAdditionalSystemsCodes
            .computeIfAbsent(housingCategory.getCode(), (k) -> new HashSet<>())
            .add(additionalhousing.getCode());
      }
    }
    return housingAllowedAdditionalSystemsCodes;
  }

  private static class FarmAnimalHousingCollector extends AbstractCategoryCollector<FarmAnimalHousingCategory> {

    private final Map<Integer, FarmAnimalCategory> farmAnimalCategories;

    protected FarmAnimalHousingCollector(final DBMessagesKey messagesKey,
        final Map<Integer, FarmAnimalCategory> farmAnimalCategories) {
      super(QueryAttribute.FARM_ANIMAL_HOUSING_CATEGORY_ID, messagesKey);
      this.farmAnimalCategories = farmAnimalCategories;
    }

    @Override
    FarmAnimalHousingCategory getNewCategory() throws SQLException {
      return new FarmAnimalHousingCategory();
    }

    @Override
    void setDescription(final FarmAnimalHousingCategory category, final ResultSet rs) throws SQLException {
      DBMessages.setFarmAnimalHousingMessages(category, messagesKey);
    }

    @Override
    void setRemainingInfo(final FarmAnimalHousingCategory category, final ResultSet rs) throws SQLException {
      category.setFarmEmissionFactorType(QueryAttribute.FARM_EMISSION_FACTOR_TYPE.getEnum(rs, FarmEmissionFactorType.class));
      final FarmAnimalCategory animalCategory = farmAnimalCategories.get(QueryAttribute.FARM_ANIMAL_CATEGORY_ID.getInt(rs));
      category.setAnimalCategoryCode(animalCategory.getCode());
      if (!category.getDescription().contains(animalCategory.getDescription())) {
        category.setDescription(String.format(DESCRIPTION_FORMAT, category.getDescription(), animalCategory.getDescription()));
      }
      category.addEmissionFactor(Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs)),
          QueryUtil.getDouble(rs, QueryAttribute.EMISSION_FACTOR));
    }

    @Override
    public void appendEntity(final FarmAnimalHousingCategory category, final ResultSet rs) throws SQLException {
      category.addEmissionFactor(Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs)),
          QueryUtil.getDouble(rs, QueryAttribute.EMISSION_FACTOR));
    }

  }

  private static class FarmAdditionalHousingSystemCollector extends AbstractCategoryCollector<FarmAdditionalHousingSystemCategory> {

    private final Map<Integer, FarmAnimalHousingCategory> farmAnimalHousingCategories;

    protected FarmAdditionalHousingSystemCollector(final DBMessagesKey messagesKey,
        final Map<Integer, FarmAnimalHousingCategory> farmAnimalHousingCategories) {
      super(QueryAttribute.FARM_ADDITIONAL_HOUSING_SYSTEM_ID, messagesKey);
      this.farmAnimalHousingCategories = farmAnimalHousingCategories;
    }

    @Override
    FarmAdditionalHousingSystemCategory getNewCategory() throws SQLException {
      return new FarmAdditionalHousingSystemCategory();
    }

    @Override
    void setDescription(final FarmAdditionalHousingSystemCategory category, final ResultSet rs) throws SQLException {
      DBMessages.setFarmAdditionalHousingSystemMessages(category, messagesKey);
    }

    @Override
    void setRemainingInfo(final FarmAdditionalHousingSystemCategory category, final ResultSet rs) throws SQLException {
      category.setAirScrubber(QueryAttribute.AIR_SCRUBBER.getBoolean(rs));
    }

    @Override
    public void appendEntity(final FarmAdditionalHousingSystemCategory category, final ResultSet rs) throws SQLException {
      final FarmAnimalHousingCategory housingCategory = farmAnimalHousingCategories.get(QueryAttribute.FARM_ANIMAL_HOUSING_CATEGORY_ID.getInt(rs));
      category.addReductionFraction(housingCategory.getCode(),
          Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs)),
          QueryUtil.getDouble(rs, QueryAttribute.REDUCTION_FACTOR));
    }
  }
}
