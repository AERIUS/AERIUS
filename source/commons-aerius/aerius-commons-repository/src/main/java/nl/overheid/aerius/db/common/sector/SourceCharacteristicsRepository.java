/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.characteristics.StandardTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.characteristics.TimeVaryingProfiles;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;

/**
 * Service class for Source Characteristics database queries.
 */
public final class SourceCharacteristicsRepository {

  private enum RepositoryAttribute implements Attribute {
    STANDARD_TIME_VARYING_PROFILE_ID,

    VALUE_INDEX;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private static final Query STANDARD_TIME_VARYING_PROFILES = QueryBuilder
      .from("standard_time_varying_profiles")
      .select(RepositoryAttribute.STANDARD_TIME_VARYING_PROFILE_ID, QueryAttribute.CODE, QueryAttribute.NAME,
          QueryAttribute.DESCRIPTION)
      .orderBy(RepositoryAttribute.STANDARD_TIME_VARYING_PROFILE_ID)
      .getQuery();

  private static final Query STANDARD_TIME_VARYING_PROFILE_VALUES = QueryBuilder
      .from("standard_time_varying_profile_values")
      .select(RepositoryAttribute.VALUE_INDEX, QueryAttribute.VALUE)
      .where(RepositoryAttribute.STANDARD_TIME_VARYING_PROFILE_ID)
      .orderBy(RepositoryAttribute.VALUE_INDEX)
      .getQuery();

  private static final Query SECTOR_DEFAULT_TIME_VARYING_PROFILES = QueryBuilder
      .from("sector_default_time_varying_profiles")
      .select(QueryAttribute.SECTOR_ID, QueryAttribute.CODE)
      .getQuery();

  private static final Query SECTOR_YEAR_DEFAULT_TIME_VARYING_PROFILES = QueryBuilder
      .from("sector_year_default_time_varying_profiles")
      .select(QueryAttribute.SECTOR_ID, QueryAttribute.YEAR, QueryAttribute.CODE)
      .getQuery();

  private SourceCharacteristicsRepository() {
  }

  public static TimeVaryingProfiles getTimeVaryingProfiles(final Connection con, final CustomTimeVaryingProfileType type) throws SQLException {
    final TimeVaryingProfiles timeVaryingProfiles = new TimeVaryingProfiles();
    timeVaryingProfiles.setStandardTimeVaryingProfiles(type, getStandardTimeVaryingProfiles(con));
    timeVaryingProfiles.setSectorDefaultStandardProfileCodes(type, getSectorDefaultProfiles(con));
    timeVaryingProfiles.setSectorYearDefaultStandardProfileCodes(type, getSectorYearDefaultProfiles(con));
    return timeVaryingProfiles;
  }

  private static List<StandardTimeVaryingProfile> getStandardTimeVaryingProfiles(final Connection con)
      throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(STANDARD_TIME_VARYING_PROFILES.get())) {
      final ResultSet rs = statement.executeQuery();

      final List<StandardTimeVaryingProfile> lst = new ArrayList<>();

      while (rs.next()) {
        final int id = QueryUtil.getInt(rs, RepositoryAttribute.STANDARD_TIME_VARYING_PROFILE_ID);
        final String code = QueryAttribute.CODE.getString(rs);
        final String name = QueryAttribute.NAME.getString(rs);
        final String description = QueryAttribute.DESCRIPTION.getString(rs);
        final List<Double> values = getStandardProfileValues(con, id);
        final StandardTimeVaryingProfile timeVaryingProfile =
            new StandardTimeVaryingProfile(id, code, name, description, CustomTimeVaryingProfileType.THREE_DAY, values);
        lst.add(timeVaryingProfile);
      }
      return lst;
    }
  }

  private static List<Double> getStandardProfileValues(final Connection con, final int id) throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(STANDARD_TIME_VARYING_PROFILE_VALUES.get())) {
      STANDARD_TIME_VARYING_PROFILE_VALUES.setParameter(statement, RepositoryAttribute.STANDARD_TIME_VARYING_PROFILE_ID, id);
      final ResultSet rs = statement.executeQuery();

      final List<Double> values = new ArrayList<>();

      while (rs.next()) {
        values.add(QueryAttribute.VALUE.getDouble(rs));
      }
      return values;
    }
  }

  private static Map<Integer, String> getSectorDefaultProfiles(final Connection con)
      throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(SECTOR_DEFAULT_TIME_VARYING_PROFILES.get())) {
      final ResultSet rs = statement.executeQuery();

      final Map<Integer, String> map = new HashMap<>();

      while (rs.next()) {
        final int sectorId = QueryAttribute.SECTOR_ID.getInt(rs);
        final String code = QueryAttribute.CODE.getString(rs);
        map.put(sectorId, code);
      }
      return map;
    }
  }

  private static Map<Integer, Map<Integer, String>> getSectorYearDefaultProfiles(final Connection con)
      throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(SECTOR_YEAR_DEFAULT_TIME_VARYING_PROFILES.get())) {
      final ResultSet rs = statement.executeQuery();

      final Map<Integer, Map<Integer, String>> map = new HashMap<>();

      while (rs.next()) {
        final int sectorId = QueryAttribute.SECTOR_ID.getInt(rs);
        final int year = QueryAttribute.YEAR.getInt(rs);
        final String code = QueryAttribute.CODE.getString(rs);
        final Map<Integer, String> yearMap = map.computeIfAbsent(sectorId, key -> new HashMap<>());
        yearMap.put(year, code);
      }
      return map;
    }
  }
}
