/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.ReceptorResult;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

public class CalculationRepositoryBean {

  private static final Logger LOG = LoggerFactory.getLogger(CalculationRepositoryBean.class);

  private final PMF pmf;

  public CalculationRepositoryBean(final PMF pmf) {
    this.pmf = pmf;
  }

  public List<ReceptorResult> getCalculationResults(final int[] calculationIds, final int resultSetId,
      final SummaryHexagonType hexagonType) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return CalculationRepository.getCalculationResultsFromSet(con, calculationIds, resultSetId, hexagonType);
    } catch (final SQLException e) {
      LOG.error("SQL error while retrieving calculation results", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public List<Integer> getCalculationResultIdSets(final int calculationId) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return CalculationRepository.getCalculationResultIdSets(con, calculationId);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting calculationResultSet", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public Map<Integer,EmissionResultKey> getCalculationResultIdSetsEmissionResultKey(final int calculationId) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return CalculationRepository.getCalculationResultIdSetsEmissionResultKey(con, calculationId);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting calculationResultSet", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * See {@link CalculationRepository#setNumberOfSources}
   */
  public void setNumberOfSources(final Connection con, final Integer calculationId, final int numberOfSources, final int numberOfSubstances)
      throws SQLException {
    CalculationRepository.setNumberOfSources(con, calculationId, numberOfSources, numberOfSubstances);
  }

}
