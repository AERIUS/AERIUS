/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.emissions.shipping.ShippingLaden;

/**
 *
 */
public class InlandCategoryEmissionFactorKey extends InlandCategoryKey {

  private final Substance substance;

  /**
   * @param emissionValueKey The emission value key for the emission factor.
  * @param direction The direction the ship is going.
   * @param laden The laden state.
   * @param waterwayCategoryId The ID of the waterway category.
   */
  public InlandCategoryEmissionFactorKey(final Substance substance, final WaterwayDirection direction, final ShippingLaden laden,
      final int waterwayCategoryId) {
    super(direction, laden, waterwayCategoryId);
    this.substance = substance;
  }

  public Substance getSubstance() {
    return substance;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + super.hashCode();
    result = prime * result + ((substance == null) ? 0 : substance.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && this.getClass() == obj.getClass()) {
      final InlandCategoryEmissionFactorKey other = (InlandCategoryEmissionFactorKey) obj;
      return super.equals(obj) && (substance != null && substance.equals(other.getSubstance())
          || substance == null && other.getSubstance() == null);
    }
    return false;
  }

  @Override
  public String toString() {
    return "InlandCategoryEmissionFactorKey [substance=" + substance + super.toString() + "]";
  }

}
