/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.db.common.configuration;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.db.common.AssessmentAreaRepository;
import nl.overheid.aerius.db.common.BasePMF;
import nl.overheid.aerius.db.common.CalculationCategoriesRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.config.Settings;
import nl.overheid.aerius.shared.constants.DrivingSide;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Class to initialize the application configuration.
 */
public class ApplicationConfigurationInitializer {
  private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfigurationInitializer.class);

  /**
   * Returns the application configuration with customer-specific context configuration. The configuration queried depends on the database configuration.
   *
   * @param con the database connection
   * @param dbMessagesKey the db message key
   * @param customer implementing organization
   * @param productProfile product type
   * @return A filled context object.
   * @throws SQLException When a database error occurs.
   * @throws AeriusException When another error occurs.
   */
  public static ApplicationConfiguration createApplicationConfiguration(final Connection con, final DBMessagesKey dbMessagesKey,
      final AeriusCustomer customer, final ProductProfile productProfile) throws SQLException, AeriusException {
    final ApplicationConfiguration appConfig = new ApplicationConfiguration();
    fillBaseData(con, appConfig, dbMessagesKey);
    fillCustomerConfiguration(con, dbMessagesKey, customer, appConfig, productProfile);
    fillApplicationSettings(con, appConfig.getSettings());
    return appConfig;
  }

  private static void fillCustomerConfiguration(final Connection con, final DBMessagesKey messagesKey, final AeriusCustomer customer,
      final ApplicationConfiguration appConfig, final ProductProfile productProfile) throws SQLException, AeriusException {
    final AbstractApplicationConfigurationRepository configRepository = switch (customer) {
    case JNCC -> new NcaApplicationConfigurationRepository();
    case RIVM -> productProfile == ProductProfile.LBV_POLICY
        ? new OwN2000LbvApplicationConfigurationRepository()
        : new OwN2000ApplicationConfigurationRepository();
    default -> {
      LOGGER.warn("Unknown customer {} passed to fill configurations.", customer);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    };

    configRepository.fill(con, appConfig, messagesKey);
    LOGGER.debug("Initialized configuration for customer {}", customer);
  }

  private static void fillApplicationSettings(final Connection con, final Settings settings)
      throws SQLException {
    SettingsUtil.addOptionalSettingPreferEnvironment(con, settings, SharedConstantsEnum.MANUAL_URL);
    SettingsUtil.addOptionalSettingPreferEnvironment(con, settings, SharedConstantsEnum.NEXT_STEPS_URL);
    SettingsUtil.addOptionalSettingPreferEnvironment(con, settings, SharedConstantsEnum.NEXT_STEPS_SECONDARY_URL);
    SettingsUtil.addOptionalSettingPreferEnvironment(con, settings, SharedConstantsEnum.QUICK_START_URL);
    SettingsUtil.addSettingPreferEnvironment(con, settings, SharedConstantsEnum.RELEASE_DETAILS_URL);

    SettingsUtil.addSetting(con, settings, SharedConstantsEnum.CALCULATOR_BOUNDARY);

    SettingsUtil.addEnumSetting(con, settings, SharedConstantsEnum.DRIVING_SIDE, DrivingSide.class);
    SettingsUtil.addIntSetting(con, settings, SharedConstantsEnum.SYSTEM_INFO_POLLING_TIME);
    SettingsUtil.addDoubleSetting(con, settings, SharedConstantsEnum.DEFAULT_NETTING_FACTOR);
    SettingsUtil.addIntSetting(con, settings, SharedConstantsEnum.DEFAULT_CALCULATION_POINTS_PLACEMENT_RADIUS);
    SettingsUtil.addIntSetting(con, settings, SharedConstantsEnum.CALCULATOR_LIMITS_MAX_LINE_LENGTH);
    SettingsUtil.addIntSetting(con, settings, SharedConstantsEnum.CALCULATOR_LIMITS_MAX_POLYGON_SURFACE);

    addVersions(con, settings);
    addUrls(con, settings);
  }

  private static void addUrls(final Connection con, final Settings settings) throws SQLException {
    SettingsUtil.addSettingPreferEnvironment(con, settings, SharedConstantsEnum.CONNECT_INTERNAL_URL);
    SettingsUtil.addSettingPreferEnvironment(con, settings, SharedConstantsEnum.GEOSERVER_INTERNAL_URL);
    SettingsUtil.addSettingPreferEnvironment(con, settings, SharedConstantsEnum.SEARCH_ENDPOINT_URL);
  }

  private static void addVersions(final Connection con, final Settings settings) throws SQLException {
    settings.setSetting(SharedConstantsEnum.DATABASE_VERSION, BasePMF.getDatabaseVersion(con));
    settings.setSetting(SharedConstantsEnum.AERIUS_VERSION, AeriusVersion.getVersionNumber());
  }

  private static void fillBaseData(final Connection con, final ApplicationConfiguration c, final DBMessagesKey messagesKey)
      throws SQLException {
    c.setReceptorGridSettings(ReceptorGridSettingsRepository.getReceptorGridSettings(con));
    c.setSectorCategories(SectorRepository.getSectorCategories(con, messagesKey));
    c.setNatureAreaDirectives(AssessmentAreaRepository.getNatureAreaDirectives(con));
    c.setCalculationPermitAreas(CalculationCategoriesRepository.getCalculationPermitAreas(con));
    c.setCalculationProjectCategories(CalculationCategoriesRepository.getCalculationProjectCategories(con));
    c.setDefaultCalculationDistances(CalculationCategoriesRepository.getDefaultDistances(con));
  }

}
