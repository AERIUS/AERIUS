/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.reference;

/**
 * Enum used to generate document reference codes.
 *
 * NOTE: DO NOT CHANGE THE ORDER OF THE ENUMS. IMPLEMENTATION DEPENDS ON ORDER.
 */
enum ReferenceType {
  /**
   * Reference for PAS Melding documents.
   *
   * @deprecated PAS Melding is not used anymore. But should be kept to be able to read old documents.
   */
  @Deprecated
  MELDING,
  /**
   * Reference for Calculator generated documents.
   */
  PERMIT,
  /**
   * Reference for Calculator LBV(-plus) policy documents.
   */
  LBV,
}
