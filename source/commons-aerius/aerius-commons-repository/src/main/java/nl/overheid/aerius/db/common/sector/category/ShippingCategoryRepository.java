/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.db.common.sector.InlandCategoryEmissionFactorKey;
import nl.overheid.aerius.db.common.sector.InlandCategoryKey;
import nl.overheid.aerius.db.common.sector.SourceCharacteristicsUtil;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.db.util.WhereClause;
import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.ShippingMovementType;
import nl.overheid.aerius.shared.emissions.shipping.ShippingLaden;

/**
 * Service class for Shipping Categories.
 */
public final class ShippingCategoryRepository {

  private enum RepositoryAttribute implements Attribute {

    SHIPPING_MARITIME_CATEGORY_ID, MOVEMENT_TYPE,

    SHIPPING_INLAND_CATEGORY_ID, SHIP_DIRECTION, LADEN_STATE,

    SHIPPING_INLAND_WATERWAY_CATEGORY_ID;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final WhereClause WHERE_LADEN_STATE = new StaticWhereClause(
      RepositoryAttribute.LADEN_STATE.attribute() + " = 'laden'::shipping_inland_laden_state");

  private static final WhereClause WHERE_MOVEMENT_TYPE = new StaticWhereClause(
      RepositoryAttribute.MOVEMENT_TYPE.attribute() + " = ?::shipping_movement_type", RepositoryAttribute.MOVEMENT_TYPE);

  private static final Query ALL_MARITIME_CATEGORIES_QUERY = QueryBuilder.from("shipping_maritime_categories")
      .select(RepositoryAttribute.SHIPPING_MARITIME_CATEGORY_ID).select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES).getQuery();

  private static final Query MARITIME_CATEGORY_EMISSION_FACTOR_QUERY = QueryBuilder.from("shipping_maritime_category_emission_factors")
      .select(QueryAttribute.SUBSTANCE_ID, QueryAttribute.EMISSION_FACTOR)
      .where(RepositoryAttribute.SHIPPING_MARITIME_CATEGORY_ID).where(WHERE_MOVEMENT_TYPE).where(QueryAttribute.YEAR).getQuery();

  private static final Query MARITIME_CATEGORY_CHARACTERISTICS_QUERY = QueryBuilder.from("shipping_maritime_source_characteristics_view")
      .select(SourceCharacteristicsUtil.OPS_CHARACTERISTICS_ATTRIBUTES).where(RepositoryAttribute.SHIPPING_MARITIME_CATEGORY_ID)
      .where(WHERE_MOVEMENT_TYPE).where(QueryAttribute.YEAR).getQuery();

  private static final Query ALL_INLAND_CATEGORIES_QUERY = QueryBuilder.from("shipping_inland_categories")
      .select(RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID).select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES).getQuery();

  private static final Query INLAND_CATEGORY_ROUTE_EMISSION_FACTOR_QUERY = QueryBuilder.from("shipping_inland_category_emission_factors")
      .select(QueryAttribute.SUBSTANCE_ID, RepositoryAttribute.SHIPPING_INLAND_WATERWAY_CATEGORY_ID,
          RepositoryAttribute.SHIP_DIRECTION, RepositoryAttribute.LADEN_STATE, QueryAttribute.EMISSION_FACTOR)
      .where(RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID).where(QueryAttribute.YEAR).getQuery();

  private static final Query INLAND_CATEGORY_ROUTE_CHARACTERISTICS_QUERY = QueryBuilder.from("shipping_inland_source_characteristics_view")
      .select(RepositoryAttribute.SHIPPING_INLAND_WATERWAY_CATEGORY_ID, RepositoryAttribute.SHIP_DIRECTION, RepositoryAttribute.LADEN_STATE)
      .select(SourceCharacteristicsUtil.OPS_CHARACTERISTICS_ATTRIBUTES).where(RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID).getQuery();

  private static final Query INLAND_CATEGORY_DOCK_EMISSION_FACTOR_QUERY = QueryBuilder.from("shipping_inland_category_emission_factors_docked")
      .select(QueryAttribute.SUBSTANCE_ID, QueryAttribute.EMISSION_FACTOR).where(RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID)
      .where(WHERE_LADEN_STATE).where(QueryAttribute.YEAR).getQuery();

  private static final Query INLAND_CATEGORY_DOCK_CHARACTERISTICS_QUERY = QueryBuilder.from("shipping_inland_source_characteristics_docked_view")
      .select(RepositoryAttribute.LADEN_STATE).select(SourceCharacteristicsUtil.OPS_CHARACTERISTICS_ATTRIBUTES)
      .where(RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID).getQuery();

  private static final Query INLAND_WATERWAY_CATEGORIES = QueryBuilder.from("shipping_inland_waterway_categories_view")
      .select(RepositoryAttribute.SHIPPING_INLAND_WATERWAY_CATEGORY_ID, RepositoryAttribute.SHIP_DIRECTION)
      .select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES).getQuery();

  private static final Query INLAND_SHIP_TO_WATERWAY = QueryBuilder.from("shipping_inland_category_emission_factors")
      .select(RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID, RepositoryAttribute.SHIPPING_INLAND_WATERWAY_CATEGORY_ID).distinct()
      .orderBy(RepositoryAttribute.SHIPPING_INLAND_WATERWAY_CATEGORY_ID).getQuery();

  private ShippingCategoryRepository() {
  }

  /**
   * Returns all maritime Ship Emission Categories from the database.
   *
   * @param con Connection to use
   * @param messagesKey DBMessagesKey to use for i18n stuff
   * @return ArrayList with all ShipEmissionCategories
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static List<MaritimeShippingCategory> findAllMaritimeShippingCategories(final Connection con, final DBMessagesKey messagesKey)
      throws SQLException {
    final AbstractCategoryCollector<MaritimeShippingCategory> collector = new AbstractCategoryCollector<MaritimeShippingCategory>(
        RepositoryAttribute.SHIPPING_MARITIME_CATEGORY_ID, messagesKey) {

      @Override
      void setDescription(final MaritimeShippingCategory category, final ResultSet rs) {
        DBMessages.setShippingCategoryMessages(category, messagesKey);
      }

      @Override
      MaritimeShippingCategory getNewCategory() {
        return new MaritimeShippingCategory();
      }
    };
    collector.collect(con, ALL_MARITIME_CATEGORIES_QUERY);
    return collector.getEntities();
  }

  /**
   * Find the emission factors for a specific ship category. For moving ships, this factor is in kg/meters.
   *
   * @param con Connection to use
   * @param category The category to determine the emission factors for.
   * @param movementType The movement type to determine the emission factors for.
   * @return Map with the emission factors for the specified category and movement type.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static Map<Substance, Double> findMaritimeCategoryEmissionFactors(final Connection con, final MaritimeShippingCategory category,
      final ShippingMovementType movementType, final int year) throws SQLException {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);

    try (final PreparedStatement statement = con.prepareStatement(MARITIME_CATEGORY_EMISSION_FACTOR_QUERY.get())) {
      MARITIME_CATEGORY_EMISSION_FACTOR_QUERY.setParameter(statement, RepositoryAttribute.SHIPPING_MARITIME_CATEGORY_ID, category.getId());
      MARITIME_CATEGORY_EMISSION_FACTOR_QUERY.setParameter(statement, RepositoryAttribute.MOVEMENT_TYPE, movementType.name().toLowerCase());
      MARITIME_CATEGORY_EMISSION_FACTOR_QUERY.setParameter(statement, QueryAttribute.YEAR, year);
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        double emissionFactor = QueryAttribute.EMISSION_FACTOR.getDouble(rs);
        // emission factors in DB are in kg/km, convert to kg/m
        emissionFactor = emissionFactor / ImaerConstants.KG_PER_KM_TO_KG_PER_METER;

        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }

  /**
   * Find the docked emission factors for a specific ship category. For docked ships, this factor is in kg/hour.
   *
   * @param con Connection to use
   * @param category The category to determine the emission factors for.
   * @return Map with the emission factors for the specified category and movement type.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static Map<Substance, Double> findMaritimeCategoryDockedEmissionFactors(final Connection con, final MaritimeShippingCategory category,
      final int year) throws SQLException {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);

    try (final PreparedStatement statement = con.prepareStatement(MARITIME_CATEGORY_EMISSION_FACTOR_QUERY.get())) {
      MARITIME_CATEGORY_EMISSION_FACTOR_QUERY.setParameter(statement, RepositoryAttribute.SHIPPING_MARITIME_CATEGORY_ID, category.getId());
      MARITIME_CATEGORY_EMISSION_FACTOR_QUERY.setParameter(statement, RepositoryAttribute.MOVEMENT_TYPE, "dock");
      MARITIME_CATEGORY_EMISSION_FACTOR_QUERY.setParameter(statement, QueryAttribute.YEAR, year);
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final double emissionFactor = QueryAttribute.EMISSION_FACTOR.getDouble(rs);
        // for docking emission factors are in kg/hour.
        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }

  /**
   * @param con The connection to use to get the right characteristics.
   * @param category The category to get the characteristics for. Used to get the default sector OPS values.
   * @param year calculationyear
   * @return The right OPSSourceCharacteristics.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static OPSSourceCharacteristics getDockedCharacteristics(final Connection con, final MaritimeShippingCategory category, final int year)
      throws SQLException {
    return getCharacteristics(con, category, "dock", year);
  }

  /**
   * @param con The connection to use to get the right characteristics.
   * @param category The category to get the characteristics for. Used to get the default sector OPS values.
   * @param movementType The movement type to determine specific characteristics for (heat content and height).
   * @param year calculationyear
   * @return The right OPSSourceCharacteristics.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static OPSSourceCharacteristics getCharacteristics(final Connection con, final MaritimeShippingCategory category,
      final ShippingMovementType movementType, final int year) throws SQLException {
    return getCharacteristics(con, category, movementType.name(), year);
  }

  private static OPSSourceCharacteristics getCharacteristics(final Connection con, final MaritimeShippingCategory category,
      final String movementType, final int year) throws SQLException {
    OPSSourceCharacteristics sourceCharacteristics = null;
    try (final PreparedStatement statement = con.prepareStatement(MARITIME_CATEGORY_CHARACTERISTICS_QUERY.get())) {
      MARITIME_CATEGORY_CHARACTERISTICS_QUERY.setParameter(statement, RepositoryAttribute.SHIPPING_MARITIME_CATEGORY_ID, category.getId());
      MARITIME_CATEGORY_CHARACTERISTICS_QUERY.setParameter(statement, RepositoryAttribute.MOVEMENT_TYPE, movementType.toLowerCase());
      MARITIME_CATEGORY_CHARACTERISTICS_QUERY.setParameter(statement, QueryAttribute.YEAR, year);

      final ResultSet rs = statement.executeQuery();

      if (rs.next()) {
        sourceCharacteristics = new OPSSourceCharacteristics();
        SourceCharacteristicsUtil.setSourceCharacteristicsFromResultSet(sourceCharacteristics, rs);
      }
    }

    return sourceCharacteristics;
  }

  /**
   * Returns the inland shipping categories.
   *
   * @param con The connection to use to get the categories
   * @param messagesKey DBMessagesKey to use for i18n stuff
   * @return Object containing all categories used for inland shipping.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static InlandShippingCategories findInlandShippingCategories(final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    final InlandShippingCategories categories = new InlandShippingCategories();
    categories.setShipCategories(findAllInlandShippingCategories(con, messagesKey));
    categories.setWaterwayCategories(getInlandWaterwayCategories(con, messagesKey));
    // link the waterways to categories
    try (final PreparedStatement statement = con.prepareStatement(INLAND_SHIP_TO_WATERWAY.get())) {
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        final InlandShippingCategory shipCategory = categories
            .getShipCategoryById(QueryUtil.getInt(rs, RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID));
        final InlandWaterwayCategory waterwayCategory = categories
            .getWaterwayCategoryById(QueryUtil.getInt(rs, RepositoryAttribute.SHIPPING_INLAND_WATERWAY_CATEGORY_ID));
        categories.addWaterwayForShip(shipCategory, waterwayCategory);
      }
    }

    return categories;

  }

  /**
   * Returns the inland ships categories.
   *
   * @param con The connection to use to get the trend factors
   * @param messagesKey DBMessagesKey to use for i18n stuff
   * @return List of inland shipping categories
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static List<InlandShippingCategory> findAllInlandShippingCategories(final Connection con, final DBMessagesKey messagesKey)
      throws SQLException {
    final AbstractCategoryCollector<InlandShippingCategory> collector = new AbstractCategoryCollector<InlandShippingCategory>(
        RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID, messagesKey) {

      @Override
      void setDescription(final InlandShippingCategory category, final ResultSet rs) {
        DBMessages.setShippingCategoryMessages(category, messagesKey);
      }

      @Override
      InlandShippingCategory getNewCategory() {
        return new InlandShippingCategory();
      }
    };
    collector.collect(con, ALL_INLAND_CATEGORIES_QUERY);
    return collector.getEntities();
  }

  /**
   * Find the emission factors for a specific inland shipping category on a route. This factor is in kg/meters/ship.
   *
   * @param con Connection to use
   * @param category The category to determine the emission factors for.
   * @return Hashmap with the emission factors for the specified category.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static Map<InlandCategoryEmissionFactorKey, Double> findInlandCategoryRouteEmissionFactors(final Connection con,
      final InlandShippingCategory category, final int year) throws SQLException {
    final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors = new HashMap<>();

    try (final PreparedStatement statement = con.prepareStatement(INLAND_CATEGORY_ROUTE_EMISSION_FACTOR_QUERY.get())) {
      INLAND_CATEGORY_ROUTE_EMISSION_FACTOR_QUERY.setParameter(statement, RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID, category.getId());
      INLAND_CATEGORY_ROUTE_EMISSION_FACTOR_QUERY.setParameter(statement, QueryAttribute.YEAR, year);
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final int waterwayCategoryId = QueryUtil.getInt(rs, RepositoryAttribute.SHIPPING_INLAND_WATERWAY_CATEGORY_ID);
        final WaterwayDirection direction = QueryUtil.getEnum(rs, RepositoryAttribute.SHIP_DIRECTION, WaterwayDirection.class);
        final ShippingLaden laden = QueryUtil.getEnum(rs, RepositoryAttribute.LADEN_STATE, ShippingLaden.class);
        final double emissionFactor = QueryAttribute.EMISSION_FACTOR.getDouble(rs);
        // emission factors in DB are in g/km, convert to kg/m
        emissionFactors.put(new InlandCategoryEmissionFactorKey(substance, direction, laden, waterwayCategoryId),
            emissionFactor / ImaerConstants.GRAM_PER_KM_TO_KG_PER_METER);
      }
    }
    return emissionFactors;
  }

  /**
   * @param con The connection to use to get the right characteristics.
   * @param category The category to get the characteristics for. Used to get the default sector OPS values.
   * @return The right OPSSourceCharacteristics for each combination of category, waterway type, laden/unladen and route direction.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static Map<InlandCategoryKey, OPSSourceCharacteristics> getInlandCategoryRouteCharacteristics(final Connection con,
      final InlandShippingCategory category) throws SQLException {
    final Map<InlandCategoryKey, OPSSourceCharacteristics> characteristics = new HashMap<>();
    try (final PreparedStatement statement = con.prepareStatement(INLAND_CATEGORY_ROUTE_CHARACTERISTICS_QUERY.get())) {
      statement.setInt(1, category.getId());
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        final int waterwayCategoryId = QueryUtil.getInt(rs, RepositoryAttribute.SHIPPING_INLAND_WATERWAY_CATEGORY_ID);
        final WaterwayDirection direction = QueryUtil.getEnum(rs, RepositoryAttribute.SHIP_DIRECTION, WaterwayDirection.class);
        final ShippingLaden laden = QueryUtil.getEnum(rs, RepositoryAttribute.LADEN_STATE, ShippingLaden.class);

        final OPSSourceCharacteristics sourceCharacteristics = new OPSSourceCharacteristics();
        SourceCharacteristicsUtil.setSourceCharacteristicsFromResultSet(sourceCharacteristics, rs);

        characteristics.put(new InlandCategoryKey(direction, laden, waterwayCategoryId), sourceCharacteristics);
      }
    }

    return characteristics;
  }

  /**
   * Find the emission factors for a specific inland shipping category which is moored.. This factor is in kg/hour/ship.
   *
   * @param con Connection to use
   * @param category The category to determine the emission factors for.
   * @param year
   * @return Hashmap with the emission factors for the specified category.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static Map<Substance, Double> findInlandCategoryMooringEmissionFactors(final Connection con, final InlandShippingCategory category,
      final int year) throws SQLException {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);

    try (final PreparedStatement statement = con.prepareStatement(INLAND_CATEGORY_DOCK_EMISSION_FACTOR_QUERY.get())) {
      INLAND_CATEGORY_DOCK_EMISSION_FACTOR_QUERY.setParameter(statement, RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID, category.getId());
      INLAND_CATEGORY_DOCK_EMISSION_FACTOR_QUERY.setParameter(statement, QueryAttribute.YEAR, year);
      final ResultSet rs = statement.executeQuery();

      // SELECT year, substance_id, emission_factor
      while (rs.next()) {
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final double emissionFactor = QueryAttribute.EMISSION_FACTOR.getDouble(rs);
        // emission factors in DB is in g/hour convert it to kg/hour
        emissionFactors.put(substance, emissionFactor / ImaerConstants.GRAM_TO_KILOGRAM);
      }
    }
    return emissionFactors;
  }

  /**
   * @param con The connection to use to get the right characteristics.
   * @param category The category to get the characteristics for. Used to get the default sector OPS values.
   * @return The right OPSSourceCharacteristics for laden/unladen.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static Map<ShippingLaden, OPSSourceCharacteristics> getInlandCategoryMooringCharacteristics(final Connection con,
      final InlandShippingCategory category) throws SQLException {
    final Map<ShippingLaden, OPSSourceCharacteristics> characteristics = new EnumMap<>(ShippingLaden.class);
    try (final PreparedStatement statement = con.prepareStatement(INLAND_CATEGORY_DOCK_CHARACTERISTICS_QUERY.get())) {
      INLAND_CATEGORY_DOCK_CHARACTERISTICS_QUERY.setParameter(statement, RepositoryAttribute.SHIPPING_INLAND_CATEGORY_ID, category.getId());
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        final ShippingLaden laden = QueryUtil.getEnum(rs, RepositoryAttribute.LADEN_STATE, ShippingLaden.class);

        final OPSSourceCharacteristics sourceCharacteristics = new OPSSourceCharacteristics();
        SourceCharacteristicsUtil.setSourceCharacteristicsFromResultSet(sourceCharacteristics, rs);

        characteristics.put(laden, sourceCharacteristics);
      }
    }

    return characteristics;
  }

  /**
   * @param con The connection to use.
   * @return The list of waterway categories that can be used.
   * @throws SQLException In case of exceptions.
   */
  public static List<InlandWaterwayCategory> getInlandWaterwayCategories(final Connection con, final DBMessagesKey messagesKey)
      throws SQLException {
    final AbstractCategoryCollector<InlandWaterwayCategory> collector = new AbstractCategoryCollector<InlandWaterwayCategory>(
        RepositoryAttribute.SHIPPING_INLAND_WATERWAY_CATEGORY_ID, messagesKey) {

      @Override
      void setDescription(final InlandWaterwayCategory category, final ResultSet rs) {
        DBMessages.setWaterwayCategoryMessages(category, messagesKey);
      }

      @Override
      InlandWaterwayCategory getNewCategory() {
        return new InlandWaterwayCategory();
      }

      @Override
      public void appendEntity(final InlandWaterwayCategory entity, final ResultSet rs) throws SQLException {
        entity.getDirections().add(QueryUtil.getEnum(rs, RepositoryAttribute.SHIP_DIRECTION, WaterwayDirection.class));
      }

    };
    collector.collect(con, INLAND_WATERWAY_CATEGORIES);
    return collector.getEntities();
  }
}
