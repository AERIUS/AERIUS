/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.db.calculator.EmissionResultRepositoryUtil;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.BooleanWhereClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.info.CriticalLevels;
import nl.overheid.aerius.shared.domain.info.HabitatAreaInfo;
import nl.overheid.aerius.shared.domain.info.HabitatGoal;
import nl.overheid.aerius.shared.domain.info.HabitatSurfaceType;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.info.HabitatTypeInfo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * Service class for Habitat database queries.
 */
public final class HabitatRepository {
  // Queries
  private static final Attribute[] HABITAT_TYPE_ATTRIBUTES = new Attribute[] {QueryAttribute.QUALITY_GOAL,
      QueryAttribute.EXTENT_GOAL,};

  private static final Attribute[] HABITAT_AREA_ATTRIBUTES = new Attribute[] {QueryAttribute.SURFACE,
      QueryAttribute.CARTOGRAPHIC_SURFACE, QueryAttribute.RELEVANT_SURFACE, QueryAttribute.RELEVANT_CARTOGRAPHIC_SURFACE,};

  private static final Attribute[] CRITICAL_LEVEL_ATTRIBUTES = new Attribute[] {QueryAttribute.CRITICAL_LEVEL, QueryAttribute.SUBSTANCE_ID,
      QueryAttribute.RESULT_TYPE,};
  private static final Attribute[] SENSITIVENESS_ATTRIBUTES = new Attribute[] {QueryAttribute.SENSITIVE, QueryAttribute.SUBSTANCE_ID,
      QueryAttribute.RESULT_TYPE,};

  private static final Query HABITAT_TYPES = getHabitatTypeQueryBuilder("habitat_types").orderBy(QueryAttribute.NAME).getQuery();

  private static final Query HABITAT_TYPES_CRITICAL_LEVELS = QueryBuilder.from("habitat_type_critical_levels")
      .select(QueryAttribute.HABITAT_TYPE_ID).select(CRITICAL_LEVEL_ATTRIBUTES)
      .where(new StaticWhereClause("critical_level != 'NaN'"))
      .getQuery();

  private static final Query HABITAT_TYPES_SENSITIVENESS = QueryBuilder.from("habitat_type_critical_levels")
      .select(QueryAttribute.HABITAT_TYPE_ID).select(SENSITIVENESS_ATTRIBUTES)
      .getQuery();

  private static final Query RELEVANT_HABITAT_INFO_BY_ASSESSMENT_AREA = getHabitatAreaInfoQueryBuilder("habitat_info_for_assessment_area_view")
      .where(new BooleanWhereClause(QueryAttribute.RELEVANT, true)).getQuery();

  private static final Query EXTRA_ASSESSMENT_HABITAT_INFO_BY_ASSESSMENT_AREA =
      getHabitatTypeInfoQueryBuilder("extra_assessment_habitat_info_for_assessment_area_view").getQuery();

  private HabitatRepository() {
    // Don't allow to instantiate.
  }

  // Query builders

  private static QueryBuilder getHabitatTypeQueryBuilder(final String from) {
    return QueryBuilder.from(from).select(QueryAttribute.HABITAT_TYPE_ID);
  }

  private static QueryBuilder getBaseHabitatQueryBuilder(final String from) {
    return getHabitatTypeQueryBuilder(from).where(QueryAttribute.ASSESSMENT_AREA_ID);
  }

  private static QueryBuilder getHabitatTypeInfoQueryBuilder(final String from) {
    return getBaseHabitatQueryBuilder(from).select(HABITAT_TYPE_ATTRIBUTES).select(CRITICAL_LEVEL_ATTRIBUTES);
  }

  private static QueryBuilder getHabitatAreaInfoQueryBuilder(final String from) {
    return getHabitatTypeInfoQueryBuilder(from).select(HABITAT_AREA_ATTRIBUTES);
  }

  // Info getters

  /**
   * Returns a list of all habitat types from the database.
   *
   * @param con The connection to use
   * @return List of all habitat types.
   * @throws SQLException In case of a database error.
   */
  public static List<HabitatType> getHabitatTypes(final Connection con, final DBMessagesKey key) throws SQLException {
    final HabitatTypeCollector collector = new HabitatTypeCollector(key);
    collector.collect(con, HABITAT_TYPES);
    return collector.getEntities();
  }

  /**
   * Returns a map with critical levels for all habitat types.
   *
   * @param con The connection to use
   * @return Map of critical levels for all habitat types (by ID).
   * @throws SQLException In case of a database error.
   */
  public static Map<Integer, CriticalLevels> getHabitatTypesCriticalLevels(final Connection con) throws SQLException {
    final Map<Integer, CriticalLevels> criticalLevelsPerHabitat = new HashMap<>();
    try (PreparedStatement ps = con.prepareStatement(HABITAT_TYPES_CRITICAL_LEVELS.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        final CriticalLevels criticalLevels = criticalLevelsPerHabitat.computeIfAbsent(QueryAttribute.HABITAT_TYPE_ID.getInt(rs),
            id -> new CriticalLevels());
        EmissionResultRepositoryUtil.addCriticalLevel(rs, criticalLevels);
      }
    }
    return criticalLevelsPerHabitat;
  }

  /**
   * Returns a map with pollutant sensitiveness for all habitat types.
   *
   * @param con The connection to use
   * @return Map of pollutant sensitiveness for all habitat types (by ID).
   * @throws SQLException In case of a database error.
   */
  public static Map<Integer, Map<EmissionResultKey, Boolean>> getHabitatTypesSensitiveness(final Connection con) throws SQLException {
    final Map<Integer, Map<EmissionResultKey, Boolean>> sensitivenessPerHabitat = new HashMap<>();
    try (PreparedStatement ps = con.prepareStatement(HABITAT_TYPES_SENSITIVENESS.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final EmissionResultType type = QueryAttribute.RESULT_TYPE.getEnum(rs, EmissionResultType.class);
        final Map<EmissionResultKey, Boolean> sensitivenessMap = sensitivenessPerHabitat.computeIfAbsent(QueryAttribute.HABITAT_TYPE_ID.getInt(rs),
            id -> new HashMap<>());
        sensitivenessMap.put(EmissionResultKey.valueOf(substance, type), QueryAttribute.SENSITIVE.getBoolean(rs));
      }
    }
    return sensitivenessPerHabitat;
  }

  /**
   * @param con The connection to use
   * @param assessmentAreaId The id of the assessment area to get the habitat type info for.
   * @return The list of relevant habitats on the assessment area.
   * @throws SQLException In case of a database error.
   */
  public static List<HabitatAreaInfo> getRelevantHabitatInfo(final Connection con, final int assessmentAreaId, final DBMessagesKey key)
      throws SQLException {
    return collectByAssessmentArea(con, RELEVANT_HABITAT_INFO_BY_ASSESSMENT_AREA, assessmentAreaId,
        new HabitatAreaExtendedInfoCollector(key));
  }

  /**
   * @param con The connection to use
   * @param assessmentAreaId The id of the assessment area to get the extra assessment habitat type info for.
   * @return The list of extra assessment habitats on the assessment area.
   * @throws SQLException In case of a database error.
   */
  public static List<HabitatTypeInfo> getExtraAssessmentHabitatInfo(final Connection con, final int assessmentAreaId, final DBMessagesKey key)
      throws SQLException {
    return collectByAssessmentArea(con, EXTRA_ASSESSMENT_HABITAT_INFO_BY_ASSESSMENT_AREA, assessmentAreaId,
        new HabitatTypeExtendedInfoCollector(key));
  }

  // Info getters - query/resultset/collector helpers

  private static <T extends HasId> ArrayList<T> collectByAssessmentArea(final Connection con, final Query query, final int assessmentAreaId,
      final EntityCollector<T> collector) throws SQLException {
    try (PreparedStatement ps = con.prepareStatement(query.get())) {
      query.setParameter(ps, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);

      final ResultSet rs = ps.executeQuery();
      collector.collectEntities(rs);
    }
    return collector.getEntities();
  }

  // Collectors

  public abstract static class HabitatCollector<T extends HabitatType> extends EntityCollector<T> {
    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryAttribute.HABITAT_TYPE_ID.getInt(rs);
    }
  }

  public static class HabitatTypeCollector extends HabitatCollector<HabitatType> {
    private final DBMessagesKey key;

    public HabitatTypeCollector(final DBMessagesKey key) {
      this.key = key;
    }

    @Override
    public HabitatType fillEntity(final ResultSet rs) throws SQLException {
      return setHabitatTypeFromResultSet(rs, new HabitatType(), key);
    }
  }

  public static class HabitatTypeInfoCollector extends HabitatCollector<HabitatTypeInfo> {
    final DBMessagesKey key;

    public HabitatTypeInfoCollector(final DBMessagesKey key) {
      this.key = key;
    }

    @Override
    public HabitatTypeInfo fillEntity(final ResultSet rs) throws SQLException {
      return setHabitatTypeFromResultSet(rs, new HabitatTypeInfo(), key);
    }

    @Override
    public void appendEntity(final HabitatTypeInfo entity, final ResultSet rs) throws SQLException {
      EmissionResultRepositoryUtil.addCriticalLevel(rs, entity.getCriticalLevels());
    }
  }

  public static class HabitatTypeExtendedInfoCollector extends HabitatTypeInfoCollector {

    public HabitatTypeExtendedInfoCollector(final DBMessagesKey key) {
      super(key);
    }

    @Override
    public HabitatTypeInfo fillEntity(final ResultSet rs) throws SQLException {
      final HabitatTypeInfo habitatInfo = new HabitatTypeInfo();
      setHabitatTypeFromResultSet(rs, habitatInfo, key);
      setGoalHabitatInfoFromResultSet(rs, habitatInfo);
      return habitatInfo;
    }
  }

  public static class HabitatAreaInfoCollector extends HabitatCollector<HabitatAreaInfo> {
    final DBMessagesKey key;

    public HabitatAreaInfoCollector(final DBMessagesKey key) {
      this.key = key;
    }

    @Override
    public HabitatAreaInfo fillEntity(final ResultSet rs) throws SQLException {
      return setHabitatAreaInfoFromResultSet(rs, new HabitatAreaInfo(), key);
    }

    @Override
    public void appendEntity(final HabitatAreaInfo entity, final ResultSet rs) throws SQLException {
      EmissionResultRepositoryUtil.addCriticalLevel(rs, entity.getCriticalLevels());
    }
  }

  public static class HabitatAreaExtendedInfoCollector extends HabitatAreaInfoCollector {

    public HabitatAreaExtendedInfoCollector(final DBMessagesKey key) {
      super(key);
    }

    @Override
    public HabitatAreaInfo fillEntity(final ResultSet rs) throws SQLException {
      final HabitatAreaInfo habitatInfo = new HabitatAreaInfo();
      setHabitatAreaInfoFromResultSet(rs, habitatInfo, key);
      setGoalHabitatInfoFromResultSet(rs, habitatInfo);
      habitatInfo.setAdditionalSurface(HabitatSurfaceType.TOTAL_MAPPED, QueryAttribute.SURFACE.getDouble(rs));
      habitatInfo.setAdditionalSurface(HabitatSurfaceType.TOTAL_CARTOGRAPHIC, QueryUtil.getDouble(rs, QueryAttribute.CARTOGRAPHIC_SURFACE));
      habitatInfo.setAdditionalSurface(HabitatSurfaceType.RELEVANT_MAPPED, QueryUtil.getDouble(rs, QueryAttribute.RELEVANT_SURFACE));
      habitatInfo.setAdditionalSurface(HabitatSurfaceType.RELEVANT_CARTOGRAPHIC,
          QueryUtil.getDouble(rs, QueryAttribute.RELEVANT_CARTOGRAPHIC_SURFACE));
      return habitatInfo;
    }

    @Override
    public void appendEntity(final HabitatAreaInfo entity, final ResultSet rs) throws SQLException {
      EmissionResultRepositoryUtil.addCriticalLevel(rs, entity.getCriticalLevels());
    }
  }

  // Collectors - helpers

  private static <T extends HabitatType> T setHabitatTypeFromResultSet(final ResultSet rs, final T habitatType, final DBMessagesKey key)
      throws SQLException {
    habitatType.setId(QueryAttribute.HABITAT_TYPE_ID.getInt(rs));
    DBMessages.setHabitatTypeMessages(habitatType, key);
    return habitatType;
  }

  private static <T extends HabitatAreaInfo> T setHabitatAreaInfoFromResultSet(final ResultSet rs,
      final T habitatInfo, final DBMessagesKey key) throws SQLException {
    setHabitatTypeFromResultSet(rs, habitatInfo, key);
    habitatInfo.setCartographicSurface(QueryAttribute.CARTOGRAPHIC_SURFACE.getDouble(rs));

    return habitatInfo;
  }

  private static <T extends HabitatTypeInfo> T setGoalHabitatInfoFromResultSet(final ResultSet rs,
      final T habitatInfo) throws SQLException {
    habitatInfo.setQualityGoal(QueryUtil.getEnum(rs, QueryAttribute.QUALITY_GOAL, HabitatGoal.class));
    habitatInfo.setExtentGoal(QueryUtil.getEnum(rs, QueryAttribute.EXTENT_GOAL, HabitatGoal.class));

    return habitatInfo;
  }

}
