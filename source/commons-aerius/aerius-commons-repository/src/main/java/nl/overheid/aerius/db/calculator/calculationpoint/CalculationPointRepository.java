/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator.calculationpoint;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.locationtech.jts.io.WKTWriter;
import org.postgis.Geometry;
import org.postgis.GeometryCollection;
import org.postgis.PGgeometry;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.geo.PGisUtils;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.info.AuthorityType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Service class for Calculation Point queries
 */
public final class CalculationPointRepository {

  private static final Query QUERY_CALCULATION_POINTS_ASSESSMENT_AREA = QueryBuilder.from("ae_assessment_area_points_of_interest_by_source_collection(?, ?, ?::authority_type[])",
      QueryAttribute.GEOMETRY, QueryAttribute.DISTANCE, QueryAttribute.AUTHORITY_TYPE)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.ASSESSMENT_AREA_NAME,
          QueryAttribute.DISTANCE, QueryAttribute.X_COORD, QueryAttribute.Y_COORD)
      .getQuery();

  private static final Query QUERY_CALCULATION_POINTS_RELEVANT_HABITAT = QueryBuilder.from("ae_relevant_habitat_points_of_interest_by_source_collection(?, ?, ?::authority_type[])",
          QueryAttribute.GEOMETRY, QueryAttribute.DISTANCE, QueryAttribute.AUTHORITY_TYPE)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.HABITAT_TYPE_ID, QueryAttribute.ASSESSMENT_AREA_NAME, QueryAttribute.HABITAT_NAME,
          QueryAttribute.DISTANCE, QueryAttribute.X_COORD, QueryAttribute.Y_COORD)
      .getQuery();

  private CalculationPointRepository() {
  }

  /**
   * Determine calculation points based habitats in the interior
   *
   * @param con Database connection
   * @param collector Results collector
   * @param sourceList The source list(s) to determine calculation points for
   * @param distanceFromSource the distance from the sources (radius)
   */
  public static void determinePointsInlandHabitats(final Connection con, final DeterminedCalculationPointsCollector collector,
      final List<EmissionSourceFeature> sourceList, final int distanceFromSource) throws SQLException, AeriusException {
    determinePoints(con, collector, sourceList, distanceFromSource, QUERY_CALCULATION_POINTS_RELEVANT_HABITAT, Set.of(AuthorityType.MINISTRY,
        AuthorityType.PROVINCE, AuthorityType.UNKNOWN));
  }

  /**
   * Determine calculation points based assessment area's in the interior
   *
   * @param con Database connection
   * @param collector Results collector
   * @param sourceList The source list(s) to determine calculation points for
   * @param distanceFromSource the distance from the sources (radius)
   */
  public static void determinePointsInlandAssessmentAreas(final Connection con, final DeterminedCalculationPointsCollector collector,
      final List<EmissionSourceFeature> sourceList, final int distanceFromSource) throws SQLException, AeriusException {
    determinePoints(con, collector, sourceList, distanceFromSource, QUERY_CALCULATION_POINTS_ASSESSMENT_AREA, Set.of(AuthorityType.MINISTRY,
        AuthorityType.PROVINCE, AuthorityType.UNKNOWN));
  }

  /**
   * Determine calculation points based assessment area's abroad
   *
   * @param con Database connection
   * @param collector Results collector
   * @param sourceList The source list(s) to determine calculation points for
   */
  public static void determinePointsAbroad(final Connection con, final DeterminedCalculationPointsCollector collector,
      final List<EmissionSourceFeature> sourceList) throws SQLException, AeriusException {
    final int radius = ConstantRepository.getInteger(con, SharedConstantsEnum.DEFAULT_CALCULATION_POINTS_PLACEMENT_RADIUS);
    determinePoints(con, collector, sourceList, radius, QUERY_CALCULATION_POINTS_ASSESSMENT_AREA, Set.of(AuthorityType.FOREIGN));
  }

  /**
   * Determine some calculation points based on nature-area's or habitats.
   *
   * @param con                Database connection
   * @param collector          Results collector
   * @param sourceList         The source list(s) to determine calculation points for
   * @param distanceFromSource the distance from the sources
   * @param query              the query to use
   * @param abroad             whether to search for areas abroad
   */
  private static void determinePoints(final Connection con, final DeterminedCalculationPointsCollector collector,
       final List<EmissionSourceFeature> sourceList, final int distanceFromSource, final Query query, final Set<AuthorityType> authorityTypes)
      throws SQLException, AeriusException {
    final int srid = ReceptorGridSettingsRepository.getSrid(con);
    final GeometryCollection coll = extractGeometries(sourceList, srid);

    if (coll != null) {
      try (final PreparedStatement stmt = con.prepareStatement(query.get())) {
        query.setParameter(stmt, QueryAttribute.AUTHORITY_TYPE, authorityTypes.stream().map(at -> at.name().toLowerCase(Locale.ROOT)).toArray(String[]::new));
        query.setParameter(stmt, QueryAttribute.GEOMETRY, new PGgeometry(coll));
        query.setParameter(stmt, QueryAttribute.DISTANCE, distanceFromSource);
        final ResultSet rst = stmt.executeQuery();

        while (rst.next()) {
          final int xPos = QueryAttribute.X_COORD.getInt(rst);
          final int yPos = QueryAttribute.Y_COORD.getInt(rst);
          final double distance = QueryAttribute.DISTANCE.getDouble(rst);
          final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rst);

          String label = QueryAttribute.ASSESSMENT_AREA_NAME.getString(rst);
          if (query == QUERY_CALCULATION_POINTS_RELEVANT_HABITAT) {
            label += " " + QueryAttribute.HABITAT_NAME.getString(rst);
          }

          collector.add(xPos, yPos, assessmentAreaId, distance, label);
        }
      }
    }

  }

  private static GeometryCollection extractGeometries(final List<EmissionSourceFeature> sourceLists, final int srid) throws AeriusException {
    final List<Geometry> geometries = new ArrayList<>();
    final WKTWriter wktWriter = new WKTWriter();

    for (final EmissionSourceFeature source : sourceLists) {
      geometries.add(PGisUtils.getGeometry(wktWriter.write(GeometryUtil.getGeometry(source.getGeometry()))));
    }

    if (geometries.isEmpty()) {
      return null;
    } else {
      final GeometryCollection coll = new GeometryCollection(geometries.toArray(new Geometry[geometries.size()]));
      coll.setSrid(srid);
      return coll;
    }
  }

}
