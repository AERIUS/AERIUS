/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.geo;

import java.sql.SQLException;

import org.postgis.GeometryBuilder;
import org.postgis.PGbox2d;
import org.postgis.PGgeometry;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;

/**
 * Utility for PostGIS conversion to shared object that can be serialized to
 * the client and back.
 */
public final class PGisUtils {

  private PGisUtils() {
  }

  /**
   * Returns a {@link Point} from {@link PGgeometry} data.
   *
   * @param pgg {@link PGgeometry} point to return
   * @return The converted point.
   */
  public static Point getPoint(final PGgeometry pgg) {
    if (pgg == null) {
      return null;
    }
    final org.postgis.Point p = pgg.getGeometry().getFirstPoint();
    return convertToAeriusPoint(p);
  }

  /**
   * Convert a PG geometry (used by database) to an AERIUS specific geometry (usable with client).
   * @param geom The PG geometry to convert.
   * @return The geometry that's actually in the PG Geometry.
   */
  public static WKTGeometry getGeometry(final PGgeometry geom) {
    if (geom == null) {
      return null;
    }
    final WKTGeometry geometry;

    switch (geom.getGeoType()) {
      case org.postgis.Geometry.POINT:
      case org.postgis.Geometry.POLYGON:
      case org.postgis.Geometry.MULTIPOLYGON:
      case org.postgis.Geometry.LINESTRING:
      case org.postgis.Geometry.MULTILINESTRING:
      case org.postgis.Geometry.MULTIPOINT:
        final StringBuffer stringBuffer = new StringBuffer();
        geom.getGeometry().outerWKT(stringBuffer);
        geometry = new WKTGeometry(stringBuffer.toString());
        break;
      default:
        throw new IllegalArgumentException("Geometry type not yet implemented...");
    }

    return geometry;
  }

  private static Point convertToAeriusPoint(final org.postgis.Point gisPoint) {
    return new Point(gisPoint.getX(), gisPoint.getY());
  }

  /**
   * Returns a {@link BBox} from {@link PGbox2d} data. Note that the wkt value is not set.
   *
   * @param b {@link PGbox2d} box to return
   * @return The converted bounding box.
   */
  public static BBox getBox(final PGbox2d b) {
    return b == null ? null : new BBox(b.getLLB().x, b.getLLB().y, b.getURT().x, b.getURT().y);
  }

  /**
   * Return the PostGIS Geometry for the given WKT string.
   * @param wkt The WKT to use.
   * @return geometry
   */
  public static org.postgis.Geometry getGeometry(final String wkt) {
    try {
      return GeometryBuilder.geomFromString(wkt);
    } catch (final SQLException e) {
      throw new IllegalArgumentException(e);
    }
  }

  /**
   * Return the PostGIS PGgeometry - suitable for writing to the DB - for the given WKT string.
   * @param wkt The WKT to use.
   * @return geometry
   */
  public static PGgeometry getPGgeometry(final String wkt) {
    try {
      return new PGgeometry(wkt);
    } catch (final SQLException e) {
      throw new IllegalArgumentException(e);
    }
  }

}
