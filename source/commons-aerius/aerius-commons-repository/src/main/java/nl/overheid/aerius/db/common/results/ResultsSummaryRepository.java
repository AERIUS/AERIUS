/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.DoubleSummaryStatistics;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.summary.CustomCalculationPointResult;
import nl.overheid.aerius.shared.domain.summary.JobSummary;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatisticReceptors;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryRequest;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

public class ResultsSummaryRepository {

  private static final Logger LOG = LoggerFactory.getLogger(ResultsSummaryRepository.class);

  private static final String ANALYZE_PREPROCESSED = "SELECT ae_analyze_calculation_related()";

  private final Map<ScenarioResultType, ResultsSummaryImpl<?>> availableSummaryTypes = new EnumMap<>(ScenarioResultType.class);

  private final PMF pmf;
  private final JobResultsSummaryRepository jobSummaryRepository;

  /**
   * Only use this constructor if you're certain the repository won't be used to retrieve information.
   */
  public ResultsSummaryRepository(final PMF pmf) {
    this(pmf, null);
  }

  public ResultsSummaryRepository(final PMF pmf, final ReceptorUtil receptorUtil) {
    this.pmf = pmf;
    availableSummaryTypes.put(ScenarioResultType.SITUATION_RESULT, new SituationResultsSummaryImpl(pmf, receptorUtil));
    availableSummaryTypes.put(ScenarioResultType.PROJECT_CALCULATION, new ProjectCalculationResultsSummaryImpl(pmf, receptorUtil));
    availableSummaryTypes.put(ScenarioResultType.MAX_TEMPORARY_EFFECT, new TempEffectResultsSummaryImpl(pmf, receptorUtil));
    availableSummaryTypes.put(ScenarioResultType.IN_COMBINATION, new InCombinationResultsSummaryImpl(pmf, receptorUtil));
    availableSummaryTypes.put(ScenarioResultType.ARCHIVE_CONTRIBUTION, new ArchiveContributionResultsSummaryImpl(pmf, receptorUtil));
    this.jobSummaryRepository = new JobResultsSummaryRepository(pmf);
  }

  public SituationResultsSummary determineReceptorResultsSummary(final SituationCalculations situationCalculations,
      final SummaryRequest summaryRequest, final AreaInformationSupplier areaInformationSupplier) throws AeriusException {

    final ResultsSummaryImpl<?> resultsSummaryImpl = availableSummaryTypes.get(summaryRequest.getResultType());

    if (resultsSummaryImpl == null) {
      return null;
    } else {
      return resultsSummaryImpl.determineReceptorResultsSummary(situationCalculations, summaryRequest, areaInformationSupplier);
    }
  }

  public SituationResultsSummary determineCustomCalculationPointResultsSummary(final SituationCalculations situationCalculations,
      final SummaryRequest summaryRequest) throws AeriusException {
    final List<CustomCalculationPointResult> customPointResults = determineCustomCalculationPointResults(
        situationCalculations, summaryRequest);
    final SituationResultsStatistics statistics = determineStatistics(customPointResults, summaryRequest.getResultType());

    return new SituationResultsSummary(statistics, List.of(), List.of(), new SituationResultsStatisticReceptors(), customPointResults);
  }

  public JobSummary determineJobSummary(final int jobId, final AreaInformationSupplier areaInformationSupplier) throws AeriusException {
    return jobSummaryRepository.determineJobSummary(jobId, areaInformationSupplier);
  }

  private List<CustomCalculationPointResult> determineCustomCalculationPointResults(final SituationCalculations situationCalculations,
      final SummaryRequest summaryRequest) throws AeriusException {

    final ResultsSummaryImpl<?> resultsSummaryImpl = availableSummaryTypes.get(summaryRequest.getResultType());

    if (resultsSummaryImpl == null) {
      return List.of();
    } else {
      return resultsSummaryImpl.determineCustomCalculationPointResults(situationCalculations, summaryRequest);
    }
  }

  private static SituationResultsStatistics determineStatistics(final List<CustomCalculationPointResult> customPointResults,
      final ScenarioResultType resultType) {
    final SituationResultsStatistics statistics = new SituationResultsStatistics();
    statistics.put(ResultStatisticType.COUNT_CALCULATION_POINTS, Double.valueOf(customPointResults.size()));

    switch (resultType) {
    case SITUATION_RESULT:
      addSituationStatistics(statistics, customPointResults);
      break;
    case PROJECT_CALCULATION:
      addProjectStatistics(statistics, customPointResults);
      break;
    case MAX_TEMPORARY_EFFECT:
      addTemporaryEffectStatistics(statistics, customPointResults);
      break;
    case IN_COMBINATION:
      addProjectStatistics(statistics, customPointResults);
      break;
    // Cases where no custom points are expected
    case DEPOSITION_SUM:
    case ARCHIVE_CONTRIBUTION:
      break;
    }

    return statistics;
  }

  private static void addSituationStatistics(final SituationResultsStatistics statistics,
      final List<CustomCalculationPointResult> customPointResults) {
    statistics.put(ResultStatisticType.MAX_CONTRIBUTION, customPointResults.stream()
        .filter(CustomCalculationPointResult::hasResult)
        .mapToDouble(CustomCalculationPointResult::getResult)
        .max()
        .orElse(0));
  }

  private static void addProjectStatistics(final SituationResultsStatistics statistics,
      final List<CustomCalculationPointResult> customPointResults) {
    final DoubleSummaryStatistics increasingResults = customPointResults.stream()
        .filter(CustomCalculationPointResult::hasResult)
        .mapToDouble(CustomCalculationPointResult::getResult)
        .filter(x -> x > 0)
        .summaryStatistics();
    statistics.put(ResultStatisticType.COUNT_CALCULATION_POINTS_INCREASE, Double.valueOf(increasingResults.getCount()));
    statistics.put(ResultStatisticType.MAX_INCREASE, increasingResults.getCount() > 0 ? increasingResults.getMax() : 0);
    final DoubleSummaryStatistics decreasingResults = customPointResults.stream()
        .filter(CustomCalculationPointResult::hasResult)
        .mapToDouble(CustomCalculationPointResult::getResult)
        .filter(x -> x < 0)
        .summaryStatistics();
    statistics.put(ResultStatisticType.COUNT_CALCULATION_POINTS_DECREASE, Double.valueOf(decreasingResults.getCount()));
    // the 'highest decrease' is the lowest value in the range, therefore use getMin() instead of getMax()
    statistics.put(ResultStatisticType.MAX_DECREASE, decreasingResults.getCount() > 0 ? decreasingResults.getMin() : 0);
  }

  private static void addTemporaryEffectStatistics(final SituationResultsStatistics statistics,
      final List<CustomCalculationPointResult> customPointResults) {
    statistics.put(ResultStatisticType.MAX_TEMP_INCREASE, customPointResults.stream()
        .filter(CustomCalculationPointResult::hasResult)
        .mapToDouble(CustomCalculationPointResult::getResult)
        .max()
        .orElse(0));
  }

  public void insertResultsSummaries(final int jobId, final SituationCalculations situationCalculations,
      final CalculationJobType jobType) throws AeriusException {
    final List<ScenarioResultType> resultTypes = ScenarioResultType.getResultTypesForJobType(jobType);
    prepareForSummaryResults(jobId, situationCalculations, resultTypes);
    processResultSummaries(jobId, situationCalculations, resultTypes);
  }

  private void prepareForSummaryResults(final int jobId, final SituationCalculations situationCalculations,
      final List<ScenarioResultType> resultTypes) throws AeriusException {
    for (final ScenarioResultType resultType : resultTypes) {
      final ResultsSummaryImpl<?> resultsSummaryImpl = availableSummaryTypes.get(resultType);
      if (resultsSummaryImpl != null) {
        resultsSummaryImpl.preprocessForSummary(jobId, situationCalculations);
      }
    }
    try (final Connection con = pmf.getConnection();
        final PreparedStatement stmt = con.prepareStatement(ANALYZE_PREPROCESSED)) {
      stmt.execute();
    } catch (final SQLException e) {
      LOG.error("SQL error while doing some analyzing", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private void processResultSummaries(final int jobId, final SituationCalculations situationCalculations, final List<ScenarioResultType> resultTypes)
      throws AeriusException {
    for (final ScenarioResultType resultType : resultTypes) {
      final ResultsSummaryImpl<?> resultsSummaryImpl = availableSummaryTypes.get(resultType);
      if (resultsSummaryImpl != null) {
        resultsSummaryImpl.insertResultsSummaries(jobId, situationCalculations);
      }
    }
  }

}
