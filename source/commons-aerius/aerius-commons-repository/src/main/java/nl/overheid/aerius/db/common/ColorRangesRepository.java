/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.summary.ResultTypeColorRangeTypes;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.util.ColorUtil;

public final class ColorRangesRepository {

  private enum RepositoryAttribute implements Attribute {
    COLOR_RANGE_TYPE,
    LOWER_VALUE,
    COLOR,

    SCENARIO_RESULT_TYPE,
    EMISSION_RESULT_TYPE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private static final Attributes COLOR_RANGE_ATTRIBUTES = new Attributes(
      RepositoryAttribute.COLOR_RANGE_TYPE,
      RepositoryAttribute.LOWER_VALUE,
      RepositoryAttribute.COLOR);

  private static final Query COLOR_RANGES_QUERY = QueryBuilder.from("system.color_ranges")
      .select(COLOR_RANGE_ATTRIBUTES)
      .orderBy(RepositoryAttribute.COLOR_RANGE_TYPE, RepositoryAttribute.LOWER_VALUE)
      .getQuery();

  private static final Query RESULT_TYPE_COLOR_RANGE_TYPES_QUERY = QueryBuilder.from("scenario_result_types_color_ranges")
      .select(RepositoryAttribute.SCENARIO_RESULT_TYPE, RepositoryAttribute.EMISSION_RESULT_TYPE, QueryAttribute.SUBSTANCE_ID,
          RepositoryAttribute.COLOR_RANGE_TYPE)
      .getQuery();

  /**
   * Retrieves all available color ranges from the database.
   *
   * @param con db connection
   * @return Map with ColorRangeType to the content of the color range
   * @throws SQLException
   */
  public static Map<ColorRangeType, List<ColorRange>> getColorRanges(final Connection con) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(COLOR_RANGES_QUERY.get())) {

      final ResultSet rs = stmt.executeQuery();
      final Map<ColorRangeType, List<ColorRange>> result = new HashMap<>();

      while (rs.next()) {
        final ColorRangeType colorRangeType = QueryUtil.getEnum(rs, RepositoryAttribute.COLOR_RANGE_TYPE, ColorRangeType.class);
        result.putIfAbsent(colorRangeType, new ArrayList<>());

        final ColorRange colorRange = new ColorRange(
            QueryUtil.getDouble(rs, RepositoryAttribute.LOWER_VALUE),
            ColorUtil.webColor(QueryUtil.getString(rs, RepositoryAttribute.COLOR)));
        result.get(colorRangeType).add(colorRange);
      }

      return result;
    }
  }

  /**
   * Retrieves color range types for result types.
   *
   * @param con db connection
   * @return ResultTypeColorRanges with the defined color range types
   * @throws SQLException
   */
  public static ResultTypeColorRangeTypes getResultTypeColorRangeTypes(final Connection con) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(RESULT_TYPE_COLOR_RANGE_TYPES_QUERY.get())) {

      final ResultSet rs = stmt.executeQuery();
      final ResultTypeColorRangeTypes result = new ResultTypeColorRangeTypes();

      while (rs.next()) {
        final ScenarioResultType scenarioResultType = QueryUtil.getEnum(rs, RepositoryAttribute.SCENARIO_RESULT_TYPE, ScenarioResultType.class);
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final EmissionResultType emissionResultType = QueryUtil.getEnum(rs, RepositoryAttribute.EMISSION_RESULT_TYPE, EmissionResultType.class);
        final EmissionResultKey emissionResultKey = EmissionResultKey.valueOf(substance, emissionResultType);
        final ColorRangeType colorRangeType = QueryUtil.getEnum(rs, RepositoryAttribute.COLOR_RANGE_TYPE, ColorRangeType.class);
        result.add(scenarioResultType, emissionResultKey, colorRangeType);
      }

      return result;
    }
  }

}
