/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.postgis.PGbox2d;

import nl.overheid.aerius.db.common.HabitatRepository.HabitatAreaInfoCollector;
import nl.overheid.aerius.db.common.HabitatRepository.HabitatTypeInfoCollector;
import nl.overheid.aerius.db.geo.PGisUtils;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.DefaultWhereClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.info.HabitatAreaInfo;
import nl.overheid.aerius.shared.domain.info.HabitatTypeInfo;
import nl.overheid.aerius.shared.domain.info.Natura2000Info;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;

/**
 * Service class for info related database data.
 */
public final class ReceptorInfoRepository {

  private enum RepositoryAttribute implements Attribute {

    NATURA2000_AREA_ID,
    ECOLOGY_QUALITY_TYPE,
    LANDSCAPE_TYPE,
    DIRECTIVE,
    DESIGN_STATUS_DESCRIPTION,

    ESTABLISHED_DATE;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ROOT);
    }

  }

  private static final Attributes BACKGROUND_RESULTS_ATTRIBUTES = new Attributes(
      QueryAttribute.SUBSTANCE_ID, QueryAttribute.EMISSION_RESULT_TYPE, QueryAttribute.RESULT);

  private static final Query SELECT_HABITAT_AREA_IN_RECEPTOR_INFO = QueryBuilder.from("relevant_habitat_info_for_receptor_view")
      .select(QueryAttribute.HABITAT_TYPE_ID, QueryAttribute.CRITICAL_LEVEL, QueryAttribute.SUBSTANCE_ID, QueryAttribute.RESULT_TYPE,
          QueryAttribute.CARTOGRAPHIC_SURFACE)
      .where(QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query SELECT_EXTRA_ASSESSMENT_HABITATS_IN_RECEPTOR_INFO = QueryBuilder.from("extra_assessment_habitat_info_for_receptor_view")
      .select(QueryAttribute.HABITAT_TYPE_ID, QueryAttribute.CRITICAL_LEVEL, QueryAttribute.SUBSTANCE_ID, QueryAttribute.RESULT_TYPE)
      .where(QueryAttribute.RECEPTOR_ID)
      .orderBy(QueryAttribute.HABITAT_TYPE_ID).getQuery();

  private static final Query SELECT_HEXAGON_TYPES_IN_RECEPTOR_INFO = QueryBuilder.from("hexagon_type_receptors")
      .select(QueryAttribute.RECEPTOR_ID, QueryAttribute.HEXAGON_TYPE)
      .where(QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query SELECT_NATURA2000_AREA_INFO = QueryBuilder.from("natura2000_area_info_view")
      .join(new JoinClause("receptors_to_assessment_areas", QueryAttribute.ASSESSMENT_AREA_ID))
      .where(QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query SELECT_BACKGROUND_EMISSION_RESULTS = QueryBuilder.from("background_cell_results_view")
      .select(BACKGROUND_RESULTS_ATTRIBUTES)
      .where(QueryAttribute.YEAR).where(new StaticWhereClause("ST_Within(ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()), geometry)",
          QueryAttribute.X_COORD, QueryAttribute.Y_COORD))
      .getQuery();

  private static final Query SELECT_BACKGROUND_EMISSION_RESULTS_BY_ERK = QueryBuilder.from("background_cell_results_view")
      .select(BACKGROUND_RESULTS_ATTRIBUTES)
      .where(QueryAttribute.YEAR, QueryAttribute.SUBSTANCE_ID)
      .where(new DefaultWhereClause(QueryAttribute.EMISSION_RESULT_TYPE, "emission_result_type"))
      .where(new StaticWhereClause("ST_Within(ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()), geometry)",
          QueryAttribute.X_COORD, QueryAttribute.Y_COORD))
      .getQuery();

  private static final Query SELECT_RECEPTOR_BACKGROUND_EMISSION_RESULTS = QueryBuilder.from("receptor_background_results_view")
      .select(BACKGROUND_RESULTS_ATTRIBUTES)
      .where(QueryAttribute.YEAR, QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query SELECT_RECEPTOR_BACKGROUND_EMISSION_RESULTS_BY_ERK = QueryBuilder.from("receptor_background_results_view")
      .select(BACKGROUND_RESULTS_ATTRIBUTES)
      .where(QueryAttribute.YEAR, QueryAttribute.RECEPTOR_ID, QueryAttribute.SUBSTANCE_ID)
      .where(new DefaultWhereClause(QueryAttribute.EMISSION_RESULT_TYPE, "emission_result_type"))
      .getQuery();

  private static final double BACKUP_POINT_COORD_TRANSLATION = 0.000000001;

  // Not allowed to instantiate.
  private ReceptorInfoRepository() {
  }

  /**
   * Get the information about natura 2000 areas that cover at least part of a specific receptor (hexagon).
   *
   * @param con The connection to use for the DB-query.
   * @param receptorId The receptor ID to get the information for. Information retrieved based on ID.
   * @return The list of natura2000 areas that insersect with the hexagon that goes with the receptor.
   * @throws SQLException When an error occurs executing the query.
   */
  public static List<Natura2000Info> getBasicNaturaAreaInfo(final Connection con, final int receptorId) throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(SELECT_NATURA2000_AREA_INFO.get())) {
      SELECT_NATURA2000_AREA_INFO.setParameter(statement, QueryAttribute.RECEPTOR_ID, receptorId);
      final ResultSet rs = statement.executeQuery();
      final ArrayList<Natura2000Info> lst = new ArrayList<>();
      while (rs.next()) {
        lst.add(getNatura2000Info(rs));
      }
      return lst;
    }
  }

  /**
   * Get the information about natura 2000 areas that cover at least part of a specific receptor (hexagon).
   * Includes habitat info in the assessment area.
   *
   * @param con The connection to use for the DB-query.
   * @param receptorId The receptor ID to get the information for.
   * @return The list of natura2000 areas that insersect with the hexagon that goes with the receptor.
   * @throws SQLException When an error occurs executing the query.
   */
  public static List<Natura2000Info> getNaturaAreaInfo(final Connection con, final int receptorId, final DBMessagesKey key)
      throws SQLException {
    final List<Natura2000Info> lst = getBasicNaturaAreaInfo(con, receptorId);
    for (final Natura2000Info info : lst) {
      info.setHabitats(HabitatRepository.getRelevantHabitatInfo(con, info.getId(), key));
      info.setExtraAssessmentHabitats(HabitatRepository.getExtraAssessmentHabitatInfo(con, info.getId(), key));
    }

    return lst;
  }

  /**
   * Get information about all habitat types that are inside a specific receptor (hexagon).
   *
   * @param con The connection to use for the DB-query.
   * @param receptorId The receptor ID to get the information for.
   * @return The list of all habitat types that are inside the hexagon that goes with the receptor.
   * @throws SQLException When an error occurs executing the query.
   */
  public static List<HabitatAreaInfo> getReceptorHabitatAreaInfo(final Connection con, final int receptorId, final DBMessagesKey key)
      throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(SELECT_HABITAT_AREA_IN_RECEPTOR_INFO.get())) {
      SELECT_HABITAT_AREA_IN_RECEPTOR_INFO.setParameter(statement, QueryAttribute.RECEPTOR_ID, receptorId);
      final ResultSet rs = statement.executeQuery();

      final HabitatAreaInfoCollector habitatInfoCollector = new HabitatAreaInfoCollector(key);
      habitatInfoCollector.collectEntities(rs);
      return habitatInfoCollector.getEntities();
    }
  }

  /**
   * Get information about all extra assessment habitat types that are inside a specific receptor (hexagon).
   *
   * @param con The connection to use for the DB-query.
   * @param receptorId The receptor ID to get the information for.
   * @return The list of all habitat types specified for extra assessment for the hexagon that goes with the receptor.
   * @throws SQLException When an error occurs executing the query.
   */
  public static List<HabitatTypeInfo> getReceptorExtraAssessmentHabitatsInfo(final Connection con, final int receptorId, final DBMessagesKey key)
      throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(SELECT_EXTRA_ASSESSMENT_HABITATS_IN_RECEPTOR_INFO.get())) {
      SELECT_EXTRA_ASSESSMENT_HABITATS_IN_RECEPTOR_INFO.setParameter(statement, QueryAttribute.RECEPTOR_ID, receptorId);
      final ResultSet rs = statement.executeQuery();

      final HabitatTypeInfoCollector habitatInfoCollector = new HabitatTypeInfoCollector(key);
      habitatInfoCollector.collectEntities(rs);
      return habitatInfoCollector.getEntities();
    }
  }

  /**
   * Get information about all summary hexagon types for a specific receptor.
   *
   * @param con The connection to use for the DB-query.
   * @param receptorId The receptor ID to get the information for.
   * @return The list of all summary hexagon types for the receptor.
   * @throws SQLException When an error occurs executing the query.
   */
  public static List<SummaryHexagonType> getHexagonTypes(final Connection con, final int receptorId) throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(SELECT_HEXAGON_TYPES_IN_RECEPTOR_INFO.get())) {
      SELECT_HEXAGON_TYPES_IN_RECEPTOR_INFO.setParameter(statement, QueryAttribute.RECEPTOR_ID, receptorId);
      final ResultSet rs = statement.executeQuery();

      final List<SummaryHexagonType> hexagonTypes = new ArrayList<>();
      while (rs.next()) {
        hexagonTypes.add(QueryUtil.getEnum(rs, QueryAttribute.HEXAGON_TYPE, SummaryHexagonType.class));
      }
      return hexagonTypes;
    }
  }

  /**
   * Get the background concentration and deposition information for a specific receptor.
   *
   * I can see you wondering; why in the name of the great oozelords would this code execute 2 queries to achieve the (seemingly) same thing?
   *
   * Well, the answer is simple.
   * The former is a general query for all background concentrations and depositions at a given point.
   * The latter is a specific query for (improved) background depositions inside a Natura 2000 area.
   * If the second query finds such a deposition, it will overwrite the deposition from the first query.
   *
   * @param con The connection to use for the DB-query.
   * @param point The point to get the info for.
   * @param receptorId The receptor to get the info for.
   * @param year The year to get the info for.
   * @return The deposition info for the specified receptor/version/year combination.
   * @throws SQLException When an error occurs executing the query.
   */
  public static EmissionResults getBackgroundEmissionResult(final Connection con, final Point point, final int receptorId, final int year)
      throws SQLException {
    final EmissionResults emissionResults = new EmissionResults();
    setBackgroundEmissionResultPoint(con, SELECT_BACKGROUND_EMISSION_RESULTS, point, year, null, emissionResults);
    // If no results, try once with shifting the point slightly (in case of edge artifacts)
    // This should work with both square and hexagon background cells.
    if (emissionResults.isEmpty()) {
      setBackgroundEmissionResultPoint(con, SELECT_BACKGROUND_EMISSION_RESULTS, translatePointSlightly(point), year, null, emissionResults);
    }
    // Override (improved) background depositions inside Natura 2000 areas.
    setBackgroundEmissionResultInN2K(con, SELECT_RECEPTOR_BACKGROUND_EMISSION_RESULTS, receptorId, year, null, emissionResults);
    return emissionResults;
  }

  /**
   * Get the background information for a specific emission result type for a specific receptor.
   * Note: It first queries the receptor id related to the point and if no results are found it queries in the background data.
   * Since background data in general is in square surfaces this might give a different result than when queried only in the background.
   * Because if the receptor center is in a different surface as the point the result will be of the receptor and not the actual
   * surface the point is in.
   *
   * @param con The connection to use for the DB-query.
   * @param point The point to get the info for.
   * @param receptorId The receptor to get the info for.
   * @param year The year to get the info for.
   * @param erk the specific emission result type to get the info for
   * @return The emission result for the specified point/version/year/emission_result_type combination.
   * @throws SQLException When an error occurs executing the query.
   */
  public static EmissionResults getBackgroundEmissionResultERK(final Connection con, final Point point, final int receptorId, final int year,
      final EmissionResultKey erk) throws SQLException {
    final EmissionResults emissionResults = new EmissionResults();
    setBackgroundEmissionResultInN2K(con, SELECT_RECEPTOR_BACKGROUND_EMISSION_RESULTS_BY_ERK, receptorId, year, erk, emissionResults);

    if (!emissionResults.hasResult(erk)) {
      setBackgroundEmissionResultPoint(con, SELECT_BACKGROUND_EMISSION_RESULTS_BY_ERK, point, year, erk, emissionResults);
    }
    if (!emissionResults.hasResult(erk)) {
      setBackgroundEmissionResultPoint(con, SELECT_BACKGROUND_EMISSION_RESULTS_BY_ERK, translatePointSlightly(point), year, erk, emissionResults);
    }

    return emissionResults;
  }

  private static void setBackgroundEmissionResultPoint(final Connection con, final Query query, final Point point, final int year,
      final EmissionResultKey erk, final EmissionResults emissionResults) throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(query.get())) {
      query.setParameter(statement, QueryAttribute.YEAR, year);
      if (erk != null) {
        query.setParameter(statement, QueryAttribute.SUBSTANCE_ID, erk.getSubstance().getId());
        query.setParameter(statement, QueryAttribute.EMISSION_RESULT_TYPE, erk.getEmissionResultType().toDatabaseString());
      }
      query.setParameter(statement, QueryAttribute.X_COORD, point.getX());
      query.setParameter(statement, QueryAttribute.Y_COORD, point.getY());
      addBackgroundEmissionResult(statement, emissionResults);
    }
  }

  private static void setBackgroundEmissionResultInN2K(final Connection con, final Query query, final int receptorId, final int year,
      final EmissionResultKey erk, final EmissionResults emissionResults) throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(query.get())) {
      query.setParameter(statement, QueryAttribute.YEAR, year);
      query.setParameter(statement, QueryAttribute.RECEPTOR_ID, receptorId);
      if (erk != null) {
        query.setParameter(statement, QueryAttribute.SUBSTANCE_ID, erk.getSubstance().getId());
        query.setParameter(statement, QueryAttribute.EMISSION_RESULT_TYPE, erk.getEmissionResultType().toDatabaseString());
      }
      addBackgroundEmissionResult(statement, emissionResults);
    }
  }

  private static void addBackgroundEmissionResult(final PreparedStatement statement, final EmissionResults emissionResults) throws SQLException {
    final ResultSet rs = statement.executeQuery();

    while (rs.next()) {
      final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
      final EmissionResultType resultType = QueryAttribute.EMISSION_RESULT_TYPE.getEnum(rs, EmissionResultType.class);
      final double result = QueryAttribute.RESULT.getDouble(rs);
      final EmissionResultKey key = EmissionResultKey.valueOf(substance, resultType);

      if (key != null) {
        emissionResults.put(key, result);
      }
    }
  }

  private static Point translatePointSlightly(final Point point) {
    return new Point(point.getX() - BACKUP_POINT_COORD_TRANSLATION, point.getY() - BACKUP_POINT_COORD_TRANSLATION);
  }

  /**
   * @param rs The result set containing the natura2000 area information.
   * @return The filled N2K info based on the result set.
   * @throws SQLException In case of errors retrieving properties from the resultset.
   */
  public static Natura2000Info getNatura2000Info(final ResultSet rs) throws SQLException {
    final Natura2000Info info = new Natura2000Info();
    info.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
    info.setCode(QueryAttribute.CODE.getString(rs));
    info.setNatura2000AreaId(QueryUtil.getInt(rs, RepositoryAttribute.NATURA2000_AREA_ID));
    info.setName(QueryAttribute.NAME.getString(rs));
    info.setContractor(QueryAttribute.AUTHORITY.getString(rs));
    info.setDirectiveCodes(QueryAttribute.DIRECTIVE_CODES.getString(rs));
    info.setSurface(Math.round(QueryUtil.getLong(rs, QueryAttribute.SURFACE) / ImaerConstants.M2_TO_HA)); // Surface is in m^2, convert to ha
    info.setProtection(QueryUtil.getString(rs, RepositoryAttribute.DIRECTIVE));
    info.setStatus(QueryUtil.getString(rs, RepositoryAttribute.DESIGN_STATUS_DESCRIPTION));
    info.setBounds(PGisUtils.getBox((PGbox2d) QueryAttribute.BOUNDINGBOX.getObject(rs)));
    return info;
  }

}
