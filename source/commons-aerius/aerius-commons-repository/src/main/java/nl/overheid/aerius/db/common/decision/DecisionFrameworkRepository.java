/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.decision;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.InsertBuilder;
import nl.overheid.aerius.db.util.InsertClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.summary.AssessmentAreaThresholdResults;
import nl.overheid.aerius.shared.domain.summary.ThresholdResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Repository to retrieve decision framework related data.
 */
public class DecisionFrameworkRepository {

  private static final Logger LOG = LoggerFactory.getLogger(DecisionFrameworkRepository.class);

  private enum RepositoryAttribute implements Attribute {
    THRESHOLD,
    FRACTION,
    DEVELOPMENT_PRESSURE_CLASS_ID;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ROOT);
    }
  }

  private static final Query DECISION_MAKING_THRESHOLD_RESULTS = QueryBuilder
      .from("jobs.ae_project_calculation_decision_threshold_table(?,?)", QueryAttribute.JOB_ID, QueryAttribute.PROPOSED_CALCULATION_ID)
      .select(QueryAttribute.RESULT_TYPE, QueryAttribute.SUBSTANCE_ID, QueryAttribute.RESULT, RepositoryAttribute.THRESHOLD,
          RepositoryAttribute.FRACTION)
      .getQuery();
  private static final Query IS_ABOVE_DECISION_MAKING_THRESHOLD = QueryBuilder
      .from("jobs.ae_project_calculation_decision_threshold_table(?,?)", QueryAttribute.JOB_ID, QueryAttribute.PROPOSED_CALCULATION_ID)
      .select(new SelectClause("count(*) > 0", QueryAttribute.RESULT.name()))
      .where(new StaticWhereClause("fraction > 1"))
      .getQuery();

  private static final Query ASSESSMENT_AREA_THRESHOLD_RESULTS = QueryBuilder
      .from("jobs.ae_project_calculation_site_threshold_table(?,?)", QueryAttribute.JOB_ID, QueryAttribute.PROPOSED_CALCULATION_ID)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.RESULT_TYPE, QueryAttribute.SUBSTANCE_ID, QueryAttribute.RESULT,
          RepositoryAttribute.THRESHOLD, RepositoryAttribute.FRACTION)
      .getQuery();

  private static final Query IS_ABOVE_ASSESSMENT_AREA_THRESHOLD = QueryBuilder
      .from("jobs.ae_project_calculation_site_threshold_table(?,?)", QueryAttribute.JOB_ID, QueryAttribute.PROPOSED_CALCULATION_ID)
      .select(new SelectClause("count(*) > 0", QueryAttribute.RESULT.name()))
      .where(new StaticWhereClause("fraction > 1"))
      .getQuery();

  private static final Query INSERT_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB_BY_PROJECTS = InsertBuilder.into("jobs.job_development_pressure_class")
      .insert(QueryAttribute.JOB_ID)
      .insert(new InsertClause(RepositoryAttribute.DEVELOPMENT_PRESSURE_CLASS_ID.attribute(),
          "(SELECT development_pressure_class_id FROM development_pressure_classes WHERE nearby_projects_range @> ?)",
          RepositoryAttribute.DEVELOPMENT_PRESSURE_CLASS_ID))
      .getQuery();

  private static final Query INSERT_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB_BY_CODE = InsertBuilder.into("jobs.job_development_pressure_class")
      .insert(QueryAttribute.JOB_ID)
      .insert(new InsertClause(RepositoryAttribute.DEVELOPMENT_PRESSURE_CLASS_ID.attribute(),
          "(SELECT development_pressure_class_id FROM development_pressure_classes WHERE code = ?)",
          QueryAttribute.CODE))
      .getQuery();

  private static final Query GET_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB = QueryBuilder
      .from("jobs.job_development_pressure_class")
      .join(new JoinClause("development_pressure_classes", RepositoryAttribute.DEVELOPMENT_PRESSURE_CLASS_ID))
      .select(RepositoryAttribute.DEVELOPMENT_PRESSURE_CLASS_ID, QueryAttribute.CODE, QueryAttribute.DESCRIPTION)
      .where(QueryAttribute.JOB_ID)
      .getQuery();

  private final PMF pmf;
  private final AreaInformationSupplier areaInformationSupplier;

  public DecisionFrameworkRepository(final PMF pmf, final AreaInformationSupplier areaInformationSupplier) {
    this.pmf = pmf;
    this.areaInformationSupplier = areaInformationSupplier;
  }

  public Map<EmissionResultKey, ThresholdResult> getDecisionMakingThresholdResults(final int jobId, final int proposedCalculationId)
      throws AeriusException {
    final Map<EmissionResultKey, ThresholdResult> results = new EnumMap<>(EmissionResultKey.class);
    try (final Connection con = pmf.getConnection();
        final PreparedStatement statement = con.prepareStatement(DECISION_MAKING_THRESHOLD_RESULTS.get())) {
      DECISION_MAKING_THRESHOLD_RESULTS.setParameter(statement, QueryAttribute.JOB_ID, jobId);
      DECISION_MAKING_THRESHOLD_RESULTS.setParameter(statement, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final EmissionResultType resultType = EmissionResultType.safeValueOf(QueryAttribute.RESULT_TYPE.getString(rs));
        final EmissionResultKey erk = EmissionResultKey.safeValueOf(substance, resultType);
        results.put(erk, new ThresholdResult(
            QueryAttribute.RESULT.getDouble(rs),
            QueryUtil.getDouble(rs, RepositoryAttribute.THRESHOLD),
            QueryUtil.getDouble(rs, RepositoryAttribute.FRACTION)));
      }
    } catch (final SQLException e) {
      LOG.error("SQL error while retrieving decision making threshold results for job {} and calculation {}", jobId, proposedCalculationId, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return results;
  }

  public boolean isAboveDecisionMakingThreshold(final int jobId, final int proposedCalculationId)
      throws AeriusException {
    boolean aboveThreshold = false;
    try (final Connection con = pmf.getConnection();
        final PreparedStatement statement = con.prepareStatement(IS_ABOVE_DECISION_MAKING_THRESHOLD.get())) {
      IS_ABOVE_DECISION_MAKING_THRESHOLD.setParameter(statement, QueryAttribute.JOB_ID, jobId);
      IS_ABOVE_DECISION_MAKING_THRESHOLD.setParameter(statement, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
      final ResultSet rs = statement.executeQuery();
      if (rs.next()) {
        aboveThreshold = QueryUtil.getBoolean(rs, QueryAttribute.RESULT);
      }
    } catch (final SQLException e) {
      LOG.error("SQL error while checking if job is above decision making thresholds for job {} and calculation {}", jobId, proposedCalculationId, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return aboveThreshold;
  }

  public List<AssessmentAreaThresholdResults> getAssessmentAreaThresholdResults(final int jobId, final int proposedCalculationId)
      throws AeriusException {
    final Map<Integer, AssessmentAreaThresholdResults> areaResultsMap = new HashMap<>();
    try (final Connection con = pmf.getConnection();
        final PreparedStatement statement = con.prepareStatement(ASSESSMENT_AREA_THRESHOLD_RESULTS.get())) {
      ASSESSMENT_AREA_THRESHOLD_RESULTS.setParameter(statement, QueryAttribute.JOB_ID, jobId);
      ASSESSMENT_AREA_THRESHOLD_RESULTS.setParameter(statement, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
        final AssessmentAreaThresholdResults areaResults = areaResultsMap.computeIfAbsent(assessmentAreaId,
            k -> new AssessmentAreaThresholdResults(areaInformationSupplier.determineAssessmentArea(assessmentAreaId)));
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final EmissionResultType resultType = EmissionResultType.safeValueOf(QueryAttribute.RESULT_TYPE.getString(rs));
        final EmissionResultKey erk = EmissionResultKey.safeValueOf(substance, resultType);
        areaResults.getResults().put(erk, new ThresholdResult(
            QueryAttribute.RESULT.getDouble(rs),
            QueryUtil.getDouble(rs, RepositoryAttribute.THRESHOLD),
            QueryUtil.getDouble(rs, RepositoryAttribute.FRACTION)));
      }
    } catch (final SQLException e) {
      LOG.error("SQL error while retrieving assessment area threshold results for job {} and calculation {}", jobId, proposedCalculationId, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return new ArrayList<>(areaResultsMap.values());
  }

  public boolean isAboveAssessmentAreaThreshold(final int jobId, final int proposedCalculationId) throws AeriusException {
    boolean aboveThreshold = false;
    try (final Connection con = pmf.getConnection();
        final PreparedStatement statement = con.prepareStatement(IS_ABOVE_ASSESSMENT_AREA_THRESHOLD.get())) {
      IS_ABOVE_ASSESSMENT_AREA_THRESHOLD.setParameter(statement, QueryAttribute.JOB_ID, jobId);
      IS_ABOVE_ASSESSMENT_AREA_THRESHOLD.setParameter(statement, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
      final ResultSet rs = statement.executeQuery();
      if (rs.next()) {
        aboveThreshold = QueryUtil.getBoolean(rs, QueryAttribute.RESULT);
      }
    } catch (final SQLException e) {
      LOG.error("SQL error while checking if job is above assessment area thresholds for job {} and calculation {}", jobId, proposedCalculationId, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return aboveThreshold;
  }

  public void insertDevelopmentPressureSearchResult(final int jobId, final int numberOfProjectsNearby) throws AeriusException {
    try (final Connection con = pmf.getConnection();
        final PreparedStatement statement = con.prepareStatement(INSERT_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB_BY_PROJECTS.get()
            + " ON CONFLICT (job_id) DO UPDATE set development_pressure_class_id = EXCLUDED.development_pressure_class_id")) {
      INSERT_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB_BY_PROJECTS.setParameter(statement, QueryAttribute.JOB_ID, jobId);
      INSERT_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB_BY_PROJECTS.setParameter(statement, RepositoryAttribute.DEVELOPMENT_PRESSURE_CLASS_ID,
          numberOfProjectsNearby);
      statement.executeUpdate();
    } catch (final SQLException e) {
      LOG.error("SQL error inserting development pressure search result for job {} and numberOfProjectsNearby {}", jobId, numberOfProjectsNearby, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public void insertDevelopmentPressureClass(final int jobId, final String code) throws AeriusException {
    try (final Connection con = pmf.getConnection();
        final PreparedStatement statement = con.prepareStatement(INSERT_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB_BY_CODE.get()
            + " ON CONFLICT (job_id) DO UPDATE set development_pressure_class_id = EXCLUDED.development_pressure_class_id")) {
      INSERT_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB_BY_CODE.setParameter(statement, QueryAttribute.JOB_ID, jobId);
      INSERT_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB_BY_CODE.setParameter(statement, QueryAttribute.CODE, code);
      statement.executeUpdate();
    } catch (final SQLException e) {
      LOG.error("SQL error inserting development pressure search result for job {} and code {}", jobId, code, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public SimpleCategory getDevelopmentPressureClass(final int jobId) throws AeriusException {
    SimpleCategory classification = null;
    try (final Connection con = pmf.getConnection();
        final PreparedStatement statement = con.prepareStatement(GET_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB.get())) {
      GET_DEVELOPMENT_PRESSURE_CLASS_FOR_JOB.setParameter(statement, QueryAttribute.JOB_ID, jobId);
      final ResultSet rs = statement.executeQuery();
      if (rs.next()) {
        final int classificatonId = QueryUtil.getInt(rs, RepositoryAttribute.DEVELOPMENT_PRESSURE_CLASS_ID);
        final String code = QueryAttribute.CODE.getString(rs);
        final String description = QueryAttribute.DESCRIPTION.getString(rs);
        classification = new SimpleCategory(classificatonId, code, description, description);
      }
    } catch (final SQLException e) {
      LOG.error("SQL error while getting development pressure class for job {}", jobId, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return classification;
  }

}
