/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.sector.category.ColdStartCategories;
import nl.overheid.aerius.shared.domain.sector.category.ColdStartSourceCategory;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;

public final class ColdStartCategoryRepository {

  private enum RepositoryAttribute implements Attribute {
    COLD_START_CATEGORY_ID, EMISSION_DIURNAL_VARIATION_ID, MOBILE_SOURCE_ON_ROAD_CATEGORY_ID, PARTICLE_SIZE_DISTRIBUTION, ROAD_VEHICLE_CATEGORY_ID;
  }

  private static final Query COLD_START_SPECIFIC_CHARACTERISTICS_QUERY = QueryBuilder.from("cold_start_specific_characteristics_view")
      .where(QueryAttribute.CODE)
      .getQuery();

  private static final Query COLD_START_SPECIFIC_CATEGORIES_QUERY = QueryBuilder
      .from("cold_start_specific_emission_factors")
      .join(new JoinClause("mobile_source_on_road_categories", "mobile_source_on_road_category_id"))
      .getQuery();

  private static final Query COLD_START_STANDARD_CHARACTERISTICS_QUERY = QueryBuilder.from("cold_start_standard_characteristics_view")
      .where(QueryAttribute.CODE)
      .getQuery();

  private static final Query COLD_START_STANDARD_CATEGORIES_QUERY = QueryBuilder
      .from("cold_start_standard_emission_factors")
      .join(new JoinClause("road_vehicle_categories", "road_vehicle_category_id"))
      .getQuery();

  private ColdStartCategoryRepository() {
    // Util class
  }

  /**
   * Returns specific vehicle Cold Start characteristics
   *
   * @param con Database connection
   * @return
   * @throws SQLException
   */
  public static OPSSourceCharacteristics findSpecificColdStartCharacteristics(final Connection con, final String code)
      throws SQLException {
    return findColdStartCharacteristics(con, COLD_START_SPECIFIC_CHARACTERISTICS_QUERY, code);
  }

  /**
   * Returns standard vehicle Cold Start characteristics
   *
   * @param con Database connection
   * @param code
   * @param year
   * @return
   * @throws SQLException
   */
  public static OPSSourceCharacteristics findStandardColdStartCharacteristics(final Connection con, final String code)
      throws SQLException {
    return findColdStartCharacteristics(con, COLD_START_STANDARD_CHARACTERISTICS_QUERY, code);
  }

  private static OPSSourceCharacteristics findColdStartCharacteristics(final Connection con, final Query query, final String code)
      throws SQLException {
    try (final PreparedStatement ps = con.prepareStatement(query.get())) {
      query.setParameter(ps, QueryAttribute.CODE, code);
      final ResultSet rs = ps.executeQuery();

      if (rs.next()) {
        return collectEntity(rs);
      }
    }
    return null;
  }

  private static OPSSourceCharacteristics collectEntity(final ResultSet rs) throws SQLException {
    final OPSSourceCharacteristics osc = new OPSSourceCharacteristics();

    osc.setHeatContent(QueryAttribute.HEAT_CONTENT.getDouble(rs));
    osc.setEmissionHeight(QueryAttribute.HEIGHT.getDouble(rs));
    osc.setSpread(QueryAttribute.SPREAD.getDouble(rs));
    osc.setDiurnalVariation(DiurnalVariation.safeValueOf(QueryUtil.getInt(rs, RepositoryAttribute.EMISSION_DIURNAL_VARIATION_ID)));
    osc.setParticleSizeDistribution(QueryUtil.getInt(rs, RepositoryAttribute.PARTICLE_SIZE_DISTRIBUTION));
    return osc;
  }

  /**
   * Returns all Cold Start categories for both standard and specific mobile categories.
   *
   * @param con Database connection
   * @param messagesKey message key for translated name/description
   * @return list of all cold start categoriess
   * @throws SQLException
   */
  public static ColdStartCategories findAllColdStartCategories(final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    final ColdStartCategories categories = new ColdStartCategories();
    categories.setStandardCategories(
        findColdStartCategories(con, RepositoryAttribute.ROAD_VEHICLE_CATEGORY_ID, COLD_START_STANDARD_CATEGORIES_QUERY, messagesKey));
    categories.setSpecificCategories(
        findColdStartCategories(con, RepositoryAttribute.MOBILE_SOURCE_ON_ROAD_CATEGORY_ID, COLD_START_SPECIFIC_CATEGORIES_QUERY, messagesKey));
    return categories;
  }

  private static List<ColdStartSourceCategory> findColdStartCategories(final Connection con, final RepositoryAttribute idAttribute,
      final Query query, final DBMessagesKey messagesKey) throws SQLException {
    final ColdStartCategoryCollector collector = new ColdStartCategoryCollector(idAttribute, messagesKey);

    try (final PreparedStatement ps = con.prepareStatement(query.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        collector.collectEntities(rs);
      }
    }
    return collector.getEntities();
  }

  private static class ColdStartCategoryCollector extends AbstractCategoryCollector<ColdStartSourceCategory> {

    private final RepositoryAttribute idAttribute;

    protected ColdStartCategoryCollector(final RepositoryAttribute idAttribute, final DBMessagesKey messagesKey) {
      super(idAttribute, messagesKey);
      this.idAttribute = idAttribute;
    }

    @Override
    ColdStartSourceCategory getNewCategory() throws SQLException {
      return new ColdStartSourceCategory();
    }

    @Override
    public void appendEntity(final ColdStartSourceCategory entity, final ResultSet rs) throws SQLException {
      final int year = QueryAttribute.YEAR.getInt(rs);
      final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));

      entity.setEmissionFactor(new EmissionValueKey(year, substance), QueryAttribute.EMISSION_FACTOR.getDouble(rs));
    }

    @Override
    void setDescription(final ColdStartSourceCategory category, final ResultSet rs) throws SQLException {
      if (RepositoryAttribute.MOBILE_SOURCE_ON_ROAD_CATEGORY_ID == idAttribute) {
        DBMessages.setMobileSourceOnRoadCategoryMessages(category, messagesKey);
      }
    }
  }

}
