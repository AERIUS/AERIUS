/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.layer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.aerius.geo.domain.legend.TextLegend;
import nl.aerius.geo.shared.LayerBingProps;
import nl.aerius.geo.shared.LayerMultiWMSProps;
import nl.aerius.geo.shared.LayerProps;
import nl.aerius.geo.shared.LayerWMSProps;
import nl.aerius.geo.shared.LayerWMTSProps;
import nl.overheid.aerius.db.common.HabitatRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.JoinClause.JoinType;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.util.ColorUtil;

/**
 * Database methods for getting layer data.
 */
public final class LayerRepository {

  private enum RepositoryAttribute implements Attribute {

    LAYER_LEGEND_ID,
    LEGEND_TYPE,
    SORT_ORDER,

    LAYER_ID,
    THEME,
    LAYER_TYPE,
    TITLE,
    MIN_SCALE,
    MAX_SCALE,
    OPACITY,
    VISIBLE,
    PART_OF_BASE_LAYER,
    RANGE,

    LAYER_CAPABILITIES_ID,
    URL,
    BUNDLE_NAME,

    BEGIN_YEAR,
    END_YEAR,
    DYNAMIC_TYPE,

    API_KEY,

    IMAGE_TYPE,
    SERVICE_VERSION,
    TILE_MATRIX_SET,
    PROJECTION,
    ATTRIBUTION,
    TILE_SIZE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private enum LayerType {
    WMS, WMTS, BING;
  }

  private enum DynamicLayerType {
    HABITAT_TYPE;
  }

  private static final Attributes LAYER_ATTRIBUTES = new Attributes(
      RepositoryAttribute.LAYER_ID, RepositoryAttribute.LAYER_TYPE, QueryAttribute.NAME, RepositoryAttribute.TITLE,
      RepositoryAttribute.LAYER_LEGEND_ID,
      RepositoryAttribute.MIN_SCALE, RepositoryAttribute.MAX_SCALE, RepositoryAttribute.OPACITY,
      RepositoryAttribute.IMAGE_TYPE, RepositoryAttribute.SERVICE_VERSION, RepositoryAttribute.TILE_MATRIX_SET,
      RepositoryAttribute.URL, RepositoryAttribute.BUNDLE_NAME, RepositoryAttribute.ATTRIBUTION,
      RepositoryAttribute.BEGIN_YEAR, RepositoryAttribute.END_YEAR,
      RepositoryAttribute.DYNAMIC_TYPE, RepositoryAttribute.TILE_SIZE, RepositoryAttribute.PROJECTION,
      RepositoryAttribute.API_KEY);

  private static final Query LAYER_LEGEND = QueryBuilder.from("system.layer_legends")
      .join(new JoinClause("system.layer_legend_color_items", RepositoryAttribute.LAYER_LEGEND_ID, JoinType.LEFT))
      .select(RepositoryAttribute.LEGEND_TYPE, QueryAttribute.DESCRIPTION, QueryAttribute.NAME, QueryAttribute.COLOR)
      .where(RepositoryAttribute.LAYER_LEGEND_ID)
      .orderBy(RepositoryAttribute.SORT_ORDER)
      .getQuery();

  private static final Query DEFAULT_LAYERS = getLayerQuery()
      .select(LAYER_ATTRIBUTES.add(new Attributes(RepositoryAttribute.VISIBLE)))
      .join(new JoinClause("system.theme_layers", RepositoryAttribute.LAYER_ID))
      .orderBy(RepositoryAttribute.SORT_ORDER)
      .where(RepositoryAttribute.PART_OF_BASE_LAYER, RepositoryAttribute.THEME)
      .getQuery();

  private static final Query SPECIFIC_LAYER = getLayerQuery()
      .select(LAYER_ATTRIBUTES)
      .where(QueryAttribute.NAME)
      .getQuery();

  private static final Logger LOG = LoggerFactory.getLogger(LayerRepository.class);

  private LayerRepository() {
    // Util class.
  }

  private static QueryBuilder getLayerQuery() {
    return QueryBuilder.from("system.layers_view");
  }

  /**
   * @param con   The connection to use to get the layers.
   * @param theme the theme to get the layers for.
   * @param key   DBMessages key.
   * @return The list of layer properties.
   * @throws SQLException    In case of database errors.
   * @throws AeriusException In case of other errors.
   */
  public static List<LayerProps> getLayers(final Connection con, final Theme theme, final DBMessagesKey key) throws SQLException, AeriusException {
    return getLayers(con, theme, key, false);
  }

  public static List<LayerProps> getBaseLayers(final Connection con, final Theme theme, final DBMessagesKey key)
      throws SQLException, AeriusException {
    return getLayers(con, theme, key, true);
  }

  private static List<LayerProps> getLayers(final Connection con, final Theme theme, final DBMessagesKey key, final boolean baseLayersOnly)
      throws SQLException, AeriusException {
    final List<LayerProps> layers = new ArrayList<>();

    try (final PreparedStatement stmt = con.prepareStatement(DEFAULT_LAYERS.get())) {
      DEFAULT_LAYERS.setParameter(stmt, RepositoryAttribute.PART_OF_BASE_LAYER, baseLayersOnly);
      DEFAULT_LAYERS.setParameter(stmt, RepositoryAttribute.THEME, theme.name().toLowerCase(Locale.ROOT), Types.OTHER);

      final ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        final LayerProps layer = getLayerProps(con, rs, key);

        layer.setVisible(QueryUtil.getBoolean(rs, RepositoryAttribute.VISIBLE));
        layers.add(layer);
      }
    }

    return layers;
  }

  /**
   * @param con The connection to use.
   * @param legendId The ID of the legend to retrieve.
   * @return The correct legend.
   * @throws SQLException In case of exceptions.
   */
  public static Legend getLegend(final Connection con, final int legendId) throws SQLException {
    Legend legend = null;
    try (final PreparedStatement stmt = con.prepareStatement(LAYER_LEGEND.get())) {
      LAYER_LEGEND.setParameter(stmt, RepositoryAttribute.LAYER_LEGEND_ID, legendId);

      final ResultSet rs = stmt.executeQuery();

      final List<String> labels = new ArrayList<>();
      final List<String> colors = new ArrayList<>();
      LegendType legendType = null;
      String description = null;
      while (rs.next()) {
        labels.add(QueryAttribute.NAME.getString(rs));
        colors.add(ColorUtil.webColor(QueryAttribute.COLOR.getString(rs)));
        legendType = QueryUtil.getEnum(rs, RepositoryAttribute.LEGEND_TYPE, LegendType.class);
        description = QueryAttribute.DESCRIPTION.getString(rs);
      }
      if (legendType != null && legendType.hasColorLegend() && !labels.isEmpty()) {
        legend = new ColorLabelsLegend(labels.toArray(new String[labels.size()]), colors.toArray(new String[colors.size()]), legendType);
      } else if (legendType != null && legendType == LegendType.TEXT) {
        legend = new TextLegend(description);
      }
    }

    return legend;
  }

  /**
   * @param con The connection to use.
   * @param name The name to get the layer for.
   * @return The correct layer (or null if not found)
   * @throws SQLException In case of database errors.
   * @throws AeriusException In case of
   */
  public static LayerProps getLayer(final Connection con, final String name, final DBMessagesKey key) throws SQLException, AeriusException {
    LayerProps layer = null;
    try (final PreparedStatement stmt = con.prepareStatement(SPECIFIC_LAYER.get())) {
      SPECIFIC_LAYER.setParameter(stmt, QueryAttribute.NAME, name);

      final ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        layer = getLayerProps(con, rs, key);
      }
    }

    return layer;
  }

  private static LayerProps getLayerProps(final Connection con, final ResultSet rs, final DBMessagesKey key) throws SQLException, AeriusException {
    final LayerProps layer;
    final String name = QueryAttribute.NAME.getString(rs);
    final LayerType type = QueryUtil.getEnum(rs, RepositoryAttribute.LAYER_TYPE, LayerType.class);
    if (type == null) {
      LOG.error("Unexpected layer type for layer with name {}", name);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    switch (type) {
    case WMS:
      layer = getWMSLayerProps(con, rs, key);
      break;
    case WMTS:
      layer = getWMTSLayerProps(con, rs, key);
      break;
    case BING:
      layer = getBingLayerProps(con, rs, key);
      break;
    default:
      layer = new LayerProps();
      break;
    }
    layer.setId(QueryUtil.getInt(rs, RepositoryAttribute.LAYER_ID));
    layer.setName(name);
    layer.setUrl(QueryUtil.getString(rs, RepositoryAttribute.URL));
    layer.setBundleName(QueryUtil.getString(rs, RepositoryAttribute.BUNDLE_NAME));
    layer.setTitle(QueryUtil.getString(rs, RepositoryAttribute.TITLE));
    layer.setAttribution(QueryUtil.getString(rs, RepositoryAttribute.ATTRIBUTION));
    layer.setMinScale(QueryUtil.getNullableDouble(rs, RepositoryAttribute.MIN_SCALE));
    layer.setMaxScale(QueryUtil.getNullableDouble(rs, RepositoryAttribute.MAX_SCALE));
    layer.setOpacity(QueryUtil.getFloat(rs, RepositoryAttribute.OPACITY));
    layer.setLegend(getLegend(con, QueryUtil.getInt(rs, RepositoryAttribute.LAYER_LEGEND_ID)));
    return layer;
  }

  private static LayerBingProps getBingLayerProps(final Connection con, final ResultSet rs, final DBMessagesKey key) throws SQLException {
    final LayerBingProps layer = new LayerBingProps();
    layer.setKey(QueryUtil.getString(rs, RepositoryAttribute.API_KEY));
    layer.setImagerySet(QueryAttribute.NAME.getString(rs));
    return layer;
  }

  private static LayerWMSProps getWMSLayerProps(final Connection con, final ResultSet rs, final DBMessagesKey key) throws SQLException {
    final LayerWMSProps layer = getSpecificWMSLayerProperties(con, rs, key);
    layer.setTileSize(QueryUtil.getInt(rs, RepositoryAttribute.TILE_SIZE));
    return layer;
  }

  private static LayerWMTSProps getWMTSLayerProps(final Connection con, final ResultSet rs, final DBMessagesKey key) throws SQLException {
    final LayerWMTSProps layer = new LayerWMTSProps();
    layer.setType(QueryUtil.getString(rs, RepositoryAttribute.IMAGE_TYPE));
    layer.setServiceVersion(QueryUtil.getString(rs, RepositoryAttribute.SERVICE_VERSION));
    layer.setTileMatrixSet(QueryUtil.getString(rs, RepositoryAttribute.TILE_MATRIX_SET));
    layer.setProjection(QueryUtil.getString(rs, RepositoryAttribute.PROJECTION));
    return layer;
  }

  private static LayerWMSProps getSpecificWMSLayerProperties(final Connection con, final ResultSet rs, final DBMessagesKey key) throws SQLException {
    final DynamicLayerType layerType = QueryUtil.getEnum(rs, RepositoryAttribute.DYNAMIC_TYPE, DynamicLayerType.class);
    final LayerWMSProps layer;
    if (layerType == null) {
      layer = new LayerWMSProps();
    } else {
      switch (layerType) {
      case HABITAT_TYPE:
        layer = getHabitatLayer(con, key);
        break;
      default:
        layer = new LayerWMSProps();
        break;
      }
    }
    return layer;
  }

  private static LayerWMSProps getHabitatLayer(final Connection con, final DBMessagesKey key) throws SQLException {
    final LayerMultiWMSProps<String> layer = new LayerMultiWMSProps<>();

    final List<HabitatType> habitatTypes = HabitatRepository.getHabitatTypes(con, key);
    final LinkedHashMap<Integer, String> habitatFilterTypes = new LinkedHashMap<>();
    for (final HabitatType ht : habitatTypes) {
      habitatFilterTypes.put(ht.getId(), ht.getCode() + ": " + ht.getName());
    }

    layer.setTypes(habitatFilterTypes);
    return layer;
  }

}
