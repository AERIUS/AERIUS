/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator.calculationpoint;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Helper class to collect the determined calculation points
 * from the repository and merge/sort them correctly.
 */
public class DeterminedCalculationPointsCollector {

  private static final int ONE_KM = 1000;

  private final Map<IntCoord, DeterminedPoint> points = new HashMap<>();
  private final Map<Integer, Double> assessmentAreaDistances = new HashMap<>();

  /**
   * Adds a determined calculation point
   *
   * @param x coordinate
   * @param y coordinate
   * @param assessmentAreaId assessment area where this point belongs to
   * @param distance distance to the closest source
   * @param label label for the point
   */
  public void add(final int x, final int y, final int assessmentAreaId, final double distance, final String label) {
    assessmentAreaDistances.merge(assessmentAreaId, distance, Double::min);
    final IntCoord coords = new IntCoord(x, y);
    final DeterminedPoint existingPoint = points.computeIfAbsent(coords, i -> new DeterminedPoint(coords, distance, new HashSet<>(),
        new ArrayList<>()));
    existingPoint.assessmentAreas.add(assessmentAreaId);
    existingPoint.labels.add(label);
  }

  /**
   * Returns the merged/sorted points as AeriusPoints
   *
   * @return merged/sorted points
   */
  public List<AeriusPoint> getPoints() {
    return points.values().stream()
        .sorted(Comparator.comparing(DeterminedPoint::getAreaDistance).thenComparing(DeterminedPoint::getDistance))
        .map(DeterminedPoint::toAeriusPoint)
        .collect(Collectors.toList());
  }

  /**
   * Determines number of assessment areas
   *
   * @return number of unique assessment areas
   */
  public int getAmountOfAssessmentAreas() {
    return assessmentAreaDistances.size();
  }

  /**
   * Helper class
   */
  private class DeterminedPoint {

    final IntCoord coords;
    final double distance;
    final Set<Integer> assessmentAreas;
    final List<String> labels;

    public DeterminedPoint(final IntCoord coords, final double distance, final Set<Integer> assessmentAreas,
        final List<String> labels) {
      this.coords = coords;
      this.distance = distance;
      this.assessmentAreas = assessmentAreas;
      this.labels = labels;
    }

    public double getAreaDistance() {
      return assessmentAreas.stream().mapToDouble(assessmentAreaDistances::get).min().orElse(Double.MAX_VALUE);
    }

    public double getDistance() {
      return distance;
    }

    public AeriusPoint toAeriusPoint() {
      final AeriusPoint aeriusPoint = new AeriusPoint(0, 0, AeriusPoint.AeriusPointType.POINT, coords.x, coords.y);

      String label = String.join(" & ", labels);
      if (distance < ONE_KM) {
        label += " (<1 km)";
      } else {
        label += " (" + (Math.round(distance / ONE_KM)) + " km)";
      }
      aeriusPoint.setLabel(label);
      return aeriusPoint;
    }
  }

  private static class IntCoord {
    final int x;
    final int y;

    public IntCoord(final int x, final int y) {
      this.x = x;
      this.y = y;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o) {
        return true;
      } else if (o == null || getClass() != o.getClass()) {
        return false;
      }
      final IntCoord intCoord = (IntCoord) o;
      return x == intCoord.x && y == intCoord.y;
    }

    @Override
    public int hashCode() {
      return Objects.hash(x, y);
    }
  }

}
