/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

public class ConstantRepositoryBean {

  private static final Logger LOG = LoggerFactory.getLogger(ConstantRepositoryBean.class);

  private final PMF pmf;

  public ConstantRepositoryBean(final PMF pmf) {
    this.pmf = pmf;
  }

  public <E extends Enum<E>> E getEnum(final Enum<?> constantEnum, final Class<E> enumClass) {
    return ConstantRepository.getEnum(pmf, constantEnum, enumClass);
  }

  /**
   * Get a constant value from the database. Runtime exception if not found.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * <p>Will throw a runtime exception when a SQL exception occurred.
   */
  public String getString(final Enum<?> constantEnum) {
    return getString(constantEnum, true);
  }

  /**
   * Get a constant value from the database.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * @param constantEnum The constant to get the value for.
   * @param mustExist If true, the constant must exist, else a runtime exception will be thrown.
   * @return The value of the constant.
   * <p>Will throw a runtime exception when a SQL exception occurred.
   */
  public String getString(final Enum<?> constantEnum, final boolean mustExist) {
    return findConstantByKey(constantEnum.name(), mustExist);
  }

  /**
   * Get a constant value from the database. Runtime exception if not found.
   * The constant will be retrieved by using the name of this enum constant, exactly as declared in its enum declaration.
   * Boolean is determined by using {@link Boolean#parseBoolean(String)}. Same rules apply.
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * <p>Will throw a runtime exception when a SQL exception occurred.
   */
  public boolean getBoolean(final Enum<?> constantEnum) {
    return Boolean.parseBoolean(getString(constantEnum, true));
  }

  /**
   * Get a constant value from the database, and cast it to Integer.
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * @throws AeriusException When an error occurred communicating with the database.
   */
  public Integer getInteger(final Enum<?> constantEnum) throws AeriusException {
    return getNumber(constantEnum, Integer.class);
  }

  /**
   * Get a constant value from the database, and cast it to Double.
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   * @throws AeriusException When an error occurred communicating with the database.
   */
  public Double getDouble(final Enum<?> constantEnum) throws AeriusException {
    return getNumber(constantEnum, Double.class);
  }

  /**
   * Get a constant value from the database, and cast it to the specified Number type.
   * @param constantEnum The constant to get the value for.
   * @param t Class reference for the type to return.
   * @param <N> Type that will be returned.
   * @return The value of the constant.
   * @throws AeriusException When an error occurred communicating with the database.
   */
  public <N extends Number> N getNumber(final Enum<?> constantEnum, final Class<N> t) throws AeriusException {
    try {
      return ConstantRepository.getNumber(pmf, constantEnum, t);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting number constant", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Get constant value.
   *
   * @param key
   *          The key to fetch
   * @param mustExist
   *          If true and constant is not found, a runtime exception will be thrown.
   * @return The value of the constant, null if not found (and mustExist = false)
   * <p>Will throw a runtime exception when a SQL exception occurred.
   */
  public String findConstantByKey(final String key, final boolean mustExist) {
    return ConstantRepository.findConstantByKey(pmf, key, mustExist);
  }

  /**
   * Returns the version of the data set as set in the database.
   * @return version of data in database
   */
  public String getDatabaseVersion() {
    return getString(ConstantsEnum.CURRENT_DATABASE_VERSION);
  }

}
