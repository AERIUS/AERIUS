/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

public class ArchiveContributionResultsSummaryInput {
  private final int proposedCalculationId;

  public ArchiveContributionResultsSummaryInput(final int proposedCalculationId) {
    this.proposedCalculationId = proposedCalculationId;
  }

  public int getProposedCalculationId() {
    return proposedCalculationId;
  }

  public static ArchiveContributionResultsSummaryInput fromSituationCalculations(final int proposedCalculationId) {
    return new ArchiveContributionResultsSummaryInput(proposedCalculationId);
  }
}
