/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.i18n;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.DBMessagesResourceBundle.Control;
import nl.overheid.aerius.db.i18n.LocalizationRepository.LocalizationObject;
import nl.overheid.aerius.db.i18n.LocalizationRepository.LocalizationProperty;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;
import nl.overheid.aerius.shared.domain.sector.category.AbstractEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalHousingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Static convenience class that gives access to the {@link DBMessagesResourceBundle}.
 */
public final class DBMessages {

  private static final Logger LOG = LoggerFactory.getLogger(DBMessages.class);

  private static final String QUERY_GET_I18N_TABLES = "SELECT table_name "
      + "FROM information_schema.tables "
      + "WHERE table_schema = 'i18n' AND table_name NOT IN ('messages','system_info_messages') ";

  // Resource name
  private static final String RESOURCE_NAME = "AERIUSDBMessages_CALCULATOR";

  private static DBMessages MESSAGES;

  // We can safely cache this control
  private final Control control;

  /**
   * Private constructor
   */
  private DBMessages(final PMF pmf, final List<LocalizationObject> localizationObjects) {
    this.control = new DBMessagesResourceBundle.Control(pmf, localizationObjects);
  }

  /**
   * Init the DBMessages for the given {@link PMF} that'll be used to access the database.
   *
   * Required call before any other calls are possible.
   *
   * @param pmf to access the database with.
   */
  public static synchronized void init(final PMF pmf) {
    DBMessages messages = MESSAGES;
    if (messages == null) {
      try {
        messages = new DBMessages(pmf, getLocalizationObjects(pmf));
      } catch (final SQLException e) {
        LOG.error("Error while trying to inialize DBMessages with PMF: {}", pmf);
        throw new RuntimeException(e);
      }

      // Preload known languages. Others (if there will ever be any) will be loaded lazily.
      for (final Locale loc : LocaleUtils.KNOWN_LOCALES) {
        try {
          LOG.debug("Getting bundle for locale: {}", loc);
          messages.getBundle(new DBMessagesKey(loc));
        } catch (final MissingResourceException e) {
          LOG.error("Bundle for locale {} not available.", loc);
        }
      }

      MESSAGES = messages;
    }

    LOG.debug("Initialized messages");
  }

  /**
   * @param cat The {@link FarmLodgingCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setFarmLodgingTypeMessages(final FarmLodgingCategory cat, final DBMessagesKey key) {
    cat.setDescription(getLocalizedString(key, LocalizationObject.FARM_LODGING_TYPE, LocalizationProperty.DESCRIPTION, cat.getId()));
  }

  /**
   * @param def The {@link FarmAnimalCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setFarmAnimalCategoryMessages(final FarmAnimalCategory def, final DBMessagesKey key) {
    def.setDescription(getLocalizedString(key, LocalizationObject.FARM_ANIMAL_CATEGORY, LocalizationProperty.DESCRIPTION, def.getId()));
  }

  /**
   * @param cat The {@link FarmAnimalHousingCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setFarmAnimalHousingMessages(final FarmAnimalHousingCategory cat, final DBMessagesKey key) {
    cat.setDescription(getLocalizedString(key, LocalizationObject.FARM_ANIMAL_HOUSING_CATEGORY, LocalizationProperty.DESCRIPTION, cat.getId()));
  }

  /**
   * @param cat The {@link FarmAdditionalHousingSystemCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setFarmAdditionalHousingSystemMessages(final FarmAdditionalHousingSystemCategory cat, final DBMessagesKey key) {
    cat.setDescription(getLocalizedString(key, LocalizationObject.FARM_ADDITIONAL_HOUSING_SYSTEM, LocalizationProperty.DESCRIPTION, cat.getId()));
  }

  /**
   * @param def The {@link FarmLodgingSystemDefinition} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setFarmLodgingSystemDefinitionMessages(final FarmLodgingSystemDefinition def, final DBMessagesKey key) {
    def.setDescription(getLocalizedString(key, LocalizationObject.FARM_LODGING_SYSTEM_DEFINITION, LocalizationProperty.DESCRIPTION, def.getId()));
  }

  /**
   * @param cat The {@link FarmAdditionalLodgingSystemCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setAdditionalFarmLodgingSystemMessages(final FarmAdditionalLodgingSystemCategory cat, final DBMessagesKey key) {
    cat.setDescription(getLocalizedString(key, LocalizationObject.FARM_ADDITIONAL_LODGING_SYSTEM, LocalizationProperty.DESCRIPTION, cat.getId()));
  }

  /**
   * @param cat The {@link FarmReductiveLodgingSystemCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setReductiveFarmLodgingSystemMessages(final FarmReductiveLodgingSystemCategory cat, final DBMessagesKey key) {
    cat.setDescription(getLocalizedString(key, LocalizationObject.FARM_REDUCTIVE_LODGING_SYSTEM, LocalizationProperty.DESCRIPTION, cat.getId()));
  }

  /**
   * @param cat The {@link FarmLodgingFodderMeasureCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setFarmLodgingFodderMeasureMessages(final FarmLodgingFodderMeasureCategory cat, final DBMessagesKey key) {
    cat.setDescription(getLocalizedString(key, LocalizationObject.FARM_LODGING_FODDER_MEASURE, LocalizationProperty.DESCRIPTION, cat.getId()));
  }

  /**
   * @param cat The {@link FarmlandCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setFarmlandMessages(final FarmlandCategory cat, final DBMessagesKey key) {
    setCategoryMessages(cat, LocalizationObject.FARMLAND_CATEGORY, key);
  }

  /**
   * @param cat The {@link FarmSourceCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setFarmSourceMessages(final FarmSourceCategory cat, final DBMessagesKey key) {
    cat.setDescription(getLocalizedString(key, LocalizationObject.FARM_SOURCE_CATEGORY, LocalizationProperty.DESCRIPTION, cat.getId()));
  }

  /**
   * @param type The {@link HabitatType} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setHabitatTypeMessages(final HabitatType type, final DBMessagesKey key) {
    type.setCode(getLocalizedString(key, LocalizationObject.HABITAT_TYPE, LocalizationProperty.NAME, type.getId()));
    type.setName(getLocalizedString(key, LocalizationObject.HABITAT_TYPE, LocalizationProperty.DESCRIPTION, type.getId()));
  }

  /**
   * @param cat The {@link OffRoadMobileSourceCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setMobileSourceOffRoadCategoryMessages(final AbstractEmissionCategory cat, final DBMessagesKey key) {
    setCategoryMessages(cat, LocalizationObject.MOBILE_SOURCE_OFF_ROAD_CATEGORY, key);
  }

  /**
   * @param cat The {@link OnRoadMobileSourceCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setMobileSourceOnRoadCategoryMessages(final AbstractEmissionCategory cat, final DBMessagesKey key) {
    setCategoryMessages(cat, LocalizationObject.MOBILE_SOURCE_ON_ROAD_CATEGORY, key);
  }

  /**
   * @param cat The {@link MaritimeShippingCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setShippingCategoryMessages(final MaritimeShippingCategory cat, final DBMessagesKey key) {
    setCategoryMessages(cat, LocalizationObject.MARITIME_SHIPPING_CATEGORY, key);
  }

  /**
   * @param cat The {@link InlandShippingCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setShippingCategoryMessages(final InlandShippingCategory cat, final DBMessagesKey key) {
    setCategoryMessages(cat, LocalizationObject.INLAND_SHIPPING_CATEGORY, key);
  }

  /**
   * @param cat The {@link InlandWaterwayCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setWaterwayCategoryMessages(final InlandWaterwayCategory cat, final DBMessagesKey key) {
    setCategoryMessages(cat, LocalizationObject.INLAND_WATERWAY_CATEGORY, key);
  }

  /**
   * Set the sector description for the given {@link Locale} and {@link Sector}.
   *
   * @param sector Sector to set the description for.
   * @param key key to set the description for.
   */
  public static void setSectorMessages(final Sector sector, final DBMessagesKey key) {
    sector.setDescription(getLocalizedString(key, LocalizationObject.SECTOR, LocalizationProperty.DESCRIPTION, sector.getSectorId()));
  }

  /**
   * Set the correct i18n stuff for the given {@link Locale} for Road areas.
   *
   * @param cat The {@link SimpleCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setRoadAreaMessages(final SimpleCategory cat, final DBMessagesKey key) {
    setCategoryMessages(cat, LocalizationObject.ROAD_AREA_CATEGORY, key);
  }

  /**
   * Set the correct i18n stuff for the given {@link Locale} for Road types.
   *
   * @param cat The {@link SimpleCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setRoadTypeMessages(final SimpleCategory cat, final DBMessagesKey key) {
    setCategoryMessages(cat, LocalizationObject.ROAD_TYPE_CATEGORY, key);
  }

  /**
   * Set the correct i18n stuff for the given {@link Locale} for Road vehicle types.
   *
   * @param cat The {@link SimpleCategory} to set messages for.
   * @param key The key to use when determining messages.
   */
  public static void setRoadVehicleTypeMessages(final SimpleCategory cat, final DBMessagesKey key) {
    setCategoryMessages(cat, LocalizationObject.ROAD_VEHICLE_CATEGORY, key);
  }

  /**
   * Get a list of possible localization objects for the 'current' product.
   *
   * @param pmf The PMF to determine LocalizationObjects for.
   * @return The list of LocalizationObjects that area available in the database build for the current productProfile.
   * @throws SQLException on SQL error
   */
  public static List<LocalizationObject> getLocalizationObjects(final PMF pmf) throws SQLException {
    final List<LocalizationObject> objects = new ArrayList<>();

    // query columns: table_name
    try (final Connection con = pmf.getConnection(); final PreparedStatement stmt = con.prepareStatement(QUERY_GET_I18N_TABLES)) {
      final ResultSet rst = stmt.executeQuery();

      while (rst.next()) {
        final String tableName = rst.getString("table_name");

        final LocalizationObject locObj = LocalizationObject.getObjectByTableName(tableName);
        if (locObj == null) {
          LOG.warn("LocalizationObject is missing for table '{}'.", tableName);
          continue;
        }

        LOG.trace("Adding LocalizationObject {}", locObj);
        objects.add(locObj);
      }

    }

    return objects;
  }

  private static void setCategoryMessages(final AbstractCategory cat, final LocalizationObject localizationObject, final DBMessagesKey messagesKey) {
    cat.setName(getLocalizedString(messagesKey, localizationObject, LocalizationProperty.NAME, cat.getId()));
    cat.setDescription(getLocalizedString(messagesKey, localizationObject, LocalizationProperty.DESCRIPTION, cat.getId()));
  }

  private ResourceBundle getBundle(final DBMessagesKey messagesKey) {
    return ResourceBundle.getBundle(RESOURCE_NAME, messagesKey.getLocale(), control);
  }

  public static String getLocalizedString(final DBMessagesKey messagesKey, final LocalizationObject localizationObject,
      final LocalizationProperty property, final Object key) {
    final DBMessages messages = MESSAGES;
    if (messages == null) {
      LOG.error("Could not find messages for {}, did you forget to init DBMessages?", messagesKey);
      // should throw an exception at this point, but NPE which is bound to occur will suffice I guess.
    }
    return messages == null ? null : messages.getBundle(messagesKey).getString(localizationObject.getKey(property, key));
  }

  /**
   * Key to be used for convenience methods in DBMessages.
   */
  public static class DBMessagesKey {

    private final Locale locale;

    /**
     * @param locale The locale to use for the key.
     */
    public DBMessagesKey(final Locale locale) {
      this.locale = locale;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (locale == null ? 0 : locale.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      return obj != null && this.getClass() == obj.getClass() && this.locale.equals(((DBMessagesKey) obj).locale);
    }

    public Locale getLocale() {
      return locale;
    }

    @Override
    public String toString() {
      return locale == null ? "" : locale.toString();
    }
  }
}
