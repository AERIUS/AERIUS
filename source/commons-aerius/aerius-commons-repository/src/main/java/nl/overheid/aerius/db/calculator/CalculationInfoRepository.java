/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.info.ScenarioEmissionResults;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * DB class to retrieve information about calculations.
 */
public final class CalculationInfoRepository {

  private static class SubPointKey {

    private final int parentId;
    private final int subPointId;

    SubPointKey(final int parentId, final int subPointId) {
      this.parentId = parentId;
      this.subPointId = subPointId;
    }

    @Override
    public int hashCode() {
      return Objects.hash(parentId, subPointId);
    }

    @Override
    public boolean equals(final Object obj) {
      return obj != null && obj.getClass() == getClass()
          && (parentId == ((SubPointKey) obj).parentId && subPointId == ((SubPointKey) obj).subPointId);
    }

  }

  private static final Query GET_RECEPTOR_EMISSION_RESULTS =
      getQueryResultBase("jobs.calculation_results_view")
          .where(QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query GET_ALL_RECEPTOR_RESULTS =
      getAllReceptorResultsBase("jobs.calculation_results_view")
          .getQuery();

  private static final Query GET_ALL_SECTOR_RECEPTOR_RESULTS =
      getAllReceptorResultsBase("jobs.calculation_sector_results_view")
          .where(QueryAttribute.SECTOR_ID)
          .getQuery();

  private static final Query GET_ALL_CALCULATION_POINT_RESULTS =
      getQueryCalculationPointResultsBase("jobs.calculation_point_results_view")
          .getQuery();

  private static final Query GET_ALL_CALCULATION_SECTOR_POINT_RESULTS =
      getQueryCalculationPointResultsBase("jobs.calculation_point_sector_results_view")
          .where(QueryAttribute.SECTOR_ID)
          .getQuery();

  private static final Query GET_ALL_SUB_POINT_RESULTS =
      getQuerySubPointResultsBase("jobs.calculation_sub_point_results_view")
          .getQuery();

  private static final Query GET_ALL_SUB_SECTOR_POINT_RESULTS =
      getQuerySubPointResultsBase("jobs.calculation_sub_point_sector_results_view")
          .where(QueryAttribute.SECTOR_ID)
          .getQuery();

  private static final Query GET_ALL_RECEPTOR_ARCHIVE_RESULTS =
      getAllReceptorResultsBase("jobs.archive_contribution_results_view")
          .getQuery();

  private static final Query GET_ALL_SUB_POINT_ARCHIVE_RESULTS =
      getQuerySubPointResultsBase("jobs.archive_contribution_sub_results_view")
          .getQuery();

  private static final Query GET_RECEPTOR_SCENARIO_RESULTS =
      QueryBuilder.from("jobs.job_scenario_results_view")
          .select(EmissionResultRepositoryUtil.REQUIRED_ATTRIBUTES)
          .select(QueryAttribute.SCENARIO_RESULT_TYPE, QueryAttribute.TOTAL_RESULT)
          .where(QueryAttribute.KEY, QueryAttribute.RECEPTOR_ID)
          .getQuery();

  private CalculationInfoRepository() {
    //don't instantiate.
  }

  /**
   * Retrieve the "total" results for a certain calculation.
   * More results can still be added after query.
   * To be sure all results are in, compare the size of the result to the expected value.
   * Other way to be sure would be to check the state of a calculation,
   * That does depend on the state actually being set to COMPLETE when all is done however.
   *
   * @param connection The connection to use
   * @param calculationId The ID of the calculation to retrieve results for
   * @return The (current) list of CalculationPointFeatures with results that match this calculation ID
   * @throws SQLException If an error occurred communicating with the DB
   */
  public static List<CalculationPointFeature> getCalculationResults(final Connection connection, final int calculationId) throws SQLException {
    final List<CalculationPointFeature> results = new ArrayList<>();
    results.addAll(getReceptors(connection, calculationId, null));
    results.addAll(getSubPoints(connection, calculationId, null));
    return results;
  }

  /**
   * Retrieve the "total" results for a certain calculation for custom points.
   * More results can still be added after query.
   * To be sure all results are in, compare the size of the result to the expected value.
   * Other way to be sure would be to check the state of a calculation,
   * That does depend on the state actually being set to COMPLETE when all is done however.
   *
   * @param connection The connection to use
   * @param calculationId The ID of the calculation to retrieve results for
   * @return The (current) results per calculation point (by ID) that match this calculation ID
   * @throws SQLException If an error occurred communicating with the DB
   */
  public static Map<Integer, Map<EmissionResultKey, Double>> getCustomPointsCalculationResults(final Connection connection, final int calculationId)
      throws SQLException {
    return retrieveCustomCalculationPointResults(connection, calculationId, null);
  }

  /**
   * The same as {@link #getCalculationResults}, but returns the "sector" results for the given
   * sector (instead of the "total" results).
   *
   * @param connection The connection to use
   * @param calculationId The ID of the calculation to retrieve results for
   * @param sectorId The ID of the sector to retrieve results for
   * @return The (current) list of CalculationPointFeatures with results that match this calculation ID
   * @throws SQLException If an error occurred communicating with the DB
   */
  public static List<CalculationPointFeature> getCalculationSectorResults(final Connection connection, final int calculationId, final int sectorId)
      throws SQLException {
    final List<CalculationPointFeature> results = new ArrayList<>();
    results.addAll(getReceptors(connection, calculationId, sectorId));
    results.addAll(getSubPoints(connection, calculationId, sectorId));
    return results;
  }

  /**
   * The same as {@link #addCustomPointsCalculationResults}, but returns the "sector" results for the given
   * sector (instead of the "total" results).
   *
   * @param connection The connection to use
   * @param calculationId The ID of the calculation to retrieve results for
   * @param sectorId The ID of the sector to retrieve results for
   * @return The (current) results per calculation point (by ID) that match this calculation ID
   * @throws SQLException If an error occurred communicating with the DB
   */
  public static Map<Integer, Map<EmissionResultKey, Double>> getCustomPointsCalculationSectorResults(final Connection connection,
      final int calculationId,
      final int sectorId) throws SQLException {
    return retrieveCustomCalculationPointResults(connection, calculationId, sectorId);
  }

  private static List<CalculationPointFeature> getReceptors(final Connection connection, final int calculationId, final Integer sectorId)
      throws SQLException {
    final List<CalculationPointFeature> receptors = new ArrayList<>();
    final Query query = sectorId == null ? GET_ALL_RECEPTOR_RESULTS : GET_ALL_SECTOR_RECEPTOR_RESULTS;

    final ReceptorUtil receptorUtil = new ReceptorUtil(ReceptorGridSettingsRepository.getReceptorGridSettings(connection));
    try (final PreparedStatement ps = connection.prepareStatement(query.get())) {
      query.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);
      if (sectorId != null) {
        query.setParameter(ps, QueryAttribute.SECTOR_ID, sectorId);
      }

      receptors.addAll(handleReceptorQuery(ps, receptorUtil));
    }

    return receptors;
  }

  private static Map<Integer, Map<EmissionResultKey, Double>> retrieveCustomCalculationPointResults(final Connection connection,
      final int calculationId, final Integer sectorId) throws SQLException {
    final Map<Integer, Map<EmissionResultKey, Double>> resultsMap = new HashMap<>();
    final Query query = (sectorId == null) ? GET_ALL_CALCULATION_POINT_RESULTS : GET_ALL_CALCULATION_SECTOR_POINT_RESULTS;

    try (final PreparedStatement ps = connection.prepareStatement(query.get())) {
      query.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);
      if (sectorId != null) {
        query.setParameter(ps, QueryAttribute.SECTOR_ID, sectorId);
      }

      try (final ResultSet rs = ps.executeQuery()) {

        while (rs.next()) {
          final int calculationPointId = QueryAttribute.CALCULATION_POINT_ID.getInt(rs);
          final Map<EmissionResultKey, Double> results = resultsMap.computeIfAbsent(calculationPointId, x -> new HashMap<>());
          EmissionResultRepositoryUtil.addEmissionResult(rs, results);
        }
      }
    }
    return resultsMap;
  }

  private static List<CalculationPointFeature> getSubPoints(final Connection connection, final int calculationId, final Integer sectorId)
      throws SQLException {
    final List<CalculationPointFeature> points = new ArrayList<>();
    final Query query = (sectorId == null) ? GET_ALL_SUB_POINT_RESULTS : GET_ALL_SUB_SECTOR_POINT_RESULTS;

    try (final PreparedStatement ps = connection.prepareStatement(query.get())) {
      query.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);
      if (sectorId != null) {
        query.setParameter(ps, QueryAttribute.SECTOR_ID, sectorId);
      }

      points.addAll(handleSubPointQuery(ps));
    }

    return points;
  }

  /**
   * @param connection The connection to use.
   * @param calculationId The calculation ID to retrieve results for.
   * @param receptorId The receptor to retrieve results for.
   * @return The emission results for the specified receptor.
   * @throws SQLException In case of database errors.
   */
  public static EmissionResults getEmissionResults(final Connection connection, final int calculationId, final int receptorId)
      throws SQLException {
    final EmissionResults emissionResults = new EmissionResults();
    try (final PreparedStatement statement = connection.prepareStatement(GET_RECEPTOR_EMISSION_RESULTS.get())) {
      GET_RECEPTOR_EMISSION_RESULTS.setParameter(statement, QueryAttribute.CALCULATION_ID, calculationId);
      GET_RECEPTOR_EMISSION_RESULTS.setParameter(statement, QueryAttribute.RECEPTOR_ID, receptorId);

      try (final ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          EmissionResultRepositoryUtil.addEmissionResult(rs, emissionResults.getHashMap());
        }
      }
    }
    return emissionResults;
  }

  /**
   * @param connection The connection to use.
   * @param jobKey The key of the calculation job to retrieve results for.
   * @param receptorId The receptor to retrieve results for.
   * @return The scenario results for the specified receptor and job.
   * @throws SQLException In case of database errors.
   */
  public static Map<ScenarioResultType, ScenarioEmissionResults> getScenarioResults(final Connection connection, final String jobKey,
      final int receptorId) throws SQLException {
    final Map<ScenarioResultType, ScenarioEmissionResults> scenarioResults = new EnumMap<>(ScenarioResultType.class);
    try (final PreparedStatement statement = connection.prepareStatement(GET_RECEPTOR_SCENARIO_RESULTS.get())) {
      GET_RECEPTOR_SCENARIO_RESULTS.setParameter(statement, QueryAttribute.KEY, jobKey);
      GET_RECEPTOR_SCENARIO_RESULTS.setParameter(statement, QueryAttribute.RECEPTOR_ID, receptorId);

      try (final ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          final ScenarioResultType scenarioResultType = QueryUtil.getEnum(rs, QueryAttribute.SCENARIO_RESULT_TYPE, ScenarioResultType.class);
          final ScenarioEmissionResults scenarioEmissionResults = scenarioResults
              .computeIfAbsent(scenarioResultType, x -> new ScenarioEmissionResults());
          EmissionResultRepositoryUtil.addEmissionResult(rs, scenarioEmissionResults.getResults().getHashMap());
          EmissionResultRepositoryUtil.addResultValue(rs, scenarioEmissionResults.getTotalResults().getHashMap(), QueryAttribute.TOTAL_RESULT);
        }
      }
    }
    return scenarioResults;
  }

  /**
   * Retrieve the archive contributions for a certain calculation.
   *
   * @param connection The connection to use
   * @param calculationId The ID of the calculation to retrieve results for
   * @return The (current) list of CalculationPointFeatures with results that match this calculation ID
   * @throws SQLException If an error occurred communicating with the DB
   */
  public static List<CalculationPointFeature> getArchiveContributions(final Connection connection, final int calculationId) throws SQLException {
    final List<CalculationPointFeature> results = new ArrayList<>();
    results.addAll(getArchiveReceptors(connection, calculationId));
    results.addAll(getArchiveSubPoints(connection, calculationId));
    return results;
  }

  private static List<CalculationPointFeature> getArchiveReceptors(final Connection connection, final int calculationId)
      throws SQLException {
    final List<CalculationPointFeature> receptors = new ArrayList<>();

    final ReceptorUtil receptorUtil = new ReceptorUtil(ReceptorGridSettingsRepository.getReceptorGridSettings(connection));
    try (final PreparedStatement ps = connection.prepareStatement(GET_ALL_RECEPTOR_ARCHIVE_RESULTS.get())) {
      GET_ALL_RECEPTOR_ARCHIVE_RESULTS.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);

      receptors.addAll(handleReceptorQuery(ps, receptorUtil));
    }

    return receptors;
  }

  private static List<CalculationPointFeature> getArchiveSubPoints(final Connection connection, final int calculationId)
      throws SQLException {
    final List<CalculationPointFeature> points = new ArrayList<>();

    try (final PreparedStatement ps = connection.prepareStatement(GET_ALL_SUB_POINT_ARCHIVE_RESULTS.get())) {
      GET_ALL_SUB_POINT_ARCHIVE_RESULTS.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);

      points.addAll(handleSubPointQuery(ps));
    }

    return points;
  }

  private static Collection<CalculationPointFeature> handleReceptorQuery(final PreparedStatement ps, final ReceptorUtil receptorUtil)
      throws SQLException {
    final HashMap<Integer, CalculationPointFeature> hashedResults = new HashMap<>();
    try (final ResultSet rs = ps.executeQuery()) {
      while (rs.next()) {
        final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
        final CalculationPointFeature receptorPoint = CalculationRepositoryUtil.toReceptorFeature(receptorUtil, receptorId);

        EmissionResultRepositoryUtil.addResultsToPoint(rs, hashedResults, receptorPoint, receptorId);
      }
    }
    return hashedResults.values();
  }

  private static Collection<CalculationPointFeature> handleSubPointQuery(final PreparedStatement ps) throws SQLException {
    final Map<SubPointKey, CalculationPointFeature> hashedResults = new HashMap<>();
    try (final ResultSet rs = ps.executeQuery()) {

      while (rs.next()) {
        final int subPointId = QueryAttribute.CALCULATION_SUB_POINT_ID.getInt(rs);
        final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
        final int level = QueryAttribute.LEVEL.getInt(rs);
        final double x = QueryUtil.getDouble(rs, QueryAttribute.X_COORD);
        final double y = QueryUtil.getDouble(rs, QueryAttribute.Y_COORD);
        final CalculationPointFeature calculationPoint = CalculationRepositoryUtil.toSubPointFeature(subPointId, receptorId, level, x, y);

        EmissionResultRepositoryUtil.addResultsToPoint(rs, hashedResults, calculationPoint, new SubPointKey(receptorId, subPointId));
      }
    }
    return hashedResults.values();
  }

  private static QueryBuilder getQueryResultBase(final String fromView) {
    return QueryBuilder.from(fromView)
        .select(EmissionResultRepositoryUtil.REQUIRED_ATTRIBUTES)
        .where(QueryAttribute.CALCULATION_ID);
  }

  private static QueryBuilder getAllReceptorResultsBase(final String fromView) {
    return getQueryResultBase(fromView)
        .select(QueryAttribute.RECEPTOR_ID);
  }

  private static QueryBuilder getQueryCalculationPointResultsBase(final String fromView) {
    return getQueryResultBase(fromView)
        .select(QueryAttribute.CALCULATION_POINT_ID);
  }

  private static QueryBuilder getQuerySubPointResultsBase(final String fromView) {
    return getQueryResultBase(fromView)
        .select(QueryAttribute.CALCULATION_SUB_POINT_ID, QueryAttribute.RECEPTOR_ID, QueryAttribute.LEVEL)
        .select(new SelectClause("ST_X(geometry)", QueryAttribute.X_COORD.attribute()),
            new SelectClause("ST_Y(geometry)", QueryAttribute.Y_COORD.attribute()))
        .where(new StaticWhereClause("level != 0"));
  }

}
