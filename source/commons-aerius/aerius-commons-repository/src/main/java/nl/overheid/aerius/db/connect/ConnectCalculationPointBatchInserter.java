/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.connect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Optional;

import nl.overheid.aerius.db.BatchInserter;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.ops.OPSCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.ops.OPSTerrainProperties;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 *
 */
public class ConnectCalculationPointBatchInserter extends BatchInserter<CalculationPointFeature> {

  private final Connection connection;
  private final int calculationPointSetId;
  private final ReceptorGridSettings rgs;
  private final ReceptorUtil receptorUtil;

  /**
   * @throws SQLException
   *
   */
  public ConnectCalculationPointBatchInserter(final Connection con, final int calculationPointSetId) throws SQLException {
    this.connection = con;
    this.calculationPointSetId = calculationPointSetId;
    this.rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(connection);
    this.receptorUtil = new ReceptorUtil(rgs);

  }

  @Override
  public void setParameters(final PreparedStatement ps, final CalculationPointFeature calculationPointFeature) throws SQLException {
    final Point geometry = calculationPointFeature.getGeometry();
    final CalculationPoint calculationPoint = calculationPointFeature.getProperties();
    final int nearestReceptorId = receptorUtil.getReceptorIdFromCoordinate(geometry.getX(), geometry.getY(), rgs.getZoomLevel1());
    int paramIdx = 1;
    ps.setInt(paramIdx, calculationPointSetId);
    paramIdx++;
    ps.setInt(paramIdx, calculationPoint.getId());
    paramIdx++;
    ps.setString(paramIdx, calculationPoint.getLabel());
    paramIdx++;
    ps.setInt(paramIdx, nearestReceptorId);
    paramIdx++;
    ps.setDouble(paramIdx, geometry.getX());
    paramIdx++;
    ps.setDouble(paramIdx, geometry.getY());
    paramIdx++;
    final Optional<OPSCustomCalculationPoint> opsReceptor;
    if (calculationPoint instanceof OPSCustomCalculationPoint) {
      opsReceptor = Optional.of((OPSCustomCalculationPoint) calculationPoint);
    } else {
      opsReceptor = Optional.empty();
    }
    if (opsReceptor.isPresent() && opsReceptor.get().getTerrainProperties() != null) {
      paramIdx = addTerrainNotNulls(ps, paramIdx, opsReceptor.get().getTerrainProperties());
    } else {
      paramIdx = addTerrainNulls(ps, paramIdx);
    }
    if (opsReceptor.isPresent() && opsReceptor.get().getHeight() != null) {
      ps.setDouble(paramIdx, opsReceptor.get().getHeight());
    } else {
      ps.setNull(paramIdx, Types.DOUBLE);
    }
    paramIdx++;
    ps.setString(paramIdx, calculationPoint.getClass().getName());
  }

  /**
   * @param ps
   * @param paramIdx
   * @param isReceptor
   * @throws SQLException
   */
  private int addTerrainNotNulls(final PreparedStatement ps, int paramIdx, final OPSTerrainProperties terrainProperties) throws SQLException {
    final Double avg = terrainProperties.getAverageRoughness();
    // The man rcp file has empty landuse columns.
    ps.setDouble(paramIdx, avg);
    paramIdx++;
    ps.setInt(paramIdx, terrainProperties.getLandUse().getOption());
    paramIdx++;
    final int[] landUses = terrainProperties.getLandUses();
    final Integer[] landUsesAsObjects = new Integer[landUses.length];
    for (int count = 0; count < landUses.length; count++) {
      landUsesAsObjects[count] = landUses[count];
    }
    ps.setArray(paramIdx++, connection.createArrayOf("integer", landUsesAsObjects));
    return paramIdx;
  }

  /**
   * @param ps
   * @param paramIdx
   * @throws SQLException
   */
  private int addTerrainNulls(final PreparedStatement ps, int paramIdx) throws SQLException {
    ps.setNull(paramIdx, Types.DOUBLE);
    paramIdx++;
    ps.setNull(paramIdx, Types.INTEGER);
    paramIdx++;
    ps.setNull(paramIdx, Types.ARRAY);
    paramIdx++;
    return paramIdx;
  }

}
