/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.connect;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 *
 */
public class ConnectUserRepositoryBean {

  private static final Logger LOG = LoggerFactory.getLogger(ConnectUserRepositoryBean.class);

  private final PMF pmf;

  public ConnectUserRepositoryBean(final PMF pmf) {
    this.pmf = pmf;
  }

  /**
   * Creates a Connect user. The ConnectUser object's id will be updated.
   *
   * @param user The user to create (id is ignored / max concurrent jobs is automatically filled).
   */
  public void createUser(final ConnectUser user) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      ConnectUserRepository.createUser(con, user);
    } catch (final SQLException e) {
      LOG.error("SQL error while creating user", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public void updateApiKeyForUser(final ConnectUser user) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      ConnectUserRepository.updateUser(con, user);
    } catch (final SQLException e) {
      LOG.error("SQL error while updating API key", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Fetch a Connect user from the database using the email address for lookup.
   * Returns null when the user is not found.
   */
  public ConnectUser getUserByEmailAddress(final String email) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ConnectUserRepository.getUserByEmailAddress(con, email);
    } catch (final SQLException e) {
      LOG.error("SQL error while retrieving user by email", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Fetch a Connect user from the database using the api key for lookup.
   * Returns null when the user is not found.
   */
  public ConnectUser getUserByApiKey(final String apiKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ConnectUserRepository.getUserByApiKey(con, apiKey);
    } catch (final SQLException e) {
      LOG.error("SQL error while retrieving user by API key", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

}
