/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.shared.domain.HasId;

/**
 * Abstract helper class to ensure an entity only occurs once.
 * @param <T> The class of the entity.
 */
public abstract class EntityCollector<T extends HasId> {

  private final Map<Integer, T> entityMap = new LinkedHashMap<>();

  protected EntityCollector() {
    //protected to ensure it won't be used outside package.
  }

  /**
   * @param rs The resultset that contains the entity information.
   * getNext() won't be called.
   * @return the entity that matches the resultset (either new object or already existing one).
   * @throws SQLException in case of database errors.
   */
  public T collectEntity(final ResultSet rs) throws SQLException {
    final int entityId = getKey(rs);
    T entity;
    if (!hasEntity(entityId)) {
      entity = fillEntity(rs);
      entity.setId(entityId);
      entityMap.put(entityId, entity);
    } else {
      entity = entityMap.get(entityId);
    }

    appendEntity(entity, rs);
    return entity;
  }

  public void collectEntities(final ResultSet rs) throws SQLException {
    while (rs.next()) {
      collectEntity(rs);
    }
  }

  /**
   * @return all entities that are collected so far by this collector.
   */
  public ArrayList<T> getEntities() {
    return new ArrayList<>(entityMap.values());
  }

  /**
   * @return the map of Key,Entity combinations used by this collector.
   */
  public Map<Integer, T> getEntityMap() {
    return entityMap;
  }

  /**
   * Implement to ensure the right key belongs to the entity.
   * @param rs The resultset which contains the key.
   * @return The proper key from that resultset.
   * @throws SQLException in case of database errors.
   */
  public abstract int getKey(final ResultSet rs) throws SQLException;

  /**
   * Implement to ensure the entity is filled the right way.
   * @param rs Fill the current entity in the resultset.
   * @return The entity based on the resultset.
   * @throws SQLException in case of database errors.
   */
  public abstract T fillEntity(final ResultSet rs) throws SQLException;

  /**
   * Can optionally be overridden to append data to an entity.
   * This is ensured to be called for every record, so also when the key already exists.
   * Where fillEntity() will only be called once for each occurrence of the key (getKey()) in a set,
   * this function will be called for each call to collectEntity().
   * @param rs Resultset with the current record.
   * @param entity The entity to fill (append).
   * @throws SQLException in case of database errors.
   */
  public void appendEntity(final T entity, final ResultSet rs) throws SQLException {
    // noop, for override purposes
  }

  /**
   * Convenience method to determine if the entity exists.
   * @param entityKey The key for the entity (the ID).
   * @return True if the entity exists (already) for this collector
   */
  public boolean hasEntity(final int entityKey) {
    return entityMap.containsKey(entityKey);
  }

  /**
   * Convenience method to retrieve an entity from the collector.
   * @param entityKey he key for the entity (the ID).
   * @return The proper entity (or null if not found).
   */
  public T getEntity(final int entityKey) {
    return entityMap.get(entityKey);
  }

  /**
   * Collect all entities based on a Query.
   * @param connection The connection to use to run the query.
   * @param query The query to run. Expected to not have any parameters.
   * @throws SQLException in case of database errors.
   */
  public void collect(final Connection connection, final Query query) throws SQLException {
    try (PreparedStatement ps = connection.prepareStatement(query.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        collectEntity(rs);
      }
    }
  }

}
