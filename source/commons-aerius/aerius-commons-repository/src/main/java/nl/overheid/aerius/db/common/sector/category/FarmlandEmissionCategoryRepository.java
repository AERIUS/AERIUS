/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;

/**
 *
 */
public final class FarmlandEmissionCategoryRepository {

  private enum RepositoryAttribute implements Attribute {

    FARMLAND_CATEGORY_ID;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  /**
   */
  private static final Query FARMLAND_EMISSION_CATEGORY_QUERY = QueryBuilder.from("farmland_categories_view")
      .select(RepositoryAttribute.FARMLAND_CATEGORY_ID, QueryAttribute.CODE, QueryAttribute.NAME, QueryAttribute.SECTOR_ID)
      .getQuery();

  private FarmlandEmissionCategoryRepository() {
    // util class
  }

  /**
   * Returns all Farmland Emission Categories.
   *
   * @param con Database connection
   * @param messagesKey DBMessagesKey to use for i18n stuff
   * @return ArrayList containing all farmland emission categories.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static List<FarmlandCategory> findAllFarmlandEmissionCategories(final Connection con, final DBMessagesKey messagesKey)
      throws SQLException {

    final AbstractCategoryCollector<FarmlandCategory> collector =
        new AbstractCategoryCollector<FarmlandCategory>(RepositoryAttribute.FARMLAND_CATEGORY_ID,
            messagesKey) {
          @Override
          FarmlandCategory getNewCategory() throws SQLException {
            return new FarmlandCategory();
          }

          @Override
          void setDescription(final FarmlandCategory category, final ResultSet rs) throws SQLException {
            DBMessages.setFarmlandMessages(category, messagesKey);
          }

          @Override
          void setRemainingInfo(final FarmlandCategory category, final ResultSet rs) throws SQLException {
            category.setSectorCode(QueryAttribute.SECTOR_ID.getInt(rs));
          }

        };

    collector.collect(con, FARMLAND_EMISSION_CATEGORY_QUERY);

    return collector.getEntities();
  }
}
