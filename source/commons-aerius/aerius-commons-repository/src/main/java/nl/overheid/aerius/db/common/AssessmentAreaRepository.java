/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.postgis.PGbox2d;
import org.postgis.PGgeometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.geo.PGisUtils;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Repository class to use for assessment area selections.
 */
public final class AssessmentAreaRepository {

  private enum RepositoryAttribute implements Attribute {
    NATURA2000_DIRECTIVE_ID,
    DIRECTIVE_CODE,
    DIRECTIVE_NAME,
    DIRECTIVE_CODES;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ROOT);
    }
  }

  private static final Logger LOG = LoggerFactory.getLogger(AssessmentAreaRepository.class);

  private static final String NATURE2000_AREAS = "natura2000_areas";

  private static final Query NATURA2000_AREAS_LIST = QueryBuilder.from(NATURE2000_AREAS)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.NAME).orderBy(QueryAttribute.NAME).getQuery();

  private static final Query ASSESSMENT_AREA = QueryBuilder.from("assessment_areas").select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.NAME)
      .select(new SelectClause("Box2D(geometry)", QueryAttribute.BOUNDINGBOX.name()))
      .select(new SelectClause("ST_PointOnSurface(geometry)", QueryAttribute.MARKER_LOCATION.name()))
      .where(QueryAttribute.ASSESSMENT_AREA_ID).getQuery();

  private static final Query NATURA2000_AREAS_BOX_LIST = QueryBuilder.from(NATURE2000_AREAS)
      .join(new JoinClause("assessment_area_directive_view", QueryAttribute.ASSESSMENT_AREA_ID))
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.NAME, RepositoryAttribute.DIRECTIVE_CODES)
      .select(new SelectClause("Box2D(geometry)", QueryAttribute.BOUNDINGBOX.name()))
      .select(new SelectClause("ST_PointOnSurface(geometry)", QueryAttribute.MARKER_LOCATION.name()))
      .getQuery();

  private static final Query NATURA2000_DIRECTIVES_LIST = QueryBuilder.from("natura2000_directives")
      .select(RepositoryAttribute.NATURA2000_DIRECTIVE_ID, RepositoryAttribute.DIRECTIVE_CODE, RepositoryAttribute.DIRECTIVE_NAME)
      .orderBy(RepositoryAttribute.DIRECTIVE_CODE)
      .getQuery();

  // Queries to get intersect with habitats and a given geometry. Added a 1 meter buffer to the returning intersection to
  // handle edge cases that touch the geometry and therefore should be included.
  private static final String BOUNDED_RELEVANT_HABITATS =
      "SELECT ST_AsText(ST_Intersection(ST_Union(geometry), ST_Buffer(ST_SetSRID(?::geometry, ae_get_srid()), 1))) "
          + "FROM relevant_habitat_areas WHERE ST_Intersects(ST_SetSRID(?::geometry, ae_get_srid()), geometry)";

  private static final String BOUNDED_ASSESSMENT_AREAS =
      "SELECT ST_AsText(ST_Intersection(ST_Union(geometry), ST_Buffer(ST_SetSRID(?::geometry, ae_get_srid()), 1))) "
          + "FROM assessment_areas WHERE ST_Intersects(ST_SetSRID(?::geometry, ae_get_srid()), geometry)";

  // Not allowed to instantiate.
  private AssessmentAreaRepository() {
  }

  /**
   * Retrieve a list of all n2k areas available in the database.
   * @param con The connection to use.
   * @return The list of all n2k areas (just names and assessment area IDs)
   * @throws SQLException When an error occurs executing the query.
   */
  public static List<AssessmentArea> getNatura2kAreas(final Connection con) throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(NATURA2000_AREAS_LIST.get())) {
      final ResultSet rs = statement.executeQuery();

      final List<AssessmentArea> lst = new ArrayList<>();

      while (rs.next()) {
        final AssessmentArea info = new AssessmentArea();
        info.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
        info.setName(QueryAttribute.NAME.getString(rs));
        lst.add(info);
      }

      return lst;
    }
  }

  /**
   * @param con The connection to use.
   * @param assessmentAreaId The ID of the assessment area to retrieve.
   * @return The assessment area corresponding to the ID, or null if not found.
   * @throws SQLException In case of database exceptions.
   */
  public static AssessmentArea getAssessmentArea(final Connection con, final int assessmentAreaId) throws SQLException {
    AssessmentArea area = null;
    try (final PreparedStatement stmt = con.prepareStatement(ASSESSMENT_AREA.get())) {
      ASSESSMENT_AREA.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);

      final ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        area = new AssessmentArea();
        area.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
        area.setName(QueryUtil.getString(rs, QueryAttribute.NAME));
        area.setBounds(PGisUtils.getBox((PGbox2d) QueryAttribute.BOUNDINGBOX.getObject(rs)));
        area.setMarkerLocation(PGisUtils.getPoint((PGgeometry) QueryUtil.getObject(rs, QueryAttribute.MARKER_LOCATION)));
      }
    }
    return area;
  }

  /**
   * @param con The connection to use.
   * @return All assessment area.
   * @throws SQLException In case of database exceptions.
   */
  public static Map<Integer, AssessmentArea> getAssessmentAreas(final Connection con) throws SQLException {
    final Map<Integer, AssessmentArea> areas = new HashMap<>();
    try (final PreparedStatement stmt = con.prepareStatement(NATURA2000_AREAS_BOX_LIST.get())) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final AssessmentArea area = new AssessmentArea();
          area.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
          area.setName(QueryUtil.getString(rs, QueryAttribute.NAME));
          area.setDirectiveCodes(QueryUtil.getString(rs, RepositoryAttribute.DIRECTIVE_CODES));
          area.setBounds(PGisUtils.getBox((PGbox2d) QueryAttribute.BOUNDINGBOX.getObject(rs)));
          area.setMarkerLocation(PGisUtils.getPoint((PGgeometry) QueryUtil.getObject(rs, QueryAttribute.MARKER_LOCATION)));
          areas.put(area.getId(), area);
        }
      }
    }
    return areas;
  }

  /**
   * @param con The connection to use.
   * @return All nature directives.
   * @throws SQLException In case of database exceptions.
   */
  public static List<SimpleCategory> getNatureAreaDirectives(final Connection con) throws SQLException {
    final List<SimpleCategory> directives = new ArrayList<>();
    try (final PreparedStatement stmt = con.prepareStatement(NATURA2000_DIRECTIVES_LIST.get())) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final String name = QueryUtil.getString(rs, RepositoryAttribute.DIRECTIVE_NAME);
          final SimpleCategory directive = new SimpleCategory(
              QueryUtil.getInt(rs, RepositoryAttribute.NATURA2000_DIRECTIVE_ID),
              QueryUtil.getString(rs, RepositoryAttribute.DIRECTIVE_CODE),
              name, name);
          directives.add(directive);
        }
      }
    }
    return directives;
  }

  /**
   * Returns the geometry of all habitats that intersect with the given geometry.
   *
   * @param con The connection to use
   * @param geometry geometry to check for intersection
   * @return geometry of all habitats that intersect
   * @throws AeriusException In case geometry could not be converted
   * @throws SQLException In case of database exceptions
   * @throws ParseException In case wkt read from the database could not be parsed
   */
  public static org.locationtech.jts.geom.Geometry relevantHabitatsInsideGeometry(final Connection con, final Geometry geometry)
      throws SQLException, AeriusException {
    return relevantInsideGeometry(con, BOUNDED_RELEVANT_HABITATS, geometry);
  }

  /**
   * Returns the geometry of the nature area that intersect with the given geometry.
   *
   * @param con The connection to use
   * @param geometry geometry to check for intersection
   * @return geometry of the nature area that intersect
   * @throws AeriusException In case geometry could not be converted
   * @throws SQLException In case of database exceptions
   * @throws ParseException In case wkt read from the database could not be parsed
   */
  public static org.locationtech.jts.geom.Geometry relevantAssessmentAreaInsideGeometry(final Connection con, final Geometry geometry)
      throws SQLException, AeriusException {
    return relevantInsideGeometry(con, BOUNDED_ASSESSMENT_AREAS, geometry);
  }

  private static org.locationtech.jts.geom.Geometry relevantInsideGeometry(final Connection con, final String query, final Geometry geometry)
      throws SQLException, AeriusException {
    try (final PreparedStatement stmt = con.prepareStatement(query)) {
      final org.locationtech.jts.geom.Geometry jtsGeometry = GeometryUtil.getGeometry(geometry);
      final PGgeometry pgGeometry = PGisUtils.getPGgeometry(jtsGeometry.toText());

      stmt.setObject(1, pgGeometry);
      stmt.setObject(2, pgGeometry);
      final ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        final String wktString = rs.getString(1);

        if (wktString != null) {
          return new WKTReader().read(wktString);
        }
      }
      return null;
    } catch (final ParseException e) {
      LOG.error("Could not parse geometry return by quering for relevant habitats inside geometry: {}", geometry);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }
}
