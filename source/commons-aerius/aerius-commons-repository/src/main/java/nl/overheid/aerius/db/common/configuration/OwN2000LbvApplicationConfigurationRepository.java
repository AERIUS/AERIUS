/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.configuration;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;

/**
 * Configuration repository for LBV product variant.
 */
public class OwN2000LbvApplicationConfigurationRepository extends OwN2000ApplicationConfigurationRepository {

  private static final List<CalculationJobType> LBV_CALCULATION_JOB_TYPES = Arrays.asList(CalculationJobType.DEPOSITION_SUM);

  private static final String MANUAL_PRODUCT = "calculator-lbv-policy";

  @Override
  protected List<CalculationJobType> getCalculationJobTypes() {
    return LBV_CALCULATION_JOB_TYPES;
  }

  @Override
  protected Map<CalculationJobType, JobType> getDefaultExportJobTypes() {
    final Map<CalculationJobType, JobType> defaultExportJobTypes = new HashMap<>();
    defaultExportJobTypes.put(CalculationJobType.DEPOSITION_SUM, JobType.REPORT);
    return defaultExportJobTypes;
  }

  @Override
  protected List<CalculationMethod> getCalculationMethods() {
    return Arrays.asList(CalculationMethod.FORMAL_ASSESSMENT);
  }

  @Override
  protected List<CalculationJobType> getAvailableExportReportOptions() {
    return Arrays.asList(CalculationJobType.DEPOSITION_SUM);
  }

  @Override
  protected SituationType getDefaultSituationType() {
    return SituationType.REFERENCE;
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected Enum getManualHost() {
    return ConstantsEnum.MANUAL_BASE_URL_LBV_POLICY;
  }

  @Override
  protected ManualBasePath getManualBasePath(final Connection con) throws SQLException {
    final ManualBasePath basePath = super.getManualBasePath(con);
    return new ManualBasePath(MANUAL_PRODUCT, basePath.version(), basePath.language());
  }

  @Override
  protected void addSettings(final Connection con, final ApplicationConfiguration config) throws SQLException {
    // Add profile dependent settings with override
    SettingsUtil.addOptionalSettingPreferEnvironment(con, config.getSettings(), SharedConstantsEnum.QUICK_START_URL,
        ConstantsEnum.QUICK_START_URL_LBV_POLICY);
  }
}
