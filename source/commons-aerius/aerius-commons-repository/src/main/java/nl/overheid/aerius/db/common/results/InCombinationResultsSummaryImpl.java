/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsEdgeReceptor;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SummaryRequest;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

class InCombinationResultsSummaryImpl extends ResultsSummaryImpl<InCombinationResultsSummaryInput> {

  private static final Query QUERY_OVERALL_DEPOSITION_STATISTICS = QueryBuilder
      .from("jobs.ae_scenario_in_combination_statistics(?, ?::hexagon_type)",
          QueryAttribute.PROPOSED_CALCULATION_ID, RepositoryAttribute.HEXAGON_TYPE)
      .where(WHERE_EMISSION_RESULT_TYPE, WHERE_SUBSTANCE_ID)
      .getQuery();

  private static final Query QUERY_AREA_DEPOSITION_STATISTICS = QueryBuilder
      .from("jobs.scenario_calculation_assessment_area_statistics_view")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.HEXAGON_TYPE, RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.VALUE)
      .where(QueryAttribute.CALCULATION_ID)
      .where(COMMON_WHERE_CLAUSES)
      .getQuery();

  private static final Query QUERY_AREA_CHART_STATISTICS = QueryBuilder
      .from("jobs.scenario_calculation_assessment_area_chart_statistics_view")
      .select(RepositoryAttribute.LOWER_BOUND, QueryAttribute.CARTOGRAPHIC_SURFACE)
      .where(QueryAttribute.CALCULATION_ID, QueryAttribute.ASSESSMENT_AREA_ID)
      .where(WHERE_CHART_TYPE)
      .where(COMMON_WHERE_CLAUSES)
      .getQuery();

  private static final Query QUERY_AREA_DEPOSITION_STATISTIC_MARKERS = QueryBuilder
      .from("jobs.scenario_calculation_assessment_area_statistic_markers_view")
      .select(RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.RECEPTOR_ID)
      .where(QueryAttribute.CALCULATION_ID)
      .where(COMMON_WHERE_CLAUSES)
      .getQuery();

  private static final Query QUERY_HABITAT_DEPOSITION_STATISTICS = QueryBuilder
      .from("jobs.scenario_calculation_critical_deposition_area_statistics_view")
      .select(QueryAttribute.CRITICAL_DEPOSITION_AREA_ID, RepositoryAttribute.HEXAGON_TYPE, RepositoryAttribute.RESULT_STATISTIC_TYPE,
          QueryAttribute.VALUE)
      .where(QueryAttribute.CALCULATION_ID, QueryAttribute.ASSESSMENT_AREA_ID)
      .where(COMMON_WHERE_CLAUSES)
      .getQuery();

  private static final Query QUERY_RECEPTOR_STATISTICS = QueryBuilder
      .from("jobs.ae_scenario_calculation_receptor_statistics(?::scenario_result_type, ?)", RepositoryAttribute.SCENARIO_RESULT_TYPE,
          QueryAttribute.CALCULATION_ID)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.RECEPTOR_ID)
      .where(WHERE_HEXAGON_TYPE, WHERE_OVERLAPPING_HEXAGON_TYPE, WHERE_EMISSION_RESULT_TYPE, WHERE_SUBSTANCE_ID)
      .getQuery();

  private static final Query QUERY_CALCULATION_POINTS = QueryBuilder
      .from("jobs.ae_scenario_calculation_point_in_combination_results(?, ?)",
          QueryAttribute.JOB_ID, QueryAttribute.PROPOSED_CALCULATION_ID)
      .select(QueryAttribute.CALCULATION_POINT_ID, QueryAttribute.GEOMETRY, QueryAttribute.RESULT)
      .where(WHERE_EMISSION_RESULT_TYPE, WHERE_SUBSTANCE_ID)
      .getQuery();

  private static final Query PREPROCESS_STATISTICS = QueryBuilder
      .from("jobs.ae_fill_in_combination_results(?, ?)",
          QueryAttribute.JOB_ID, QueryAttribute.PROPOSED_CALCULATION_ID)
      .getQuery();

  private static final Query INSERT_STATISTICS = QueryBuilder
      .from("jobs.ae_scenario_in_combination_fill_calculation_statistics(?, ?)",
          QueryAttribute.JOB_ID, QueryAttribute.PROPOSED_CALCULATION_ID)
      .getQuery();

  public InCombinationResultsSummaryImpl(final PMF pmf, final ReceptorUtil receptorUtil) {
    super(pmf, receptorUtil);
  }

  @Override
  protected InCombinationResultsSummaryInput createSummaryInput(final SituationCalculations situationCalculations, final Integer calculationId) {
    return InCombinationResultsSummaryInput.fromSituationCalculations(calculationId);
  }

  @Override
  protected List<InCombinationResultsSummaryInput> createSummaryInputs(final SituationCalculations situationCalculations) {
    return situationCalculations.getCalculationIdsOfSituationType(SituationType.PROPOSED).stream()
        .map(proposedCalculationId -> createSummaryInput(situationCalculations, proposedCalculationId))
        .collect(Collectors.toList());
  }

  @Override
  protected PreparedStatement prepareOverallStatisticsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final InCombinationResultsSummaryInput summaryInput) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_OVERALL_DEPOSITION_STATISTICS.get());
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.PROPOSED_CALCULATION_ID, summaryInput.getProposedCalculationId());
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE,
        summaryRequest.getHexagonType().name().toLowerCase(Locale.ROOT), Types.OTHER);
    setCommonQueryValues(QUERY_OVERALL_DEPOSITION_STATISTICS, stmt, summaryRequest);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareAreaStatisticsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final InCombinationResultsSummaryInput summaryInput)
      throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_DEPOSITION_STATISTICS.get());
    QUERY_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    setCommonQueryValues(QUERY_AREA_DEPOSITION_STATISTICS, stmt, summaryRequest);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareHabitatStatisticsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final InCombinationResultsSummaryInput summaryInput,
      final int assessmentAreaId) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_HABITAT_DEPOSITION_STATISTICS.get());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    setCommonQueryValues(QUERY_HABITAT_DEPOSITION_STATISTICS, stmt, summaryRequest);
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareChartStatisticsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final InCombinationResultsSummaryInput summaryInput, final int assessmentAreaId, final EmissionResultChartType chartType)
      throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_CHART_STATISTICS.get());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    setCommonQueryValues(QUERY_AREA_CHART_STATISTICS, stmt, summaryRequest);
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, RepositoryAttribute.EMISSION_RESULT_CHART_TYPE, chartType.name().toLowerCase(Locale.ROOT));
    return stmt;
  }

  @Override
  protected PreparedStatement prepareStatisticMarkersStatement(final Connection connection, final SummaryRequest summaryRequest,
      final InCombinationResultsSummaryInput summaryInput) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.get());
    QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    setCommonQueryValues(QUERY_AREA_DEPOSITION_STATISTIC_MARKERS, stmt, summaryRequest);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareReceptorStatisticsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final InCombinationResultsSummaryInput summaryInput)
      throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_RECEPTOR_STATISTICS.get());
    QUERY_RECEPTOR_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    setCommonQueryValues(QUERY_RECEPTOR_STATISTICS, stmt, summaryRequest);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareCustomCalculationPointResultsStatement(final Connection connection, final SummaryRequest summaryRequest,
      final InCombinationResultsSummaryInput summaryInput) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_CALCULATION_POINTS.get());
    QUERY_CALCULATION_POINTS.setParameter(stmt, QueryAttribute.JOB_ID, summaryRequest.getJobId());
    QUERY_CALCULATION_POINTS.setParameter(stmt, QueryAttribute.PROPOSED_CALCULATION_ID, summaryInput.getProposedCalculationId());
    setCommonQueryValues(QUERY_CALCULATION_POINTS, stmt, summaryRequest);
    return stmt;
  }

  @Override
  protected void preprocessSummaryStep(final Connection connection, final int jobId, final InCombinationResultsSummaryInput summaryInput)
      throws SQLException {
    try (final PreparedStatement stmt = connection.prepareStatement(PREPROCESS_STATISTICS.get())) {
      PREPROCESS_STATISTICS.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
      PREPROCESS_STATISTICS.setParameter(stmt, QueryAttribute.PROPOSED_CALCULATION_ID, summaryInput.getProposedCalculationId());
      stmt.execute();
    }
  }

  @Override
  protected PreparedStatement prepareInsertResultsSummaryStatement(final Connection connection, final int jobId,
      final InCombinationResultsSummaryInput summaryInput) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(INSERT_STATISTICS.get());
    INSERT_STATISTICS.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
    INSERT_STATISTICS.setParameter(stmt, QueryAttribute.PROPOSED_CALCULATION_ID, summaryInput.getProposedCalculationId());
    return stmt;
  }

  @Override
  protected Map<Integer, List<SituationResultsEdgeReceptor>> determineEdgeReceptors(final Connection connection, final SummaryRequest summaryRequest,
      final InCombinationResultsSummaryInput summaryInput) throws SQLException {
    return Collections.emptyMap();
  }

  @Override
  protected int compare(final SituationResultsStatistics statistics1, final SituationResultsStatistics statistics2) {
    return compareValues(
        statistics1.get(ResultStatisticType.MAX_INCREASE),
        statistics2.get(ResultStatisticType.MAX_INCREASE));
  }

}
