/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.CriticalLevels;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.LocaleUtils;

public class GeneralRepositoryBean {

  private static final Logger LOG = LoggerFactory.getLogger(GeneralRepositoryBean.class);

  private final PMF pmf;

  private Map<Integer, AssessmentArea> cachedAssessmentAreas;
  private final Map<Locale, Map<Integer, HabitatType>> cachedHabitatTypes = new HashMap<>();
  private Map<Integer, CriticalLevels> cachedHabitatTypesCriticalLevels;
  private Map<Integer, Map<EmissionResultKey, Boolean>> cachedHabitatTypesSensitiveness;

  public GeneralRepositoryBean(final PMF pmf) {
    this.pmf = pmf;
  }

  /**
   * Determine the assessment area for an ID.
   * Will return an empty optional if not found or if a database error occurs.
   */
  public Optional<AssessmentArea> determineAssessmentArea(final int assessmentAreaId) {
    try {
      return Optional.ofNullable(getAssessmentAreas().get(assessmentAreaId));
    } catch (final AeriusException e) {
      LOG.error("Error while retrieving assessment areas", e);
      return Optional.empty();
    }
  }

  /**
   * @return All assessment area.
   */
  public Map<Integer, AssessmentArea> getAssessmentAreas() throws AeriusException {
    if (cachedAssessmentAreas == null) {
      cachedAssessmentAreas = getAssessmentAreasFromDB();
    }
    return cachedAssessmentAreas;
  }

  private Map<Integer, AssessmentArea> getAssessmentAreasFromDB() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return AssessmentAreaRepository.getAssessmentAreas(con);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting assessment area information", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Determine the habitat type for an ID.
   * Will return an empty optional if not found or if a database error occurs.
   */
  public Optional<HabitatType> determineHabitatType(final int habitatTypeId) {
    try {
      return Optional.ofNullable(getHabitatTypes(LocaleUtils.getDefaultLocale()).get(habitatTypeId));
    } catch (final AeriusException e) {
      LOG.error("Error while retrieving habitat types", e);
      return Optional.empty();
    }
  }

  /**
   * Determine the critical levels for a habitat type based on an ID.
   * Will return an empty optional if not found or if a database error occurs.
   */
  public Optional<CriticalLevels> determineHabitatTypeCriticalLevels(final int habitatTypeId) {
    try {
      return Optional.ofNullable(getHabitatTypesCriticalLevels().get(habitatTypeId));
    } catch (final AeriusException e) {
      LOG.error("Error while retrieving habitat type critical levels", e);
      return Optional.empty();
    }
  }

  /**
   * @return All habitat types.
   */
  public Map<Integer, HabitatType> getHabitatTypes(final Locale locale) throws AeriusException {
    if (!cachedHabitatTypes.containsKey(locale)) {
      cachedHabitatTypes.put(locale, getHabitatTypesFromDB(locale));
    }
    return cachedHabitatTypes.get(locale);
  }

  private Map<Integer, HabitatType> getHabitatTypesFromDB(final Locale locale) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final DBMessagesKey messagesKey = new DBMessagesKey(locale);
      final List<HabitatType> habitatTypes = HabitatRepository.getHabitatTypes(con, messagesKey);
      return habitatTypes.stream().collect(Collectors.toMap(HabitatType::getId, Function.identity()));
    } catch (final SQLException e) {
      LOG.error("SQL error while getting habitat type information", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * @return All known critical levels for habitat types.
   */
  public Map<Integer, CriticalLevels> getHabitatTypesCriticalLevels() throws AeriusException {
    if (cachedHabitatTypesCriticalLevels == null) {
      cachedHabitatTypesCriticalLevels = getHabitatTypesCriticalLevelsFromDB();
    }
    return cachedHabitatTypesCriticalLevels;
  }

  private Map<Integer, CriticalLevels> getHabitatTypesCriticalLevelsFromDB() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return HabitatRepository.getHabitatTypesCriticalLevels(con);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting habitat type information", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Determine the pollutant sensitiveness for a habitat type based on an ID.
   * Will return an empty optional if not found or if a database error occurs.
   */
  public Optional<Map<EmissionResultKey, Boolean>> determineHabitatTypeSensitiveness(final int habitatTypeId) {
    try {
      return Optional.ofNullable(getHabitatTypesSensitiveness().get(habitatTypeId));
    } catch (final AeriusException e) {
      LOG.error("Error while retrieving habitat type sensitiveness", e);
      return Optional.empty();
    }
  }

  /**
   * @return All known sensitiveness for habitat types.
   */
  public Map<Integer, Map<EmissionResultKey, Boolean>> getHabitatTypesSensitiveness() throws AeriusException {
    if (cachedHabitatTypesSensitiveness == null) {
      cachedHabitatTypesSensitiveness = getHabitatTypesSensitivenessFromDB();
    }
    return cachedHabitatTypesSensitiveness;
  }

  private Map<Integer, Map<EmissionResultKey, Boolean>> getHabitatTypesSensitivenessFromDB() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return HabitatRepository.getHabitatTypesSensitiveness(con);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting habitat type information", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

}
