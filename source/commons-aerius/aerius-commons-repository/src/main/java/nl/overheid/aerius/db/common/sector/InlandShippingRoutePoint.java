/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.emissions.shipping.InlandShippingRouteEmissionPoint;

/**
 *
 */
public class InlandShippingRoutePoint extends SridPoint implements InlandShippingRouteEmissionPoint {

  private static final long serialVersionUID = 3937897241630505665L;

  private final double lockFactor;
  private final double measure;
  private int waterwayCategoryId;
  private String waterwayCategoryCode;
  private WaterwayDirection direction;

  /**
   * @param x The x-coordinate representing this point.
   * @param y The y-coordinate representing this point.
   * @param measure the length of the line related to this point
   * @param lockFactor The lockfactor for this point (1 if no lock or 'sluis').
   */
  public InlandShippingRoutePoint(final int x, final int y, final double measure, final double lockFactor) {
    this(x, y, measure, lockFactor, 0, null, null);
  }

  /**
   * @param x The x-coordinate representing this point.
   * @param y The y-coordinate representing this point.
   * @param measure the length of the line related to this point
   * @param lockFactor The lockfactor for this point (1 if no lock or 'sluis').
   * @param waterwayCategoryId The ID of the category for this waterway.
   * @param waterwayCategoryCode The code of the category for this waterway (used for error messages).
   * @param direction The direction of the route at this point.
   */
  public InlandShippingRoutePoint(final int x, final int y, final double measure, final double lockFactor, final int waterwayCategoryId,
      final String waterwayCategoryCode, final WaterwayDirection direction) {
    super(x, y);
    this.measure = measure;
    this.lockFactor = lockFactor;
    this.waterwayCategoryId = waterwayCategoryId;
    this.waterwayCategoryCode = waterwayCategoryCode;
    this.direction = direction;
  }

  public double getMeasure() {
    return measure;
  }

  @Override
  public double getLockFactor() {
    return lockFactor;
  }

  public int getWaterwayCategoryId() {
    return waterwayCategoryId;
  }

  public void setWaterwayCategoryId(final int waterwayCategoryId) {
    this.waterwayCategoryId = waterwayCategoryId;
  }

  public String getWaterwayCategoryCode() {
    return waterwayCategoryCode;
  }

  public void setWaterwayCategoryCode(final String waterwayCategoryCode) {
    this.waterwayCategoryCode = waterwayCategoryCode;
  }

  public WaterwayDirection getDirection() {
    return direction;
  }

  public void setDirection(final WaterwayDirection direction) {
    this.direction = direction;
  }

  @Override
  public String toString() {
    return "InlandShippingRoutePoint [" + super.toString() + ", lockFactor=" + lockFactor + ", waterwayCategoryId="
        + waterwayCategoryId + ", direction=" + direction + ", measure=" + measure + "]";
  }

  @Override
  public double getSegmentLength() {
    return measure;
  }

  @Override
  public InlandWaterway getWaterway() {
    final InlandWaterway waterway = new InlandWaterway();
    waterway.setDirection(direction);
    waterway.setWaterwayCode(waterwayCategoryCode);
    return waterway;
  }
}
