/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory.RoadEmissionCategoryKey;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;

/**
 * Service class for Road Emission factors table.
 */
public final class RoadEmissionCategoryRepository {

  private enum RepositoryAttribute implements Attribute {

    ROAD_AREA_CATEGORY_ID,

    ROAD_TYPE_CATEGORY_ID,

    ROAD_VEHICLE_CATEGORY_ID,

    ROAD_AREA_CODE,
    ROAD_TYPE_CODE,
    ROAD_VEHICLE_CODE,
    MAXIMUM_SPEED,
    SPEED_LIMIT_ENFORCEMENT,
    GRADIENT,
    STAGNATED_EMISSION_FACTOR;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final String STRICT_SPEED_LIMIT_ENFORCEMENT = "strict";
  private static final String IRRELEVANT_SPEED_LIMIT_ENFORCEMENT = "irrelevant";
  private static final String SUBSTANCE_WHERE = "substance_id IN ("
      + Stream.of(Substance.EMISSION_SUBSTANCES).map(Substance::getId).map(String::valueOf).collect(Collectors.joining(","))
      + ")";

  /**
   * This view works on road_category_emission_factors and interpolates all missing years.
   * Only query for PM10, NO2, NOx, and NH3 because those are only supported.
   */
  private static final Query INTERPOLATED_ROAD_EMISSION_FACTORS_QUERY = QueryBuilder.from("road_categories_view")
      .select(QueryAttribute.YEAR, QueryAttribute.SUBSTANCE_ID, RepositoryAttribute.ROAD_AREA_CODE, RepositoryAttribute.ROAD_TYPE_CODE,
          RepositoryAttribute.ROAD_VEHICLE_CODE, RepositoryAttribute.MAXIMUM_SPEED, RepositoryAttribute.SPEED_LIMIT_ENFORCEMENT,
          RepositoryAttribute.GRADIENT, QueryAttribute.EMISSION_FACTOR, RepositoryAttribute.STAGNATED_EMISSION_FACTOR)
      .where(new StaticWhereClause(SUBSTANCE_WHERE)).getQuery();

  private static final Query ROAD_AREA_CATEGORIES_QUERY = QueryBuilder
      .from("road_area_categories")
      .select(RepositoryAttribute.ROAD_AREA_CATEGORY_ID, QueryAttribute.CODE, QueryAttribute.NAME)
      .getQuery();

  private static final Query ROAD_TYPE_CATEGORIES_QUERY = QueryBuilder
      .from("road_type_categories")
      .select(RepositoryAttribute.ROAD_TYPE_CATEGORY_ID, QueryAttribute.CODE, QueryAttribute.NAME)
      .getQuery();

  private static final Query ROAD_VEHICLE_CATEGORIES_QUERY = QueryBuilder
      .from("road_vehicle_categories")
      .select(RepositoryAttribute.ROAD_VEHICLE_CATEGORY_ID, QueryAttribute.CODE, QueryAttribute.NAME)
      .getQuery();

  private static final Query HIDE_ROAD_TYPE_CATEGORIES_QUERY = QueryBuilder
      .from("system.hide_road_type_categories")
      .join(new JoinClause("road_type_categories", RepositoryAttribute.ROAD_TYPE_CATEGORY_ID))
      .select(QueryAttribute.CODE)
      .getQuery();

  private RoadEmissionCategoryRepository() {
  }

  /**
   * Returns all Road Emission Categories and interpolates the years in between.
   *
   * @param con Database connection
   * @return A new RoadEmissionCategories object filled with all categories.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static RoadEmissionCategories findAllRoadEmissionCategories(
      final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    final RoadEmissionCategories categories = new RoadEmissionCategories();

    addAreaCategories(con, messagesKey, categories);
    addRoadTypeCategories(con, messagesKey, categories);
    addVehicleTypeCategories(con, messagesKey, categories);
    addRoadCategories(con, categories);
    addHideRoadTypeCategories(con, categories);

    return categories;
  }

  private static void addRoadCategories(final Connection con, final RoadEmissionCategories categories) throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(INTERPOLATED_ROAD_EMISSION_FACTORS_QUERY.get())) {
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final EmissionValueKey evk = new EmissionValueKey(QueryAttribute.YEAR.getInt(rs), substance);
        final String roadAreaCode = QueryUtil.getString(rs, RepositoryAttribute.ROAD_AREA_CODE);
        final String roadTypeCode = QueryUtil.getString(rs, RepositoryAttribute.ROAD_TYPE_CODE);
        final String vehicleTypeCode = QueryUtil.getString(rs, RepositoryAttribute.ROAD_VEHICLE_CODE);
        final String speedLimitEnforcement = QueryUtil.getString(rs, RepositoryAttribute.SPEED_LIMIT_ENFORCEMENT);
        final boolean strictSLE = STRICT_SPEED_LIMIT_ENFORCEMENT.equals(speedLimitEnforcement);
        final boolean irrelevantSLE = IRRELEVANT_SPEED_LIMIT_ENFORCEMENT.equals(speedLimitEnforcement);
        // if speed is irrelevant store it as 0.
        final int maximumSpeed = irrelevantSLE ? 0 : QueryUtil.getInt(rs, RepositoryAttribute.MAXIMUM_SPEED);
        final int gradient = QueryUtil.getInt(rs, RepositoryAttribute.GRADIENT);

        final RoadEmissionCategoryKey key = new RoadEmissionCategoryKey(roadAreaCode, roadTypeCode, vehicleTypeCode);
        final RoadEmissionCategory newCategory = new RoadEmissionCategory(key, strictSLE, maximumSpeed, gradient);
        RoadEmissionCategory category = categories.getRoadEmissionCategory(newCategory);

        if (category == null) {
          category = newCategory;
          categories.add(category);
        }

        category.setEmissionFactor(evk, QueryAttribute.EMISSION_FACTOR.getDouble(rs));
        category.setStagnatedEmissionFactor(evk, QueryUtil.getDouble(rs, RepositoryAttribute.STAGNATED_EMISSION_FACTOR));
      }
    }
  }

  private static void addAreaCategories(final Connection con, final DBMessagesKey messagesKey,
      final RoadEmissionCategories categories) throws SQLException {

    final SimpleCategoryCollector collector =
        new SimpleCategoryCollector(RepositoryAttribute.ROAD_AREA_CATEGORY_ID, messagesKey,
            (cat, key) -> DBMessages.setRoadAreaMessages(cat, messagesKey));
    collector.collect(con, ROAD_AREA_CATEGORIES_QUERY);
    categories.setAreas(collector.getEntities());
  }

  private static void addRoadTypeCategories(final Connection con, final DBMessagesKey messagesKey,
      final RoadEmissionCategories categories) throws SQLException {

    final SimpleCategoryCollector collector =
        new SimpleCategoryCollector(RepositoryAttribute.ROAD_TYPE_CATEGORY_ID, messagesKey,
            (cat, key) -> DBMessages.setRoadTypeMessages(cat, messagesKey));

    collector.collect(con, ROAD_TYPE_CATEGORIES_QUERY);
    categories.setRoadTypes(collector.getEntities());
  }

  private static void addVehicleTypeCategories(final Connection con, final DBMessagesKey messagesKey,
      final RoadEmissionCategories categories) throws SQLException {

    final SimpleCategoryCollector collector =
        new SimpleCategoryCollector(RepositoryAttribute.ROAD_VEHICLE_CATEGORY_ID, messagesKey,
            (cat, key) -> DBMessages.setRoadVehicleTypeMessages(cat, messagesKey));

    collector.collect(con, ROAD_VEHICLE_CATEGORIES_QUERY);
    categories.setVehicleTypes(collector.getEntities());
  }

  private static void addHideRoadTypeCategories(final Connection con, final RoadEmissionCategories categories) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(HIDE_ROAD_TYPE_CATEGORIES_QUERY.get())) {
      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        categories.getHideRoadTypeCodes().add(QueryAttribute.CODE.getString(rs));
      }
    }
  }

  private static class SimpleCategoryCollector extends AbstractCategoryCollector<SimpleCategory> {

    private final BiConsumer<SimpleCategory, DBMessagesKey> messagesConsumer;

    protected SimpleCategoryCollector(final Attribute categoryId, final DBMessagesKey messagesKey,
        final BiConsumer<SimpleCategory, DBMessagesKey> messagesConsumer) {
      super(categoryId, messagesKey);
      this.messagesConsumer = messagesConsumer;
    }

    @Override
    SimpleCategory getNewCategory() throws SQLException {
      return new SimpleCategory();
    }

    @Override
    void setDescription(final SimpleCategory category, final ResultSet rs) throws SQLException {
      messagesConsumer.accept(category, messagesKey);
    }

  }
}
