/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.configuration;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.aerius.search.domain.SearchCapability;
import nl.aerius.search.domain.SearchRegion;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.sector.SectorIconUtil;
import nl.overheid.aerius.shared.domain.v2.building.BuildingLimits;

public class RblApplicationConfigurationRepository extends AbstractApplicationConfigurationRepository {
  private static final ArrayList<Substance> EMISSION_SUBSTANCES_ROAD = new ArrayList<>(
      EnumSet.of(Substance.NOX, Substance.NO2, Substance.PM10, Substance.PM25, Substance.EC));
  private static final List<EmissionResultKey> EMISSION_RESULT_KEYS = Arrays.asList(
      EmissionResultKey.NOX_CONCENTRATION,
      EmissionResultKey.NO2_CONCENTRATION, EmissionResultKey.NO2_EXCEEDANCE_HOURS,
      EmissionResultKey.PM10_CONCENTRATION, EmissionResultKey.PM10_EXCEEDANCE_DAYS,
      EmissionResultKey.PM25_CONCENTRATION,
      EmissionResultKey.EC_CONCENTRATION);
  private static final @JsProperty List<SectorGroup> ALLOWED_SECTORGROUPS = new ArrayList<>(EnumSet.of(SectorGroup.ROAD_TRANSPORTATION));

  @Override
  protected Theme getTheme() {
    return Theme.RBL;
  }

  @Override
  protected SearchRegion getSearchRegion() {
    return SearchRegion.NL;
  }

  @Override
  protected List<SearchCapability> getSearchCapabilities() {
    return Arrays.asList(SearchCapability.BASIC_INFO, SearchCapability.COORDINATE);
  }

  @Override
  protected List<Integer> getCalculationYears(final Connection con) throws SQLException {
    return Arrays.asList(
        2019,
        2020,
        2021,
        2022,
        2023,
        2024,
        2025,
        2026,
        2027,
        2028,
        2029,
        2030);
  }

  protected List<Integer> getSrm2Options() {
    return Arrays.asList(
        2018,
        2020,
        2030);
  }

  @Override
  protected List<Substance> getSubstances() {
    return EMISSION_SUBSTANCES_ROAD;
  }

  @Override
  protected List<Substance> getEmissionSubstancesRoad() {
    return EMISSION_SUBSTANCES_ROAD;
  }

  @Override
  protected List<EmissionResultKey> getEmissionResultKeys() {
    return EMISSION_RESULT_KEYS;
  }

  @Override
  protected List<CalculationJobType> getCalculationJobTypes() {
    return Arrays.asList();
  }

  @Override
  protected List<CalculationJobType> getAvailableExportReportOptions() {
    return Arrays.asList();
  }

  @Override
  protected List<CalculationMethod> getCalculationMethods() {
    return Arrays.asList(CalculationMethod.CUSTOM_POINTS);
  }

  @Override
  protected int getCalculationYearDefault() {
    return 2019;
  }

  @Override
  protected CalculationMethod getDefaultCalculationMethod() {
    return CalculationMethod.CUSTOM_POINTS;
  }

  @Override
  protected List<SectorGroup> getSectorGroups() {
    return ALLOWED_SECTORGROUPS;
  }

  @Override
  protected void addProjections(final ApplicationConfiguration appConfig) {
    addSingleProjection(appConfig, Proj4BoilerPlate.EPSG_28992, Proj4BoilerPlate.RDNEW_PROJ4_DEF);
  }

  @Override
  protected BuildingLimits getBuildingLimits() {
    // RBL doesn't support buildings.
    return null;
  }

  @Override
  protected Map<Integer, SectorIcon> getSectorIcons() {
    return SectorIconUtil.rbl();
  }

  @Override
  Map<String, ColorRangeType> getRoadTrafficVolumeColors() {
    // RBL doesn't support legenda.
    return Map.of();
  }
}
