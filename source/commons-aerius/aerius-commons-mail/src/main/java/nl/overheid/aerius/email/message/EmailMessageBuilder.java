/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.message;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.shared.domain.email.MailTo;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Builder class for creating the message object to send to the email worker.
 */
class EmailMessageBuilder {
  private static final Logger LOGGER = LoggerFactory.getLogger(EmailMessageBuilder.class);

  private static final String TOKEN_CREATION_DATE_FORMAT = "EEEE dd MMMM yyyy";
  private static final String TOKEN_CREATION_TIME_FORMAT = "HH:mm";

  private final AeriusCustomer customer;

  public EmailMessageBuilder(final AeriusCustomer customer) {
    this.customer = customer;
  }

  /**
   * Constructs a {@link MailMessageData} object from the {@link EmailMessage}.
   *
   * @param emailMessage data to construct the email message from
   * @param downloadUrl download url for the file to be download by the user
   * @return object to send to the email worker
   */
  public <T extends ExportData> MailMessageData build(final EmailMessage<T> emailMessage, final String downloadUrl) {
    final ExportData data = emailMessage.getData();

    final Locale locale = LocaleUtils.getLocale(data.getLocale());
    final MailMessageData mailMessageData = new MailMessageData(emailMessage.getEmailType(), locale, new MailTo(data.getEmailAddress()));
    setTemplates(mailMessageData, emailMessage);
    final Map<ReplacementToken, String> map = setReplacementTokens(emailMessage, data.getCreationDate(), locale, downloadUrl);
    map.forEach(mailMessageData::setReplacement);
    setTheme(mailMessageData, data);
    return mailMessageData;
  }

  private void setTheme(final MailMessageData mailMessageData, final ExportData data) {
    final Theme theme;

    if (customer == AeriusCustomer.JNCC) {
      theme = Theme.NCA;
    } else {
      theme = data instanceof final CalculationInputData cid && cid.getExportType() == ExportType.CIMLK_CSV ? Theme.RBL : Theme.OWN2000;
    }
    mailMessageData.setTheme(theme);
  }

  private static void setTemplates(final MailMessageData mailMessageData, final EmailMessage<?> emailMessage) {
    emailMessage.populateEnumReplacementTokens(mailMessageData.getEnumReplacementTokens());
    mailMessageData.setEnumReplacementToken(ReplacementToken.MAIL_SUBJECT, emailMessage.getMailSubject());
    mailMessageData.setEnumReplacementToken(ReplacementToken.MAIL_CONTENT, emailMessage.getMailContent());
  }

  private static Map<ReplacementToken, String> setReplacementTokens(final EmailMessage<?> emailMessage, final Date creationDate, final Locale locale,
      final String downloadURL) {
    final Map<ReplacementToken, String> map = new EnumMap<>(ReplacementToken.class);

    setDateTimeReplacementTokens(map, creationDate, locale);
    emailMessage.populateStringReplacementTokens(map);
    setDownloadUrl(map, downloadURL);
    return map;
  }

  private static void setDateTimeReplacementTokens(final Map<ReplacementToken, String> map, final Date creationDate, final Locale locale) {
    map.put(ReplacementToken.CREATION_DATE, getDefaultDateFormatted(creationDate, locale));
    map.put(ReplacementToken.CREATION_TIME, getDefaultTimeFormatted(creationDate, locale));
  }

  private static void setDownloadUrl(final Map<ReplacementToken, String> map, final String url) {
    if (url == null) {
      return; // No download, just return here.
    }
    LOGGER.trace("Adding download link: {}", url);
    map.put(ReplacementToken.DOWNLOAD_LINK, url);
  }

  /**
   * @param date The date to format according to default format for mail text.
   * @param locale The locale to use to format it.
   * @return The formatted date. (only date information, no time).
   */
  public static String getDefaultDateFormatted(final Date date, final Locale locale) {
    return new SimpleDateFormat(TOKEN_CREATION_DATE_FORMAT, locale).format(date);
  }

  /**
   * @param date The time to format according to default format for mail text.
   * @param locale The locale to use to format it.
   * @return The formatted time. (only time information, no date).
   */
  public static String getDefaultTimeFormatted(final Date date, final Locale locale) {
    return new SimpleDateFormat(TOKEN_CREATION_TIME_FORMAT, locale).format(date);
  }
}
