/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.worker;

import java.util.Properties;

import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.DatabaseConfigurationBuilder;

/**
 * Builder class for constructing the configuration for the Message worker.
 */
class EmailConfigurationBuilder extends ConfigurationBuilder<EmailConfiguration> {

  private static final String MAIL_PREFIX = "email";
  private static final String HOST = "host";
  private static final String DEFAULT_MAIL_HOST = "localhost";

  private static final String USERNAME = "username";
  private static final String PASSWORD = "password";

  private static final String SMTP_PORT = "smtp.port";
  private static final String SMTP_SSL_PORT = "smtp.ssl.port";

  private static final String SSL_ENABLE = "ssl.enable";
  private static final String STARTTLS_ENABLE = "starttls.enable";

  public EmailConfigurationBuilder(final Properties properties) {
    super(properties, MAIL_PREFIX);
    addBuilder(new DatabaseConfigurationBuilder<EmailConfiguration>(properties));
  }

  @Override
  protected boolean isActive() {
    return workerProperties.isActive();
  }

  @Override
  protected EmailConfiguration create() {
    return new EmailConfiguration(workerProperties.getProcesses());
  }

  @Override
  protected void build(final EmailConfiguration configuration) {
    configuration.setHostname(workerProperties.getPropertyOrDefault(HOST, DEFAULT_MAIL_HOST));
    configuration.setUsername(workerProperties.getProperty(USERNAME));
    configuration.setPassword(workerProperties.getProperty(PASSWORD));
    configuration.setPort(workerProperties.getPropertyIntSafe(SMTP_PORT));
    configuration.setSslPort(workerProperties.getProperty(SMTP_SSL_PORT));
    configuration.setSslEnable(workerProperties.getPropertyBooleanSafe(SSL_ENABLE));
    configuration.setStartTlsEnable(workerProperties.getPropertyBooleanSafe(STARTTLS_ENABLE));
  }
}
