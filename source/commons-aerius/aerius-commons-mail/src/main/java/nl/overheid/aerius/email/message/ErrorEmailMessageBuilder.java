/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.message;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.MailAttachment;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Builds the Error Email message content to be sent via RabbitMQ to email worker.
 */
class ErrorEmailMessageBuilder {

  private static final String DEFAULT_ERROR_CODE = "NoErrorCode";

  private static final String ERROR_CODE_SEPARATOR = ", ";

  private final EmailMessageBuilder genericBuilder;

  public ErrorEmailMessageBuilder(final EmailMessageBuilder genericBuilder) {
    this.genericBuilder = genericBuilder;
  }

  /**
   * Builds the data object with all relevant information to email.
   *
   * @param data data to construct the email message from
   * @param exception exception that was thrown
   * @param jobKey Jobkey that corresponds to the error.
   * @param backupAttachments optional backup attachment to include in the email
   * @return object to send to the email worker
   */
  public <T extends ExportData> MailMessageData build(final T data, final Exception exception,
      final String jobKey, final Collection<StringDataSource> backupAttachments) {
    final MailMessageData messageData = genericBuilder.build(new ErrorEmailMessage(data), null);

    setErrorReplacementTokens(messageData, data, exception, jobKey);
    setBackupAttachments(messageData, backupAttachments);
    return messageData;
  }

  private static <T extends ExportData> void setErrorReplacementTokens(final MailMessageData errorMailMessageData, final T data, final Exception e,
      final String jobKey) {
    final Locale locale = LocaleUtils.getLocale(data.getLocale());
    final AeriusExceptionMessages aem = new AeriusExceptionMessages(locale);

    if (e instanceof final AeriusException ae && ae.isInternalError()) {
      errorMailMessageData.setEnumReplacementToken(ReplacementToken.ERROR_MESSAGE, EmailMessagesEnum.INTERNAL_ERROR_MESSAGE);
      errorMailMessageData.setEnumReplacementToken(ReplacementToken.ERROR_MESSAGE_HEADER, EmailMessagesEnum.INTERNAL_ERROR_MESSAGE_HEADER);
    } else {
      errorMailMessageData.setReplacement(ReplacementToken.ERROR_MESSAGE, aem.getString(e));
      errorMailMessageData.setEnumReplacementToken(ReplacementToken.ERROR_MESSAGE_HEADER, EmailMessagesEnum.EXTERNAL_ERROR_MESSAGE_HEADER);
    }

    errorMailMessageData.setReplacement(ReplacementToken.ERROR_CODE, getErrorCodes(e, data, jobKey));
  }

  /**
   * Get the error codes that can help with identifying the problem.
   * First the normal error code, the one used in the AeriusException's Reason (if it was an AeriusException with a reason).
   * Then the reference for the AeriusException, which usually is the number of milliseconds since... (if it was an AeriusException).
   * And if the jobkey was supplied, that will be added as well.
   *
   * If all those are not available, fallback to something in the project (if available)
   * and if that doesn't end up with something good use the default error code.
   */
  private static <T extends ExportData> String getErrorCodes(final Exception e, final T data, final String jobKey) {
    final List<String> errorCodes = new ArrayList<>();
    if (e instanceof final AeriusException ae && ae.getReason() != null) {
      if (ae.getReason() != null) {
        errorCodes.add(Integer.toString(ae.getReason().getErrorCode()));
      }
      errorCodes.add(Long.toString(ae.getReference()));
    }
    if (jobKey != null) {
      errorCodes.add(jobKey);
    }
    if (errorCodes.isEmpty()) {
      final String fallback = getFallbackErrorCode(data);
      errorCodes.add(fallback == null ? DEFAULT_ERROR_CODE : fallback);
    }
    return errorCodes.stream()
        .filter(c -> c != null && !c.isEmpty())
        .collect(Collectors.joining(ERROR_CODE_SEPARATOR));
  }

  /**
   * @param inputData The inputdata to use when constructing a fallback error code.
   * @return A fallback error code to use in error-mails (can be null).
   */
  private static <T extends ExportData> String getFallbackErrorCode(final T inputData) {
    final String errorCode;

    if ((!(inputData instanceof final CalculationInputData cid)) || cid.getScenario() == null) {
      errorCode = "NoData";
    } else {
      final Scenario scenario = ((CalculationInputData) inputData).getScenario();
      if (scenario.getReference() == null) {
        errorCode = scenario.getMetaData().getProjectName();
      } else {
        errorCode = scenario.getReference();
      }
    }
    return errorCode;
  }

  private static void setBackupAttachments(final MailMessageData messageData, final Collection<StringDataSource> backupAttachments) {
    backupAttachments.stream()
        .map(stringDataSource -> new MailAttachment(stringDataSource.data(), stringDataSource.filename(), stringDataSource.mimeType()))
        .forEach(attachment -> messageData.getAttachments().add(attachment));
  }
}
