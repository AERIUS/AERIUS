/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.i18n;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;

/**
 * ResourceBundle wrapper for email texts that supports both a theme specific bundle and the fallback bundle for a specific locale.
 */
public class EmailMessagesBundle {

  private static final String EMAIL_MESSAGES = EmailMessagesBundle.class.getPackageName() + ".EmailMessages";
  private static final String EMAIL_MESSAGES_THEME = EMAIL_MESSAGES + "_%s";

  private final ResourceBundle rootBundle;
  private final ResourceBundle defaultLocaleBundle;
  private final ResourceBundle themeBundle;

  public EmailMessagesBundle(final Theme theme, final Locale locale) {
    rootBundle = ResourceBundle.getBundle(EMAIL_MESSAGES, Locale.ROOT);
    defaultLocaleBundle = ResourceBundle.getBundle(EMAIL_MESSAGES, locale);
    themeBundle = ResourceBundle.getBundle(String.format(EMAIL_MESSAGES_THEME, theme.name()), locale);
  }

  Enumeration<String> getDefaultKeys() {
    return defaultLocaleBundle.getKeys();
  }

  Enumeration<String> getThemeKeys() {
    return themeBundle.getKeys();
  }

  public Map<String, String> buildKeyMap() {
    return Stream.of(toStream(rootBundle), toStream(defaultLocaleBundle), toStream(themeBundle))
        .flatMap(Function.identity()).distinct().collect(Collectors.toMap(Function.identity(), this::getString));
  }

  public String getString(final EmailMessagesEnum enumKey) {
    return getString(enumKey.name());
  }

  public String getString(final String key) {
    if (themeBundle.containsKey(key)) {
      return themeBundle.getString(key);
    } else if (defaultLocaleBundle.containsKey(key)) {
      return defaultLocaleBundle.getString(key);
    } else {
      return rootBundle.getString(key);
    }
  }

  private static Stream<String> toStream(final ResourceBundle resourceBundle) {
    final Set<String> set = new HashSet<>();
    final Enumeration<String> keys = resourceBundle.getKeys();

    while (keys.hasMoreElements()) {
      set.add(keys.nextElement());
    }
    return set.stream();
  }

}
