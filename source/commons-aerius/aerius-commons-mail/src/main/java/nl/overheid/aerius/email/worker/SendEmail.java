/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.worker;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.email.MailAttachment;
import nl.overheid.aerius.shared.domain.email.MailTo;

/**
 * Send HTML content based e-mails with attachments.
 */
public final class SendEmail {

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(SendEmail.class);

  private static final Pattern REGEXP_BASE64_IMAGES = Pattern.compile(" src=\"data:(image/[\\w]+);base64,([^\"]+)\"");

  private final HtmlEmailBuilder builder;

  public SendEmail(final HtmlEmailBuilder builder) {
    this.builder = builder;
  }

  /**
   * Construct the email message and sends the email.
   *
   * @param from Email address from.
   * @param mailToAddresses Email addresses to send to
   * @param subject Subject of the email.
   * @param bodyHtml Body of the email in HTML.
   * @param bodyText Body of the email in plain text.
   * @param attachments Any datasources that need to be attached to the email. The string value is used for the filename header field.
   * @return True if email was send.
   * @throws EmailException
   * @throws IOException
   */
  public void send(final String from, final MailTo mailToAddresses, final String subject, final String bodyHtml, final String bodyText,
      final List<MailAttachment> attachments) throws EmailException {
    final HtmlEmail email = builder.constructConfiguredEmail();

    email.setFrom(from);
    setToAddresses(email, mailToAddresses);
    email.setSubject(subject);

    setBody(email, bodyHtml, bodyText);
    attach(email, attachments);

    email.send();
  }

  private static void setToAddresses(final HtmlEmail email, final MailTo mailToAddresses) throws EmailException {
    if (mailToAddresses.getToAddresses() != null) {
      for (final String to : mailToAddresses.getToAddresses()) {
        email.addTo(to);
      }
    }
    if (mailToAddresses.getCcAddresses() != null) {
      for (final String cc : mailToAddresses.getCcAddresses()) {
        email.addCc(cc);
      }
    }
    if (mailToAddresses.getBccAddresses() != null) {
      for (final String bcc : mailToAddresses.getBccAddresses()) {
        email.addBcc(bcc);
      }
    }
  }

  private static void setBody(final HtmlEmail email, final String bodyHtml, final String bodyText) throws EmailException {
    final String actualBody = embedBase64Images(email, bodyHtml);

    email.setHtmlMsg(actualBody);
    email.setTextMsg(bodyText);
  }

  private static String embedBase64Images(final HtmlEmail email, final String body) throws EmailException {
    // Check if there are base64 encoded images in the body, if so, add them as attachments and replace them with the cid's.
    final Matcher matcher = REGEXP_BASE64_IMAGES.matcher(body);
    final StringBuilder parsedBody = new StringBuilder();
    int imageCounter = 0;

    while (matcher.find()) {
      imageCounter++;
      final String mimetype = matcher.group(1);
      final String base64EncodedImage = matcher.group(2);

      String contentId;
      try {
        contentId = email.embed(createDataSource(Base64.getDecoder().decode(base64EncodedImage), mimetype), "image_" + imageCounter);
      } catch (final IllegalArgumentException e) {
        matcher.appendReplacement(parsedBody, Matcher.quoteReplacement(matcher.group()));

        // Skip image.
        continue;
      }

      matcher.appendReplacement(parsedBody, Matcher.quoteReplacement(" src=\"cid:" + contentId + "\""));
    }
    matcher.appendTail(parsedBody);

    return parsedBody.toString();
  }

  private static void attach(final HtmlEmail email, final List<MailAttachment> attachments) throws EmailException {
    if (attachments != null) {
      for (final MailAttachment source : attachments) {
        email.attach(createDataSource(source.getBytes(), source.getMimeType()), source.getFileName(), source.getFileName());
      }
    }
  }

  /**
   * Helper method to create a datasource to use as attachment.
   * No reason you can't create your own datasource, this one is just there to help.
   * (Hides apache commons ByteArrayDataSource.)
   *
   * @param file The byte[] of the file to use as datasource.
   * @param fileMIMEType The type of the file.
   * @return The datasource to be used as attachment.
   */
  public static DataSource createDataSource(final byte[] file, final String fileMIMEType) {
    return new ByteArrayDataSource(file, fileMIMEType);
  }
}
