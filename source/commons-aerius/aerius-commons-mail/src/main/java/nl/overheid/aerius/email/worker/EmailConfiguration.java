/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.worker;

import nl.overheid.aerius.worker.DatabaseConfiguration;

/**
 * Configuration for Email message worker.
 */
class EmailConfiguration extends DatabaseConfiguration {

  private String hostname;
  private String username;
  private String password;
  private int port;
  private String sslPort;
  private boolean sslEnable;
  private boolean startTlsEnable;

  public EmailConfiguration(final int processes) {
    super(processes);
  }

  public String getHostname() {
    return hostname;
  }

  void setHostname(final String hostname) {
    this.hostname = hostname;
  }

  public String getUsername() {
    return username;
  }

  void setUsername(final String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  void setPassword(final String password) {
    this.password = password;
  }

  public int getPort() {
    return port;
  }

  void setPort(final int port) {
    this.port = port;
  }

  public String getSslPort() {
    return sslPort;
  }

  void setSslPort(final String sslPort) {
    this.sslPort = sslPort;
  }

  public boolean isSslEnable() {
    return sslEnable;
  }

  void setSslEnable(final boolean sslEnable) {
    this.sslEnable = sslEnable;
  }

  public boolean isStartTlsEnable() {
    return startTlsEnable;
  }

  void setStartTlsEnable(final boolean startTlsEnable) {
    this.startTlsEnable = startTlsEnable;
  }
}
