/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.worker;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.DatabaseWorkerFactory;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * Factory to create EmailMessage worker.
 */
public class EmailMessageWorkerFactory extends DatabaseWorkerFactory<EmailConfiguration> {

  private static final Logger LOG = LoggerFactory.getLogger(EmailMessageWorkerFactory.class);

  public EmailMessageWorkerFactory() {
    super(WorkerType.MESSAGE);
  }

  @Override
  public ConfigurationBuilder<EmailConfiguration> configurationBuilder(final Properties properties) {
    return new EmailConfigurationBuilder(properties);
  }

  @Override
  protected Worker<Serializable, Serializable> createWorkerHandlerWithPMF(final EmailConfiguration configuration,
      final WorkerConnectionHelper workerConnectionHelper) throws Exception {
    return new MessageWorkerHandler(workerConnectionHelper.getPMF(), new HtmlEmailBuilder(configuration));
  }

  /**
   * Worker for the Message worker queue. This worker sends the data to the specific
   * database worker.
   */
  private static class MessageWorkerHandler implements Worker<Serializable, Serializable> {

    private final PMF pmf;
    private final SendEmail sendMail;

    public MessageWorkerHandler(final PMF pmf, final HtmlEmailBuilder builder) {
      this.pmf = pmf;
      sendMail = new SendEmail(builder);
    }

    @Override
    public Serializable run(final Serializable input, final JobIdentifier jobIdentifier,
        final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
      final Serializable output;
      try {
        if (input instanceof final MailMessageData mmd) {
          output = new EmailWorkerHandler(pmf, sendMail).run(mmd, jobIdentifier, workerIntermediateResultSender);
        } else {
          throw new UnsupportedOperationException("Worker does not know how to handle the input:"
              + input.getClass().getName());
        }
      } catch (final SQLException e) {
        LOG.error("SQLException for " + input.getClass(), e);
        return new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
      }
      return output;
    }
  }
}
