/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.message;

import java.io.IOException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.aerius.taskmanager.client.WorkerQueueType;
import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Interface defining methods to set mail messages on the event bus for the MessageWorker to pick up and send.
 * @param <T> the type of input data that the message contents depend on.
 */
public class EmailMessageTaskSender {

  private static final Logger LOGGER = LoggerFactory.getLogger(EmailMessageTaskSender.class);

  private final TaskManagerClientSender taskManagerClient;
  private final EmailMessageBuilder emailMessageBuilder;
  private final ErrorEmailMessageBuilder errorEmailMessageBuilder;

  public EmailMessageTaskSender(final TaskManagerClientSender taskManagerClient, final PMF pmf) {
    this(taskManagerClient, ConstantRepository.getEnum(pmf, ConstantsEnum.CUSTOMER, AeriusCustomer.class));
  }

  public EmailMessageTaskSender(final TaskManagerClientSender taskManagerClient, final AeriusCustomer customer) {
    this.taskManagerClient = taskManagerClient;
    emailMessageBuilder = new EmailMessageBuilder(customer);
    errorEmailMessageBuilder = new ErrorEmailMessageBuilder(emailMessageBuilder);
  }

  /**
   * Prepares a message to be sent by mail to a user.
   * @param emailMessage the input data given by the user
   * @param downloadURL the download url to be included in the message
   * @param taskId unique id to identify the task sent
   * @return {@code null} if url was null and a mail with backupAttachments is sent, otherwise a status object about the mail with download url.
   * @throws AeriusException
   */
  public <T extends ExportData> void sendMailToUser(final EmailMessage<T> emailMessage, final String downloadURL, final String taskId)
      throws AeriusException {
    send(emailMessageBuilder.build(emailMessage, downloadURL), taskId);
  }

  /**
   * Prepares a message to be sent by mail to a user in case of an error.
   * @param inputData the input data given by the user.
   * @param exception the exception that occurred
   * @param jobKey the job key for which the exception occurred.
   * @param backupAttachments the attachments to the message
   * @throws AeriusException
   */
  public <T extends ExportData> void sendMailToUserOnError(final T inputData, final Exception exception, final String jobKey,
      final Collection<StringDataSource> backupAttachments) throws AeriusException {
    send(errorEmailMessageBuilder.build(inputData, exception, jobKey, backupAttachments), jobKey);
  }

  private void send(final MailMessageData messageData, final String taskId) throws AeriusException {
    try {
      final WorkerQueueType workerQueueType = WorkerType.MESSAGE.type();

      taskManagerClient.sendTask(messageData, taskId, taskId, null, workerQueueType, QueueEnum.MESSAGE.getQueueName());
    } catch (final IOException e) {
      LOGGER.error("IOException when trying to start message task for connect api key e-mail message.", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }
}
