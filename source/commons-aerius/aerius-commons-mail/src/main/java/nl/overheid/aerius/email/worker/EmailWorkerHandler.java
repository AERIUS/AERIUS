/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.worker;

import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.email.EmailContentType;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.MailAttachment;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.worker.AbstractMessageWorker;

/**
 * Worker handler that performs the actual sending of the email messages.
 */
class EmailWorkerHandler extends AbstractMessageWorker {

  private static final Logger LOG = LoggerFactory.getLogger(EmailWorkerHandler.class);

  private final SendEmail sendEmail;
  private final String emailFrom;

  public EmailWorkerHandler(final PMF pmf, final SendEmail sendMail) {
    super(pmf, "Email worker");
    sendEmail = sendMail;
    this.emailFrom = ConstantRepository.getString(pmf, ConstantsEnum.NOREPLY_EMAIL);
  }

  @Override
  protected void sendMessage(final MailMessageData input, final String jobKey) throws Exception {
    final List<MailAttachment> attachments = input.getAttachments();
    // Build HTML content
    final MailReplaceTokenMap htmlMap = constructTokenMap(input, EmailContentType.HTML);
    final String emailSubject = htmlMap.getMailText(ReplacementToken.MAIL_SUBJECT.name());
    final String emailHtmlContent = htmlMap.getMailText(EmailMessagesEnum.TEMPLATE_CONTENT);
    // Build Text content
    final MailReplaceTokenMap textMap = constructTokenMap(input, EmailContentType.TEXT);
    final String emailTextContent = textMap.getMailText(EmailMessagesEnum.TEMPLATE_CONTENT);

    LOG.info("Sending {} email for job: {}", input.getEmailType(), jobKey);
    sendEmail.send(emailFrom, input.getMailTo(), emailSubject, emailHtmlContent, emailTextContent, attachments);
  }


  private MailReplaceTokenMap constructTokenMap(final MailMessageData input, final EmailContentType contentType) {
    final MailReplaceTokenMap mailReplaceTokenMap = new MailReplaceTokenMap(input.getTheme(), input.getLocale(), contentType);

    for (final Entry<ReplacementToken, Enum<?>> entry : input.getEnumReplacementTokens().entrySet()) {
      if (entry.getValue() instanceof final EmailMessagesEnum eme) {
        mailReplaceTokenMap.setReplaceTokenValue(entry.getKey(), eme, contentType);
      } else if (entry.getValue() instanceof final SharedConstantsEnum sce) {
        mailReplaceTokenMap.setReplaceTokenValue(entry.getKey(), ConstantRepository.getString(getPMF(), sce));
      }
    }

    for (final Entry<ReplacementToken, String> entry : input.getReplacements().entrySet()) {
      mailReplaceTokenMap.setReplaceTokenValue(entry.getKey(), entry.getValue());
    }
    return mailReplaceTokenMap;
  }
}
