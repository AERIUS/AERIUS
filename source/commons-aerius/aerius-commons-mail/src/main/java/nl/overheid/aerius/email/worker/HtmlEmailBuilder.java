/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.worker;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

/**
 * HtmlEmail configuration from environment settings.
 */
final class HtmlEmailBuilder {

  private final EmailConfiguration configuration;

  public HtmlEmailBuilder(final EmailConfiguration configuration) {
    this.configuration = configuration;
  }

  /**
   * Constructs a `HtmlEmail` with the settings from the environment.
   *
   * @return Configured email
   * @throws EmailException
   */
  public HtmlEmail constructConfiguredEmail() throws EmailException {
    // Host
    final HtmlEmail email = new HtmlEmail();
    email.setHostName(configuration.getHostname());

    // Authentication
    if (configuration.getUsername() != null && configuration.getPassword() != null) {
      email.setAuthentication(configuration.getUsername(), configuration.getPassword());
    }

    // Ports
    if (configuration.getPort() > 0) {
      email.setSmtpPort(configuration.getPort());
    }
    if (configuration.getSslPort() != null) {
      email.setSslSmtpPort(configuration.getSslPort());
    }

    // SSL
    if (configuration.isSslEnable()) {
      email.setSSLOnConnect(true);
      email.setSSLCheckServerIdentity(true);
    }

    // StartTLS
    if (configuration.isStartTlsEnable()) {
      email.setStartTLSEnabled(true);
      email.setStartTLSRequired(true);
    }
    return email;
  }
}
