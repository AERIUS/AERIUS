/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.message;

import java.util.Map;

import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.EmailType;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.shared.domain.export.ExportData;

/**
 * Interface to implement to build the tokens to construct an e-mail message.
 *
 * @param <T> input data type from which the message needs to be constructed
 */
public abstract class EmailMessage<T extends ExportData> {

  private final EmailType emailType;
  private final T data;

  protected EmailMessage(final EmailType emailType, final T data) {
    this.emailType = emailType;
    this.data = data;
  }

  public EmailType getEmailType() {
    return emailType;
  }

  public T getData() {
    return data;
  }

  /**
   * Populates the given map with replacement tokens used in the email message.
   * The data to fill the tokens with is obtained from the data passed via the constructor of this class.
   *
   * @param map map to populate
   */
  protected void populateStringReplacementTokens(final Map<ReplacementToken, String> map) {}

  /**
   *
   * @param map
   */
  protected void populateEnumReplacementTokens(final Map<ReplacementToken, Enum<?>> map) {}

  /**
   * @return The AeriusTextEnum specifying the content template to use in the mail to be sent to the user.
   */
  public abstract EmailMessagesEnum getMailContent();

  /**
   * @return The AeriusTextEnum specifying the subject template to use in the mail to be sent to the user.
   */
  public abstract EmailMessagesEnum getMailSubject();
}
