/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.worker;

import java.util.Locale;
import java.util.Map;

import org.apache.commons.text.StringSubstitutor;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

import nl.overheid.aerius.email.i18n.EmailMessagesBundle;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.email.EmailContentType;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;

/**
 * Class to construct the actual email text.
 * First add the tokens and values that the token should be replaced with.
 * Second call getMailText to email text where all tokens are substituted by the values.
 */
class MailReplaceTokenMap {

  private static final String TOKEN_PREFIX = "[";
  private static final String TOKEN_SUFFIX = "]";
  private static final PolicyFactory ESCAPE_POLICY_FACTORY = new HtmlPolicyBuilder().toFactory();

  private final Map<String, String> map;
  private final EmailContentType contentType;

  public MailReplaceTokenMap(final Theme theme, final Locale locale, final EmailContentType contentType) {
    this.contentType = contentType;
    map = new EmailMessagesBundle(theme, locale).buildKeyMap();
  }

  /**
   * Returns the email text with the given token as the root token that is used as the template.
   * If a token was in the template, but not in this map, it will remain as it is in the text.
   *
   * @param token root token representing the template
   * @return email text
   */
  public String getMailText(final EmailMessagesEnum token) {
    return getMailText(token.getKey(contentType));
  }

  /**
   * The template where every occurrence of the replacement tokens in this map are replaced.
   *
   * @param token The template to use for getting the mail text.
   * @return The template where every occurrence of the replacement tokens in this map are replaced.
   */
  public String getMailText(final String token) {
    if (token == null) {
      throw new IllegalArgumentException("Template for mail not allowed to be null.");
    }
    //StrSubstitutor works in a recursive way. No need to keep on replacing.
    return new StringSubstitutor(map, TOKEN_PREFIX, TOKEN_SUFFIX).replace(TOKEN_PREFIX + token + TOKEN_SUFFIX);
  }

  /**
   * Set the value to replace a ReplacementToken with when sending an email,
   * overwriting any previously added value for that token.
   * @param token The token to set the value for
   * @param value The value that will replace the token in the template.
   * Can be null, in which case the token will not be replaced at all
   * (leaving a [TOKEN] in the mail if it was present in the template).
   *
   * NOTE: HTML in the value will be escaped
   */
  public void setReplaceTokenValue(final ReplacementToken token, final String value) {
    if (value == null) {
      setReplaceTokenTrustedValue(token, null);
    } else {
      final String sanitizedValue = ESCAPE_POLICY_FACTORY.sanitize(value);
      setReplaceTokenTrustedValue(token, sanitizedValue);
    }
  }

  /**
   * Set the value to replace a ReplacementToken with when sending an email,
   * overwriting any previously added value for that token.
   * The value is obtained from the database using the AeriusTextEnum.
   *
   * NOTE: HTML in the value is not escaped, stored messages in the database are trusted.
   *
   * @param token The token to set the value for
   * @param messageEnum Message enum to retrieve
   */
  public void setReplaceTokenValue(final ReplacementToken token, final EmailMessagesEnum messageEnum, final EmailContentType contentType) {
    setReplaceTokenTrustedValue(token, TOKEN_PREFIX + messageEnum.getKey(contentType) + TOKEN_SUFFIX);
  }

  /**
   * Set the value to replace a ReplacementToken with when sending an email,
   * overwriting any previously added value for that token.
   *
   * @param token The token to set the value for
   * @param value The value that will replace the token in the template.
   * Can be null, in which case the token will not be replaced at all
   * (leaving a [TOKEN] in the mail if it was present in the template).
   *
   * WARNING: HTML in the value is not escaped but trusted
   */
  private void setReplaceTokenTrustedValue(final ReplacementToken token, final String value) {
    final String name = token.name();

    if (value == null) {
      map.remove(name);
    } else {
      map.put(name, value);
    }
  }
}
