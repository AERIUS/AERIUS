/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.i18n;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.email.EmailContentType;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class to test {@link EmailMessagesEnum} values.
 */
class EmailMessagesEnumTest {

  private static List<Arguments> data() throws SQLException, IOException {
    return List.of(
        Arguments.of(Theme.NCA, Locale.ENGLISH),
        Arguments.of(Theme.RBL, LocaleUtils.DUTCH),
        Arguments.of(Theme.RBL, Locale.ENGLISH),
        Arguments.of(Theme.OWN2000, LocaleUtils.DUTCH),
        Arguments.of(Theme.OWN2000, Locale.ENGLISH));
  }

  @ParameterizedTest
  @MethodSource("data")
  void testAeriusTextEnumsHMTL(final Theme theme, final Locale locale) {
    assertAeriusTextEnums(theme, locale, EmailContentType.HTML);
  }

  @ParameterizedTest
  @MethodSource("data")
  void testAeriusTextEnumsText(final Theme theme, final Locale locale) {
    assertAeriusTextEnums(theme, locale, EmailContentType.TEXT);
  }

  /**
   * Tests if all EmailMessagesEnum keys appear either as key or in the value in the properties file.
   *
   * @param theme theme to test
   * @param locale locale to test
   * @param contentType Test for specific content type.
   */
  private static void assertAeriusTextEnums(final Theme theme, final Locale locale, final EmailContentType contentType) {
    final Map<String, String> propertiesKeyMap = new EmailMessagesBundle(theme, locale).buildKeyMap();
    final List<EmailMessagesEnum> missingKeys = new ArrayList<>();

    for (final EmailMessagesEnum key : EmailMessagesEnum.values()) {
      if (!propertiesKeyMap.entrySet().stream().anyMatch(
          e -> e.getKey().equals(key.getKey(contentType)) ||
              e.getValue().contains("[" + key.getKey(contentType) + "]"))) {
        missingKeys.add(key);
      }
    }
    final String title = "Missing " + contentType + " keys for " + theme + ", locale: " + locale;

    print(title, missingKeys);
    assertTrue(missingKeys.isEmpty(), title);
  }

  private static void print(final String title, final List<?> keys) {
    if (keys.isEmpty()) {
      return;
    }
    final String sep = "-".repeat(10);
    System.out.println(sep + title + sep);
    keys.stream().sorted().forEach(System.out::println);
  }
}
