/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataSource;

import org.apache.commons.mail.HtmlEmail;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.EmailType;
import nl.overheid.aerius.shared.domain.email.MailAttachment;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.shared.domain.email.MailTo;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link EmailWorkerHandler}.
 */
@ExtendWith(MockitoExtension.class)
class EmailWorkerHandlerTest {

  private static final String FILENAME = "random_name";
  private static final String DUMMY_EMAIL_ADDRESS = "aerius@example.com";
  private static final Map<ReplacementToken, String> DOWNLOAD_STRING_REPLACEMENTS = Map.of(
      ReplacementToken.CREATION_DATE, "",
      ReplacementToken.CREATION_TIME, "",
      ReplacementToken.DOWNLOAD_LINK, "");
  private static final Map<ReplacementToken, String> CONNECT_APIKEY_STRING_REPLACEMENTS = Map.of(
      ReplacementToken.CREATION_DATE, "",
      ReplacementToken.CREATION_TIME, "",
      ReplacementToken.CONNECT_APIKEY, "");
  private static final Map<ReplacementToken, String> ERROR_STRING_REPLACEMENTS = Map.of(
      ReplacementToken.CREATION_DATE, "",
      ReplacementToken.CREATION_TIME, "",
      ReplacementToken.ERROR_CODE, "",
      ReplacementToken.ERROR_MESSAGE, "",
      ReplacementToken.ERROR_MESSAGE_HEADER, "");
  private static final Pattern TOKEN_MATCHER = Pattern.compile("\\[[^\\]]+\\]");
  private static final List<List<Object>> THEMES = List.of(
      List.of(Theme.NCA, Locale.ENGLISH),
      List.of(Theme.RBL, LocaleUtils.DUTCH),
      List.of(Theme.RBL, Locale.ENGLISH),
      List.of(Theme.OWN2000, LocaleUtils.DUTCH),
      List.of(Theme.OWN2000, Locale.ENGLISH));

  private @Mock HtmlEmailBuilder configuation;
  private @Mock MailMessageData mailMessage;
  private @Mock HtmlEmail htmlEmail;
  private @Captor ArgumentCaptor<String> attachFilenameCaptor;
  private @Captor ArgumentCaptor<String> emailToCaptor;
  private @Captor ArgumentCaptor<String> subjectCaptor;
  private @Captor ArgumentCaptor<String> htmlTextMsgcaptor;
  private @Captor ArgumentCaptor<String> plainTextMsgcaptor;

  private EmailWorkerHandler handler;
  private @Mock PMF pmf;
  private @Mock Connection connection;
  private @Mock PreparedStatement preparedStatement;
  private @Mock ResultSet resultSet;

  @BeforeEach
  public void setUp() throws Exception {
    doReturn(resultSet).when(preparedStatement).executeQuery();
    doReturn(true).when(resultSet).next();
    when(connection.prepareStatement(any())).thenReturn(preparedStatement);
    when(pmf.getConnection()).thenReturn(connection);
    lenient().when(configuation.constructConfiguredEmail()).thenReturn(htmlEmail);
    final SendEmail sendMail = new SendEmail(configuation);
    handler = new EmailWorkerHandler(pmf, sendMail);
    lenient().when(mailMessage.getMailTo()).thenReturn(new MailTo(DUMMY_EMAIL_ADDRESS));
    lenient().when(mailMessage.getAttachments()).thenReturn(List.of(new MailAttachment("test", FILENAME, "same")));
  }

  private static List<Arguments> data() {
    final List<Arguments> list = new ArrayList<>();
    final List<List<Object>> messagesData = messagesData();

    for (final List<Object> themeList : THEMES) {
      for (final List<Object> objects : messagesData) {
        list.add(Arguments.of(themeList.get(0), themeList.get(1), objects.get(0), objects.get(1)));
      }
    }
    return list;
  }

  private static List<List<Object>> messagesData() {
    return List.of(
        List.of(Map.of(
            ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_PAA_SUBJECT,
            ReplacementToken.MAIL_CONTENT, EmailMessagesEnum.DOWNLOAD_CONTENT,
            ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_GML,
            ReplacementToken.ADDITIONAL_DOWNLOAD_INFO_1, EmailMessagesEnum.DOWNLOAD_IMAER_IMPORT), DOWNLOAD_STRING_REPLACEMENTS),
        List.of(Map.of(
            ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_SUBJECT,
            ReplacementToken.MAIL_CONTENT, EmailMessagesEnum.DOWNLOAD_CONTENT,
            ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_ENGINE_INPUT,
            ReplacementToken.ADDITIONAL_DOWNLOAD_INFO_1, EmailMessagesEnum.DOWNLOAD_IMAER_IMPORT,
            ReplacementToken.ADDITIONAL_DOWNLOAD_INFO_2, EmailMessagesEnum.DOWNLOAD_PDF_NO_ENGLISH), DOWNLOAD_STRING_REPLACEMENTS),
        List.of(Map.of(
            ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_SUBJECT_JOB,
            ReplacementToken.MAIL_CONTENT, EmailMessagesEnum.DOWNLOAD_CONTENT,
            ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_ENGINE_INPUT), DOWNLOAD_STRING_REPLACEMENTS),
        List.of(Map.of(
            ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.CONNECT_APIKEY_CONFIRM_SUBJECT,
            ReplacementToken.MAIL_CONTENT, EmailMessagesEnum.CONNECT_APIKEY_CONFIRM_BODY), CONNECT_APIKEY_STRING_REPLACEMENTS),
        List.of(Map.of(
            ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.ERROR_SUBJECT,
            ReplacementToken.MAIL_CONTENT, EmailMessagesEnum.ERROR_CONTENT), ERROR_STRING_REPLACEMENTS));
  }

  @ParameterizedTest
  @MethodSource("data")
  void testSendEmail(final Theme theme, final Locale locale, final Map<ReplacementToken, Enum<?>> enumMap,
      final Map<ReplacementToken, String> stringMap) throws Exception {
    when(mailMessage.getEmailType()).thenReturn(EmailType.GML_DOWNLOAD);
    when(mailMessage.getTheme()).thenReturn(theme);
    when(mailMessage.getLocale()).thenReturn(locale);
    when(mailMessage.getEnumReplacementTokens()).thenReturn(enumMap);
    when(mailMessage.getReplacements()).thenReturn(stringMap);
    final boolean send = handler.run(mailMessage, new JobIdentifier("test"), null);

    assertTrue(send, "Worker result");
    verify(htmlEmail).addTo(emailToCaptor.capture());
    verify(htmlEmail).setSubject(subjectCaptor.capture());
    verify(htmlEmail).setHtmlMsg(htmlTextMsgcaptor.capture());
    verify(htmlEmail).setTextMsg(plainTextMsgcaptor.capture());
    verify(htmlEmail).attach((DataSource) any(), attachFilenameCaptor.capture(), any());
    assertEquals(DUMMY_EMAIL_ADDRESS, emailToCaptor.getValue(), "Should has set email address");
    assertEquals(FILENAME, attachFilenameCaptor.getValue(), "Should have set file attachment");
    assertBody(htmlTextMsgcaptor, "Heeft u nog vragen");
    assertBody(plainTextMsgcaptor, "Heeft u nog vragen");
  }

  private void assertBody(final ArgumentCaptor<String> captor, final String checkString) {
    final String value = captor.getValue();

    assertFalse(value.isBlank(), "Body should not be empty");
    final Matcher matcher = TOKEN_MATCHER.matcher(value);
    final boolean find = matcher.find();
    assertFalse(find, "All replacement tokens should have been replaced:" + (find ? matcher.group() : ""));
  }

  @Test
  void testEscapedReplaceTokenValue() throws Exception {
    when(mailMessage.getEmailType()).thenReturn(EmailType.GML_DOWNLOAD);
    when(mailMessage.getTheme()).thenReturn(Theme.OWN2000);
    when(mailMessage.getLocale()).thenReturn(LocaleUtils.getDefaultLocale());
    when(mailMessage.getEnumReplacementTokens()).thenReturn(Map.of(ReplacementToken.MAIL_CONTENT, EmailMessagesEnum.DOWNLOAD_CONTENT));
    when(mailMessage.getReplacements()).thenReturn(Map.of(ReplacementToken.CREATION_DATE, "<a href='Malicious'>Link</a>"));
    handler.run(mailMessage, new JobIdentifier("test"), null);

    verify(htmlEmail).setHtmlMsg(htmlTextMsgcaptor.capture());
    assertFalse(htmlTextMsgcaptor.getValue().contains(ReplacementToken.CREATION_DATE.name()), "Token should have been replaced.");
    assertFalse(htmlTextMsgcaptor.getValue().contains("<a href='Malicious'>Link</a>"), "Malicious link should have been escaped.");
  }
}
