/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.email.message;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Test class for {@link ErrorEmailMessageBuilder}.
 */
@ExtendWith(MockitoExtension.class)
class ErrorEmailMessageBuilderTest {

  private static final String LOCALE = "en";
  private static final String PROJECT_NAME = "Project Name";
  private static final String REFERENCE = "1234";
  private static final String ERROR_MESSAGE = "An unknown error occurred. If this is a problem please contact the helpdesk.";
  private static final long ERROR_REFERENCE = 999;
  private static final Date CREATION_DATE = new Date(0);
  private static final String CREATION_DATE_STRING = "Thursday 01 January 1970";
  private static final String CREATION_TIME_STRING = "01:00";
  private static final Map<ReplacementToken, Enum<?>> DEFAULT_TOKEN_REPLACEMENTS = Map.of(
      ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.ERROR_SUBJECT,
      ReplacementToken.MAIL_CONTENT, EmailMessagesEnum.ERROR_CONTENT);
  private static final Map<ReplacementToken, String> DEFAULT_STRING_REPLACEMENTS = Map.of(
      ReplacementToken.CREATION_DATE, CREATION_DATE_STRING,
      ReplacementToken.CREATION_TIME, CREATION_TIME_STRING);
  private static final AeriusExceptionReason AERIUS_EXCEPTION_REASON = AeriusExceptionReason.INTERNAL_ERROR;
  private static final String AERIUS_EXCEPTION_CODE = Integer.toString(AERIUS_EXCEPTION_REASON.getErrorCode());
  private static final Exception OTHER_EXCEPTION = new IllegalArgumentException("unknown error");
  private static final String JOB_KEY = "OurJobKey";

  private @Mock TaskManagerClientSender taskManagerClient;
  private @Captor ArgumentCaptor<MailMessageData> captor;

  @ParameterizedTest
  @MethodSource("data")
  void testErrorEmailMessage(final CalculationInputData data, final Exception exception, final String jobKey,
      final Map<ReplacementToken, Enum<?>> expectedEnumReplacementTokens, final Map<ReplacementToken, String> expectedReplacements)
      throws AeriusException, IOException {
    final EmailMessageTaskSender sender = new EmailMessageTaskSender(taskManagerClient, AeriusCustomer.RIVM);

    sender.sendMailToUserOnError(data, exception, jobKey, List.of());

    verify(taskManagerClient).sendTask(captor.capture(), any(), any(), isNull(), eq(WorkerType.MESSAGE.type()), eq(QueueEnum.MESSAGE.getQueueName()));
    final MailMessageData actualMessage = captor.getValue();

    assertEquals(expectedEnumReplacementTokens, actualMessage.getEnumReplacementTokens(), "Expected enum tokens should be the same");
    assertEquals(expectedReplacements, actualMessage.getReplacements(), "Expected replace enum tokens should be the same");
  }

  private static List<Arguments> data() {
    return List.of(
        Arguments.of(createCalculationInputData(null, null), mockAeriusException(), null,
            merge(DEFAULT_TOKEN_REPLACEMENTS, Map.of(
                ReplacementToken.ERROR_MESSAGE, EmailMessagesEnum.INTERNAL_ERROR_MESSAGE,
                ReplacementToken.ERROR_MESSAGE_HEADER, EmailMessagesEnum.INTERNAL_ERROR_MESSAGE_HEADER)),
            merge(DEFAULT_STRING_REPLACEMENTS, Map.of(
                ReplacementToken.ERROR_CODE, AERIUS_EXCEPTION_CODE + ", " + ERROR_REFERENCE,
                ReplacementToken.PROJECT_NAME, "",
                ReplacementToken.REFERENCE, ""))),
        Arguments.of(createCalculationInputData(null, null), mockAeriusException(), JOB_KEY,
            merge(DEFAULT_TOKEN_REPLACEMENTS, Map.of(
                ReplacementToken.ERROR_MESSAGE, EmailMessagesEnum.INTERNAL_ERROR_MESSAGE,
                ReplacementToken.ERROR_MESSAGE_HEADER, EmailMessagesEnum.INTERNAL_ERROR_MESSAGE_HEADER)),
            merge(DEFAULT_STRING_REPLACEMENTS, Map.of(
                ReplacementToken.ERROR_CODE, AERIUS_EXCEPTION_CODE + ", " + ERROR_REFERENCE + ", " + JOB_KEY,
                ReplacementToken.PROJECT_NAME, "",
                ReplacementToken.REFERENCE, ""))),
        Arguments.of(createCalculationInputData(PROJECT_NAME, null), OTHER_EXCEPTION, JOB_KEY,
            merge(DEFAULT_TOKEN_REPLACEMENTS, Map.of(
                ReplacementToken.ERROR_MESSAGE_HEADER, EmailMessagesEnum.EXTERNAL_ERROR_MESSAGE_HEADER)),
            merge(DEFAULT_STRING_REPLACEMENTS, Map.of(
                ReplacementToken.ERROR_CODE, JOB_KEY,
                ReplacementToken.ERROR_MESSAGE, ERROR_MESSAGE,
                ReplacementToken.PROJECT_NAME, PROJECT_NAME,
                ReplacementToken.REFERENCE, ""))),
        Arguments.of(createCalculationInputData(PROJECT_NAME, null), OTHER_EXCEPTION, null,
            merge(DEFAULT_TOKEN_REPLACEMENTS, Map.of(
                ReplacementToken.ERROR_MESSAGE_HEADER, EmailMessagesEnum.EXTERNAL_ERROR_MESSAGE_HEADER)),
            merge(DEFAULT_STRING_REPLACEMENTS, Map.of(
                ReplacementToken.ERROR_CODE, PROJECT_NAME,
                ReplacementToken.ERROR_MESSAGE, ERROR_MESSAGE,
                ReplacementToken.PROJECT_NAME, PROJECT_NAME,
                ReplacementToken.REFERENCE, ""))),
        Arguments.of(createCalculationInputData(null, REFERENCE), OTHER_EXCEPTION, null,
            merge(DEFAULT_TOKEN_REPLACEMENTS, Map.of(
                ReplacementToken.ERROR_MESSAGE_HEADER, EmailMessagesEnum.EXTERNAL_ERROR_MESSAGE_HEADER)),
            merge(DEFAULT_STRING_REPLACEMENTS, Map.of(
                ReplacementToken.ERROR_CODE, REFERENCE,
                ReplacementToken.ERROR_MESSAGE, ERROR_MESSAGE,
                ReplacementToken.PROJECT_NAME, "",
                ReplacementToken.REFERENCE, REFERENCE))));
  }

  private static AeriusException mockAeriusException() {
    // Mock the exception, as the reference would otherwise change for each test (and we're validating on that)
    final AeriusException mockedAeriusException = mock(AeriusException.class);
    when(mockedAeriusException.getMessage()).thenReturn(ERROR_MESSAGE);
    when(mockedAeriusException.getReason()).thenReturn(AERIUS_EXCEPTION_REASON);
    when(mockedAeriusException.getArgs()).thenReturn(new String[0]);
    when(mockedAeriusException.getReference()).thenReturn(ERROR_REFERENCE);
    when(mockedAeriusException.isInternalError()).thenReturn(true);
    return mockedAeriusException;
  }

  private static CalculationInputData createCalculationInputData(final String projectName, final String reference) {
    final CalculationInputData inputData = new CalculationInputData();
    final Scenario scenario = new Scenario();
    final ScenarioSituation situation = new ScenarioSituation();
    final ScenarioMetaData metaData = new ScenarioMetaData();

    metaData.setProjectName(projectName);
    scenario.setMetaData(metaData);
    situation.setReference(reference);
    scenario.getSituations().add(situation);
    inputData.setName(projectName);
    inputData.setCreationDate(CREATION_DATE);
    inputData.setLocale(LOCALE);
    inputData.setScenario(scenario);
    inputData.setExportType(ExportType.PDF_PAA);
    return inputData;
  }

  private static Map<ReplacementToken, ?> merge(final Map<ReplacementToken, ?> map1, final Map<ReplacementToken, ?> map2) {
    return Stream.of(map1, map2).flatMap(m -> m.entrySet().stream()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }
}
