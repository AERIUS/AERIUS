# Email Worker Module

The email module handles sending e-mails to the user.
It consists of 2 different phases.
The first phase is to be used to construct the email content and pass this to the email worker via the RabbitMQ queue.
The second phase is the email worker that takes email content and performs the actual sending of the email.

## Constructing Email Messages

An email to be send to the user.
This module provides the code to messages.
A message consists of a map of ReplacementTokens and either an enum or actual values.
The enum refers to a database constant in either the i18n table or the constants table.
An actual value refers to data obtains from input derived from the user action.
When a message is constructed it is put on the RabbitMQ message queue to be handled by the message worker.

## Email Worker

The message worker is the worker that takes messages from the RabbitMQ queue and performs the actual sending of the email.
The worker takes the message from the queue and replaces all tokens recursively until no replacement tokens are left.
It replaces the tokens either with values from the database or with the actual value provided in the message.
In general there is only 1 instance of the worker needed,
also because this worker needs the privileges to sending email's.


### Email templates

An email is constructed using templating with keys that are replaced by the content the key is mapped to.
The keys/value pairs are stored in properties files and keys can be nested.
There are 3 levels of properties files to look up keys.
First the theme and locale specific, second the locale specific and third the root properties file.
The guideline to where to store a key value is:
1) Is is theme and locale specific it should go in the theme/locale specific properties file.
2) Is is theme independent than it should go in the locale generic properties file.
3) Is it theme and locale independent than it should go in the root properties file.
A key is a string with bracket, like `[ERROR_CODE]`.
In the code there are 2 keys types available.
First the `ReplacementToken`.
The `ReplacementToken` key has no value in a properties file, but should get a value dynamically based on the context.
Secondly the `EmailMessagesEnum`.
The `EmailMessagesEnum` key are keys that have values in the properties files and can be used as values for the `ReplacementToken` key.
The properties file contain more keys than present in the `EmailMessagesEnum`, as some keys are part of values of other keys.
