/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadStandardEmissionFactorsKey;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadStandardsInterpolationValues;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class CategoryRoadEmissionFactorSupplierTest {

  private static final int TEST_YEAR = 2022;
  private static final String TEST_AREA = "NL";
  private static final String TEST_ROAD_TYPE = "offroad";
  private static final String TEST_VEHICLE_TYPE = "unicycle";

  @Mock SectorCategories sectorCategories;
  @Mock RoadEmissionCategories roadCategories;

  CategoryRoadEmissionFactorSupplier supplier;

  @BeforeEach
  void beforeEach() throws AeriusException {
    when(sectorCategories.getRoadEmissionCategories()).thenReturn(roadCategories);
    supplier = new CategoryRoadEmissionFactorSupplier(sectorCategories, TEST_YEAR);
  }

  @ParameterizedTest(name = "{0}")
  @MethodSource("interpolationCases")
  void testGetRoadStandardVehicleInterpolationValues(final String name, final int targetSpeed, final double targetGradient,
      final int expectedSpeedFloor, final int expectedSpeedCeiling, final double expectedGradientFloor, final double expectedGradientCeiling) {
    when(roadCategories.getMaximumSpeedCategories(TEST_AREA, TEST_ROAD_TYPE)).thenReturn(List.of(25, 50, 75, 100));
    when(roadCategories.getGradientCategories(TEST_AREA, TEST_ROAD_TYPE, TEST_VEHICLE_TYPE)).thenReturn(List.of(-4, -2, 0, 2, 4));
    final RoadStandardEmissionFactorsKey emissionFactorsKey = new RoadStandardEmissionFactorsKey(TEST_AREA, TEST_ROAD_TYPE, TEST_VEHICLE_TYPE,
        targetSpeed, false, targetGradient);

    final RoadStandardsInterpolationValues result = supplier.getRoadStandardVehicleInterpolationValues(emissionFactorsKey);

    assertEquals(expectedSpeedFloor, result.getSpeedFloor(), "Speed floor");
    assertEquals(expectedSpeedCeiling, result.getSpeedCeiling(), "Speed ceiling");
    assertEquals(expectedGradientFloor, result.getGradientFloor(), "Gradient floor");
    assertEquals(expectedGradientCeiling, result.getGradientCeiling(), "Gradient ceiling");
  }

  private static Stream<Arguments> interpolationCases() {
    return Stream.of(
        Arguments.of("Exact matches", 50, 2, 50, 50, 2, 2),
        Arguments.of("Interpolate speed", 40, 2, 25, 50, 2, 2),
        Arguments.of("Interpolate gradient", 50, 3.1, 50, 50, 2, 4),
        Arguments.of("Interpolate speed and gradient", 40, 3.1, 25, 50, 2, 4),
        Arguments.of("Interpolate negative gradient", 50, -2.3, 50, 50, -4, -2),
        Arguments.of("Speed above range", 200, 2, 100, 100, 2, 2),
        Arguments.of("Speed under range", 10, 2, 25, 25, 2, 2),
        Arguments.of("Gradient above range", 50, 10, 50, 50, 4, 4),
        Arguments.of("Gradient under range", 50, -10, 50, 50, -4, -4));
  }

  void testGetRoadStandardVehicleInterpolationValuesEmptyLists() {
    when(roadCategories.getMaximumSpeedCategories(TEST_AREA, TEST_ROAD_TYPE)).thenReturn(List.of());
    when(roadCategories.getGradientCategories(TEST_AREA, TEST_ROAD_TYPE, TEST_VEHICLE_TYPE)).thenReturn(List.of());
    final RoadStandardEmissionFactorsKey emissionFactorsKey = new RoadStandardEmissionFactorsKey(TEST_AREA, TEST_ROAD_TYPE, TEST_VEHICLE_TYPE,
        30, false, 2.0);

    final RoadStandardsInterpolationValues result = supplier.getRoadStandardVehicleInterpolationValues(emissionFactorsKey);

    assertEquals(0, result.getSpeedFloor(), "Speed floor");
    assertEquals(0, result.getSpeedCeiling(), "Speed ceiling");
    assertEquals(0, result.getGradientFloor(), "Gradient floor");
    assertEquals(0, result.getGradientCeiling(), "Gradient ceiling");
  }
}
