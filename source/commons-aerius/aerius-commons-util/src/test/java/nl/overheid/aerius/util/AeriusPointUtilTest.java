/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;

/**
 * Test class for {@link AeriusPointUtil}.
 */
class AeriusPointUtilTest {

  @ParameterizedTest
  @MethodSource("data")
  void testDecodeName(final String codedPoint, final AeriusPointType expectedPointType, final int expectedId, final Integer expectedParentId) {
    final AeriusPoint decodedPoint = AeriusPointUtil.decodeName(codedPoint);

    assertDecodedPoint(expectedPointType, expectedId, expectedParentId, decodedPoint);
  }

  @ParameterizedTest
  @MethodSource("data")
  void testEncodeName(final String expectedEncodedCode, final AeriusPointType pointType, final int id, final Integer parentId) {
    AeriusPoint aeriusPoint;
    aeriusPoint = new AeriusPoint(id, pointType);
    aeriusPoint.setParentId(parentId);
    final String encodedPoint = AeriusPointUtil.encodeName(aeriusPoint);

    assertEquals(expectedEncodedCode, encodedPoint, "Not the expected encoded point");
  }

  private static List<Arguments> data() {
    return List.of(
        Arguments.of("R10", AeriusPointType.RECEPTOR, 10, 0),
        Arguments.of("P10", AeriusPointType.POINT, 10, 0),
        Arguments.of("S10-1", AeriusPointType.SUB_RECEPTOR, 1, 10),
        Arguments.of("Z10-1", AeriusPointType.SUB_POINT, 1, 10));
  }

  private static void assertDecodedPoint(final AeriusPointType pointType, final int id, final Integer parentId, final AeriusPoint decodedPoint) {
    assertEquals(pointType, decodedPoint.getPointType(), "Not the expected point type " + decodedPoint);
    assertEquals(id, decodedPoint.getId(), "Not the expected id " + decodedPoint);
    assertEquals(parentId, decodedPoint.getParentId(), "Not the expected parent id " + decodedPoint);
  }
}
