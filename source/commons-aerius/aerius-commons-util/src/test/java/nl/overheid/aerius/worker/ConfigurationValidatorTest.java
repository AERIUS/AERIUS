/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeFalse;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.overheid.aerius.util.OSUtils;

/**
 * Test class for {@link ConfigurationValidator}.
 */
public class ConfigurationValidatorTest {

  @TempDir
  private File tempDir;

  @Test
  void testValidateDirectory() throws IOException {
    assumeFalse(OSUtils.isWindows(), "Ignored on windows, posix permissions issue");
    assertValidDirectory(tempDir, true);

    // test directory not writable, but should not writable either.
    assertValidDirectory(createNonWritableDirectory(), false);
  }

  private void assertValidDirectory(final File directory, final boolean writable) {
    final ConfigurationValidator validator = new ConfigurationValidator();

    validator.validateDirectory("TEST", directory, writable);
    assertEquals(List.of(), validator.getValidationErrors(), "Should have no validation errors");
  }

  @Test
  void testValidateDirectoryInvalid() throws IOException {
    assumeFalse(OSUtils.isWindows(), "Ignored on windows, posix permissions issue");
    assertValidInvalidDirectory(null, true);
    // Passed directory is not a directory
    assertValidInvalidDirectory(Files.createFile(tempDir.toPath().resolve("test.txt")).toFile(), true);
    // Test directory not writable, but should be writable.
    assertValidInvalidDirectory(createNonWritableDirectory(), true);
  }

  private void assertValidInvalidDirectory(final File directory, final boolean writable) {
    final ConfigurationValidator validator = new ConfigurationValidator();

    validator.validateDirectory("TEST", directory, writable);
    assertEquals(1, validator.getValidationErrors().size(), "Should have 1 directory validation error");
  }

  private File createNonWritableDirectory() throws IOException {
    return Files.createDirectory(tempDir.toPath().resolve("testdir"),
        PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("r-xr-x---"))).toFile();
  }

  @Test
  void testValidateRequiredProperty() {
    final ConfigurationValidator validator = new ConfigurationValidator();

    assertRequiredPropertyValid(validator, "test");
    assertRequiredPropertyValid(validator, new Object());
    assertEquals(List.of(), validator.getValidationErrors(), "Should have no validation errors");
  }

  private void assertRequiredPropertyValid(final ConfigurationValidator validator, final Object object) {
    assertTrue(validator.validateRequiredProperty("TEST1", object), "Validation should return true");
  }

  @Test
  void testValidateRequiredPropertyNull() {
    assertRequiredPropertyEmpty(null);
    assertRequiredPropertyEmpty("");
  }

  private void assertRequiredPropertyEmpty(final String value) {
    final ConfigurationValidator validator = new ConfigurationValidator();

    assertFalse(validator.validateRequiredProperty("TEST", null), "Validation should return false");
    assertEquals(1, validator.getValidationErrors().size(), "Should have 1 validation error");
  }
}
