/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.description;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.http.MockHttpClientProxy;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ModelDataUtil}.
 */
@ExtendWith(MockitoExtension.class)
class ModelDataUtilTest {

  private static final String BASE_URL = "baseUrl";
  private static final String MODEL = "ops";
  private static final String MODEL_VERSION = "modelVersion";
  private static final String MODEL_FILE = "my_test-file.txt";
  private static final String DUMMY_TXT = "dummy.txt";

  private @Mock ModelDataUtil.ValidationTest validation;

  @Test
  void testDownloadDecompressAndUnpack(@TempDir final Path tempDir) throws Exception {
    final MockHttpClientProxy mockHttpClientProxy = new MockHttpClientProxy();

    final Path modelTestFile = Path.of(ModelDataUtilTest.class.getResource("modelVersion.tar.gz").toURI());
    final HttpClientManager httpClientManager = mockHttpClientProxy.mockFileResponse(modelTestFile);
    final ModelDataUtil modelDataUtil = new ModelDataUtil(new HttpClientProxy(httpClientManager));

    modelDataUtil.ensureModelDataAvailable(BASE_URL, MODEL, MODEL_VERSION, tempDir.toString(), ModelDataUtilTest::simpleTest, validation);

    assertTrue(Files.isDirectory(tempDir.resolve(MODEL_VERSION)), "A directory <modelsRootDirectory>/<model>/<modelVersion> should be present");
    assertTrue(Files.exists(tempDir.resolve(MODEL_VERSION).resolve("executable.sh")), "Contents of archive should be present");
    assertTrue(Files.isExecutable(tempDir.resolve(MODEL_VERSION).resolve("executable.sh")), "File permissions should be preserved");
    verify(validation, description("The validation function should be called.")).run();
  }

  @Test
  void testModelDataAlreadyPresent(@TempDir final Path tempDir) throws Exception {
    Files.createDirectory(tempDir.resolve(MODEL_VERSION));
    Files.createFile(tempDir.resolve(MODEL_VERSION).resolve(DUMMY_TXT));

    final MockHttpClientProxy mockHttpClientProxy = new MockHttpClientProxy();
    final HttpClientManager httpClientManagerMock = mockHttpClientProxy.getHttpClientManagerMock();

    final ModelDataUtil modelDataUtil = new ModelDataUtil(new HttpClientProxy(httpClientManagerMock));
    modelDataUtil.ensureModelDataAvailable(BASE_URL, MODEL, MODEL_VERSION, tempDir.toString(), ModelDataUtilTest::simpleTest, validation);

    verifyNoInteractions(httpClientManagerMock);
    verify(validation, description("The validation function should be called even when data already downloaded.")).run();
  }

  @Test
  void testModelDataNotFound(@TempDir final Path tempDir) throws IOException {
    final MockHttpClientProxy mockHttpClientProxy = new MockHttpClientProxy();
    final HttpClientManager httpClientManager = mockHttpClientProxy.mockHttpException();
    final ModelDataUtil modelDataUtil = new ModelDataUtil(new HttpClientProxy(httpClientManager));

    assertThrows(AeriusException.class,
        () -> modelDataUtil.ensureModelDataAvailable(BASE_URL, MODEL, MODEL_VERSION, tempDir.toString(), ModelDataUtilTest::simpleTest, validation),
        "Should throw an exception if the data could not be found");
  }

  @Test
  void testDownloadModelFile(@TempDir final Path tempDir) throws Exception {
    final MockHttpClientProxy mockHttpClientProxy = new MockHttpClientProxy();

    final Path modelTestFile = Path.of(ModelDataUtilTest.class.getResource("modelVersion/my_test-file.txt.gz").toURI());
    final HttpClientManager httpClientManager = mockHttpClientProxy.mockFileResponse(modelTestFile);
    final ModelDataUtil modelDataUtil = new ModelDataUtil(new HttpClientProxy(httpClientManager));

    modelDataUtil.ensureModelDataFileAvailable(BASE_URL, MODEL, List.of(MODEL_VERSION), MODEL_FILE, tempDir.toString());

    assertTrue(Files.exists(tempDir.resolve(MODEL_VERSION).resolve(MODEL_FILE)), "A file <modelsRootDirectory>/<model>/<modelVersion>/<modelFile> should be present");
  }

  @Test
  void testModelDataFileAlreadyPresent(@TempDir final Path tempDir) throws Exception {
    Files.createDirectory(tempDir.resolve(MODEL_VERSION));
    Files.createFile(tempDir.resolve(MODEL_VERSION).resolve(MODEL_FILE));

    final MockHttpClientProxy mockHttpClientProxy = new MockHttpClientProxy();
    final HttpClientManager httpClientManagerMock = mockHttpClientProxy.getHttpClientManagerMock();

    final ModelDataUtil modelDataUtil = new ModelDataUtil(new HttpClientProxy(httpClientManagerMock));
    modelDataUtil.ensureModelDataFileAvailable(BASE_URL, MODEL, List.of(MODEL_VERSION), MODEL_FILE, tempDir.toString());

    verifyNoInteractions(httpClientManagerMock);
  }

  @Test
  void testModelDataFileNotFound(@TempDir final Path tempDir) throws IOException {
    final MockHttpClientProxy mockHttpClientProxy = new MockHttpClientProxy();
    final HttpClientManager httpClientManager = mockHttpClientProxy.mockHttpException();
    final ModelDataUtil modelDataUtil = new ModelDataUtil(new HttpClientProxy(httpClientManager));

    final File file = modelDataUtil.ensureModelDataFileAvailable(BASE_URL, MODEL, List.of(MODEL_VERSION), MODEL_FILE, tempDir.toString());
    assertNull(file, "If file not found, don't throw but return null");
  }

  private static boolean simpleTest(final Path modelVersionRootDirectory) {
    // If there is at least one file in the model data directory.
    try (final Stream<Path> modelDataDirectoryContents = Files.list(modelVersionRootDirectory)) {
      return modelDataDirectoryContents.findFirst().isPresent();
    } catch (final IOException e) {
      return false;
    }
  }
}
