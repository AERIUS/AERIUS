/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;
import java.util.Properties;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link WorkerProperties}.
 */
class WorkerPropertiesTest {

  private static final int TEST_INT_VALUE = 123;
  private static final String TEST_STRING_VALUE = "dummy";
  private static final String TEST_KEY_1 = "key1";
  private static final String TEST_KEY_2 = "key2";
  private static final String PROCESSES = "processes";
  private static final String KEY_TEST = "test";

  @Test
  void testGetProcesses() {
    final WorkerProperties twc0 = getTestConfiguration(TEST_KEY_1, 10);
    assertEquals(0, twc0.getProcesses(), "Should return 0");
    assertFalse(twc0.isActive(), "Should not be active");

    final WorkerProperties twc1 = getTestConfiguration(PROCESSES, 10);
    assertEquals(10, twc1.getProcesses(), "Should return 10");
    assertTrue(twc1.isActive(), "Should not be active");

    final WorkerProperties twc2 = getTestConfiguration(PROCESSES, "auto");

    assertEquals(WorkerProperties.AUTO_PROCESSES, twc2.getProcesses(), "Should return 'AUTO_PROCESSES' value");
    assertTrue(twc2.isActive(), "Should not be active");
  }

  @Test
  void testGetPropertyOptional() {
    final WorkerProperties twc = getTestConfiguration(TEST_KEY_1, TEST_STRING_VALUE);

    assertEquals(TEST_STRING_VALUE, twc.getPropertyOptional(TEST_KEY_1).get(), "Should return int value");
    assertEquals(Optional.empty(), twc.getPropertyOptional(TEST_KEY_2), "Should return default 0 when key doesn't exist");
  }

  @Test
  void testGetPropertyIntSafe() {
    final WorkerProperties twc = getTestConfiguration(TEST_KEY_1, TEST_INT_VALUE);

    assertEquals(TEST_INT_VALUE, twc.getPropertyIntSafe(TEST_KEY_1), "Should return int value");
    assertEquals(0, twc.getPropertyIntSafe(TEST_KEY_2), "Should return default 0 when key doesn't exist");
  }

  @Test
  void testGetPropertyDefault() {
    final WorkerProperties emptyWorkerProperties = new WorkerProperties(new Properties(), KEY_TEST);

    assertEquals(TEST_INT_VALUE, emptyWorkerProperties.getPropertyOrDefault(KEY_TEST, TEST_INT_VALUE),
        "Shoudl return default int value");
    assertEquals(TEST_STRING_VALUE,
        new WorkerProperties(new Properties(), KEY_TEST).getPropertyOrDefault(KEY_TEST, TEST_STRING_VALUE),
        "Shoudl return default string value");
  }

  @Test
  void testGetPropertyBooleanSafe() {
    final WorkerProperties twc = getTestConfiguration(TEST_KEY_1, true);

    assertEquals(true, twc.getPropertyBooleanSafe(TEST_KEY_1), "Should return boolean value");
    assertEquals(false, twc.getPropertyBooleanSafe(TEST_KEY_2), "Should return false when key doesn't exist");
  }

  private WorkerProperties getTestConfiguration(final String key, final Object value) {
    final Properties p = new Properties();

    p.setProperty(KEY_TEST + "." + key, String.valueOf(value));
    return new WorkerProperties(p, KEY_TEST);
  }
}