/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMonitorSubstance;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRejectionGrounds;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.NcaCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;

/**
 * Test class for {@link FeaturePointToEnginePointUtil}.
 */
class FeaturePointToEnginePointUtilTest {

  @Test
  void testToSubPoint() {
    final SubPoint subPoint = new SubPoint();
    subPoint.setReceptorId(10);
    subPoint.setSubPointId(2);
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setGeometry(new Point());
    feature.setProperties(subPoint);

    final List<AeriusPoint> convertedPoints = FeaturePointToEnginePointUtil.convert(List.of(feature));
    assertEquals(1, convertedPoints.size(), "Should have the converted point");
    assertEquals(2, convertedPoints.get(0).getId(), "Should have id");
    assertEquals(10, convertedPoints.get(0).getParentId(), "Should have parent id");
  }

  @Test
  void testToNCAPointEmpty() {
    final NcaCustomCalculationPoint customPoint = new NcaCustomCalculationPoint();

    final AeriusPoint converted = FeaturePointToEnginePointUtil.toNCAPoint(customPoint);

    assertTrue(converted instanceof ADMSCalculationPoint, "Should be expected type");
    final ADMSCalculationPoint convertedADMS = (ADMSCalculationPoint) converted;
    assertEquals(0.0, convertedADMS.getRoadLocalFNO2(), "Should have default double value");
  }

  @Test
  void testToNCAPointWithValue() {
    final NcaCustomCalculationPoint customPoint = new NcaCustomCalculationPoint();
    final double customFraction = 0.4;
    customPoint.setRoadLocalFractionNO2(customFraction);

    final AeriusPoint converted = FeaturePointToEnginePointUtil.toNCAPoint(customPoint);

    assertTrue(converted instanceof ADMSCalculationPoint, "Should be expected type");
    final ADMSCalculationPoint convertedADMS = (ADMSCalculationPoint) converted;
    assertEquals(customFraction, convertedADMS.getRoadLocalFNO2(), "Should have the original point value");
  }

  @Test
  void testToCimlkPoint() {
    final CIMLKCalculationPoint point = new CIMLKCalculationPoint();
    point.setMonitorSubstance(CIMLKMonitorSubstance.ALL);
    point.setRejectionGrounds(CIMLKRejectionGrounds.NONE);

    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setProperties(point);
    feature.setGeometry(new nl.overheid.aerius.shared.domain.v2.geojson.Point(0, 0));

    final List<AeriusPoint> enginePoints = FeaturePointToEnginePointUtil.convert(List.of(feature));

    assertEquals(nl.overheid.aerius.shared.domain.theme.cimlk.CIMLKCalculationPoint.class, enginePoints.get(0).getClass(),
        "Result should be of cimlk engine point type");
    final nl.overheid.aerius.shared.domain.theme.cimlk.CIMLKCalculationPoint cimlkCalculationPoint =
        (nl.overheid.aerius.shared.domain.theme.cimlk.CIMLKCalculationPoint) enginePoints.get(0);

    assertEquals(CIMLKMonitorSubstance.ALL, cimlkCalculationPoint.getMonitorSubstance(),
        "Engine point should have the original monitorSubstance value");
    assertEquals(CIMLKRejectionGrounds.NONE, cimlkCalculationPoint.getRejectionGrounds(),
        "Engine point should have the original rejectionGrounds value");
  }
}
