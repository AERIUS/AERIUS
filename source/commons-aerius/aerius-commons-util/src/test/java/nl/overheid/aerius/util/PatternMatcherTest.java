/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

class PatternMatcherTest {

  @Test
  void testMatchEmpty() {
    final PatternMatcher matcher = new PatternMatcher("");
    assertEquals(List.of(), matcher.getRegexPatterns(), "returns the correct regex patterns");
    assertFalse(matcher.matches(""), "should match nothing");
    assertFalse(matcher.matches("any string"), "should match nothing");
  }

  @Test
  void testMatchAll() {
    final PatternMatcher matcher = new PatternMatcher("*");
    assertEquals(List.of(".*"), matcher.getRegexPatterns(), "returns the correct regex patterns");
    assertTrue(matcher.matches("any string"), "should match any string");
  }

  @Test
  void testMatchExact() {
    final PatternMatcher matcher = new PatternMatcher("2022");
    assertEquals(List.of("\\Q2022\\E"), matcher.getRegexPatterns(), "returns the correct regex patterns");
    assertTrue(matcher.matches("2022"), "should match only exact string");
    assertFalse(matcher.matches("2022.1"), "should not match not exact strings");
  }

  @Test
  void testMatchAlternatives() {
    final PatternMatcher matcher = new PatternMatcher("2022,2023");
    assertEquals(List.of("\\Q2022\\E", "\\Q2023\\E"), matcher.getRegexPatterns(), "returns the correct regex patterns");
    assertTrue(matcher.matches("2022"), "should match only exact string (first alternative)");
    assertTrue(matcher.matches("2023"), "should match only exact string (second alternative)");
    assertFalse(matcher.matches("2022.1"), "should not match not exact strings");
  }

  @Test
  void testMatchWildcard() {
    final PatternMatcher matcher = new PatternMatcher("2022.2,2023.*");
    assertEquals(List.of("\\Q2022.2\\E", "\\Q2023.\\E.*"), matcher.getRegexPatterns(), "returns the correct regex patterns");
    assertTrue(matcher.matches("2022.2"), "should match the exact string (first alternative)");
    assertTrue(matcher.matches("2023.1"), "should match the wildcard (second alternative)");
    assertTrue(matcher.matches("2023.2"), "should match the wildcard (second alternative)");
    assertFalse(matcher.matches("2022.1"), "should not match any alternative");
  }
}
