/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.i18n;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link AeriusExceptionMessages}.
 */
class AeriusExceptionMessagesTest {

  private static final String JUST_A_SENSTENCE = "JUST A SENSTENCE";

  @Test
  void testGetMessage() {
    final AeriusExceptionMessages bundle = new AeriusExceptionMessages(LocaleUtils.getDefaultLocale());

    final AeriusException ae666 = new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    final String ae666Message = bundle.getString(ae666);
    assertFalse(ae666Message.isEmpty(), "Check interne error");
    final AeriusException ae1001 = new AeriusException(AeriusExceptionReason.CALCULATION_NO_SOURCES);
    assertNotEquals(ae666Message, bundle.getString(ae1001), "Check not always internal error");
    assertTrue(bundle.getString(ae1001).contains("{0}"), "Check contains replace token");
    final AeriusException ae1001_2 = new AeriusException(AeriusExceptionReason.CALCULATION_NO_SOURCES, JUST_A_SENSTENCE);
    assertTrue(bundle.getString(ae1001_2).contains(JUST_A_SENSTENCE), "Check contains replaced value");
    final AeriusException ae668 = new AeriusException(AeriusExceptionReason.SQL_ERROR_CONSTANTS_MISSING);
    assertNotEquals(ae666Message, bundle.getString(ae668), "Check not a internal error");
    assertFalse(bundle.getString(ae668).isEmpty(), "Check not empty");
  }

  @ParameterizedTest
  @CsvSource({"nl", "en"})
  void testMissingFromBundles(final String locale) {
    final AeriusExceptionMessages bundle = new AeriusExceptionMessages(new Locale(locale));
    final List<Reason> missingReasons = new ArrayList<>();

    System.out.println(locale);
    for (final Reason reason : allReasons()) {
      if (!bundle.containsKey(reason.getErrorCodeKey())) {
        missingReasons.add(reason);
        System.out.println(reason + ":" + reason.getErrorCode());
      }
    }
    assertTrue(missingReasons.isEmpty(),
        "Missing reasons from bundle " + locale + ": " + missingReasons.stream().map(Reason::getErrorCodeKey).collect(Collectors.joining(",")));
  }

  @ParameterizedTest
  @CsvSource({"nl", "en"})
  void testObsoleteInBundles(final String locale) {
    final AeriusExceptionMessages bundle = new AeriusExceptionMessages(new Locale(locale));
    final List<String> obsoleteKeys = new ArrayList<>();

    for (final Iterator<String> iterator = bundle.getKeys().asIterator(); iterator.hasNext();) {
      boolean present = false;
      final String key = iterator.next();
      for (final Reason reason : allReasons()) {
        if (key.equals(reason.getErrorCodeKey())) {
          present = true;
        }
      }
      if (!present) {
        obsoleteKeys.add(key);
      }
    }
    obsoleteKeys.sort((a, b) -> Integer.compare(Integer.parseInt(a.substring(1)), Integer.parseInt(b.substring(1))));
    if (!obsoleteKeys.isEmpty()) {
      System.out.println(locale);
      obsoleteKeys.forEach(System.out::println);
    }
    assertTrue(obsoleteKeys.isEmpty(),
        "Obsolete keys in bundle " + locale + ": " + String.join(",", obsoleteKeys));
  }

  private static List<Reason> allReasons() {
    return Stream.concat(Stream.of(ImaerExceptionReason.values()), Stream.of(AeriusExceptionReason.values())).collect(Collectors.toList());
  }

}
