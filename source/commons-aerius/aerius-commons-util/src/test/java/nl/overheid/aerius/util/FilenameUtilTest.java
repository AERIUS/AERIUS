/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static nl.overheid.aerius.util.FilenameUtil.MAX_OPTIONAL_PART_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.Theme;

/**
 * Test class for {@link FilenameUtil}.
 */
class FilenameUtilTest {

  private static final String TEST_FILE_NAME = "someFil_-.";
  private static final String TEST_FILE_NAME_WITH_SPACES = "some Fil   ë   _-.";
  private static final String TEST_UNSAFE_FILE_NAME = "someFil:/$^%'_ë-.";
  private static final String TEST_FILE_NAME_EXTENSION = "gml";
  private static final String TEST_LONG_FILE_NAME = "a".repeat(300);

  @Test
  void testGetSafeFilename() {
    String filename = FilenameUtil.getSafeFilename(null);
    assertNull(filename, "Supplying null should return null");
    filename = FilenameUtil.getSafeFilename("");
    assertEquals("", filename, "Supplying empty string should return empty string");
    filename = FilenameUtil.getSafeFilename(TEST_FILE_NAME);
    assertEquals(TEST_FILE_NAME, filename, "Correct file name should be returned as is");
    filename = FilenameUtil.getSafeFilename(TEST_FILE_NAME_WITH_SPACES);
    assertEquals(TEST_FILE_NAME, filename, "Spaces should be removed");
    filename = FilenameUtil.getSafeFilename(TEST_UNSAFE_FILE_NAME);
    assertEquals(TEST_FILE_NAME, filename, "Unsafe characters should be removed");
  }

  @ParameterizedTest
  @MethodSource("dataFilenames")
  void testGetFilename(final Theme prefix, final String extension, final String opt1, final String opt2, final Date date,
      final String expectedFilename) {
    final String filename = FilenameUtil.getFilename(prefix, extension, date, opt1, opt2);
    assertNotNull(filename, "filename returned shouldn't be null");
    final String processedFilename = date == null ? filename.replaceFirst("\\d{14}", "X") : filename;

    assertEquals(expectedFilename, processedFilename, "Should match expected filename");
  }

  // Convert LocalDateTime to Date
  private static Collection<Arguments> dataFilenames() {
    return List.of(
        Arguments.of(Theme.OWN2000, TEST_FILE_NAME_EXTENSION, null, null, null, "AERIUS_X.gml"),
        Arguments.of(Theme.NCA, TEST_FILE_NAME_EXTENSION, null, null, null, "UKAPAS_X.gml"),
        Arguments.of(Theme.OWN2000, '.' + TEST_FILE_NAME_EXTENSION, null, null, null, "AERIUS_X.gml"),
        Arguments.of(Theme.OWN2000, "", null, null, null, "AERIUS_X"),
        Arguments.of(Theme.OWN2000, TEST_FILE_NAME_EXTENSION, null, null,
            Date.from(LocalDateTime.of(2013, 12, 30, 18, 21, 33).atZone(ZoneId.systemDefault()).toInstant()), "AERIUS_20131230182133.gml"),
        Arguments.of(Theme.OWN2000, TEST_FILE_NAME_EXTENSION, "one", null, null, "AERIUS_X_one.gml"),
        Arguments.of(Theme.OWN2000, TEST_FILE_NAME_EXTENSION, "one", "", null, "AERIUS_X_one.gml"),
        Arguments.of(Theme.OWN2000, TEST_FILE_NAME_EXTENSION, "", "two", null, "AERIUS_X_two.gml"),
        Arguments.of(Theme.OWN2000, TEST_FILE_NAME_EXTENSION, null, "two", null, "AERIUS_X_two.gml"),
        Arguments.of(Theme.OWN2000, TEST_FILE_NAME_EXTENSION, "one", "two", null, "AERIUS_X_one_two.gml"),
        Arguments.of(Theme.OWN2000, TEST_FILE_NAME_EXTENSION, TEST_LONG_FILE_NAME, null, null,
            String.format("AERIUS_X_%." + MAX_OPTIONAL_PART_LENGTH + "s.gml", TEST_LONG_FILE_NAME)),
        Arguments.of(Theme.OWN2000, TEST_FILE_NAME_EXTENSION, null, TEST_LONG_FILE_NAME, null,
            String.format("AERIUS_X_%." + MAX_OPTIONAL_PART_LENGTH + "s.gml", TEST_LONG_FILE_NAME))
        );
  }

  @Test
  void testGetFilenameWithoutExtendsionNullPrefix() {
    assertThrows(
        IllegalArgumentException.class,
        () -> FilenameUtil.getFilename((Theme) null, null, null, null, null),
        "Expected IllegalArgumentException");
  }

  @Test
  void testGetFilenameNullPrefix() {
    assertThrows(
        IllegalArgumentException.class,
        () -> FilenameUtil.getFilename((Theme) null, TEST_FILE_NAME_EXTENSION, null, null, null),
        "Expected IllegalArgumentException");
  }

  @Test
  void testGetFilenameNullExtension() {
    assertThrows(
        IllegalArgumentException.class,
        () -> FilenameUtil.getFilename(TEST_FILE_NAME, null, null, null, null),
        "Expected IllegalArgumentException");
  }
}
