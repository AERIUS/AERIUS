/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;

/**
 * Test class for {@link AbstractSubReceptorAggregator}
 */
class AbstractSubReceptorAggregatorTest {

  private static final int TEST_RECEPTOR_1_ID = 23131;
  private static final int TEST_RECEPTOR_2_ID = 48483;
  private static final int TEST_SUB_RECEPTOR_2_ID = 103;
  private static final int CUSTOM_POINT_ID = 998;
  private static final Set<EmissionResultKey> NOX_ERK = EnumSet.of(EmissionResultKey.NOX_CONCENTRATION);

  @Test
  void testAggregation() {
    final Collection<AeriusPoint> receptors = createSubPointsFromHexagon();
    receptors.add(createSubReceptor(TEST_SUB_RECEPTOR_2_ID, TEST_RECEPTOR_2_ID, AeriusPointType.RECEPTOR, AeriusPointType.SUB_RECEPTOR));
    receptors.add(createSubReceptor(TEST_RECEPTOR_2_ID, TEST_RECEPTOR_2_ID, AeriusPointType.RECEPTOR, AeriusPointType.RECEPTOR));
    receptors.add(createSubReceptor(CUSTOM_POINT_ID, CUSTOM_POINT_ID, AeriusPointType.POINT, AeriusPointType.POINT));
    final TestSubReceptorAggregator aggregator = new TestSubReceptorAggregator(receptors, NOX_ERK, false);
    final List<AeriusResultPoint> originalResults = createResults(receptors);
    final List<AeriusResultPoint> allResults = aggregator.aggregate(originalResults);

    assertEquals(12, originalResults.size(), "Expect all result input to be without type receptor");
    assertEquals(14, allResults.size(), "Expect same size as original number of receptors");

    // Assert if aggregated results are as expected
    assertEquals(3, aggregator.aggregatedReceptorResults.size(),
        "Because receptors are expected to have been supplied along with a sub receptor, we expect 3 aggregated results");

    // Assert if aggregated results are as expected
    // Avg: (1 + 2 + 0 + 4 + 5 + 0 + 7 + 8 + 0 + 10) / 10
    final Optional<AeriusResultPoint> centerResults = aggregator.aggregatedReceptorResults.stream().filter(p -> p.getId() == TEST_RECEPTOR_1_ID)
        .findAny();
    assertTrue(centerResults.isPresent(), "Results should contain the center receptor again");
    assertEquals(10, centerResults.get().getNumberOfSubPointsUsed(), "Not the expected number of points calculated for aggregated receptor");
    assertEquals(3.7, centerResults.get().getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1, "Should have aggregated concentration");

    // Assert result for 2nd receptor point (which is receptor with sub receptor at level 0)
    final Optional<AeriusResultPoint> receptor2Result = aggregator.aggregatedReceptorResults.stream().filter(p -> p.getId() == TEST_RECEPTOR_2_ID)
        .findAny();
    assertTrue(receptor2Result.isPresent(), "Results should contain the other receptor again, even if no subreceptors were around");
    assertEquals(103.0, receptor2Result.get().getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1, "Should have aggregated concentration");

    // Assert if subreceptors results are as expected
    assertEquals(11, aggregator.subReceptorResults.size(), "Results should contain the same number of subreceptors.");
    assertEquals(140.0, aggregator.subReceptorResults.stream().mapToDouble(p -> p.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION)).sum(), 0.1,
        "Expect the total of subreceptors to sum up");

    // Assert if other point results are as expected
    final List<AeriusResultPoint> otherResults = allResults.stream().filter(p -> p.getPointType() == AeriusPointType.POINT).toList();
    assertEquals(1, otherResults.size(), "Results should contain the custom point results");
    // using id = nox concentration, so can compare to ID in this case (it shouldn't be changed).
    assertEquals(CUSTOM_POINT_ID, otherResults.get(0).getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1,
        "Should have aggregated concentration");
  }

  @Test
  void testAggregationExcludingZeroResults() {
    final Collection<AeriusPoint> receptors = createSubPointsFromHexagon();
    final TestSubReceptorAggregator aggregator = new TestSubReceptorAggregator(receptors, NOX_ERK, true);
    final List<AeriusResultPoint> originalResults = createResults(receptors);
    aggregator.aggregate(originalResults);

    assertEquals(1, aggregator.aggregatedReceptorResults.size(),
        "Because receptors are expected to have been supplied along with a sub receptor, we expect 1 aggregated results");
    final Optional<AeriusResultPoint> centerResults = aggregator.aggregatedReceptorResults.stream().filter(p -> p.getId() == TEST_RECEPTOR_1_ID)
        .findAny();
    assertTrue(centerResults.isPresent(), "Results should contain the center receptor again");
    // Avg: (1 + 2 + 4 + 5 + 7 + 8 + 10) / 7
    assertEquals(5.28, centerResults.get().getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1, "Should have aggregated concentration");
    assertEquals(10, aggregator.subReceptorResults.size(), "Results should contain the same number of subreceptors.");
  }

  @Test
  void testAggregationWithNaN() {
    final Collection<AeriusPoint> receptors = createSubPointsFromHexagon();

    final TestSubReceptorAggregator aggregator = new TestSubReceptorAggregator(receptors, NOX_ERK, false);
    final List<AeriusResultPoint> originalResults = createResults(receptors);
    // Simulate one of the subpoints having a NaN result.
    originalResults.get(4).setEmissionResult(EmissionResultKey.NOX_CONCENTRATION, Double.NaN);
    aggregator.aggregate(originalResults);

    assertEquals(1, aggregator.aggregatedReceptorResults.size(),
        "Because receptors are expected to have been supplied along with a sub receptor, we expect 1 aggregated results");
    final Optional<AeriusResultPoint> centerResults = aggregator.aggregatedReceptorResults.stream().filter(p -> p.getId() == TEST_RECEPTOR_1_ID)
        .findAny();
    assertTrue(centerResults.isPresent(), "Results should contain the center receptor again");
    assertTrue(Double.isNaN(centerResults.get().getEmissionResult(EmissionResultKey.NOX_CONCENTRATION)), "Aggregated result should also be NaN");
  }

  @Test
  void testAggregationWithMultipleSubstances() {
    final Collection<AeriusPoint> receptors = createSubPointsFromHexagon();
    final Set<EmissionResultKey> erks = EnumSet.of(EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NH3_CONCENTRATION);
    final TestSubReceptorAggregator aggregator = new TestSubReceptorAggregator(receptors, erks, false);
    final List<AeriusResultPoint> noxResults = createResults(receptors);
    aggregator.aggregate(noxResults);

    assertEquals(1, aggregator.aggregatedReceptorResults.size(),
        "Because receptors are expected to have been supplied along with a sub receptor, we expect 1 aggregated results");
    final Optional<AeriusResultPoint> centerResults = aggregator.aggregatedReceptorResults.stream().filter(p -> p.getId() == TEST_RECEPTOR_1_ID)
        .findAny();
    assertTrue(centerResults.isPresent(), "Results should contain the center receptor again");
    assertEquals(3.7, centerResults.get().getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1, "Should have aggregated NOx concentration");
    assertFalse(centerResults.get().getEmissionResults().hasResult(EmissionResultKey.NH3_CONCENTRATION),
        "Shouldn't have aggregated NH3 concentration");

    final List<AeriusResultPoint> nh3Results = createResults(receptors, EmissionResultKey.NH3_CONCENTRATION);
    aggregator.aggregate(nh3Results);
    assertTrue(centerResults.isPresent(), "Results should contain the center receptor again");
    assertEquals(3.7, centerResults.get().getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1,
        "Should still have aggregated NOx concentration");
    assertEquals(7.4, centerResults.get().getEmissionResult(EmissionResultKey.NH3_CONCENTRATION), 0.1, "Should have aggregated NH3 concentration");
  }

  @Test
  void testPlainReceptorPoint() {
    final Collection<AeriusPoint> receptors = List.of(new AeriusPoint(TEST_RECEPTOR_1_ID, AeriusPointType.RECEPTOR));
    final TestSubReceptorAggregator aggregator = new TestSubReceptorAggregator(receptors, NOX_ERK, false);
    final List<AeriusResultPoint> noxResults = createResults(receptors);

    noxResults.get(0).setPointType(AeriusPointType.RECEPTOR);
    final List<AeriusResultPoint> aggregatedResults = aggregator.aggregate(noxResults);
    assertEquals(1, aggregator.aggregatedReceptorResults.size(),
        "Only a single receptor point should be in results as there were no sub receptor points to aggregate into a new result point.");
    final AeriusResultPoint plainResult = aggregatedResults.stream().filter(p -> p.getId() == TEST_RECEPTOR_1_ID).findAny().get();
    assertEquals(1, plainResult.getNumberOfSubPointsUsed(), "Not the expected number of points calculated for plain receptors");
    assertEquals(23131, plainResult.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1, "Should have aggregated NOx concentration");
    assertEquals(AeriusPointType.RECEPTOR, plainResult.getPointType(), "Should have point type RECEPTOR");
  }

  private Collection<AeriusPoint> createSubPointsFromHexagon() {
    final List<AeriusPoint> points = new ArrayList<>();

    points.add(createSubReceptor(TEST_RECEPTOR_1_ID, TEST_RECEPTOR_1_ID, AeriusPointType.RECEPTOR, AeriusPointType.RECEPTOR));
    for (int i = 1; i <= 10; i++) {
      final TestSubPoint rp = createSubReceptor(i, TEST_RECEPTOR_1_ID, AeriusPointType.RECEPTOR, AeriusPointType.SUB_RECEPTOR);
      points.add(rp);
    }
    return points;
  }

  private List<AeriusResultPoint> createResults(final Collection<AeriusPoint> receptors) {
    return createResults(receptors, EmissionResultKey.NOX_CONCENTRATION);
  }

  private List<AeriusResultPoint> createResults(final Collection<AeriusPoint> receptors, final EmissionResultKey erk) {
    return receptors.stream().filter(p -> (!(p instanceof IsSubPoint)) || p.getPointType() != AeriusPointType.RECEPTOR).map(p -> {
      final AeriusResultPoint rp = new AeriusResultPoint(p);
      if (p.getId() % 3 != 0) {
        rp.setEmissionResult(erk, Double.valueOf(p.getId() * (erk == EmissionResultKey.NOX_CONCENTRATION ? 1 : 2)));
      }
      return rp;
    }).toList();
  }

  private TestSubPoint createSubReceptor(final int id, final Integer parentId, final AeriusPointType parentPointType, final AeriusPointType type) {
    return new TestSubPoint(id, parentId, parentPointType, type);
  }

  private static class TestSubReceptorAggregator extends AbstractSubReceptorAggregator<AeriusResultPoint, AeriusPoint> {
    final List<AeriusResultPoint> aggregatedReceptorResults = new ArrayList<>();
    final List<AeriusResultPoint> subReceptorResults = new ArrayList<>();

    protected TestSubReceptorAggregator(final Collection<AeriusPoint> receptors, final Set<EmissionResultKey> erks,
        final boolean excludeZeroResults) {
      super(receptors, erks, excludeZeroResults);
    }

    @Override
    protected List<AeriusResultPoint> combine(final Collection<AeriusResultPoint> aggregatedReceptorResults,
        final Collection<AeriusResultPoint> subReceptorResults) {
      // Intercept the supplied arguments for further validation.
      this.aggregatedReceptorResults.addAll(aggregatedReceptorResults);
      this.subReceptorResults.addAll(subReceptorResults);

      return Stream.of(aggregatedReceptorResults, subReceptorResults).flatMap(Collection::stream).collect(Collectors.toList());
    }

  }

  private static class TestSubPoint extends AeriusPoint implements IsSubPoint {

    private static final long serialVersionUID = 2L;

    private AeriusPointType parentPointType;

    TestSubPoint(final int id, final Integer parentId, final AeriusPointType parentPointType, final AeriusPointType type) {
      super(id, type);
      setParentId(parentId);
    }

    @Override
    public AeriusPointType getParentPointType() {
      return parentPointType;
    }

    @Override
    public void setParent(final AeriusPoint parentPoint) {
      throw new IllegalStateException("Not expected to be used in this test");
    }

    @Override
    public int getLevel() {
      throw new IllegalStateException("Not expected to be used in this test");
    }

    @Override
    public Polygon getArea() {
      throw new IllegalStateException("Not expected to be used in this test");
    }

    @Override
    public void setArea(final Polygon geometry) {
      throw new IllegalStateException("Not expected to be used in this test");
    }

  }
}
