/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
class ScenarioValidationUtilTest {

  @ParameterizedTest
  @MethodSource("validCases")
  void testIsValidProjectScenarioValid(final List<ScenarioSituation> situations) throws AeriusException {
    // Test with valid situation
    final Scenario scenario = mock(Scenario.class);
    when(scenario.getSituations()).thenReturn(situations);
    final boolean valid = ScenarioValidationUtil.isValidProjectScenario(scenario);

    assertTrue(valid, "Supplied case should be valid");
  }

  private static Stream<Arguments> validCases() {
    return Stream.of(
        Arguments.of(List.of(mockSituation(SituationType.PROPOSED))),
        Arguments.of(List.of(mockSituation(SituationType.PROPOSED), mockSituation(SituationType.PROPOSED))),
        Arguments.of(List.of(mockSituation(SituationType.PROPOSED), mockSituation(SituationType.REFERENCE))),
        Arguments.of(List.of(mockSituation(SituationType.PROPOSED), mockSituation(SituationType.OFF_SITE_REDUCTION))),
        Arguments.of(List.of(mockSituation(SituationType.PROPOSED), mockSituation(SituationType.REFERENCE), mockSituation(SituationType.OFF_SITE_REDUCTION))));
  }

  @ParameterizedTest
  @MethodSource("invalidCases")
  void testIsValidProjectScenarioInvalid(final List<ScenarioSituation> situations) throws AeriusException {
    // Test with valid situation
    final Scenario scenario = mock(Scenario.class);
    when(scenario.getSituations()).thenReturn(situations);
    final boolean valid = ScenarioValidationUtil.isValidProjectScenario(scenario);

    assertFalse(valid, "Supplied case should be invalid");
  }

  private static Stream<Arguments> invalidCases() {
    return Stream.of(
        Arguments.of(List.of()),
        Arguments.of(List.of(mockSituation(SituationType.REFERENCE))),
        Arguments.of(List.of(mockSituation(SituationType.PROPOSED), mockSituation(SituationType.REFERENCE), mockSituation(SituationType.REFERENCE))),
        Arguments.of(List.of(mockSituation(SituationType.PROPOSED), mockSituation(SituationType.REFERENCE), mockSituation(SituationType.TEMPORARY))));
  }

  private static ScenarioSituation mockSituation(final SituationType type) {
    final ScenarioSituation situation = mock(ScenarioSituation.class);
    when(situation.getType()).thenReturn(type);
    return situation;
  }

}
