/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.processmonitor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.exec.ProcessListener;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link ProcessMonitorService}.
 */
@ExtendWith(MockitoExtension.class)
class ProcessMonitorServiceTest {

  private static final String JOB_KEY_1 = "1234";
  private static final String JOB_KEY_2 = "4321";

  private @Mock Process process1;
  private @Mock Process process2;
  private @Mock Supplier<Long> now;
  private @InjectMocks ProcessMonitorService processMonitorService;
  private ProcessListener processListener2;

  @Test
  void testIsCancelledJob() throws AeriusException {
    mockNow();
    processMonitorService.killCancelledJob(JOB_KEY_1);
    assertTrue(processMonitorService.isCancelledJob(JOB_KEY_1), "Job should be being marked as cancelled.");
    assertProcess2Alive();
  }

  @Test
  void testCreateProcessListener() throws AeriusException {
    mockNow();
    // Should have new process listener
    final ProcessListener processListenerA = processMonitorService.registerProcessListener(JOB_KEY_1);
    assertNotNull(processListenerA, "Should have a processListener");
    // If registering again it should return the same processlistener
    final ProcessListener processListenerB = processMonitorService.registerProcessListener(JOB_KEY_1);
    assertSame(processListenerA, processListenerB, "Should return the same process listener when registering again");
    processMonitorService.killCancelledJob(JOB_KEY_1);
    // Trigger exception on cancelled job
    final AeriusException exception = assertThrows(AeriusException.class, () -> processMonitorService.registerProcessListener(JOB_KEY_1),
        "Should throw an exception that the job was cancelled");
    assertSame(AeriusExceptionReason.CONNECT_JOB_CANCELLED, exception.getReason(), "Should have CONNECT_JOB_CANCELLED reason");
    assertProcess2Alive();
  }

  @Test
  void testCleanCache() throws AeriusException {
    assertJobIsCancelled();
    // Test if job is not removed when job is too young to be removed.
    processMonitorService.cleanCache(1);
    assertTrue(processMonitorService.isCancelledJob(JOB_KEY_1), "Job should still being marked as cancelled because clear is for older jobs.");
    assertCleanOfCacheWorked();
  }

  @Test
  void testCleanCacheWithProcessListener() throws AeriusException {
    doReturn(true).when(process1).isAlive();
    // Test if job is not removed when a process is still registered.
    final ProcessListener processListener = processMonitorService.registerProcessListener(JOB_KEY_1);
    processListener.registerProcess(process1);
    assertJobIsCancelled();

    processMonitorService.cleanCache(-1);  // use -1 to fake timestamp that is older than current time, basically it will give a future time.
    assertTrue(processMonitorService.isCancelledJob(JOB_KEY_1), "Job should still being marked as cancelled because process still alive.");
    processListener.removeProcess(process1);
    assertTrue(((List<Process>) processListener).isEmpty(), "Check if process listener is empty after kill processes.");
    assertCleanOfCacheWorked();
  }

  @Test
  void testCleanCacheWithDeadProcesses() throws AeriusException {
    doReturn(false).when(process1).isAlive();
    // Test if job is not removed when a process is still registered.
    final ProcessListener processListener = processMonitorService.registerProcessListener(JOB_KEY_1);
    processListener.registerProcess(process1);
    assertJobIsCancelled();

    processMonitorService.cleanCache(-1);  // use -1 to fake timestamp that is older than current time, basically it will give a future time.
    assertFalse(processMonitorService.isCancelledJob(JOB_KEY_1), "Job should not marked as cancelled because process was dead.");
    assertTrue(((List<Process>) processListener).isEmpty(), "Check if process listener is empty after kill processes.");
    assertCleanOfCacheWorked();
  }

  private void assertJobIsCancelled() throws AeriusException {
    doReturn(System.currentTimeMillis()).when(now).get();
    mockAdditionlJob();
    processMonitorService.killCancelledJob(JOB_KEY_1);
    // Test if job actual exists
    assertTrue(processMonitorService.isCancelledJob(JOB_KEY_1), "Should have Job being marked as cancelled.");
  }

  private void assertCleanOfCacheWorked() {
    // Test if job is not known anymore if deleted because to old
    processMonitorService.cleanCache(-1);
    assertFalse(processMonitorService.isCancelledJob(JOB_KEY_1), "Job should not be known anymore and return false.");
    assertProcess2Alive();
  }

  @Test
  void testKillCancelledJob() throws AeriusException {
    assertJobBeingCancelled(() -> {}, 1);
  }

  @Test
  void testKillAllCancelledJobs() throws AeriusException {
    assertJobBeingCancelled(() -> processMonitorService.killAllCancelledJobs(), 2);
  }

  private void assertJobBeingCancelled(final Runnable killCall, final int times) throws AeriusException {
    mockNow();
    doReturn(true).when(process1).isAlive();
    final ProcessListener processListener = processMonitorService.registerProcessListener(JOB_KEY_1);
    assertFalse(processMonitorService.isCancelledJob(JOB_KEY_1), "Job should not being registered as being cancelled.");
    processListener.registerProcess(process1);
    processMonitorService.killCancelledJob(JOB_KEY_1);
    killCall.run();
    verify(process1, times(times)).destroyForcibly();
    assertTrue(processMonitorService.isCancelledJob(JOB_KEY_1), "Should have Job being marked as cancelled after kill command.");
    assertProcess2Alive();
  }

  private void mockNow() throws AeriusException {
    doReturn(1L).when(now).get();
    mockAdditionlJob();
  }

  private void mockAdditionlJob() throws AeriusException {
    processListener2 = processMonitorService.registerProcessListener(JOB_KEY_2);
    processListener2.registerProcess(process2);
    doReturn(true).when(process2).isAlive();
    processMonitorService.killCancelledJob(JOB_KEY_2);
  }

  private void assertProcess2Alive() {
    assertTrue(processMonitorService.isCancelledJob(JOB_KEY_2), "Check if job 2 still is cancelled");
    assertEquals(1, ((List<Process>) processListener2).size(), "Check if 2nd process listener is not empty.");
  }
}
