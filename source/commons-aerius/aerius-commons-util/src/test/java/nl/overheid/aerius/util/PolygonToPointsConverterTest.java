/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;

/**
 * Test class for {@link PolygonToPointsConverter}.
 */
class PolygonToPointsConverterTest {

  private static final double GRID_SIZE = 100;
  private final PolygonToPointsConverter converter = new PolygonToPointsConverter(GRID_SIZE);
  private String sourceName;

  private static final String MULTI_POLYGON = "POLYGON((262258 468584,262262 468583,262263 468585,262284 468578,262308 468569,262331 468561,262378 468544,262424 468526,262470 468506,262516 468487,262562 468466,262607 468446,262629 468433,262652 468422,262689 468405,262689 468405,262688 468374,262685 468333,262684 468316,262682 468316,262682 468315,262682 468310,262682 468300,262684 468281,262702 468213,262707 468195,262679 468143,262595 468024,262569 467988,262553 467964,262509 467894,262509 467894,262477 467851,262445 467807,262378 467716,262371 467706,262334 467604,262327 467586,262311 467542,262256 467472,262235 467446,262195 467395,262178 467374,262186 467368,262197 467360,262164 467314,262124 467257,262083 467200,262061 467171,262029 467126,262014 467105,262003 467090,261986 467067,261989 467067,261985 467061,261982 467062,261968 467043,261961 467033,261905 467040,261868 466993,261850 466971,261836 466973,261826 466973,261816 466972,261797 466969,261782 466965,261738 466954,261724 466952,261711 466951,261691 466948,261662 466945,261651 466944,261647 466943,261641 466939,261636 466935,261626 466934,261604 466930,261575 466926,261555 466923,261537 466920,261508 466916,261488 466913,261479 466912,261471 466910,261463 466909,261461 466909,261447 466907,261422 466903,261405 466901,261382 466897,261352 466893,261325 466889,261303 466885,261289 466883,261289 466883,261271 466880,261267 466905,261273 466913,261268 466922,261266 466928,261264 466930,261254 466930,261247 466933,261236 466938,261223 466943,261211 466945,261200 466949,261183 466957,261176 466961,261174 466963,261173 466967,261169 466976,261165 466983,261108 467068,260994 467219,261010 467261,261020 467286,261026 467301,261056 467288,261057 467292,261059 467299,261061 467307,261071 467305,261173 467276,261176 467275,261176 467277,261178 467276,261181 467282,261183 467289,261192 467312,261198 467328,261199 467330,261199 467330,261202 467339,261227 467400,261235 467414,261235 467415,261256 467467,261302 467580,261333 467658,261352 467707,261354 467710,261369 467747,261370 467752,261425 467728,261438 467746,261442 467744,261446 467740,261457 467756,261454 467761,261445 467768,261468 467809,261406 467841,261428 467897,261429 467898,261436 467902,261589 468001,261600 468008,261648 468047,261725 468111,261725 468111,261742 468124,261791 468163,261810 468178,261818 468184,261826 468191,261854 468209,261862 468214,261872 468220,261890 468227,261895 468229,261902 468229,261911 468235,261912 468235,261929 468251,261944 468267,262009 468339,262012 468342,262016 468346,262039 468370,262084 468415,262109 468445,262137 468469,262155 468487,262156 468488,262201 468533,262217 468549,262217 468549,262222 468554,262224 468555,262250 468582,262251 468582,262254 468585,262258 468584))";

  @Test
  void testConvertLargePolygonToPoints() throws AeriusException, SQLException {
    final Polygon polygon = (Polygon) GeometryUtil.getAeriusGeometry(GeometryUtil.getGeometry(MULTI_POLYGON));
    final List<WeightedPoint> convertedPoints = converter.convert(sourceName, polygon);

    assertEquals(179, convertedPoints.size(), "Expected number of points");
    assertEquals(144.2326501868, convertedPoints.stream().mapToDouble(WeightedPoint::getFactor).sum(), 0.00001,
        "Summed weight factor should match expected value");
  }

  @Test
  void testConvertPolygonToPoints() throws AeriusException {
    //create polygon with points (0,0), (0,4000), (4000,4000), (4000,0), (0,0)
    final int side = 4000;
    final String polygonWKT = "POLYGON((0 0,0 " + side + "," + side + " " + side + "," + side + " 0,0 0))";
    final Polygon polygon = (Polygon) GeometryUtil.getAeriusGeometry(GeometryUtil.getGeometry(polygonWKT));
    List<WeightedPoint> convertedPoints = new ArrayList<>();
    convertedPoints = converter.convert(sourceName, polygon);
    //known case for squares where each side is divisable by the max square side size.
    //function starts with creating squares in the middle, radiating outwards.
    //each point inside the surface (in this case the square) will be returned.
    //points on the border will have a weight factor of 0.5 and those exactly on the corners 0.25.
    assertEquals(Math.pow(side / GRID_SIZE + 1, 2),
        convertedPoints.size(),
        0.5, "Number of segments");
    // The coordinates of the point of the b
    final double offset = GRID_SIZE / 4;
    for (final WeightedPoint point : convertedPoints) {
      assertTrue(point.getX() >= 0 && point.getX() <= side, "Point x coord inside square");
      assertTrue(point.getY() >= 0 && point.getY() <= side, "Point y coord inside square");
      //points on border get weight factor 0.5, others get 1 (are fully inside).
      if ((point.getX() == offset || point.getX() == (side - offset))
          && (point.getY() == offset || point.getY() == (side - offset))) {
        assertEquals(0.25, point.getFactor(), 0.001, "Weight factor points on the corners");
      } else if (point.getX() == offset || point.getX() == (side - offset)
          || point.getY() == offset || point.getY() == (side - offset)) {
        assertEquals(0.5, point.getFactor(), 0.001, "Weight factor points on the border");
      } else {
        assertEquals(1, point.getFactor(), 0.001, "Weight factor points within the polygon");
      }
    }
  }

  @Test
  void testConvertPolygonToPointsSmall() throws AeriusException {
    //create small polygon with points (0,0), (0,20), (20,20), (20,0), (0,0)
    final int side = 20;
    final String polygonWKT = "POLYGON((0 0,0 " + side + "," + side + " " + side + "," + side + " 0,0 0))";
    final Polygon polygon = (Polygon) GeometryUtil.getAeriusGeometry(GeometryUtil.getGeometry(polygonWKT));
    List<WeightedPoint> convertedPoints = new ArrayList<>();

    convertedPoints = converter.convert(sourceName, polygon);
    //if surface for the polygon is below the max surface, it should just return one point with full weight
    //it should be centered on polygon as well.
    assertEquals(1, convertedPoints.size(), "Number of segments");
    final WeightedPoint point = convertedPoints.get(0);
    assertTrue(point.getFactor() > 0.0, "Weight factor");
    assertTrue(point.getX() >= 0 && point.getX() <= side, "Point x coord inside square");
    assertTrue(point.getY() >= 0 && point.getY() <= side, "Point y coord inside square");
    assertEquals(side / 2.0, point.getX(), 0.001, "Point x coord center of square");
    assertEquals(side / 2.0, point.getY(), 0.001, "Point Y coord center of square");
  }

  @Test
  void testConvertPolygonToPointsSmallWidth() throws AeriusException {
    //create diagonal small width long polygon with points (0,0), (0,20), (4000,4020), (4000,4000), (0,0)
    final int side = 20;
    final String polygonWKT = "POLYGON((0 0,0 " + side + ",4000 " + (4000 + side) + ",4000 4000,0 0))";
    final Polygon polygon = (Polygon) GeometryUtil.getAeriusGeometry(GeometryUtil.getGeometry(polygonWKT));
    List<WeightedPoint> convertedPoints = new ArrayList<>();

    convertedPoints = converter.convert(sourceName, polygon);
    //if surface for the polygon is below the max surface, it should just return one point with full weight
    //it should be centered on polygon as well.
    assertTrue(convertedPoints.size() > 1, "Number of segments should be more than 1 (unsure how many exactly)");
    //can we tell anything about where the points should generally be?
  }

  /**
   * Tests self-intersecting polygons; They are not supported (yet) thus it should throw an exception.
   */
  @Test
  void testPolygotnToPointsBowTie() throws AeriusException {
    //create Bow-Tie polygon with points (0,0), (4000,4000), (4000,0), (0,4000), (0,0)
    final int side = 4000;
    final String polygonWKT = "POLYGON((0 0," + side + " " + side + "," + side + " 0,0 " + side + ",0 0))";
    final Polygon polygon = (Polygon) GeometryUtil.getAeriusGeometry(GeometryUtil.getGeometry(polygonWKT));

    final AeriusException exception = assertThrows(AeriusException.class, () -> converter.convert(sourceName, polygon),
        "Should throw exception on self intersect.");
    assertEquals(ImaerExceptionReason.GML_GEOMETRY_INTERSECTS, exception.getReason(), "Should throw intersect reason");
  }
}
