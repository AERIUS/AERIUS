/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link HexagonEquilateralTrianglesMap}.
 */
class HexagonEquilateralTrianglesMapTest {

  private static final List<Double> DISTANCES = List.of(2000D, 1000D, 500D, 200D);
  private static final int RADIUS = 100;
  private static final int LEVEL_OFFSET = 1;
  private static HexagonEquilateralTrianglesMap<AeriusPoint> MAP = HexagonEquilateralTrianglesMap.build(RADIUS, DISTANCES, LEVEL_OFFSET,
      (p, l) -> new SubPoint(p, l));

  @ParameterizedTest
  @MethodSource("distanceToCheck")
  void testDistances(final double distanceToCheck, final int expectedLevel) {
    assertEquals(expectedLevel, MAP.findLevel(distanceToCheck), "Not the expected level found");
  }

  private static Object[][] distanceToCheck() {
    return new Object[][] {
      {4000D, 0},
      {1500D, 1},
      {750D, 2},
      {350D, 3},
      {50D, 4},
    };
  }

  @ParameterizedTest
  @MethodSource("data")
  void testTriangles(final int level, final int numberOfPoints) throws AeriusException {
    final List<AeriusPoint> points = MAP.computePoints(new AeriusPoint(0), level, null);

    assertEquals(numberOfPoints, points.size(), "Expected number of points didn't match.");
    // assert if points not exceed boundery of hexagon
    assertFalse(points.stream().anyMatch(p -> Math.abs(p.getX()) > RADIUS), "X coordinate should not exceed radius");
    final double height = Math.sqrt(RADIUS * RADIUS - (RADIUS / 2 + RADIUS / 2));
    assertFalse(points.stream().anyMatch(p -> Math.abs(p.getY()) > height), "Y coordinate should not exceed height");
    // assert if points not map to original 0,0 coordinate
    assertTrue(points.stream().allMatch(p -> Math.abs(p.getY()) > 0), "All Y coordinate should not match 0.0");
    // assert the sum of all coordinates is 0. This works because the upper/left part is a mirror of the lower/right part of the hexagon.
    assertEquals(0.0, points.stream().mapToDouble(p -> p.getX() + p.getY()).sum(), 0.001, "Sum of coordinates should be zero");
  }

  @Test
  void testTrianglesFilter() throws AeriusException {
    final List<AeriusPoint> points = MAP.computePoints(new AeriusPoint(0), 4, p -> false);
    assertTrue(points.isEmpty(), "Filter should have filtered out all points");
  }

  private static List<Arguments> data() {
    return List.of(
        //           level 1, number of points
        Arguments.of(1, 6),
        Arguments.of(2, 24),
        Arguments.of(3, 96),
        Arguments.of(4, 384));
  }

  @ParameterizedTest
  @MethodSource("levels")
  void testTrianglesPoints(final int level, final double[][] expectedPoints) throws AeriusException {
    final List<AeriusPoint> points = MAP.computePoints(new AeriusPoint(1000, 2000), level, null);
    final List<double[]> actual = points.stream().map(p -> new double[] {p.getId(), p.getX(), p.getY()}).collect(Collectors.toList());

    for (int i = 0; i < actual.size(); i++) {
      assertArrayEquals(expectedPoints[i], actual.get(i), 0.01, "Points at level '" + level + "' don't match for point at index " + i);
    }
  }

  @ParameterizedTest
  @MethodSource("triangleCoordinates")
  void testTrianglesCoordinates(final int level, final double[][] expectedCoordinates) throws AeriusException {
    final List<AeriusPoint> points = MAP.computePoints(new AeriusPoint(1000, 2000), level, null);

    for (int i = 0; i < points.size(); i++) {
      final double[] actual = Stream.of(((SubPoint) points.get(i)).getArea().getCoordinates()[0])
          .flatMap(c -> Stream.of(c[0], c[1])).mapToDouble(d -> d).toArray();

      assertArrayEquals(expectedCoordinates[i], actual, 0.01,
          "Triangle geometry at level '" + level + "' doesn't match expected geometry for index " + i);
    }
  }

  //@formatter:off
  private static List<Arguments> levels() {
    return List.of(
        // level 1
        Arguments.of(
            1,
            new double[][] {
              {1, 950, 2028.8675},
              {2, 1000, 2057.735},
              {3, 1050, 2028.8675},
              {4, 950, 1971.1325},
              {5, 1000, 1942.265},
              {6, 1050, 1971.1325},
            }),
        // level 2
        Arguments.of(
            2,
            new double[][] {
              // upper part of the hexagon
              {7,   950, 2057.735},
              {8,   925, 2014.4338},
              {9,   950, 2028.8675},
              {10,  975, 2014.4338},
              {11,  975, 2072.1688},
              {12, 1000, 2057.735},
              {13, 1025, 2072.1688},
              {14, 1000, 2028.8675},
              {15, 1050, 2057.735},
              {16, 1025, 2014.4338},
              {17, 1050, 2028.8675},
              {18, 1075, 2014.4338},
              // lower part of the hexagon
              {19,  925, 1985.5662},
              {20,  950, 1971.1325},
              {21,  975, 1985.5662},
              {22,  950, 1942.265},
              {23, 1000, 1971.1325},
              {24,  975, 1927.8312},
              {25, 1000, 1942.265},
              {26, 1025, 1927.8312},
              {27, 1025, 1985.5662},
              {28, 1050, 1971.1325},
              {29, 1075, 1985.5662},
              {30, 1050, 1942.265},
            })
        );
  }

  private static List<Arguments> triangleCoordinates() {
    return List.of(
        // level 1
        Arguments.of(
            1,
            new double[][] {
              {950.0, 2086.6025, 900.0, 2000.0, 1000.0, 2000.0, 950.0, 2086.6025},
              {1000.0, 2000.0, 950.0, 2086.6025, 1050.0, 2086.6025, 1000.0, 2000.0},
              {1050.0, 2086.6025, 1000.0, 2000.0, 1100.0, 2000.0, 1050.0, 2086.6025},
              {950.0, 1913.3975, 900.0, 2000.0, 1000.0, 2000.0, 950.0, 1913.3975},
              {1000.0, 2000.0, 950.0, 1913.3975, 1050.0, 1913.3975, 1000.0, 2000.0},
              {1050.0, 1913.3975, 1000.0, 2000.0, 1100.0, 2000.0, 1050.0, 1913.3975},
            }),
        // level 2
        Arguments.of(
            2,
            new double[][] {
              // upper part of the hexagon
              {950.0, 2086.6025, 925.0, 2043.3013, 975.0, 2043.3013, 950.0, 2086.6025},
              {925.0, 2043.3013, 900.0, 2000.0, 950.0, 2000.0, 925.0, 2043.3013},
              {950.0, 2000.0, 925.0, 2043.3013, 975.0, 2043.3013, 950.0, 2000.0},
              {975.0, 2043.3013, 950.0, 2000.0, 1000.0, 2000.0, 975.0, 2043.3013},
              {975.0, 2043.3013, 950.0, 2086.6025, 1000.0, 2086.6025, 975.0, 2043.3013},
              {1000.0, 2086.6025, 975.0, 2043.3013, 1025.0, 2043.3013, 1000.0, 2086.6025},
              {1025.0, 2043.3013, 1000.0, 2086.6025, 1050.0, 2086.6025, 1025.0, 2043.3013},
              {1000.0, 2000.0, 975.0, 2043.3013, 1025.0, 2043.3013, 1000.0, 2000.0},
              {1050.0, 2086.6025, 1025.0, 2043.3013, 1075.0, 2043.3013, 1050.0, 2086.6025},
              {1025.0, 2043.3013, 1000.0, 2000.0, 1050.0, 2000.0, 1025.0, 2043.3013},
              {1050.0, 2000.0, 1025.0, 2043.3013, 1075.0, 2043.3013, 1050.0, 2000.0},
              {1075.0, 2043.3013, 1050.0, 2000.0, 1100.0, 2000.0, 1075.0, 2043.3013},
              // lower part of the hexagon
              {925.0, 1956.6987, 900.0, 2000.0, 950.0, 2000.0, 925.0, 1956.6987},
              {950.0, 2000.0, 925.0, 1956.6987, 975.0, 1956.6987, 950.0, 2000.0},
              {975.0, 1956.6987, 950.0, 2000.0, 1000.0, 2000.0, 975.0, 1956.6987},
              {950.0, 1913.3975, 925.0, 1956.6987, 975.0, 1956.6987, 950.0, 1913.3975},
              {1000.0, 2000.0, 975.0, 1956.6987, 1025.0, 1956.6987, 1000.0, 2000.0},
              {975.0, 1956.6987, 950.0, 1913.3975, 1000.0, 1913.3975, 975.0, 1956.6987},
              {1000.0, 1913.3975, 975.0, 1956.6987, 1025.0, 1956.6987, 1000.0, 1913.3975},
              {1025.0, 1956.6987, 1000.0, 1913.3975, 1050.0, 1913.3975, 1025.0, 1956.6987},
              {1025.0, 1956.6987, 1000.0, 2000.0, 1050.0, 2000.0, 1025.0, 1956.6987},
              {1050.0, 2000.0, 1025.0, 1956.6987, 1075.0, 1956.6987, 1050.0, 2000.0},
              {1075.0, 1956.6987, 1050.0, 2000.0, 1100.0, 2000.0, 1075.0, 1956.6987},
              {1050.0, 1913.3975, 1025.0, 1956.6987, 1075.0, 1956.6987, 1050.0, 1913.3975},
            })
        );
  }
  //@formatter:on

  private static class SubPoint extends AeriusPoint implements IsSubPoint {

    private static final long serialVersionUID = 1L;

    private Polygon triangle;

    public SubPoint(final AeriusPoint point, final int level) {
      super(point.getId(), point.getParentId(), AeriusPointType.SUB_RECEPTOR, point.getX(), point.getY(), point.getHeight());
    }

    @Override
    public Integer getParentId() {
      throw new IllegalArgumentException("Not used in test");
    }

    @Override
    public AeriusPointType getParentPointType() {
      throw new IllegalArgumentException("Not used in test");
    }

    @Override
    public void setParent(final AeriusPoint parentPoint) {
      throw new IllegalArgumentException("Not used in test");
    }

    @Override
    public int getLevel() {
      throw new IllegalArgumentException("Not used in test");
    }

    @Override
    public Polygon getArea() {
      return triangle;
    }

    @Override
    public void setArea(final Polygon geometry) {
      triangle = geometry;
    }
  }
}
