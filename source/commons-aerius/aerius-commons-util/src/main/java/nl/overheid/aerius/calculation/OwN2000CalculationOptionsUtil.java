/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static nl.overheid.aerius.shared.domain.Substance.NH3;
import static nl.overheid.aerius.shared.domain.Substance.NO2;
import static nl.overheid.aerius.shared.domain.Substance.NOX;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NH3_DEPOSITION;
import static nl.overheid.aerius.shared.domain.result.EmissionResultKey.NOX_DEPOSITION;

import java.util.List;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.OwN2000CalculationOptions;
import nl.overheid.aerius.shared.domain.calculation.SubReceptorsMode;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Util class to set the default calculation options for a OwN2000 {@link CalculationMethod#FORMAL_ASSESSMENT} calculation.
 */
public final class OwN2000CalculationOptionsUtil {

  public static final List<Substance> PERMIT_SUBSTANCES = List.of(NH3, NOX, NO2);
  private static final List<EmissionResultKey> PERMIT_EMISSION_RESULTKEYS = List.of(NH3_DEPOSITION, NOX_DEPOSITION);

  private OwN2000CalculationOptionsUtil() {
    // Util class.
  }

  /**
   * Returns a {@link CalculationSetOptions} object configured to do a Wet Natuur Bescherming (OwN2000) calculation.
   * @return CalculationSetOptions object.
   */
  public static CalculationSetOptions createOwN2000CalculationSetOptions() {
    final CalculationSetOptions options = new CalculationSetOptions();
    options.getSubstances().addAll(PERMIT_SUBSTANCES);
    options.getEmissionResultKeys().addAll(PERMIT_EMISSION_RESULTKEYS);
    options.setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
    options.setStacking(true);
    final OwN2000CalculationOptions own2000Options = options.getOwN2000CalculationOptions();
    own2000Options.setUseMaxDistance(true);
    own2000Options.setForceAggregation(false);
    own2000Options.setSubReceptorsMode(SubReceptorsMode.ENABLED_RECEPTORS_ONLY);
    own2000Options.setSubReceptorZoomLevel(1);
    own2000Options.setMeteo(null); // Use default meteo
    own2000Options.setSplitSubReceptorWork(false);

    return options;
  }
}
