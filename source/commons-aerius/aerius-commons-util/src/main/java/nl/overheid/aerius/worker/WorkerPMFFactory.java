/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.postgresql.PGConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.BasePMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.LocaleDBUtils;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Factory to create a Worker PMF object for a specific database (i.e. product type).
 */
public final class WorkerPMFFactory {
  private static final Logger LOG = LoggerFactory.getLogger(WorkerPMFFactory.class);

  private WorkerPMFFactory() {
    // util class
  }

  /**
   * Create a PMF object.
   * @param cfg the database configuration with database connection information
   * @return PMF object
   * @throws AeriusException exception
   */
  public static PMF createPMF(final DatabaseConfiguration cfg) throws AeriusException {
    final String databaseUrl = cfg.getDatabaseUrl();

    if (databaseUrl == null) {
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR, "Missing database url property");
    } else {
      final PMF pmf = new WorkerPMF(databaseUrl, cfg.getDatabaseUsername(), cfg.getDatabasePassword());
      DBMessages.init(pmf);
      validateDatabaseValues(pmf);
      LocaleUtils.setDefaultLocale(LocaleDBUtils.getDefaultLocale(pmf).getLanguage());
      LOG.info("Initialized database configuration");
      return pmf;
    }
  }

  private static void validateDatabaseValues(final PMF pmf) throws AeriusException {
    if (validateConstants(pmf)) {
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR_CONSTANTS_MISSING);
    }
  }

  private static boolean validateConstants(final PMF pmf) {
    boolean failure = false;
    for (final ConstantsEnum constant : ConstantsEnum.values()) {
      if (constant.isRequired()) {
        try {
          @SuppressWarnings("unused")
          final String repo = ConstantRepository.getString(pmf, constant);
        } catch (final IllegalArgumentException e) {
          failure = true;
          LOG.error("No constants configuration key {}", constant.toString());
        }
      }
    }
    return failure;
  }

  /**
   * Persistence Manager Factory manages the database connection.
   */
  private static final class WorkerPMF extends BasePMF {

    private final String jdbcURL;
    private final String dbUsername;
    private final String dbPassword;

    public WorkerPMF(final String jdbcURL, final String dbUsername, final String dbPassword) {
      this.jdbcURL = jdbcURL;
      this.dbUsername = dbUsername;
      this.dbPassword = dbPassword;
    }

    /**
     * Returns a new connection to the AeriusDB database.
     * Ensure the jdbcURL, dbUsername and dbPassword are set.
     *
     * @return a database connection
     * @throws SQLException on SQL errors
     */
    @Override
    public Connection getConnection() throws SQLException {
      final Connection connection = DriverManager.getConnection(
          jdbcURL,
          dbUsername,
          dbPassword);

      if (connection instanceof PGConnection) {
        ((PGConnection) connection).addDataType("geometry", org.postgis.PGgeometry.class);
      }

      return connection;
    }

    @Override
    public String toString() {
      return "WorkerPMF [jdbcURL=" + jdbcURL + "]";
    }
  }
}
