/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

/**
 * Exit codes returned by worker executable.
 */
public enum WorkerExitCode {
  /**
   * Worker completed successfully.
   */
  SUCCESS(0),
  /**
   * Worker exited with exception.
   */
  FAILURE(1),
  /**
   * The configuration has validation errors.
   */
  VALIDATION_ERRORS(2),
  /**
   * No processes configured
   */
  NOTHING_TO_DO(3),
  /**
   * Worker encountered an OutOfMemory error.
   */
  FAILURE_OOM(4);

  private final int exitCode;

  WorkerExitCode(final int exitCode) {
    this.exitCode = exitCode;
  }

  public int getExitCode() {
    return exitCode;
  }
}
