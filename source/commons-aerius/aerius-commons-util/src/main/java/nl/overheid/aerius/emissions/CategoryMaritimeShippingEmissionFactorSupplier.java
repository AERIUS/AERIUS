/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.emissions.MaritimeRouteKey;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.ShippingMovementType;
import nl.overheid.aerius.shared.emissions.MaritimeShippingEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.shipping.MaritimeShippingRouteEmissionPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

class CategoryMaritimeShippingEmissionFactorSupplier implements MaritimeShippingEmissionFactorSupplier {

  private static final Logger LOG = LoggerFactory.getLogger(CategoryMaritimeShippingEmissionFactorSupplier.class);

  private final Map<MaritimeRouteKey, Map<Substance, Double>> cacheRouteEmissionFactors = new HashMap<>();
  private final Map<String, Map<Substance, Double>> cacheDockedEmissionFactors = new HashMap<>();

  private final SectorCategories categories;
  private final PMF pmf;
  private final int year;

  CategoryMaritimeShippingEmissionFactorSupplier(final SectorCategories categories, final PMF pmf, final int year) {
    this.categories = categories;
    this.pmf = pmf;
    this.year = year;
  }

  @Override
  public List<MaritimeShippingRouteEmissionPoint> determineMaritimeRoutePoints(final Geometry geometry) throws AeriusException {
    final List<MaritimeShippingRouteEmissionPoint> points = new ArrayList<>();
    try (final Connection con = pmf.getConnection()) {
      final double maxSegmentSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_LINE_TO_POINTS_SEGMENT_SIZE, Double.class);
      points.addAll(ShippingRepository.getMaritimeShippingRoutePoints(con, geometry, maxSegmentSize));
    } catch (final SQLException e) {
      LOG.error("Error determining maritime route points", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return points;
  }

  @Override
  public List<MaritimeShippingRouteEmissionPoint> determineMaritimeMooringInlandRoutePoints(final Geometry geometry, final String shipCode,
      final boolean mooringOnA, final boolean mooringOnB) throws AeriusException {
    final List<MaritimeShippingRouteEmissionPoint> points = new ArrayList<>();
    try (final Connection con = pmf.getConnection()) {
      final double maxSegmentSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_LINE_TO_POINTS_SEGMENT_SIZE, Double.class);
      points.addAll(ShippingRepository.getMaritimeMooringShippingInlandRoute(con, geometry, shipCode, maxSegmentSize, mooringOnA, mooringOnB));
    } catch (final SQLException e) {
      LOG.error("Error determining maritime mooring route points", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return points;
  }

  @Override
  public List<MaritimeShippingRouteEmissionPoint> determineMaritimeMooringInlandRoutePointsForGrossTonnage(final Geometry geometry,
      final int grossTonnage, final boolean mooringOnA, final boolean mooringOnB) throws AeriusException {
    final List<MaritimeShippingRouteEmissionPoint> points = new ArrayList<>();
    try (final Connection con = pmf.getConnection()) {
      final double maxSegmentSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_LINE_TO_POINTS_SEGMENT_SIZE, Double.class);
      points.addAll(ShippingRepository.getMaritimeMooringShippingInlandRouteForGrossTonnage(con, geometry, grossTonnage, maxSegmentSize, mooringOnA,
          mooringOnB));
    } catch (final SQLException e) {
      LOG.error("Error determining maritime mooring route points based on gross tonnage", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return points;
  }

  @Override
  public Map<Substance, Double> getMaritimeShippingRouteEmissionFactors(final ShippingMovementType movementType, final String shipCode)
      throws AeriusException {
    final MaritimeRouteKey routeKey = new MaritimeRouteKey(movementType, shipCode);
    if (!cacheRouteEmissionFactors.containsKey(routeKey)) {
      cacheRouteEmissionFactors.put(routeKey, getRouteEmissionFactorsFromDatabase(routeKey));
    }
    return cacheRouteEmissionFactors.get(routeKey);
  }

  private Map<Substance, Double> getRouteEmissionFactorsFromDatabase(final MaritimeRouteKey routeKey) throws AeriusException {
    final MaritimeShippingCategory ship = shipCategory(routeKey.getShipCode());
    try (final Connection con = pmf.getConnection()) {
      return ShippingCategoryRepository.findMaritimeCategoryEmissionFactors(con, ship, routeKey.getMovementType(), year);
    } catch (final SQLException e) {
      LOG.error("Error determining maritime route emission factors", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  @Override
  public Map<Substance, Double> getMaritimeShippingDockedEmissionFactors(final String shipCode) throws AeriusException {
    if (!cacheDockedEmissionFactors.containsKey(shipCode)) {
      cacheDockedEmissionFactors.put(shipCode, getDockedEmissionFactorsFromDatabase(shipCode));
    }
    return cacheDockedEmissionFactors.get(shipCode);
  }

  private Map<Substance, Double> getDockedEmissionFactorsFromDatabase(final String shipCode) throws AeriusException {
    final MaritimeShippingCategory ship = shipCategory(shipCode);
    try (final Connection con = pmf.getConnection()) {
      return ShippingCategoryRepository.findMaritimeCategoryDockedEmissionFactors(con, ship, year);
    } catch (final SQLException e) {
      LOG.error("Error determining maritime docked emission factors", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private MaritimeShippingCategory shipCategory(final String shipCode) {
    return categories.determineMaritimeShippingCategoryByCode(shipCode);
  }
}
