/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Class to help with setting state for jobs.
 */
public class JobStateHelper implements JobStateHelperAdapter {

  private static final Logger LOGGER = LoggerFactory.getLogger(JobStateHelper.class);

  private final PMF pmf;

  public JobStateHelper(final PMF pmf) {
    this.pmf = pmf;
  }

  @Override
  public void setJobStatus(final String jobKey, final JobState jobState) {
    try (final Connection con = pmf.getConnection()) {
      JobRepository.updateJobStatus(con, jobKey, jobState);
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying to set job to running {}", jobKey, e);
    }
  }

  public JobState getJobStatus(final String jobKey) {
    try (final Connection con = pmf.getConnection()) {
      return JobRepository.getJobStatus(con, jobKey);
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying to get job status {}", jobKey, e);
      return JobState.UNDEFINED;
    }
  }

  public void setErrorStateAndMessageJob(final String jobKey, final Exception originException) {
    setErrorStateAndMessageJob(jobKey, LocaleUtils.getDefaultLocale(), originException);
  }

  @Override
  public void setErrorStateAndMessageJob(final String jobKey, final Locale locale, final Exception originException) {
    final String message = getErrorMessage(originException, locale);

    try (final Connection con = pmf.getConnection()) {
      JobRepository.setErrorMessage(con, jobKey, message);
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying to set the error message {} for the job {}", jobKey, message, e);
    }
  }

  @Override
  public void failIfJobCancelledByUser(final JobIdentifier jobIdentifier) throws AeriusException {
    WorkerUtil.failIfJobCancelledByUser(pmf, jobIdentifier.getJobKey());
  }

  public static boolean isNotCancelledJob(final Exception e) {
    return !(e instanceof final AeriusException ae) || ae.getReason() != AeriusExceptionReason.CONNECT_JOB_CANCELLED;
  }

  private static String getErrorMessage(final Exception e, final Locale locale) {
    return new AeriusExceptionMessages(locale).getString(e);
  }
}
