/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Pattern;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;

/**
 * Utility class to create filenames for exported files.
 */
public final class FilenameUtil {

  static final int MAX_OPTIONAL_PART_LENGTH = 32;

  private static final String AERIUS_PREFIX = "AERIUS";
  private static final String NCA_PREFIX = "UKAPAS";
  private static final String ARCHIVE_FILE_PART = "ARCHIVE";
  // Allow letters, digits, dots, hyphens, and underscores
  private static final Pattern UNSAFE_FILENAME_CHARACTERS_REGEX = Pattern.compile("[^\\w._\\-]");
  private static final String DATEFORMAT_FILENAME = "yyyyMMddHHmmss";

  private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern(DATEFORMAT_FILENAME);
  private static final String FILENAME_SEPARATOR = "_";

  private FilenameUtil() {
    // Util class
  }

  /**
   * Constructs a filename from the {@link CalculationInputData} for the given extension.
   * Adds scenario reference, input name, and date to the filename.
   * Filename is <code>&lt;theme prefix>_&lt;date>&lt;reference>_&lt;job name>.&lt;extension></code>.
   *
   * @param inputData calculation input data
   * @param fileformat file extension to use
   * @param reference reference
   * @return Name of the file for a job
   */
  public static String jobFilename(final CalculationInputData inputData, final FileFormat fileformat, final String reference) {
    return getFilename(inputData.getScenario().getTheme(), fileformat.getExtension(), inputData.getCreationDate(), reference, inputData.getName());
  }

  /**
   * Constructs a filename from scenario situation object for GML files.
   * Adds situation reference, situation name, and date to the filename.
   * Filename is <code>&lt;theme prefix>_&lt;date>_&lt;reference>_&lt;situation name>.gml</code>.
   *
   * @param theme theme the filename is for
   * @param situation situation to get data from
   * @param date date
   * @param optionalName Optional part appended to the filename
   * @return Name of the gml file
   */
  public static String situationFilename(final Theme theme, final ScenarioSituation situation, final Date date, final String optionalName) {
    return getFilename(theme, FileFormat.GML.getExtension(), date, situation.getReference(), situation.getName(), optionalName);
  }

  /**
   * Constructs a filename for the archive contributions GML file.
   * Adds date to the filename.
   * Filename is <code>&lt;theme prefix>_&lt;date>_ARCHIVE.gml</code>.
   *
   * @param theme theme the filename is for
   * @param date date
   * @return Name of the gml file
   */
  public static String archiveContributionsFilename(final Theme theme, final Date date) {
    return getFilename(theme, FileFormat.GML.getExtension(), date, ARCHIVE_FILE_PART);
  }

  /**
   * Constructs a filename including path to a zip file to store model input data.
   * Filename is <code>&lt;zipPrefix>_&lt;time now>_&lt;random string based on uuid, 8 chars long>.zip</code>
   *
   * @param directory directory for the file
   * @param zipPrefix prefix to use for the filename
   * @return Name of zip file
   */
  public static File modelZipFilename(final File directory, final String zipPrefix) {
    final String randomPrefix = UUID.randomUUID().toString().replace("-", "").substring(0, 8);

    return new File(directory,
        FileUtil.getFilename(zipPrefix, FileFormat.ZIP.getExtension(), randomPrefix, Calendar.getInstance().getTime()));
  }

  /**
   * Get an (AERIUS) filename for a given prefix string.
   * Format: prefix_datestring[optionalName1][_optionalName2][.]extension
   *
   * @param prefix Prefix to use in the filename.
   * @param extension Extension to use for the filename. Will be prefixed by a . if not in the string.
   * @param optionalDate The optional date to use for the datestring. If null, current time will be used.
   * @param optionalNames Optional parts appended to the filename.
   * @return The filename that can be used.
   */
  public static String getFilename(final String prefix, final String extension, final Date optionalDate, final String... optionalNames) {
    if (prefix == null || extension == null) {
      throw new IllegalArgumentException("Prefix or extension not allowed to be null. Prefix: " + prefix + ", extension: " + extension);
    }
    return getActualFilename(prefix, extension, optionalDate, optionalNames);
  }

  /**
   * Get an (AERIUS) filename with the prefix based on a theme.
   * Format: prefix_datestring[optionalName1][_optionalName2][.]extension
   *
   * @param theme Theme The current theme to use for the prefix of the filename
   * @param extension Extension to use for the filename. Will be prefixed by a . if not in the string
   * @param optionalDate The optional date to use for the datestring. If null, current time will be used
   * @param optionalNames Optional parts appended to the filename
   * @return The filename that can be used
   */
  public static String getFilename(final Theme theme, final String extension, final Date optionalDate, final String... optionalNames) {
    if (theme == null || extension == null) {
      throw new IllegalArgumentException("Theme or extension not allowed to be null. Theme:" + theme + ", extension: " + extension);
    }
    final String themePrefix = switch (theme) {
      case OWN2000, RBL -> AERIUS_PREFIX;
      case NCA -> NCA_PREFIX;
      default -> throw new IllegalArgumentException("Unknown or null theme passed. Theme: " + theme.toString());
    };

    return getActualFilename(themePrefix, extension, optionalDate, optionalNames);
  }

  private static String getActualFilename(final String prefix, final String extension, final Date optionalDate, final String... optionalNames) {
    final StringBuilder fileNameBuilder = new StringBuilder();
    fileNameBuilder.append(prefix);
    fileNameBuilder.append(FILENAME_SEPARATOR);

    fileNameBuilder.append(optionalDate == null
        ? LocalDateTime.now().format(DATE_TIME_FORMAT)
        : DATE_TIME_FORMAT.format(optionalDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()));
    for (final String optionalName : optionalNames) {
      appendOptionalName(optionalName, fileNameBuilder);
    }
    if (!extension.isEmpty()) {
      fileNameBuilder.append(extension.charAt(0) == '.' ? extension : ('.' + extension));
    }
    return fileNameBuilder.toString();
  }

  private static void appendOptionalName(final String optionalName, final StringBuilder fileNameBuilder) {
    if (optionalName != null && !optionalName.isEmpty()) {
      fileNameBuilder.append(FILENAME_SEPARATOR);
      fileNameBuilder.append(getSafeOptionalName(optionalName));
    }
  }

  private static String getSafeOptionalName(final String optionalName) {
    //optional name can be user-input, so assure it's safe and not too long
    final String safeOptionalName = getSafeFilename(optionalName);
    return safeOptionalName != null && safeOptionalName.length() > MAX_OPTIONAL_PART_LENGTH
        ? safeOptionalName.substring(0, MAX_OPTIONAL_PART_LENGTH)
        : safeOptionalName;
  }

  /**
   * Strips possibly invalid characters from the filename to ensure a valid filename.
   *
   * @param name The possibly invalid filename
   * @return Valid filename
   */
  public static String getSafeFilename(final String name) {
    return name == null ? null : UNSAFE_FILENAME_CHARACTERS_REGEX.matcher(name).replaceAll("");
  }

}
