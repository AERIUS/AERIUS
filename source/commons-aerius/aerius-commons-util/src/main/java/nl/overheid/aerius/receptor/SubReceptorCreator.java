/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import java.util.List;

import nl.overheid.aerius.function.AeriusFunction;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Abstract class to create subreceptors from points;
 *
 * @param <R> sub receptor type
 */
public abstract class SubReceptorCreator<P extends AeriusPoint & IsSubPoint> {

  private final HexagonEquilateralTrianglesMap<P> hetMap;
  private final AeriusPointType aeriusPointType;

  /**
   * Constructor
   *
   * @param aeriusPointType the point type of the subreceptor
   * @param hexagonRadius radius of the zoom level 1 hexagon
   * @param distance Distances to use to determine if subreceptor calculation should be used
   * @param levelOffset The id of the first level in the list of distances
   */
  protected SubReceptorCreator(final AeriusPointType aeriusPointType, final double hexagonRadius, final List<Double> distance,
      final int levelOffset) {
    this.aeriusPointType = aeriusPointType;
    hetMap = HexagonEquilateralTrianglesMap.build(hexagonRadius, distance, levelOffset, this::createSubReceptorPoint);
  }

  /**
   * {@link HexagonEquilateralTrianglesMap#findLevel(double)}
   */
  public int findLevel(final double distanceToCheck) {
    return hetMap.findLevel(distanceToCheck);
  }

  /**
   * {@link HexagonEquilateralTrianglesMap#computePoints(nl.overheid.aerius.shared.domain.v2.geojson.Point, int, AeriusFunction)}
   */
  public List<P> computePoints(final P original, final int level, final AeriusFunction<P, Boolean> filter) throws AeriusException {
    return hetMap.computePoints(original, level, filter);
  }

  /**
   * Copies the given point to a sub receptor class instance.
   *
   * @param aeriusPoint point to copy
   * @param pointType point type to set on the new sub receptor instance
   * @param subLevel sub level of the new sub receptor instance
   * @return copy of the original point in a sub receptor instance
   */
  public abstract P copyToSubReceptorPoint(AeriusPoint aeriusPoint, AeriusPointType pointType, int subLevel);

  /**
   * Creates a new sub receptor class instance from the given point and sets the sub receptor type.
   * It gives it a new id and sets the receptor id to the id of the given point.
   *
   * @param point point to copy
   * @param level level of the new sub receptor instance
   * @return new subreceptor instance
   */
  public P createSubReceptorPoint(final AeriusPoint point, final int level) {
    final P subReceptor = copyToSubReceptorPoint(point, aeriusPointType, level);

    subReceptor.setParent(point);
    subReceptor.setId(0);

    return subReceptor;
  }
}
