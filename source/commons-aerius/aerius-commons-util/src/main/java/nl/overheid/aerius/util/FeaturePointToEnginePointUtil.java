/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.OPSCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.theme.nca.ADMSCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.NcaPointHeightSupplier;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.NcaCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;

/**
 * Utility class to help with converting from CalculationPointFeature (json serializable, new domain objects)
 * to AeriusPoint (points used in EngineInputData, old domain objects).
 *
 * Might be temporary, but a lot depends on the AeriusPoint class, hence it's easier to do this conversion before calculating.
 */
public final class FeaturePointToEnginePointUtil {

  private FeaturePointToEnginePointUtil() {
    // util class
  }

  /**
   * Convert a feature to a {@link AeriusPoint}.
   *
   * @param feature feature to convert
   * @return converted to AeriusPoint
   */
  public static List<AeriusPoint> convert(final List<CalculationPointFeature> features) {
    return convert(features, FeaturePointToEnginePointUtil::determineType);
  }

  /**
   * Convert a feature to a {@link AeriusPoint}. The toTypeFunc creates the specific instance of the AeriusPoint.
   * This method itself sets all the values on the AeriusPoint instance.
   *
   * @param feature feature to convert
   * @param toTypeFunc function to create the specific instance of AeriusPoint from the feature
   * @return converted to AeriusPoint
   */
  public static List<AeriusPoint> convert(final List<CalculationPointFeature> features, final Function<CalculationPoint, AeriusPoint> toTypeFunc) {
    final List<AeriusPoint> points = features.stream().map(p -> FeaturePointToEnginePointUtil.convert(p, toTypeFunc)).toList();

    if (points.stream().allMatch(point -> point.getId() == 0)) {
      int index = 0;
      for (final AeriusPoint point : points) {
        point.setId(index++);
      }
    }
    return points;
  }

  private static AeriusPoint convert(final CalculationPointFeature feature, final Function<CalculationPoint, AeriusPoint> toTypeFunc) {
    final Point point = feature.getGeometry();
    final CalculationPoint calculationPoint = feature.getProperties();
    final AeriusPoint returnPoint = toTypeFunc.apply(calculationPoint);

    returnPoint.setId(calculationPoint.getId());
    if (calculationPoint instanceof final SubPoint sp) {
      returnPoint.setParentId(sp.getReceptorId());
    }
    returnPoint.setX(point.getX());
    returnPoint.setY(point.getY());

    // At the moment these points are always custom calculation points, so set appropriate point type
    returnPoint.setPointType(AeriusPointType.POINT);
    returnPoint.setGmlId(calculationPoint.getGmlId());
    returnPoint.setLabel(calculationPoint.getLabel());
    if (calculationPoint instanceof final CustomCalculationPoint csp && returnPoint.getHeight() == null) {
      returnPoint.setHeight(csp.getHeight());
    }
    return returnPoint;
  }

  private static AeriusPoint determineType(final CalculationPoint calculationPoint) {
    final AeriusPoint returnPoint;

    if (calculationPoint instanceof final OPSCustomCalculationPoint opsPoint) {
      returnPoint = toReceptorPoint(opsPoint);
    } else if (calculationPoint instanceof final NcaCustomCalculationPoint ncaPoint) {
      returnPoint = toNCAPoint(ncaPoint);
    } else if (calculationPoint instanceof final CIMLKCalculationPoint cimlkPoint) {
      returnPoint = toCIMLKPoint(cimlkPoint);
    } else {
      returnPoint = new AeriusPoint();
    }
    return returnPoint;
  }

  private static AeriusPoint toCIMLKPoint(final CIMLKCalculationPoint point) {
    final nl.overheid.aerius.shared.domain.theme.cimlk.CIMLKCalculationPoint cimlkCalculationPoint =
        new nl.overheid.aerius.shared.domain.theme.cimlk.CIMLKCalculationPoint();

    cimlkCalculationPoint.setMonitorSubstance(point.getMonitorSubstance());
    cimlkCalculationPoint.setRejectionGrounds(point.getRejectionGrounds());
    return cimlkCalculationPoint;
  }

  private static AeriusPoint toReceptorPoint(final OPSCustomCalculationPoint point) {
    final OPSReceptor receptor = new OPSReceptor();

    receptor.setTerrainProperties(point.getTerrainProperties() == null ? null : point.getTerrainProperties().copy());
    return receptor;
  }

  public static AeriusPoint toNCAPoint(final CustomCalculationPoint point) {
    final ADMSCalculationPoint admsPoint = new ADMSCalculationPoint();

    if (point instanceof final NcaCustomCalculationPoint ncaPoint) {
      admsPoint.setRoadLocalFNO2(Optional.ofNullable((ncaPoint).getRoadLocalFractionNO2()).orElse(0.0));
    }
    admsPoint.setHeight(NcaPointHeightSupplier.getHeight(point));
    return admsPoint;
  }
}
