/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service.export;

import java.io.IOException;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.taskmanager.client.WorkerType;

public final class ExportTaskClient {

  private ExportTaskClient() {
    // Util class
  }

  /**
   * @param client The client to use to send the task.
   * @param workerType the worker type to use for the queue.
   * @param queueName name of the queue to place the work on.
   * @param properties The properties containing extra data like year and emailAddress.
   * @param jobKey The key of the job to use.
   * @return a random unique task ID.
   * @throws IOException In case putting task on queue failed.
   */
  public static String startExport(final TaskManagerClientSender client, final WorkerType workerType, final String queueName,
      final ExportProperties properties, final String jobKey) throws IOException {
    final CalculationInputData inputData = new CalculationInputData();
    setScenarioData(queueName, properties, inputData);
    client.sendTask(inputData, jobKey, jobKey, null, workerType.type(), queueName);
    return jobKey;
  }

  private static void setScenarioData(final String queueName, final ExportProperties properties, final CalculationInputData inputData) {
    inputData.setExportType(properties.getExportType());
    inputData.setAppendices(properties.getAppendices());
    inputData.setName(properties.getName());
    inputData.setEmailAddress(properties.getEmailAddress());
    inputData.setLocale(properties.getLocale());
    inputData.setExpire(properties.getExpire());
    inputData.setQueueName(queueName);
    inputData.setAdditionalOptions(properties.getAdditionalOptions());
    inputData.setScenarioResults(properties.getScenarioResults());
  }
}
