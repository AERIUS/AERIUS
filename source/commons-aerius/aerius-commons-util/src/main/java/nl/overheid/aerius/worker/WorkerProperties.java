/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.util.Optional;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.util.EnvUtils;

/**
 * Contains the properties that can be used by different WorkerConfinguration classes, so they don't have to keep track of it.
 * It's also possible to configure a property as environment variable. If the properties is prefixed with 'aerius.' and it exists and environment
 * variable the value of the environment variable overrides the property in the file. In such a case it's also possible to ommit the property in the
 * configuration file.
 */
public class WorkerProperties {

  public static final int AUTO_PROCESSES = -1;

  protected static final String PROCESSES = "processes";

  private static final String AUTO = "auto";
  private static final Logger LOG = LoggerFactory.getLogger(WorkerProperties.class);

  private final Properties properties;
  private final String keyPrefix;

  /**
   * Save the properties. The keyPrefix is prepended to every getProperty call.
   * @param properties Properties containing the predefined properties.
   * @param keyPrefix property prefix
   */
  public WorkerProperties(final Properties properties, final String keyPrefix) {
    this.properties = properties;
    this.keyPrefix = keyPrefix == null || keyPrefix.isEmpty() ? "" : (keyPrefix + '.');
  }

  /**
   * @return Returns the number of worker processes that should be spawned.
   */
  public int getProcesses() {
    return AUTO.equals(getProperty(PROCESSES)) ? AUTO_PROCESSES : getPropertyIntSafe(PROCESSES);
  }

  public boolean isActive() {
    return getProcesses() != 0;
  }

  public String getPropertyOrDefault(final String key, final String defaultValue) {
    final String value = getProperty(key);

    return value == null ? defaultValue : value;
  }

  public String getProperty(final String key) {
    final String fullPropertyName = getFullPropertyName(key);
    final String prop = System.getProperty(fullPropertyName);

    if (prop == null) {
      final String env = EnvUtils.getEnvProperty(fullPropertyName);

      if (env == null) {
        return properties.getProperty(fullPropertyName);
      } else {
        LOG.debug("Reading property {} from environment variable", fullPropertyName);
        return env;
      }
    } else {
      LOG.debug("Reading property {} from property variable", fullPropertyName);
      return prop;
    }
  }

  public Optional<String> getPropertyOptional(final String key) {
    return Optional.ofNullable(getProperty(key));
  }

  /**
   * Returns the full name of the property including the prefix.
   * @param key name of the property to get the full name
   * @return full property name
   */
  public String getFullPropertyName(final String key) {
    return keyPrefix + key;
  }

  /**
   * Returns the property value as int or 0 if value not set.
   *
   * @param key key of property to get value for
   * @return property value or 0
   */
  public int getPropertyIntSafe(final String key) {
    return getPropertyOrDefault(key, 0);
  }

  /**
   * Returns the property value as int or the default value if value not set.
   *
   * @param key key of property to get value for
   * @param defaultValue value to return if not set
   * @return property value or default value
   */
  public int getPropertyOrDefault(final String key, final int defaulValue) {
    final String value = getProperty(key);
    return value == null ? defaulValue : Integer.parseInt(value);
  }

  /**
   * Returns true if and only if property is true. If not found returns false.
   * @param key key to check
   * @return true if set or false if not found or set to false
   */
  public boolean getPropertyBooleanSafe(final String key) {
    return Boolean.parseBoolean(getProperty(key));
  }

  /**
   * Returns the as boolean or the default if the value not set.
   * @param key key of property to get value for
   * @param defaultValue value to return if not set
   * @return property value or default value
   */
  public boolean getPropertyBooleanSafe(final String key, final boolean defaultValue) {
    final String value = getProperty(key);

    return value == null ? defaultValue : Boolean.parseBoolean(value);
  }
}
