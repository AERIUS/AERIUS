/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

/**
 * Base class for worker configurations.
 */
public class WorkerConfiguration {

  private final int processes;
  private String fileServerInternalUrl;
  /**
   * Default base location where a file can be downloaded from.
   */
  private String fileServerExternalUrl;
  private int fileServerTimeout;
  private String authentication;

  /**
   * Constructs {@link WorkerConfiguration} with no processes set. Use if processes are not relevant.
   */
  protected WorkerConfiguration() {
    this(0);
  }

  /**
   * Constructor
   *
   * @param processes number of concurrent worker processes that should be started
   */
  protected WorkerConfiguration(final int processes) {
    this.processes = processes;
  }

  public int getProcesses() {
    return processes;
  }

  public String getFileServerInternalUrl() {
    return fileServerInternalUrl;
  }

  void setFileServerInternalUrl(final String fileServerUrl) {
    this.fileServerInternalUrl = fileServerUrl;
  }

  public String getFileServerExternalUrl() {
    return fileServerExternalUrl;
  }

  void setFileServerExternalUrl(final String fileServerUrl) {
    this.fileServerExternalUrl = fileServerUrl;
  }

  public String getFileServerAuthentication() {
    return authentication;
  }

  void setFileServerAuthentication(final String authentication) {
    this.authentication = authentication;
  }

  public int getFileServerTimeout() {
    return fileServerTimeout;
  }

  void setFileServerTimeout(final int fileServerTimeout) {
    this.fileServerTimeout = fileServerTimeout;
  }
}
