/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.backgrounddata;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

public class DepositionVelocityMap {
  protected final int hexHor;
  protected final ReceptorUtil receptorUtil;
  protected final Map<Integer, Double> depositionVelocities = new HashMap<>();
  private final Supplier<Double> defaultValueSupplier;

  public DepositionVelocityMap(final int hexHor, final ReceptorUtil receptorUtil, final Supplier<Double> defaultValueSupplier) {
    this.hexHor = hexHor;
    this.receptorUtil = receptorUtil;
    this.defaultValueSupplier = defaultValueSupplier;
  }

  public boolean put(final Integer receptorId, final Double value) {
    return depositionVelocities.put(receptorId, value) == null;
  }

  /**
   * Finds the deposition velocity for the level 1 hexagon nearest the given point.
   *
   * @param point
   *          The point.
   * @return The deposition velocity, or null if they could not be found.
   */
  public Double getDepositionVelocity(final AeriusResultPoint point) {
    if (point.getPointType() == AeriusPoint.AeriusPointType.RECEPTOR) {
      return getDepositionVelocityById(point.getId());
    } else {
      return getDepositionVelocityByCoordinate(point.getX(), point.getY());
    }
  }

  /**
   * Finds the deposition velocity for the level 1 hexagon nearest the given coordinates.
   *
   * @param x The x coordinate.
   * @param y The y coordinate.
   * @return The deposition velocity, or the default as provided by te defaultValueSupplier.
   */
  public Double getDepositionVelocityByCoordinate(final double x, final double y) {
    return getDepositionVelocityById(receptorUtil.getReceptorIdFromPoint(new Point(x, y)));
  }

  public Double getDepositionVelocityById(final int receptorId) {
    return Optional.ofNullable(depositionVelocities.get(receptorId)).orElseGet(defaultValueSupplier);
  }

  public int size() {
    return depositionVelocities.size();
  }
}
