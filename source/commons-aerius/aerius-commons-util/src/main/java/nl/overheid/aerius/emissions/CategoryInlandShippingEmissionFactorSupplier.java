/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.InlandCategoryEmissionFactorKey;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.emissions.InlandRouteKey;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.emissions.InlandShippingEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.shipping.InlandShippingRouteEmissionPoint;
import nl.overheid.aerius.shared.emissions.shipping.ShippingLaden;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

class CategoryInlandShippingEmissionFactorSupplier implements InlandShippingEmissionFactorSupplier {

  private static final Logger LOG = LoggerFactory.getLogger(CategoryInlandShippingEmissionFactorSupplier.class);

  private final Map<InlandRouteKey, Map<Substance, Double>> cacheRouteEmissionFactors = new HashMap<>();
  private final Map<String, Map<Substance, Double>> cacheDockedEmissionFactors = new HashMap<>();

  private final SectorCategories categories;
  private final PMF pmf;
  private final int year;

  CategoryInlandShippingEmissionFactorSupplier(final SectorCategories categories, final PMF pmf, final int year) {
    this.categories = categories;
    this.pmf = pmf;
    this.year = year;
  }

  @Override
  public List<InlandShippingRouteEmissionPoint> determineInlandRoutePoints(final Geometry geometry, final Optional<InlandWaterway> waterway)
      throws AeriusException {
    final List<InlandShippingRouteEmissionPoint> points = new ArrayList<>();
    try (final Connection con = pmf.getConnection()) {
      points.addAll(ShippingRepository.getInlandShippingRoutePoints(con, geometry, waterway));
    } catch (final SQLException e) {
      LOG.error("Error determining inland route points", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return points;
  }

  @Override
  public Map<Substance, Double> getInlandShippingRouteEmissionFactors(final String waterwayCode, final WaterwayDirection direction,
      final ShippingLaden laden, final String shipCode) throws AeriusException {
    final InlandRouteKey routeKey = new InlandRouteKey(waterwayCode, direction, laden, shipCode);
    if (!cacheRouteEmissionFactors.containsKey(routeKey)) {
      cacheRouteEmissionFactors.put(routeKey, getRouteEmissionFactorsFromDatabase(routeKey));
    }
    return cacheRouteEmissionFactors.get(routeKey);
  }

  private Map<Substance, Double> getRouteEmissionFactorsFromDatabase(final InlandRouteKey routeKey) throws AeriusException {
    final Map<Substance, Double> factors = new EnumMap<>(Substance.class);
    final InlandShippingCategory ship = shipCategory(routeKey.getShipCode());
    final InlandWaterwayCategory waterway = waterwayCategory(routeKey.getWaterwayCode());
    try (final Connection con = pmf.getConnection()) {
      final Map<InlandCategoryEmissionFactorKey, Double> databaseFactors = ShippingCategoryRepository.findInlandCategoryRouteEmissionFactors(con,
          ship, year);
      for (final Entry<InlandCategoryEmissionFactorKey, Double> entry : databaseFactors.entrySet()) {
        final InlandCategoryEmissionFactorKey key = entry.getKey();
        if (key.getDirection() == routeKey.getDirection()
            && key.getWaterwayId() == waterway.getId()
            && (key.isLaden() == (routeKey.getLaden() == ShippingLaden.LADEN))) {
          factors.put(key.getSubstance(), entry.getValue());
        }
      }
    } catch (final SQLException e) {
      LOG.error("Error determining inland route emission factors", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return factors;
  }

  @Override
  public Map<Substance, Double> getInlandShippingDockedEmissionFactors(final String shipCode) throws AeriusException {
    if (!cacheDockedEmissionFactors.containsKey(shipCode)) {
      cacheDockedEmissionFactors.put(shipCode, getDockedEmissionFactorsFromDatabase(shipCode));
    }
    return cacheDockedEmissionFactors.get(shipCode);
  }

  private Map<Substance, Double> getDockedEmissionFactorsFromDatabase(final String shipCode) throws AeriusException {
    final InlandShippingCategory ship = shipCategory(shipCode);
    try (final Connection con = pmf.getConnection()) {
      return ShippingCategoryRepository.findInlandCategoryMooringEmissionFactors(con, ship, year);
    } catch (final SQLException e) {
      LOG.error("Error determining inland docked emission factors", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private InlandShippingCategory shipCategory(final String shipCode) {
    return categories.getInlandShippingCategories().getShipCategoryByCode(shipCode);
  }

  private InlandWaterwayCategory waterwayCategory(final String waterwayCode) {
    return categories.getInlandShippingCategories().getWaterwayCategoryByCode(waterwayCode);
  }
}
