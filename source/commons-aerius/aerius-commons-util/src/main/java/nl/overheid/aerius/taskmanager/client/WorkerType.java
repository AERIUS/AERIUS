/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

import nl.aerius.taskmanager.client.WorkerQueueType;

/**
 * Contains the names of the group of queues.
 */
public enum WorkerType {

  /**
   * CERC ADMS calculation engine.
   */
  ADMS,
  /**
   * AERIUS SRM2 calculation engine.
   */
  ASRM,
  /**
   * Worker type for processing calculator/connect database actions.
   */
  CONNECT,
  /**
   * Worker type for importing AERIUS files.
   */
  IMPORTER,
  /**
   * Worker type for processing messages (to be sent to users for instance).
   */
  MESSAGE,
  /**
   * Worker type for processing GOV UK notify messages that are also received via the message queue.
   * Notify worker will also read from message queue, therefore queue name is set accordingly.
   */
  NOTIFY("MESSAGE"),
  /**
   * OPS calculation engine.
   */
  OPS,
  /**
   * Generate the PDF.
   */
  PDF,

  /**
   * Test worker type. doesn't have it's own queue, but can be used in mockup testing.
   */
  TEST;

  private final WorkerQueueType type;

  WorkerType() {
    type = new WorkerQueueType(name());
  }

  WorkerType(final String queueName) {
    type = new WorkerQueueType(queueName);
  }

  public WorkerQueueType type() {
    return type;
  }
}
