/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.backgrounddata;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * Utility class for reading deposition velocity files.
 */
public final class DepositionVelocityReader {

  private static final Logger LOG = LoggerFactory.getLogger(DepositionVelocityReader.class);

  private static final String SPLIT_PATTERN = "[,\t]";
  // The number of columns: receptorId, NO2, NH3
  private static final int COLUMNS = 3;

  private DepositionVelocityReader() {
    // util class.
  }

  /**
   * Reads the deposition velocity data from the given input stream.
   *
   * @param inputStream
   *          The input stream containing the deposition velocities.
   * @param hexHor Number of hexagons on a receptor grid row for zoomlevel 1.
   * @return A mapping from receptor id to deposition velocity.
   * @throws IOException
   *           Thrown if an error occurred while reading the stream.
   */
  public static <T extends DepositionVelocityMap> Map<Substance, T> read(final InputStream inputStream, final int hexHor,
      final ReceptorUtil receptorUtil, final Class<T> velocityMapClass) throws IOException {
    return read(inputStream, hexHor, receptorUtil, velocityMapClass, Map.of(Substance.NO2, () -> null, Substance.NH3, () -> null));
  }

  public static <T extends DepositionVelocityMap> Map<Substance, T> read(final InputStream inputStream, final int hexHor,
      final ReceptorUtil receptorUtil, final Class<T> velocityMapClass, final Map<Substance, Supplier<Double>> defaultValueSuppliers)
      throws IOException {

    final Map<Substance, T> depositionVelocities = new EnumMap<>(Substance.class);
    try {
      depositionVelocities.put(Substance.NO2, velocityMapClass
          .getDeclaredConstructor(int.class, ReceptorUtil.class, Supplier.class)
          .newInstance(hexHor, receptorUtil, defaultValueSuppliers.get(Substance.NO2)));

      depositionVelocities.put(Substance.NH3, velocityMapClass
          .getDeclaredConstructor(int.class, ReceptorUtil.class, Supplier.class)
          .newInstance(hexHor, receptorUtil, defaultValueSuppliers.get(Substance.NH3)));

    } catch (final InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
      LOG.error("Failed to instantiate '{}'", velocityMapClass.getName(), e);
    }

    try (final InputStreamReader is = new InputStreamReader(inputStream, StandardCharsets.UTF_8.name());
        final LineNumberReader reader = new LineNumberReader(is)) {
      // Discard first line as it contains the headers.
      final String header = reader.readLine();
      if (header != null) {
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
          parseLine(depositionVelocities.get(Substance.NO2), depositionVelocities.get(Substance.NH3), reader, line);
        }
      }
    }

    return depositionVelocities;
  }

  private static void parseLine(final DepositionVelocityMap depositionVelocitiesNO2, final DepositionVelocityMap depositionVelocitiesNH3,
      final LineNumberReader reader, final String line) throws IOException {
    final String[] parts = line.split(SPLIT_PATTERN);
    if (parts.length != COLUMNS) {
      throw new IOException("Invalid number of columns for line " + reader.getLineNumber()
          + ". Expected " + COLUMNS + ", actual " + parts.length);
    }
    final int receptorId = parseReceptorId(reader, parts);
    parseSubstance(depositionVelocitiesNO2, reader, parts[1], receptorId, "NOx");
    parseSubstance(depositionVelocitiesNH3, reader, parts[2], receptorId, "NH3");
  }

  private static int parseReceptorId(final LineNumberReader reader, final String... parts) throws IOException {
    final int receptorId;
    try {
      receptorId = Integer.parseInt(parts[0]);
    } catch (final NumberFormatException e) {
      throw new IOException("Error parsing receptor id (" + parts[0] + ")  on line " + reader.getLineNumber(), e);
    }
    return receptorId;
  }

  private static void parseSubstance(final DepositionVelocityMap depositionVelocitiesMap, final LineNumberReader reader,
      final String part, final int receptorId, final String substance) throws IOException {
    try {
      final double velocityValue = Double.parseDouble(part);
      depositionVelocitiesMap.put(receptorId, velocityValue);
    } catch (final NumberFormatException e) {
      throw new IOException("Error parsing " + substance + " (" + part + ") velocity on line " + reader.getLineNumber(), e);
    }
  }
}
