/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadStandardEmissionFactorsKey;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadStandardsInterpolationValues;
import nl.overheid.aerius.shared.emissions.RoadEmissionFactorSupplier;

class CategoryRoadEmissionFactorSupplier implements RoadEmissionFactorSupplier {

  private final SectorCategories categories;
  private final int year;

  CategoryRoadEmissionFactorSupplier(final SectorCategories categories, final int year) {
    this.categories = categories;
    this.year = year;
  }

  @Override
  public Map<Substance, Double> getRoadSpecificVehicleEmissionFactors(final String specificVehicleCode, final String roadTypeCode) {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);
    final OnRoadMobileSourceCategory category = categories.determineOnRoadMobileSourceCategoryByCode(specificVehicleCode);
    for (final Substance substance : Substance.values()) {
      final double emissionFactor = category.getEmissionFactor(substance, roadTypeCode, year);
      if (emissionFactor > 0) {
        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }

  @Override
  public Map<Substance, Double> getRoadStandardVehicleEmissionFactors(final RoadStandardEmissionFactorsKey emissionFactorsKey) {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);
    final RoadEmissionCategory category = standardCategory(emissionFactorsKey);
    for (final Substance substance : Substance.values()) {
      final double emissionFactor = category.getEmissionFactor(new EmissionValueKey(year, substance));
      if (emissionFactor > 0) {
        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }

  @Override
  public Map<Substance, Double> getRoadStandardVehicleStagnatedEmissionFactors(final RoadStandardEmissionFactorsKey emissionFactorsKey) {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);
    final RoadEmissionCategory category = standardCategory(emissionFactorsKey);
    for (final Substance substance : Substance.values()) {
      final double emissionFactor = category.getStagnatedEmissionFactor(new EmissionValueKey(year, substance));
      if (emissionFactor > 0) {
        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }

  private RoadEmissionCategory standardCategory(final RoadStandardEmissionFactorsKey emissionFactorsKey) {
    return categories.getRoadEmissionCategories().findClosestCategory(
        emissionFactorsKey.getRoadAreaCode(), emissionFactorsKey.getRoadTypeCode(), emissionFactorsKey.getStandardVehicleCode(),
        emissionFactorsKey.getStrictEnforcement(), emissionFactorsKey.getMaximumSpeed(), emissionFactorsKey.getGradient());
  }

  @Override
  public RoadStandardsInterpolationValues getRoadStandardVehicleInterpolationValues(final RoadStandardEmissionFactorsKey emissionFactorsKey) {
    final List<Integer> speeds = categories.getRoadEmissionCategories().getMaximumSpeedCategories(emissionFactorsKey.getRoadAreaCode(),
        emissionFactorsKey.getRoadTypeCode());
    final List<Integer> gradients = categories.getRoadEmissionCategories().getGradientCategories(emissionFactorsKey.getRoadAreaCode(),
        emissionFactorsKey.getRoadTypeCode(), emissionFactorsKey.getStandardVehicleCode());
    return new RoadStandardsInterpolationValues(
        lowerBound(speeds, emissionFactorsKey.getMaximumSpeed()), upperBound(speeds, emissionFactorsKey.getMaximumSpeed()),
        lowerBound(gradients, emissionFactorsKey.getGradient()), upperBound(gradients, emissionFactorsKey.getGradient()));
  }

  private static int lowerBound(final List<Integer> values, final double targetValue) {
    if (values.isEmpty()) {
      return 0;
    }
    final OptionalInt lowerValue = values.stream()
        .mapToInt(x -> x)
        .filter(x -> x <= targetValue)
        .max();
    return lowerValue.orElseGet(() -> values.stream()
        .mapToInt(x -> x)
        .min()
        .orElse((int) Math.round(targetValue)));
  }

  private static int upperBound(final List<Integer> values, final double targetValue) {
    if (values.isEmpty()) {
      return 0;
    }
    final OptionalInt upperValue = values.stream()
        .mapToInt(x -> x)
        .filter(x -> x >= targetValue)
        .min();
    return upperValue.orElseGet(() -> values.stream()
        .mapToInt(x -> x)
        .max()
        .orElse((int) Math.round(targetValue)));
  }

}
