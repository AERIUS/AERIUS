/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * This Util class contains a method to divide up a hexagon in equilateral triangles and returns the center points for those triangles.
 * The triangle coordinates can be used to transpose to points for a specific hexagon by adding the coordinates to the center point of the hexagon.
 * Note that the coordinates are only for the upper half of a hexagon as the triangles on the lower half of a hexagon are a mirror of the upper half.
 * Computing the points for the lower half can simply be done by using the negated y coordinate of the triangle.
 *
 * <p>Description of the algorithm to compute the equilateral triangle center points.
 *
 * <p>To calculate the specific equilateral triangle center points a depth first algorithm is used.
 * This algorithm makes use of the fact each equilateral triangle contains 4 equilateral triangles on the next level.
 * These equilateral triangles on the next level have a defined pattern.
 * Either the equilateral triangle points up or down. This looks like this:
 * <pre>
 *                  ________
 *    /\            \  /\  /
 *   /__\     and    \/__\/
 *  /\  /\            \  /
 * /__\/__\            \/
 * </pre>
 * The points in the internal equilateral triangles points can be derived using mathematical ratios of these points.
 * The depth first is also used to give the equilateral triangles an unique id.
 * This id is unique per level.
 * This means based on the id the level can be derived.
 * By using depth first to create the equilateral triangles and give them a sequential number the relation to the lower and higher levels is
 * preserved. This means given a id it possible to get the ids of the equilateral triangles contained on a higher level or to get the ids
 * of equilateral triangle on a lower level.
 *
 * For different levels the following values apply:
 * <pre>
 * level | points         | ids
 *  1    |    6           | 1 ... 6
 *  2    |   24           | 7 ... 30
 *  3    |   96           | 31 ... 126
 *  4    |  384           | 127 ... 510
 *  n    |  6*4^(level-1) |
 * </pre>
 */
final class Hexagon2EquilateralTriangles {

  private static final MathContext MATH_CONTEXT = MathContext.DECIMAL64;
  private static final BigDecimal TWO = BigDecimal.valueOf(2);
  private static final BigDecimal THREE = BigDecimal.valueOf(3);

  private final Map<Integer, AtomicInteger> map = new HashMap<>();
  private final Map<Integer, List<TrianglePoint>> points = new HashMap<>();

  private final BigDecimal radiusHexagon;
  private final int levels;

  /**
   *
   * @param radiusHexagaxon radius of the hexagon to compute the equilateral triangles for
   * @param levels number of levels to prepare
   */
  public Hexagon2EquilateralTriangles(final double radiusHexagaxon, final int levels) {
    this.radiusHexagon = BigDecimal.valueOf(radiusHexagaxon);
    this.levels = levels;
  }

  /**
   * Creates the equilateral triangles center coordinates and triangle area coordinates of a hexagon given the level and radius of the hexagon.
   *
   * @param level level number, with 1 as first level of 6 equilateral triangles in a hexagon
   * @return array of equilateral triangles coordinates and triangle area coordinates
   */
  public Map<Integer, TrianglePoint[]> createTriangleCenterPoints() {
    final BigDecimal halfRadius = radiusHexagon.divide(TWO);
    final BigDecimal height = radiusHexagon.pow(2).subtract(halfRadius.pow(2)).sqrt(MATH_CONTEXT);
    final BigDecimal twoThird = height.divide(THREE, MATH_CONTEXT).multiply(TWO);
    traverseTrianglesDepthFirst(0, 0, radiusHexagon, BigDecimal.ZERO, twoThird, true);
    traverseTrianglesDepthFirst(0, 0, radiusHexagon, BigDecimal.ZERO, twoThird.negate(), false);

    return points.entrySet().stream()
        .collect(Collectors.<Map.Entry<Integer, List<TrianglePoint>>, Integer, TrianglePoint[]> toUnmodifiableMap(Map.Entry::getKey,
            p -> p.getValue().toArray(new TrianglePoint[0])));
  }

  private void traverseTrianglesDepthFirst(final int level, final int id, final BigDecimal radius, final BigDecimal x, final BigDecimal y,
      final boolean up) {
    if (level > levels) {
      return;
    }
    final AtomicInteger levelCounter = map.computeIfAbsent(level, k -> new AtomicInteger(startLevelCount(level + 1)));
    final BigDecimal halfEdge = radius.divide(TWO);
    final BigDecimal halfRadius = radius.divide(TWO);
    final BigDecimal height = radius.pow(2).subtract(halfRadius.pow(2)).sqrt(MATH_CONTEXT);
    final BigDecimal oneThird = height.divide(THREE, MATH_CONTEXT);
    final BigDecimal twoThird = oneThird.add(oneThird);
    final int nextLevel = level + 1;
    if (level > 0) {
      points.computeIfAbsent(level, k -> new ArrayList<>()).add(createTrianglePoint(id, x, y, up, oneThird, twoThird, halfEdge));
    }

    // Start a level 0. Level 0 is the hexagon. But a hexagon is 2 triangles with the triangle above, and below cut off.
    // This is handles here by ignoring the up and down triangle on level 0.
    if (up) {
      if (level > 0) {
        traverseTrianglesDepthFirst(nextLevel, levelCounter.incrementAndGet(), halfRadius, x, y.add(twoThird), up);
      }
      traverseTrianglesDepthFirst(nextLevel, levelCounter.incrementAndGet(), halfRadius, x.subtract(halfRadius), y.subtract(oneThird), up);
      traverseTrianglesDepthFirst(nextLevel, levelCounter.incrementAndGet(), halfRadius, x, y, !up);
      traverseTrianglesDepthFirst(nextLevel, levelCounter.incrementAndGet(), halfRadius, x.add(halfRadius), y.subtract(oneThird), up);
    } else {
      traverseTrianglesDepthFirst(nextLevel, levelCounter.incrementAndGet(), halfRadius, x.subtract(halfRadius), y.add(oneThird), up);
      traverseTrianglesDepthFirst(nextLevel, levelCounter.incrementAndGet(), halfRadius, x, y, !up);
      traverseTrianglesDepthFirst(nextLevel, levelCounter.incrementAndGet(), halfRadius, x.add(halfRadius), y.add(oneThird), up);
      if (level > 0) {
        traverseTrianglesDepthFirst(nextLevel, levelCounter.incrementAndGet(), halfRadius, x, y.subtract(twoThird), up);
      }
    }
  }

  private static int startLevelCount(final int level) {
    return level == 1 ? 0 : (int) (6 + (8 * (Math.pow(4, level - 2.0) - 1)));
  }

  private static TrianglePoint createTrianglePoint(final int id, final BigDecimal x, final BigDecimal y, final boolean up, final BigDecimal oneThird,
      final BigDecimal twoThird, final BigDecimal halfEdge) {
    return new TrianglePoint(id, x, y, triangleAreaCoordinates(x, y, up, oneThird.multiply(TWO), twoThird.multiply(TWO), halfEdge.multiply(TWO)));
  }

  private static BigDecimal[][] triangleAreaCoordinates(final BigDecimal x, final BigDecimal y, final boolean up, final BigDecimal oneThird,
      final BigDecimal twoThird, final BigDecimal halfEdge) {
    final BigDecimal[][] coordinates = new BigDecimal[4][2];

    coordinates[0][0] = x;
    coordinates[1][0] = x.subtract(halfEdge);
    coordinates[2][0] = x.add(halfEdge);
    if (up) {
      coordinates[0][1] = y.add(twoThird);
      coordinates[1][1] = y.subtract(oneThird);
      coordinates[2][1] = y.subtract(oneThird);
    } else {
      coordinates[0][1] = y.subtract(twoThird);
      coordinates[1][1] = y.add(oneThird);
      coordinates[2][1] = y.add(oneThird);
    }
    coordinates[3] = coordinates[0];
    return coordinates;
  }
}
