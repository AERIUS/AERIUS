/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.util.Collections;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.shared.emissions.FarmlandEmissionFactorSupplier;

/**
 * Emission factor supplier for farmland sources.
 */
public class CategoryFarmlandEmissionFactorSupplier implements FarmlandEmissionFactorSupplier {

  private final SectorCategories sectorCategories;

  public CategoryFarmlandEmissionFactorSupplier(final SectorCategories sectorCategories) {
    this.sectorCategories = sectorCategories;
  }

  @Override
  public Map<Substance, Double> getFarmSourceEmissionFactors(final String farmSourceCategoryCode) {
    return sectorCategories.getFarmSourceCategories().stream()
        .filter(c -> c.getCode().equals(farmSourceCategoryCode))
        .map(FarmSourceCategory::getEmissionFactors)
        .findFirst()
        .orElse(Collections.emptyMap());
  }

  @Override
  public FarmEmissionFactorType getFarmSourceEmissionFactorType(final String farmSourceCategoryCode) {
    return sectorCategories.getFarmSourceCategories().stream()
        .filter(c -> c.getCode().equals(farmSourceCategoryCode))
        .map(FarmSourceCategory::getFarmEmissionFactorType)
        .findFirst()
        .orElse(null);
  }
}
