/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasName;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Class to collect input validations using javax validation. Can both throw and write to file validation results.
 */
public class InputValidatorCollector<T extends EngineSource> {

  private static final String VALIDATION_ERRORS_TXT = "validationErrors.txt";
  private static final long MAX_NUMBER_OF_MESSAGES = 30;
  private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

  private final boolean validationErrorsToFile;
  private final Reason exceptionReason;

  private final List<String> generalErrorMessages = new ArrayList<>();
  private final Map<Integer, List<String>> sourcesErrorMessages = new HashMap<>();

  /**
   * Constructor.
   *
   * @param validationErrorsToFile if true validation errors will be collected to be written to file, otherwise these will be thrown.
   * @param exceptionReason Exception reason to use to log the validation errors.
   */
  public InputValidatorCollector(final boolean validationErrorsToFile, final Reason exceptionReason) {
    this.validationErrorsToFile = validationErrorsToFile;
    this.exceptionReason = exceptionReason;
  }

  /**
   * Adds to generic validation errors that should be included in every collected validation result if validation errors need to be written to file.
   * Returns true if collected to be written to file.
   *
   * @param e exception to collect
   * @return true if written to file
   */
  public boolean addGeneralValidationErrors(final Exception e) {
    if (validationErrorsToFile) {
      generalErrorMessages.add(e.getMessage());
      return true;
    } else {
      return false;
    }
  }

  /**
   * Collect validation errors in the objects. Validations in the map can later be written to file using the key of the map
   * (if configured to write to file).
   *
   * @param object Object to check for validation errors.
   * @param map Map to check for validation errors.
   * @throws AeriusException if validations should not be written to files the validations are thrown as exception
   */
  public void collect(final Object object, final Map<Integer, Collection<T>> map) throws AeriusException {
    collectValidationsOfObject(object);
    collectValidationsOnMap(map);
    if (!validationErrorsToFile) {
      throwError();
    }
  }

  private void collectValidationsOfObject(final Object input) throws AeriusException {
    aggregateError("", validator.validate(input), generalErrorMessages);
  }

  private void collectValidationsOnMap(final Map<Integer, Collection<T>> sources) {
    for (final Entry<Integer, Collection<T>> entry : sources.entrySet()) {
      final List<String> sourcesByKeyErrorMessages = sourcesErrorMessages.computeIfAbsent(entry.getKey(), e -> new ArrayList<>());

      for (final T source : entry.getValue()) {
        aggregateError(sourceName(source), validator.validate(source), sourcesByKeyErrorMessages);
      }
    }
  }

  private void throwError() throws AeriusException {
    final List<String> errorMessages = new ArrayList<>();

    errorMessages.addAll(generalErrorMessages);
    sourcesErrorMessages.entrySet().forEach(e -> errorMessages.addAll(e.getValue()));
    if (!errorMessages.isEmpty()) {
      throw new AeriusException(exceptionReason, String.join(", ", errorMessages));
    }
  }

  /**
   * If configured to write validation errors to file this method writes the
   *
   * @param key key into the map for which the validations were collected
   * @param directory directory to write the validation errors file to
   * @param newline newline to use in validation errors file
   * @throws IOException
   */
  public void writeErrorsToFile(final Integer key, final File directory, final String newline) throws IOException {
    if (!validationErrorsToFile) {
      return;
    }
    final List<String> errorMessages = new ArrayList<>();

    errorMessages.addAll(generalErrorMessages);
    if (sourcesErrorMessages.containsKey(key)) {
      errorMessages.addAll(sourcesErrorMessages.get(key));
    }
    if (!errorMessages.isEmpty()) {
      try (final Writer writer = Files.newBufferedWriter(new File(directory, VALIDATION_ERRORS_TXT).toPath(), StandardCharsets.UTF_8)) {
        writer.append(String.join(newline, errorMessages));
      }
    }
  }

  private static <C> void aggregateError(final String name, final Set<ConstraintViolation<C>> sViolations, final List<String> errorMessages) {
    if (!sViolations.isEmpty()) {
      errorMessages.addAll(sViolations.stream()
          .limit(MAX_NUMBER_OF_MESSAGES)
          .map(violation -> name + ":" + violation.getPropertyPath() + ": " + violation.getMessage())
          .toList());
    }
  }

  private static String sourceName(final EngineSource source) {
    final String name;
    if (source instanceof final HasName hasName) {
      name = hasName.getName();
    } else if (source instanceof final HasId hasId) {
      name = String.valueOf(hasId.getId());
    } else {
      name = source.toString();
    }
    return name;
  }
}
