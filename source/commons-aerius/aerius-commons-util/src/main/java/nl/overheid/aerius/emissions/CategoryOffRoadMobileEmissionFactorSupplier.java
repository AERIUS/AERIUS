/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.util.EnumMap;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.function.ToDoubleBiFunction;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.emissions.OffRoadMobileEmissionFactorSupplier;

class CategoryOffRoadMobileEmissionFactorSupplier implements OffRoadMobileEmissionFactorSupplier {

  private final SectorCategories categories;

  CategoryOffRoadMobileEmissionFactorSupplier(final SectorCategories categories) {
    this.categories = categories;
  }

  @Override
  public Map<Substance, Double> getOffRoadMobileEmissionFactorsPerLiterFuel(final String offRoadMobileSourceCode) {
    return getEmissionFactors(offRoadMobileSourceCode,
        (category, substance) -> category.getEmissionFactorPerLiterFuel(substance));
  }

  @Override
  public Map<Substance, Double> getOffRoadMobileEmissionFactorsPerOperatingHour(final String offRoadMobileSourceCode) {
    return getEmissionFactors(offRoadMobileSourceCode,
        (category, substance) -> category.getEmissionFactorPerOperatingHour(substance));
  }

  @Override
  public OptionalDouble getMaxAdBlueFuelRatio(final String offRoadMobileSourceCode) {
    final Double value = offRoadMobile(offRoadMobileSourceCode).getMaxAdBlueFuelRatio();
    return value == null ? OptionalDouble.empty() : OptionalDouble.of(value);
  }

  @Override
  public Map<Substance, Double> getOffRoadMobileEmissionFactorsPerLiterAdBlue(final String offRoadMobileSourceCode) {
    return getEmissionFactors(offRoadMobileSourceCode,
        (category, substance) -> category.getEmissionFactorPerLiterAdBlue(substance));
  }

  private Map<Substance, Double> getEmissionFactors(final String offRoadMobileSourceCode,
      final ToDoubleBiFunction<OffRoadMobileSourceCategory, Substance> emissionFactorFunction) {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);
    final OffRoadMobileSourceCategory category = offRoadMobile(offRoadMobileSourceCode);
    for (final Substance substance : Substance.values()) {
      final double emissionFactor = emissionFactorFunction.applyAsDouble(category, substance);
      if (Double.compare(emissionFactor, 0) != 0) {
        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }

  private OffRoadMobileSourceCategory offRoadMobile(final String offRoadMobileSourceCode) {
    return categories.determineOffRoadMobileSourceCategoryByCode(offRoadMobileSourceCode);
  }

}
