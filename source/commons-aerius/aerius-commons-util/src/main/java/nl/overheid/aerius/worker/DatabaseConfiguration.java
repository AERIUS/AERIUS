/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

/**
 * Base configuration class for workers that need a database connection.
 */
public class DatabaseConfiguration extends WorkerConfiguration {

  private String url;
  private String username;
  private String password;

  /**
   * Constructs configuration with no processes.
   */
  protected DatabaseConfiguration() {
  }

  /**
   * Constructor
   *
   * @param processes number of concurrent worker processes that should be started
   */
  protected DatabaseConfiguration(final int processes) {
    super(processes);
  }

  public String getDatabaseUrl() {
    return url;
  }

  void setDatabaseUrl(final String url) {
    this.url = url;
  }

  public String getDatabaseUsername() {
    return username;
  }

  void setDatabaseUsername(final String username) {
    this.username = username;
  }

  public String getDatabasePassword() {
    return password;
  }

  void setDatabasePassword(final String password) {
    this.password = password;
  }
}
