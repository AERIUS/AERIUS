/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;

/**
 * Class to split sources into 2 groups, and to create a matching set of receptors.
 *
 * For OPS split calculation the calculation is split into 2 groups.
 * <ol>
 * <li>Sources that are <i>within</i> the given split distance to the receptors</li>
 * <li>Sources that are <i>beyond</i> the given split distance to the receptors</li>
 * </ol>
 *
 * Sources within the given split distances will be calculated with all receptors,
 * i.e. including the subreceptors.
 * Sources beyond the given split distance will only be calculated for the receptor center point.
 * Part of this class it to filter the receptor center points from the given list of receptors.
 * When calculated the aggregated results of the subreceptor calculation of the first group
 * and the results of the second group will be add up.
 *
 * However there are some complicating issues.
 * This has to do when sources are within or beyond the split distance, but with or without subreceptors.
 * This can happen when the split distance is either beyond the distance of subreceptors
 * or smaller than the sub receptor distance, i.e. the sources are close enough to the receptor to generate subreceptors,
 * but the split distance is smaller. In that case not sub receptors are calculated at all.
 * Here is a table of the different options:
 *
 * <table>
 * <thead>
 *   <tr>
 *     <td>Sources distance</td>
 *     <td>Subreceptors?</td>
 *     <td>How to calculate</td>
 *     <td>What receptor set(s) to use</td>
 *     <td>Sources No SubReceptors</td>
 *     <td>Sources With SubReceptors</td>
 *     <td>Receptors No SubReceptors</td>
 *   </tr>
 * </thead>
 * <tbody>
 *   <tr>
 *     <td>All within distance</td>
 *     <td>Yes</td>
 *     <td>Calculate as if subreceptor calculation</td>
 *     <td>Use original receptor set</td>
 *     <td>Empty</td>
 *     <td>All</td>
 *     <td>Do not use</td>
 *   </tr>
 *   <tr>
 *     <td>All within distance</td>
 *     <td>No</td>
 *     <td>Calculate as if subreceptor calculation</td>
 *     <td>Use original receptor set</td>
 *     <td>Empty</td>
 *     <td>All</td>
 *     <td>Do not use</td>
 *   </tr>
 *   <tr>
 *     <td>All beyond distance</td>
 *     <td>Yes</td>
 *     <td>Calculate all without subreceptors</td>
 *     <td>Use receptor set without subrceptors</td>
 *     <td>All</td>
 *     <td>Empty</td>
 *     <td>Use</td>
 *   </tr>
 *   <tr>
 *     <td>All beyond distance</td>
 *     <td>No</td>
 *     <td>Calculate all without subreceptors</td>
 *     <td>Use original receptor set</td>
 *     <td>All</td>
 *     <td>Empty</td>
 *     <td>Use</td>
 *   </tr>
 *   <tr>
 *     <td>Both</td>
 *     <td>Yes</td>
 *     <td>Calculate into 2 calculations</td>
 *     <td>Split into 2 receptor sets</td>
 *     <td>Split</td>
 *     <td>Split</td>
 *     <td>Use</td>
 *   </tr>
 *   <tr>
 *     <td>Both</td>
 *     <td>No</td>
 *     <td>Calculate all without subreceptors</td>
 *     <td>Use original receptor set</td>
 *     <td>All</td>
 *     <td>Empty</td>
 *     <td>Use</td>
 *   </tr>
 * </tbody>
 * </table>
 *
 * Note: If all sources beyond the distance and there were sub receptors it means the distance was shorter than the distance
 * at which sub receptors are included.
 */
public abstract class SplitSubReceptorCalculation<S extends EngineEmissionSource, R extends AeriusPoint> {
  private final Collection<S> sources;
  private final Collection<R> receptors;
  private final double splitDistance;

  private final Collection<S> sourcesBeyondDistance = new ArrayList<>();
  private final Collection<S> sourcesWithinDistance = new ArrayList<>();
  private Collection<R> receptorsNoSubreceptors;

  protected SplitSubReceptorCalculation(final Collection<S> sources, final Collection<R> receptors, final double splitDistance) {
    this.sources = sources;
    this.receptors = receptors;
    this.splitDistance = splitDistance;
  }

  public boolean isAllWithinDistance() {
    return sourcesBeyondDistance.isEmpty();
  }

  public boolean isAllBeyondDistance() {
    return sourcesWithinDistance.isEmpty();
  }

  public Collection<S> getSourcesNoSubReceptors() {
    return sourcesBeyondDistance;
  }

  public Collection<S> getSourcesWithSubReceptors() {
    return sourcesWithinDistance;
  }

  /**
   * @return Returns the receptors without sub receptors. These are receptors with {@link AeriusPointType} RECEPTOR or POINT.
   * These are also the types that should be sent back the the chunker and therefor can be used to do a run without aggregation of
   * results. Aggregation calculates point values for the {@link AeriusPointType} RECEPTOR or POINT types.
   */
  public Collection<R> getReceptorsWithOutSubreceptors() {
    return receptorsNoSubreceptors;
  }

  /**
   * Performs the splitting of sources/receptors.
   */
  protected void split() {
    receptorsNoSubreceptors = reduceReceptors();
    sources.forEach(this::splitSource);
  }

  /**
   * @return Returns all receptors that are not sub receptors.
   */
  private Collection<R> reduceReceptors() {
    return receptors.stream().filter(r -> r.getParentId() == null || r.getParentId() == 0).collect(Collectors.toList());
  }

  private void splitSource(final S source) {
    if (receptorsNoSubreceptors.stream().anyMatch(r -> withinDistance(source, r))) {
      sourcesWithinDistance.add(source);
    } else {
      sourcesBeyondDistance.add(source);
    }
  }

  /**
   * Returns true if the source is within the split distance and therefore should be calculated with subreceptors.
   *
   * @param source source to check
   * @param receptor receptor to check
   * @return true if source should be calculated with subreceptors
   */
  private boolean withinDistance(final S source, final R receptor) {
    return distance(source, receptor) < splitDistance;
  }

  /**
   * Returns the distance that should be used to check if the source-receptor distance is within the split distance.
   *
   * @param source source to check
   * @param receptor receptor to check
   * @return split distance of source-receptor
   */
  protected abstract double distance(final S source, final R receptor);
}
