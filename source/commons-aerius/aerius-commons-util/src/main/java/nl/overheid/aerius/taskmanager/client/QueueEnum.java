/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

import java.util.Locale;

/**
 * Contains the names of the static queues used.
 */
public enum QueueEnum {
  // Queues from the Calculator UI
  /**
   * Queue for small UI calculations.
   */
  CALCULATOR_UI_SMALL,
  /**
   * Queue for large UI calculations.
   */
  CALCULATOR_UI_LARGE,
  /**
   * Queue for exporting GML with results from the UI.
   */
  CALCULATOR_GML_EXPORT,
  /**
   * Queue for PDF exports from the UI.
   */
  CALCULATOR_PAA_EXPORT,
  /**
   * Queue for Procurement Policy export.
   */
  CALCULATOR_PP_EXPORT,

  // Queues from Connect API

  /**
   * Queue for PDF exports from Connect API.
   */
  CONNECT_PAA_EXPORT,
  /**
   * Queue for exporting GML with results from Connect API.
   */
  CONNECT_GML_EXPORT,
  /**
   * Queue for Procurement Policy export from Connect API
   */
  CONNECT_PP_EXPORT,
  /**
   * Queue for Register.
   */
  CONNECT_REGISTER_EXPORT,

  /**
   * Queue for exporting input files.
   */
  INPUT_EXPORT,

  // Queue for email messages

  /**
   * Queue for sending emails to a user.
   */
  MESSAGE;

  public String getQueueName() {
    return name().toLowerCase(Locale.ROOT);
  }
}
