/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.trace.Span;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.otel.TracerUtil;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;

/**
 * Helper class to log the duration of a worker process.
 */
public final class WorkerTimeLogger {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerTimeLogger.class);

  private static final int TWO_DIGITS_RESOLUTION = 2;
  private static final String WORKER_TYPE_ATTRIBUTE_KEY = "worker_type";
  private static final String RUN_DURATION_METRIC = "aer.worker.run.duration";

  private final String name;
  private final String unitsAtttribute;
  private final String durationAtttribute;
  private final Attributes runDurationAttributes;
  private final long startTime;

  private WorkerTimeLogger(final String workerTypeName) {
    this.name = workerTypeName.toLowerCase(Locale.ROOT);
    this.unitsAtttribute = name + ".units";
    this.durationAtttribute = name + ".duration";
    startTime = System.nanoTime();
    runDurationAttributes = Attributes.builder()
        .put(AttributeKey.stringKey(WORKER_TYPE_ATTRIBUTE_KEY), name)
        .build();
  }

  public static WorkerTimeLogger start(final String workerTypeName) {
    return new WorkerTimeLogger(workerTypeName);
  }

  /**
   * Log the duration of the given job. Also sets the duration on the OTEL Span.
   *
   * @param jobIdentifier identifier of the job
   */
  public void logDuration(final JobIdentifier jobIdentifier) {
    try {
      LOG.info("Total execution time for {}: {}ms: {}", name, logDuration(), jobIdentifier);
    } catch (final RuntimeException e) {
      LOG.error("Failed to collect statistics for {}", jobIdentifier, e);
    }
  }

  /**
   * Log the duration and statistics about sources, receptors and averages per unit of the given engine worker job.
   * Also sets these values on the OTEL Spans.
   *
   * @param jobIdentifier identifier of the job
   * @param engineInput data to get information about number of sources, receptors
   */
  public <T extends EngineSource> void logDuration(final JobIdentifier jobIdentifier, final EngineInputData<T, ?> engineInput) {
    try {
      final Map<Integer, Collection<T>> emissionSources = engineInput.getEmissionSources();
      final int receptorsSize = engineInput.getReceptors().size();
      final int engineUnits = countEngineUnits(emissionSources, receptorsSize);
      final long delta = logDuration();
      final double average = delta == 0 || engineUnits == 0
          ? 0
          : BigDecimal.valueOf((engineUnits * 1000.0) / delta).setScale(TWO_DIGITS_RESOLUTION, RoundingMode.HALF_UP).doubleValue();

      Span.current().setAttribute(unitsAtttribute, engineUnits);
      LOG.info("Total execution time for {} {} units: {} ms. Average: {} units per second: {}", name, engineUnits, delta, average, jobIdentifier);
    } catch (final RuntimeException e) {
      LOG.error("Failed to collect statistics for {}", jobIdentifier, e);
    }
  }

  private long logDuration() {
    final long delta = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime);

    Span.current().setAttribute(durationAtttribute, delta);
    TracerUtil.getDoubleHistogram(RUN_DURATION_METRIC, "ms").record(delta, runDurationAttributes);
    return delta;
  }

  /**
   * Count the total number of sources * total number of receptors.
   *
   * @param sourcesMap map of source objects
   * @param receptorsSize Number of receptors to calculate
   * @return total sources count * receptors
   */
  private static <T extends EngineSource> int countEngineUnits(final Map<Integer, Collection<T>> sourcesMap, final int receptorsSize) {
    int engineUnits = 0;

    for (final Entry<Integer, Collection<T>> entry : sourcesMap.entrySet()) {
      for (final T source : entry.getValue()) {
        if (source instanceof final EngineEmissionSource ees) {
          engineUnits += ees.getSubstances().size();
        }
      }
    }
    return engineUnits * receptorsSize;
  }
}
