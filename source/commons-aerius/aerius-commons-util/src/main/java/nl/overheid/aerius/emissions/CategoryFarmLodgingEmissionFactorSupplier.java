/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.shared.emissions.FarmLodgingEmissionFactorSupplier;

class CategoryFarmLodgingEmissionFactorSupplier implements FarmLodgingEmissionFactorSupplier {

  private final SectorCategories categories;

  CategoryFarmLodgingEmissionFactorSupplier(final SectorCategories categories) {
    this.categories = categories;
  }

  @Override
  public Map<Substance, Double> getLodgingEmissionFactors(final String lodgingCode) {
    final double emissionFactor = lodgingCategory(lodgingCode).getEmissionFactor();
    return Map.of(Substance.NH3, emissionFactor);
  }

  @Override
  public FarmEmissionFactorType getLodgingEmissionFactorType(final String lodgingCode) {
    final FarmLodgingCategory lodgingCategory = lodgingCategory(lodgingCode);
    return lodgingCategory.getFarmEmissionFactorType();
  }

  @Override
  public boolean canLodgingEmissionFactorsBeConstrained(final String lodgingCode) {
    final FarmLodgingCategory lodgingCategory = lodgingCategory(lodgingCode);
    return lodgingCategory.canLodgingEmissionFactorsBeConstrained();
  }

  @Override
  public boolean isAdditionalSystemScrubber(final String additionalSystemCode) {
    return additionalCategory(additionalSystemCode).isScrubber();
  }

  @Override
  public boolean isReductiveSystemScrubber(final String reductiveSystemCode) {
    return reductiveCategory(reductiveSystemCode).isScrubber();
  }

  @Override
  public Map<Substance, Double> getLodgingConstrainedEmissionFactors(final String lodgingCode) {
    final double emissionFactor = lodgingCategory(lodgingCode).getConstrainedEmissionFactor();
    return Map.of(Substance.NH3, emissionFactor);
  }

  @Override
  public Map<Substance, Double> getAdditionalSystemEmissionFactors(final String additionalSystemCode) {
    final double emissionFactor = additionalCategory(additionalSystemCode).getEmissionFactor();
    return Map.of(Substance.NH3, emissionFactor);
  }

  @Override
  public Map<Substance, Double> getReductiveSystemRemainingFractions(final String reductiveSystemCode) {
    final double reductionFactor = reductiveCategory(reductiveSystemCode).getReductionFactor();
    return Map.of(Substance.NH3, 1 - reductionFactor);
  }

  @Override
  public Map<Substance, Double> getFodderRemainingFractionTotal(final String fodderMeasureCode) {
    final double reductionFactorTotal = fodderMeasure(fodderMeasureCode).getReductionFactorTotal();
    return Map.of(Substance.NH3, 1 - reductionFactorTotal);
  }

  @Override
  public boolean canFodderApplyToLodging(final String fodderMeasureCode, final String lodgingCode) {
    return fodderMeasure(fodderMeasureCode).canApplyToFarmLodgingCategory(lodgingCategory(lodgingCode));
  }

  @Override
  public Map<Substance, Double> getFodderProportionFloor(final String fodderMeasureCode, final String lodgingCode) {
    final FarmAnimalCategory animalCategory = lodgingCategory(lodgingCode).getAnimalCategory();
    final double proportionFloor = fodderMeasure(fodderMeasureCode).getAmmoniaProportions().get(animalCategory).getProportionFloor();
    return Map.of(Substance.NH3, proportionFloor);
  }

  @Override
  public Map<Substance, Double> getFodderProportionCellar(final String fodderMeasureCode, final String lodgingCode) {
    final FarmAnimalCategory animalCategory = lodgingCategory(lodgingCode).getAnimalCategory();
    final double proportionCellar = fodderMeasure(fodderMeasureCode).getAmmoniaProportions().get(animalCategory).getProportionCellar();
    return Map.of(Substance.NH3, proportionCellar);
  }

  @Override
  public Map<Substance, Double> getFodderRemainingFractionFloor(final String fodderMeasureCode) {
    final double reductionFractionFloor = fodderMeasure(fodderMeasureCode).getReductionFactorFloor();
    return Map.of(Substance.NH3, 1 - reductionFractionFloor);
  }

  @Override
  public Map<Substance, Double> getFodderRemainingFractionCellar(final String fodderMeasureCode) {
    final double reductionFractionCellar = fodderMeasure(fodderMeasureCode).getReductionFactorCellar();
    return Map.of(Substance.NH3, 1 - reductionFractionCellar);
  }

  private FarmLodgingCategory lodgingCategory(final String code) {
    return categories.determineFarmLodgingCategoryByCode(code);
  }

  private FarmAdditionalLodgingSystemCategory additionalCategory(final String code) {
    return categories.getFarmLodgingCategories().determineAdditionalLodgingSystemByCode(code);
  }

  private FarmReductiveLodgingSystemCategory reductiveCategory(final String code) {
    return categories.getFarmLodgingCategories().determineReductiveLodgingSystemByCode(code);
  }

  private FarmLodgingFodderMeasureCategory fodderMeasure(final String code) {
    return categories.getFarmLodgingCategories().determineLodgingFodderMeasureByCode(code);
  }
}
