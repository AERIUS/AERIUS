/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Wrapper for {@link SubReceptorLevelMap} to create a map for Hexagon Equilateral Triangles sub receptors.
 */
public final class HexagonEquilateralTrianglesMap<P extends AeriusPoint> extends
    SubReceptorLevelMap<P, TrianglePoint[], HexagonEquilateralTrianglesUtil<P>> {

  private HexagonEquilateralTrianglesMap(final HexagonEquilateralTrianglesUtil<P> subReceptorUtil) {
    super(subReceptorUtil);
  }

  /**
   * Build a {@link HexagonEquilateralTrianglesMap} with levels for distances.
   *
   * @param radius This is the length of a side of the hexagon and determines the size of the triangles.
   * @param distances Depending on the distance between a emission source and receptor it is determined what sub receptor level should be used
   * and that implies the number of subreceptors are used.
   * @param levelOffset Because technically there is an algorithmic dependency between the level and number of sub receptors, and it is possible to
   * not supportlower levels an parameter levelOffset is supported. This implies the distances list start at the offset level instead of level 1.
   * For example if offset level is 3, than when subreceptors are requested based on the first entry in the distances list. It will return
   * subreceptors at level 3.
   * @param createPointFunction function to create the specific sub receptor instance
   * @return HexagonEquilateralTrianglesMap with data per level
   */
  public static <P extends AeriusPoint> HexagonEquilateralTrianglesMap<P> build(final double radius, final List<Double> distances,
      final int levelOffset, final BiFunction<P, Integer, P> createPointFunction) {
    return new Builder<>(createPointFunction, radius, levelOffset, distances).build();
  }

  public static class Builder<P extends AeriusPoint> extends
      SubReceptorLevelMap.Builder<P, TrianglePoint[], HexagonEquilateralTrianglesUtil<P>, HexagonEquilateralTrianglesMap<P>> {

    private final BiFunction<P, Integer, P> createPointFunction;

    public Builder(final BiFunction<P, Integer, P> createPointFunction, final double radius, final int levelOffset, final List<Double> distances) {
      this.createPointFunction = createPointFunction;
      final int levels = distances.size();
      final Hexagon2EquilateralTriangles h2et = new Hexagon2EquilateralTriangles(radius, levels + levelOffset - 1);
      final Map<Integer, TrianglePoint[]> triangles = h2et.createTriangleCenterPoints();

      IntStream.range(0, levels).forEach(i -> add(i + levelOffset, distances.get(i), triangles.get(i + levelOffset)));
    }

    @Override
    protected HexagonEquilateralTrianglesMap<P> create() {
      return new HexagonEquilateralTrianglesMap<>(new HexagonEquilateralTrianglesUtil<>(createPointFunction));
    }
  }
}
