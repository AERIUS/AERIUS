/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.Serializable;
import java.sql.SQLException;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Factory for database based workers.
 */
public abstract class DatabaseWorkerFactory<C extends DatabaseConfiguration> implements WorkerFactory<C> {

  protected final WorkerType workerType;

  protected DatabaseWorkerFactory(final WorkerType workerType) {
    this.workerType = workerType;
  }

  @Override
  public <S extends Serializable, T extends Serializable> Worker<S, T> createWorkerHandler(final C configuration,
      final WorkerConnectionHelper workerConnectionHelper) throws Exception {
    workerConnectionHelper.setPmf(WorkerPMFFactory.createPMF(configuration));
    return createWorkerHandlerWithPMF(configuration, workerConnectionHelper);
  }

  /**
   * Creates the worker type specific worker handler with the database connection factory initialized.
   *
   * @param configuration database configuration.
   * @param workerConnectionHelper Helper class to get connections to external services
   * @return new worker instance
   * @throws AeriusException
   * @throws SQLException
   */
  protected abstract <S extends Serializable, T extends Serializable> Worker<S, T> createWorkerHandlerWithPMF(C configuration,
      WorkerConnectionHelper workerConnectionHelper) throws Exception;

  @Override
  public WorkerType getWorkerType() {
    return workerType;
  }
}
