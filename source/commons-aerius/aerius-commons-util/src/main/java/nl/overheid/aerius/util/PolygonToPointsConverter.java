/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.ArrayList;
import java.util.List;

import org.locationtech.jts.algorithm.Centroid;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.TopologyException;

import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;

/**
 * Class for dividing a polygon into weighted points.
 * The center of the specified polygon is taken, from here the polygon is gridded.
 * The center of the polygon is also the center of the center grid square.
 * The 'resolution' of this grid is specified via 'squaresize'.
 * Each grid square becomes the centroid of the intersection determined between the grid square and the polygon; the location of the point source.
 * There is an associated weight factor with a source,
 * this is the degree of overlap of the square over the polygon. So a square that is completely inside the polygon has a weight of 1.0.
 */
public class PolygonToPointsConverter {

  private final GeometryFactory factory;
  private final double squaresize;

  public PolygonToPointsConverter(final double squaresize) {
    this.squaresize = squaresize;
    this.factory = new GeometryFactory();
  }

  /**
   * Convert a Polygon into a list of points that represent the polygon plane.
   * Each point represents a point in the polygon.
   *
   * @param name The name to reference this polygon in case of an error
   * @param polygon The input polygon geometry
   * @return A collection of point geometries with corresponding weights.
   * @throws AeriusException
   */
  public List<WeightedPoint> convert(final String name, final Polygon polygon) throws AeriusException {
    try {
      final List<WeightedPoint> points = new ArrayList<>();
      final Geometry geometry = GeometryUtil.getGeometry(polygon);
      final Envelope boundingbox = geometry.getEnvelopeInternal();
      final Coordinate center = geometry.getCentroid().getCoordinate();
      final double diameter = squaresize * 0.5;

      final double start_x = Math.round(center.x - Math.round((center.x - boundingbox.getMinX()) / squaresize) * squaresize);
      final double end_x = Math.round(center.x + Math.round((boundingbox.getMaxX() - center.x) / squaresize) * squaresize);
      final double start_y = Math.round(center.y - Math.round((center.y - boundingbox.getMinY()) / squaresize) * squaresize);
      final double end_y = Math.round(center.y + Math.round((boundingbox.getMaxY() - center.y) / squaresize) * squaresize);

      double y = start_y;

      while (y <= end_y) {
        double x = start_x;

        while (x <= end_x) {
          final Envelope square = new Envelope(new Coordinate(x - diameter, y + diameter), new Coordinate(x + diameter, y - diameter));

          if (square.intersects(boundingbox)) {
            final Geometry intersection = factory.toGeometry(square).intersection(geometry);
            final Coordinate point = Centroid.getCentroid(intersection);
            final double weight = intersection.getArea() / (squaresize * squaresize);

            if (weight > 0.0) {
              points.add(new WeightedPoint(point.x, point.y, weight));
            }
          }
          x = x + squaresize;
        }
        y = y + squaresize;
      }
      return points;
    } catch (final TopologyException e) {
      throw new AeriusException(ImaerExceptionReason.GML_GEOMETRY_INTERSECTS, name, "polygon");
    }
  }
}
