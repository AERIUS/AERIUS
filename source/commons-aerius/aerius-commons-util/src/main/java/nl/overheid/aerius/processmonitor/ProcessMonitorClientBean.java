/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.processmonitor;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.worker.ProcessMonitorConstants;

/**
 * Class to sent a cancel job to the RabbitMQ processmonitor exchange. It will use fanout to broadcast to all clients listing to this exchange.
 */
public class ProcessMonitorClientBean {

  private static final Logger LOG = LoggerFactory.getLogger(ProcessMonitorClientBean.class);

  private final BrokerConnectionFactory factory;

  public ProcessMonitorClientBean(final BrokerConnectionFactory factory) {
    this.factory = factory;
  }

  public void cancelJob(final String jobKey) {
    try {
      try (Channel channel = factory.getConnection().createChannel()) {
        channel.exchangeDeclare(ProcessMonitorConstants.PROCESS_MONITOR_EXCHANGE, "fanout");
        channel.basicPublish(ProcessMonitorConstants.PROCESS_MONITOR_EXCHANGE, "", null, jobKey.getBytes(StandardCharsets.UTF_8));
        LOG.debug("Cancelled job:{}", jobKey);
      }
    } catch (IOException | TimeoutException e) {
      LOG.warn("Failed to sent cancel job event for jobKey: {}", jobKey, e);
    }
  }
}
