/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.emissions.ColdStartEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.FarmAnimalHousingEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.FarmLodgingEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.FarmlandEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.InlandShippingEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.ManureStorageEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.MaritimeShippingEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.OffRoadMobileEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.RoadEmissionFactorSupplier;

/**
 * SectorCategories based EmissionFactorSupplier.
 */
public class CategoryBasedEmissionFactorSupplier implements EmissionFactorSupplier {

  private final SectorCategories categories;
  private final PMF pmf;
  private final int year;

  public CategoryBasedEmissionFactorSupplier(final SectorCategories categories, final PMF pmf, final int year) {
    this.categories = categories;
    this.pmf = pmf;
    this.year = year;
  }

  @Override
  public FarmLodgingEmissionFactorSupplier farmLodging() {
    return new CategoryFarmLodgingEmissionFactorSupplier(categories);
  }

  @Override
  public FarmAnimalHousingEmissionFactorSupplier farmAnimalHousing() {
    return new CategoryFarmAnimalHousingEmissionFactorSupplier(categories);
  }

  @Override
  public FarmlandEmissionFactorSupplier farmland() {
    return new CategoryFarmlandEmissionFactorSupplier(categories);
  }

  @Override
  public ManureStorageEmissionFactorSupplier manureStorage() {
    return new CategoryManureStorageEmissionFactorSupplier(categories);
  }

  @Override
  public OffRoadMobileEmissionFactorSupplier offRoadMobile() {
    return new CategoryOffRoadMobileEmissionFactorSupplier(categories);
  }

  @Override
  public ColdStartEmissionFactorSupplier coldStart() {
    return new CategoryColdStartEmissionFactorSupplier(categories, year);
  }

  @Override
  public RoadEmissionFactorSupplier road() {
    return new CategoryRoadEmissionFactorSupplier(categories, year);
  }

  @Override
  public InlandShippingEmissionFactorSupplier inlandShipping() {
    return new CategoryInlandShippingEmissionFactorSupplier(categories, pmf, year);
  }

  @Override
  public MaritimeShippingEmissionFactorSupplier maritimeShipping() {
    return new CategoryMaritimeShippingEmissionFactorSupplier(categories, pmf, year);
  }

}
