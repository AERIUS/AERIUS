/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.EnumSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;

/**
 * Util class for validations regarding scenarios.
 */
public final class ScenarioValidationUtil {

  private static final Set<SituationType> PROJECT_SCENARIO_SITUATION_TYPES = EnumSet.of(
      SituationType.PROPOSED,
      SituationType.REFERENCE,
      SituationType.OFF_SITE_REDUCTION);

  private static final Set<SituationType> PROJECT_SCENARIO_SITUATION_TYPES_MAX_ONE = EnumSet.of(
      SituationType.REFERENCE,
      SituationType.OFF_SITE_REDUCTION);

  private ScenarioValidationUtil() {
    // util class
  }

  /**
   * Validate if the supplied scenario is considered a valid Project scenario.
   * @param scenario The scenario to validate.
   * @return True if valid, false if not.
   */
  public static boolean isValidProjectScenario(final Scenario scenario) {
    final Map<SituationType, Long> countPerSituationType = scenario.getSituations().stream()
        .collect(Collectors.groupingBy(ScenarioSituation::getType, Collectors.counting()));

    return containsAtLeastProposed(countPerSituationType)
        && containsOnlySupportedTypes(countPerSituationType)
        && containsCorrectAmountPerType(countPerSituationType);
  }

  private static boolean containsAtLeastProposed(final Map<SituationType, Long> countPerSituationType) {
    return countPerSituationType.containsKey(SituationType.PROPOSED)
        && countPerSituationType.get(SituationType.PROPOSED) != 0;
  }

  private static boolean containsOnlySupportedTypes(final Map<SituationType, Long> countPerSituationType) {
    boolean valid = true;
    for (final SituationType typeToTest : countPerSituationType.keySet()) {
      if (!PROJECT_SCENARIO_SITUATION_TYPES.contains(typeToTest)) {
        valid = false;
      }
    }
    return valid;
  }

  private static boolean containsCorrectAmountPerType(final Map<SituationType, Long> countPerSituationType) {
    boolean valid = true;
    for (final Entry<SituationType, Long> entry : countPerSituationType.entrySet()) {
      final SituationType typeToTest = entry.getKey();
      if (PROJECT_SCENARIO_SITUATION_TYPES_MAX_ONE.contains(typeToTest) && entry.getValue() > 1) {
        valid = false;
      }
    }
    return valid;
  }

}
