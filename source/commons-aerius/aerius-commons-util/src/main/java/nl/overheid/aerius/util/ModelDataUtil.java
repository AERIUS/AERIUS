/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Util to validate ensure the presence of model data. Downloads and extracts in case not available.
 */
public class ModelDataUtil {

  private static final Logger LOGGER = LoggerFactory.getLogger(ModelDataUtil.class);

  private final HttpClientProxy httpClientProxy;

  public ModelDataUtil(final HttpClientProxy httpClientProxy) {
    this.httpClientProxy = httpClientProxy;
  }

  /**
   * Ensure model data is available, downloading from nexus and unpacking when necessary.
   * @param baseUrl url to nexus repository
   * @param modelName name of the model
   * @param modelVersion explicit version of the model
   * @param modelRootDirectory location where model data should be saved
   * @param presenceTest custom predicate to test the versioned model directory (for example: /ops/5.0.2.0-alpha/) for availability of model data
   *                     (e.g. some binary is present)
   * @throws IOException
   * @throws AeriusException
   */
  public synchronized void ensureModelDataAvailable(final String baseUrl, final String modelName, final String modelVersion,
      final String modelRootDirectory, final Predicate<Path> presenceTest, final ValidationTest validationTest) throws Exception {
    // Check presence of the versioned model data root directory, and whatever specific model validation should be run.
    final Path modelDataDirectoryPath = Path.of(modelRootDirectory, modelVersion);
    if (modelDataDirectoryPath.toFile().isDirectory() && presenceTest.test(modelDataDirectoryPath)) {
      LOGGER.debug("Model data is already present for {} version {}", modelName, modelVersion);
      validationTest.run();
      return;
    }

    // prepare temporary files
    final String downloadFileName = modelVersion + ".tar.gz";
    final File download = Files.createTempFile(null, downloadFileName).toFile();
    final File tarFile = Files.createTempFile(null, modelVersion + ".tar").toFile();

    try {
      download(download, baseUrl, List.of(modelName, downloadFileName));
      decompress(download, tarFile);
      unpack(tarFile, modelDataDirectoryPath);
      validationTest.run();
    } finally {
      // cleanup
      try {
        Files.delete(download.toPath());
        Files.delete(tarFile.toPath());
      } catch (final IOException e) {
        LOGGER.warn("Exception while cleaning up temporary files for model '{}', version '{}':", modelName, modelVersion, e);
      }
    }
  }

  /**
   * Ensure model data is available, downloading from nexus and unpacking when necessary.
   * @param baseUrl url to nexus repository
   * @param modelName name of the model
   * @param modelVersion explicit version of the model
   * @param modelFile actual file name
   * @param modelRootDirectory location where model data should be saved
   * @return File the file that should now be present. Can be null if it was not found or if there was trouble downloading.
   */
  public synchronized File ensureModelDataFileAvailable(final String baseUrl, final String modelName, final List<String> modelVersion,
      final String modelFile, final String modelRootDirectory) {
    // Check presence of the file.
    final Path modelDataDirectoryPath = Path.of(modelRootDirectory, String.join(File.separator, modelVersion));
    final File modelDataFilePath = modelDataDirectoryPath.resolve(modelFile).toFile();
    if (modelDataFilePath.isFile()) {
      LOGGER.debug("Model data is already present for {} version {}", modelName, modelVersion);
      return modelDataFilePath;
    }
    modelDataDirectoryPath.toFile().mkdirs();

    // prepare temporary files
    final String downloadFileName = modelFile + ".gz";
    File download = null;

    try {
      download = Files.createTempFile(null, downloadFileName).toFile();
      final List<String> pathSegments = new ArrayList<>();
      pathSegments.add(modelName);
      pathSegments.addAll(modelVersion);
      pathSegments.add(downloadFileName);
      download(download, baseUrl, pathSegments);
      decompress(download, modelDataFilePath);
    } catch (IOException | AeriusException e) {
      LOGGER.error("Unable to download {}: {}", modelFile, e.getMessage());
      return null;
    } finally {
      // cleanup
      if (download != null) {
        try {
          Files.delete(download.toPath());
        } catch (final IOException e) {
          LOGGER.warn("Exception while cleaning up temporary files for model '{}', version '{}', file '{}':", modelName, modelVersion, modelFile, e);
        }
      }
    }
    return modelDataFilePath;
  }

  public interface ValidationTest {
    void run() throws Exception;
  }

  private void download(final File targetLocation, final String baseUrl, final List<String> extraPathSegments)
      throws IOException, AeriusException {
    try {
      final URIBuilder uriBuilder = new URIBuilder(baseUrl);
      final List<String> pathSegments = new ArrayList<>(uriBuilder.getPathSegments());
      pathSegments.addAll(extraPathSegments);
      uriBuilder.setPathSegments(pathSegments);
      final String url = uriBuilder.build().toString();
      LOGGER.info("Retrieving file from '{}' to '{}'", url, targetLocation);
      httpClientProxy.getContent(url, targetLocation);
    } catch (final URISyntaxException | HttpException e) {
      LOGGER.error("Unable to download {} from {}", extraPathSegments, baseUrl, e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private static void decompress(final File targetLocation, final File tarFile) throws IOException {
    try (final FileOutputStream fos = new FileOutputStream(tarFile);
        final CompressorInputStream cis = new GzipCompressorInputStream(new FileInputStream(targetLocation))) {
      LOGGER.debug("Uncompressing '{}' into '{}'", targetLocation, tarFile);
      cis.transferTo(fos);
    }
  }

  private void unpack(final File tarFile, final Path modelDataDirectoryPath) throws IOException {
    final File modelDataDirectory = modelDataDirectoryPath.getParent().toFile();
    try (final TarArchiveInputStream tais = new TarArchiveInputStream(new FileInputStream(tarFile))) {
      TarArchiveEntry entry;
      while ((entry = (TarArchiveEntry) tais.getNextEntry()) != null) {
        final File outputFile = new File(modelDataDirectory, entry.getName());

        if (entry.isDirectory()) {
          LOGGER.debug("Attempting to write output directory {}.", outputFile.getAbsolutePath());
          if (!outputFile.exists()) {
            LOGGER.debug("Attempting to create output directory {}.", outputFile.getAbsolutePath());
            if (!outputFile.mkdirs()) {
              throw new IllegalStateException(String.format("Couldn't create directory %s.", outputFile.getAbsolutePath()));
            }
          }
        } else {
          LOGGER.debug("Creating output file {}.", outputFile.getAbsolutePath());
          try (final OutputStream outputFileStream = new FileOutputStream(outputFile)) {
            // Create file
            IOUtils.copy(tais, outputFileStream);

            // Set permissions
            if (!OSUtils.isWindows()) {
              Files.setPosixFilePermissions(outputFile.toPath(), getPosixFilePermissions(entry.getMode()));
            }
          }
        }
      }
    }
  }

  /**
   * Parse the posixFilePermission flags from mode integer as a set of PosixFilePermissions.
   * @param mode the file mode from {@code TarArchiveEntry}
   */
  private static Set<PosixFilePermission> getPosixFilePermissions(final int mode) {
    final Set<PosixFilePermission> ret = EnumSet.noneOf(PosixFilePermission.class);
    if ((mode & 0001) > 0) {
      ret.add(PosixFilePermission.OTHERS_EXECUTE);
    }
    if ((mode & 0002) > 0) {
      ret.add(PosixFilePermission.OTHERS_WRITE);
    }
    if ((mode & 0004) > 0) {
      ret.add(PosixFilePermission.OTHERS_READ);
    }
    if ((mode & 0010) > 0) {
      ret.add(PosixFilePermission.GROUP_EXECUTE);
    }
    if ((mode & 0020) > 0) {
      ret.add(PosixFilePermission.GROUP_WRITE);
    }
    if ((mode & 0040) > 0) {
      ret.add(PosixFilePermission.GROUP_READ);
    }
    if ((mode & 0100) > 0) {
      ret.add(PosixFilePermission.OWNER_EXECUTE);
    }
    if ((mode & 0200) > 0) {
      ret.add(PosixFilePermission.OWNER_WRITE);
    }
    if ((mode & 0400) > 0) {
      ret.add(PosixFilePermission.OWNER_READ);
    }
    return ret;
  }
}
