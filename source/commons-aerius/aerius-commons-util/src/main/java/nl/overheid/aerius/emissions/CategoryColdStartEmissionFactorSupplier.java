/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.util.EnumMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.ColdStartSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.emissions.ColdStartEmissionFactorSupplier;

/**
 * Supplies emission factors for cold start emission sources.
 */
public class CategoryColdStartEmissionFactorSupplier implements ColdStartEmissionFactorSupplier {

  private final SectorCategories categories;
  private final int year;

  public CategoryColdStartEmissionFactorSupplier(final SectorCategories categories, final int year) {
    this.categories = categories;
    this.year = year;
  }

  @Override
  public Map<Substance, Double> getColdStartSpecificVehicleEmissionFactors(final String vehicleCode) {
    return getColdStartEmissionFactors(categories.getColdStartCategories().determineSpecificByCode(vehicleCode));
  }

  @Override
  public Map<Substance, Double> getColdStartStandardVehicleEmissionFactors(final String vehicleCode) {
    return getColdStartEmissionFactors(categories.getColdStartCategories().determineStandardByCode(vehicleCode));
  }

  private Map<Substance, Double> getColdStartEmissionFactors(final ColdStartSourceCategory category) {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);

    for (final Substance substance : Substance.values()) {
      final double emissionFactor = category.getEmissionFactor(substance, year);
      if (emissionFactor > 0) {
        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }
}
