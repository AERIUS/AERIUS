/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import nl.overheid.aerius.db.PMF;

/**
 * Abstract worker that can be used to obtain connections with the Database.
 * @param <T> The type of input.
 * @param <S> The type of output.
 */
public abstract class AbstractDBWorker<T extends Serializable, S extends Serializable> implements Worker<T, S> {

  private final PMF pmf;

  /**
   * @param pmf The persistence manager factory to use.
   */
  protected AbstractDBWorker(final PMF pmf) {
    this.pmf = pmf;
  }

  protected PMF getPMF() {
    return pmf;
  }

  protected Connection getConnection() throws SQLException {
    return pmf.getConnection();
  }
}
