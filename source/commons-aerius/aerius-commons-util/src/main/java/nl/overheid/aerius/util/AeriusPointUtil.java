/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;

/**
 * Utility class to encode the AERIUS point type into the name/id usable for model point files.
 *
 * Points are decoded by prefixing the point type. e.g. P123 or R342.
 * This is needed to make a distinction between receptor points and other points.
 */
public final class AeriusPointUtil {

  private static final char RECEPTOR_PREFIX = 'R';
  private static final char POINT_PREFIX = 'P';
  private static final char SUB_RECEPTOR_PREFIX = 'S';
  private static final char SUB_POINT_PREFIX = 'Z';
  private static final char SUB_SEPARATOR = '-';

  private AeriusPointUtil() {
    // Util class
  }

  /**
   * Decodes the {@link AeriusPointType} and the point id from the name.
   *
   * @param name decoded point name
   * @return tuple with point type and id
   */
  public static AeriusPoint decodeName(final String name) {
    final char firstChar = name.charAt(0);
    final AeriusPointType apt;
    final int idx;
    final int subIdx;

    switch (firstChar) {
    case RECEPTOR_PREFIX:
      apt = AeriusPointType.RECEPTOR;
      idx = 1;
      subIdx = -1;
      break;
    case SUB_RECEPTOR_PREFIX:
      apt = AeriusPointType.SUB_RECEPTOR;
      idx = 1;
      subIdx = name.indexOf(SUB_SEPARATOR);
      break;
    case SUB_POINT_PREFIX:
      apt = AeriusPointType.SUB_POINT;
      idx = 1;
      subIdx = name.indexOf(SUB_SEPARATOR);
      break;
    case POINT_PREFIX:
      apt = AeriusPointType.POINT;
      idx = 1;
      subIdx = -1;
      break;
    default:
      apt = AeriusPointType.POINT;
      idx = 0;
      subIdx = -1;
      break;
    }
    final AeriusPoint ap;

    if (apt.isSubReceptor()) {
      ap = new AeriusPoint(Integer.parseInt(name.substring(subIdx + 1)), apt);
      ap.setParentId(Integer.parseInt(idx == 0 ? name : name.substring(idx, subIdx)));
    } else {
      ap = new AeriusPoint(Integer.parseInt(idx == 0 ? name : name.substring(idx)), apt);
      ap.setParentId(0);
    }
    return ap;
  }

  /**
   * Encodes the point name given an {@link AeriusPoint}.
   *
   * @param point point to encode
   * @return encoded name
   */
  public static String encodeName(final AeriusPoint point) {
    final String prefix;
    if (point.getPointType() == AeriusPointType.POINT) {
      prefix = String.valueOf(POINT_PREFIX);
    } else if (point.getPointType() == AeriusPointType.SUB_RECEPTOR) {
      prefix = SUB_RECEPTOR_PREFIX + String.valueOf(point.getParentId()) + SUB_SEPARATOR;
    } else if (point.getPointType() == AeriusPointType.SUB_POINT) {
      prefix = SUB_POINT_PREFIX + String.valueOf(point.getParentId()) + SUB_SEPARATOR;
    } else {
      prefix = String.valueOf(RECEPTOR_PREFIX);
    }
    return prefix + String.valueOf(point.getId());
  }
}
