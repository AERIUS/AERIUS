/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.email.MailMessageData;

/**
 * Base class for workers that sent a message to the user.
 */
public abstract class AbstractMessageWorker extends AbstractDBWorker<MailMessageData, Boolean> {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractMessageWorker.class);

  private final String mailService;
  private final JobStateHelperAdapter jobStateHelper;
  private final JobStateHelperAdapter noJobJobStateHelper = new JobStateHelperAdapter() {};

  protected AbstractMessageWorker(final PMF pmf, final String mailService) {
    super(pmf);
    this.mailService = mailService;
    this.jobStateHelper = new JobStateHelper(pmf);
  }

  @Override
  public final Boolean run(final MailMessageData input, final JobIdentifier jobIdentifier,
      final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
    final String jobKey = jobIdentifier.getJobKey();
    LOG.info("Received message to sent via {} {}", mailService, jobKey);
    final JobStateHelperAdapter jsHelper = input.getEmailType().isJob() ? jobStateHelper : noJobJobStateHelper;

    try {
      jsHelper.failIfJobCancelledByUser(jobIdentifier);
      sendMessage(input, jobKey);
      jsHelper.setJobStatus(jobIdentifier.getJobKey(), JobState.COMPLETED);
      LOG.info("Successfully sent mail via {} {}", mailService, jobKey);
    } catch (final Exception e) {
      if (JobStateHelper.isNotCancelledJob(e)) {
        jsHelper.setErrorStateAndMessageJob(jobKey, input.getLocale(), e);
        LOG.warn("Sending an email via {} failed:", mailService, e);
      }
    }
    return Boolean.TRUE;
  }

  /**
   * Should do the actual delivery of the message to the user.
   *
   * @param input message input
   * @param jobKey unique key to identify the message
   * @throws Exception
   */
  protected abstract void sendMessage(MailMessageData input, String jobKey) throws Exception;
}
