/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.util.Locale;

import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Generic helper interface for JobStatus actions. Can be implemented with specific actions or use default implementation when no database actions
 * needed.
 */
public interface JobStateHelperAdapter {

  /**
   * Set the Job State to the new job state.
   * Default implementation does nothing.
   *
   * @param jobKey job key
   * @param jobState job state to set
   */
  default void setJobStatus(final String jobKey, final JobState jobState) {
    // Default nothing to do
  }

  /**
   * Set the job as failed (error) with the given original error message.
   * Default implementation does nothing.
   *
   * @param jobKey job key
   * @param locale locale of the message
   * @param originException original exception
   */
  default void setErrorStateAndMessageJob(final String jobKey, final Locale locale, final Exception originException) {
    // Default nothing to do
  }

  /**
   * Check is the job was cancelled/deleted and if so throws an {@link AeriusException}.
   * Default implementation does nothing.

   * @param jobIdentifier job identifier
   * @throws AeriusException exception thrown when job was cancelled/deleted.
   */
  default void failIfJobCancelledByUser(final JobIdentifier jobIdentifier) throws AeriusException {
    // Default nothing to do
  }
}
