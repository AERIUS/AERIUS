/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import nl.overheid.aerius.calculation.EngineVersion;

/**
 * Interface to preload engine model data.
 * This is used to load any needed data/executables needed by the engine model workers if this data is not already present.
 *
 * @param <V> Engine model version enum
 * @param <E> Engine model configuration type
 */
public interface ModelDataUtilPreloader<V extends EngineVersion, E extends EngineWorkerConfiguration<V>> {

  /**
   * Ensure the configured preloaded data is preloaded. Throws an exception if preload failed.
   *
   * @param config model configuration
   * @throws Exception error preloading data
   */
  void ensureModelDataPresence(final E config) throws Exception;
}
