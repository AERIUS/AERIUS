/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.util.List;
import java.util.Properties;

/**
 * Abstract base class for workers that need a database connection. This build handles the database connection configuration.
 * Only 1 database configuration is supported.
 */
public final class DatabaseConfigurationBuilder<C extends DatabaseConfiguration> extends ConfigurationBuilder<C> {

  private static final String DATABASE_PREFIX = "database";
  private static final String DATABASE_USERNAME_KEY = "username";
  private static final String DATABASE_PASSWORD_KEY = "password";
  private static final String DATABASE_URL_KEY = "url";

  public DatabaseConfigurationBuilder(final Properties properties) {
    super(properties, DATABASE_PREFIX);
  }

  @Override
  protected C create() {
    return (C) new DatabaseConfiguration();
  }

  @Override
  protected void build(final C configuration) {
    configuration.setDatabaseUrl(workerProperties.getProperty(DATABASE_URL_KEY));
    configuration.setDatabaseUsername(workerProperties.getProperty(DATABASE_USERNAME_KEY));
    configuration.setDatabasePassword(workerProperties.getProperty(DATABASE_PASSWORD_KEY));
  }

  @Override
  protected final List<String> validate(final C configuration) {
    final ConfigurationValidator validator = new ConfigurationValidator();

    validator.validateRequiredProperty(workerProperties.getFullPropertyName(DATABASE_URL_KEY), configuration.getDatabaseUrl());
    validator.validateRequiredProperty(workerProperties.getFullPropertyName(DATABASE_USERNAME_KEY), configuration.getDatabaseUsername());
    validator.validateRequiredProperty(workerProperties.getFullPropertyName(DATABASE_PASSWORD_KEY), configuration.getDatabasePassword());
    return validator.getValidationErrors();
  }
}
