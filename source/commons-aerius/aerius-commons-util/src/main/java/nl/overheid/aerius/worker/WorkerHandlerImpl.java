/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.Serializable;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanKind;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.otel.TracerUtil;
import nl.overheid.aerius.processmonitor.ProcessMonitorService;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Abstract class handling the worker call. This class wraps the actual call in the
 * {@link #handleWorkLoad(Serializable,String, WorkerIntermediateResultSender)} and takes care of a number of worker specific error handling.
 * Subclasses should implement the actual handling of the data in {@link #run(Serializable, String, WorkerIntermediateResultSender)} which is typed.
 * @param <S> The input class
 * @param <T> The output class
 */
public final class WorkerHandlerImpl<S extends Serializable, T extends Serializable> implements WorkerHandler {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerHandlerImpl.class);
  private final Worker<S, T> worker;
  private final WorkerType workerType;
  private final ProcessMonitorService processMonitorService;

  public WorkerHandlerImpl(final Worker<S, T> worker, final WorkerType workerType, final ProcessMonitorService processMonitorService) {
    this.worker = worker;
    this.workerType = workerType;
    this.processMonitorService = processMonitorService;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Serializable handleWorkLoad(final Serializable input, final String correlationId,
      final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
    final JobIdentifier jobIdentifier = new JobIdentifier(correlationId);

    WorkerThreadUtil.setThreadName("Worker", workerType.name(), jobIdentifier.getLocalId());
    final Span span = TracerUtil.getSpanBuilder(correlationId, "aerius." + workerType.name().toLowerCase(Locale.ROOT), SpanKind.CONSUMER).startSpan();
    Serializable result;

    try {
      if (processMonitorService.isCancelledJob(jobIdentifier.getJobKey())) {
        throw new AeriusException(AeriusExceptionReason.CONNECT_JOB_CANCELLED);
      }
      log("Starting {}", jobIdentifier);
      result = worker.run((S) input, jobIdentifier, workerIntermediateResultSender);
      if (result instanceof final Exception e) {
        TracerUtil.setException(span, e);
      }
      log("Finished {}", jobIdentifier);
    } catch (final AeriusException e) {
      if (e.isInternalError()) {
        LOG.warn("Job crashed with AeriusException, {}", jobIdentifier, e);
      } else {
        LOG.info("Job stopped with AeriusException, {}: {}", jobIdentifier, e.getMessage());
      }
      result = e;
      TracerUtil.setException(span, e);
    } catch (final ClassCastException e) { // before catching Runtime as this is a RuntimeException, but handled as fatal error.
      LOG.error("Job crashed with ClassCastException, {}", jobIdentifier, e);
      TracerUtil.setException(span, e);
      throw e;
    } catch (final RuntimeException e) {
      LOG.warn("Job crashed with RuntimeException '{}', {}", e.getMessage(), jobIdentifier, e);
      TracerUtil.setException(span, e);
      result = new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    } catch (final RequeueTaskException e) {
      TracerUtil.setException(span, e);
      throw e;
    } finally {
      span.end();
      WorkerThreadUtil.setThreadName("Worker", workerType.name(), "IDLE");
    }
    return result;
  }

  private void log(final String message, final JobIdentifier jobIdentifier) {
    if (worker.logStartStop()) {
      LOG.info(message, jobIdentifier);
    } else {
      LOG.debug(message, jobIdentifier);
    }
  }
}
