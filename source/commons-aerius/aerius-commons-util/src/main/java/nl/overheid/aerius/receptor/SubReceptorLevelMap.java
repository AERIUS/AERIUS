/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import nl.overheid.aerius.function.AeriusFunction;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Special Map to store generic subreceptor data.
 * It stores different number of subreceptors per level.
 * Sub classes are to be created to set the specific generic classes required for this class to make the class easier to use.
 */
class SubReceptorLevelMap<P extends Point, C, T extends SubReceptorUtil<P, C>> {

  // Sorted by distance map of level number to distance/coordinates
  private final Map<Integer, LevelDistance<C>> level2DistanceMap = new LinkedHashMap<>();
  private final T subReceptorUtil;

  protected SubReceptorLevelMap(final T subReceptorUtil) {
    this.subReceptorUtil = subReceptorUtil;
  }

  void setList(final List<LevelDistance<C>> list) {
    for (final LevelDistance<C> levelDistance : list.stream().sorted().collect(Collectors.toList())) {
      level2DistanceMap.put(levelDistance.getLevel(), levelDistance);
    }
  }

  /**
   * Returns the sub receptors level based on the distance.
   * The level is used to determine the number of subreceptors inside a hexagon.
   * @param distanceToCheck source - receptor distance to determine level
   * @return the subreceptors level associated with the source - receptor distance
   */
  public int findLevel(final double distanceToCheck) {
    for (final Entry<Integer, LevelDistance<C>> subLevel : level2DistanceMap.entrySet()) {
      if (distanceToCheck < subLevel.getValue().getDistance()) {
        return subLevel.getKey();
      }
    }
    return 0;
  }

  /**
   * Computes the subreceptor points relative to the given original center point of the hexagon.
   * The level is used to determine the number of subreceptors.
   * An optional filter can be given to filter out subreceptors within a certain distance of sources. If no filter is given all points are added.
   * If a filter is given all points that return true by the filter will be added.
   *
   * @param original original center point of the hexaon
   * @param level level to determine number of subreceptors.
   * @param filter Optional filter to filter out points within a certain distance of sources.
   * @return List of subreceptors points relative to the original hexagon
   * @throws AeriusException
   */
  public List<P> computePoints(final P original, final int level, final AeriusFunction<P, Boolean> filter) throws AeriusException {
    if (!level2DistanceMap.containsKey(level)) {
      throw new IllegalArgumentException("Given level '" + level + "' not available.");
    }
    return subReceptorUtil.computePoints(original, level2DistanceMap.get(level).getCoordinates(), filter, level);
  }

  private static class LevelDistance<C> implements Comparable<LevelDistance<C>> {
    private final int level;
    private final double distance;
    private final C coordinates;

    public LevelDistance(final int level, final double distance, final C coordinates) {
      this.level = level;
      this.distance = distance;
      this.coordinates = coordinates;
    }

    public int getLevel() {
      return level;
    }

    public double getDistance() {
      return distance;
    }

    public C getCoordinates() {
      return coordinates;
    }

    @Override
    public int compareTo(final LevelDistance<C> o) {
      return Double.compare(distance, o.distance);
    }
  }

  /**
   * Abstract builder class to create the specific {@link SubReceptorLevelMap} in which all levels are initialized.
   */
  public abstract static class Builder<P extends Point, C, T extends SubReceptorUtil<P, C>, H extends SubReceptorLevelMap<P, C, T>> {
    private final List<LevelDistance<C>> list = new ArrayList<>();

    protected void add(final int level, final double distance, final C coordinates) {
      list.add(new LevelDistance<>(level, distance, coordinates));
    }

    protected abstract H create();

    public final H build() {
      final H map = create();

      map.setList(list);
      return map;
    }
  }
}
