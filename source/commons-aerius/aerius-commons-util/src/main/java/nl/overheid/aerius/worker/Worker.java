/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.Serializable;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;

/**
 * Interface for a worker which takes typed input and returns typed output.
 */
public interface Worker<S extends Serializable, T extends Serializable> {

  /**
   * Runs the worker process. It gets typed input. This call is not thread safe. Meaning multiple calls can be done by different threads. This means
   * the implementing class should not set call specific data on this call.
   * @param input worker input
   * @param jobIdentifier The job identifier of the message
   * @param workerIntermediateResultSender handler to sent intermediate results back
   * @return worker output
   * @throws Exception throws exception in case of problems
   */
  T run(S input, JobIdentifier jobIdentifier, WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception;

  /**
   * Returns true if start/stop of a worker process should be logged to INFO.
   */
  default boolean logStartStop() {
    return true;
  }
}
