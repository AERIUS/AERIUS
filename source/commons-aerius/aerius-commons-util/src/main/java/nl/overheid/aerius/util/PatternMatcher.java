/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Simplified pattern matching for strings.
 * <br>
 * <ul>
 * <li>`*` denotes a wildcard matching 0 or more characters</li>
 * <li>`,` denotes alternative patterns</li>
 * </ul>
 * <br>
 * Examples:
 * <ul>
 * <li>`*` always matches</li>
 * <li>`2021.*` matches any string starting with the prefix `2021.`</li>
 * <li>`2022,2023` matches the strings `2022` and `2023`</li>
 * </ul>
 */
public class PatternMatcher {

  private static final String CHAR_ALTERNATIVE = ",";
  private static final String CHAR_WILDCARD = "*";
  private static final String REGEX_ANY_MATCH = ".*";

  private final List<Pattern> regexPatterns;

  /**
   * Construct a pattern matcher
   * @param patterns according to specification see class documentation
   */
  public PatternMatcher(final String patterns) {
    this.regexPatterns = Arrays.stream(patterns.split(CHAR_ALTERNATIVE))
        .filter(s -> !s.isEmpty())
        .map(PatternMatcher::patternToRegex)
        .collect(Collectors.toList());
  }

  private static Pattern patternToRegex(final String pattern) {

    // String.split(String, -1) ensures trailing white space is retained, preventing trailing wildcards to be removed
    // See https://docs.oracle.com/javase/6/docs/api/java/lang/String.html#split%28java.lang.String,%20int%29
    final Stream<String> betweenWildcards = Arrays.stream(pattern.split(Pattern.quote(CHAR_WILDCARD), -1));
    final String regexPattern = betweenWildcards.map(s -> {
          if ("".equals(s)) {
            return "";
          } else {
            // Prevent unwanted regex special characters in pattern
            return Pattern.quote(s);
          }
        }
    ).collect(Collectors.joining(REGEX_ANY_MATCH));

    return Pattern.compile(regexPattern);
  }

  /**
   * Returns the string representation of the converted regex patterns
   *
   * @return list of regex strings
   */
  public List<String> getRegexPatterns() {
    return regexPatterns.stream().map(Pattern::pattern).collect(Collectors.toList());
  }

  /**
   * Check the needle against one of the patterns
   *
   * @param needle the string to look for
   * @return whether the needle matches one of the patterns
   */
  public boolean matches(final String needle) {
    if (needle == null) {
      return false;
    }
    return regexPatterns.stream()
        .map(Pattern::asMatchPredicate)
        .anyMatch(p -> p.test(needle));
  }
}
