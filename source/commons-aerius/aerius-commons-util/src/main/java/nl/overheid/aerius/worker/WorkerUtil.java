/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Utility class for workers to use.
 */
public final class WorkerUtil {

  private static final Logger LOGGER = LoggerFactory.getLogger(WorkerUtil.class);

  private static final String FOLDER_NAME = "aerius_jobs";

  private WorkerUtil() {
    // Util class
  }

  /**
   * Utility method to get a directory for a specific job.
   */
  public static File getFolderForJob(final JobIdentifier jobIdentifier) {
    final File folderForJob = getFolderForJobLocation(jobIdentifier.getLocalId());

    if (folderForJob.exists()) {
      // folder was already created earlier on.
    } else if (!folderForJob.mkdirs()) {
      throw new IllegalArgumentException("Could not create temp directories needed to write job files to: " + folderForJob);
    }
    return folderForJob;
  }

  private static File getFolderForJobLocation(final String localId) {
    return Paths.get(System.getProperty("java.io.tmpdir"), FOLDER_NAME, localId).toFile();
  }

  /**
   * Utility method to automatically fail if a job is already cancelled by the user.
   */
  public static void failIfJobCancelledByUser(final PMF pmf, final String jobKey) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      failIfJobCancelledByUser(con, jobKey);
    } catch (final SQLException e) {
      LOGGER.debug("SQLException while getting job progress for: {}", jobKey, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  /**
   * Utility method to automatically fail if a job is already cancelled by the user.
   */
  public static void failIfJobCancelledByUser(final Connection con, final String jobKey) throws AeriusException {
    JobProgress jobProgress = null;
    try {
      jobProgress = JobRepository.getProgress(con, jobKey);
    } catch (final SQLException e) {
      LOGGER.debug("SQLException while getting job progress for: {}", jobKey, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    if (jobProgress != null && (jobProgress.getState() == JobState.CANCELLED || jobProgress.getState() == JobState.DELETED)) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_JOB_CANCELLED);
    }
  }
}
