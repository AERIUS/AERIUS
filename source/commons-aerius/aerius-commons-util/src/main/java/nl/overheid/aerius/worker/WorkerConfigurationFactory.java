/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.util.Properties;

/**
 * Interface for worker configuration builders.
 *
 * @param <C> specific configuration class
 */
public interface WorkerConfigurationFactory<C extends WorkerConfiguration> {
  /**
   * Returns a worker specific configuration builder given a properties object.
   * @param properties properties object with worker configuration
   * @return worker specific configuration builder.
   */
  ConfigurationBuilder<C> configurationBuilder(Properties properties);

  /**
   * Called when the Worker shuts down.
   */
  default void shutdown() {
  }
}
