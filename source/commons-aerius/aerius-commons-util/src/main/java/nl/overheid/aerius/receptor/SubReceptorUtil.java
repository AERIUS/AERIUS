/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */package nl.overheid.aerius.receptor;

 import java.util.List;

import nl.overheid.aerius.function.AeriusFunction;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;

 /**
  * Interface for utility classes computing the sub receptors points for a given hexagon receptor point.
  */
 interface SubReceptorUtil<P extends Point, C> {

   /**
    * Returns the sub receptor points around the given original point. The level determines the number of sub receptor points to calculate.
    * The way the sub receptor points are organized with the hexagon is implementation dependent.
    *
    * @param original original point representing the center of the hexagon
    * @param coordinates of points to use as offset points to compute subreceptors
    * @param filter if filter null or filter returns true subreceptor point will be returned, otherwise subreceptor point will not be added
    * @param level The sub level for which the points are made.
    * @return List of all sub receptor points
    * @throws AeriusException
    * @throws IllegalArgumentException if the level is out of range
    */
   List<P> computePoints(P original, C coordinates, AeriusFunction<P, Boolean> filter, int level) throws AeriusException;
 }
