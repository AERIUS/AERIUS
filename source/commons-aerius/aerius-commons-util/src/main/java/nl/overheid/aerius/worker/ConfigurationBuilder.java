/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

/**
 * Abstract class to build worker configuration objects.
 * @param <T> type of the specific configuration object
 */
public abstract class ConfigurationBuilder<T> {

  private final List<ConfigurationBuilder<T>> builders = new ArrayList<>();
  private final List<String> validations = new ArrayList<>();

  protected final WorkerProperties workerProperties;

  public ConfigurationBuilder(final Properties properties, final String prefix) {
    workerProperties = new WorkerProperties(properties, prefix);
    addBuilder(this);
  }

  public final void addBuilder(final ConfigurationBuilder<T> builder) {
    builders.add(builder);
  }

  /**
   * @return returns true if this configuration is configured at all by the user.
   * If not configured no need to try to build other options.
   */
  protected boolean isActive() {
    return true;
  }

  /**
   * Builds the configuration if active and performs the validation of the properties.
   * Validation results can be retrieved via {@link #getValidationErrors()}.
   * @return Optional of configuration or empty optional if not active
   */
  public final Optional<T> buildAndValidate() {
    if (isActive()) {
      final T configuration = create();

      builders.forEach(b -> b.build(configuration));
      validations.addAll(builders.stream().flatMap(b -> b.validate(configuration).stream()).toList());
      return Optional.of(configuration);
    } else {
      return Optional.empty();
    }
  }

  /**
   * Implementations should creates the actual database configuration subclass and initialize worker specific configurations in this method.
   *
   * @return new configuration
   */
  protected abstract T create();

  /**
   * Abstract method that should implement the building of the configuration file.
   * @return configuration
   */
  protected abstract void build(T configuration);

  /**
   * Method that should do the validation of the configuration and return any validation errors or an empty list.
   *
   * @param configuration configuration to validate
   * @return list of validation errors or empty list
   */
  protected List<String> validate(final T configuration) {
    return List.of();
  }

  /**
   * @return Returns the validation errors or an empty list if there are no validation errors.
   */
  public List<String> getValidationErrors() {
    return validations;
  }
}
