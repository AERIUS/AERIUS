/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.util.List;

import nl.overheid.aerius.calculation.EngineVersion;

/**
 * Configuration base class for Engine workers.
 *
 * @param <V> EngineVersion
 */
public class EngineWorkerConfiguration<V extends EngineVersion> extends WorkerConfiguration {

  /**
   * Root directory for model data and executable files.
   */
  private File versionsRoot;

  /**
   * Directory to create temporary directory in to store input files to pass to the engine executable.
   */
  private File runFilesDirectory;
  /**
   * If true doesn't delete the generated input and output files after engine run.
   */
  private boolean keepGeneratedFiles;
  private List<V> modelPreloadVersions;
  private String modelDataUrl;

  public EngineWorkerConfiguration(final int processes) {
    super(processes);
  }

  public File getVersionsRoot() {
    return versionsRoot;
  }

  public void setVersionsRoot(final File versionsRoot) {
    this.versionsRoot = versionsRoot;
  }

  /**
   * @return The directory where the files used by OPS can be (temporarily) stored.
   */
  public File getRunFilesDirectory() {
    return runFilesDirectory;
  }

  public void setRunFilesDirectory(final File runFilesDirectory) {
    this.runFilesDirectory = runFilesDirectory;
  }

  /**
   * @return If true doesn't delete the generated input and output files after a run.
   */
  public boolean isKeepGeneratedFiles() {
    return keepGeneratedFiles;
  }

  public void setKeepGeneratedFiles(final boolean keepGeneratedFiles) {
    this.keepGeneratedFiles = keepGeneratedFiles;
  }

  public List<V> getModelPreloadVersions() {
    return modelPreloadVersions;
  }

  public void setModelPreloadVersions(final List<V> modelPreloadVersions) {
    this.modelPreloadVersions = modelPreloadVersions;
  }

  public String getModelDataUrl() {
    return modelDataUrl;
  }

  public void setModelDataUrl(final String modelDataUrl) {
    this.modelDataUrl = modelDataUrl;
  }
}
