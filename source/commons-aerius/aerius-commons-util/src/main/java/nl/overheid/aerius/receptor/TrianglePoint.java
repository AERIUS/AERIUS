/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import java.math.BigDecimal;

/**
 * Data class to store center x-y and triangle coordinates of a sub receptor equilateral triangle.
 */
class TrianglePoint {
  private final int id;
  private final BigDecimal x;
  private final BigDecimal y;
  private final BigDecimal[][] triangleCoordinates;

  public TrianglePoint(final int id, final BigDecimal x, final BigDecimal y, final BigDecimal[][] triangleCoordinates) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.triangleCoordinates = triangleCoordinates;
  }

  public int getId() {
    return id;
  }

  public BigDecimal getX() {
    return x;
  }

  public BigDecimal getY() {
    return y;
  }

  public BigDecimal[][] getTriangleCoordinates() {
    return triangleCoordinates;
  }
}
