/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Validator to keep track of worker configuration validation results and add new validation results.
 * When all validations are done the validations can be retrieved via {@link #getValidationErrors()}.
 */
public class ConfigurationValidator {

  private final List<String> validations = new ArrayList<>();

  /**
   * @return Return the validation issues added
   */
  public List<String> getValidationErrors() {
    return validations;
  }

  /**
   * Validate if the property key isn't empty and is a valid directory and if writable is true it checks if the directory is writable. If any of these
   * conditions are not met this is stored in the reasons list.
   *
   * @param key property key to check
   * @param directory The directory derived from the key property.
   * @param writable checks if the directory is writable by this process.
   */
  public void validateDirectory(final String key, final File directory, final boolean writable) {
    if (validateRequiredProperty(key, directory)) {
      if (directory.isDirectory()) {
        if (writable && !directory.canWrite()) {
          validations.add(String.format("Directory configured in property '%s' is not writable: %s", key, directory));
        } else if (!directory.canRead()) {
          validations.add(String.format("Directory configured in property '%s' is not readable: %s", key, directory));
        }
      } else {
        validations.add(String.format("Property '%s' is not a valid directory: %s", key, directory));
      }
    }
  }

  /**
   * Validate if the required property is set. If set the method returns true
   * @param key key to validate
   * @param validations add error to list of reasons in case the property isn't set
   * @return true if property is valid
   */
  public boolean validateRequiredProperty(final String key, final Object value) {
    return validateRequiredProperty(key, "Required property '%s' is not set.", value);
  }

  /**
   * Validate if the required property is set. If set the method returns true
   * @param key key to validate
   * @param message message to give to the user about the invalidated property, should include a %s to include the key in the message
   * @param validations add error to list of reasons in case the property isn't set
   * @return true if property is valid
   */
  public boolean validateRequiredProperty(final String key, final String message, final Object value) {
    if (value == null || (value instanceof final String s && s.isBlank())) {
      validations.add(String.format(message, key));
      return false;
    }
    return true;
  }

}
