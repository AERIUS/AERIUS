/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.processmonitor.ProcessMonitorService;
import nl.overheid.aerius.util.HttpClientManager;

/**
 * Interface to external connection providers used within a worker.
 */
public interface WorkerConnectionHelper {

  /**
   * @return Returns the RabbitMQ broker connection factory.
   */
  BrokerConnectionFactory getBrokerConnectionFactory();

  /**
   * @return Returns a http client manager.
   */
  HttpClientManager getHttpClientManager();

  /**
   * @return Returns the {@link ProcessMonitorService}.
   */
  ProcessMonitorService getProcessMonitorService();

  /**
   * Creates a new {@link FileServerProxy} object.
   *
   * @param config configuration to use to create object.
   * @return new object.
   */
  default FileServerProxy createFileServerProxy(final WorkerConfiguration config) {
    return new FileServerProxy(getHttpClientManager(), config.getFileServerInternalUrl(), config.getFileServerExternalUrl(),
        config.getFileServerTimeout());
  }

  /**
   * @return creates a new {@link TaskManagerClientSender}.
   */
  default TaskManagerClientSender createTaskManagerClient() {
    return new TaskManagerClientSender(getBrokerConnectionFactory());
  }

  /**
   * @return Returns a database connection factory. For workers without a database connection this returns null.
   */
  PMF getPMF();

  /**
   * Set the database connection factories.
   * @param pmf the database connection factory
   */
  default void setPmf(final PMF pmf) {}
}
