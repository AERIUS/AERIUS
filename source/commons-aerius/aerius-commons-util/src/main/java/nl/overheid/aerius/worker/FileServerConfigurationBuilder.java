/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Properties;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Configuration builder for file server configuration properties.
 */
public class FileServerConfigurationBuilder<T extends WorkerConfiguration> extends ConfigurationBuilder<T> {

  private static final Logger LOG = LoggerFactory.getLogger(FileServerConfigurationBuilder.class);

  private static final String FILE_SERVER_PREFIX = "fileserver";
  private static final String FILE_SERVER_INTERNAL_URL_KEY = "internal.url";
  private static final String FILE_SERVER_EXTERNAL_URL_KEY = "external.url";
  private static final String FILE_SERVER_EXTERNAL_PATH_PREFIX = "/api/v8/files";

  private static final String FILE_SERVER_TIMEOUT_KEY = "timeout";
  private static final int FILE_SERVER_TIMEOUT_DEFAULT = 60;

  public FileServerConfigurationBuilder(final Properties properties) {
    super(properties, FILE_SERVER_PREFIX);
  }

  @Override
  protected T create() {
    throw new IllegalStateException("FileServerConfigurationBuilder should not be used as separated configuration.");
  }

  @Override
  protected boolean isActive() {
    return false;
  }

  @Override
  protected void build(final T configuration) {
    configuration.setFileServerInternalUrl(constructUrl(FILE_SERVER_INTERNAL_URL_KEY, ""));
    configuration.setFileServerExternalUrl(constructUrl(FILE_SERVER_EXTERNAL_URL_KEY, FILE_SERVER_EXTERNAL_PATH_PREFIX));
    configuration.setFileServerTimeout(workerProperties.getPropertyOrDefault(FILE_SERVER_TIMEOUT_KEY, FILE_SERVER_TIMEOUT_DEFAULT));
  }

  private String constructUrl(final String key, final String path) {
    final String url = workerProperties.getProperty(key);

    try {
      return url == null ? null : new URIBuilder(url + path).toString();
    } catch (final URISyntaxException e) {
      LOG.error("Could not parse property '{}':", workerProperties.getFullPropertyName(key), e);
      return null;
    }
  }

  @Override
  protected List<String> validate(final T configuration) {
    final ConfigurationValidator validator = new ConfigurationValidator();

    validator.validateRequiredProperty(workerProperties.getFullPropertyName(FILE_SERVER_INTERNAL_URL_KEY), configuration.getFileServerInternalUrl());
    validator.validateRequiredProperty(workerProperties.getFullPropertyName(FILE_SERVER_EXTERNAL_URL_KEY), configuration.getFileServerExternalUrl());
    return validator.getValidationErrors();
  }
}
