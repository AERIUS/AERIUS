/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import nl.overheid.aerius.function.AeriusFunction;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Class to compute equilateral triangles center points of a hexagon.
 */
class HexagonEquilateralTrianglesUtil<P extends AeriusPoint> implements SubReceptorUtil<P, TrianglePoint[]> {

  private static final int ROUNDING_SCALE = 4;

  private final BiFunction<P, Integer, P> createPointFunction;

  public HexagonEquilateralTrianglesUtil(final BiFunction<P, Integer, P> createPointFunction) {
    this.createPointFunction = createPointFunction;
  }

  /**
   * Computes the equilateral triangles center points around the given original point, with points for the given level.
   *
   * @param original original point representing the center of the hexagon
   * @param trianglePoints generic coordinates of the equilateral triangles to map to sources
   * @param filter filter tests if point should be added
   * @param level the level for which the points are made.
   * @return List of all equilateral triangles center points
   * @throws AeriusException
   */
  @Override
  public List<P> computePoints(final P original, final TrianglePoint[] trianglePoints, final AeriusFunction<P, Boolean> filter, final int level)
      throws AeriusException {
    final List<P> points = new ArrayList<>();

    for (final TrianglePoint trianglePoint : trianglePoints) {
      conditionalAdd(points, createPoint(original, trianglePoint, level), filter);
    }
    return points;
  }

  private void conditionalAdd(final List<P> points, final P subReceptorPoint, final AeriusFunction<P, Boolean> filter) throws AeriusException {
    if (filter == null || filter.apply(subReceptorPoint)) {
      points.add(subReceptorPoint);
    }
  }

  private P createPoint(final P original, final TrianglePoint trianglePoint, final int level) {
    final P point = createPointFunction.apply(original, level);
    final BigDecimal x = BigDecimal.valueOf(original.getX());
    final BigDecimal y = BigDecimal.valueOf(original.getY());

    point.setId(trianglePoint.getId());
    point.setX(round(x.add(trianglePoint.getX())));
    point.setY(round(y.add(trianglePoint.getY())));
    if (point instanceof final IsSubPoint subPoint && point.getPointType().isSubReceptor()) {
      subPoint.setArea(createTriangleGeometry(x, y, trianglePoint.getTriangleCoordinates()));
    }
    return point;
  }

  private static Polygon createTriangleGeometry(final BigDecimal x, final BigDecimal y, final BigDecimal[][] triangleCoordinates) {
    final Polygon geometry = new Polygon();
    final double[][][] coordinates = new double[1][4][2];

    for (int i = 0; i < triangleCoordinates.length; i++) {
      coordinates[0][i][0] = round(x.add(triangleCoordinates[i][0]));
      coordinates[0][i][1] = round(y.add(triangleCoordinates[i][1]));
    }
    geometry.setCoordinates(coordinates);
    return geometry;
  }

  private static double round(final BigDecimal value) {
    return value.setScale(ROUNDING_SCALE, RoundingMode.HALF_UP).doubleValue();
  }
}
