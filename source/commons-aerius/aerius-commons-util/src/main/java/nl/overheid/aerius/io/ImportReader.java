/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.io;

import java.io.IOException;
import java.io.InputStream;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface for readers used to import in a generic way specific data files that contain AERIUS input data.
 * The classes are used first by calling detect which determines if the input is
 */
public interface ImportReader {

  /**
   * Detect if the given input can be read by this import reader. This is done based on the input filename.
   * @param filename name of the file to check
   * @return true if this import reader can read the file
   */
  boolean detect(String filename);

  /**
   * Reads the file from the input stream and returns the result in a {@link ImportParcel} object.
   * @param filename name of the file b
   * @param inputStream the input stream containing the data
   * @param categories Sector categories
   * @param substance if the input is for a specific substance it's this substance
   * @oaram importParcel data object to append results and errors/warnings.
   * @throws IOException in case of problems with reading the file
   * @throws AeriusException data error
   */
  void read(String filename, InputStream inputStream, SectorCategories categories, Substance substance, ImportParcel importParcel)
      throws IOException, AeriusException;
}
