/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.util.AeriusPointUtil;

/**
 * Class to aggregate results of sub receptors points inside a hexagon to a single value (per EmissionResultKey) for the whole hexagon.
 */
public abstract class AbstractSubReceptorAggregator<R extends AeriusResultPoint, P extends AeriusPoint> {

  private final PointTypeResults<R, P> receptorResults;
  private final PointTypeResults<R, P> pointResults;

  private final Set<EmissionResultKey> erks;
  private final boolean excludeZeroResult;

  protected AbstractSubReceptorAggregator(final Collection<P> receptors, final Set<EmissionResultKey> erks, final boolean excludeZeroResult) {
    this(receptors, erks, excludeZeroResult, p -> (R) new AeriusResultPoint(p));
  }

  /**
   * Initializes the aggregator with the receptors.
   * It filters out all {@link IsSubPoint} points as those points are only relevant for subreceptors.
   *
   * @param receptors all receptors
   * @param erks emission result keys for which results are requested.
   * @param excludeZeroResult Whether or not to exclude receptors where all results are exactly equal to 0
   */
  protected AbstractSubReceptorAggregator(final Collection<P> receptors, final Set<EmissionResultKey> erks, final boolean excludeZeroResult,
      final Function<P, R> createResultPointFunction) {
    this.erks = erks;
    this.excludeZeroResult = excludeZeroResult;
    receptorResults = new PointTypeResults<>(createResultPointFunction);
    pointResults = new PointTypeResults<>(createResultPointFunction);
    for (final P point : receptors) {
      if (point.getPointType() == AeriusPointType.SUB_RECEPTOR) {
        receptorResults.putSubReceptor(point);
      } else if (point.getPointType() == AeriusPointType.RECEPTOR) {
        receptorResults.putPoint(point);
      } else if (point.getPointType() == AeriusPointType.SUB_POINT) {
        pointResults.putSubReceptor(point);
      } else if (point.getPointType() == AeriusPointType.POINT) {
        pointResults.putPoint(point);
      }
    }
  }

  /**
   * Given the results, aggregate the subreceptors to a single hexagon result.
   * It calculates the averaged value for the hexagon based on the results of the sub receptors.
   *
   * @param results calculated results
   * @return Returns enriched results with averaged subreceptor receptor results.
   */
  public List<R> aggregate(final List<R> results) {
    final Results<R> collectedResults = collectSubReceptors(results);

    return Stream.concat(collectedResults.receptors().collect().stream(), collectedResults.points().collect().stream()).toList();
  }

  /**
   * Combine the results as needed by the particular model.
   * Up to implementation how these results are combined (should all results be returned, or should subreceptor results be filtered out).
   * When subreceptor results need to be stored in the database these need to be returned here.
   * When subreceptors are only needed to get aggregate results the subreceptors should not be returned.
   *
   * @param aggregatedReceptorResults The aggregated results from the sub receptors
   * @param subReceptorResults the subreceptor results
   * @return The combined points as required by the implementing subclass
   */
  protected abstract Collection<R> combine(Collection<R> aggregatedReceptorResults, final Collection<R> subReceptorResults);

  private Results<R> collectSubReceptors(final List<R> results) {
    final Results<R> groupedResults = new Results<>();

    for (final R result : results) {
      switch (result.getPointType()) {
      case SUB_RECEPTOR:
        groupedResults.receptors().addSubReceptorResult(result);
        break;
      case SUB_POINT:
        groupedResults.points().addSubReceptorResult(result);
        break;
      case RECEPTOR:
        groupedResults.receptors().addReceptorResult(result);
        break;
      case POINT:
        groupedResults.points().addReceptorResult(result);
        break;
      }
    }
    return groupedResults;
  }

  private static final class PointTypeResults<R extends AeriusResultPoint, P extends AeriusPoint> {
    private final Function<P, R> createResultPointFunction;
    // key: subreceptor id , value: parent id
    private final Map<String, Integer> sub2Hexagon = new HashMap<>();
    // key: parentId, value: instance of receptor result
    private final Map<Integer, R> point2Hexagon = new HashMap<>();

    public PointTypeResults(final Function<P, R> createResultPointFunction) {
      this.createResultPointFunction = createResultPointFunction;
    }

    public void putSubReceptor(final AeriusPoint point) {
      sub2Hexagon.put(AeriusPointUtil.encodeName(point), point.getParentId());
    }

    public void putPoint(final P point) {
      point2Hexagon.put(point.getId(), createResultPointFunction.apply(point));
    }

    public Collection<R> values() {
      return point2Hexagon.values();
    }

    public R getResultPoint(final Integer id) {
      return point2Hexagon.get(id);
    }

    public Integer getPointFromSubPoint(final AeriusPoint point) {
      return sub2Hexagon.get(AeriusPointUtil.encodeName(point));
    }
  }

  private static class SumResult<A extends AeriusResultPoint> {
    private int count;
    private int sumCount;
    private final EmissionResults emissionResult = new EmissionResults();
    private final boolean excludeZeroResult;

    SumResult(final boolean excludeZeroResult) {
      this.excludeZeroResult = excludeZeroResult;
    }

    public void add(final Set<EmissionResultKey> erks, final A result) {
      count++;
      if (excludeZeroResult && !result.hasAnyResult()) {
        return;
      }
      sumCount++;
      setResults(erks, result);
    }

    public void set(final Set<EmissionResultKey> erks, final A result) {
      count++;
      setResults(erks, result);
    }

    private void setResults(final Set<EmissionResultKey> erks, final A result) {
      for (final EmissionResultKey erk : erks) {
        if (result.getEmissionResults().hasResult(erk)) {
          emissionResult.add(erk, result.getEmissionResult(erk));
        }
      }
    }

    public void setSum(final Set<EmissionResultKey> erks, final A resultPoint) {
      erks.stream().filter(emissionResult::hasResult).forEach(erk -> {
        final double value = emissionResult.get(erk);

        resultPoint.setEmissionResult(erk, Double.isNaN(value) || sumCount == 0
            ? value
            : BigDecimal.valueOf(value).divide(BigDecimal.valueOf(sumCount), RoundingMode.HALF_UP).doubleValue());
      });
      resultPoint.setNumberOfSubPointsUsed(count);
    }
  }

  private final class Results<A extends AeriusResultPoint> {
    private final SubReceptorResults receptorSubResults;
    private final SubReceptorResults pointSubResults;

    public Results() {
      receptorSubResults = new SubReceptorResults(receptorResults);
      pointSubResults = new SubReceptorResults(pointResults);
    }

    public SubReceptorResults points() {
      return pointSubResults;
    }

    public SubReceptorResults receptors() {
      return receptorSubResults;
    }
  }

  private final class SubReceptorResults {
    private final Collection<R> subPointResults = new ArrayList<>();
    private final Map<Integer, SumResult<R>> pointSumResults = new HashMap<>();
    private final PointTypeResults<R, P> allResults;

    public SubReceptorResults(final PointTypeResults<R, P> allResults) {
      this.allResults = allResults;
    }

    public void addSubReceptorResult(final R result) {
      subPointResults.add(result);
      pointSumResults.computeIfAbsent(allResults.getPointFromSubPoint(result), idx -> new SumResult<>(excludeZeroResult)).add(erks, result);
    }

    public void addReceptorResult(final R result) {
      pointSumResults.computeIfAbsent(result.getId(), idx -> new SumResult<>(excludeZeroResult)).set(erks, result);
    }

    public Collection<R> collect() {
      pointSumResults.forEach((id, sr) -> sr.setSum(erks, allResults.getResultPoint(id)));

      return combine(allResults.values(), subPointResults);
    }
  }
}
