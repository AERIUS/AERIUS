/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineVersion;

/**
 * Abstract configuration builder with common configuration settings handling for engine workers.
 */
public abstract class EngineWorkerConfigurationBuilder<V extends EngineVersion, E extends EngineWorkerConfiguration<V>>
    extends ConfigurationBuilder<E> {

  protected static final String ROOT = "root";
  protected static final String MODEL_PRELOAD_VERSIONS = "model.preload.versions";

  private static final String RUNFILES_DIRECTORY = "runfiles.directory";
  private static final String KEEPGENERATEDFILES = "keepgeneratedfiles";
  private static final String MODEL_DATA_URL = "model.data.url";
  private static final String PRELOADING_INVALID_URL_VALIDATION_MESSAGE = "Preloading versions configured, but no '%s' set.";

  private static final Logger LOG = LoggerFactory.getLogger(EngineWorkerConfigurationBuilder.class);

  private final ModelDataUtilPreloader<V, E> preloader;
  private final boolean hasRunDirectory;

  protected EngineWorkerConfigurationBuilder(final Properties properties, final String prefix, final ModelDataUtilPreloader<V, E> preloader,
      final boolean hasRunDirectory) {
    super(properties, prefix);
    this.preloader = preloader;
    this.hasRunDirectory = hasRunDirectory;
  }

  @Override
  protected boolean isActive() {
    return workerProperties.isActive();
  }

  @Override
  protected final void build(final E configuration) {
    configuration.setVersionsRoot(getVersionsRoot());
    configuration.setModelPreloadVersions(getModelPreloadVersions());
    configuration.setModelDataUrl(getModelDataUrl());
    configureRunDirectory(configuration);
    configuration.setKeepGeneratedFiles(isKeepGeneratedFiles());
    buildEngine(configuration);
  }

  /**
   * Implement engine worker specific configuration settings
   *
   * @param workerProperties worker properties object to get the specific settings from
   * @return configuration object
   */
  protected abstract void buildEngine(E configuration);

  private void configureRunDirectory(final E configuration) {
    if (hasRunDirectory) {
      final File runFilesDirectory = getRunFilesDirectory();

      configuration.setRunFilesDirectory(runFilesDirectory);
      if (runFilesDirectory != null && !runFilesDirectory.exists()) {
        runFilesDirectory.mkdirs();
      }
    }
  }

  @Override
  protected List<String> validate(final E configuration) {
    final ConfigurationValidator validator = new ConfigurationValidator();
    if (validator.validateRequiredProperty(workerProperties.getFullPropertyName(ROOT), configuration.getVersionsRoot())) {
      // root directory must be writable if data is to be downloaded dynamically. Because this is somewhat difficult to
      // determine here if data is present we just check if the directory is empty than it should be writable.
      final boolean writable = configuration.getVersionsRoot().exists() && configuration.getVersionsRoot().isDirectory() &&
          configuration.getVersionsRoot().listFiles().length == 0;

      validator.validateDirectory(workerProperties.getFullPropertyName(ROOT), configuration.getVersionsRoot(), writable);
    }
    if (hasRunDirectory) {
      validator.validateDirectory(workerProperties.getFullPropertyName(RUNFILES_DIRECTORY), configuration.getRunFilesDirectory(), true);
    }
    return preloadWithValidations(configuration, validator);
  }

  protected List<String> preloadWithValidations(final E configuration, final ConfigurationValidator validator) {
    if (!configuration.getModelPreloadVersions().isEmpty()) {
      validator.validateRequiredProperty(workerProperties.getFullPropertyName(MODEL_DATA_URL), PRELOADING_INVALID_URL_VALIDATION_MESSAGE,
          configuration.getModelDataUrl());
    }
    return preload(configuration, validator.getValidationErrors());
  }

  private List<String> preload(final E configuration, final List<String> validationErrors) {
    try {
      // only preload if no validation errors.
      if (validationErrors.isEmpty() && preloader != null) {
        preloader.ensureModelDataPresence(configuration);
      }
    } catch (final Exception e) {
      LOG.error("Preloading failed.", e);
      validationErrors.add(e.getMessage());
    }
    return validationErrors;
  }

  protected File getVersionsRoot() {
    final String root = workerProperties.getProperty(ROOT);

    if (root == null) {
      return null;
    }
    return new File(root);
  }

  protected abstract V getVersionByLabel(String version);

  protected List<V> getModelPreloadVersions() {
    return workerProperties.getPropertyOptional(MODEL_PRELOAD_VERSIONS)
        .map(versions -> Arrays.asList(versions.split(",")))
        .orElse(Collections.emptyList())
        .stream()
        .map(this::getVersionByLabel)
        .toList();
  }

  private String getModelDataUrl() {
    return workerProperties.getProperty(MODEL_DATA_URL);
  }

  private File getRunFilesDirectory() {
    final String directory = workerProperties.getProperty(RUNFILES_DIRECTORY);
    return directory == null ? null : new File(directory);
  }

  private boolean isKeepGeneratedFiles() {
    return workerProperties.getPropertyBooleanSafe(KEEPGENERATEDFILES);
  }
}
