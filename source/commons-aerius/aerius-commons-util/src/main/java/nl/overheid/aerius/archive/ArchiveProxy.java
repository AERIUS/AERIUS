/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.archive;

import java.io.IOException;
import java.util.Map;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.archive.InCombinationProcessRequest.Receptor;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.http.HttpException;

/**
 * Proxy to collect data from the In Combination Process Contribution Archive wrapping to and from JSON to Java objects.
 */
public class ArchiveProxy {

  private static final String IN_COMBINATION_PROCESS_CONTRIBUTION = "/in-combination-process-contribution";
  private static final String DPS_METHOD = "/development-pressure-search";
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final String API_KEY_HEADER = "api-key";

  private final HttpClientProxy httpClientProxy;
  private final String archiveUrl;
  private final String archiveApiKey;

  public ArchiveProxy(final HttpClientProxy httpClientProxy, final String archiveUrl, final String archiveApiKey) {
    this.httpClientProxy = httpClientProxy;
    this.archiveUrl = archiveUrl;
    this.archiveApiKey = archiveApiKey;
  }

  public InCombinationProcessResponse getInCombinationProcess(final int calculationYear, final Map<Integer, Integer> receptorIdToLevel)
      throws IOException {
    final HttpPost httpPost = new HttpPost(archiveUrl + IN_COMBINATION_PROCESS_CONTRIBUTION);

    httpPost.setHeader(API_KEY_HEADER, archiveApiKey);
    httpPost.setEntity(
        new StringEntity(OBJECT_MAPPER.writeValueAsString(createICPRequest(calculationYear, receptorIdToLevel)), ContentType.APPLICATION_JSON));
    try {
      return OBJECT_MAPPER.readValue(httpClientProxy.execute(httpPost), InCombinationProcessResponse.class);
    } catch (final HttpException e) {
      throw new IOException(e);
    }
  }

  private static InCombinationProcessRequest createICPRequest(final int calculationYear, final Map<Integer, Integer> receptorIdToLevel) {
    return new InCombinationProcessRequest(calculationYear,
        receptorIdToLevel.entrySet().stream().map(e -> new Receptor(e.getKey(), e.getValue())).toList());
  }

  public DevelopmentPressureSearchResponse developmentPressureSearch(final String polygonWkt)
      throws IOException {
    final HttpPost httpPost = new HttpPost(archiveUrl + DPS_METHOD);

    httpPost.setHeader(API_KEY_HEADER, archiveApiKey);
    httpPost.setEntity(
        new StringEntity(OBJECT_MAPPER.writeValueAsString(new DevelopmentPressureSearchRequest(polygonWkt)), ContentType.APPLICATION_JSON));
    try {
      return OBJECT_MAPPER.readValue(httpClientProxy.execute(httpPost), DevelopmentPressureSearchResponse.class);
    } catch (final HttpException e) {
      throw new IOException(e);
    }
  }
}
