/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.processmonitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.exec.ProcessListener;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Service class to keep track of cancelled jobs, and can cancel running processes that are registered with this service.
 */
public class ProcessMonitorService {
  private static final Logger LOG = LoggerFactory.getLogger(ProcessMonitorService.class);

  private final Map<String, Long> jobKeyCache = new TreeMap<>();
  private final Map<String, ProcessListenerImpl> processListeners = new HashMap<>();
  private final Supplier<Long> now;
  private final Object mutex = new Object();

  public ProcessMonitorService() {
    this(System::currentTimeMillis);
  }

  ProcessMonitorService(final Supplier<Long> now) {
    this.now = now;
  }

  /**
   * Returns true if the job is registered as a cancelled job. But it can be a process running for this job is still running.
   *
   * @param jobKey key for the job to check
   * @return returns true if the job is registered as being cancelled
   */
  public boolean isCancelledJob(final String jobKey) {
    synchronized (mutex) {
      return jobKeyCache.containsKey(jobKey);
    }
  }

  /**
   * Creates a new processListener and registers it internally to keep track of running processes.
   * The processListener can be used to register processes associated with the given job key,
   * and to also remove the finished processes.
   *
   * @param jobKey key of the job to register a new process listener for.
   * @return the new process listener.
   * @throws AeriusException throws a job cancelled exception in case the job already is known to be cancelled
   */
  public ProcessListener registerProcessListener(final String jobKey) throws AeriusException {
    synchronized (mutex) {
      if (isCancelledJob(jobKey)) {
        throw new AeriusException(AeriusExceptionReason.CONNECT_JOB_CANCELLED);
      }
      return processListeners.computeIfAbsent(jobKey, k -> new ProcessListenerImpl(() -> processListeners.remove(jobKey)));
    }
  }

  /**
   * Cleans the job cache of old entries that are older than the given number of days.
   * If there is still a process listener registered the job won't be cleaned.
   *
   * @param nrOfDaysOld Number of days old jobs should be before being cleaned.
   */
  public void cleanCache(final long nrOfDaysOld) {
    synchronized (mutex) {
      final long toOld = now.get() - TimeUnit.DAYS.toMillis(nrOfDaysOld);
      final List<String> toRemove = jobKeyCache.entrySet().stream()
          .filter(e -> e.getValue() < toOld)
          .filter(e -> containsNoAliveProcesses(e.getKey()))
          .map(Map.Entry::getKey)
          .toList();
      // Use separate list to avoid concurrent modification problems.
      toRemove.forEach(jobKeyCache::remove);
      // Clean any dead processes from the list
      processListeners.forEach(this::cleanDeadProcesses);
    }
  }

  private boolean containsNoAliveProcesses(final String jobKey) {
    return Optional.ofNullable(processListeners.getOrDefault(jobKey, null))
        .map(p -> p.stream().noneMatch(Process::isAlive))
        .orElse(true);
  }

  private void cleanDeadProcesses(final String jobKey, final ProcessListenerImpl processListener) {
    final List<Process> deadProcesses = processListener.stream().filter(p -> !p.isAlive()).toList();

    deadProcesses.forEach(processListener::remove);
  }

  /**
   * Kills any process being registered with the job key and mark the job as being cancelled.
   *
   * @param jobKey key for job being cancelled.
   */
  public void killCancelledJob(final String jobKey) {
    synchronized (mutex) {
      LOG.debug("Kill event for job: {}", jobKey);
      // job key can occur multiple times, each time it will override the entry, but that is intended,
      // because it will update the time stamp to be the latest time stamp.
      jobKeyCache.put(jobKey, now.get());
      killProcessesForKey(jobKey);
    }
  }

  /**
   * Kills all processes still registered for jobs that are being marked as cancelled.
   */
  public void killAllCancelledJobs() {
    synchronized (mutex) {
      jobKeyCache.entrySet().forEach(e -> killProcessesForKey(e.getKey()));
    }
  }

  /**
   * Kill processes that are associated with the given jobKey.
   *
   * @param jobKey jobKey to kill processes for
   */
  private void killProcessesForKey(final String jobKey) {
    Optional.ofNullable(processListeners.get(jobKey)).ifPresent(p -> p.forEach(process -> killProcess(process, jobKey)));
  }

  private void killProcess(final Process process, final String jobKey) {
    if (process.isAlive()) {
      process.destroyForcibly();
    }
  }

  private class ProcessListenerImpl extends ArrayList<Process> implements ProcessListener {
    private final Runnable cleanListener;

    ProcessListenerImpl(final Runnable cleanListener) {
      this.cleanListener = cleanListener;
    }

    @Override
    public void registerProcess(final Process process) {
      add(process);
    }

    @Override
    public void removeProcess(final Process process) {
      synchronized (mutex) {
        remove(process);
        if (isEmpty()) {
          cleanListener.run();
        }
      }
    }
  }
}
