# AERIUS

AERIUS is the calculation tool for the environment.
AERIUS consists of several products, each aimed at a specific one user task.


See [AERIUS website](https://link.aerius.nl/website) for more background details, as well as fact sheets, and user manuals.

## About AERIUS

AERIUS was developed to support the Integrated Approach to Nitrogen (PAS), the Dutch policy for coping with high nitrogen deposition.
However, in 2019 the Dutch Council of State (Raad van State) declared the PAS void.
Since then AERIUS has been repositioned as the calculation tool for calculating Nitrogen depositions and can still be used as the basis for permit applications.
For more information about the Dutch approach to nitrogen see [aanpakstikstof.nl](https://www.aanpakstikstof.nl/) (Dutch).

In 2020 the AERIUS calculation core was extended to support calculating air quality as defined under the [Nationaal Samenwerkingsprogramma Luchtkwaliteit](https://www.infomil.nl/onderwerpen/lucht-water/luchtkwaliteit/regelgeving/wet-milieubeheer/nsl/) (NSL).
Since Januari 2023 the program CIMLK ([Centraal Instrument Monitoring Luchtkwaliteit](https://www.cimlk.nl/)) is the leading air quality monitoring tool for NSL, and later the Omgevingswet.
Both nitrogen and air quality calculations for road traffic are based on the same SRM model.
Therefor it is only logical to use the same open implementation of that model.

In 2021 the AERIUS pilot project in the United Kingdom was initiated by the [JNCC](https://jncc.gov.uk/our-work/air-pollution-assessment-service/) under the name Air Pollution Assessment Service (APAS).
The APAS project will develop an online tool to support UK risk assessment of air pollution effects on ecosystems, statutory reporting requirements and also the potential to support the issue of permissions for individual plans or projects (for example, Environmental Permits and planning permission).

## AERIUS Development

### OSS development paradigm

AERIUS is developed using git.
To effectively use git read the [OSS Development paradigm](doc/git/oss_development_paradigm.md) page on how to effectively work with git.

### Development, Debugging and Testing

AERIUS is build with open source technologies.
How to setup a development environment, build the project and debug the project the project read the [development setup page](doc/development_setup.md) page.
For how to test the project read the [Cypress test](source/calculator/aerius-calculator-wui-client-test/README.md) page.
Information on Releases, Themes and Versions can be found on the [product versions page](doc/product_versions.md).

### Production and Deploying

To learn more about running AERIUS in a production environment, read the [deploy](doc/deploy.md) page.

### Manuals and Background Information

The [manuals and background information](https://www.aerius.nl/) (Dutch) for all AERIUS products can be found on the AERIUS website.

### Architecture, Design Patterns

To learn more about the architecture, project structure, design patterns, goals and pointers see:

[Non Functional Requirements](doc/non_functional_requirements.md)

[High Level Architecture](doc/high_level_architecture.md)

[Project Module Structure](doc/project_structure.md)

[Scaling AERIUS](doc/scaling_aerius.md)

[Design patterns](doc/design_patterns.md)

[Adding multilingual support](doc/i18n.md)

### Database development

PostgreSQL conventions ([dutch](doc/database/postgresql_conventies.md))

Common modules descriptions ([dutch](doc/database/AERIUS_common_modules.md))

Database buildscript and datasources guide ([dutch](doc/database/buildsysteem_en_databronnen.md))

Database fundamentals ([dutch](doc/database/AERIUS_db_implementatie_keuzes.md))

[Setting up the database development environment on Windows](doc/database/how_to_setup_the_database_build_environment_on_windows.md)

### IMAER development

Coding guidelines ([dutch](doc/imaer/imaer_coding_guidelines_en_werkwijze.md))

### Disclaimer

Although the source code published here is of the AERIUS products no rights are granted.
The owner of AERIUS assumes no liability.
AERIUS is a registered trademark in the Benelux.
All rights not expressly granted herein are reserved.

### LICENSE

Copyright [the State of the Netherlands](https://www.government.nl)
Crown Copyright

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.

